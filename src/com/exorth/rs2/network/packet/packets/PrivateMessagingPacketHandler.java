package com.exorth.rs2.network.packet.packets;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;
import com.exorth.rs2.network.security.PmLog;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.NameUtility;

public class PrivateMessagingPacketHandler implements PacketHandler {

	public static final int ADD_FRIEND = 188;
	public static final int REMOVE_FRIEND = 215;
	public static final int ADD_IGNORE = 133;
	public static final int REMOVE_IGNORE = 74;
	public static final int SEND_PM = 126;

	@Override
	public void handlePacket(Player player, Packet packet) {
		switch (packet.getOpcode()) {
			case ADD_FRIEND:
				long name = packet.getIn().readLong();
				player.getPrivateMessaging().addToFriendsList(name);
				break;
			case REMOVE_FRIEND:
				name = packet.getIn().readLong();
				player.getPrivateMessaging().removeFromList(player.getFriends(), name);
				break;
			case ADD_IGNORE:
				name = packet.getIn().readLong();
				player.getPrivateMessaging().addToIgnoresList(name);
				break;
			case REMOVE_IGNORE:
				name = packet.getIn().readLong();
				player.getPrivateMessaging().removeFromList(player.getIgnores(), name);
				break;
			case SEND_PM:
				long to = packet.getIn().readLong();
				int size = packet.getPacketLength() - 8;
				byte[] message = packet.getIn().readBytes(size);
				
				/**
				 * Decode the message.
				 */
				String decoded = Misc.textUnpack(message, size);
				decoded = Misc.filterText(decoded);
				decoded = Misc.optimizeText(decoded);
				
				/**
				 * Convert receiver name from long to string.
				 */
				String receiver = NameUtility.longToName(to);
				receiver = Misc.capitalize(receiver);
				
				player.getPrivateMessaging().sendPrivateMessage(player, to, message, size);
				PmLog.recordPm(player.getUsername(), receiver, decoded);
				break;
		}
	}
}
