package com.exorth.rs2.network.packet.packets;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.StreamBuffer;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;
import com.exorth.rs2.util.Language;
import com.exorth.rs2.util.clip.PathFinder;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class WalkPacketHandler implements PacketHandler {

	public static final int MINI_MAP_WALK = 248;
	public static final int MAIN_WALK = 164;
	public static final int OTHER_WALK = 98;

	@Override
	public void handlePacket(Player player, Packet packet) {
		int length = packet.getPacketLength();

		if (player.getAttribute("isFrozen") != null) {
			player.getActionSender().sendMessage(Language.MAGICAL_FORCE);
			player.getActionSender().removeMapFlag();
			return;
		}

		if (!player.canWalk()) {
			player.getActionSender().sendMessage("You can't move.");
			player.getActionSender().removeMapFlag();
			return;
		}

		if (player.getAttribute("isStunned") != null) {
			player.getActionSender().sendMessage("You're stunned!");
			player.getActionSender().removeMapFlag();
			return;
		}

		if (packet.getOpcode() == MINI_MAP_WALK) {
			length -= 14;
		}

		player.getMovementHandler().reset();

		int steps = (length - 5) / 2;
		int[][] path = new int[steps][2];
		int firstStepX = packet.getIn().readShort(StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);

		for (int i = 0; i < steps; i++) {
			path[i][0] = packet.getIn().readByte(true);
			path[i][1] = packet.getIn().readByte(true);
		}

		int firstStepY = packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE);

		boolean noclip = player.getAttribute("noclip") != null;

		if (noclip) {
			player.getMovementHandler().reset();
			player.getMovementHandler().addToPath(new Position(firstStepX, firstStepY));
		} else {
			PathFinder.getSingleton().moveTo(player, firstStepX, firstStepY);
		}

		if (noclip) {
			for (int i = 0; i < steps; i++) {
				path[i][0] += firstStepX;
				path[i][1] += firstStepY;
				player.getMovementHandler().addToPath(new Position(path[i][0], path[i][1]));
			}
		} else {
			for (int i = 0; i < steps; i++) {
				path[i][0] += firstStepX;
				path[i][1] += firstStepY;
				PathFinder.getSingleton().moveTo(player, path[i][0], path[i][1]);
			}
		}

		if (player.getAttribute("runLocked") == null) {
			player.getMovementHandler().setRunPath(packet.getIn().readByte(StreamBuffer.ValueType.C) == 1);
		}
		if (noclip) {
			player.getMovementHandler().finish();
		}

		if ((firstStepX != player.getPosition().getX() || firstStepY != player.getPosition().getY()) || player.isInstigatingAttack()) {
			player.getMovementHandler().resetActionsOnMovement();
		}
	}
}
