package com.exorth.rs2.network.packet.packets;

import com.exorth.rs2.Constants;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;

/**
 * This packet should be secured enough to prevent invisible hacks.
 * 
 * @author AkZu
 * 
 */
public class AppearancePacketHandler implements PacketHandler {

	public static final int APPEARANCE = 101;

	private static final short[][] MALE_VALUES = {
		{ 0, 8 }, // head
		{ 10, 17 }, // jaw
		{ 18, 25 }, // torso
		{ 26, 31 }, // arms
		{ 33, 34 }, // hands
		{ 36, 40 },// legs
		{ 42, 43 } // feet
	};

	private static final short[][] FEMALE_VALUES = {
		{ 45, 54 }, // head
		{ 0, 255 }, // jaw
		{ 56, 60 }, // torso
		{ 61, 65 }, // arms
		{ 67, 68 }, // hands
		{ 70, 77 }, // legs
		{ 79, 80 }, // feet
	};

	private static final short[][] MALE_VALUES_474 = {
		{ 90, 96 }, // head 474
		{ 97, 103 }, // jaw 474
		{ 110, 115 }, // torso 474
		{ 104, 109 }, // arms 474
		{ 116, 120 }, // hands 474
		{ 84, 89 },// legs 474
		{ 42, 43 } // feet
	};

	private static final short[][] FEMALE_VALUES_474 = {
		{ 128, 139 }, // head 474
		{ 0, 255 }, // jaw 474 (because readByte signed false)
		{ 146, 151 }, // torso 474
		{ 140, 145 }, // arms 474
		{ 152, 155 }, // hands 474
		{ 121, 127 }, // legs 474
		{ 79, 80 }, // feet
	};

	private static final byte[][] ALLOWED_COLORS = {
		{ 0, 24 }, // hair color
		{ 0, 28 }, // torso color
		{ 0, 28 }, // legs color
		{ 0, 5 }, // feet color
		{ 0, 7 } // skin color
	};

	@Override
	public void handlePacket(Player player, Packet packet) {
		byte gender = (byte) packet.getIn().readByte();
		if (gender != 0 && gender != 1) {
			return;
		}
		player.setGender(gender);

		short[] data = new short[12];
		for (int i = 0; i < 12; i++) {
			data[i] = (short) packet.getIn().readByte(false);

			// If the data received was didn't match given values, return.
			if (i < 7 && !(data[i] >= (data[i] >= 84 ? (gender == 0 ? MALE_VALUES_474[i][0] : FEMALE_VALUES_474[i][0]) : (gender == 0 ? MALE_VALUES[i][0] : FEMALE_VALUES[i][0])) && data[i] <= (data[i] >= 84 ? (gender == 0 ? MALE_VALUES_474[i][1] : FEMALE_VALUES_474[i][1]) : (gender == 0 ? MALE_VALUES[i][1] : FEMALE_VALUES[i][1])))) {
				return;
			} else if (i >= 7 && !(data[i] >= ALLOWED_COLORS[i - 7][0] && data[i] <= ALLOWED_COLORS[i - 7][1])) {
				return;
			}
		}

		player.getAppearance()[Constants.APPEARANCE_SLOT_HEAD] = data[0];
		player.getAppearance()[Constants.APPEARANCE_SLOT_BEARD] = data[1];
		player.getAppearance()[Constants.APPEARANCE_SLOT_CHEST] = data[2];
		player.getAppearance()[Constants.APPEARANCE_SLOT_ARMS] = data[3];
		player.getAppearance()[Constants.APPEARANCE_SLOT_HANDS] = data[4];
		player.getAppearance()[Constants.APPEARANCE_SLOT_LEGS] = data[5];
		player.getAppearance()[Constants.APPEARANCE_SLOT_FEET] = data[6];

		player.getColors()[0] = (byte) data[7];
		player.getColors()[1] = (byte) data[8];
		player.getColors()[2] = (byte) data[9];
		player.getColors()[3] = (byte) data[10];
		player.getColors()[4] = (byte) data[11];
	}
}
