package com.exorth.rs2.network.packet.packets;

import com.exorth.rs2.content.BankPin.AtInterfaceStatus;
import com.exorth.rs2.content.duel.DuelArena;
import com.exorth.rs2.content.shop.ShopManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Player.DiceStage;
import com.exorth.rs2.model.players.Player.TeleotherStage;
import com.exorth.rs2.model.players.Player.TradeStage;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;

public class CloseInterfacePacketHandler implements PacketHandler {

	public static final int CLOSE_INTERFACE = 130;

	@Override
	public void handlePacket(Player player, Packet packet) {
		if (player.getAttribute("teleotherPartnerId") != null) {
			player.setTeleotherStage(TeleotherStage.WAITING);
			player.removeAttribute("teleotherPartnerId");
			player.removeAttribute("teleotherSpellId");
		}

		player.removeAttribute("DIALOG_CLICK_ID");
		player.setInteractingEntity(null);

		if (player.getTradeStage() != TradeStage.WAITING && player.getTradeStage() != TradeStage.SEND_REQUEST) {
			player.getTrading().declineTrade(player);
		}
		
		if (player.getDiceStage() != DiceStage.WAITING && player.getDiceStage() != DiceStage.SEND_REQUEST) {
			player.getDicing().declineBet(player);
		}

		if ((String) player.getAttribute("duel_stage") != "WAITING" && (String) player.getAttribute("duel_stage") != "VICTORY" && (String) player.getAttribute("duel_stage") != "INGAME" && (String) player.getAttribute("duel_stage") != "SENT_REQUEST") {
			DuelArena.getInstance().declineDuel(player, false);
		}

		if ((String) player.getAttribute("duel_stage") == "VICTORY") {
			DuelArena.getInstance().give_won_items(player);
		}

		if (player.getBankPin().getInterfaceStatus() != AtInterfaceStatus.NONE) {
			player.getBankPin().setAtInterfaceStatus(AtInterfaceStatus.NONE);
		}

		if (player.getAttribute("shop") != null || player.getAttribute("shopListener") != null) {
			ShopManager.close(player);
		}

		if (player.getAttribute("isBanking") != null) {
			player.getBank().shift();
			player.removeAttribute("BANK_MODE_NOTE");
			player.removeAttribute("BANK_MODE_INSERT");
			player.getActionSender().sendConfig(304, 0);
			player.getActionSender().sendConfig(115, 0);
			player.removeAttribute("isBanking");
		}
	}
}
