package com.exorth.rs2.network.packet.packets;

import java.util.logging.Logger;

import com.exorth.rs2.content.duel.DuelArena;
import com.exorth.rs2.content.gambling.dice.*;
import com.exorth.rs2.content.gambling.slots.SlotMachine;
import com.exorth.rs2.content.skills.cooking.Cookable;
import com.exorth.rs2.content.skills.cooking.CookingAction;
import com.exorth.rs2.content.skills.crafting.ArmourCreation;
import com.exorth.rs2.content.skills.crafting.Craftable;
import com.exorth.rs2.content.skills.crafting.CraftingType;
import com.exorth.rs2.content.skills.crafting.Gem;
import com.exorth.rs2.content.skills.crafting.GemCutting;
import com.exorth.rs2.content.skills.crafting.Glass;
import com.exorth.rs2.content.skills.crafting.GlassBlowing;
import com.exorth.rs2.content.skills.crafting.Hide;
import com.exorth.rs2.content.skills.crafting.LeatherTanning;
import com.exorth.rs2.content.skills.fletching.ArrowCreation;
import com.exorth.rs2.content.skills.fletching.ArrowTips;
import com.exorth.rs2.content.skills.fletching.BowCreation;
import com.exorth.rs2.content.skills.fletching.CreationType;
import com.exorth.rs2.content.skills.fletching.FletchableLogs;
import com.exorth.rs2.content.skills.fletching.HeadlessArrowCreation;
import com.exorth.rs2.content.skills.fletching.LogFletching;
import com.exorth.rs2.content.skills.fletching.UnstrungBows;
import com.exorth.rs2.content.skills.herblore.Herbalism;
import com.exorth.rs2.content.skills.herblore.IngredientType;
import com.exorth.rs2.content.skills.herblore.PrimaryIngredient;
import com.exorth.rs2.content.skills.herblore.SecondaryIngredient;
import com.exorth.rs2.content.skills.smithing.Smelting;
import com.exorth.rs2.model.players.BankManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.StreamBuffer;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class ChatInterfacePacketHandler implements PacketHandler {

	private static final Logger logger = Logger.getLogger(ChatInterfacePacketHandler.class.getName());

	int enterAmountId;
	public static final int DIALOGUE = 40;
	public static final int SHOW_ENTER_X = 135;
	public static final int ENTER_X = 208;

	@Override
	public void handlePacket(Player player, Packet packet) {
		switch (packet.getOpcode()) {
			case DIALOGUE:
				handleDialogue(player, packet);
				break;
			case SHOW_ENTER_X:
				showEnterX(player, packet);
				break;
			case ENTER_X:
				handleEnterX(player, packet);
				break;
		}
	}

	/**
	 * 
	 * @param player
	 * @param packet
	 */
	private void handleDialogue(Player player, Packet packet) {
		player.nextDialogue();
	}

	/**
	 * 
	 * @param player
	 * @param packet
	 */
	private void showEnterX(Player player, Packet packet) {
		player.setEnterXSlot(packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE));
		player.setEnterXInterfaceId(packet.getIn().readShort(StreamBuffer.ValueType.A));
		player.setEnterXId(packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE));
		player.getActionSender().sendEnterX();
	}

	/**
	 * Handle Enter X / Handle InterfaceAction #5
	 */
	private void handleEnterX(Player player, Packet packet) {
		int amount = packet.getIn().readInt();
		logger.info("Enter X Interface: " + player.getEnterXInterfaceId());
		//Begin Single Model Interface (ID: 4429)
		if (player.getEnterXInterfaceId() == 1748) { //Single Model Interface - Make X
			if (player.getAttribute("craftingType") != null) {
				switch ((CraftingType) player.getAttribute("craftingType")) {
					case GEM_CRAFTING:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new GemCutting(player, amount, (Gem) player.getAttribute("craftingGem")));
						player.removeAttribute("craftingType");
						player.removeAttribute("craftingGem");
						player.getActionSender().moveComponent(0, 0, 1746);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						}
						logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						break;
				}
			} else if (player.getAttribute("herbloreType") != null) {
				switch ((IngredientType) player.getAttribute("herbloreType")) {
					case PRIMARY_INGREDIENT:
						player.getActionDeque().addAction(new Herbalism(player, amount, (IngredientType) player.getAttribute("herbloreType"), (PrimaryIngredient) player.getAttribute("herbloreIngredient"), null));
						player.getActionSender().removeInterfaces();
						break;
					case SECONDARY_INGREDIENT:
						player.getActionDeque().addAction(new Herbalism(player, amount, IngredientType.SECONDARY_INGREDIENT, null, (SecondaryIngredient) player.getAttribute("herbloreIngredient")));
						player.getActionSender().removeInterfaces();
						player.removeAttribute("herbloreIngredient");
						player.removeAttribute("herbloreType");
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.getAttribute("herbloreIngredient") != null) {
							player.removeAttribute("herbloreIngredient");
						}
						if (player.getAttribute("herbloreType") != null) {
							player.removeAttribute("herbloreType");
						}
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Herblore Type: " + player.getAttribute("herbloreType") + "!");
						}
						logger.severe("Unhandled Herblore Type: " + player.getAttribute("herbloreType") + "!");
						break;
				}
			} else if (player.getAttribute("fletchingType") != null) {
				switch ((CreationType) player.getAttribute("fletchingType")) {
					case BOW_CREATION:
						player.getActionDeque().addAction(new BowCreation(player, amount, (UnstrungBows) player.getAttribute("fletchingItem")));
						player.getActionSender().removeInterfaces();
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingItem");
						break;
					case CROSSBOW_CREATION:
						// TODO: Write and implement.
						player.getActionSender().removeInterfaces();
						break;
					case UNSTRUNG_BOW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new LogFletching(player, amount, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingLog");
						player.getActionSender().moveComponent(0, 0, 1746);
						break;
					case ARROW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new ArrowCreation(player, amount, (ArrowTips) player.getAttribute("arrowTip")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("arrowTip");
						player.removeAttribute("creationAmount");
						player.getActionSender().moveComponent(0, 0, 1746);
						break;
					case HEADLESS_ARROW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new HeadlessArrowCreation(player, amount));
						player.removeAttribute("fletchingType");
						player.removeAttribute("creationAmount");
						player.getActionSender().moveComponent(0, 0, 1746);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						}
						logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		//End Single Model Interface (ID: 4429)
		//Begin Double Interface (ID: 8866)
		} else if (player.getEnterXInterfaceId() == 8871) { //Double Model Interface - Make X (First Model)
			if (player.getAttribute("fletchingType") != null) {
				switch ((CreationType) player.getAttribute("fletchingType")) {
					case UNSTRUNG_BOW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new LogFletching(player, amount, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingLog");
						player.getActionSender().moveComponent(0, 0, 8869);
						player.getActionSender().moveComponent(0, 0, 8870);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						}
						logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		} else if (player.getEnterXInterfaceId() == 8875) { //Double Model Interface - Make X (Second Model)
			if (player.getAttribute("fletchingType") != null) {
				switch ((CreationType) player.getAttribute("fletchingType")) {
					case UNSTRUNG_BOW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new LogFletching(player, amount, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingLog");
						player.getActionSender().moveComponent(0, 0, 8869);
						player.getActionSender().moveComponent(0, 0, 8870);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						}
						logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		//End Double Interface (ID: 8866)
		//Begin Triple Interface (ID: 8880)
		} else if (player.getEnterXInterfaceId() == 8886) { //Triple Model Interface - Make X (First Model)
			if (player.getAttribute("fletchingType") != null) {
				switch ((CreationType) player.getAttribute("fletchingType")) {
					case UNSTRUNG_BOW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new LogFletching(player, amount, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingLog");
						player.getActionSender().moveComponent(0, 0, 8883);
						player.getActionSender().moveComponent(0, 0, 8884);
						player.getActionSender().moveComponent(0, 0, 8885);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						}
						logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						break;
				}
			} else if (player.getAttribute("craftingType") != null) {
				switch ((CraftingType) player.getAttribute("craftingType")) {
					case ARMOUR_CREATION:
						if (player.getAttribute("craftingHide") == null) {
							return;
						}
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new ArmourCreation(player, amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
						player.removeAttribute("craftingHide");
						player.removeAttribute("craftingType");
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						}
						logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		} else if (player.getEnterXInterfaceId() == 8890) { //Triple Model Interface - Make X (Second Model)
			if (player.getAttribute("fletchingType") != null) {
				switch ((CreationType) player.getAttribute("fletchingType")) {
					case UNSTRUNG_BOW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new LogFletching(player, amount, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingLog");
						player.getActionSender().moveComponent(0, 0, 8883);
						player.getActionSender().moveComponent(0, 0, 8884);
						player.getActionSender().moveComponent(0, 0, 8885);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						}
						logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						break;
				}
			} else if (player.getAttribute("craftingType") != null) {
				switch ((CraftingType) player.getAttribute("craftingType")) {
					case ARMOUR_CREATION:
						if (player.getAttribute("craftingHide") == null) {
							return;
						}
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new ArmourCreation(player, amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
						player.removeAttribute("craftingHide");
						player.removeAttribute("craftingType");
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						}
						logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		} else if (player.getEnterXInterfaceId() == 8894) { //Triple Model Interface - Make X (Third Model)
			if (player.getAttribute("fletchingType") != null) {
				switch ((CreationType) player.getAttribute("fletchingType")) {
					case UNSTRUNG_BOW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new LogFletching(player, amount, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingLog");
						player.getActionSender().moveComponent(0, 0, 8883);
						player.getActionSender().moveComponent(0, 0, 8884);
						player.getActionSender().moveComponent(0, 0, 8885);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						}
						logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						break;
				}
			} else if (player.getAttribute("craftingType") != null) {
				switch ((CraftingType) player.getAttribute("craftingType")) {
					case ARMOUR_CREATION:
						if (player.getAttribute("craftingHide") == null) {
							return;
						}
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new ArmourCreation(player, amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
						player.removeAttribute("craftingHide");
						player.removeAttribute("craftingType");
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						}
						logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		//End Triple Interface (ID: 8880)
		//Begin Quadruple Interface (ID: 8899)
		} else if (player.getEnterXInterfaceId() == 8906) { //Quadruple Model Interface - Make X (First Model)
			if (player.getAttribute("fletchingType") != null) {
				switch ((CreationType) player.getAttribute("fletchingType")) {
					case UNSTRUNG_BOW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new LogFletching(player, amount, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingLog");
						player.getActionSender().moveComponent(0, 0, 8902);
						player.getActionSender().moveComponent(0, 0, 8903);
						player.getActionSender().moveComponent(0, 0, 8904);
						player.getActionSender().moveComponent(0, 0, 8905);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						}
						logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		} else if (player.getEnterXInterfaceId() == 8910) { //Quadruple Model Interface - Make X (Second Model)
			if (player.getAttribute("fletchingType") != null) {
				switch ((CreationType) player.getAttribute("fletchingType")) {
					case UNSTRUNG_BOW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new LogFletching(player, amount, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingLog");
						player.getActionSender().moveComponent(0, 0, 8902);
						player.getActionSender().moveComponent(0, 0, 8903);
						player.getActionSender().moveComponent(0, 0, 8904);
						player.getActionSender().moveComponent(0, 0, 8905);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						}
						logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		} else if (player.getEnterXInterfaceId() == 8914) { //Quadruple Model Interface - Make X (Third Model)
			if (player.getAttribute("fletchingType") != null) {
				switch ((CreationType) player.getAttribute("fletchingType")) {
					case UNSTRUNG_BOW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new LogFletching(player, amount, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingLog");
						player.getActionSender().moveComponent(0, 0, 8902);
						player.getActionSender().moveComponent(0, 0, 8903);
						player.getActionSender().moveComponent(0, 0, 8904);
						player.getActionSender().moveComponent(0, 0, 8905);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						}
						logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		} else if (player.getEnterXInterfaceId() == 8918) { //Quadruple Model Interface - Make X (Fourth Model)
			if (player.getAttribute("fletchingType") != null) {
				switch ((CreationType) player.getAttribute("fletchingType")) {
					case UNSTRUNG_BOW_CREATION:
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new LogFletching(player, amount, 3, (FletchableLogs) player.getAttribute("fletchingLog")));
						player.removeAttribute("fletchingType");
						player.removeAttribute("fletchingLog");
						player.getActionSender().moveComponent(0, 0, 8902);
						player.getActionSender().moveComponent(0, 0, 8903);
						player.getActionSender().moveComponent(0, 0, 8904);
						player.getActionSender().moveComponent(0, 0, 8905);
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						}
						logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		//End Quadruple Interface (ID: 8899)
		//Begin Quintuple Interface (ID: 8938)
		} else if (player.getEnterXInterfaceId() == 8946) {
			if (player.getAttribute("craftingType") != null) {
				switch ((CraftingType) player.getAttribute("craftingType")) {
					case ARMOUR_CREATION:
						if (player.getAttribute("craftingHide") == null) {
							return;
						}
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new ArmourCreation(player, amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
						player.removeAttribute("craftingHide");
						player.removeAttribute("craftingType");
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						}
						logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		} else if (player.getEnterXInterfaceId() == 8950) {
			if (player.getAttribute("craftingType") != null) {
				switch ((CraftingType) player.getAttribute("craftingType")) {
					case ARMOUR_CREATION:
						if (player.getAttribute("craftingHide") == null) {
							return;
						}
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new ArmourCreation(player, amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
						player.removeAttribute("craftingHide");
						player.removeAttribute("craftingType");
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						}
						logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		} else if (player.getEnterXInterfaceId() == 8954) {
			if (player.getAttribute("craftingType") != null) {
				switch ((CraftingType) player.getAttribute("craftingType")) {
					case ARMOUR_CREATION:
						if (player.getAttribute("craftingHide") == null) {
							return;
						}
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new ArmourCreation(player, amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
						player.removeAttribute("craftingHide");
						player.removeAttribute("craftingType");
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						}
						logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		} else if (player.getEnterXInterfaceId() == 8958) {
			if (player.getAttribute("craftingType") != null) {
				switch ((CraftingType) player.getAttribute("craftingType")) {
					case ARMOUR_CREATION:
						if (player.getAttribute("craftingHide") == null) {
							return;
						}
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new ArmourCreation(player, amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[3])));
						player.removeAttribute("craftingHide");
						player.removeAttribute("craftingType");
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						}
						logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		} else if (player.getEnterXInterfaceId() == 8962) {
			if (player.getAttribute("craftingType") != null) {
				switch ((CraftingType) player.getAttribute("craftingType")) {
					case ARMOUR_CREATION:
						if (player.getAttribute("craftingHide") == null) {
							return;
						}
						player.getActionSender().removeInterfaces();
						player.getActionDeque().addAction(new ArmourCreation(player, amount, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[4])));
						player.removeAttribute("craftingHide");
						player.removeAttribute("craftingType");
						break;
					default:
						player.getActionSender().removeInterfaces();
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						}
						logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
						break;
				}
			} else {
				player.getActionSender().removeInterfaces();
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
				}
				logger.severe("Unhandled Creation! Button ID: " + player.getEnterXInterfaceId() + ".");
			}
		//End Quintuple Interface (ID: 8938)
		} else if (player.getEnterXInterfaceId() == 2402) {
			player.getActionSender().removeInterfaces();
			PeteDicing dice = new PeteDicing(player, amount);
			dice.playPete();
		} else if (player.getEnterXInterfaceId() == 2403) {
			player.getActionSender().removeInterfaces();
			SlotMachine slots = new SlotMachine();
			slots.playSlotsAutomatic(player, slots, amount);
			System.out.println(amount);
		} else if (player.getEnterXInterfaceId() == 5064) {
			BankManager.bankItem(player, player.getEnterXSlot(), player.getEnterXId(), amount);
		} else if (player.getEnterXInterfaceId() == 5382) {
			BankManager.withdrawItem(player, player.getEnterXSlot(), player.getEnterXId(), amount);
		} else if (player.getEnterXInterfaceId() == 3322) {
			player.getTrading().offerItem(player, player.getEnterXSlot(), player.getEnterXId(), amount);
		} else if (player.getEnterXInterfaceId() == 6669) {
			DuelArena.getInstance().removeItem(player, player.getEnterXSlot(), amount);
		} else if (player.getEnterXInterfaceId() == 6574) {
			DuelArena.getInstance().offerItem(player, player.getEnterXSlot(), amount);
		} else if (player.getEnterXInterfaceId() == 3415) {
			player.getTrading().removeTradeItem(player, player.getEnterXSlot(), player.getEnterXId(), amount);
		//Begin Glassblowing
		} else if (player.getEnterXInterfaceId() == 11471) { //Make X Vials
			player.getActionSender().removeInterfaces();
			player.getActionDeque().addAction(new GlassBlowing(player, amount, Glass.forReward(229)));
		} else if (player.getEnterXInterfaceId() == 11475) { //Make X Uncharged Orbs
			player.getActionSender().removeInterfaces();
			player.getActionDeque().addAction(new GlassBlowing(player, amount, Glass.forReward(567)));
		} else if (player.getEnterXInterfaceId() == 12397) { //Make X Beer Glass
			player.getActionSender().removeInterfaces();
			player.getActionDeque().addAction(new GlassBlowing(player, amount, Glass.forReward(1919)));
		} else if (player.getEnterXInterfaceId() == 12401) { //Make X Candle Lantern
			player.getActionSender().removeInterfaces();
			player.getActionDeque().addAction(new GlassBlowing(player, amount, Glass.forReward(4527)));
		} else if (player.getEnterXInterfaceId() == 12405) { //Make X Oil Lamp
			player.getActionSender().removeInterfaces();
			player.getActionDeque().addAction(new GlassBlowing(player, amount, Glass.forReward(4522)));
		} else if (player.getEnterXInterfaceId() == 6200) { //Make X Fish Bowl
			player.getActionSender().removeInterfaces();
			player.getActionDeque().addAction(new GlassBlowing(player, amount, Glass.forReward(6667)));
		} else if (player.getEnterXInterfaceId() == 12409) { //Make X Lantern Lens
			player.getActionSender().removeInterfaces();
			player.getActionDeque().addAction(new GlassBlowing(player, amount, Glass.forReward(4542)));
		//End Glassblowing
		//Begin Tanning
		} else if (player.getEnterXInterfaceId() == 14801) { //Tan X Cowhide -> Soft Leather
			player.getActionDeque().addAction(new LeatherTanning(player, amount, Hide.forReward(1741)));
		} else if (player.getEnterXInterfaceId() == 14802) { //Tan X Cowhide -> Hard Leather
			player.getActionDeque().addAction(new LeatherTanning(player, amount, Hide.forReward(1743)));
		} else if (player.getEnterXInterfaceId() == 14803) { //Tan X Snake Hide -> Snakeskin
			player.getActionDeque().addAction(new LeatherTanning(player, amount, Hide.forId(6287)));
		} else if (player.getEnterXInterfaceId() == 14804) { //Tan X Snake Hide (Swamp) -> Snakeskin
			player.getActionDeque().addAction(new LeatherTanning(player, amount, Hide.forId(7801)));
		} else if (player.getEnterXInterfaceId() == 14805) { //Tan X Green D'hide -> Green D-leather
			player.getActionDeque().addAction(new LeatherTanning(player, amount, Hide.forId(1753)));
		} else if (player.getEnterXInterfaceId() == 14806) { //Tan X Blue D'hide -> Blue D-leather
			player.getActionDeque().addAction(new LeatherTanning(player, amount, Hide.forId(1751)));
		} else if (player.getEnterXInterfaceId() == 14807) { //Tan X Red D'hide -> Red D-leather
			player.getActionDeque().addAction(new LeatherTanning(player, amount, Hide.forId(1749)));
		} else if (player.getEnterXInterfaceId() == 14808) { //Tan X Black D'hide -> Black D-leather
			player.getActionDeque().addAction(new LeatherTanning(player, amount, Hide.forId(1747)));
		//End Tanning
		} else if (player.getEnterXInterfaceId() == 2400) { //Smelt X
			player.getActionSender().removeInterfaces();
			player.getActionDeque().addAction(new Smelting(player, player.getCurrentBar(), amount));
		} else if (player.getEnterXInterfaceId() == 2401) { //Cook X
			player.getActionSender().removeInterfaces();
			Cookable cookItem = Cookable.forId(player.getCookingQueue());
			player.getActionDeque().addAction(new CookingAction(player, cookItem, amount));
		} else if (player.getEnterXInterfaceId() == 1337) {
			if (amount < 1) {
				amount = 1;
			}
			if (player.getEnterXId() != 5) {
				player.getSkill().getLevel()[player.getEnterXId()] = (byte) amount;
			} else {
				player.getSkill().setPrayerPoints(amount);
			}
			player.getSkill().getExp()[player.getEnterXId()] = player.getSkill().getXPForLevel(amount);
			player.getSkill().refresh(player.getEnterXId());
			player.setAppearanceUpdateRequired(true);
		}
	}
}
