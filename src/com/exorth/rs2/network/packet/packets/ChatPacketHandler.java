package com.exorth.rs2.network.packet.packets;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.StreamBuffer;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;
import com.exorth.rs2.network.security.ChatLog;
import com.exorth.rs2.util.Misc;

/**
 * 
 * @author Joshua Barry
 * 
 */
@SuppressWarnings("all")
public class ChatPacketHandler implements PacketHandler {

	public static final int CHAT = 4;

	@Override
	public void handlePacket(Player player, Packet packet) {
		/* muting system by Sneakyhearts */
		if (player.getAttribute("silence_time") != null) {
			long time = (Long) player.getAttribute("silence_time");
			long duration = ((Long) (player.getAttribute("silence_duration")) * 60000);

			if (System.currentTimeMillis() - time <= duration) {
				player.getActionSender().sendMessage("You are currently silenced and cannot be heard by others.");
				player.getActionSender().sendMessage("You will be unsilenced in: " + ((duration - (System.currentTimeMillis() - time)) / 60000) + " minutes.");
				return;
			}
			player.removeAttribute("silence_duration");
			player.removeAttribute("silence_time");
		}

		int effects = packet.getIn().readByte(false, StreamBuffer.ValueType.S);
		int color = packet.getIn().readByte(false, StreamBuffer.ValueType.S);
		int chatLength = (packet.getPacketLength() - 2);
		byte[] text = packet.getIn().readBytesReverse(chatLength, StreamBuffer.ValueType.A);
		String unpacked = Misc.textUnpack(text, chatLength);
		unpacked = Misc.filterText(unpacked);
		unpacked = Misc.optimizeText(unpacked);

		byte[] packed = new byte[chatLength];
		Misc.textPack(packed, unpacked);

		player.setAttribute("chatEffectsMode", (byte) effects);
		player.setChatColor(color);
		player.setChatText(packed);
		player.setChatUpdateRequired(true);
		ChatLog.recordChat(player.getUsername(), unpacked);
	}
}
