package com.exorth.rs2.network.packet.packets;

import java.util.logging.Logger;

import com.exorth.rs2.Constants;
import com.exorth.rs2.Server;
import com.exorth.rs2.content.Explosive;
import com.exorth.rs2.content.TeleportTabs;
import com.exorth.rs2.content.book.ReadingSession;
import com.exorth.rs2.content.book.impl.Baxtorian;
import com.exorth.rs2.content.book.impl.GettingStarted;
import com.exorth.rs2.content.clue.Clue;
import com.exorth.rs2.content.clue.Map;
import com.exorth.rs2.content.clue.Riddle;
import com.exorth.rs2.content.clue.Type;
import com.exorth.rs2.content.consumables.Food;
import com.exorth.rs2.content.consumables.FoodLoader;
import com.exorth.rs2.content.consumables.Potion;
import com.exorth.rs2.content.consumables.PotionLoader;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.impl.ProgressHat;
import com.exorth.rs2.content.duel.DuelArena;
import com.exorth.rs2.content.shop.Shop;
import com.exorth.rs2.content.skills.crafting.CraftingType;
import com.exorth.rs2.content.skills.crafting.Gem;
import com.exorth.rs2.content.skills.crafting.Hide;
import com.exorth.rs2.content.skills.firemaking.FireCraft;
import com.exorth.rs2.content.skills.firemaking.LogType;
import com.exorth.rs2.content.skills.fletching.ArrowTips;
import com.exorth.rs2.content.skills.fletching.CreationType;
import com.exorth.rs2.content.skills.fletching.FletchableLogs;
import com.exorth.rs2.content.skills.fletching.UnstrungBows;
import com.exorth.rs2.content.skills.herblore.HerbIdentifier;
import com.exorth.rs2.content.skills.herblore.HerbType;
import com.exorth.rs2.content.skills.herblore.IngredientType;
import com.exorth.rs2.content.skills.herblore.PrimaryIngredient;
import com.exorth.rs2.content.skills.herblore.SecondaryIngredient;
import com.exorth.rs2.content.skills.prayer.BoneType;
import com.exorth.rs2.content.skills.prayer.BurialAction;
import com.exorth.rs2.content.skills.smithing.Forging;
import com.exorth.rs2.content.skills.smithing.Product;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.BankManager;
import com.exorth.rs2.model.players.GroundItem;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Player.DiceStage;
import com.exorth.rs2.model.players.Player.TradeStage;
import com.exorth.rs2.model.players.shops.PlayerShop;
import com.exorth.rs2.network.StreamBuffer;
import com.exorth.rs2.network.StreamBuffer.ValueType;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;
import com.exorth.rs2.network.security.DropLog;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.task.impl.Dig;
import com.exorth.rs2.util.Language;
import com.exorth.rs2.util.Misc;

/**
 * Refactoring and security checks by Ares & AkZu.
 * 
 * @author Joshua Barry <Ares>
 * @author AkZu
 * 
 */
public class ItemPacketHandler implements PacketHandler {

	public static final int ITEM_OPERATE = 75;
	public static final int DROP_ITEM = 87;
	public static final int PICKUP_ITEM = 236;
	public static final int HANDLE_OPTIONS = 214;
	public static final int PACKET_145 = 145;
	public static final int PACKET_117 = 117;
	public static final int PACKET_43 = 43;
	public static final int PACKET_129 = 129;
	public static final int EQUIP_ITEM = 41;
	public static final int USE_ITEM_ON_ITEM = 53;
	public static final int FIRST_CLICK_ITEM = 122;
	public static final int THIRD_CLICK_ITEM = 16;
	private static final Logger logger = Logger.getLogger(ItemPacketHandler.class.getName());

	@Override
	public void handlePacket(Player player, Packet packet) {
		if (player.getTradeStage().equals(TradeStage.SECOND_TRADE_WINDOW) || player.getTradeStage().equals(TradeStage.ACCEPT)) {
			player.getActionSender().sendMessage(Language.CANT_DO);
			return;
		}
		if (player.isDebugging()) {
			player.getActionSender().sendMessage("Item Packet: <col=CC0000>" + packet.hashCode() + "</col>");
			player.getActionSender().sendMessage("Packet Opcode: <col=CC0000>" + packet.getOpcode() + "</col>, Packet Length: <col=CC0000>" + packet.getPacketLength() + "</col>");
		}
		switch (packet.getOpcode()) {
			case USE_ITEM_ON_ITEM:
				useItemOnItem(player, packet);
				break;
			case ITEM_OPERATE:
				handleAlternativeItemOption(player, packet);
				break;
			case DROP_ITEM:
				handleDropItem(player, packet);
				break;
			case PICKUP_ITEM:
				handlePickupItem(player, packet);
				break;
			case HANDLE_OPTIONS:
				handleOptions(player, packet);
				break;
			case PACKET_145:
				handleInterfaceAction1(player, packet);
				break;
			case PACKET_117:
				handleInterfaceAction2(player, packet);
				break;
			case PACKET_43:
				handleInterfaceAction3(player, packet);
				break;
			case PACKET_129:
				handleInterfaceAction4(player, packet);
				break;
			case EQUIP_ITEM:
				handleEquipItem(player, packet);
				break;
			case FIRST_CLICK_ITEM:
				handleFirstClickItem(player, packet);
				break;
			case THIRD_CLICK_ITEM:
				handleThirdClickItem(player, packet);
				break;
		}
	}

	/**
	 * TODO read in vals
	 * 
	 * THIS IS NOT ITEM OPERATE -Aki (Item operate is interfaceActionMenuSlot3)
	 * 
	 * @param player
	 * @param packet
	 */
	private void handleAlternativeItemOption(Player player, Packet packet) {
		/*
		int interfaceId = packet.getIn().readShort(true, ValueType.A, ByteOrder.LITTLE);
		int unknown = packet.getIn().readShort(true, ValueType.A); Item Id, only use slot.
		*/
	}

	/**
	 * Working Clienth4x fix - <Ares>
	 * 
	 * @param player
	 * @param packet
	 */
	private void handleDropItem(Player player, Packet packet) {
		packet.getIn().readShort(ValueType.A);
		packet.getIn().readShort();

		int itemSlot = packet.getIn().readShort(ValueType.A);
		Item item = player.getInventory().get(itemSlot);

		if (item == null) {
			return;
		}

		// Check for pre-existing stackable items on current tile (check only for visible items?)
		if (item.getDefinition().isStackable()) {
			for (GroundItem other : player.getRegion().getTile(player.getPosition()).getGroundItems()) {
				if (other.getPos().getX() == player.getPosition().getX() && other.getPos().getY() == player.getPosition().getY() && other.getItem().getId() == item.getId()) {
					if (!other.isRespawn() && (other.isGlobal() || (other.getOwner() != null && other.getOwner().equals(player.getUsername())))) {
						int allowed_amt = Integer.MAX_VALUE - other.getItem().getCount();
						if (allowed_amt <= 0) {
							player.stopAllActions(true);
							player.getActionSender().sendMessage("The floor doesn't have enough space to hold that item.");
							return;
						}
						if (allowed_amt > item.getCount()) {
							allowed_amt = item.getCount();
						}
						other.getItem().setCount(other.getItem().getCount() + allowed_amt);
						player.stopAllActions(true);
						player.getInventory().removeItem(new Item(item.getId(), allowed_amt));
						return;
					}
				}
			}
		}

		switch (item.getId()) {
			case 4045: //Explosive Potion
				Explosive.explode(player);
				break;
			default:
				ItemManager.getInstance().createGroundItem(player, player.getUsername(), new Item(item.getId(), item.getCount()), new Position(player.getPosition().getX(), player.getPosition().getY(), player.getPosition().getZ()), ItemManager.getInstance().isUntradeable(item.getId()) ? Constants.GROUND_START_TIME_UNTRADEABLE : Constants.GROUND_START_TIME);
				DropLog.recordDrop(player, item, "Drop");
				break;
		}

		player.stopAllActions(true);
		player.getInventory().removeItemSlot(itemSlot);
	}

	/**
	 * Uses an item on an object. Most specific itemOnObjects are in WalkToActions.java
	 * @param player
	 * @param packet
	 */
	private void useItemOnItem(Player player, Packet packet) {
		int firstSlot = packet.getIn().readShort();
		int secondSlot = packet.getIn().readShort(StreamBuffer.ValueType.A);
		Item usedWith = player.getInventory().get(firstSlot); //Item the player clicks on to use itemUsed with
		Item itemUsed = player.getInventory().get(secondSlot); //Item the player clicks to Use
		/**
		 * Example
		 * Player right clicks (or left clicks if left click is set to 'Use') and selects 'Use', making this item itemUsed
		 * Player then left clicks (with itemUsed still selected with a white border) on another item, making this other item usedWith
		 */

		if (usedWith == null || itemUsed == null) {
			return;
		}

		//Pineapple Pizza
		if (usedWith.getId() == 2118 || itemUsed.getId() == 2118) {
			if (itemUsed.getId() == 2118 && usedWith.getId() == 2289) {
				player.getInventory().removeItem(new Item(2118));
				player.getInventory().removeItem(new Item(2289));
				player.getInventory().addItem(new Item(2301));
			} else if (itemUsed.getId() == 2289 && usedWith.getId() == 2118) {
				player.getInventory().removeItem(new Item(2118));
				player.getInventory().removeItem(new Item(2289));
				player.getInventory().addItem(new Item(2301));
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}
		//Meat Pizza
		if (usedWith.getId() == 2142 || itemUsed.getId() == 2142) {
			if (itemUsed.getId() == 2142 && usedWith.getId() == 2289) {
				player.getInventory().removeItem(new Item(2142));
				player.getInventory().removeItem(new Item(2289));
				player.getInventory().addItem(new Item(2293));
			} else if (itemUsed.getId() == 2289 && usedWith.getId() == 2142) {
				player.getInventory().removeItem(new Item(2142));
				player.getInventory().removeItem(new Item(2289));
				player.getInventory().addItem(new Item(2293));
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}
		//Anchovy Pizza
		if (usedWith.getId() == 319 || itemUsed.getId() == 319) {
			if (itemUsed.getId() == 319 && usedWith.getId() == 2289) {
				player.getInventory().removeItem(new Item(319));
				player.getInventory().removeItem(new Item(2289));
				player.getInventory().addItem(new Item(2297));
			} else if (itemUsed.getId() == 2289 && usedWith.getId() == 319) {
				player.getInventory().removeItem(new Item(319));
				player.getInventory().removeItem(new Item(2289));
				player.getInventory().addItem(new Item(2297));
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}

		//Firemaking
		if (usedWith.getId() == 590 || itemUsed.getId() == 590) {
			int logId = usedWith.getId() == 590 ? itemUsed.getId() : usedWith.getId();
			LogType logType = LogType.forId(logId);
			if (logType == null) {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			} else {
				player.getActionDeque().addAction(new FireCraft(player, logType, 0));
			}
		}
		//TODO: Cookable items on Fires (plays different anim: 897)

		//Molten Glass on Glassblowing Pipe
		if (usedWith.getId() == 1785 || itemUsed.getId() == 1785) {
			if (usedWith.getId() == 1785 && itemUsed.getId() == 1775) {
				player.getActionSender().sendInterface(11462);
			} else if (itemUsed.getId() == 1785 && usedWith.getId() == 1775) {
				player.getActionSender().sendInterface(11462);
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}

		//Needle on Leather
		if (usedWith.getId() == 1733 || itemUsed.getId() == 1733) {
			Hide hide;
			if (usedWith.getId() == 1733) {
				hide = Hide.forReward(itemUsed.getId());
			} else {
				hide = Hide.forReward(usedWith.getId());
			}
			player.getActionSender().removeInterfaces();
			if (hide != null) {
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Working with Hide: [ID: <col=CC0000>" + hide.getItemId() + "</col>, Name: <col=CC0000>" + ItemManager.getInstance().getItemName(hide.getItemId()) + "</col>]");
				}
				logger.info("Working with Hide: [ID: " + hide.getItemId() + ", Name: " + ItemManager.getInstance().getItemName(hide.getItemId()) + "]");
				player.setAttribute("craftingType", CraftingType.ARMOUR_CREATION);
				player.setAttribute("craftingHide", hide);
				if (hide.getItemId() == 1739) { //Soft Leather or Hard Leather
					player.getActionSender().sendInterface(2311);
				} else if (hide.getItemId() == 6287 || hide.getItemId() == 7801) { //Snakeskin (Snake Hide or Snake Hide (Swamp))
					player.getActionSender().sendItemOnInterface(8941, 170, hide.getCraftableOutcomes()[0]);
					player.getActionSender().sendItemOnInterface(8942, 170, hide.getCraftableOutcomes()[1]);
					player.getActionSender().sendItemOnInterface(8943, 170, hide.getCraftableOutcomes()[2]);
					player.getActionSender().sendItemOnInterface(8944, 170, hide.getCraftableOutcomes()[3]);
					player.getActionSender().sendItemOnInterface(8945, 170, hide.getCraftableOutcomes()[4]);
					player.getActionSender().moveComponent(0, -10, 8941);
					player.getActionSender().moveComponent(0, -10, 8942);
					player.getActionSender().moveComponent(0, -10, 8943);
					player.getActionSender().moveComponent(0, -10, 8944);
					player.getActionSender().moveComponent(0, -10, 8945);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat("Body"), 8949);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat("Chaps"), 8953);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat("Bandana"), 8957);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat("Boots"), 8961);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat("V'brace"), 8965);
					player.getActionSender().sendChatInterface(8938);
				} else {
					String prefix = ItemManager.getInstance().getItemName(hide.getOutcome()).split(" ")[0];
					player.getActionSender().sendItemOnInterface(8883, 170, hide.getCraftableOutcomes()[0]);
					player.getActionSender().sendItemOnInterface(8884, 170, hide.getCraftableOutcomes()[1]);
					player.getActionSender().sendItemOnInterface(8885, 170, hide.getCraftableOutcomes()[2]);
					player.getActionSender().moveComponent(0, -10, 8883);
					player.getActionSender().moveComponent(0, -10, 8884);
					player.getActionSender().moveComponent(0, -10, 8885);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " body"), 8889);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " vambraces"), 8893);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " chaps"), 8897);
					player.getActionSender().sendChatInterface(8880);
				}
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}

		//Chisel on Gem
		if (usedWith.getId() == 1755 || itemUsed.getId() == 1755) {
			Gem gem;
			if (usedWith.getId() == 1755) {
				gem = Gem.forId(itemUsed.getId());
			} else {
				gem = Gem.forId(usedWith.getId());
			}
			if (gem != null) {
				player.setAttribute("craftingType", CraftingType.GEM_CRAFTING);
				player.setAttribute("craftingGem", gem);
				player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(gem.getCutGem()), 2799);
				player.getActionSender().moveComponent(0, -5, 1746);
				player.getActionSender().sendItemOnInterface(1746, 175, gem.getCutGem());
				player.getActionSender().sendChatInterface(4429);
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}

		//Arrow Shafts to Headless Arrows
		if (usedWith.getId() == 52 || itemUsed.getId() == 52) {
			int lowestAmount = 0;
			if (player.getInventory().getItemContainer().getCount(52) < player.getInventory().getItemContainer().getCount(314)) {
				lowestAmount = player.getInventory().getItemContainer().getCount(52);
			} else {
				lowestAmount = player.getInventory().getItemContainer().getCount(314);
			}
			if ((usedWith.getId() == 52 && itemUsed.getId() == 314) || (usedWith.getId() == 314 && itemUsed.getId() == 52)) {
				player.setAttribute("creationAmount", lowestAmount);
				player.setAttribute("fletchingType", CreationType.HEADLESS_ARROW_CREATION);
				player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(53), 2799);
				player.getActionSender().moveComponent(0, -15, 1746);
				player.getActionSender().sendItemOnInterface(1746, 175, 53);
				player.getActionSender().sendChatInterface(4429);
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}
		//Headless Arrows to Arrows
		if (usedWith.getId() == 53 || itemUsed.getId() == 53) {
			int lowestAmount = 0;
			ArrowTips arrowtip;
			if (itemUsed.getId() == 53) {
				arrowtip = ArrowTips.forId(usedWith.getId());
			} else {
				arrowtip = ArrowTips.forId(itemUsed.getId());
			}
			if (arrowtip != null) {
				if (player.getInventory().getItemContainer().getCount(53) < player.getInventory().getItemContainer().getCount(arrowtip.getItemId())) {
					lowestAmount = player.getInventory().getItemContainer().getCount(53);
				} else {
					lowestAmount = player.getInventory().getItemContainer().getCount(arrowtip.getItemId());
				}
				player.setAttribute("arrowTip", arrowtip);
				player.setAttribute("creationAmount", lowestAmount);
				player.setAttribute("fletchingType", CreationType.ARROW_CREATION);
				player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(arrowtip.getReward()), 2799);
				player.getActionSender().moveComponent(0, -15, 1746);
				player.getActionSender().sendItemOnInterface(1746, 175, arrowtip.getReward());
				player.getActionSender().sendChatInterface(4429);
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}
		//Using Knife on Logs
		if (usedWith.getId() == 946 || itemUsed.getId() == 946) {
			FletchableLogs log;
			if (itemUsed.getId() == 946) {
				log = FletchableLogs.forId(usedWith.getId());
			} else {
				log = FletchableLogs.forId(itemUsed.getId());
			}
			if (log != null) {
				player.setAttribute("fletchingType", CreationType.UNSTRUNG_BOW_CREATION);
				player.setAttribute("fletchingLog", log);
				if (log.getLogId() == 1511) { //Regular Logs
					player.getActionSender().sendItemOnInterface(8902, 170, log.getRewards()[0]);
					player.getActionSender().sendItemOnInterface(8903, 170, log.getRewards()[1]);
					player.getActionSender().sendItemOnInterface(8904, 170, log.getRewards()[2]);
					player.getActionSender().sendItemOnInterface(8905, 170, log.getRewards()[3]);
					player.getActionSender().moveComponent(0, -10, 8902);
					player.getActionSender().moveComponent(0, -10, 8903);
					player.getActionSender().moveComponent(0, -10, 8904);
					player.getActionSender().moveComponent(0, -10, 8905);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat("15 Arrow Shafts"), 8909);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat("Shortbow (u)"), 8913);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat("Longbow (u)"), 8917);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat("Wooden Stock"), 8921);
					player.getActionSender().sendChatInterface(8899);
				} else if (log.getLogId() == 1513) { //Magic Logs
					String prefix = ItemManager.getInstance().getItemName(log.getRewards()[0]).split(" ")[0];
					player.getActionSender().sendItemOnInterface(8869, 170, log.getRewards()[0]);
					player.getActionSender().sendItemOnInterface(8870, 170, log.getRewards()[1]);
					player.getActionSender().moveComponent(0, -10, 8869);
					player.getActionSender().moveComponent(0, -10, 8870);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " Shortbow (u)"), 8874);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " Longbow (u)"), 8878);
					player.getActionSender().sendChatInterface(8866);
				} else if (log.getLogId() == 6333 || log.getLogId() == 6332) { //Teak or Mahogany Logs
					String prefix = ItemManager.getInstance().getItemName(log.getRewards()[0]).split(" ")[0];
					player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " Stock"), 2799);
					player.getActionSender().moveComponent(0, -10, 1746);
					player.getActionSender().sendItemOnInterface(1746, 170, log.getRewards()[0]);
					player.getActionSender().sendChatInterface(4429);
				} else {
					String prefix = ItemManager.getInstance().getItemName(log.getRewards()[0]).split(" ")[0];
					player.getActionSender().sendItemOnInterface(8883, 170, log.getRewards()[0]);
					player.getActionSender().sendItemOnInterface(8884, 170, log.getRewards()[1]);
					player.getActionSender().sendItemOnInterface(8885, 170, log.getRewards()[2]);
					player.getActionSender().moveComponent(0, -10, 8883);
					player.getActionSender().moveComponent(0, -10, 8884);
					player.getActionSender().moveComponent(0, -10, 8885);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " Shortbow (u)"), 8889);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " Longbow (u)"), 8893);
					player.getActionSender().sendString("\\n \\n \\n \\n".concat(prefix + " Stock"), 8897);
					player.getActionSender().sendChatInterface(8880);
				}
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}
		//Stringing Shortbows and Longbows
		if (usedWith.getId() == 1777 || itemUsed.getId() == 1777) {
			UnstrungBows unstrungBow;
			if (usedWith.getId() == 1777) {
				unstrungBow = UnstrungBows.forId(itemUsed.getId());
			} else {
				unstrungBow = UnstrungBows.forId(usedWith.getId());
			}
			if (unstrungBow != null) {
				player.setAttribute("fletchingType", CreationType.BOW_CREATION);
				player.setAttribute("fletchingItem", unstrungBow);
				player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(unstrungBow.getProduct()), 2799);
				player.getActionSender().moveComponent(0, -18, 1746);
				player.getActionSender().sendItemOnInterface(1746, 175, unstrungBow.getProduct());
				player.getActionSender().sendChatInterface(4429);
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}

		//Unf. Potion Mixing
		if (usedWith.getId() == 227 || itemUsed.getId() == 227) {
			PrimaryIngredient primaryIngredient;
			if (usedWith.getId() == 227) {
				primaryIngredient = PrimaryIngredient.forId(itemUsed.getId());
			} else {
				primaryIngredient = PrimaryIngredient.forId(usedWith.getId());
			}
			if (primaryIngredient != null) {
				player.setAttribute("herbloreType", IngredientType.PRIMARY_INGREDIENT);
				player.setAttribute("herbloreIngredient", primaryIngredient);
				player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(primaryIngredient.getReward()), 2799);
				player.getActionSender().moveComponent(0, 6, 1746);
				player.getActionSender().sendItemOnInterface(1746, 150, primaryIngredient.getReward());
				player.getActionSender().sendChatInterface(4429);
			} else {
				player.getActionSender().sendMessage("Nothing interesting happens.");
			}
		}
		//Secondary Potion Mixing
		SecondaryIngredient ingredient = null;
		for (SecondaryIngredient secondaryIngredient : SecondaryIngredient.values()) {
			if ((secondaryIngredient.getId() == itemUsed.getId() && secondaryIngredient.getRequiredItem().getId() == usedWith.getId())
				|| (secondaryIngredient.getId() == usedWith.getId() && secondaryIngredient.getRequiredItem().getId() == itemUsed.getId())) {
				ingredient = secondaryIngredient;
			}
		}
		if (ingredient != null) {
			player.setAttribute("herbloreType", IngredientType.SECONDARY_INGREDIENT);
			player.setAttribute("herbloreIngredient", ingredient);
			player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(ingredient.getReward()), 2799);
			player.getActionSender().moveComponent(0, 6, 1746);
			player.getActionSender().sendItemOnInterface(1746, 150, ingredient.getReward());
			player.getActionSender().sendChatInterface(4429);
			return;
		}
	}

	/**
	 * NEEDS ALOT OF WORK - stop tick if no space - check if has space Write
	 * shorter & more efficient code
	 * 
	 * @param player
	 * @param packet
	 */
	private void handlePickupItem(final Player player, Packet packet) {
		player.setClickY(packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE));
		player.setClickId(packet.getIn().readShort());
		player.setClickX(packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE));

		if ((Boolean) player.getAttribute("disableItemPickup") != null) {
			return;
		}

		if (Misc.getDistance(new Position(player.getClickX(), player.getClickY()), player.getPosition()) >= 20) {
			player.getMovementHandler().reset();
			return;
		}

		//If we're top of the item...
		if (player.getPosition().getX() == player.getClickX() && player.getPosition().getY() == player.getClickY()) {
			ItemManager.getInstance().pickupItem(player, player.getClickId(), new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ()));
			//TODO: Fix it so it displays correct amount
			DropLog.recordDrop(player, new Item(player.getClickId()), "Pickup");
		} else {
			player.setWalkToAction(new Task(1, true) {
				@Override
				protected void execute() {
					//If Item has vanished, reset movement and stop.
					if (!ItemManager.getInstance().itemExists(player.getClickId(), new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ()))) {
						player.getMovementHandler().reset();
						this.stop();
						return;
					}
					//Wait until we are next to the item...
					if (!player.getMovementHandler().walkToAction(new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ()), 1)) {
						return;
					}
					World.submit(new Task(1) {
						@Override
						protected void execute() {
							//Finally, pick up the item!
							if (Misc.getDistance(player.getPosition(), new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ())) < 1) {
								ItemManager.getInstance().pickupItem(player, player.getClickId(), new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ()));
							}
							this.stop();
						}
					});
					if (player.getAttribute("pickedUpItem") != null) {
						player.removeAttribute("pickedUpItem");
						this.stop();
						return;
					}
				}
			});
		}
	}

	/**
	 * Handles the action when a player is swapping items' position in their inventory when an interface is open.
	 * 
	 * @param player
	 * @param packet
	 */
	private void handleOptions(Player player, Packet packet) {
		int interfaceId = packet.getIn().readShort(StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
		packet.getIn().readByte(StreamBuffer.ValueType.C);
		int fromSlot = packet.getIn().readShort(StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
		int toSlot = packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE);
		switch (interfaceId) {
			case 5382:
				BankManager.handleBankOptions(player, fromSlot, toSlot);
				break;
			case 3214:
				player.getInventory().swap(fromSlot, toSlot);
				break;
			default:
				logger.severe("Swap Items: " + interfaceId);
				break;
		}
	}

	/**
	 * Withdraw/Deposit 1/X
	 * 
	 * TODO: implement a method to determine the current container.
	 * TODO: Item item = null You need to specify from which container you get the item e.g getTrade().get(slot) e.g getInventory().get(slot)
	 * 
	 * @param player
	 * @param packet
	 */
	private void handleInterfaceAction1(Player player, Packet packet) {
		int interfaceID = packet.getIn().readShort(StreamBuffer.ValueType.A);
		int slot = packet.getIn().readShort(StreamBuffer.ValueType.A);
		int itemId = packet.getIn().readShort(StreamBuffer.ValueType.A);
		//packet.getIn().readShort(StreamBuffer.ValueType.A);
		Item item;

		if (player.isDebugging()) {
			player.getActionSender().sendMessage("Packet145 InterfaceID >> " + interfaceID);
		}

		Shop shop = player.getAttribute("shop");
		PlayerShop playerShop; //TODO

		switch (interfaceID) {
			case 6574:
				DuelArena.getInstance().offerItem(player, slot, 1);
				break;
			case 6669:
				DuelArena.getInstance().removeItem(player, slot, 1);
				break;
			case 1688:
				player.getEquipment().unequip(slot);
				break;
			case 2274:
					item = Server.getPartyRoom().chestDeposit.get(slot);
					if (item != null) {
						Server.getPartyRoom().removeDepositItem(player, slot, item.getId(), 1);
					}
				break;
			case 3322:
				item = player.getInventory().get(slot);
				if (item != null) {
					if (player.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT)) {
						player.getDicing().betItem(player, slot, item.getId(), 1);
					} else if (player.getTradeStage().equals(TradeStage.SEND_REQUEST_ACCEPT)){
						player.getTrading().offerItem(player, slot, item.getId(), 1);
					} else {
						Server.getPartyRoom().depositItem(player, slot, item.getId(), 1);
					}
				}
				break;
			case 3415:
				if (player.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT)) {
					item = player.getDice().get(slot);
					if (item != null) {
						player.getDicing().removeBetItem(player, slot, item.getId(), 1);
					}
				} else {
					item = player.getTrade().get(slot);
					if (item != null) {
						player.getTrading().removeTradeItem(player, slot, item.getId(), 1);
					}
				}
				break;
			case 3823:
				// ShopManager.getSellValue(player, item.getId());
				if (shop == null) {
					player.getActionSender().sendMessage("Shop (#1) is null.");
					return;
				}
				Item s_item = player.getInventory().get(slot);
				shop.value(player, s_item, true, true);
				break;
			case 3900:
				// ShopManager.getBuyValue(player, item.getId());
				// item = player.getInventory().get(slot);
				if (shop == null) {
					player.getActionSender().sendMessage("Shop (#1) is null.");
					return;
				}
				Item b_item = shop.getCurrentStock().get(slot);
				shop.value(player, b_item, true, false);
				break;
			case 5064:
				item = player.getInventory().get(slot);
				if (item != null) {
					BankManager.bankItem(player, slot, item.getId(), 1);
				}
				break;
			case 5382:
				item = player.getBank().get(slot);
				if (item != null) {
					BankManager.withdrawItem(player, slot, item.getId(), 1);
				}
				break;
			//Begin Smithing
			case 1119:
			case 1120:
			case 1121:
			case 1122:
			case 1123:
				Product smithItem = Product.forId(itemId);
				player.getActionDeque().addAction(new Forging(player, smithItem, 1));
				player.getActionSender().removeInterfaces();
				break;
			//End Smithing
			default:
				logger.severe("Unhandled Interface Action #1 Interface ID: " + interfaceID + ".");
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Interface Action #1 Interface ID: <col=CC0000>" + interfaceID + "</col>.");
				}
				break;
		}
	}

	/**
	 * Withdraw/Deposit 5
	 * 
	 * TODO: implement a method to determine the current container.
	 * 
	 * @param player
	 * @param packet
	 */
	private void handleInterfaceAction2(Player player, Packet packet) {
		int interfaceID = packet.getIn().readShort(true, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
		int itemId = packet.getIn().readShort(true, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
		int slot = packet.getIn().readShort(true, StreamBuffer.ByteOrder.LITTLE);
		System.out.println(itemId);
		Item item;
		Shop shop = player.getAttribute("shop");

		switch (interfaceID) {
			//Begin Smithing
			case 1119:
			case 1120:
			case 1121:
			case 1122:
			case 1123:
				Product smithItem = Product.forId(itemId);
				player.getActionDeque().addAction(new Forging(player, smithItem, 5));
				player.getActionSender().removeInterfaces();
				break;
			//End Smithing
			case 6574:
				item = player.getInventory().get(slot);
				if (item != null) {
					DuelArena.getInstance().offerItem(player, slot, 5);
				}
				break;
			case 6669:
				DuelArena.getInstance().removeItem(player, slot, 5);
				break;
			case 2274:
				item = Server.getPartyRoom().chestDeposit.get(slot);
				if (item != null) {
					Server.getPartyRoom().removeDepositItem(player, slot, item.getId(), 5);
				}				
				break;
			case 3322:
				item = player.getInventory().get(slot);
				if (item != null) {
					if (player.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT)) {
						player.getDicing().betItem(player, slot, item.getId(), 5);
					} else if (player.getTradeStage().equals(TradeStage.SEND_REQUEST_ACCEPT)) {
						player.getTrading().offerItem(player, slot, item.getId(), 5);
					} else {
						Server.getPartyRoom().depositItem(player, slot, item.getId(), 5);
					}
				}
				break;
			case 3415:
				if (player.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT)) {
					item = player.getDice().get(slot);
					if (item != null) {
						player.getDicing().removeBetItem(player, slot, item.getId(), 5);
					}
				} else {
					item = player.getTrade().get(slot);
					if (item != null) {
						player.getTrading().removeTradeItem(player, slot, item.getId(), 5);
					}
				}
				break;
			case 3823:
				// ShopManager.sellItem(player, slot, item.getId(), 1);
				if (shop == null) {
					player.getActionSender().sendMessage("Shop (#2) is null.");
					return;
				}
				shop.sell(player, slot, 1);
				break;
			case 3900:
				// ShopManager.buyItem(player, slot, item.getId(), 1);
				if (shop == null) {
					player.getActionSender().sendMessage("Shop (#2) is null.");
					return;
				}
				shop.buy(player, slot, 1);
				break;
			case 5064:
				item = player.getInventory().get(slot);
				if (item != null) {
					BankManager.bankItem(player, slot, item.getId(), 5);
				}
				break;
			case 5382:
				item = player.getBank().get(slot);
				if (item != null) {
					BankManager.withdrawItem(player, slot, item.getId(), 5);
				}
				break;
			default:
				logger.severe("Unhandled Interface Action #2 Interface ID: " + interfaceID + ".");
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Interface Action #2 Interface ID: <col=CC0000>" + interfaceID + "</col>.");
				}
				break;
		}
	}

	/**
	 * Withdraw/Deposit 10
	 * Also seems to be operate for dragon fire shield once equipped.
	 * 
	 * TODO: implement a method to determine the current container.
	 * 
	 * @param player
	 * @param packet
	 */
	private void handleInterfaceAction3(Player player, Packet packet) {
		int interfaceID = packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE);
		int itemId = packet.getIn().readShort(StreamBuffer.ValueType.A);
		int slot = packet.getIn().readShort(StreamBuffer.ValueType.A);
		Item item;
		Shop shop = player.getAttribute("shop");

		switch (interfaceID) {
			//Begin Smithing
			case 1119:
			case 1120:
			case 1121:
			case 1122:
			case 1123:
				Product smithItem = Product.forId(itemId);
				player.getActionDeque().addAction(new Forging(player, smithItem, 10));
				player.getActionSender().removeInterfaces();
				break;
			//End Smithing
			case 6574:
				DuelArena.getInstance().offerItem(player, slot, 10);
				break;
			case 6669:
				DuelArena.getInstance().removeItem(player, slot, 10);
				break;
			case 1688:
				item = player.getEquipment().get(slot);
				if (item == null) {
					return;
				}
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("[<col=CC0000>DEBUG</col>] Item Operate >> " + item);
				}
				player.getActionSender().sendMessage("This item cannot be operated.");
				System.out.println("Unhandled Item Operation ID: " + item.getId() + ".");
				break;
			case 2274:
				item = Server.getPartyRoom().chestDeposit.get(slot);
				if (item != null) {
					Server.getPartyRoom().removeDepositItem(player, slot, item.getId(), 10);
				}
				break;
			case 3322:
				item = player.getInventory().get(slot);
				if (item != null) {
					if (player.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT)) {
						player.getDicing().betItem(player, slot, item.getId(), 10);
					} else if (player.getTradeStage().equals(TradeStage.SEND_REQUEST_ACCEPT)) {
						player.getTrading().offerItem(player, slot, item.getId(), 10);
					} else {
						Server.getPartyRoom().depositItem(player, slot, item.getId(), 10);
					}
				}
				break;
			case 3415:
				if (player.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT)) {
					item = player.getDice().get(slot);
					if (item != null) {
						player.getDicing().removeBetItem(player, slot, item.getId(), 10);
					}
				} else {
					item = player.getTrade().get(slot);
					if (item != null) {
						player.getTrading().removeTradeItem(player, slot, item.getId(), 10);
					}
				}
				break;
			case 3823:
				// ShopManager.sellItem(player, slot, item.getId(), 5);
				if (shop == null) {
					player.getActionSender().sendMessage("Shop (#3) is null.");
					return;
				}
				shop.sell(player, slot, 5);
				break;
			case 3900:
				// ShopManager.buyItem(player, slot, item.getId(), 5);
				if (shop == null) {
					player.getActionSender().sendMessage("Shop (#3) is null.");
					return;
				}
				shop.buy(player, slot, 5);
				break;
			case 5064:
				item = player.getInventory().get(slot);
				if (item != null) {
					BankManager.bankItem(player, slot, item.getId(), 10);
				}
				break;
			case 5382:
				item = player.getBank().get(slot);
				if (item != null) {
					BankManager.withdrawItem(player, slot, item.getId(), 10);
				}
				break;
			default:
				logger.severe("Unhandled Interface Action #3 Interface ID: " + interfaceID + ".");
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Interface Action #3 Interface ID: <col=CC0000>" + interfaceID + "</col>.");
				}
				break;
		}
	}

	/**
	 * Withdraw/Deposit All
	 * 
	 * TODO: implement a method to determine the current container.
	 * 
	 * @param player
	 * @param packet
	 */
	private void handleInterfaceAction4(Player player, Packet packet) {
		int slot = packet.getIn().readShort(StreamBuffer.ValueType.A);
		int interfaceID = packet.getIn().readShort();
		packet.getIn().readShort(StreamBuffer.ValueType.A);
		Item item;
		Shop shop = player.getAttribute("shop");

		switch (interfaceID) {
			case 6574:
				item = player.getInventory().get(slot);
				if (item != null) {
					DuelArena.getInstance().offerItem(player, slot, item.getDefinition().isStackable() ? item.getCount() : 28);
				}
				break;
			case 6669:
				item = player.getDuelInventory().get(slot);
				if (item != null) {
					DuelArena.getInstance().removeItem(player, slot, item.getDefinition().isStackable() ? item.getCount() : 28);
				}
				break;
			case 2274:
				item = Server.getPartyRoom().chestDeposit.get(slot);
				if (item != null) {
					Server.getPartyRoom().removeDepositItem(player, slot, item.getId(), item.getDefinition().isStackable() ? item.getCount() : 28);
				}
				break;
			case 3322:
				item = player.getInventory().get(slot);
				if (item != null) {
					if (player.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT)) {
						player.getDicing().betItem(player, slot, item.getId(), item.getDefinition().isStackable() ? item.getCount() : 28);
					} else if (player.getTradeStage().equals(TradeStage.SEND_REQUEST_ACCEPT)) {
						player.getTrading().offerItem(player, slot, item.getId(), item.getDefinition().isStackable() ? item.getCount() : 28);
					} else {
						Server.getPartyRoom().depositItem(player, slot, item.getId(), item.getDefinition().isStackable() ? item.getCount() : 28);
					}
				}
				break;
			case 3415:
				if (player.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT)) {
					item = player.getDice().get(slot);
					if (item == null) {
						return;
					}
					int amount = item.getDefinition().isStackable() ? item.getCount() : 28;
					player.getDicing().removeBetItem(player, slot, item.getId(), amount);
				} else {
					item = player.getTrade().get(slot);
					if (item == null) {
						return;
					}
					int amount = item.getDefinition().isStackable() ? item.getCount() : 28;
					player.getTrading().removeTradeItem(player, slot, item.getId(), amount);
				}
				break;
			case 3823:
				// ShopManager.sellItem(player, slot, itemId, 10);
				if (shop == null) {
					player.getActionSender().sendMessage("Shop (#4) is null.");
					return;
				}
				shop.sell(player, slot, 10);
				break;
			case 3900:
				// ShopManager.buyItem(player, slot, itemId, 10);
				if (shop == null) {
					player.getActionSender().sendMessage("Shop (#4) is null.");
					return;
				}
				shop.buy(player, slot, 10);
				break;
			case 5064:
				item = player.getInventory().get(slot);
				if (item == null) {
					return;
				}
				int amount2 = item.getDefinition().isStackable() ? item.getCount() : 28;
				BankManager.bankItem(player, slot, item.getId(), amount2);
				break;
			case 5382:
				item = player.getBank().get(slot);
				if (item == null) {
					return;
				}
				int amount1 = 0;
				if (player.getAttribute("BANK_MODE_NOTE") != null) {
					amount1 = item.getCount();
				} else {
					amount1 = item.getDefinition().isStackable() ? item.getCount() : 28;
				}
				BankManager.withdrawItem(player, slot, item.getId(), amount1);
				break;
			default:
				logger.severe("Unhandled Interface Action #4 Interface ID: " + interfaceID + ".");
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Interface Action #4 Interface ID: <col=CC0000>" + interfaceID + "</col>.");
				}
				break;
		}
	}

	/**
	 * @param player The player involved in the interaction.
	 * @param packet The packet representation.
	 * 
	 * TODO:
	 * BY AKZU: Should probably have check for ids for the actions like I do with food items, if its food then do only
	 * food actions. Like in the buttons class we should not have getClass().performButtons() but a list of cases what buttons
	 * performs those actions (annoying.), other way around would be making sure only the certain buttons are used and we use
	 * default:return;
	 */
	@SuppressWarnings("rawtypes")
	private void handleFirstClickItem(Player player, Packet packet) {
		packet.getIn().readShort(StreamBuffer.ValueType.A);
		int slot = packet.getIn().readShort(StreamBuffer.ValueType.A);
		packet.getIn().readShort(StreamBuffer.ByteOrder.LITTLE);
		Item item = player.getInventory().get(slot);
		if (item == null) {
			return;
		}

		switch (item.getId()) {
			//Clue Scrolls - Maps
			case 10180: 
			case 10182: 
			case 10184: 
			case 10186: 
			case 10188: 
			case 10190: 
			case 10194: 
			case 10202: 
			case 10204: 
			case 10208: {
				int scrollId = item.getId();
				Clue clue = null;

				if (Map.forId(scrollId) != null) {
					Map map = Map.forId(scrollId);
					player.setAttribute("clue_map", map.getScrollId());
					clue = new Clue(map.getDifficulty(), Type.MAPS);
				} else {
					player.getActionSender().sendMessage("A Map was not found for this Clue Scroll (ID: <col=CC0000>" + item.getId() + "</col>)!");
				}
				if (clue != null) {
					clue.read(player);
				}
				break;
			}
			//Clue Scrolls - Riddles
			case 2667: {
				int scrollId = item.getId();
				Clue clue = null;

				if (Riddle.forId(scrollId) != null) {
					Riddle riddle = Riddle.forId(scrollId);
					player.setAttribute("clue_riddle", riddle.getScrollId());
					clue = new Clue(riddle.getDifficulty(), Type.RIDDLE);
				} else {
					player.getActionSender().sendMessage("A Riddle was not found for this Clue Scroll (ID: <col=CC0000>" + item.getId() + "</col>)!");
				}
				if (clue != null) {
					clue.read(player);
				}
				break;
			}
			case 4155: //Enchanted Gem
				if (player.getSlayerTask() != 0) {
					player.getActionSender().sendMessage(player.getSlayer().checkTask());
				} else {
					player.getActionSender().sendMessage("You have no task!");
				}
				break;
			case 4049: //Bandages
				if (player.getInventory().removeItem(new Item(4049))) {
					player.getSkill().increaseLevelOnce(3, 5);
				}
				break;
			case 292:  {//Book on Baxtorian
				ReadingSession session = new Baxtorian(player);
				player.read(session.evaluate());
				player.getActionSender().sendMessage("The old book is missing many pages, and you can faintly make out what it says...");
				break;
			}
			case 784: {//Getting started
				ReadingSession session = new GettingStarted(player);
				player.read(session.evaluate());
				break;
			}
			case 526: //Regular Bones
			case 528: //Burnt Bones
			case 530: //Bat Bones
			case 532: //Big Bones
			case 534: //Babydragon Bones
			case 536: //Dragon Bones
			case 2859: //Wolf Bones
			case 3123: //Shaikahan Bones
			case 3125: //Jogre Bones
			case 3179: //Monkey Bones
			case 4812: //Zogre Bones
			case 4830: //Fayrg Bones
			case 4832: //Raurg Bones
			case 4834: //Ourg Bones
			case 6729: //Dagannoth Bones
			case 6812: 
				player.getActionSender().sendMessage("You dig a hole in the ground...");
				player.getActionDeque().addAction(new BurialAction(player, BoneType.forId(item.getId())));
				break;
			case 952: //Spade
				player.getUpdateFlags().sendAnimation(831);
				World.submit(new Dig(player));
				break;
			case 550: //Newcomer Map
				player.getActionSender().sendInterface(5392);
				player.getActionSender().sendMessage("You unfold the map and take a look at its contents.");
				break;
			case 6885:   //Dull (0 < Pizazz Points < 900)
			case 6886:   //Energized (9000 <= Pizazz Points < 16000)
			case 6887: { //Bright (16000 <= Pizazz Points)
				DialogueSession session = new ProgressHat(player, null);
				player.open(session.evaluate());
				break;
			}
			case 2520:   //Brown Toy Horsey
			case 2522:   //White Toy Horsey
			case 2524:   //Black Toy Horsey
			case 2526: { //Grey Toy Horsey
				String[] sayings = { "Come on Dobbin, we can win the race!", "Hi-ho Silver, and away!", "Giddy-up horsey!" };
				String message = Misc.generatedString(sayings);
				player.getUpdateFlags().sendForceMessage(message);
				if (item.getId() == 2520) {
					player.getUpdateFlags().sendAnimation(918);
				} else if (item.getId() == 2522) {
					player.getUpdateFlags().sendAnimation(919);
				} else if (item.getId() == 2524) {
					player.getUpdateFlags().sendAnimation(920);
				} else if (item.getId() == 2526) {
					player.getUpdateFlags().sendAnimation(921);
				}
				break;
			}
			default:
				player.getActionSender().sendMessage("Nothing interesting happens.");
				logger.severe("Unhandled First Click Item: [ID: " + item.getId() + ", Amount: " + item.getCount() + "].");
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled First Click Item: [ID: <col=CC0000>" + item.getId() + "</col>, Amount:" + item.getCount() + "</col>].");
				}
				break;
		}

		if (FoodLoader.getFood(item.getId()) != null) {
			Food.eatFood(player, item, slot);
			return;
		}

		if (PotionLoader.getPotion(item.getId()) != null) {
			Potion.drinkPotion(player, item, slot);
			return;
		}

		TeleportTabs.getInstance().activateTeleportTab(player, item.getId(), slot);

		HerbType herb = HerbType.forId(item.getId());

		if (herb != null) {
			HerbIdentifier identifier = new HerbIdentifier(player, herb);
			identifier.identifyHerb();
		}
	}

	/**
	 * Working Clienth4x fix <Ares>
	 * 
	 * @param player
	 * @param packet
	 */
	private void handleThirdClickItem(Player player, Packet packet) {
		packet.getIn().readShort(StreamBuffer.ValueType.A);
		int slot = packet.getIn().readShort(true, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
		packet.getIn().readShort();
		Item item = player.getInventory().get(slot);
		if (item == null) {
			return;
		}

		switch (item.getId()) {
			case 11694: //Armadyl Godsword
			case 11696: //Bandos Godsword
			case 11698: //Saradomin Godsword
			case 11700: //Zamorak Godsword
				//TODO: Add dismantling and creation of Godswords
				player.getActionSender().sendMessage("You can't dismantle this weapon.");
				break;
			default:
				player.getActionSender().sendMessage("Nothing interesting happens.");
				logger.severe("Unhandled Third Click Item: [ID: " + item.getId() + ", Amount: " + item.getCount() + "].");
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Third Click Item: [ID: <col=CC0000>" + item.getId() + "</col>, Amount:" + item.getCount() + "</col>].");
				}
				break;
		}
	}

	//TODO: Make real handleThirdClickItem method, and rename ^^^^^ to handleSecondClickItem

	/**
	 * Working Clienth4x fix <Ares>
	 * 
	 * @param player
	 * @param packet
	 */
	private void handleEquipItem(Player player, Packet packet) {
		packet.getIn().readShort(); // do not use - clienth4x <Ares>
		int slot = packet.getIn().readShort(StreamBuffer.ValueType.A);
		packet.getIn().readShort(); // Interface ID.
		player.getEquipment().equip(slot);
	}
}
