package com.exorth.rs2.network.packet.packets;

import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.object.CustomObjectManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;

public class LoadRegionPacketHandler implements PacketHandler {

	public static final int LOAD_REGION = 121;

	@Override
	public void handlePacket(final Player player, Packet packet) {
		ItemManager.getInstance().loadOnRegion(player);
		CustomObjectManager.createGlobalObject(player);
	}
}
