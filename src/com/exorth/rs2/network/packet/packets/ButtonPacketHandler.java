package com.exorth.rs2.network.packet.packets;

import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.exorth.rs2.Constants;
import com.exorth.rs2.Server;
import com.exorth.rs2.content.EmoteActions;
import com.exorth.rs2.content.SkillMenus;
import com.exorth.rs2.content.SkillMenus.MenuData;
import com.exorth.rs2.content.Skillcapes;
import com.exorth.rs2.content.combat.magic.Magic;
import com.exorth.rs2.content.combat.ranged.RangedDataLoader;
import com.exorth.rs2.content.combat.util.CombatState.CombatStyle;
import com.exorth.rs2.content.combat.util.Prayer;
import com.exorth.rs2.content.combat.util.SpecialAttack;
import com.exorth.rs2.content.duel.DuelArena;
import com.exorth.rs2.content.gambling.slots.SlotMachine;
import com.exorth.rs2.content.quest.Quest;
import com.exorth.rs2.content.quest.QuestRepository;
import com.exorth.rs2.content.skills.agility.TicketExchange;
import com.exorth.rs2.content.skills.agility.TicketExchange.Ticket;
import com.exorth.rs2.content.skills.cooking.Cookable;
import com.exorth.rs2.content.skills.cooking.CookingAction;
import com.exorth.rs2.content.skills.crafting.ArmourCreation;
import com.exorth.rs2.content.skills.crafting.Craftable;
import com.exorth.rs2.content.skills.crafting.CraftingType;
import com.exorth.rs2.content.skills.crafting.Gem;
import com.exorth.rs2.content.skills.crafting.GemCutting;
import com.exorth.rs2.content.skills.crafting.Glass;
import com.exorth.rs2.content.skills.crafting.GlassBlowing;
import com.exorth.rs2.content.skills.crafting.Hide;
import com.exorth.rs2.content.skills.crafting.LeatherTanning;
import com.exorth.rs2.content.skills.fletching.ArrowCreation;
import com.exorth.rs2.content.skills.fletching.ArrowTips;
import com.exorth.rs2.content.skills.fletching.BowCreation;
import com.exorth.rs2.content.skills.fletching.CreationType;
import com.exorth.rs2.content.skills.fletching.FletchableLogs;
import com.exorth.rs2.content.skills.fletching.HeadlessArrowCreation;
import com.exorth.rs2.content.skills.fletching.LogFletching;
import com.exorth.rs2.content.skills.fletching.UnstrungBows;
import com.exorth.rs2.content.skills.herblore.Herbalism;
import com.exorth.rs2.content.skills.herblore.IngredientType;
import com.exorth.rs2.content.skills.herblore.PrimaryIngredient;
import com.exorth.rs2.content.skills.herblore.SecondaryIngredient;
import com.exorth.rs2.content.skills.magic.BoneConversion;
import com.exorth.rs2.content.skills.magic.Charge;
import com.exorth.rs2.content.skills.magic.CustomSpells;
import com.exorth.rs2.content.skills.magic.Spell;
import com.exorth.rs2.content.skills.magic.Teleother;
import com.exorth.rs2.content.skills.magic.Teleportation;
import com.exorth.rs2.content.skills.smithing.Bar;
import com.exorth.rs2.content.skills.smithing.Smelting;
import com.exorth.rs2.model.Entity.AttackTypes;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Player.DiceStage;
import com.exorth.rs2.model.players.Player.TeleotherStage;
import com.exorth.rs2.model.players.shops.ShopLoader;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;
import com.exorth.rs2.util.Misc;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class ButtonPacketHandler implements PacketHandler {

	public static final int BUTTON = 185;

	private static final Logger logger = Logger.getLogger(ButtonPacketHandler.class.getName());
	
	private SlotMachine slots = new SlotMachine();

	@Override
	public void handlePacket(Player player, Packet packet) {
		handleButton(player, packet.getIn().readShort());
	}

	private void handleButton(final Player player, int buttonId) {
		if (player.isDebugging()) {
			player.getActionSender().sendMessage("Button: <col=CC0000>" + buttonId + "</col>.");
		}

		Player otherPlayer;

		switch (buttonId) {
			case 2246: //Accept button party room.
				Server.getPartyRoom().submitItems(player);
				break;
			//Skill Menus
			case 8654: //Attack
			case 8657: //Strength
			case 8660: //Defence
			case 8655: //Hitpoints
			case 8663: //Ranged
			case 8666: //Prayer
			case 8669: //Magic
			case 8665: //Cooking
			case 8671: //Woodcutting
			case 8670: //Fletching
			case 8662: //Fishing
			case 8668: //Firemaking
			case 8667: //Crafting
			case 8659: //Smithing
			case 8656: //Mining
			case 8661: //Herblore
			case 8658: //Agility
			case 8664: //Thieving
			case 12162: //Slayer
			case 13928: //Farming
			case 8672: //Runecrafting
			case 6996: //Construction
				MenuData menu = MenuData.forId(buttonId);
				SkillMenus skillMenu = new SkillMenus(menu);
				skillMenu.constructMenu(player);
				break;
			//Cooking
			case 13720: //Cook 1
				player.getActionSender().removeInterfaces();
				Cookable cookItem = Cookable.forId(player.getCookingQueue());
				player.getActionDeque().addAction(new CookingAction(player, cookItem, 1));
				break;
			case 13719: //Cook 5
				player.getActionSender().removeInterfaces();
				cookItem = Cookable.forId(player.getCookingQueue());
				player.getActionDeque().addAction(new CookingAction(player, cookItem, 5));
				break;
			case 13718: //Cook X
				player.setEnterXInterfaceId(2401);
				player.getActionSender().sendEnterX();
				break;
			case 13717: //Cook All
				player.getActionSender().removeInterfaces();
				cookItem = Cookable.forId(player.getCookingQueue());
				player.getActionDeque().addAction(new CookingAction(player, cookItem, 28));
				break;
			//Slots
			case 13898:
				slots.playSlotsManual(player, slots);
				break;
			//Agility Ticket Exchange
			case 8387: //Agility Experience (1 Ticket)
			case 8389: //Agility Experience (10 Tickets)
			case 8390: //Agility Experience (25 Tickets)
			case 8391: //Agility Experience (100 Tickets)
			case 8392: //Agility Experience (1000 Tickets)
			case 8382: //Toadflax (3 Tickets)
			case 8393: //Snapdragon (10 Tickets)
			case 8381: //Pirate's Hook (800 Tickets)
				Ticket ticket = Ticket.forID(buttonId);
				TicketExchange.exchangeTickets(player, ticket);
				break;
			//Smithing: Smelting Bars
			//Start of Bronze
			case 3987:
				Bar bar = Bar.BRONZE;
				player.getActionDeque().addAction(new Smelting(player, bar, 1));
				player.getActionSender().removeInterfaces();
				break;
			case 3986:
				bar = Bar.BRONZE;
				player.getActionDeque().addAction(new Smelting(player, bar, 5));
				player.getActionSender().removeInterfaces();
				break;
			case 2807:
				bar = Bar.BRONZE;
				player.getActionDeque().addAction(new Smelting(player, bar, 10));
				player.getActionSender().removeInterfaces();
				break;
			case 2414:
				player.setCurrentBar(Bar.BRONZE);
				player.setEnterXInterfaceId(2400);
				player.getActionSender().sendEnterX();
				break;
			//End of Bronze
			//Start of Iron
			case 3991:
				bar = Bar.IRON;
				player.getActionDeque().addAction(new Smelting(player, bar, 1));
				player.getActionSender().removeInterfaces();
				break;
			case 3990:
				bar = Bar.IRON;
				player.getActionDeque().addAction(new Smelting(player, bar, 5));
				player.getActionSender().removeInterfaces();
				break;
			case 3989:
				bar = Bar.IRON;
				player.getActionDeque().addAction(new Smelting(player, bar, 10));
				player.getActionSender().removeInterfaces();
				break;
			case 3988:
				player.setCurrentBar(Bar.IRON);
				player.setEnterXInterfaceId(2400);
				player.getActionSender().sendEnterX();
				break;
			//End of Iron
			//Start of Silver
			case 3995:
				bar = Bar.SILVER;
				player.getActionDeque().addAction(new Smelting(player, bar, 1));
				player.getActionSender().removeInterfaces();
				break;
			case 3994:
				bar = Bar.SILVER;
				player.getActionDeque().addAction(new Smelting(player, bar, 5));
				player.getActionSender().removeInterfaces();
				break;
			case 3993:
				bar = Bar.SILVER;
				player.getActionDeque().addAction(new Smelting(player, bar, 10));
				player.getActionSender().removeInterfaces();
				break;
			case 3992:
				player.setCurrentBar(Bar.SILVER);
				player.setEnterXInterfaceId(2400);
				player.getActionSender().sendEnterX();
				break;
			//End of Silver
			//Start of Steel
			case 3999:
				bar = Bar.STEEL;
				player.getActionDeque().addAction(new Smelting(player, bar, 1));
				player.getActionSender().removeInterfaces();
				break;
			case 3998:
				bar = Bar.STEEL;
				player.getActionDeque().addAction(new Smelting(player, bar, 5));
				player.getActionSender().removeInterfaces();
				break;
			case 3997:
				bar = Bar.STEEL;
				player.getActionDeque().addAction(new Smelting(player, bar, 10));
				player.getActionSender().removeInterfaces();
				break;
			case 3996:
				player.setCurrentBar(Bar.STEEL);
				player.setEnterXInterfaceId(2400);
				player.getActionSender().sendEnterX();
				break;
			//End of Steel
			//Start of Gold
			case 4003:
				bar = Bar.GOLD;
				player.getActionDeque().addAction(new Smelting(player, bar, 1));
				player.getActionSender().removeInterfaces();
				break;
			case 4002:
				bar = Bar.GOLD;
				player.getActionDeque().addAction(new Smelting(player, bar, 5));
				player.getActionSender().removeInterfaces();
				break;
			case 4001:
				bar = Bar.GOLD;
				player.getActionDeque().addAction(new Smelting(player, bar, 10));
				player.getActionSender().removeInterfaces();
				break;
			case 4000:
				player.setCurrentBar(Bar.GOLD);
				player.setEnterXInterfaceId(2400);
				player.getActionSender().sendEnterX();
				break;
			//End of Gold
			//Start of Mithril
			case 7441:
				bar = Bar.MITHRIL;
				player.getActionDeque().addAction(new Smelting(player, bar, 1));
				player.getActionSender().removeInterfaces();
				break;
			case 7440:
				bar = Bar.MITHRIL;
				player.getActionDeque().addAction(new Smelting(player, bar, 5));
				player.getActionSender().removeInterfaces();
				break;
			case 6397:
				bar = Bar.MITHRIL;
				player.getActionDeque().addAction(new Smelting(player, bar, 10));
				player.getActionSender().removeInterfaces();
				break;
			case 4158:
				player.setCurrentBar(Bar.MITHRIL);
				player.setEnterXInterfaceId(2400);
				player.getActionSender().sendEnterX();
				break;
			//End of Mithril
			//Start of Adamant
			case 7446:
				bar = Bar.ADAMANT;
				player.getActionDeque().addAction(new Smelting(player, bar, 1));
				player.getActionSender().removeInterfaces();
				break;
			case 7444:
				bar = Bar.ADAMANT;
				player.getActionDeque().addAction(new Smelting(player, bar, 5));
				player.getActionSender().removeInterfaces();
				break;
			case 7443:
				bar = Bar.ADAMANT;
				player.getActionDeque().addAction(new Smelting(player, bar, 10));
				player.getActionSender().removeInterfaces();
				break;
			case 7442:
				player.setCurrentBar(Bar.ADAMANT);
				player.setEnterXInterfaceId(2400);
				player.getActionSender().sendEnterX();
				break;
			//End of Adamant
			//Start of Runite
			case 7450:
				bar = Bar.RUNITE;
				player.getActionDeque().addAction(new Smelting(player, bar, 1));
				player.getActionSender().removeInterfaces();
				break;
			case 7449:
				bar = Bar.RUNITE;
				player.getActionDeque().addAction(new Smelting(player, bar, 5));
				player.getActionSender().removeInterfaces();
				break;
			case 7448:
				bar = Bar.RUNITE;
				player.getActionDeque().addAction(new Smelting(player, bar, 10));
				player.getActionSender().removeInterfaces();
				break;
			case 7447:
				player.setCurrentBar(Bar.RUNITE);
				player.setEnterXInterfaceId(2400);
				player.getActionSender().sendEnterX();
				break;
			//End of Runite
			//End of Smithing: Smelting Bars

			//Begin Duel Arena
			case 6674:
				DuelArena.getInstance().accept_stage_1(player);
				break;
			case 6520:
				DuelArena.getInstance().accept_stage_2(player);
				break;
			/**
			 * Start of duel configuring... duel_button_ranged = null=OFF, 0=ON
			 * 
			 * TODO: Confirm that everything applies how No Ranged rule is done.
			 */
			case 6698: //No Ranged
			case 6725:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				if (player.getAttribute("duel_button_melee") != null && player.getAttribute("duel_button_magic") != null && player.getAttribute("duel_button_ranged") == null) {
					player.getActionSender().sendMessage("You must have atleast one combat style to use.");
					return;
				}
				//Forfeiting Fix
				if (player.getAttribute("duel_button_forfeit") != null && player.getAttribute("duel_button_melee") != null) {
					player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[0]));
					player.toggleAttribute("duel_button_forfeit");
					player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
					if (otherPlayer != null) {
						otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
						otherPlayer.toggleAttribute("duel_button_forfeit");
						otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_ranged") == null ? Constants.duel_rule_configs[2] : -Constants.duel_rule_configs[2])));
				player.toggleAttribute("duel_button_ranged");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_ranged");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT")) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 6699: //No Melee
			case 6726:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				if (player.getAttribute("duel_button_melee") == null && player.getAttribute("duel_button_magic") != null && player.getAttribute("duel_button_ranged") != null) {
					player.getActionSender().sendMessage("You must have atleast one combat style to use.");
					return;
				}
				// Forfeiting Fix
				if (player.getAttribute("duel_button_forfeit") != null && player.getAttribute("duel_button_melee") == null) {
					player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[0]));
					player.toggleAttribute("duel_button_forfeit");
					player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
					if (otherPlayer != null) {
						otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
						otherPlayer.toggleAttribute("duel_button_forfeit");
						otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					}
				}
				// Ranged + Weapon Disabled Fix
				if (player.getAttribute("duel_button_weapon") != null && player.getAttribute("duel_button_ranged") == null && player.getAttribute("duel_button_magic") != null) {
					player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[14]));
					player.toggleAttribute("duel_button_weapon");
					player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
					if (otherPlayer != null) {
						otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
						otherPlayer.toggleAttribute("duel_button_weapon");
						otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_melee") == null ? Constants.duel_rule_configs[3] : -Constants.duel_rule_configs[3])));
				player.toggleAttribute("duel_button_melee");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_melee");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 6697: //No Magic
			case 6727:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				if (player.getAttribute("duel_button_melee") != null && player.getAttribute("duel_button_magic") == null && player.getAttribute("duel_button_ranged") != null) {
					player.getActionSender().sendMessage("You must have atleast one combat style to use.");
					return;
				}
				// Forfeiting Fix
				if (player.getAttribute("duel_button_forfeit") != null && player.getAttribute("duel_button_melee") != null) {
					player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[0]));
					player.toggleAttribute("duel_button_forfeit");
					player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
					if (otherPlayer != null) {
						otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
						otherPlayer.toggleAttribute("duel_button_forfeit");
						otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					}
				}
				// Ranged + Weapon Disabled Fix
				if (player.getAttribute("duel_button_weapon") != null && player.getAttribute("duel_button_ranged") == null && player.getAttribute("duel_button_melee") != null) {
					player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[14]));
					player.toggleAttribute("duel_button_weapon");
					player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
					if (otherPlayer != null) {
						otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
						otherPlayer.toggleAttribute("duel_button_weapon");
						otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_magic") == null ? Constants.duel_rule_configs[4] : -Constants.duel_rule_configs[4])));
				player.toggleAttribute("duel_button_magic");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_magic");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 7817: //No Special Attacks
			case 7816:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_sp_atk") == null ? Constants.duel_rule_configs[10] : -Constants.duel_rule_configs[10])));
				player.toggleAttribute("duel_button_sp_atk");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_sp_atk");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 669: //Fun Weapons
			case 670:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				if (player.getAttribute("duel_button_weapon") != null) {
					player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[14]));
					player.toggleAttribute("duel_button_weapon");
					player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
					if (otherPlayer != null) {
						otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
						otherPlayer.toggleAttribute("duel_button_weapon");
						otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_fun_wep") == null ? Constants.duel_rule_configs[9] : -Constants.duel_rule_configs[9])));
				player.toggleAttribute("duel_button_fun_wep");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_fun_wep");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 6696: //No Forfeit
			case 6721:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				if (player.getAttribute("duel_button_movement") != null) {
					player.getActionSender().sendMessage("You can't toggle forfeiting off with no movement.");
					return;
				}
				if (player.getAttribute("duel_button_melee") != null && (player.getAttribute("duel_button_magic") == null || player.getAttribute("duel_button_ranged") == null)) {
					player.getActionSender().sendMessage("You can't have no forfeit and no melee - you could run out of ammo.");
					return;
				} 
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_forfeit") == null ? Constants.duel_rule_configs[0] : -Constants.duel_rule_configs[0])));
				player.toggleAttribute("duel_button_forfeit");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_forfeit");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 6701: //No Drinks
			case 6728:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_drinks") == null ? Constants.duel_rule_configs[5] : -Constants.duel_rule_configs[5])));
				player.toggleAttribute("duel_button_drinks");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_drinks");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 6702: //No Food
			case 6729:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_food") == null ? Constants.duel_rule_configs[6] : -Constants.duel_rule_configs[6])));
				player.toggleAttribute("duel_button_food");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_food");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 6703: //No Prayer
			case 6730:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_prayer") == null ? Constants.duel_rule_configs[7] : -Constants.duel_rule_configs[7])));
				player.toggleAttribute("duel_button_prayer");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_prayer");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 6704: //No Movement
			case 6722:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				//Forfeit Fix
				if (player.getAttribute("duel_button_forfeit") != null) {
					player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[0]));
					player.toggleAttribute("duel_button_forfeit");
					player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
					if (otherPlayer != null) {
						otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
						otherPlayer.toggleAttribute("duel_button_forfeit");
						otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					}
				}
				//Forfeit Fix
				if (player.getAttribute("duel_button_obstacles") != null) {
					player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") - Constants.duel_rule_configs[8]));
					player.toggleAttribute("duel_button_obstacles");
					player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
					if (otherPlayer != null) {
						otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
						otherPlayer.toggleAttribute("duel_button_obstacles");
						otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_movement") == null ? Constants.duel_rule_configs[1] : -Constants.duel_rule_configs[1])));
				player.toggleAttribute("duel_button_movement");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_movement");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 6731: //Obstacles
			case 6732:
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				if (player.getAttribute("duel_button_movement") != null) {
					player.getActionSender().sendMessage("You can't go into the obstacle arena with no movement.");
					return;
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_obstacles") == null ? Constants.duel_rule_configs[8] : -Constants.duel_rule_configs[8])));
				player.toggleAttribute("duel_button_obstacles");
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_obstacles");
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13813: //Helmets
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(0) != null) {
					int free_inv_slots = player.getInventory().getItemContainer().freeSlots();
					int weapon_count = player.getEquipment().get(3) == null ? 0 : player.getEquipment().get(3).getCount();
					int arrows_count = player.getEquipment().get(13) == null ? 0 : player.getEquipment().get(13).getCount();
					int weapon_count_inv = player.getEquipment().get(3) == null ? 0 : player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null ? player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() : 0;
					int arrows_count_inv = player.getEquipment().get(13) == null ? 0 : player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount();
					int weapon_count_duel_inv = player.getEquipment().get(3) == null ? 0 : player.getDuelInventory().contains(player.getEquipment().get(3).getId()) ? player.getDuelInventory().getById(player.getEquipment().get(3).getId()).getCount() : 0;
					int arrows_count_duel_inv = player.getEquipment().get(13) == null ? 0 : player.getDuelInventory().contains(player.getEquipment().get(13).getId()) ? player.getDuelInventory().getById(player.getEquipment().get(13).getId()).getCount() : 0;
					int weapon_count_other_duel_inv = otherPlayer.getEquipment().get(3) == null ? 0 : otherPlayer.getDuelInventory().contains(player.getEquipment().get(3).getId()) ? otherPlayer.getDuelInventory().getById(player.getEquipment().get(3).getId()).getCount() : 0;
					int arrows_count_other_duel_inv = otherPlayer.getEquipment().get(13) == null ? 0 : otherPlayer.getDuelInventory().contains(player.getEquipment().get(13).getId()) ? otherPlayer.getDuelInventory().getById(player.getEquipment().get(13).getId()).getCount() : 0;
					int required_space = (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_helmet") != null ? -1 : 0);
					if (weapon_count != 0) {
						if (weapon_count_inv + weapon_count <= 0) {
							required_space += 1;
						} else {
							required_space -= 1;
						}
					}
					if (free_inv_slots <= required_space) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				}
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(0) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_helmet") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount() + (otherPlayer.getDuelInventory().contains(otherPlayer.getEquipment().get(13).getId()) ? otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(13).getId()).getCount() : 0) + (player.getDuelInventory().contains(otherPlayer.getEquipment().get(13).getId()) ? player.getDuelInventory().getById(otherPlayer.getEquipment().get(13).getId()).getCount() : 0) <= 0 ? 0 : -1) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount() + (otherPlayer.getDuelInventory().contains(otherPlayer.getEquipment().get(3).getId()) ? otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(3).getId()).getCount() : 0) + (player.getDuelInventory().contains(otherPlayer.getEquipment().get(3).getId()) ? player.getDuelInventory().getById(otherPlayer.getEquipment().get(3).getId()).getCount() : 0) <= 0 ? 0 : -1))) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_helmet") == null ? Constants.duel_rule_configs[11] : -Constants.duel_rule_configs[11])));
				player.toggleAttribute("duel_button_helmet");
				if (player.getEquipment().get(0) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_helmet") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					if (otherPlayer.getEquipment().get(0) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_helmet") != null ? 1 : -1));
					}
					otherPlayer.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space"));
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13814: //Capes
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(1) != null) {
					if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_cape") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				}
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(1) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_cape") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_cape") == null ? Constants.duel_rule_configs[12] : -Constants.duel_rule_configs[12])));
				player.toggleAttribute("duel_button_cape");
				if (player.getEquipment().get(1) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_cape") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_cape");
					if (otherPlayer.getEquipment().get(1) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_cape") != null ? 1 : -1));
					}
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13815: //Amulet
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(2) != null) {
					if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_amulet") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				}
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(2) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_amulet") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_amulet") == null ? Constants.duel_rule_configs[13] : -Constants.duel_rule_configs[13])));
				player.toggleAttribute("duel_button_amulet");
				if (player.getEquipment().get(2) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_amulet") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_amulet");
					if (otherPlayer.getEquipment().get(2) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_amulet") != null ? 1 : -1));
					}
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13816: //Arrows
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(13) != null) {
					if (player.getInventory().getItemContainer().freeSlots() + ((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !(player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount() < 1)) ? 1 : 0) < (Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_arrows") != null ? -1 : 1) || !player.getInventory().hasRoomFor(player.getEquipment().get(13)) || (player.getDuelInventory().getById(player.getEquipment().get(13).getId()) != null && Integer.MAX_VALUE - (player.getDuelInventory().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				}
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(13) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() + ((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !(otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount() < 1)) ? 1 : 0) < (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_arrows") != null ? -1 : 1) || !otherPlayer.getInventory().hasRoomFor(otherPlayer.getEquipment().get(13)) || (otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(13).getId()) != null && Integer.MAX_VALUE - (otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_arrows") == null ? Constants.duel_rule_configs[21] : -Constants.duel_rule_configs[21])));
				player.toggleAttribute("duel_button_arrows");
				if (player.getEquipment().get(13) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_arrows") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_arrows");
					if (otherPlayer.getEquipment().get(13) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_arrows") != null ? 1 : -1));
					}
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13817: //Weapon
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				if (player.getAttribute("duel_button_magic") != null && player.getAttribute("duel_button_melee") != null) {
					player.getActionSender().sendMessage("You can't disable weapon slot if you're using ranged combat.");
					return;
				}
				if (player.getAttribute("duel_button_fun_wep") != null) {
					player.getActionSender().sendMessage("You can't disable weapon slot if you're going to use fun weapons.");
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(3) != null) {
					if (player.getInventory().getItemContainer().freeSlots() + (player.getEquipment().get(3).getDefinition().isStackable() && player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !(player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount() < 1) ? 1 : 0) < (Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_weapon") != null ? -1 : 1) || (player.getEquipment().get(3).getDefinition().isStackable() && !player.getInventory().hasRoomFor(player.getEquipment().get(3)) || (player.getDuelInventory().getById(player.getEquipment().get(3).getId()) != null && Integer.MAX_VALUE - (player.getDuelInventory().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0))) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				}
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(3) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() + (otherPlayer.getEquipment().get(3).getDefinition().isStackable() && otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !(otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount() < 1) ? 1 : 0) < (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_weapon") != null ? -1 : 1) || (otherPlayer.getEquipment().get(3).getDefinition().isStackable() && !otherPlayer.getInventory().hasRoomFor(otherPlayer.getEquipment().get(3)) || (otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(3).getId()) != null && Integer.MAX_VALUE - (otherPlayer.getDuelInventory().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0))) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_weapon") == null ? Constants.duel_rule_configs[14] : -Constants.duel_rule_configs[14])));
				player.toggleAttribute("duel_button_weapon");
				if (player.getEquipment().get(3) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_weapon") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_weapon");
					if (otherPlayer.getEquipment().get(3) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_weapon") != null ? 1 : -1));
					}
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13818: //Chest
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(4) != null) {
					if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_chest") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				}
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(4) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_chest") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_chest") == null ? Constants.duel_rule_configs[15] : -Constants.duel_rule_configs[15])));
				player.toggleAttribute("duel_button_chest");
				if (player.getEquipment().get(4) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_chest") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
	
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_chest");
					if (otherPlayer.getEquipment().get(4) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_chest") != null ? 1 : -1));
					}
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13819: //Shield
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(5) != null) {
					if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_shield") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				}
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(5) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_shield") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				if (player.getAttribute("duel_button_shield") == null) {
					player.getActionSender().sendMessage("Beware: You won't be able to use two-handed weapons such as bows.");
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_shield") == null ? Constants.duel_rule_configs[16] : -Constants.duel_rule_configs[16])));
				player.toggleAttribute("duel_button_shield");
				if (player.getEquipment().get(5) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_shield") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_shield");
					if (otherPlayer.getEquipment().get(5) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_shield") != null ? 1 : -1));
					}
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13820: //Legs
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(7) != null) {
					if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_legs") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				}
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(7) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_legs") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_legs") == null ? Constants.duel_rule_configs[17] : -Constants.duel_rule_configs[17])));
				player.toggleAttribute("duel_button_legs");
				if (player.getEquipment().get(7) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_legs") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_legs");
					if (otherPlayer.getEquipment().get(7) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_legs") != null ? 1 : -1));
					}
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13823: //Gloves
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(9) != null) {
					if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_gloves") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				} 
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(9) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_gloves") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_gloves") == null ? Constants.duel_rule_configs[18] : -Constants.duel_rule_configs[18])));
				player.toggleAttribute("duel_button_gloves");
				if (player.getEquipment().get(9) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_gloves") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_gloves");
					if (otherPlayer.getEquipment().get(9) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_gloves") != null ? 1 : -1));
					}
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13822: //Boots
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(10) != null) {
					if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_boots") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				}
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(10) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_boots") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_boots") == null ? Constants.duel_rule_configs[19] : -Constants.duel_rule_configs[19])));
				player.toggleAttribute("duel_button_boots");
				if (player.getEquipment().get(10) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_boots") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_boots");
					if (otherPlayer.getEquipment().get(10) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_boots") != null ? 1 : -1));
					}
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			case 13821: //Ring
				if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
					return;
				}
				otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
				//Our player has an equipped item? Has space?
				if (player.getEquipment().get(12) != null) {
					if (player.getInventory().getItemContainer().freeSlots() <= ((Integer) player.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (player.getAttribute("duel_button_ring") != null ? -1 : 0) + (player.getEquipment().get(13) != null && player.getAttribute("duel_button_arrows") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(13).getId()).getCount() + player.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (player.getEquipment().get(3) != null && player.getAttribute("duel_button_weapon") != null && (player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()) != null && !((player.getInventory().getItemContainer().getById(player.getEquipment().get(3).getId()).getCount() + player.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("You don't have enough inventory space for this duel.");
						return;
					}
				}
				//Opponent has an equipped item? Has space?
				if (otherPlayer.getEquipment().get(12) != null) {
					if (otherPlayer.getInventory().getItemContainer().freeSlots() <= ((Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (player.getDuelInventory().size() + otherPlayer.getDuelInventory().size()) + (otherPlayer.getAttribute("duel_button_ring") != null ? -1 : 0) + (otherPlayer.getEquipment().get(13) != null && otherPlayer.getAttribute("duel_button_arrows") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(13).getId()).getCount() + otherPlayer.getEquipment().get(13).getCount()) <= 0)) ? -1 : 0) + (otherPlayer.getEquipment().get(3) != null && otherPlayer.getAttribute("duel_button_weapon") != null && (otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()) != null && !((otherPlayer.getInventory().getItemContainer().getById(otherPlayer.getEquipment().get(3).getId()).getCount() + otherPlayer.getEquipment().get(3).getCount()) <= 0)) ? -1 : 0))) {
						player.getActionSender().sendMessage("Opponent doesn't have enough space for this duel.");
						return;
					}
				}
				player.setAttribute("duel_button_config", ((Integer) player.getAttribute("duel_button_config") + (player.getAttribute("duel_button_ring") == null ? Constants.duel_rule_configs[20] : -Constants.duel_rule_configs[20])));
				player.toggleAttribute("duel_button_ring");
				if (player.getEquipment().get(12) != null) {
					player.setAttribute("duel_equipment_req_space", (Integer) player.getAttribute("duel_equipment_req_space") + (player.getAttribute("duel_button_ring") != null ? 1 : -1));
				}
				player.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				if (otherPlayer != null) {
					otherPlayer.setAttribute("duel_button_config", (Integer) player.getAttribute("duel_button_config"));
					otherPlayer.toggleAttribute("duel_button_ring");
					if (otherPlayer.getEquipment().get(12) != null) {
						otherPlayer.setAttribute("duel_equipment_req_space", (Integer) otherPlayer.getAttribute("duel_equipment_req_space") + (otherPlayer.getAttribute("duel_button_ring") != null ? 1 : -1));
					}
					otherPlayer.getActionSender().sendConfig(286, (Integer) player.getAttribute("duel_button_config"));
				}
				if ((player.getAttribute("duel_stage") == "ACCEPT" || otherPlayer.getAttribute("duel_stage") == "ACCEPT") && otherPlayer != null) {
					player.setAttribute("duel_stage", "FIRST_WINDOW");
					otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
					player.getActionSender().sendString("", 6684);
					otherPlayer.getActionSender().sendString("", 6684);
				}
				break;
			//End Duel Arena
			case 2461: // 2 Dialogue Options, Option 1
			case 2471: // 3 Dialogue Options, Option 1
			case 2482: // 4 Dialogue Options, Option 1
			case 2494: // 5 Dialogue Options, Option 1
				player.getCurrentDialogue().click(0, player);
				break;
			case 2462: // 2 Dialogue Options, Option 2
			case 2472: // 3 Dialogue Options, Option 2
			case 2483: // 4 Dialogue Options, Option 2
			case 2495: // 5 Dialogue Options, Option 2
				player.getCurrentDialogue().click(1, player);
				break;
			case 2473: // 3 Dialogue Options, Option 3
			case 2484: // 4 Dialogue Options, Option 3
			case 2496: // 5 Dialogue Options, Option 3
				player.getCurrentDialogue().click(2, player);
				break;
			case 2485: // 4 Dialogue Options, Option 4
			case 2497: // 5 Dialogue Options, Option 4
				player.getCurrentDialogue().click(3, player);
				break;
			case 2498: // 5 Dialogue Options, Option 5
				player.getCurrentDialogue().click(4, player);
				break;
			case 17001: //XP Lock
				if (!player.getXpLock()) {
					player.setXpLock(true);
					player.getActionSender().sendMessage("You will no longer gain Combat experience.");
				} else {
					player.setXpLock(false);
					player.getActionSender().sendMessage("You will now gain Combat experience.");
				}
				break;
			//Spells
			case 17009: //Flaming Arrows
			case 17011: //Steroids
			case 17013: //Cure
			case 17015: //Heal
			case 17017: //Multi Flaming Arrows
			case 17019: //Multi Steroids
			case 17021: //Multi Cure
			case 17023: //Multi Heal
				Spell spell = Spell.forID(buttonId);
				if (CustomSpells.canCastSpell(player, spell)) {
					CustomSpells.getInstance().castEnhancement(player, spell);
					player.getUpdateFlags().sendForceMessage(spell.getCastMessage());
				}
				break;
			//Begin Teleports
			//Begin Free
			case 17041: //Falador
			case 17045: //Draynor
			case 17049: //Edgeville
			case 17051: //Varrock
			case 17053: //Azerith
				Teleportation.getInstance().activateTeleportButton(player, buttonId, false);
				break;
			//End Free
			//Begin Premium
			case 17043: //Fishing Guild
			case 17047: //Shilo Village
			case 17055: //Minigame
				Teleportation.getInstance().activateTeleportButton(player, buttonId, true);
				break;
			//End Premium
			//End Teleports
			case 1193: // Charge
				Charge.handleCharge(player);
				break;
			case 175:
				Skillcapes.doSkillcape(player);
				break;
			case 1159:
			case 15877:
				BoneConversion.convert(player, buttonId);
				break;
			case 12566: // Teleother Accept
				if (player.getTeleotherStage() == TeleotherStage.RECEIVED_REQUEST) {
					int playerId = (Integer) player.getAttribute("teleotherPartnerId");
					Player caster = World.getPlayer(playerId);
					if (caster != null) {
						if (Misc.getDistance(player.getPosition(), caster.getPosition()) > 10) {
							Teleother.handleDecline(player);
							return;
						}
						Teleother.handleAccept(caster, player, (Integer) player.getAttribute("teleotherSpellId"));
					}
				}
				break;
			case 12568: // Teleother Decline
				Teleother.handleDecline(player);
				break;
			case 1757:
			case 1772:
			case 4454:
				if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) == null) {
					return;
				}
				player.setAttackType(AttackTypes.RANGED);
				player.getCombatState().setCombatStyle(CombatStyle.ACCURATE);
				break;
			case 1756:
			case 1771:
			case 4453:
				if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) == null) {
					return;
				}
				player.setAttackType(AttackTypes.RANGED);
				player.getCombatState().setCombatStyle(CombatStyle.AGGRESSIVE);
				break;
			case 1755:
			case 1770:
			case 4452:
				if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) == null) {
					return;
				}
				player.setAttackType(AttackTypes.RANGED);
				player.getCombatState().setCombatStyle(CombatStyle.DEFENSIVE);
				break;
			case 335:
			case 433:
			case 785:
			case 1707:
			case 2285:
			case 2432:
			case 3805:
			case 4714:
			case 5579:
			case 6137:
			case 7771:
			case 12298:
			case 5862:
				if (buttonId == 335) {
					Magic.getInstance().resetAll(player, false);
				}
				if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null) {
					return;
				}
				player.setAttackType(AttackTypes.MELEE);
				player.getCombatState().setCombatStyle(CombatStyle.ACCURATE);
				break;
			case 334:
			case 432:
			case 784:
			case 1706:
			case 2284:
			case 2431:
			case 3804:
			case 4713:
			case 5578:
			case 5861:
			case 6136:
			case 7770:
				if (buttonId == 334) {
					Magic.getInstance().resetAll(player, false);
				}
				if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null) {
					return;
				}
				player.setAttackType(AttackTypes.MELEE);
				player.getCombatState().setCombatStyle(CombatStyle.AGGRESSIVE);
				break;
			case 1704:
			case 4711:
			case 5576:
			case 782:
			case 2282:
				if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null) {
					return;
				}
				player.setAttackType(AttackTypes.MELEE);
				player.getCombatState().setCombatStyle(CombatStyle.AGGRESSIVE_1);
				break;
			case 336:
			case 431:
			case 783:
			case 1705:
			case 2283:
			case 2430:
			case 3803:
			case 4686:
			case 4712:
			case 1080:
			case 5577:
			case 5860:
			case 6135:
			case 7769:
			case 12296:
				if (buttonId == 336) {
					Magic.getInstance().resetAll(player, false);
				}
				if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null) {
					return;
				}
				player.setAttackType(AttackTypes.MELEE);
				player.getCombatState().setCombatStyle(CombatStyle.DEFENSIVE);
				break;
			case 4688:
			case 8468:
				if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null) {
					return;
				}
				player.setAttackType(AttackTypes.MELEE);
				player.getCombatState().setCombatStyle(CombatStyle.CONTROLLED_1);
				break;
			case 4687:
			case 12297:
				if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null) {
					return;
				}
				player.setAttackType(AttackTypes.MELEE);
				player.getCombatState().setCombatStyle(CombatStyle.CONTROLLED_2);
				break;
			case 4685:
			case 2429:
			case 3802:
			case 7768:
				if (player.getEquipment().get(3) != null && RangedDataLoader.getProjectileId(player.getEquipment().get(3).getId()) != null) {
					return;
				}
				player.setAttackType(AttackTypes.MELEE);
				player.getCombatState().setCombatStyle(CombatStyle.CONTROLLED_3);
				break;
			case 150:
				player.setAutoRetaliate((!player.shouldAutoRetaliate()));
				break;
			case 14922:
				player.getActionSender().removeInterfaces();
				break;
			case 3546:
				if (player.getDiceStage().equals(DiceStage.SECOND_DICE_WINDOW)) {
					player.getDicing().acceptStageTwo(player);;
				} else {
					player.getTrading().acceptStageTwo(player);
				}
				break;
			case 3420:
				if (player.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT)) {
					player.getDicing().acceptStageOne(player);
				} else {
					player.getTrading().acceptStageOne(player);
				}
				break;
			case 3651:
				player.setAppearanceUpdateRequired(true);
				player.getUpdateFlags().setUpdateRequired(true);
				player.setAttribute("hasDesigned", (byte) 0);
				player.getActionSender().removeInterfaces();
				break;
			case 13030:
				if (player.getCombatTimer() > 0) {
					player.getActionSender().sendMessage("You can't logout until 10 seconds after the end of combat.");
					return;
				}
				player.getActionSender().sendLogout();
				break;
			//Begin Single Model Interface (ID: 4429)
			case 1747: //Single Model Interface - Make All
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case GEM_CRAFTING:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new GemCutting(player, player.getInventory().getItemContainer().getCount(((Gem) player.getAttribute("craftingGem")).getUncutGem()), (Gem) player.getAttribute("craftingGem")));
							player.removeAttribute("craftingType");
							player.removeAttribute("craftingGem");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else if (player.getAttribute("herbloreType") != null) {
					switch ((IngredientType) player.getAttribute("herbloreType")) {
						case PRIMARY_INGREDIENT:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new Herbalism(player, player.getInventory().getItemContainer().getCount(((PrimaryIngredient) player.getAttribute("herbloreIngredient")).getId()), (IngredientType) player.getAttribute("herbloreType"), (PrimaryIngredient) player.getAttribute("herbloreIngredient"), null));
							player.removeAttribute("herbloreIngredient");
							player.removeAttribute("herbloreType");
							break;
						case SECONDARY_INGREDIENT:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new Herbalism(player, player.getInventory().getItemContainer().getCount(((SecondaryIngredient) player.getAttribute("herbloreIngredient")).getId()), IngredientType.SECONDARY_INGREDIENT, null, (SecondaryIngredient) player.getAttribute("herbloreIngredient")));
							player.removeAttribute("herbloreIngredient");
							player.removeAttribute("herbloreType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.getAttribute("herbloreIngredient") != null) {
								player.removeAttribute("herbloreIngredient");
							}
							if (player.getAttribute("herbloreType") != null) {
								player.removeAttribute("herbloreType");
							}
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Herblore Type: " + player.getAttribute("herbloreType") + "!");
							}
							logger.severe("Unhandled Herblore Type: " + player.getAttribute("herbloreType") + "!");
							break;
					}
				} else if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new BowCreation(player, player.getInventory().getItemContainer().getCount(((UnstrungBows) player.getAttribute("fletchingItem")).getItemId()), (UnstrungBows) player.getAttribute("fletchingItem")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingItem");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, player.getInventory().getItemContainer().getCount(((FletchableLogs) player.getAttribute("fletchingLog")).getLogId()), 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						case ARROW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArrowCreation(player, (int) player.getAttribute("creationAmount"), (ArrowTips) player.getAttribute("arrowTip")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("arrowTip");
							player.removeAttribute("creationAmount");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						case HEADLESS_ARROW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new HeadlessArrowCreation(player, (int) player.getAttribute("creationAmount")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("creationAmount");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 1748: //Single Model Interface - Make X
				player.setEnterXInterfaceId(1748);
				player.getActionSender().sendEnterX();
				break;
			case 2798: //Single Model Interface - Make 5
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case GEM_CRAFTING:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new GemCutting(player, 5, (Gem) player.getAttribute("craftingGem")));
							player.removeAttribute("craftingType");
							player.removeAttribute("craftingGem");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else if (player.getAttribute("herbloreType") != null) {
					switch ((IngredientType) player.getAttribute("herbloreType")) {
						case PRIMARY_INGREDIENT:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new Herbalism(player, 5, (IngredientType) player.getAttribute("herbloreType"), (PrimaryIngredient) player.getAttribute("herbloreIngredient"), null));
							break;
						case SECONDARY_INGREDIENT:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new Herbalism(player, 5, IngredientType.SECONDARY_INGREDIENT, null, (SecondaryIngredient) player.getAttribute("herbloreIngredient")));
							player.removeAttribute("herbloreIngredient");
							player.removeAttribute("herbloreType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.getAttribute("herbloreIngredient") != null) {
								player.removeAttribute("herbloreIngredient");
							}
							if (player.getAttribute("herbloreType") != null) {
								player.removeAttribute("herbloreType");
							}
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Herblore Type: " + player.getAttribute("herbloreType") + "!");
							}
							logger.severe("Unhandled Herblore Type: " + player.getAttribute("herbloreType") + "!");
							break;
					}
				} else if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new BowCreation(player, 5, (UnstrungBows) player.getAttribute("fletchingItem")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingItem");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 5, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						case ARROW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArrowCreation(player, 5, (ArrowTips) player.getAttribute("arrowTip")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("arrowTip");
							player.removeAttribute("creationAmount");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						case HEADLESS_ARROW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new HeadlessArrowCreation(player, 5));
							player.removeAttribute("fletchingType");
							player.removeAttribute("creationAmount");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 2799: //Single Model Interface - Make 1
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case GEM_CRAFTING:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new GemCutting(player, 1, (Gem) player.getAttribute("craftingGem")));
							player.removeAttribute("craftingType");
							player.removeAttribute("craftingGem");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else if (player.getAttribute("herbloreType") != null) {
					switch ((IngredientType) player.getAttribute("herbloreType")) {
						case PRIMARY_INGREDIENT:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new Herbalism(player, 1, (IngredientType) player.getAttribute("herbloreType"), (PrimaryIngredient) player.getAttribute("herbloreIngredient"), null));
							player.removeAttribute("herbloreIngredient");
							player.removeAttribute("herbloreType");
							break;
						case SECONDARY_INGREDIENT:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new Herbalism(player, 1, IngredientType.SECONDARY_INGREDIENT, null, (SecondaryIngredient) player.getAttribute("herbloreIngredient")));
							player.removeAttribute("herbloreIngredient");
							player.removeAttribute("herbloreType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.getAttribute("herbloreIngredient") != null) {
								player.removeAttribute("herbloreIngredient");
							}
							if (player.getAttribute("herbloreType") != null) {
								player.removeAttribute("herbloreType");
							}
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Herblore Type: " + player.getAttribute("herbloreType") + "!");
							}
							logger.severe("Unhandled Herblore Type: " + player.getAttribute("herbloreType") + "!");
							break;
					}
				} else if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new BowCreation(player, 1, (UnstrungBows) player.getAttribute("fletchingItem")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingItem");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 1, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						case ARROW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArrowCreation(player, 1, (ArrowTips) player.getAttribute("arrowTip")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("arrowTip");
							player.removeAttribute("creationAmount");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						case HEADLESS_ARROW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new HeadlessArrowCreation(player, 1));
							player.removeAttribute("fletchingType");
							player.removeAttribute("creationAmount");
							player.getActionSender().moveComponent(0, 0, 1746);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			//End Single Interface (ID: 4429)
			//Begin Double Interface (ID: 8866)
			case 8871: //Double Model Interface - Make X (First Model)
				player.setEnterXInterfaceId(8871);
				player.getActionSender().sendEnterX();
				break;
			case 8872: //Double Model Interface - Make 10 (First Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 10, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8869);
							player.getActionSender().moveComponent(0, 0, 8870);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8873: //Double Model Interface - Make 5 (First Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 5, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8869);
							player.getActionSender().moveComponent(0, 0, 8870);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8874: //Double Model Interface - Make 1 (First Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 1, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8869);
							player.getActionSender().moveComponent(0, 0, 8870);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8875: //Double Model Interface - Make X (Second Model)
				player.setEnterXInterfaceId(8875);
				player.getActionSender().sendEnterX();
				break;
			case 8876: //Double Model Interface - Make 10 (Second Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 10, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8869);
							player.getActionSender().moveComponent(0, 0, 8870);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8877: //Double Model Interface - Make 5 (Second Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 5, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8869);
							player.getActionSender().moveComponent(0, 0, 8870);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8878: //Double Model Interface - Make 1 (Second Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 1, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8869);
							player.getActionSender().moveComponent(0, 0, 8870);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			//End Double Interface (ID: 8866)
			//Begin Triple Interface (ID: 8880)
			case 8886: //Triple Model Interface - Make X (First Model)
				player.setEnterXInterfaceId(8886);
				player.getActionSender().sendEnterX();
				break;
			case 8887: //Triple Model Interface - Make 10 (First Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 10, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8883);
							player.getActionSender().moveComponent(0, 0, 8884);
							player.getActionSender().moveComponent(0, 0, 8885);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8888: //Triple Model Interface - Make 5 (First Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 5, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8883);
							player.getActionSender().moveComponent(0, 0, 8884);
							player.getActionSender().moveComponent(0, 0, 8885);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8889: //Triple Model Interface - Make 1 (First Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 1, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8883);
							player.getActionSender().moveComponent(0, 0, 8884);
							player.getActionSender().moveComponent(0, 0, 8885);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8890: //Triple Model Interface - Make X (Second Model)
				player.setEnterXInterfaceId(8890);
				player.getActionSender().sendEnterX();
				break;
			case 8891: //Triple Model Interface - Make 10 (Second Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 10, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8883);
							player.getActionSender().moveComponent(0, 0, 8884);
							player.getActionSender().moveComponent(0, 0, 8885);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8892: //Triple Model Interface - Make 5 (Second Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 5, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8883);
							player.getActionSender().moveComponent(0, 0, 8884);
							player.getActionSender().moveComponent(0, 0, 8885);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8893: //Triple Model Interface - Make 1 (Second Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 1, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8883);
							player.getActionSender().moveComponent(0, 0, 8884);
							player.getActionSender().moveComponent(0, 0, 8885);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8894: //Triple Model Interface - Make X (Third Model)
				player.setEnterXInterfaceId(8894);
				player.getActionSender().sendEnterX();
				break;
			case 8895: //Triple Model Interface - Make 10 (Third Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 10, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8896: //Triple Model Interface - Make 5 (Third Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 5, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8897: //Triple Model Interface - Make 1 (Third Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 1, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8883);
							player.getActionSender().moveComponent(0, 0, 8884);
							player.getActionSender().moveComponent(0, 0, 8885);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			//End Triple Interface (ID: 8880)
			//Begin Quadruple Interface (ID: 8899)
			case 8906: //Quadruple Model Interface - Make X (First Model)
				player.setEnterXInterfaceId(8906);
				player.getActionSender().sendEnterX();
				break;
			case 8907: //Quadruple Model Interface - Make 10 (First Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 10, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8908: //Quadruple Model Interface - Make 5 (First Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 5, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8909: //Quadruple Model Interface - Make 1 (First Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 1, 0, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8910: //Quadruple Model Interface - Make X (Second Model)
				player.setEnterXInterfaceId(8910);
				player.getActionSender().sendEnterX();
				break;
			case 8911: //Quadruple Model Interface - Make 10 (Second Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 10, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8912: //Quadruple Model Interface - Make 5 (Second Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 5, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8913: //Quadruple Model Interface - Make 1 (Second Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 1, 1, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8914: //Quadruple Model Interface - Make X (Third Model)
				player.setEnterXInterfaceId(8914);
				player.getActionSender().sendEnterX();
				break;
			case 8915: //Quadruple Model Interface - Make 10 (Third Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 10, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8916: //Quadruple Model Interface - Make 5 (Third Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 5, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8917: //Quadruple Model Interface - Make 1 (Third Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 1, 2, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8918: //Quadruple Model Interface - Make X (Fourth Model)
				player.setEnterXInterfaceId(8918);
				player.getActionSender().sendEnterX();
				break;
			case 8919: //Quadruple Model Interface - Make 10 (Fourth Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 10, 3, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8920: //Quadruple Model Interface - Make 5 (Fourth Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 5, 3, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8921: //Quadruple Model Interface - Make 1 (Fourth Model)
				if (player.getAttribute("fletchingType") != null) {
					switch ((CreationType) player.getAttribute("fletchingType")) {
						case UNSTRUNG_BOW_CREATION:
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new LogFletching(player, 1, 3, (FletchableLogs) player.getAttribute("fletchingLog")));
							player.removeAttribute("fletchingType");
							player.removeAttribute("fletchingLog");
							player.getActionSender().moveComponent(0, 0, 8902);
							player.getActionSender().moveComponent(0, 0, 8903);
							player.getActionSender().moveComponent(0, 0, 8904);
							player.getActionSender().moveComponent(0, 0, 8905);
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							}
							logger.severe("Unhandled Fletching Type: " + player.getAttribute("fletchingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			//End Quadruple Interface (ID: 8899)
			//Begin Quintuple Interface (ID: 8938)
			case 8946: //Quintuple Model Interface - Make X (First Model)
				player.setEnterXInterfaceId(8946);
				player.getActionSender().sendEnterX();
				break;
			case 8947: //Quintuple Model Interface - Make 10 (First Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8948: //Quintuple Model Interface - Make 5 (First Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8949: //Quintuple Model Interface - Make 1 (First Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[0])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8950: //Quintuple Model Interface - Make X (Second Model)
				player.setEnterXInterfaceId(8950);
				player.getActionSender().sendEnterX();
				break;
			case 8951: //Quintuple Model Interface - Make 10 (Second Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8952: //Quintuple Model Interface - Make 5 (Second Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8953: //Quintuple Model Interface - Make 1 (Second Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[1])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8954: //Quintuple Model Interface - Make X (Third Model)
				player.setEnterXInterfaceId(8954);
				player.getActionSender().sendEnterX();
				break;
			case 8955: //Quintuple Model Interface - Make 10 (Third Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8956: //Quintuple Model Interface - Make 5 (Third Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8957: //Quintuple Model Interface - Make 1 (Third Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[2])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8958: //Quintuple Model Interface - Make X (Fourth Model)
				player.setEnterXInterfaceId(8958);
				player.getActionSender().sendEnterX();
				break;
			case 8959: //Quintuple Model Interface - Make 10 (Fourth Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[3])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8960: //Quintuple Model Interface - Make 5 (Fourth Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[3])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8961: //Quintuple Model Interface - Make 1 (Fourth Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[3])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8962: //Quintuple Model Interface - Make X (Fifth Model)
				player.setEnterXInterfaceId(8962);
				player.getActionSender().sendEnterX();
				break;
			case 8963: //Quintuple Model Interface - Make 10 (Fifth Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[4])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8964: //Quintuple Model Interface - Make 5 (Fifth Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[4])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			case 8965: //Quintuple Model Interface - Make 1 (Fifth Model)
				if (player.getAttribute("craftingType") != null) {
					switch ((CraftingType) player.getAttribute("craftingType")) {
						case ARMOUR_CREATION:
							if (player.getAttribute("craftingHide") == null) {
								return;
							}
							player.getActionSender().removeInterfaces();
							player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(((Hide) player.getAttribute("craftingHide")).getCraftableOutcomes()[4])));
							player.removeAttribute("craftingHide");
							player.removeAttribute("craftingType");
							break;
						default:
							player.getActionSender().removeInterfaces();
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							}
							logger.severe("Unhandled Crafting Type: " + player.getAttribute("craftingType") + "!");
							break;
					}
				} else {
					player.getActionSender().removeInterfaces();
					if (player.isDebugging()) {
						player.getActionSender().sendMessage("Unhandled Creation! Button ID: " + buttonId + ".");
					}
					logger.severe("Unhandled Creation! Button ID: " + buttonId + ".");
					break;
				}
				break;
			//End Quintuple Interface (ID: 8938)
			case 5386: //Enable Bank Noting
				player.setAttribute("BANK_MODE_NOTE", (byte) 0);
				break;
			case 5387: //Disable Bank Noting
				player.removeAttribute("BANK_MODE_NOTE");
				break;
			case 8130: //Enable Bank Insertion
				player.removeAttribute("BANK_MODE_INSERT");
				break;
			case 8131: //Disable Bank Insertion
				player.setAttribute("BANK_MODE_INSERT", (byte) 0);
				break;
			case 12858: // Mouse Buttons
				player.setAttribute("mouseButtons", (byte) ((Byte) player.getAttribute("mouseButtons") == 1 ? 0 : 1));
				break;
			case 12859: // Chat Effects
				player.setAttribute("chatEffects", (byte) ((Byte) player.getAttribute("chatEffects") == 1 ? 0 : 1));
				break;
			case 1195: // Split Private Chat
				player.setAttribute("splitPrivate", (byte) ((Byte) player.getAttribute("splitPrivate") == 1 ? 0 : 1));
				break;
			case 1196: // Accept Aid
				player.setAttribute("acceptAid", (byte) ((Byte) player.getAttribute("acceptAid") == 1 ? 0 : 1));
				break;
			case 1197: // Run Toggle
				if (player.getAttribute("runLocked") == null) {
					if (player.getAttribute("isRunning") == null) {
						player.toggleRunMode(true);
					} else {
						player.toggleRunMode(false);
					}
				}
				break;
			case 12711:
				player.flip(false);
				break;
			case 12713:
				player.flip(true);
				break;
			case 7344:
				//May not work
				Quest q = QuestRepository.get(buttonId);
				q.start(player);
				break;
			case 8633: // Make 10 Leather Bodies
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(1129)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8634: // Make 5 Leather Bodies
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(1129)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8635: // Make 1 Leather Body
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(1129)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8636: // Make 10 Leather Gloves
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(1059)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8637: // Make 5 Leather Gloves
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(1059)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8638: // Make 1 Leather Gloves
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(1059)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8639: // Make 10 Leather Boots
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(1061)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8640: // Make 5 Leather Boots
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(1061)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8641: // Make 1 Leather Boots
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(1061)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");	
				break;
			case 8642: // Make 10 Leather Vambraces
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(1063)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8643: // Make 5 Leather Vambraces
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(1063)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8644: // Make 1 Leather Vambraces
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(1063)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8645: // Make 10 Leather Chaps
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(1095)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8646: // Make 5 Leather Chaps
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(1095)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8647: // Make 1 Leather Chaps
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(1095)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8648: // Make 10 Leather Coifs
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(1169)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8649: // Make 5 Leather Coifs
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(1169)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");	
				break;
			case 8650: // Make 1 Leather Coif
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(1169)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8651: // Make 10 Leather Cowls
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 10, Craftable.forReward(1167)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			case 8652: // Make 5 Leather Cowls
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 5, Craftable.forReward(1167)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");	
				break;
			case 8653: // Make 1 Leather Cowl
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new ArmourCreation(player, 1, Craftable.forReward(1167)));
				player.removeAttribute("craftingType");
				player.removeAttribute("craftingHide");
				break;
			//Begin Glassblowing
			case 11471: // Make X Vials
				player.setEnterXInterfaceId(11471);
				player.getActionSender().sendEnterX();
				break;
			case 11472: // Make 10 Vials
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 10, Glass.forReward(229)));
				break;
			case 11473: // Make 5 Vials
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 5, Glass.forReward(229)));
				break;
			case 11474: // Make 1 Vial
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 1, Glass.forReward(229)));
				break;
			case 11475: //Make X Uncharged Orbs
				player.setEnterXInterfaceId(11475);
				player.getActionSender().sendEnterX();
				break;
			case 12394: // Make 10 Uncharged Orbs
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 10, Glass.forReward(567)));
				break;
			case 12395: // Make 5 Uncharged Orbs
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 5, Glass.forReward(567)));
				break;
			case 12396: // Make 1 Uncharged Orb
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 1, Glass.forReward(567)));
				break;
			case 12397: // Make X Beer Glass
				player.setEnterXInterfaceId(12397);
				player.getActionSender().sendEnterX();
				break;
			case 12398: // Make 10 Beer Glass
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 10, Glass.forReward(1919)));
				break;
			case 12399: // Make 5 Beer Glass
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 5, Glass.forReward(1919)));
				break;
			case 12400: // Make 1 Beer Glass
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 1, Glass.forReward(1919)));
				break;
			case 12401: // Make X Candle Lantern
				player.setEnterXInterfaceId(12401);
				player.getActionSender().sendEnterX();
				break;
			case 12402: // Make 10 Candle Lantern
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 10, Glass.forReward(4527)));
				break;
			case 12403: // Make 5 Candle Lantern
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 5, Glass.forReward(4527)));
				break;
			case 12404: // Make 1 Candle Lantern
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 1, Glass.forReward(4527)));
				break;
			case 12405: // Make X Oil Lamp
				player.setEnterXInterfaceId(12405);
				player.getActionSender().sendEnterX();
				break;
			case 12406: // Make 10 Oil Lamp
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 10, Glass.forReward(4522)));
				break;
			case 12407: // Make  5 Oil Lamp
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 5, Glass.forReward(4522)));
				break;
			case 12408: // Make 1 Oil Lamp
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 1, Glass.forReward(4522)));
				break;
			case 6200: // Make X Fish Bowl
				player.setEnterXInterfaceId(6200);
				player.getActionSender().sendEnterX();
				break;
			case 6201: // Make 10 Fish Bowl
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 10, Glass.forReward(6667)));
				break;
			case 6202: // Make 5 Fish Bowl
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 5, Glass.forReward(6667)));
				break;
			case 6203: // Make 1 Fish Bowl
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 1, Glass.forReward(6667)));
				break;
			case 12409: // Make X Lantern Lens
				player.setEnterXInterfaceId(12409);
				player.getActionSender().sendEnterX();
				break;
			case 12410: // Make 10 Lantern Lens
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 10, Glass.forReward(4542)));
				break;
			case 12411: // Make 5 Lantern Lens
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 5, Glass.forReward(4542)));
				break;
			case 12412: // Make 1 Lantern Lens
				player.getActionSender().removeInterfaces();
				player.getActionDeque().addAction(new GlassBlowing(player, 1, Glass.forReward(4542)));
				break;
			//End Glassblowing
			//Begin Tanning
			case 14793: // Tan All Cowhide -> Soft Leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, player.getInventory().getItemContainer().getCount(1739), Hide.forReward(1741))
						);
					}
				}
				break;
			case 14794: // Tan All Cowhide -> Hard Leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, player.getInventory().getItemContainer().getCount(1739), Hide.forReward(1743))
						);
					}
				}
				break;
			case 14795: // Tan All Snake Hide -> Snakeskin
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, player.getInventory().getItemContainer().getCount(6287), Hide.forId(6287))
						);
					}
				}
				break;
			case 14796: // Tan All Snake Hide (Swamp) -> Snakeskin
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
								new LeatherTanning(player, player.getInventory().getItemContainer().getCount(7801), Hide.forId(7801))
						);
					}
				}
				break;
			case 14797: // Tan All Green D'hide -> Green D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, player.getInventory().getItemContainer().getCount(1753), Hide.forId(1753))
						);
					}
				}
				break;
			case 14798: // Tan All Blue D'hide -> Blue D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, player.getInventory().getItemContainer().getCount(1751), Hide.forId(1751))
						);
					}
				}
				break;
			case 14799: // Tan All Red D'hide -> Red D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, player.getInventory().getItemContainer().getCount(1749), Hide.forId(1749))
						);
					}
				}
				break;
			case 14800: // Tan All Black D'hide -> Black D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, player.getInventory().getItemContainer().getCount(1747), Hide.forId(1747))
						);
					}
				}
				break;
			case 14801: // Tan X Cowhide -> Soft Leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.setEnterXInterfaceId(14801);
						player.getActionSender().sendEnterX();
					}
				}
				break;
			case 14802: // Tan X Cowhide -> Hard Leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.setEnterXInterfaceId(14802);
						player.getActionSender().sendEnterX();
					}
				}
				break;
			case 14803: // Tan X Snake Hide -> Snakeskin
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.setEnterXInterfaceId(14803);
						player.getActionSender().sendEnterX();
					}
				}
				break;
			case 14804: // Tan X Snake Hide (Swamp) -> Snakeskin
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.setEnterXInterfaceId(14804);
						player.getActionSender().sendEnterX();
					}
				}
				break;
			case 14805: // Tan X Green D'hide -> Green D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.setEnterXInterfaceId(14805);
						player.getActionSender().sendEnterX();
					}
				}
				break;
			case 14806: // Tan X Blue D'hide -> Blue D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.setEnterXInterfaceId(14806);
						player.getActionSender().sendEnterX();
					}
				}
				break;
			case 14807: // Tan X Red D'hide -> Red D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.setEnterXInterfaceId(14807);
						player.getActionSender().sendEnterX();
					}
				}
				break;
			case 14808: // Tan X Black D'hide -> Black D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.setEnterXInterfaceId(14808);
						player.getActionSender().sendEnterX();
					}
				}
				break;
			case 14809: // Tan 5 Cowhide -> Soft Leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 5, Hide.forReward(1741))
						);
					}
				}
				break;
			case 14810: // Tan 5 Cowhide -> Hard Leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 5, Hide.forReward(1743))
						);
					}
				}
				break;
			case 14811: // Tan 5 Snake Hide -> Snakeskin
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 5, Hide.forId(6287))
						);
					}
				}
				break;
			case 14812: // Tan 5 Snake Hide (Swamp) -> Snakeskin
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 5, Hide.forId(7801))
						);
					}
				}
				break;
			case 14813: // Tan 5 Green D'hide -> Green D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 5, Hide.forId(1753))
						);
					}
				}
				break;
			case 14814: // Tan 5 Blue D'hide -> Blue D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 5, Hide.forId(1751))
						);
					}
				}
				break;
			case 14815: // Tan 5 Red D'hide -> Red D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 5, Hide.forId(1749))
						);
					}
				}
				break;
			case 14816: // Tan 5 Black D'hide -> Black D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 5, Hide.forId(1747))
						);
					}
				}
				break;

			case 14817: // Tan 1 Cowhide -> Soft Leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 1, Hide.forReward(1741))
						);
					}
				}
				break;
			case 14818: // Tan 1 Cowhide -> Hard Leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 1, Hide.forReward(1743))
						);
					}
				}
				break;
			case 14819: // Tan 1 Snake Hide -> Snakeskin
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 1, Hide.forId(6287))
						);
					}
				}
				break;
			case 14820: // Tan 1 Snake Hide (Swamp) -> Snakeskin
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 1, Hide.forId(7801))
						);
					}
				}
				break;
			case 14821: // Tan 1 Green D'hide -> Green D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 1, Hide.forId(1753))
						);
					}
				}
				break;
			case 14822: // Tan 1 Blue D'hide -> Blue D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 1, Hide.forId(1751))
						);
					}
				}
				break;
			case 14823: // Tan 1 Red D'hide -> Red D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 1, Hide.forId(1749))
						);
					}
				}
				break;
			case 14824: // Tan 1 Black D'hide -> Black D-leather
				if (player.getInteractingEntity().isNpc()) {
					String name = player.getInteractingEntity().getName().toLowerCase();
					String[] names = { "ellis", "sbott", "tanner" };
					if (Arrays.asList(names).contains(name)) {
						player.getActionDeque().addAction(
							new LeatherTanning(player, 1, Hide.forId(1747))
						);
					}
				}
				break;
			default:
				logger.log(Level.SEVERE, "Unhandled Action Button ID: " + buttonId + ".");
				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Unhandled Action Button ID: <col=CC0000>" + buttonId + "</col>.");
				}
				break;
		}
		Prayer.getInstance().setPrayers(player, buttonId);
		switch (buttonId) {
			case 7462:
			case 7487:
			case 7587:
			case 7537:
			case 7562:// 2276
			case 7612:// 3796
			case 7662:// 4679
			case 7687:// 4705
			case 7788:// 7762
			case 8481:// 8460
			case 12311:// 12290
				SpecialAttack.getInstance().clickSpecialBar(player, buttonId);
				break;
		}
		Magic.getInstance().handleAutocastButtons(player, buttonId);
		player.getBankPin().clickPinButton(buttonId);
		player.getBankPin().handleGeneralButtons(buttonId);
		EmoteActions.getInstance().activateEmoteButton(player, buttonId);
	}
}
