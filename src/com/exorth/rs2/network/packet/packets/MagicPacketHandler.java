package com.exorth.rs2.network.packet.packets;

import java.util.logging.Logger;

import com.exorth.rs2.content.combat.Combat;
import com.exorth.rs2.content.combat.magic.Magic;
import com.exorth.rs2.content.skills.magic.Alchemy;
import com.exorth.rs2.content.skills.magic.Enchanting;
import com.exorth.rs2.content.skills.magic.Superheating;
import com.exorth.rs2.content.skills.magic.Teleother;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.StreamBuffer.ByteOrder;
import com.exorth.rs2.network.StreamBuffer.ValueType;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;

/**
 * 
 * @author Jacob && AkZu
 * 
 */
public class MagicPacketHandler implements PacketHandler {

	public static final int MAGIC_ON_PLAYER = 249;
	public static final int MAGIC_ON_NPC = 131;
	public static final int MAGIC_ON_ITEM = 237;

	@Override
	public void handlePacket(Player player, Packet packet) {
		switch (packet.getOpcode()) {
			case MAGIC_ON_PLAYER:
				handleMagicOnPlayer(player, packet);
				break;
			case MAGIC_ON_NPC:
				handleMagicOnNpc(player, packet);
				break;
			case MAGIC_ON_ITEM:
				handleMagicOnItem(player, packet);
				break;
		}
	}

	private void handleMagicOnPlayer(Player player, Packet packet) {
		final int otherPlayerId = packet.getIn().readShort(true, ValueType.A);
		Player otherPlayer = World.getPlayer(otherPlayerId);
		if (otherPlayer == null) {
			return;
		}

		int spellId = packet.getIn().readShort(true, ByteOrder.LITTLE);
		Logger.getAnonymousLogger().info("Casting spell: " + spellId);

		switch (spellId) {
			case 12425:
			case 12435:
			case 12455:
				Teleother.handleFunctions(player, otherPlayer, spellId);
				return;
		}
		// TODO Attack player
		player.getUpdateFlags().faceEntity(otherPlayerId + 32768);
		if (!Combat.getInstance().meetsAttackRequirements(player, otherPlayer)) {
			player.getMovementHandler().reset();
			player.getActionSender().removeMapFlag();
			return;
		}
		player.getActionSender().removeMapFlag();
		// TODO: Single attack
		if (!Magic.getInstance().attackWithMagic(player, otherPlayer, spellId, true) || Combat.getInstance().withinRange(player, otherPlayer)) {
			player.getMovementHandler().reset();
			player.getActionSender().removeMapFlag();
		}
	}

	private void handleMagicOnNpc(Player player, Packet packet) {
		final int npc_id = packet.getIn().readShort(true, ValueType.A, ByteOrder.LITTLE);
		Npc npc = World.getNpc(npc_id);
		if (npc == null) {
			return;
		}
		int spellId = packet.getIn().readShort(true, ValueType.A);
		Logger.getAnonymousLogger().info("Casting spell : " + spellId);
		player.getUpdateFlags().faceEntity(npc_id);
		if (!Combat.getInstance().meetsAttackRequirements(player, npc)) {
			player.getMovementHandler().reset();
			player.getActionSender().removeMapFlag();
			return;
		}
		player.getActionSender().removeMapFlag();
		// TODO: Single attack
		if (!Magic.getInstance().attackWithMagic(player, npc, spellId, true) || Combat.getInstance().withinRange(player, npc)) {
			player.getMovementHandler().reset();
			player.getActionSender().removeMapFlag();
		}
	}

	private void handleMagicOnItem(Player player, Packet packet) {
		final int itemSlot = packet.getIn().readShort();
		packet.getIn().readShort(ValueType.A); // itemId, junk
		packet.getIn().readShort(); // interface id, junk
		final int spellId = packet.getIn().readShort(ValueType.A);
		// Logger.getAnonymousLogger().info(spellId);

		switch (spellId) {
			case 1173:
			case 17005:
				Superheating.handleSuperheating(player, spellId, itemSlot);
				break;
			case 1155:
			case 1165:
			case 1176:
			case 1180:
			case 1187:
			case 6003:
				Enchanting.handleEnchanting(player, spellId, itemSlot);
				break;
			case 17003:
				// TODO: Enchanting?
				player.getActionSender().sendMessage("Enchanting is currently being developed.");
				break;
			case 1162:
			case 1178:
			case 17007:
				Alchemy.alchemise(player, spellId, itemSlot);
				break;
		}
	}
}
