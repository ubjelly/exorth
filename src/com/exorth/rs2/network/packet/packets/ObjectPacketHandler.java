package com.exorth.rs2.network.packet.packets;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.WalkToActions;
import com.exorth.rs2.model.players.WalkToActions.Actions;
import com.exorth.rs2.network.StreamBuffer;
import com.exorth.rs2.network.StreamBuffer.ByteOrder;
import com.exorth.rs2.network.packet.Packet;
import com.exorth.rs2.network.packet.PacketManager.PacketHandler;

public class ObjectPacketHandler implements PacketHandler {

	public static final int ITEM_ON_OBJECT = 192;
	public static final int FIRST_CLICK = 132;
	public static final int SECOND_CLICK = 252;
	public static final int THIRD_CLICK = 70;

	@Override
	public void handlePacket(Player player, Packet packet) {
		if (player.isDebugging()) {
			player.getActionSender().sendMessage("Object Packet: <col=CC0000>" + packet.hashCode() + "</col>");
			player.getActionSender().sendMessage("Packet Opcode: <col=CC0000>" + packet.getOpcode() + "</col> Packet Length: <col=CC0000>" + packet.getPacketLength() + "</col>");
		}
		switch (packet.getOpcode()) {
			case ITEM_ON_OBJECT:
				handleItemOnObject(player, packet);
				break;
			case FIRST_CLICK:
				handleFirstClick(player, packet);
				break;
			case SECOND_CLICK:
				handleSecondClick(player, packet);
				break;
			case THIRD_CLICK:
				handleThirdClick(player, packet);
				break;
		}
	}

	/**
	 * Handles an item on an object. Most specific itemOnObjects are in WalkToActions.java
	 * @param player
	 * @param packet
	 */
	private void handleItemOnObject(Player player, Packet packet) {
		packet.getIn().readShort(); // inv interface
		int objectId = packet.getIn().readShort(true, StreamBuffer.ByteOrder.LITTLE);
		int objectY = packet.getIn().readShort(true, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
		int slot = packet.getIn().readShort(true, ByteOrder.LITTLE);
		int objectX = packet.getIn().readShort(true, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
		packet.getIn().readShort(); // itemId, not needed use

		Item item = player.getInventory().get(slot);

		if (item == null) {
			return;
		}

		// player.setItemUsed?
		player.setClickId(objectId);
		player.setClickX(objectX);
		player.setClickY(objectY);
		WalkToActions.setActions(Actions.ITEM_ON_OBJECT);
		WalkToActions.doActions(player, item);
		if (player.isDebugging()) {
			player.getActionSender().sendMessage("Object: <col=CC0000>" + objectId + "</col> (X: <col=CC0000>" + objectX + "</col>, Y: <col=CC0000>" + objectY + "</col>).");
		}
	}

	private void handleFirstClick(Player player, Packet packet) {
		player.setClickX(packet.getIn().readShort(true, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE));
		player.setClickId(packet.getIn().readShort());
		player.setClickY(packet.getIn().readShort(StreamBuffer.ValueType.A));
		player.getActionSender().sendMessage("Object: <col=CC0000>" + player.getClickId() + "</col> (X: <col=CC0000>" + player.getClickX() + "</col>, Y: <col=CC0000>" + player.getClickY() + "</col>).");
		WalkToActions.setActions(Actions.OBJECT_FIRST_CLICK);
		WalkToActions.doActions(player);
		if (player.isDebugging()) {
			player.getActionSender().sendMessage("Object: <col=CC0000>" + player.getClickId() + "</col> (X: <col=CC0000>" + player.getClickX() + "</col>, Y: <col=CC0000>" + player.getClickY() + "</col>).");
		}
	}

	private void handleSecondClick(Player player, Packet packet) {
		player.setClickId(packet.getIn().readShort(StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE));
		player.setClickY(packet.getIn().readShort(true, StreamBuffer.ByteOrder.LITTLE));
		player.setClickX(packet.getIn().readShort(StreamBuffer.ValueType.A));
		WalkToActions.setActions(Actions.OBJECT_SECOND_CLICK);
		WalkToActions.doActions(player);
		if (player.isDebugging()) {
			player.getActionSender().sendMessage("Object: <col=CC0000>" + player.getClickId() + "</col> (X: <col=CC0000>" + player.getClickX() + "</col>, Y: <col=CC0000>" + player.getClickY() + "</col>).");
		}
	}

	private void handleThirdClick(Player player, Packet packet) {
		player.setClickX(packet.getIn().readShort(true, StreamBuffer.ByteOrder.LITTLE));
		player.setClickY(packet.getIn().readShort());
		player.setClickId(packet.getIn().readShort(StreamBuffer.ValueType.A, ByteOrder.LITTLE));
		WalkToActions.setActions(Actions.OBJECT_THIRD_CLICK);
		WalkToActions.doActions(player);
		if (player.isDebugging()) {
			player.getActionSender().sendMessage("Object: <col=CC0000>" + player.getClickId() + "</col> (X: <col=CC0000>" + player.getClickX() + "</col>, Y: <col=CC0000>" + player.getClickY() + "</col>).");
		}
	}
}
