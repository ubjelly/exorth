package com.exorth.rs2.network;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.exorth.rs2.Constants;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.io.AccountManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.util.SQLConnector;

public class VbIntegration {

	public static boolean loadViaVB(Player player) {
		try {
			SQLConnector.forumConnection();
			Statement statement = SQLConnector.forumConn.createStatement();
			ResultSet group = statement.executeQuery("SELECT * FROM user WHERE username = '"+player.getUsername()+"'");
					 while (group.next()) {
						int dbId = group.getInt("userid");
						player.setDatabaseId(dbId);
						findUserRank(player, group.getInt("usergroupid"));
						String pass = group.getString("password");
						String salt = group.getString("salt");
						String passe = passHash(player.getTempPassword(), salt);
				if (pass.equalsIgnoreCase(passe)) {
					statement.close();
					return true;
				} else { 
					statement.close();
					return false; 
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	private static void findUserRank(Player player, int forumRank) {
		switch (forumRank) {
			case 6: //Administrators
				player.setPremium(true);
				player.setRights(Rights.ADMINISTRATOR);
				break;
			case 5: //Global Moderators
			case 11: //Server Moderators
				player.setPremium(true);
				player.setRights(Rights.PLAYER_MODERATOR);
				break;
			case 15: //Forum Moderators
				player.setPremium(true);
				player.setRights(Rights.FORUM_MODERATOR);
				break;
			case 16: //Developers
			case 14: //Gold Members
			case 10: //Councilmen
			case 9: //Premium Members
				player.setPremium(true);
				player.setRights(Rights.PREMIUM);
				break;
			default: 
				player.setPremium(false);
				player.setRights(Rights.PLAYER);
				break;
		}
	}

	private static void getSlayerTask(Player player, int npc, int amount) {
		player.setSlayerTask(npc);
		player.setKillsLeft(amount);
	}

	/**
	 * Checks if the new user exists.
	 * @param p The player to save.
	 */
	public static boolean userExists(Player player) {
		try {
			SQLConnector.serverConnection();
			String query = "select * from exorth_players";
			ResultSet loading = SQLConnector.serverConn.createStatement().executeQuery(query);
			while (loading.next()) {
				if (loading.getInt("dbid") == player.getDatabaseId()) {
					return true;
				}
			}
			loading.close();
		} catch(SQLException e) {
			System.err.println("ERROR: "+e.getMessage());
		}
		return false;
	}
	/**
	 * Saves a character file.
	 * @param p The player to save.
	 */
	public static void saveCharactertoSQL(Player player) throws Exception {
		if (!userExists(player)) {
			try {
				SQLConnector.serverConnection();
				Statement statement = SQLConnector.serverConn.createStatement();
				String query = "INSERT INTO exorth_players(dbid, username) VALUES ('"+ player.getDatabaseId() + "', '"+ player.getUsername() + "')";
				statement.executeUpdate(query);
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
				return ;
			}
		} else {
			try {
				SQLConnector.serverConnection();
				Statement statement = SQLConnector.serverConn.createStatement();
				//Stats
				for (int i = 0; i < Skill.SKILL_NAME.length; i++) {
					String stats = "UPDATE exorth_players SET " + Skill.getSkillName((short) i).toLowerCase() + "='" + player.getSkill().getExp()[i] + "' WHERE dbid='"+player.getDatabaseId()+"'";
					statement.executeUpdate(stats);
				}
				int design = 0;
				if (player.getAttribute("hasDesigned") != null) {
					design = 1;
				}
				Long silenceTime = (long) 0;
				Long silenceDuration = (long) 0;
				if (player.getAttribute("silence_time") != null && player.getAttribute("silence_duration") != null) {
					silenceTime = player.getAttribute("silence_time");
					silenceDuration = player.getAttribute("silence_duration");
				}
				String query = "UPDATE exorth_players SET username ='" + player.getUsername() + "', posx ='" + player.getPosition().getX() + "'" +
						", posy ='" +player.getPosition().getY() + "', posz ='" + player.getPosition().getZ() + "', gender ='" + player.getGender() + "'," +
						"hasdesigned = '" + design +"', silencetime = '" + silenceTime + "', silenceduration = '" + silenceDuration + "', health = '" + player.getSkillLevel(3) + 
						"', currentprayer = '" + player.getSkill().getPrayerPoints() + "', slayertask = '" + player.getSlayerTask() + "', killsleft = '" + player.getKillsLeft() +
						"' WHERE dbid='"+player.getDatabaseId()+"'";
				statement.executeUpdate(query);
				statement.close();
			} catch (Exception e) {
				e.printStackTrace();
				return ;
			}
		}
	}

	public static void loadCharacterviaSQL(Player player) {
		if (!VbIntegration.loadViaVB(player)) {
			player.setReturnCode(Constants.LOGIN_RESPONSE_INVALID_CREDENTIALS);
			player.sendLoginResponse();
			player.disconnect();
			return;
		}
		try {
			SQLConnector.serverConnection();
			System.out.println("UserID: " + player.getDatabaseId());
			String query = "select * from exorth_players where dbid = '" + player.getDatabaseId() + "'";
			ResultSet loading = SQLConnector.serverConn.createStatement().executeQuery(query);
			if (loading.next()) {
				//stats
				for (int i = 0; i < Skill.SKILL_NAME.length; i++) {
					player.getSkill().getExp()[i] = loading.getInt(Skill.getSkillName((short) i).toLowerCase());
					player.getSkill().getLevel()[i] = player.getSkill().getLevelForXP(loading.getInt(Skill.SKILL_NAME[i].toLowerCase()));
					if (i == 3) {
						player.getSkill().getLevel()[3] = (byte) loading.getInt("health");
						if (player.getSkill().getLevel()[3] == 0) {
							player.getSkill().getLevel()[3] = player.getSkill().getLevelForXP(player.getSkill().getExp()[3]);
						}
					}
				}
				player.getSkill().setPrayerPoints(loading.getInt("currentprayer"));
				//position
				player.getPosition().setX(loading.getInt("posx"));
				player.getPosition().setY(loading.getInt("posy"));
				player.getPosition().setZ(loading.getInt("posz"));
				//appearance
				if (loading.getInt("hasdesigned") == 1) {
					player.setAttribute("hasDesigned", (byte) 0);
				}
				player.setGender(loading.getInt("gender"));
				//mute
				player.setAttribute("silence_duration", loading.getLong("silenceduration"));
				player.setAttribute("silence_time", loading.getLong("silencetime"));
				getSlayerTask(player, loading.getInt("slayertask"), loading.getInt("killsleft"));
			}
			loading.close();
			AccountManager.load(player);
		} catch (Exception e) {
			e.printStackTrace();
			return ;
		}
	}

	public static String passHash(String in, String salt) {
		String a = stringToMd5(in);
		String b = stringToMd5(a + salt);
		return b;
	}

	private static String stringToMd5(String input) {
		String original = input;
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(original.getBytes());
			byte messageDigest[] = md.digest();
			StringBuffer hexString = new StringBuffer();
			for (int i=0;i<messageDigest.length;i++) {
				String hex=Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1) {
					hexString.append('0');
				}
				hexString.append(hex);
			}
			return hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
