package com.exorth.rs2.network.security;

import java.sql.Statement;
import java.util.logging.Logger;

import com.exorth.rs2.model.players.Yeller;
import com.exorth.rs2.util.SQLConnector;

/**
 * Saves all the chat logs to the 'chat_log' database. 
 * @author Stephen
 */
public class ChatLog extends LogEntry {

	/**
	 * The logger for the class.
	 */
	private static final Logger logger = Logger.getLogger(ChatLog.class.getName());
	
	/**
	 * Inserts a new entry into the 'chat_log' table.
	 * @param player The player sending the message.
	 * @param message The message sent.
	 */
	public static void recordChat(String player, String message) {
		try {
			SQLConnector.serverConnection();
			Statement statement = SQLConnector.serverConn.createStatement();
			String query = "INSERT INTO chat_log(username, message, timestamp) VALUES ('" + player + "', '" + message + "', '" + getTimeStamp() + "')";
			statement.executeUpdate(query);
			statement.close();
		} catch (Exception e) {
			logger.severe("Unable to record chat!");
			e.printStackTrace();
			Yeller.alertStaff("Unable to record chat logs, please contact an admin.");
		}
	}
}
