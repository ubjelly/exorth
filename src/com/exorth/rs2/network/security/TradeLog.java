package com.exorth.rs2.network.security;

import java.sql.Statement;
import java.util.logging.Logger;

import com.exorth.rs2.model.players.Yeller;
import com.exorth.rs2.util.SQLConnector;

/**
 * Records all trades between players.
 * @author Stephen
 */
public class TradeLog extends LogEntry {

	/**
	 * The logger for the class.
	 */
	private static final Logger logger = Logger.getLogger(TradeLog.class.getName());
	
	/**
	 * Inserts a new entry into the table 'trade_log'.
	 * @param player The player who initiated the trade.
	 * @param otherPlayer The other player involved in the trade.
	 * @param playerItems The items the player is trading.
	 * @param otherPlayerItems The items the other player is trading.
	 */
	public static void recordTrade(String player, String otherPlayer, String playerItems, String otherPlayerItems) {
		try {
			SQLConnector.serverConnection();
			Statement statement = SQLConnector.serverConn.createStatement();
			String query = "INSERT INTO trade_log(player, otherplayer, playeritems, otherplayeritems, timestamp) VALUES ('" + player + "', '" + otherPlayer + "', '" + playerItems +  "', '" + otherPlayerItems + "', '" + getTimeStamp() + "')";
			statement.executeUpdate(query);
			statement.close();
		} catch (Exception e) {
			logger.severe("Unable to record trade!");
			e.printStackTrace();
			Yeller.alertStaff("Unable to record a trade, please contact an admin.");
		}
	}
}
