package com.exorth.rs2.network.security;

import java.sql.Statement;
import java.util.logging.Logger;

import com.exorth.rs2.model.players.Yeller;
import com.exorth.rs2.util.SQLConnector;

/**
 * Saves all bets for player vs. player dicing.
 * @author Stephen
 */
public class DiceLog extends LogEntry {

	/**
	 * The logger for the class.
	 */
	private final static Logger logger = Logger.getLogger(DiceLog.class.getName());
	
	/**
	 * Inserts a new entry to the 'dice_log' table.
	 * @param player The name of the player who initiates the dice game.
	 * @param opponent The name of the opponent.
	 * @param playerBet The string form of all the items the player bet.
	 * @param opponentBet The string form of all the items the opponent bet.
	 */
	public static void recordBet(String player, String opponent, String playerBet, String opponentBet, String winner) {
		try {
			SQLConnector.serverConnection();
			Statement statement = SQLConnector.serverConn.createStatement();
			String query = "INSERT INTO dice_log(player, opponent, playerbet, opponentbet, winner, timestamp) VALUES ('" + player + "', '" + opponent + "', '" + playerBet + "', '" + opponentBet + "', '" + winner + "', '" + getTimeStamp() + "')";
			statement.executeUpdate(query);
			statement.close();
		} catch (Exception e) {
			logger.severe("Unable to record bet!");
			e.printStackTrace();
			Yeller.alertStaff("Unable to record dicing bets, please contact an admin.");
		}
	}
}
