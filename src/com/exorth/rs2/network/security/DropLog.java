package com.exorth.rs2.network.security;

import java.sql.Statement;
import java.util.logging.Logger;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Yeller;
import com.exorth.rs2.util.SQLConnector;

/**
 * Saves information about every player dropped item on the server.
 * Contains (Username, the item, the amount).
 * @author Stephen
 */
public class DropLog extends LogEntry {

	/**
	 * The logger for the class.
	 */
	private final static Logger logger = Logger.getLogger(DropLog.class.getName());
	
	/**
	 * Adds a drop record to the drop table.
	 * @param player The player dropping the item.
	 * @param item The item being dropped.
	 */
	public static void recordDrop(Player player, Item item, String type) {
		try {
			SQLConnector.serverConnection();
			Statement statement = SQLConnector.serverConn.createStatement();
			String query = "INSERT INTO drop_log(username, item, amount, type, timestamp) VALUES ('" + player.getUsername() + "', '" + item.getName() + "', '" + item.getCount() + "', '" + type + "', '" + getTimeStamp() + "')";
			statement.executeUpdate(query);
			statement.close();
		} catch (Exception e) {
			logger.severe("Unable to record dropped item!");
			e.printStackTrace();
			Yeller.alertStaff("Unable to record dropped items, please contact an admin.");
		}
	}

}
