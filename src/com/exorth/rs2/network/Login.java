package com.exorth.rs2.network;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.security.SecureRandom;
import java.util.logging.Logger;

import com.exorth.rs2.Constants;
import com.exorth.rs2.HostGateway;
import com.exorth.rs2.Server;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Player.LoginStages;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.NameUtility;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 *
 */
public class Login {

	BigInteger RSA_EXPONENT = new BigInteger("124425314960550024206991065332877157931472210939505789558012215720454903710618146200843877022273818555405810618059191162604008259757866640421952188957253368398733319663236323097864278319463888334484786055755767881706264786840339899269810859874287402892848784247637729987603089254067178011764721326471352835473");

	BigInteger RSA_MODULUS = new BigInteger("143690958001225849100503496893758066948984921380482659564113596152800934352119496873386875214251264258425208995167316497331786595942754290983849878549630226741961610780416197036711585670124061149988186026407785250364328460839202438651793652051153157765358767514800252431284681765433239888090564804146588087023");
	
	/* 337 VW GTI */
	private static final int UID = 337;

	public void handleLogin(Player player, ByteBuffer inData) throws Exception {
		switch (player.getLoginStage()) {
			case CONNECTED:
				if (inData.remaining() < 2) {
					inData.compact();
					return;
				}

				// Validate the request.
				int request = inData.get() & 0xff;
				inData.get(); // Name hash.
				if (request != 14) {
					Logger.getAnonymousLogger().severe("Invalid login request: " + request);
					player.disconnect();
					return;
				}

				// Write the response.
				StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(17);
				out.writeLong(0); // First 8 bytes are ignored by the client.

				// -- Check if login attempts aren't full
				if (!HostGateway.attempt(player.getHost())) {
					out.writeByte(Constants.LOGIN_RESPONSE_LOGIN_ATTEMPTS_EXCEEDED);
					player.send(out.getBuffer());
					player.disconnect();
					break;
				}

				out.writeByte(0); // The response opcode, 0 for logging in.
				out.writeLong(new SecureRandom().nextLong()); // SSK.
				player.send(out.getBuffer());
				player.setLoginStage(LoginStages.LOGGING_IN);
				break;
			case LOGGING_IN:
				if (inData.remaining() < 2) {
					inData.compact();
					return;
				}

				// Validate the login type.
				int loginType = inData.get();
				if (loginType != 16 && loginType != 18) {
					Logger.getAnonymousLogger().severe("Invalid login type: " + loginType);
					player.disconnect();
					return;
				}

				// Ensure that we can read all of the login block.
				int blockLength = inData.get() & 0xFF;
				int loginEncryptSize = blockLength - (36 + 1 + 1 + 2);
				if (loginEncryptSize <= 0) {
					inData.flip();
					inData.compact();
					return;
				}

				// Read the login block.
				StreamBuffer.InBuffer in = StreamBuffer.newInBuffer(inData);

				/* The magic id (255) */
				int magicID = in.readByte(false);
				if (magicID != 255) {
					player.disconnect();
					return;
				}

				// Validate the client version.
				int clientVersion = in.readShort();
				if (clientVersion != 317) {
					Logger.getAnonymousLogger().severe("Invalid client version: " + clientVersion);
					player.disconnect();
					return;
				}

				/* If the client is in low memory mode. */
				in.readByte();

				// Skip the CRC keys.
				for (int i = 0; i < 9; i++) {
					in.readInt();
				}

				loginEncryptSize--;
				int reportedSize = inData.get() & 0xFF;
				if (reportedSize != loginEncryptSize) {
					Logger.getAnonymousLogger().severe("Encrypted packet size zero or negative: " + loginEncryptSize);
					player.disconnect();
					return;
				}
				byte[] encryptionBytes = new byte[loginEncryptSize];
				inData.get(encryptionBytes);
				ByteBuffer rsaBuffer = ByteBuffer.wrap(new BigInteger(encryptionBytes).modPow(RSA_EXPONENT, RSA_MODULUS).toByteArray());

				// Validate that the RSA block was decoded properly.
				int rsaOpcode = rsaBuffer.get() & 0xFF;
				if (rsaOpcode != 10) {
					Logger.getAnonymousLogger().severe("Unable to decode RSA block properly: " + rsaOpcode);
					player.disconnect();
					return;
				}

				// Set up the ISAAC ciphers.
				long clientHalf = rsaBuffer.getLong();
				long serverHalf = rsaBuffer.getLong();
				int[] isaacSeed = { (int) (clientHalf >> 32), (int) clientHalf, (int) (serverHalf >> 32), (int) serverHalf };
				player.setDecryptor(new ISAACCipher(isaacSeed));
				for (int i = 0; i < isaacSeed.length; i++) {
					isaacSeed[i] += 50;
				}
				player.setEncryptor(new ISAACCipher(isaacSeed));

				/* The client unique ID. */
				int uniqueId = rsaBuffer.getInt();
				if (uniqueId != UID) {
					player.disconnect();
					return;
				}

				String username = NameUtility.getRS2String(rsaBuffer);
				String password = NameUtility.getRS2String(rsaBuffer);

				username = username.replaceAll("[^A-z 0-9]", " ").trim();
				password = password.replaceAll("[^A-z 0-9]", " ").trim();

				if (username.length() < 1 || password.length() < 4 || password.length() > 20 || username.length() > 12) {
					player.setReturnCode(Constants.LOGIN_RESPONSE_INVALID_CREDENTIALS);
					player.sendLoginResponse();
					player.disconnect();
					return;
				}

				player.setUsername(Misc.capitalize(username));
				player.setTempPassword(password);
				player.setUsernameAsLong(NameUtility.nameToLong(player.getUsername().toLowerCase()));
				player.setLoginStage(LoginStages.AWAITING_LOGIN_COMPLETE);

				if (player.beginLogin() && player.getLoginStage() == LoginStages.AWAITING_LOGIN_COMPLETE) {
					// Switch the player to the cycled reactor.
					synchronized (DedicatedReactor.getInstance()) {
						DedicatedReactor.getInstance().getSelector().wakeup();
						player.getKey().interestOps(player.getKey().interestOps() & ~SelectionKey.OP_READ);
						player.getSocketChannel().register(Server.getSingleton().getSelector(), SelectionKey.OP_READ, player);
					}
				}
				break;
			case AWAITING_LOGIN_COMPLETE:
				break;
			case LOGGED_IN:
				break;
			case LOGGED_OUT:
				break;
			case LOGGING_OUT:
				break;
			default:
				break;
		}
	}
}
