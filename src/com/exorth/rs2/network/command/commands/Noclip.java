package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Noclip implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		//TODO: Fix
		if (player.getAttribute("noclip") == null) {
			player.setAttribute("noclip", 0);
			player.getActionSender().sendMessage("You have <col=008000>enabled</col> Noclip mode.");
		} else {
			player.removeAttribute("noclip");
			player.getActionSender().sendMessage("You have <col=CC0000>disabled</col> Noclip mode.");
		}
	}
}