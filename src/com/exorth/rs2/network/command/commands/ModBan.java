package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.content.Judgement;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class ModBan implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.PLAYER_MODERATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		String reason = "";
		if (parts.length < 2) {
			reason = "N/A";
		} else {
			reason = parts[1];
		}
		Judgement.ban(player, reason);
	}
}
