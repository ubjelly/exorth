package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class NpcPlayer implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int npcId = Integer.parseInt(parts[1]);
		player.setHumanToNpc(npcId);
		player.setAppearanceUpdateRequired(true);
		player.getActionSender().sendMessage("Morphed into NPC ID: <col=CC0000>" + npcId + "</col>. ::pnpc -1 to unmorph.");
	}
}