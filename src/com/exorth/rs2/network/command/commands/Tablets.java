package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Tablets implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		int[] tablets = { 8007, 8008, 8009, 8010, 8011, 8012, 8013, 8014, 8015, 8016, 8017, 8018, 8019, 8020, 8021, 8022 };
		for (int tab : tablets) {
			Item item = new Item(tab, 1);
			if (player.getInventory().hasRoomFor(item)) {
				player.getInventory().addItem(item);
			}
		}
	}
}