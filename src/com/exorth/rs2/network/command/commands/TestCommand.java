package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.content.minigame.hordedefence.Zombie;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.command.Command;

/**
 * This class is for no specific command but for testing purposes.
 * Use as needed.
 * @author Stephen
 */
public class TestCommand implements Command {

	/**
	 * Current use: Zombie minigame
	 */
	public void invoke(Player player, String command) {
		Zombie zombie = new Zombie(new Npc(73));
		zombie.spawnZombie();
	}
}
