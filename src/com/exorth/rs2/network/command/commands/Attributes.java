package com.exorth.rs2.network.command.commands;

import java.util.Map.Entry;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Attributes implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		player.getActionSender().sendMessage("<col=CC0000>Attributes</col>:");
		player.getActionSender().sendMessage("--------------------------");
		for (Entry<String, Object> attribute : player.getAttributes().entrySet()) {
			player.getActionSender().sendMessage("<col=CC0000>" + attribute.getKey() + "</col> : <col=CC0000>" + attribute.getValue() + "</col>");
		}
		player.getActionSender().sendMessage("--------------------------");
	}
}