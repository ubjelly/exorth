package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.Camera;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class CameraStill implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		if (parts.length == 6) {
			int x = Integer.parseInt(parts[1]);
			int y = Integer.parseInt(parts[2]);
			int velocity = Integer.parseInt(parts[3]);
			int angle = Integer.parseInt(parts[4]);
			int height = Integer.parseInt(parts[5]);
			Camera camera = new Camera(player);
			camera.move(x, y, velocity);
			camera.rotate(angle);
			camera.ascend(height);
			camera.update();
		} else {
			player.getActionSender().sendMessage("Wrong Parameters! Syntax is ::camstill x y velocity angle height.");
		}
	}
}