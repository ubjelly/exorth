package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.content.Judgement;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class ModMute implements Command {

	@SuppressWarnings("static-access")
	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.PLAYER_MODERATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		if (parts.length < 2) {
			player.getActionSender().sendMessage("Invalid Syntax! Syntax is ::mute player minutes.");
			return;
		}
		int mins = 0;
		if (parts.length < 3) {
			mins = 5;
		} else {
			Integer.parseInt(parts[2]);
		}
		String name = parts[1].toLowerCase().replace("-", " ");
		Player p2 = World.getInstance().getPlayerByName(name);
		if (p2 != null) {
			Judgement.silence(p2, (long) mins);
		}
	}
}
