package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Announce implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		String msg = "[Exorth]<col=CC0000> ";

		for (String part : parts) {
			if (part != parts[0]) {
				msg += part;
			}
		}

		for (Player p : World.getPlayers()) {
			if (p != null) {
				p.getActionSender().sendMessage(msg + "</col>");
			}
		}
	}
}