package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.command.Command;

public class PlayersOnline implements Command {

	public void invoke(Player player, String command) {
		int count = World.playerAmount();
		String prefix = "";
		String word = "";
		if (count > 1) {
			prefix = "are";
			word = "players";
		} else {
			prefix = "is";
			word = "player";
		}
		player.getActionSender().sendMessage("There " + prefix + " " + World.playerAmount() + " " + word + " online.");
	}
}
