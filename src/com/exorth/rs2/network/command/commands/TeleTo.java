package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class TeleTo implements Command {

	@SuppressWarnings("static-access")
	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.PLAYER_MODERATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		String name = parts[1].toLowerCase().replace("-", " ");
		Player p2 = World.getInstance().getPlayerByName(name);
		if (p2 != null) {
			player.teleport(p2.getPosition());
			player.getActionSender().sendMessage("You teleport to " + p2.getUsername() + ".");
		} else {
			player.getActionSender().sendMessage("Player '" + name + "' is not logged in.");
		}
	}
}
