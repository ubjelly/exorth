package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class TeleToMe implements Command {

	@SuppressWarnings("static-access")
	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.PLAYER_MODERATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		String name = parts[1].toLowerCase().replace("-", " ");
		Player p2 = World.getInstance().getPlayerByName(name);
		if (p2 != null) {
			p2.teleport(player.getPosition());
			player.getActionSender().sendMessage("You teleport " + p2.getUsername() + " to you.");
		} else {
			player.getActionSender().sendMessage("Player '" + name + "' is not logged in.");
		}
	}
}
