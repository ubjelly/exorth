package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.network.command.Command;

public class Tele implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int x = Integer.parseInt(parts[1]);
		int y = Integer.parseInt(parts[2]);
		int z = 0;
		if (parts.length > 3) {
			z = Integer.parseInt(parts[3]);
		}
		player.teleport(new Position(x, y, z));
	}
}