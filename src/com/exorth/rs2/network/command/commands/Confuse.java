package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;
import com.exorth.rs2.task.impl.RegainClarity;

/**
 *Confuses the player with the inability to walk or see anything (black screen), then they are permitted their visibility and freedom to walk.
 *
 * @author Mko
 *
 */
public class Confuse implements Command {
	
	@SuppressWarnings("static-access")
	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		String name = parts[1].toLowerCase().replace("-", " ");
		Player p2 = World.getInstance().getPlayerByName(name);
		if (p2 != null) {
			p2.getActionSender().sendMessage("You have been stricken by confusion!");
			//p2.getActionSender().sendWalkableInterface(13583);
			p2.getActionSender().sendInterface(13583);
			p2.setCanWalk(false);
			World.submit(new RegainClarity(p2));
		} else {
			player.getActionSender().sendMessage("Player '" + name + "' is not logged in.");
		}
	}
}