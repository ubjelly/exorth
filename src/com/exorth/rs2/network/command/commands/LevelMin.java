package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class LevelMin implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		int level = 1;
		for (int skill = 0; skill < 22; skill++) {
			if (skill == 3) { //Hitpoints
				player.getSkill().getLevel()[skill] = (byte) 10;
				player.getSkill().getExp()[skill] = player.getSkill().getXPForLevel(10);
			} else {
				player.getSkill().getLevel()[skill] = (byte) level;
				player.getSkill().getExp()[skill] = player.getSkill().getXPForLevel(level);
				player.getSkill().setPrayerPoints(level);
			}
		}
		player.getSkill().refresh();
	}
}