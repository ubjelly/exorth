package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class LevelSetSpecificAll implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int level = Integer.parseInt(parts[1]);
		if (level > 99 || level < 1) {
			player.getActionSender().sendMessage("Level must be between 1 and 99 (inclusive).");
			return;
		}
		for (int skill = 0; skill < 22; skill++) {
			player.getSkill().getLevel()[skill] = (byte) level;
			player.getSkill().getExp()[skill] = player.getSkill().getXPForLevel(level);
			player.getSkill().setPrayerPoints(level);
		}
		player.getSkill().refresh();
	}
}