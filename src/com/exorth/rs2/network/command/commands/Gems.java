package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Gems implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}
		//Uncut Diamond, Uncut Ruby, Uncut Emerald, Uncut Sapphire, Uncut Opal, Uncut Jade, Uncut Red Topaz, Uncut Dragonstone, Uncut Onyx
		int[] gems = { 1617, 1619, 1621, 1623, 1625, 1627, 1629, 1631, 6571 };
		for (int gem : gems) {
			player.getInventory().addItem(new Item(gem, 1));
		}
	}
}