package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class PickupItem implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int itemId = Integer.parseInt(parts[1]);
		int amt = 0;
		if (parts.length > 2) {
			amt = Integer.parseInt(parts[2]);
			if (amt > Item.MAX_AMOUNT) {
				amt = Item.MAX_AMOUNT;
			}
			Item item = new Item(itemId, amt);
			if (!player.getInventory().hasRoomFor(item) && !item.getDefinition().isStackable()) {
				amt = player.getInventory().getItemContainer().freeSlots();
			}
		} else {
			amt = 1;
		}
		Item item = new Item(itemId, amt);
		if (player.getInventory().addItem(item)) {
			player.getActionSender().sendMessage("You have successfully spawned <col=CC0000>" + amt + "</col> of Item (ID: <col=CC0000>" + itemId + "</col>, Name: <col=CC0000>" + item.getDefinition().getName() + "</col>).");
		}
	}
}