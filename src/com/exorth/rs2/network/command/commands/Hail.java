package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Hail implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		if (World.playerAmount() == 1) {
			player.getActionSender().sendMessage("There are no users online to hail you.");
			return;
		}
		for (Player other : World.getPlayers()) {
			if (other instanceof Player) {
				if (other != player) {
					other.getUpdateFlags().sendFaceToDirection(player.getPosition());
					other.getUpdateFlags().sendAnimation(1651);
					other.getUpdateFlags().sendForceMessage("ALL HAIL " + player.getUsername().toUpperCase() + "!");
				}
			}
		}
	}
}