package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Jewellery implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		Item[] rings = { new Item(1635) };
		Item[] necklaces = { new Item(1654) };
		Item[] amulets = { new Item(1673) };
		if (player.getInventory().getItemContainer().contains(1592)) {
			player.getActionSender().sendString("", 4230);
		}
		if (player.getInventory().getItemContainer().contains(1597)) {
			player.getActionSender().sendString("", 4236);
		}
		if (player.getInventory().getItemContainer().contains(1595)) {
			player.getActionSender().sendString("", 4242);
		}
		player.getActionSender().sendUpdateItems(4233, rings);
		player.getActionSender().sendUpdateItems(4239, necklaces);
		player.getActionSender().sendUpdateItems(4245, amulets);
		player.getActionSender().sendInterface(4161);
	}
}