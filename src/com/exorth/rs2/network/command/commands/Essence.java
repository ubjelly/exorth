package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Essence implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		//Fills the inventory's free slots with essence
		player.getInventory().addItem(new Item(7936, player.getInventory().getItemContainer().freeSlots()));
	}
}