package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class LevelSetSpecific implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int skillId = Integer.parseInt(parts[1]);
		int level = Integer.parseInt(parts[2]);
		if (skillId > 21 || skillId < 0) {
			player.getActionSender().sendMessage("Skill ID must be between 0 and 21 (inclusive).");
			return;
		}
		player.getSkill().getLevel()[skillId] = (byte) level;
		player.getSkill().getExp()[skillId] = player.getSkill().getXPForLevel(level);
		if (skillId == player.getSkill().PRAYER) {
			player.getSkill().setPrayerPoints(level);
		}
		player.getSkill().refresh();
	}
}