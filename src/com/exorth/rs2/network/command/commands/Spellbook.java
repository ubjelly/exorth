package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.content.combat.magic.Magic;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Player.MagicBookTypes;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Spellbook implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int spellbook = Integer.parseInt(parts[1]);
		if (spellbook == 0) {
			Magic.getInstance().changeSpellbook(player, MagicBookTypes.MODERN);
		} else if (spellbook == 1) {
			Magic.getInstance().changeSpellbook(player, MagicBookTypes.ANCIENT);
		} else if (spellbook == 2) {
			Magic.getInstance().changeSpellbook(player, MagicBookTypes.LUNAR);
		} else {
			player.getActionSender().sendMessage("Invalid Spellbook ID! Must be either 0, 1, or 2.");
		}
	}
}