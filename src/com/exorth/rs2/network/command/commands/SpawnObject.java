package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.object.CustomObject;
import com.exorth.rs2.model.players.GlobalObjectHandler;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;
import com.exorth.rs2.util.clip.ObjectDefinition;

public class SpawnObject implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int objectId = Integer.parseInt(parts[1]);
		int x = player.getPosition().getX();
		int y = player.getPosition().getY();
		int z = player.getPosition().getZ();
		GlobalObjectHandler.createGlobalObject(player, new CustomObject(objectId, new Position(x, y, z), 0, 10));
		player.getActionSender().sendMessage("Spawned Object ID: <col=CC0000>" + objectId + "</col>, Name: <col=CC0000>" + ObjectDefinition.forId(objectId).getName() + "</col>.");
	}
}