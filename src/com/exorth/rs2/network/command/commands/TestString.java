package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class TestString implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String msg = "";

		String[] parts = command.split(" ");
		for (int i = 1; i < parts.length; i++) {
			msg += parts[i] + " ";
		}
		player.getActionSender().sendMessage(msg);
	}
}
