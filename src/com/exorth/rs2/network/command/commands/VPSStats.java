package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class VPSStats implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		player.getActionSender().sendMessage(
			"<trans=150><shad><col=ccff00>" +
			"[VPS LOAD] [CPU COUNT: " + Runtime.getRuntime().availableProcessors() + "] " +
			"[RAM HEAP (Free/Total): " + Runtime.getRuntime().freeMemory() / 1000000 + "mb/" + Runtime.getRuntime().maxMemory() / 1000000 + "mb]" +
			"</col></shad></trans>"
		);
	}
}