package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Yell implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.PREMIUM.ordinal()) {
			player.getActionSender().sendMessage("You must be a Premium Member to use the Yell channel.");
			return;
		}
		int rights = player.getRights();
		String prefix = "";
		if (rights == 3) { //Player/Global Moderator
			prefix = "<img=0>";
		} else if (rights == 2) { //Forum Moderator
			//TODO: Forum Mod Crown
		} else if (rights == 4) { //Server Administrator
			prefix = "<img=2>";
		} else if (rights == 5) { //Administrator
			prefix = "<img=1>";
		}

		String msg = "[" + prefix + "" + player.getUsername() + "]: ";

		String[] parts = command.split(" ");
		for (int i = 1; i < parts.length; i++) {
			msg += parts[i] + " ";
		}

		for (Player x : World.getPlayers()) {
			if (x != null) {
				x.getActionSender().sendMessage(msg);
			}
		}
	}
}
