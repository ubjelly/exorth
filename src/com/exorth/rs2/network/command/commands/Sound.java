package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Sound implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int id = Integer.parseInt(parts[1]);
		player.getActionSender().sendSound(id, 100, 0);
		player.getActionSender().sendMessage("Played Sound ID: " + id + ".");
	}
}
