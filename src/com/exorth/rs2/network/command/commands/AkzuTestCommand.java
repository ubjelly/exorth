package com.exorth.rs2.network.command.commands;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.Iterator;

import com.exorth.rs2.Constants;
import com.exorth.rs2.Server;
import com.exorth.rs2.content.combat.Combat;
import com.exorth.rs2.content.combat.util.Bonuses.BonusDefinition;
import com.exorth.rs2.content.combat.util.CombatFormula;
import com.exorth.rs2.content.combat.util.Weapons.WeaponLoader;
import com.exorth.rs2.content.duel.Course;
import com.exorth.rs2.io.XStreamController;
import com.exorth.rs2.model.Palette;
import com.exorth.rs2.model.Palette.PaletteTile;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import java.util.Deque;
import java.util.logging.Logger;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Equipment;
import com.exorth.rs2.model.players.GlobalItemSpawns;
import com.exorth.rs2.model.players.GlobalObject;
import com.exorth.rs2.model.players.GroundItem;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Player.MagicBookTypes;
import com.exorth.rs2.model.players.container.Container;
import com.exorth.rs2.model.players.container.ContainerType;
import com.exorth.rs2.model.region.DynamicRegion;
import com.exorth.rs2.model.region.Region;
import com.exorth.rs2.model.region.Tile;
import com.exorth.rs2.network.command.Command;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.task.impl.PrayerDrainTask;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.clip.PathFinder;
import com.exorth.rs2.util.clip.RegionClipping;
import com.exorth.rs2.util.clip.TileControl;

@SuppressWarnings("unused")
public class AkzuTestCommand implements Command {

	// Method for writing equipment bonuses to binary data file for clients use to update bonuses to interface
	/*DataOutputStream d_out;
	try { 
		d_out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("./config/main_file_cache.dat3", true)));
		for (int i = 0; i < 13000; i++) {
			if (BonusDefinition.getBonus(i) != null) {
				d_out.writeByte(1);
				d_out.writeShort(i);
				for (int i2 = 0; i2 < 12; i2++) {
					d_out.writeShort(BonusDefinition.getBonus(i).getBonus(i2));
				}
			}
		}
		System.out.println("Dumped bonus definition file.");
		d_out.close();
	} catch (Exception d_ex) {
		d_ex.printStackTrace();
	}*/

	// Writes to .txt for debugging
	/*BufferedWriter b_out;
	try {
		b_out = new BufferedWriter(new FileWriter("./config/bonuses.txt", true));
		for (int i = 0; i < 13000; i++) {
			if (BonusDefinition.getBonus(i) != null) {
				b_out.write("1");
				b_out.newLine();
				b_out.write("" + i);
				b_out.newLine();
				for (int i2 = 0; i2 < 12; i2++) {
					b_out.write("" + BonusDefinition.getBonus(i).getBonus(i2));
					b_out.newLine();
				}
			}
		}
		System.out.println("Dumped bonus definition file.");
		b_out.close();
	} catch (Exception b_ex) {
		b_ex.printStackTrace();
	}*/

	@Override
	public void invoke(final Player player, String command) {
		if (!player.getUsername().equals("Akzu")) {
			return;
		}

		/**
		 * 237 random?
		 * 330 misc opening sounds
		 * 334 potion drink
		 * 356 pick?
		 * 390 specs
		 * 400 wep sounds
		 * 435 prayer sounds
		 * 458 stun
		 * 466 ???
		 * 481 runecraft
		 * 661 ???
		 * 790 weapon wielding sounds
		 * 810 block sounds
		 * 820-900 slayer npc sounds
		 * 893 open door?
		 * 996 mage/pray sounds
		 * 1056 some wep sounds
		 * 1174 rubber chicken
		 * 1210 potion drank
		 * 1230 mining
		 * 1241 picklock?
		 * 1253 bankpin?
		 * 1252 wrong pin
		 * 1257 correct pin!
		 * 1262 some bank pin stuff
		 */

		/*
		String[] parts = command.split(" ");
		player.getActionSender().createNpcHints(2530);
		player.getActionSender().sendInterface(8134);

		DataOutputStream out;
		try {
			out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream("./config/equipment_bonuses.dat", true)));
			for (int i = 0; i < 13000; i++) {
				if (BonusDefinition.getBonus(i) != null) {
					out.writeShort(i);
					for (int i2 = 0; i2 < 12; i2++) {
						out.writeShort(BonusDefinition.getBonus(i).getBonus(i2));
					}
				}
			}
			System.out.println("Dumped bonus definition file."); out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Position[] pos = new Position[1000];
		Region newRegion = World.getInstance().getRegionManager().getRegionByLocation(player.getPosition());
		for (Npc other : World.getInstance().getRegionManager().getLocalNpcs(player)) {
			if (other.getAttribute("NpcDecoy") != null) {
				System.err.println(other.getDefinition().getSize());
			}
		}
		Region[] reg = World.getInstance().getRegionManager().getSurroundingRegions(player.getPosition());
		Item whip = new Item(4151);
		for (int x = -100; x < 100; x++) {
			for (int y = -100; y < 100; y++) {
				ItemManager.getInstance().createGroundItem(
					player, "Akzu", whip, new Position(player.getPosition().getX() + x, player.getPosition().getY() + y
				), 350);
			}
		}

		System.err.println(new Position(reg[k].getCoordinates().getX() * 32, reg[k].getCoordinates().getY() * 32));

		Npc[] npcs = new Npc[12];
		int loop = 0;
		for (Npc npc : World.getInstance().getRegionManager().getLocalNpcs(player)) {
			if (npc.getAttribute("NpcDecoy") != null) {
				npcs[loop] = npc; loop++;
			}
		}
		npcs[0].setFollowingEntity(npcs[1]);
		npcs[0].setTarget(npcs[1]);
		npcs[0].setInstigatingAttack(true);
		Combat.getInstance().attackEntity(npcs[0], npcs[1]);

		player.clipTeleport(new Position(3214, 3434, 0), 0, 2, 1);
		player.addToRegion(newRegion);
		System.err.println(newRegion + " " + newRegion.getMobs());
		player.getActionSender().sendInterface(17537);

		try {
			XStreamController.loadAllFiles();
		} catch (Exception e) {
			e.printStackTrace();
		}

		int shift = (1 << 8) + 65536; //If you want to test you could use 504
		player.getActionSender().sendConfig(504, shift);
		System.err.println((shift));

		System.err.println(RegionClipping.getRSObject(3593, 3502,0).getId());
		player.getActionSender().sendChatInterface(2400);

		player.getActionSender().sendString("100 Quest Points\\n87,5k Magic XP\\nAmulet Of Ownage", 4444);
		player.getActionSender().sendString("You have completed the Test Quest!", 301);
		player.getActionSender().sendConfig(101, 304);
		player.getActionSender().sendInterface(297);

		WalkInterfaces.addWalkableInterfaces(player);
		int id = Integer.parseInt(parts[1]);
		player.getActionSender().createPlayerHints(World.getPlayerByName("Akzu2").getIndex());

		World.submit(new Task(2, true) { byte shouts = 3;
			@Override
			protected void execute() {
				if (shouts == 0) {
					player.getUpdateFlags().sendForceMessage("FIGHT!");
					this.stop();
					return;
				}
				player.getUpdateFlags().sendForceMessage("" + shouts);
				shouts--;
			}
		});

		player.getActionSender().sendItemOnInterface(2405, 160, 2361);
		player.getActionSender().sendChatInterface(2400);
		player.getMagic().changeSpellbook(MagicBookTypes.ANCIENT);
		player.getActionSender().sendNPCDialogueHead(17, 4883);
		player.getActionSender().sendChatInterface(4882);

		player.getActionSender().sendPlayerDialogueHead(969);
		player.getActionSender().sendChatInterface(968);
		player.getActionSender().sendChatInterface(id);
		player.getSkill().increaseLevel(2, 6);
		player.getSkill().raiseLevelOnce(2, 2);
		player.setClickId(3000);
		player.getActionSender().sendConfig(948, 5);
		player.getSkill().decreaseLevelOnce(2, 2);

		player.getBank().add(new Item(id, 10000));
		BankManager.openBank(player);

		//Start the player skill equalizer task.
		World.submit(new Task(2) { int id = 1260;
			@Override
			protected void execute() {
				Logger.getAnonymousLogger().info(id);
				player.getActionSender().sendSound(id, 0, 0);
				stop();
				id++;
			}
		});
		player.getActionSender().sendSound(id, 0, 0);

		if (player.getMagicBookType() == MagicBookTypes.MODERN) {
			player.getMagic().changeSpellbook(MagicBookTypes.ANCIENT);
		} else {
			player.getMagic().changeSpellbook(MagicBookTypes.MODERN);
		}

		for (int i = 0; i < 21; i++) {
			player.getSkill().getLevel()[i] = 99;
		}
		player.getSkill().refresh();

		try {
			XStreamUtil.loadAllFiles();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Logger.getAnonymousLogger().info(StaffData.staffUsed(1381));
		player.getActionSender().sendCoords8x8();
		player.getBankPin().changeBankPin();
		
		BufferedWriter bw = null;
		BufferedReader bw2 = null;
		BufferedReader bw3 = null;
		try {
			bw = new BufferedWriter(new FileWriter(Constants.CONFIG_DIRECTORY + "content/itemdefs.txt", true));
			bw2 = new BufferedReader(new FileReader(Constants.CONFIG_DIRECTORY + "content/alchprices.txt"));
			bw3 = new BufferedReader(new FileReader( Constants.CONFIG_DIRECTORY + "content/weights.xml"));
		
			int noteId[] = new int[11791];
			int highAlch[] = new int[11791];
			int lowAlch[] = new int[11791];
			double weight[] = new double[11791];
			int alchId = 0;
			int weightId = 0;
			String line = null;
			String line2 = null;

			for (int i = 0; i < Constants.MAX_ITEMS; i++) {
				if (ItemManager.getInstance().getItemDefinitions()[i] .getParentId() > -1) {
					noteId[ItemManager.getInstance().getItemDefinitions()[i].getParentId()] = i;
				} 
			}

			while ((line = bw2.readLine()) != null) {
				if (line.contains("		<itemId>")) {
					alchId = Integer.parseInt(line.substring(line.indexOf("<itemId>") + 8, line.indexOf("</itemId>")));
				}
				if (line.contains("		<highAlch>")) {
					highAlch[alchId] = Integer.parseInt(line.substring(line.indexOf("<highAlch>") + 10, line.indexOf("</highAlch>")));
				}
				if (line.contains("		<lowAlch>")) {
					lowAlch[alchId] = Integer.parseInt(line.substring( line.indexOf("<lowAlch>") + 9, line.indexOf("</lowAlch>")));
				}
			}

			while ((line2 = bw3.readLine()) != null) {
				if (line2.contains("		<itemId>")) {
					weightId = Integer.parseInt(line2.substring(line2.indexOf("<itemId>") + 8, line2.indexOf("</itemId>")));
				}
				if (line2.contains("		<kg>")) {
					weight[weightId] = Double.parseDouble(line2.substring(line2.indexOf("<kg>") + 4, line2.indexOf("</kg>")));
				}
			}

			for (int i = 0; i < Constants.MAX_ITEMS; i++) {
				bw.write("	<itemDef>");
				bw.newLine();
				bw.write("		<id>" + i +"</id>");
				bw.newLine();
				bw.write("		<name>" + ItemManager.getInstance().getItemDefinitions()[i] .getName() + "</name>");
				bw.newLine();
			
				if (ItemManager.getInstance().getItemDefinitions()[i].getHighAlchValue() > 1 && !ItemManager.getInstance().getItemDefinitions()[i].isNote()) {
					bw.write("		<highAlchValue>" + ItemManager.getInstance().getItemDefinitions()[i].getHighAlchValue() + "</highAlchValue>");
					bw.newLine();
				}
				if (ItemManager.getInstance().getItemDefinitions()[i].getLowAlchValue() > 1 && !ItemManager.getInstance().getItemDefinitions()[i].isNote()) {
					bw.write("		<lowAlchValue>" + ItemManager.getInstance().getItemDefinitions()[i].getLowAlchValue() + "</lowAlchValue>");
					bw.newLine();
				}
				if (ItemManager.getInstance().getItemDefinitions()[i].getWeight() > 0.0 && !ItemManager.getInstance().getItemDefinitions()[i] .isNote()) {
					bw.write("		<weight>" + ItemManager.getInstance().getItemDefinitions()[i].getWeight() + "</weight>");
					bw.newLine();
				}
				if (ItemManager.getInstance().getItemDefinitions()[i] .isStackable()) {
					bw.write("		<stackable>" + ItemManager.getInstance().getItemDefinitions()[i] .isStackable() + "</stackable>");
					bw.newLine();
				}
				if (ItemManager.getInstance().getItemDefinitions()[i].isNote()) {
					bw.write("		<note>true</note>");
					bw.newLine();
				} if (ItemManager.getInstance().getItemDefinitions()[i] .getParentId() > 0) {
					bw.write("		<parentId>" + ItemManager.getInstance().getItemDefinitions()[i] .getParentId() + "</parentId>");
					bw.newLine();
				} if (ItemManager.getInstance().getItemDefinitions()[i].getNoteId() > 0) {
					bw.write("		<noteId>" + ItemManager.getInstance().getItemDefinitions()[i].getNoteId() + "</noteId>");
					bw.newLine();
				}
				bw.write("	</itemDef>");
				bw.newLine();
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		player.hit(30, 1, false);

		// TODO: Spawn nice bank with this loop
		Player plr = World.getPlayerByName("Scarpet");
		if (plr != null) {
			player.getActionSender().sendMessage(plr.getPosition().toString());
		}

		player.getActionSender().sendInterface(3559);
		player.getBank().clear(); for (int kk = 700; kk < 8360; kk++) {
			Item item = new Item(kk, 100);
			if (item.getDefinition().getName().startsWith("null")) {
				continue;
			}
			if (item.getDefinition().getName().startsWith("Whoopsie")) {
				continue;
			}
			if (item.getDefinition().isNote()) {
				item = new Item(item.getDefinition().getParentId(), 100);
			} else {
				new Item(kk, 100);
			}
			player.getBank().add(item);
		}
		BankManager.openBank(player);

		player.getUpdateFlags().sendAnimation(6132, 30);
		player.getUpdateFlags().sendFaceToDirection(new
		Position(player.getPosition().getX(), player.getPosition().getY() + 1));
		World.submit(new Task(1) {
			@Override
			protected void execute() {
				player.getUpdateFlags().sendForceMovement(); stop();
			}
		});
		World.submit(new Task(2) {
			@Override
			protected void execute() {
				player.teleport(new Position(3090, 3523));
				//player.getUpdateFlags().sendFaceToDirection(new Position(player.getPosition().getX(), player.getPosition().getY() - 1));
				stop();
			}
		});
		
		player.getActionSender().sendSidebarInterface(6, 12855);
		String[] parts = command.split(" ");
		int id = Integer.parseInt(parts[1]);
		player.setHumanToNpc(id);
		player.setAppearanceUpdateRequired(true);

		player.getSkill().addExperience(5, 3660);
		PathFinder.getSingleton().moveTo(player, 3170, 3420);
		player.getActionSender().sendConfig(960, id);
		BankManager.openBank(player);

		int x = 0;
		if (player.getPosition().getX() == 3087) {
			x = 6; 
		} else { 
			x = 0;
		}
		PathFinder.getSingleton().moveTo(player, 3087+x, 3504);
		Player plr = World.getPlayerByName("Akzu3");
		PathFinder.getSingleton().moveTo(plr, 3087 + x, 3505);

		World.submit(new Task(3, true) {
			int kk = 11287;
			@Override
			protected void execute() {
				Logger.getAnonymousLogger().info("Item ID: " + kk);
				if (kk > Constants.MAX_ITEMS) {
					stop(); return;
				}
				RuneWiki.getWeight(kk);
				kk++;
				stop();
			}
		});
		Logger.getAnonymousLogger().info(player.getCurrentRegion().toString());
		player.getMovementHandler().setRunToggled(!player.getMovementHandler().isRunToggled());
		Skillcapes.doSkillcape(player);

		player.getInventory().addItem(new Item(4151, 1));
		player.getInventory().addItem(new Item(5698, 1));
		player.getInventory().addItem(new Item(6570, 1));
		player.getInventory().addItem(new Item(10551, 1));
		player.getInventory().addItem(new Item(1079, 1));
		player.getInventory().addItem(new Item(6737, 1));
		player.getInventory().addItem(new Item(4131, 1));
		player.getInventory().addItem(new Item(7462, 1));
		player.getInventory().addItem(new Item(8850, 1));
		player.getInventory().addItem(new Item(3751, 1));
		player.getInventory().addItem(new Item(6585, 1));
		player.getInventory().addItem(new Item(2440, 1));
		player.getInventory().addItem(new Item(2436, 1));
		player.getInventory().addItem(new Item(2434, 2));
		player.getInventory().addItem(new Item(386, 100));

		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(Constants.CONFIG_DIRECTORY + "FloorMaps.txt"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String line = null;
		int[] x = new int[1500];
		int[] y = new int[1500];
		int index = 0;

		try {
			while ((line = bufferedReader.readLine()) != null) {
				x[index] = Integer.parseInt(line.substring(line.indexOf("X") + 1, line.indexOf("Y")));
				y[index] = Integer.parseInt(line.substring(line.indexOf("Y") + 1));
				index++;
			}
			final int[] x1 = x;
			final int[] y1 = y;

			World.submit(new Task(5) {
				int count = 0;
				@Override
				protected void execute() {
					player.teleport(new Position(x1[count], y1[count], 0));
					count++;
					Logger.getAnonymousLogger().info(count);
				}
			});
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(Constants.CONFIG_DIRECTORY + "dumpedbonuses.dat")));
			while (in.readShort() == 4151) {
				for (int i2 = 0; i2 < 12; i2++) {
					Logger.getAnonymousLogger().info(in.readShort());
					in.close();
				}
			}
			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(Constants.CONFIG_DIRECTORY + "main_file_cache.dat3", true)));
			for (int i = 0; i < 12000; i++) {
				if (BonusDefinition.getBonus(i) != null) {
					out.writeByte(1);
					out.writeShort(i);
					for (int i2 = 0; i2 < 12; i2++) {
						out.writeShort(BonusDefinition.getBonus(i).getBonus(i2));
					}
					System.out.println("Settings successfully saved.");
				}
			}
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int id = Integer.parseInt(parts[1]);
		World.submit(new Task(1, true) {
			int id = 700;
			@Override
			protected void execute() {
				player.getActionSender().animateObject(player.getClickX(), player.getClickY(), player.getPosition().getZ(), id);
				id++;
				Logger.getAnonymousLogger().info(id);
				stop();
			}
		});

		player.getUpdateFlags().sendFaceToDirection(new Position(2715, 3472, 1));
		player.getActionSender().sendMessage("" + CombatFormula.getMeleeMaxHit(player, false));
		BankManager.openBank(player);
		player.teleport(new Position(player.getPosition().getX(), player.getPosition().getY(), id));
		Logger.getAnonymousLogger().info(Region.blockedSouthEast(3245, 3211, 0, false));
		Logger.getAnonymousLogger().info(Region.getClipping(player.getPosition().getX(), player.getPosition().getY(), 0) & 0x20000);
		Logger.getAnonymousLogger().info(player.getBonuses());
		player.setBonuses(3, 1, BonusDefinition.getBonus(3751).getBonus(1));
		List<Integer> myValues = (List<Integer>) player.getBonuses().get(3);
		System.out.println(player.getBonuses().get(3));
		System.out.println(player.getEquipment().get(Constants.EQUIPMENT_SLOT_ARROWS));
		player.getActionSender().sendProjectile(player.getPosition(), 3, 3, 34, 43, 31, 96, -(player.getIndex() - 1), 51, 16, 64, 50);
		System.out.println(SpecialAttackDataLoader.SpecialAttackLoader.getSpecialAttack(4151).getBarId());

		player.getActionSender().sendMapRegion();

		player.getActionSender().sendInterface(11146);

		// Visualize an area
		Palette p = new Palette();
		// p.setTile(6, 6, 0, new PaletteTile(3232, 3232, 0, 0));
		p.setTile(6, 6, 0, new PaletteTile(3232, 3432, 0, 2));
		p.setTile(5, 6, 0, new PaletteTile(3232, 3432, 0, 0));
		DynamicRegion.getInstance().create(player, p);

		int regionX = 3968 >> 3;
		int regionY = 3968 >> 3;
		int regionId = ((regionX / 8) << 8) + (regionY / 8);
		for (int z = 0; z < 4; z++) {
			for (int x = 0; x < 13; x++) {
				for (int y = 0; y < 13; y++) {
					p.setTile(x, y, 0, new PaletteTile(3100, 3500, 0, 0));
					PaletteTile tile = p.getTile(x, y, z);
					if (z == 0 && x < 8 && y < 8) {
						RegionClipping.addClipping(4000 + x, 4000 + y, z, RegionClipping.getClipping(3223 + x, 3225 + y, z));
						System.err.println(RegionClipping.getClipping(3223 + x, 3225 + y, z));
					}
				}
			}
		}

		player.teleport(new Position(4000, 4000, 0));
		player.setAttribute("2nd_map_region_mode", 0);
		player.getActionSender().sendConstructMapRegion(p);

		for (int x = 0; x < 8; x++) {
			for (int y = 0; y < 8; y++) {
				player.getActionSender().sendObject(new GlobalObject(1, new Position(4000 + x, 4000 + y), 0, 10));
			}
		}

		player.setLoggingIn(false);
		Poison.appendPoison(player, true, 6);
		player.applyLife();

		player.getSkill().setPrayerPoints(99);
		player.setPrayerDrainTask(new PrayerDrainTask(player));
		World.submit(player.getPrayerDrainTask());
		player.getActionSender().sendUpdateItem(1, 3214, new Item(-1, 0));
		player.sendTeleport(player.getPosition().getX(),
		player.getPosition().getY(), id);
		player.setSpecialAmount(10);
		int weapon = player.getEquipment().getItemContainer().get(3).getId();
		SpecialAttackData spec = SpecialAttackLoader.getSpecialAttack(weapon);
		player.getActionSender().updateSpecialAmount(spec.getBarId());

		// 21 June 2006 (Update)
		Server.getScheduler().submit(new Task(3) {
			int id = 4200;
			@Override
			protected void execute() {
				if (!player.isLoggedIn()) {
					stop();
					return;
				}
				if (id > 4400) {
					stop(); return;
				}
				player.getUpdateFlags().sendAnimation(id, 0);
				player.getActionSender().sendMessage("" + id);
				id += 1;
			}
		});
		
		player.getActionSender().sendConfig(id, 1);
		player.getActionSender().sendConfig(172, id);
		int xp = Integer.parseInt(parts[2]);
		player.getActionSender().sendSound(id, 1, 0);
		System.out.println(CombatFormula.getRangedMaxHit(player));
		player.setMagicBookType(MagicBookTypes.ANCIENT);
		player.getSkill().addExperience(id, xp);
		player.sendTeleport(player.getPosition().getX(),
		player.getPosition().getY(), id);
		ItemManager.getInstance().loadOnHeight(player);
		System.out.println(GlobalItemSpawns.getInstance().getTotalSpawns());
		ItemManager.getInstance().spawnGroundItems();
		ItemManager.getInstance().createRespawnGroundItem(new Item(995, 20000000), new Position(3232,3232,0));
		System.out.println(WeaponLoader.getWeapon(4151).getSound(1));

		player.getInventory().addItem(new Item(Equipment.PLATEBODY[Misc.random (Equipment.PLATEBODY.length)]));
		player.getInventory().addItem(new Item(Equipment.AMULETS[Misc.random(Equipment.AMULETS.length)]));
		player.getInventory().addItem(new Item(Equipment.BODY[Misc.random(Equipment.BODY.length)]));
		player.getInventory().addItem(new Item(Equipment.BOOTS[Misc.random(Equipment.BOOTS.length)]));
		player.getInventory().addItem(new Item(Equipment.CAPES[Misc.random(Equipment.CAPES.length)]));
		player.getInventory().addItem(new Item(Equipment.HELM_NO_HAIR[Misc.random(Equipment.HELM_NO_HAIR.length)]));
		player.getInventory().addItem(new Item (Equipment.FULL_MASK[Misc.random(Equipment.FULL_MASK.length)]));
		player.getInventory().addItem(new Item(Equipment.GLOVES[Misc.random(Equipment.GLOVES.length)]));
		player.getInventory().addItem(new Item(Equipment.HATS[Misc.random(Equipment.HATS.length)]));
		player.getInventory().addItem(new Item(Equipment.LEGS[Misc.random(Equipment.LEGS.length)]));
		player.getInventory().addItem(new Item(Equipment.RINGS[Misc.random(Equipment.RINGS.length)]));
		player.getInventory().addItem(new Item(Equipment.SHIELDS[Misc.random(Equipment.SHIELDS.length)]));
		*/
	}
}
