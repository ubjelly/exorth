package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Anim implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int animId = Integer.parseInt(parts[1]);
		player.getUpdateFlags().sendAnimation(animId, 0);
		player.getActionSender().sendMessage("Played Animation ID: <col=CC0000>" + animId + "</col>. ::anim -1 to stop.");
	}
}