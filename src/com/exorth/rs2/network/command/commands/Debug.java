package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Debug implements Command {
	
	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		if (player.getAttribute("isDebugging") == null) {
			player.setAttribute("isDebugging", true);
			player.getActionSender().sendMessage("You have <col=008000>enabled</col> Debug mode.");
		} else {
			player.removeAttribute("isDebugging");
			player.getActionSender().sendMessage("You have <col=CC0000>disabled</col> Debug mode.");
		}
	}
}