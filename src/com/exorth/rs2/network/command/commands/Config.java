package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Config implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int configId = 0;
		int configValue = 0;
		if (parts.length < 3) {
			player.getActionSender().sendMessage("Wrong Parameters! Syntax is ::config configId configValue");
			return;
		} else {
			configId = Integer.parseInt(parts[1]);
			configValue = Integer.parseInt(parts[2]);
		}

		player.getActionSender().sendConfig(configId, configValue);
		player.getActionSender().sendMessage("Set Config ID " + configId + " to " + configValue + ".");
	}
}