package com.exorth.rs2.network.command.commands;

import java.io.FileNotFoundException;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.npcs.NpcLoader;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class ReloadContent implements Command {

	@Override
	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.ADMINISTRATOR.ordinal()) {
			return;
		}

		// First unregistering all npcs from world so they wont get doubled.
		for (Npc npc : World.getNpcs()) {
			if (npc != null) {
				npc.setAttribute("invisible", 0);
				World.unregister(npc);
			}
		}

		// Then reloading the data.
		try {
			NpcLoader.loadDefinitions();
			NpcLoader.loadSpawns();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		player.getActionSender().sendMessage("Content reloaded successfully.");
	}
}
