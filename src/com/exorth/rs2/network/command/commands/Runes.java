package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class Runes implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		int[] runes = { 554, 555, 556, 557, 558, 559, 560, 561, 562, 563, 564, 565, 566 };
		for (int runeId : runes) {
			Item rune = new Item(runeId, 100000);
			player.getInventory().addItem(rune);
		}
	}
}