package com.exorth.rs2.network.command.commands;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Rights;
import com.exorth.rs2.network.command.Command;

public class InterfaceWalking implements Command {

	public void invoke(Player player, String command) {
		if (player.getRights() < Rights.SERVER_ADMINISTRATOR.ordinal()) {
			return;
		}

		String[] parts = command.split(" ");
		int interfaceId = Integer.parseInt(parts[1]);
		//TODO: Make it so if invalid packet is sent error is displayed and player isn't disconnected
		if (player.getActionSender().sendWalkableInterface(interfaceId) == null) {
			player.getActionSender().sendMessage("Error while processing command.");
		} else {
			player.getActionSender().sendMessage("Opened Interface ID: <col=CC0000>" + interfaceId + "</col>. ::cinter to close.");
			player.getActionSender().sendWalkableInterface(interfaceId);
		}
	}
}