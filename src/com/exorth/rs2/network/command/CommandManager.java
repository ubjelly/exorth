package com.exorth.rs2.network.command;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.command.commands.Anim;
import com.exorth.rs2.network.command.commands.Announce;
import com.exorth.rs2.network.command.commands.Attributes;
import com.exorth.rs2.network.command.commands.Bank;
import com.exorth.rs2.network.command.commands.CameraReset;
import com.exorth.rs2.network.command.commands.CameraSpin;
import com.exorth.rs2.network.command.commands.CameraStill;
import com.exorth.rs2.network.command.commands.CharacterRedesign;
import com.exorth.rs2.network.command.commands.Config;
import com.exorth.rs2.network.command.commands.Confuse;
import com.exorth.rs2.network.command.commands.Debug;
import com.exorth.rs2.network.command.commands.Empty;
import com.exorth.rs2.network.command.commands.Essence;
import com.exorth.rs2.network.command.commands.GFX;
import com.exorth.rs2.network.command.commands.Gems;
import com.exorth.rs2.network.command.commands.Hail;
import com.exorth.rs2.network.command.commands.InteractingEntity;
import com.exorth.rs2.network.command.commands.Interface;
import com.exorth.rs2.network.command.commands.InterfaceClear;
import com.exorth.rs2.network.command.commands.InterfaceWalking;
import com.exorth.rs2.network.command.commands.Jewellery;
import com.exorth.rs2.network.command.commands.LevelMax;
import com.exorth.rs2.network.command.commands.LevelMin;
import com.exorth.rs2.network.command.commands.LevelSetSpecific;
import com.exorth.rs2.network.command.commands.LevelSetSpecificAll;
import com.exorth.rs2.network.command.commands.Noclip;
import com.exorth.rs2.network.command.commands.NpcDecoy;
import com.exorth.rs2.network.command.commands.NpcPlayer;
import com.exorth.rs2.network.command.commands.PickupItem;
import com.exorth.rs2.network.command.commands.PlayersOnline;
import com.exorth.rs2.network.command.commands.Position;
import com.exorth.rs2.network.command.commands.RestoreRunEnergy;
import com.exorth.rs2.network.command.commands.RestoreSpecialAttack;
import com.exorth.rs2.network.command.commands.Runes;
import com.exorth.rs2.network.command.commands.Song;
import com.exorth.rs2.network.command.commands.Sound;
import com.exorth.rs2.network.command.commands.SpawnObject;
import com.exorth.rs2.network.command.commands.Spellbook;
import com.exorth.rs2.network.command.commands.Tablets;
import com.exorth.rs2.network.command.commands.Tele;
import com.exorth.rs2.network.command.commands.TeleTo;
import com.exorth.rs2.network.command.commands.TeleToMe;
import com.exorth.rs2.network.command.commands.TestCommand;
import com.exorth.rs2.network.command.commands.TestString;
import com.exorth.rs2.network.command.commands.Update;
import com.exorth.rs2.network.command.commands.VPSStats;
import com.exorth.rs2.network.command.commands.Yell;
//import com.exorth.rs2.network.command.commands.ModBan;
//import com.exorth.rs2.network.command.commands.ModMute;

/**
 * Allows the user to execute commands that were developed in Java {@code Command} objects.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class CommandManager {

	/**
	 * Represents an invokable command.
	 * 
	 * @author Joshua Barry <Sneakyhearts>
	 * 
	 */
	private enum Invokable {

		//Begin All Players
		REDESIGN("redesign", new CharacterRedesign()), //Character Redesign
		PLAYERS("players", new PlayersOnline()), //Not needed with the new client update
		TEST("test", new TestCommand()),
		//End All Players
		//Begin Premium
		YELL("yell", new Yell()), // Yell
		//End Premium
		//Begin Moderators
		TELETO("teleto", new TeleTo()), //Teleport invoker to a Player
		TELETOME("teletome", new TeleToMe()), //Teleport a Player to invoker
		//MUTE("mute", new ModMute()), //Mutes a player. TODO: Integrate with Website or make Muting only thru website
		//BAN("ban", new ModBan()), //Bans a player. TODO: Integrate with Website or make Banning only thru website
		//End Moderators
		//Administrators
		POSITION("pos", new Position()), //Display a Player's current Position
		SOUND("sound", new Sound()), //Plays a Sound
		CONFUSE("confuse", new Confuse()), //Confuses a Player
		DEBUG("debug", new Debug()), //Toggles Debugging
		EMPTY("empty", new Empty()), //Empties a Player's inventory
		SPELLBOOK("spellbook", new Spellbook()), //Switches a Player's spellbook interface
		TELE("tele", new Tele()), //Teleports to coordinates
		ITEM("item", new PickupItem()), //Spawns an Item
		GEMS("gems", new Gems()), //Spawns Gems
		SONG("song", new Song()), //Plays a Song
		ANIMATION("anim", new Anim()), //Performs an Animation
		BANK("bank", new Bank()), //Opens a Player's bank
		SPEC("spec", new RestoreSpecialAttack()), //Restore a Player's special attack
		GFX("gfx", new GFX()), //Invokes a Graphic
		ENERGY("energy", new RestoreRunEnergy()), //Restore Run Energy
		INTERACTING_ENTITY("ie", new InteractingEntity()), //Returns the Entity interacting with the Player (if there is one)
		INTERFACE("inter", new Interface()), //Displays an Interface
		WALKING_INTERFACE("winter", new InterfaceWalking()), //Displays a Walking Interface
		CLEAR_INTERFACE("cinter", new InterfaceClear()), //Clears any open Interfaces
		CONFIG("config", new Config()), //Modifies a Config
		JEWELLERY("jewellery", new Jewellery()), //Opens a Jewellery Interface
		ESSENCE("ess", new Essence()), //Spawns Pure Essence in a Player's open inventory slots
		TABLETS("tablets", new Tablets()), //Spawns all the Tablets
		RUNES("runes", new Runes()), //Spawns 100k of each Rune
		ANNOUNCE("announce", new Announce()), //Broadcasts a Server-wide Announcement
		NOCLIP("noclip", new Noclip()), //Toggle Noclip
		CAMERA_STILL("camstill", new CameraStill()), //Moves camera to a specified x y, with velocity, angle, and height
		CAMERA_RESET("camreset", new CameraReset()), //Resets the Player's camera
		CAMERA_SPIN("camspin", new CameraSpin()), //Spins the Player's camera to a specified x y, with height speed and angle
		HAIL("hail", new Hail()), //Hails a Player
		PLAYER_NPC("pnpc", new NpcPlayer()), //Changes a Player into a NPC
		ATTRIBUTES("attributes", new Attributes()), //Displays a list of the Player's Attributes
		VPSSTATISTICS("vps", new VPSStats()), //Displays VPS Statistics
		LEVEL_SPECIFIC("level", new LevelSetSpecific()), //Sets a specific skill to a specific level
		LEVEL_SPECIFIC_ALL("xlevel", new LevelSetSpecificAll()), //Sets all skills to a specific level
		LEVEL_MAX("levelmax", new LevelMax()), //"Max" all a Player's skill levels
		LEVEL_MIN("levelmin", new LevelMin()), //"Min" all a Player's skill levels
		TEST_STRING("teststring", new TestString()), //Outputs a string as if it is a game message (useful for testing messages)
		SPAWN_OBJECT("obj", new SpawnObject()), //Spawn an Object
		NPC("npc", new NpcDecoy()), //Spawn a NPC
		//AKZU("akzu", new AkzuTestCommand()), // Akzu Command
		//RELOAD("xreload", new ReloadContent()), //Reloads all reloadable content
		UPDATE("update", new Update()); //Updates the game
		//End Administrators

		/**
		 * The string used to identify the command.
		 */
		private String identifier;

		/**
		 * The command to execute.
		 */
		private Command command;

		/**
		 * 
		 * @param identifier A string used to identify the command.
		 * @param command The command that will be invoked.
		 */
		private Invokable(String identifier, Command command) {
			this.identifier = identifier;
			this.command = command;
		}

		public String getIdentifier() {
			return identifier;
		}

		public Command getCommand() {
			return command;
		}

		/**
		 * 
		 * @param name The name of the command
		 * @return invokable The invokable command, if one is found.
		 */
		public static Invokable forIdentifier(String name) {
			for (Invokable invokable : Invokable.values()) {
				if (name.equalsIgnoreCase(invokable.getIdentifier())) {
					return invokable;
				}
			}
			return null;
		}
	}

	/**
	 * Executes the command.
	 * 
	 * @param player
	 * @param command
	 */
	public static void invoke(Player player, String command) {
		String name = " ";
		String[] args = {};
		if (command.indexOf(' ') > -1) {
			command.replaceAll(":", "");
			args = command.split(" ");
			name = args[0].toLowerCase();
		} else {
			name = command.toLowerCase();
		}
		if (name.length() == 0) {
			player.getActionSender().sendMessage("The command you have entered does not exist.");
			return;
		} else {
			Invokable invokable = Invokable.forIdentifier(name);
			if (invokable != null) {
				try {
					invokable.getCommand().invoke(player, command);
				} catch (Exception ex) {
					//ex.printStackTrace();
					player.getActionSender().sendMessage("Error while processing command.");
				}
			} else {
				player.getActionSender().sendMessage("The command you have entered does not exist.");
			}
		}
	}
}
