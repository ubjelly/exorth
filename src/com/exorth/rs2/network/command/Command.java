package com.exorth.rs2.network.command;

import com.exorth.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public interface Command {

	public void invoke(Player player, String command);

}