package com.exorth.rs2;

/*
 * This file is part of RuneSource.
 *
 * RuneSource is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RuneSource is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RuneSource. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.concurrent.ConcurrentHashMap;

import com.exorth.rs2.util.Misc.Stopwatch;

/**
 * A static gateway type class that is used to limit the maximum amount of
 * connections per host.
 * 
 * @author blakeman8192
 */
public class HostGateway {

	/**
	 * The maximum amount of connections per host. 
	 */
	public static final int MAX_CONNECTIONS_PER_HOST = 3;

	/**
	 * Used to keep track of hosts and their amount of connections.
	 */
	private static ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<String, Integer>();

	/**
	 * Used to keep track of login attempts.
	 */
	private static ConcurrentHashMap<String, ConnectionAttempt> attempts = new ConcurrentHashMap<String, ConnectionAttempt>();

	public static class ConnectionAttempt {
		private static final long TIMEOUT = 5 * 60 * 1000; // 5 minutes (300,000ms)
		private static final int MAX_ATTEMPTS = 7;
		Stopwatch timer;
		int attempts = 0;

		public ConnectionAttempt() {
			timer = new Stopwatch();
		}

		public void addAttempt() {
			if (attempts < MAX_ATTEMPTS) {
				attempts += 1;
				timer.reset();
			} else {
				if (!canConnect()) {
					timer.reset();
				} else {
					reset();
				}
			}
		}

		public void removeAttempt() {
			if (attempts > 0) {
				attempts -= 1;
			}
		}

		public void reset() {
			attempts = 0;
			timer.reset();
		}

		public boolean canConnect() {
			return attempts != MAX_ATTEMPTS || timer.elapsed() >= TIMEOUT;
		}
	}

	public static boolean attempt(String host) {
		final ConnectionAttempt attempt = attempts.putIfAbsent(host, new ConnectionAttempt());

		if (attempt != null) {
			attempt.addAttempt();
		}

		if (attempt != null && !attempt.canConnect()) {
			return false;
		}

		return true;
	}

	public static void attemptRemove(String host) {
		final ConnectionAttempt attempt = attempts.putIfAbsent(host, new ConnectionAttempt());

		if (attempt != null) {
			attempt.removeAttempt();
		}
	}

	/**
	 * Checks the host into the gateway.
	 * 
	 * @param host the host
	 * @return true if the host can connect, false if it has reached the maximum
	 *		 amount of connections
	 */
	public static boolean enter(String host) {
		// Server is being updated
		if (!Constants.ACCEPT_CONNECTIONS) {
			return false;
		}

		Integer amount = map.putIfAbsent(host, 1);

		// If they've reached the connection limit, return false.
		if (amount != null && amount == MAX_CONNECTIONS_PER_HOST) {
			return false;
		}

		amount = amount == null ? 0 : amount;

		// Otherwise, replace the key with the next value if it was present.
		map.replace(host, amount + 1);
		return true;
	}

	/**
	 * Unchecks the host from the gateway.
	 * 
	 * @param host the host
	 */
	public static void exit(String host) {
		Integer amount = map.get(host);

		if (amount == null) {
			return;
		}

		// Remove the host from the map if it's at 1 connection.
		if (amount == 1) {
			map.remove(host);
			return;
		}

		// Otherwise decrement the amount of connections stored.
		if (amount != null) {
			map.replace(host, amount - 1);
		}
	}
}