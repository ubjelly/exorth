package com.exorth.rs2.content;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.impl.MinuteIntervalCounter;

/**
 * Player moderation methods.
 * @author Joshua Barry <Sneakyhearts>
 * @author Stephen
 */
public class Judgement {
	private static final File directory = new File("./accounts/banned/");

	/**
	 * Bans a player from the server.
	 * @param player The player to ban.
	 * @param reason The reason for the ban.
	 * @return true If the ban was successful.
	 */
	public static boolean ban(Player player, String reason) {
		if (!directory.exists() || !directory.mkdir()) {
			return false;
		}

		String name = player.getUsername().toLowerCase();

		byte[] buffer = reason.getBytes();
		int number_of_lines = 400000;

		System.out.println("Banning " + name + "...");

		try {
			RandomAccessFile file = new RandomAccessFile(name.concat(".dat"), "w");
			FileChannel channel = file.getChannel();
			ByteBuffer wrBuf = channel.map(FileChannel.MapMode.READ_WRITE, 0, name.length() * number_of_lines);
			for (int i = 0; i < number_of_lines; i++) {
				wrBuf.put(buffer);
			}
			channel.close();
			file.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return true;
	}

	/**
	 * Mutes a player for a specified amount of time.
	 * @param player The player being muted.
	 * @param duration The duration in minutes to mute.
	 */
	public static void silence(final Player player, final Long duration) {
		// The current time (in ms)
		long time = System.currentTimeMillis();

		System.out.println("Player : " + player.getUsername() + " has been muted for " + duration + " minutes.");

		player.setAttribute("silence_time", time);
		player.setAttribute("silence_duration", duration);

		player.getActionSender().sendMessage("You have been muted for " + duration + " minutes!");

		/* Removes the attribute after the time is up */
		World.submit(new MinuteIntervalCounter(1) {
			int elapsed = 0;
			@Override
			protected void execute() {
				elapsed++;
				if (elapsed == duration) {
					if (player.isLoggedIn()) {
						player.removeAttribute("silence_time");
						player.removeAttribute("silence_duration");
					}
					this.stop();
				}
			}
		});
	}
	
	/**
	 * Gets the remaining time a player is silenced for.
	 * @param player The player to check.
	 * @return The time left on the silence.
	 */
	public static Long getSilenceTime(Player player) {
		return player.getAttribute("silence_duration");
	}
}