package com.exorth.rs2.content;

import java.util.HashMap;

import com.exorth.rs2.model.players.Player;

/**
 * Handles the player emotes.
 * 
 * @author Jacob Ford
 */
public class EmoteActions {
	private static EmoteActions instance = null;

	public static EmoteActions getInstance() {
		if (instance == null) {
			instance = new EmoteActions();
		}
		return instance;
	}

	public enum emoteData {
		YES(168, 855),
		NO(169, 856),
		BOW(162, 857),
		THINK(164, 858),
		BECKON(165, 859),
		CRY(161, 860),
		LAUGH(170, 861),
		CHEER(171, 862),
		WAVE(163, 863),
		ANGRY(167, 864),
		CLAP(172, 865),
		DANCE(166, 866),
		PANIC(13362, 2105),
		JIG(13363, 2106),
		SPIN(13364, 2107),
		HEAD_BANG(13365, 2108),
		JUMP_FOR_JOY(13366, 2109),
		RASPBERRY(13367, 2110),
		YAWN(13368, 2111),
		SALUTE(13369, 2112),
		SHRUG(13370, 2113),
		BLOW_KISS(11100, 1368, 574),
		GLASS_BOX(667, 1131),
		CLIMB_ROPE(6503, 1130),
		LEAN(6506, 1129),
		GLASS_WALL(666, 1128),
		GOBLIN_BOW(13383, 2127),
		GOBLIN_SALUTE(13384, 2128),
		SCARED(15166, 2836),
		ZOMBIE_DANCE(18464, 3544),
		ZOMBIE_WALK(18465, 3543),
		BUNNY_HOP(18686, 6111),
		STOMP(176, 4278),
		FLAP(177, 4280),
		SLAP_HEAD(178, 4275),
		IDEA(173, 4276, 712);

		private int animationId;

		private int graphicId;

		private int buttonId;

		/**
		 * Emotes without a GFX
		 * 
		 * @param animID The Animation.
		 * @param buttonId The Button.
		 */
		private emoteData(int buttonId, int animId) {
			this.buttonId = buttonId;
			this.animationId = animId;
		}

		/**
		 * Emotes with a GFX
		 * 
		 * @param animId The Animation.
		 * @param buttonId The Button.
		 * @param gfxId The Graphic.
		 */
		private emoteData(int buttonId, int animId, int gfxId) {
			this.buttonId = buttonId;
			this.animationId = animId;
			this.graphicId = gfxId;
		}

		public static HashMap<Integer, emoteData> emotes = new HashMap<Integer, emoteData>();

		public static emoteData loadEmotes(int buttonId) {
			return emotes.get(buttonId);
		}

		static {
			for (emoteData emote : emoteData.values()) {
				emotes.put(emote.getButtonId(), emote);
			}
		}

		public int getAnimId() {
			return animationId;
		}

		public int getGfxId() {
			return graphicId;
		}

		public int getButtonId() {
			return buttonId;
		}
	}

	public void activateEmoteButton(Player player, int buttonId) {
		emoteData emote = emoteData.loadEmotes(buttonId);
		if (emote != null) {
			if (emote.getAnimId() > 0) {
				player.getUpdateFlags().sendAnimation(emote.getAnimId());
				if (emote.getGfxId() > 0) {
					player.getUpdateFlags().sendGraphic(emote.getGfxId(), 0);
				}
			}
			emote = null;
		}
	}
}