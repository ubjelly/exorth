package com.exorth.rs2.content.clue;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.area.Area;

/**
 * Stores a collection of map type clues in an enumeration.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public enum Map {
	// The statue outside Falador
	SARADOMIN_MAP(10180, 17537, new Area(new Position(2962, 3408, 0), new Position(2972, 3417, 0)), Difficulty.EASY),
	/* all the below are correct difficulties but idk the locations - Josh */
	//North of Varrock east mine
	VARROCK_EAST_MAP(10182, 7045, new Area(new Position(3290, 3373, 0), new Position(3289, 3372, 0)), Difficulty.EASY),
	//South of Wizard's Tower
	WIZARD_TOWER_MAP(10184, 9275, new Area(new Position(3109, 3153, 0), new Position(3109, 3152, 0)), Difficulty.EASY),
	//Galahad's House - NW of Seers' Village
	GALAHAD_HOUSE_MAP(10186, 9108, new Area(new Position(2612, 3481, 0), new Position(2613, 3481, 0)), Difficulty.EASY),
	//North of Falador, near rocks
	NORTH_FALADOR_MAP(10188, 7271, new Area(new Position(3043, 3398, 0), new Position(3043, 3399, 0)), Difficulty.EASY),
	//West of Crafting Guild on small island
	CRAFTING_GUILD_WEST_MAP(10190, 4305, new Area(new Position(2906, 3294, 0), new Position(2907, 3295)), Difficulty.MEDIUM),
	//LAGHANDLER_MAP(10192, 7162, Difficulty.MEDIUM), //Not on Orbscape.
	//South of Draynor Bank
	DRAYNOR_BANK_MAP(10194, 7113, new Area(new Position(3093, 3227, 0), new Position(3094, 3228, 0)), Difficulty.MEDIUM),
	//ONE_HUNDRED_PERCENT_CACHE_LOADED_MAP(10196, 9196, Difficulty.MEDIUM),
	// //Crate
	//DYEL_MAP(10198, 9632, Difficulty.MEDIUM), //No Tower of Life?
	//MADBRO_MAP(10200, 9720, Difficulty.MEDIUM), // Crate
	//West of Rimmington
	RIMMINGTON_MAP(10202, 9839, new Area(new Position(2924, 3210, 0), new Position(2923, 3218, 0)), Difficulty.MEDIUM),
	//West Ardougne
	WEST_ARDOUGNE_MAP(10204, 9359, new Area(new Position(2469, 3323, 0), new Position(2472, 3325, 0)), Difficulty.HARD),
	//QUAKE_MAP(10206, 9454, Difficulty.HARD), //Crate
	//South-East Yanille
	YANILLE_MAP(10208, 9043, new Area(new Position(2616, 3077, 0), new Position(2618, 3076)), Difficulty.HARD);

	private int scrollId;

	private int interfaceId;

	private Area area;

	private Difficulty difficulty;

	/**
	 * Constructs a new map type clue.
	 * 
	 * @param scrollId The ID of the clue scroll item.
	 * @param interfaceId The ID of the interface that displays the map.
	 * @param area The are you must be within to dig successfully.
	 * @param difficulty The level of difficulty.
	 */
	private Map(int scrollId, int interfaceId, Area area, Difficulty difficulty) {
		this.scrollId = scrollId;
		this.interfaceId = interfaceId;
		this.area = area;
		this.difficulty = difficulty;
	}

	/**
	 * 
	 * @return the scrollId
	 */
	public int getScrollId() {
		return scrollId;
	}

	/**
	 * @return the interfaceId
	 */
	public int getInterfaceId() {
		return interfaceId;
	}

	/**
	 * 
	 * @return the area.
	 */
	public Area getArea() {
		return area;
	}

	/**
	 * @return the difficulty
	 */
	public Difficulty getDifficulty() {
		return difficulty;
	}

	/**
	 * Access a new map clue.
	 * 
	 * @param scrollId The id of the clue scroll item.
	 * @return
	 */
	public static Map forId(int scrollId) {
		for (Map map : Map.values()) {
			if (scrollId == map.getScrollId()) {
				return map;
			}
		}
		return null;
	}
}