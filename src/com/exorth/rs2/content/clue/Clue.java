package com.exorth.rs2.content.clue;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Misc;

/**
 * Represents a clue in the Treasure Trails.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Clue {
	private Difficulty difficulty;

	private Type type;

	/**
	 * Constructs a new clue, based off the difficulty level and the type.
	 * 
	 * @param difficulty The level of difficulty.
	 * @param type The type of clue that determines how it must be solved.
	 * 
	 */
	public Clue(Difficulty difficulty, Type type) {
		this.difficulty = difficulty;
		this.type = type;

	}

	/**
	 * @return the difficulty
	 */
	public Difficulty getDifficulty() {
		return difficulty;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	public void read(Player player) {
		switch (type) {
			case RIDDLE:
				// 6968 - 6975 sendString ( "The quick brown fox.. " )
				int childId = 6968;
				int riddleId = (Integer) player.getAttribute("clue_riddle");
				Riddle riddle = Riddle.forId(riddleId);
				for (int i = 0; i < 8; i++) {
					if (i + 1 > riddle.getContext().length) {
						player.getActionSender().sendString("", childId);
					} else if (riddle.getContext()[i] == null) {
						player.getActionSender().sendString("", childId);
					} else {
						player.getActionSender().sendString(riddle.getContext()[i], childId);
					}
					childId += 1;
				}
				player.getActionSender().sendInterface(6965);
				break;
			case MAPS:
				int scrollId = (Integer) player.getAttribute("clue_map");
				Map map = Map.forId(scrollId);
				player.getActionSender().sendInterface(map.getInterfaceId());
				break;
			default:
				System.err.println("Undetermined clue difficulty called!");
				return;
		}
	}

	/**
	 * Called when a {@code Player} solves a clue.
	 * 
	 * @param player The player who solved the clue.
	 */
	public void complete(Player player) {
		/*
		 * as the clue gets harder, the chance of getting more items will increase.
		 */
		int amount = Misc.randomMinMax(1, (difficulty.ordinal() + 3));

		Item[] rewards = new Item[amount];

		/* generate random items from the possible reward list. */
		for (int i = 0; i < rewards.length; i++) {
			Item item = new Item(Misc.generatedValue(difficulty.getRewards()));
			rewards[i] = item;
			if (player.getInventory().getItemContainer().hasRoomFor(item)) {
				player.getInventory().addItem(item);
			}
		}

		/* set-up the interface. */
		player.getActionSender().sendUpdateItems(6963, rewards);
		player.getActionSender().sendInterface(6960);
		player.getActionSender().sendMessage("Well done, you've completed the Treasure Trail!");
	}
}