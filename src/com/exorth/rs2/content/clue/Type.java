package com.exorth.rs2.content.clue;

/**
 * The type of clue which determines how the clue is described to the player.
 * 
 * @author Joshua Barry
 * 
 */
public enum Type {
	MAPS, RIDDLE, URI_EMOTE;
}