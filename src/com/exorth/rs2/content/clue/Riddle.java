package com.exorth.rs2.content.clue;

/**
 * Stores a collection of riddles in an enumeration.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public enum Riddle {
	WILDERNESS_FOURTY_SIX(2677, Difficulty.EASY, "46 is my number.", "My body is burnt orange colour", "and crawls with those with eight.", "3 mouths I have yet I can not eat.", "My blinking blue eye", "hides my grave.");

	private int scrollId;

	private Difficulty difficulty;

	private String[] context;

	/**
	 * Constructs a new riddle.
	 * 
	 * @param scrollId The ID of the clue scroll item that generates this clue.
	 * @param difficulty The level of difficulty.
	 * @param context The message context for the interface.
	 */
	private Riddle(int scrollId, Difficulty difficulty, String... context) {
		this.scrollId = scrollId;
		this.difficulty = difficulty;
		this.context = context;
	}

	/**
	 * 
	 * @return the scrollId;
	 */
	public int getScrollId() {
		return scrollId;
	}

	/**
	 * @return the difficulty
	 */
	public Difficulty getDifficulty() {
		return difficulty;
	}

	/**
	 * @return the context
	 */
	public String[] getContext() {
		return context;
	}

	public static Riddle forId(int id) {
		for (Riddle riddle : Riddle.values()) {
			if (id == riddle.getScrollId()) {
				return riddle;
			}
		}
		return null;
	}
}