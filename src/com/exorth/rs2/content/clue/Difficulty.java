package com.exorth.rs2.content.clue;

/**
 * Represents the level of difficulty associated with a clue.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public enum Difficulty {
	EASY(1077, 1125, 1165, 1195, 1297, 1367, 853, 7390, 7392, 7394, 7396, 7386, 7388, 1099, 1135, 1065, 851),
	MEDIUM(1073, 1123, 1161, 1199, 1301, 1371, 857, 2577, 2579, 2487, 2493, 2499, 2631, 855),
	HARD(1079, 1093, 1113, 1127, 1147, 1163, 1185, 1201, 1275, 1303, 1319, 1333, 1359, 1373, 2491, 2497, 2503, 861, 859, 2581, 2651),
	ELITE(1038, 1040, 1042, 1044, 1046, 1048, 1050, 1053, 1055, 1057);

	private Integer[] rewards;

	/**
	 * Creates a difficulty level and stores the possible rewards associated
	 * with the level in the element of the enumeration.
	 * 
	 * @param rewards An array of items that are all possible reward items upon
	 *			clue completion.
	 */
	private Difficulty(Integer... rewards) {
		this.rewards = rewards;
	}

	public Integer[] getRewards() {
		return rewards;
	}
}