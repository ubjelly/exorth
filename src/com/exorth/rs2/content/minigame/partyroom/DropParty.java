package com.exorth.rs2.content.minigame.partyroom;

import java.util.ArrayList;
import java.util.Random;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.npcs.Npc.WalkType;
import com.exorth.rs2.model.object.CustomObjectManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.clip.RSObject;

/**
 * The actual drop party.
 * @author Stephen
 */
public class DropParty extends PartyRoom {
	
	/**
	 * A collection of entertainers.
	 */
	private ArrayList<Npc> entertainers = new ArrayList<Npc>();
	
	/**
	 * An array of adjectives the entertainers will say.
	 */
	private String[] adjectives = {"godlike", "amazing", "beast"};
	
	/**
	 * The owner of the drop party.
	 */
	private Player owner;
	
	/**
	 * Whether or not there is a party going on.
	 */
	private boolean partyActive;
	
	/**
	 * Constructs a drop party.
	 * @param owner The owner of the drop party.
	 */
	public DropParty(Player owner) {
		this.owner = owner;
		populateTiles();
		partyActive = true;
	}
	
	/**
	 * Starts the drop party.
	 * @param player The player clicking the lever.
	 */
	public void dropBalloons(Player player) {
		if (player.getIndex() != owner.getIndex()) {
			player.getActionSender().sendMessage("You are not the owner of this drop party!");
			return;
		}
		spawnEntertainers();
		entertain();
		World.submit(new Task(5) { //Every 5 cycles drop 5 balloons
			@Override
			protected void execute() {
				if (balloonDrops.size() >= 30) { //Prevent massive lag
					System.out.println("Waiting for balloons to be popped");
					return;
				}
				
				Random randomBalloon = new Random();
				for (Player player : World.getPlayers()) {
					if (inPartyRoom(player)) {
						System.out.println("Dropping");
						for (int i = 0; i <= 5; i++) {
							if (partyChest.size() == 0) { //When all items have been dropped.
								partyActive = false;
								removeEntertainers();
								this.stop();
							}
							RSObject balloon = new RSObject(balloonIds[randomBalloon.nextInt(balloonIds.length)], getRandomTile(), 10, 0);
							CustomObjectManager.createGlobalObject(balloon);
							System.out.println("Balloon drop size before adding: " + balloonDrops.size());
							balloonDrops.add(balloon.getPosition());
							System.out.println("Balloon drop size after adding: " + balloonDrops.size());
						}
					}
				}
			}
		});
	}

	/**
	 * Spawns the entertainers on the table.
	 */
	private void spawnEntertainers() {
		for(Position pos : entertainerSpawns) {
			Npc npc = new Npc(660);
			npc.setSpawnPosition(pos);
			npc.setMaxWalkingArea(new Position(0, 0, 0));
			npc.setWalkType(WalkType.STAND);
			npc.setFaceType(0);
			npc.getUpdateFlags().sendGraphic(86, 0, 100);
			entertainers.add(npc);
			World.register(npc);
		}
	}
	
	/**
	 * Removes the entertainers after the drop party is over.
	 */
	private void removeEntertainers() {
		for (Npc npc : entertainers) {
			npc.getUpdateFlags().sendForceMessage("Great party " + owner.getUsername() + "!");
			npc.getUpdateFlags().sendAnimation(863);
			npc.getUpdateFlags().sendGraphic(86, 0, 100);
			World.unregister(npc);
		}
	}
	
	/**
	 * Repeatedly makes the npcs 'entertain' while the drop party is active.
	 */
	private void entertain() {
		final Random random = new Random();
		for(final Npc npc : entertainers) {
			npc.getUpdateFlags().sendAnimation(866);
			npc.getUpdateFlags().sendForceMessage(owner.getUsername() + " is " + adjectives[random.nextInt(adjectives.length)] + "!");
			World.submit(new Task(7) {
				@Override
				protected void execute() {
					if (partyActive) {
						npc.getUpdateFlags().sendAnimation(866);
						npc.getUpdateFlags().sendForceMessage(owner.getUsername() + " is " + adjectives[random.nextInt(adjectives.length)] + "!");
					} else {
						this.stop();
					}
				}
			});
		}
	}
	
	/**
	 * Generates a random tile in the party room that doesn't have a balloon on it.
	 * @return The random tile.
	 */
	private Position getRandomTile() {
		Random random = new Random();
		Position randomPos = validTiles.get(random.nextInt(validTiles.size()));
		while (balloonDrops.contains(randomPos)) {
			System.out.println("Balloon already on tile, getting new tile.");
			randomPos = validTiles.get(random.nextInt(validTiles.size()));
		}
		return randomPos;
	}
	
	/**
	 * Gets the owner of the drop party.
	 * @return The owner of the drop party.
	 */
	public Player getOwner() {
		return owner;
	}
}
