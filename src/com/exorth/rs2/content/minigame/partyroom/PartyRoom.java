package com.exorth.rs2.content.minigame.partyroom;

import java.util.ArrayList;
import java.util.Random;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.object.CustomObjectManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.container.Container;
import com.exorth.rs2.model.players.container.ContainerType;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.clip.PathFinder;

/**
 * The main class for the party room minigame.
 * @author Stephen
 */
public class PartyRoom {
	
	/**
	 * A collection of tiles for the party room area.
	 */	
	protected ArrayList<Position> validTiles = new ArrayList<Position>();
		
	/**
	 * A collection of tiles where balloons have been dropped.
	 */
	protected ArrayList<Position> balloonDrops = new ArrayList<Position>();
	
	/**
	 * A list of positions that the entertainers will spawn on.
	 */
	protected ArrayList<Position> entertainerSpawns = new ArrayList<Position>();

	/**
	 * The size of the party chest container.
	 */
	private final int CONTAINER_SIZE = 216;
	
	/**
	 * The interface id of the chest container.
	 */
	public final int CONTAINER_ID = 2273;
	
	/**
	 * The container of the party chest.
	 */
	protected Container partyChest = new Container(ContainerType.STANDARD, CONTAINER_SIZE);
	
	/**
	 * The size of the deposit container.
	 */
	public final int DEPOSIT_SIZE = 8;
	
	/**
	 * The interface id of the deposit container.
	 */
	public final int DEPOSIT_ID = 2274;
	
	/**
	 * The container which allows users to deposit items into the chest.
	 */
	public Container chestDeposit = new Container(ContainerType.STANDARD, DEPOSIT_SIZE);
	
	/**
	 * The id of the drop party chest interface.
	 */
	public final int DROP_PARTY_CHEST = 2156;
	
	/**
	 * The id of the offer inventory view.
	 */
	public final int OFFER_INVENTORY = 3322;
	
	/**
	 * Object ids consisting of various types of balloons that fall from the ceiling.
	 */
	public int[] balloonIds = {115, 116, 117, 118, 119, 120};
	
	/**
	 * The animation for popping balloons.
	 */
	private final int POPPING_ANIM = 794;
	
	/**
	 * The graphic for popping balloons.
	 */
	private final int POPPING_GRAPHIC = 524;
	
	/**
	 * Populate the valid tiles map.
	 */
	public void populateTiles() {
		/*
		 * Construct a rectangle of tiles and put each tile into the 'validTiles' array list.
		 */
		int width = 13;
		int length = 12;
		int startX = 2731;
		int startY = 3463;
		int i = 0;
		while (i < width) {
			for (int j = 0; j <= length; j++) {
				validTiles.add(new Position(startX + i, startY + j));
			}
			i++;
		}
		
		/*
		 * Now remove the tiles that the table is covering while adding them to an array list for future use.
		 */
		int tableStartX = 2735;
		int tableY = 3468;
		for (int k = 0; k <= 5; k++) {
			validTiles.remove(new Position(tableStartX + k, tableY));
			entertainerSpawns.add(new Position(tableStartX + k, tableY));
		}
	}
	
	/**
	 * Opens the party chest interface.
	 * @param player The player opening the chest.
	 */
	public void openChest(Player player) {
		player.getActionSender().sendInterface(DROP_PARTY_CHEST);
		refresh(player);
		player.getActionSender().sendItfInvOverlay(DROP_PARTY_CHEST, 3321);
	}
	
	/**
	 * Refreshes the items in the drop party chest.
	 * @param player The player viewing the chest.
	 */
	private void refresh(Player player) {
		player.getActionSender().sendUpdateItems(OFFER_INVENTORY, player.getInventory().getItemContainer().toArray());
		player.getActionSender().sendUpdateItems(CONTAINER_ID, partyChest.toArray());
		player.getActionSender().sendUpdateItems(DEPOSIT_ID, chestDeposit.toArray());
	}
	
	/**
	 * Puts an item into the deposit container.
	 * @param player The player depositing items.
	 * @param slot The slot of the item.
	 * @param itemId The id of the item.
	 * @param amount The amount of the item.
	 */
	public void depositItem(Player player, int slot, int itemId, int amount) {
		if (player == null || !player.isLoggedIn()) return;
		
		if (chestDeposit.size() == DEPOSIT_SIZE) {
			player.getActionSender().sendMessage("You cannot deposit any more items until you click accept!");
			return;
		}
		
		Item item = player.getInventory().getItemContainer().get(slot);
		int invAmount = player.getInventory().getItemContainer().getCount(itemId);
		
		if (ItemManager.getInstance().isUntradeable(itemId)) {
			player.getActionSender().sendMessage("You can't add this item to the chest.");
			return;
		}

		if (invAmount > amount) {
			invAmount = amount;
		}
		
		if (item.getDefinition().isStackable()) {
			player.getInventory().removeItemSlot(slot, amount);
		} else {
			for (int i = 0; i < invAmount; i++) {
				if (invAmount == 1) {
					player.getInventory().removeItemSlot(slot);
				} else {
					player.getInventory().removeItem(new Item(itemId, 1));
				}
			}
		}
		
		int itemAmount = chestDeposit.getCount(itemId);
		if (itemAmount > 0 && item.getDefinition().isStackable()) {
			chestDeposit.set(chestDeposit.getSlotById(itemId), new Item(itemId, itemAmount + invAmount));
		} else {
			chestDeposit.add(new Item(item.getId(), invAmount));
		}
		
		refresh(player);
	}
	
	/**
	 * Removes an item from the deposit container.
	 * @param player The player removing the item.
	 * @param slot The slot of the item.
	 * @param itemId The id of the item.
	 * @param amount The amount of the item.
	 */
	public void removeDepositItem(Player player, int slot, int itemId, int amount) {
		if (player == null || !player.isLoggedIn()) {
			return;
		}
		
		Item itemOnScreen = chestDeposit.get(slot);
		int itemOnScreenAmount = chestDeposit.getCount(itemId);
		if (itemOnScreen == null || itemOnScreen.getId() <= 0 || itemOnScreen.getId() != itemId) {
			return;
		}
		
		if (itemOnScreenAmount > amount) {
			itemOnScreenAmount = amount;
		}
		
		player.getInventory().addItem(new Item(itemOnScreen.getId(), itemOnScreenAmount));
		chestDeposit.remove(new Item(itemId, itemOnScreenAmount), slot);
		refresh(player);
	}
	
	/**
	 * Submits the player's items to the chest container.
	 * @param player the player to send the refresh to.
	 */
	public void submitItems(Player player) {
		if (chestDeposit.size() == 0) {
			player.getActionSender().sendMessage("You haven't put any items in!");
			return;
		}
		
		for(Item item : chestDeposit.toArray()) {
			partyChest.add(item);
		}
		chestDeposit.clear();
		refresh(player);
	}
	
	/**
	 * Pops a balloon and sends the object removal to all players in the party room.
	 * @param player The player popping the balloon.
	 * @param balloon The balloon being popped.
	 */
	public void popBalloon(final Player player, final Position balloon) {	
		if (player == null || balloon == null) return;
		
		World.submit(new Task(true) {
			@Override
			protected void execute() {
				if (!player.getPosition().equals(balloon)) {
					PathFinder.getSingleton().moveTo(player, balloon.getX(), balloon.getY());
					return;
				} else {
					this.stop();
					player.getUpdateFlags().sendAnimation(POPPING_ANIM);
					player.getUpdateFlags().sendGraphic(POPPING_GRAPHIC, 30, 0);
					World.submit(new Task(2) {
						@Override
						protected void execute() {
							CustomObjectManager.removeGlobalObject(player.getPosition());
							System.out.println("Balloon drops size before removal: " + balloonDrops.size()); //PROBLEM IS HERE
							if (balloonDrops.contains(player.getPosition()))
								balloonDrops.remove(player.getPosition());
							System.out.println("Balloon drops size: " + balloonDrops.size());
							Random chance = new Random();
							double itemChance = chance.nextDouble();
							System.out.println("Item chance: " + itemChance);
							if (itemChance <= .40) {
								Item reward = getReward();
								if (player.getInventory().hasRoomFor(reward)) {
									player.getInventory().addItem(getReward());
								}
							}
						this.stop();
						}
					});
				}
			}
		});
	}
	
	/**
	 * Determines whether or not a player is within the confines of a party room.
	 * @param player The player to check.
	 * @return true If a player is in a valid tile.
	 */
	protected boolean inPartyRoom(Player player) {
		for (Position pos : validTiles) {
		    if (player != null && pos.equals(player.getPosition())) return true;
		}
		return false;
	}
	
	/**
	 * Gets a random item from the chest container and removes it.
	 * @return The reward.
	 */
	private Item getReward() {
		Random random = new Random();
		Item reward = partyChest.get(random.nextInt(partyChest.size()));
		partyChest.remove(reward);
		return reward;
	}
}
