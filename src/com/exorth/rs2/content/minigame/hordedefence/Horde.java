package com.exorth.rs2.content.minigame.hordedefence;

import java.util.Random;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.npcs.Npc.WalkType;
import com.exorth.rs2.task.Task;

public class Horde extends HordeGame {

	/**
	 * The current wave;
	 */
	private int wave;
	
	/**
	 * The size of the horde.
	 */
	private int size;
	
	/**
	 * Constructs a new horde.
	 */
	public Horde() {
		wave = 0;
		size = 0;
	}
	
	/**
	 * Constantly spawns zombies.
	 * @param zombie
	 */
	public void spawnHorde(final Npc zombie) {
		World.submit(new Task(getSpawnInterval()) {
			@Override
			protected void execute() { 
				Random random = new Random();
				zombie.setSpawnPosition(zombieSpawns[random.nextInt(zombieSpawns.length)]);
				zombie.setWalkType(WalkType.WALK);
				World.register(zombie);
				
				if (size == 0) {
					this.stop();
				}
			}
		});
	}
	
	/**
	 * How often zombies will spawn.
	 */
	private int getSpawnInterval() {
		//TODO: Formula for this
		return 1;
	}
	
	public void setWave(int wave) {
		this.wave = wave;
	}
	
	public int getWave() {
		return wave;
	}
}
