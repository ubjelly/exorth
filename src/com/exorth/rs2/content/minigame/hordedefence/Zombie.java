package com.exorth.rs2.content.minigame.hordedefence;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.npcs.Npc.WalkType;
import com.exorth.rs2.model.players.Player;

public class Zombie {

	/**
	 * The zombie NPC
	 */
	private Npc zombie;
	
	/**
	 * Constructs a zombie.
	 * @param zombie The zombie.
	 */
	public Zombie(Npc zombie) {
		this.zombie = zombie;
		System.out.println("Zombie attributes: " + zombie.getAttributes());
	}
	
	public void spawnZombie() {
		zombie.setSpawnPosition(new Position(3503, 3576));
		zombie.setMaxWalkingArea(new Position(0, 0, 0));
		zombie.setWalkType(WalkType.STAND);
		World.register(zombie);
	}
	
	private void editHp() {
		zombie.setAttribute("current_hp", 50);
	}

	public static void die(Npc npc) {
		System.out.println("aids");
		
		npc.getCombatState().getDamageMap().removeInvalidEntries();
		final Player killer = (Player) ((npc.getCombatState().getDamageMap().highestDamage() != null) ? npc.getCombatState().getDamageMap().highestDamage() : null);
		if (killer == null) return;
		
		System.out.println("Killer: " + killer.getUsername());
		npc.teleport(new Position(0, 0, 0));
		World.unregister(npc);
	}
}
