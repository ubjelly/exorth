package com.exorth.rs2.content.minigame.hordedefence;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.Player;

/**
 * A zombie horde game kind of like CoD. Each wave, the zombies will get progressively harder and you will have the 
 * ability to buy different weapons and upgrades. One weapon could be a potion that removes all zombies within a certain raidus
 * or something.
 * 
 * Found a sick place for the minigame - 3504 3575 Grave island.
 * @author Stephen
 */
public class HordeGame {

	/**
	 * Possible spawn positions of zombies.
	 */
	protected Position[] zombieSpawns = { new Position(3503, 3576), new Position(3504, 3576), new Position(3505, 3576), new Position(3504, 3571) };
	
	/**
	 * Horde
	 */
	private static Horde horde = new Horde();
	
	public HordeGame() {
		 
	}
	
	public static void updateInterface(Player player) {
		player.getActionSender().sendString("Wave: " + horde.getWave(), 18001);
		player.getActionSender().sendString("")
	}
	
	private boolean hasRequirements(Player player) {
		if (player.getInventory().getCount() != 0) {
			player.getActionSender().sendMessage("Please empty your inventory before playing.");
			return false;
		}
		
		return true;
	}
}
