package com.exorth.rs2.content.minigame;

/**
 * A flag that determines whether the item is safe or not.
 * 
 * @author Joshua Barry
 * 
 */
public enum ItemSafety {

	SAFE, UNSAFE

}
