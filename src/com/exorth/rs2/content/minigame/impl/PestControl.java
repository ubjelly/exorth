package com.exorth.rs2.content.minigame.impl;

import java.util.Collection;

import com.exorth.rs2.content.minigame.ItemSafety;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.area.Area;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class PestControl extends MultiMinigameStructure {

	private final int gameSize = 2;

	@Override
	public Collection<Player> getParticipants() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Player> getWaitingPlayers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getMinimumGameSize() {
		// TODO Auto-generated method stub
		return gameSize;
	}

	@Override
	public Area getGameArea() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] getEndPosition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] getStartPosition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ItemSafety getItemSafety() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDeadMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Task getGameCycle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean attackListener(Player player, Entity victim) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean defeatListener(Player player, Entity victim) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sessionListener(Player player) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getGameName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getGameInterfaceId() {
		// TODO Auto-generated method stub
		return 0;
	}
}
