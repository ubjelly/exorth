package com.exorth.rs2.content.minigame.impl;

import java.util.logging.Logger;

import com.exorth.rs2.content.minigame.MultiMinigame;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.area.Area;
import com.exorth.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public abstract class MultiMinigameStructure implements MultiMinigame {

	/**
	 * Our logger instance for debugging and printing.
	 */
	private final Logger logger = Logger.getAnonymousLogger();

	@Override
	public void init() {
		if (getGameCycle() != null) {
			World.submit(getGameCycle());
			logger.info(getGameName().concat(" minigame rolling..."));
		}
	}

	@Override
	public void start() {
		for (Player player : getWaitingPlayers()) {
			player.clipTeleport((Position) getStartPosition()[0], (Integer) getStartPosition()[1], (Integer) getStartPosition()[2], (Integer) getStartPosition()[3]);
			if (getGameInterfaceId() != -1) {
				player.getActionSender().sendWalkableInterface(getGameInterfaceId());
			}
		}
		getParticipants().addAll(getWaitingPlayers());
		getWaitingPlayers().clear();
	}

	@Override
	public void end() {
		for (Player player : getParticipants()) {
			player.clipTeleport((Position) getEndPosition()[0], (Integer) getEndPosition()[1], (Integer) getEndPosition()[2], (Integer) getEndPosition()[3]);

			if (player.getMinigame() != null) {
				player.setMinigame(null);
			}

			if (this.getGameInterfaceId() > 0) {
				player.getActionSender().sendWalkableInterface(-1);
			}
		}

		if (getGameCycle() != null) {
			if (!getGameCycle().isStopped()) {
				getGameCycle().stop();
			}
		}

		this.getParticipants().clear();
	}

	@Override
	public void quit(Player player) {
		// teleport the player back to the starting position.
		player.clipTeleport((Position) getEndPosition()[0], (Integer) getEndPosition()[1], (Integer) getEndPosition()[2], (Integer) getEndPosition()[3]);

		player.setAppearanceUpdateRequired(true);

		// set the players minigame status to none.
		player.setMinigame(null);

		player.getActionSender().sendWalkableInterface(-1);

		// remove this player from the mapping of participants.
		getParticipants().remove(player);

		// if there are no players left, end the game.
		if (getParticipants().size() < 1) {
			end();
		}
	}

	@Override
	public void movementListener(Player player) {
		if (!getGameArea().isInArea(player.getPosition())) {
			quit(player);
		}
	}

	@Override
	public Area getWaitingArea() {
		return null;
	}
}
