package com.exorth.rs2.content.minigame.impl;

import java.util.logging.Logger;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.minigame.ItemSafety;
import com.exorth.rs2.content.minigame.Minigame;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.area.Area;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.npcs.Npc.WalkType;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * Tz'haar Fight Caves
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class FightCaves implements Minigame<Npc> {

	/* our logger instance */
	private static final Logger logger = Logger.getAnonymousLogger();

	private final Position maxWalk = new Position(1, 0, 0);

	/* the player participating in the solo minigame */
	private Player player;

	/* The Npcs the player is facing on */
	private Npc npc;

	/* the reward for completion */
	private final Item reward = new Item(6570);

	/* the ending position */
	private final Object[] end = new Object[]
	{ new Position(2436, 5169, 0), 0, 5, 3 };

	/* the starting position */
	private final Object[] start = new Object[]
	{ new Position(2412, 5115, 0), 0, 2, 2 };

	/* the position the npcs will generally spawn at */
	private final Position spawn = new Position(2407, 5097, 0);

	/* The bottom left corner of the game area */
	private final Position bottomLeft = new Position(2371, 5062, 0);

	/* The top right corner of the game area */
	private final Position topRight = new Position(2423, 5117, 0);

	/* The game area */
	private final Area area = new Area(bottomLeft, topRight);

	public FightCaves(Player player, Npc npc) {
		/* declare the player of the game, as well as mobs */
		this.player = player;
		this.npc = npc;

		/* initialize the game */
		init();
	}

	/**
	 * Complete the minigame.
	 */
	@SuppressWarnings("unused")
	private void complete() {
		Dialogue dialogue = new ChatDialogue(npc, null, "");
		player.open(dialogue);
		player.getInventory().addItem(reward);
	}

	@Override
	public Area getGameArea() {
		return area;
	}

	@Override
	public Object[] getEndPosition() {
		return end;
	}

	@Override
	public Object[] getStartPosition() {
		return start;
	}

	@Override
	public ItemSafety getItemSafety() {
		return ItemSafety.SAFE;
	}

	@Override
	public String getDeadMessage() {
		return "Oh dear, you have died!";
	}

	@Override
	public Task getGameCycle() {
		return new Task(50) {
			byte waveId = 1;

			@Override
			protected void execute() {
				logger.info("Current Wave before execution: " + waveId);
				if (waveId > (Wave.values().length - 1)) {
					this.stop();
					return;
				}

				if (player.getAttribute("currentWave") == null) {
					player.getActionSender().sendMessage("Starting Wave: " + waveId);
					Wave wave = Wave.forOrdinal(waveId);
					player.setAttribute("wave", wave);

					for (int id : wave.getNpcs()) {
						Npc npc = new Npc(id);
						npc.setSpawnPosition(spawn);
						npc.setWalkType(WalkType.WALK);
						npc.setMaxWalkingArea(maxWalk);
						npc.setAttribute("IS_BUSY", (byte) 1);
						npc.getDefinition().setRespawnTimer(0);
						World.register(npc);
					}

					waveId++;
					logger.info("Next Wave: " + waveId);
				}
			}

			@Override
			public void stop() {
				logger.info("Stopped the game cycle!");
				super.stop();
			}
		};
	}

	@Override
	public void init() {
		if (getGameCycle() != null) {
			World.submit(getGameCycle());
			logger.info(getGameName().concat(" minigame rolling..."));
		}
	}

	@Override
	public void start() {
		Dialogue dialogue = new ChatDialogue(npc, null, "You're on your own now JalYt, prepare to fight for", "your life!");
		player.setAttribute("soloMinigame", this);
		player.clipTeleport((Position) getStartPosition()[0], (Integer) getStartPosition()[1], (Integer) getStartPosition()[2], (Integer) getStartPosition()[3]);
		player.open(dialogue);

		/* sets a slight delay before the wave is spawned. */

		World.submit(new Task(2) {
			@Override
			protected void execute() {
				Wave wave = Wave.forOrdinal(0);
				player.setAttribute("currentWave", wave);

				for (int id : wave.getNpcs()) {
					Npc npc = new Npc(id);
					npc.setSpawnPosition(spawn);
					npc.setWalkType(WalkType.WALK);
					npc.setMaxWalkingArea(maxWalk);
					npc.setAttribute("IS_BUSY", (byte) 1);
					npc.getDefinition().setRespawnTimer(0);
					World.register(npc);
				}
				this.stop();
			}

			@Override
			public void stop() {
				logger.info("Stopped the game cycle!");
				super.stop();
			}
		});
	}

	@Override
	public void end() {
		/* does a few checks and makes sure all events have been finalized */
		logger.info("Fight Caves officially ended!");
		if (!getGameCycle().isStopped()) {
			getGameCycle().stop();
		}

		if (player.getAttribute("currentWave") != null) {
			player.removeAttribute("currentWave");
		}
		if (player.getAttribute("soloMinigame") != null) {
			player.removeAttribute("soloMinigame");
		}
	}

	@Override
	public void quit(Player player) {
		logger.info("Fight Caves quit!");
		player.clipTeleport((Position) getEndPosition()[0], (Integer) getEndPosition()[1], (Integer) getEndPosition()[2], (Integer) getEndPosition()[3]);
		player.removeAttribute("currentWave");
		player.removeAttribute("soloMinigame");
		end();
	}

	@Override
	public void movementListener(Player player) {
		if (!area.isInArea(player.getPosition())) {
			quit(player);
		}
	}

	@Override
	public boolean attackListener(Player player, Npc victim) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean defeatListener(Player player, Npc victim) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean sessionListener(Player player) {
		logger.info("Session listener called for the Fight Caves Minigame.");
		quit(player);
		return true;
	}

	@Override
	public String getGameName() {
		return "Fight Caves";
	}

	@Override
	public int getGameInterfaceId() {
		return -1;
	}

	/**
	 * Handles the 'waves' of the game, which send mobs of {@code Npc}s to kill
	 * your {@code Player}.
	 * 
	 * @author Joshua Barry <Sneakyhearts>
	 * 
	 */
	private enum Wave {
		// TODO: Full 63 Waves
		ONE(82), TWO(82, 92);

		private Integer[] npcs;

		private Wave(Integer... npcs) {
			this.npcs = npcs;
		}

		/**
		 * 
		 * @param idx
		 * @return
		 */
		public static Wave forOrdinal(int idx) {
			for (Wave wave : Wave.values()) {
				if (idx == wave.ordinal()) {
					return wave;
				}
			}
			return null;
		}

		public Integer[] getNpcs() {
			return npcs;
		}
	}
}
