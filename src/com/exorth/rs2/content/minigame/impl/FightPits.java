package com.exorth.rs2.content.minigame.impl;

import java.util.ArrayList;

import com.exorth.rs2.content.combat.Combat;
import com.exorth.rs2.content.combat.util.Poison;
import com.exorth.rs2.content.combat.util.Prayer;
import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.minigame.ItemSafety;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.area.Area;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class FightPits extends MultiMinigameStructure {

	private final Npc npc = new Npc(2618);

	/**
	 * The fighting area.
	 */
	public static final Area gameArea = new Area(new Position(2373, 5126, 0), new Position(2424, 5168, 0));

	/**
	 * The waiting area.
	 */
	public static final Area waitingArea = new Area(new Position(2393, 5169, 0), new Position(2405, 5175));

	private Task game_start;

	/**
	 * The position where the player is teleported when the game is finished.
	 */
	private final Object[] endPosition = new Object[]{ new Position(2395, 5170, 0), 0, 8, 3 };

	/**
	 * The position where the game starts.
	 */
	private final Object[] startPosition = new Object[]{ new Position(2392, 5154, 0), 0, 14, 10 };

	/**
	 * The participants of the game.
	 */
	private ArrayList<Player> participants;

	/**
	 * The waiting players for the next game.
	 */
	private ArrayList<Player> waitingPlayers;

	/**
	 * The name of the minigame.
	 */
	private final String gameName = "Fight Pits";

	/**
	 * The game winner.
	 */
	private String winnerName;

	/**
	 * The minimum players of this game.
	 */
	private final int minimumPlayers = 2;

	/**
	 * A flag to determine if the game is running or waiting.
	 */
	private boolean gameStarted = false;

	public FightPits() {
		// create a new blank mapping of player participants.
		participants = new ArrayList<Player>();

		// create a new blank mapping of waiting players.
		waitingPlayers = new ArrayList<Player>();

		// specify the default winner name.
		winnerName = "Tzhaar-Xil-Huz";

		// start the minigame rolling.
		init();
	}

	@Override
	public void start() {
		// First tele the players to starting area
		for (Player player : getWaitingPlayers()) {
			player.clipTeleport((Position) getStartPosition()[0], (Integer) getStartPosition()[1], (Integer) getStartPosition()[2], (Integer) getStartPosition()[3]);
		}

		// Move waiting players to be game players
		getParticipants().addAll(getWaitingPlayers());
		getWaitingPlayers().clear();

		// Inform game players
		World.submit(new Task(true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(npc, null, "Wait for my signal before fighting.");
				for (Player player : getParticipants()) {
					player.getActionSender().sendString("Current Champion: " + getWinnerName(), 2805);
					player.getActionSender().sendConfig(560, participants.size() - 1);
					player.open(dialogue);
					this.stop();
				}
			}
		});

		// If there's already task going on, stop it first.
		if (game_start != null) {
			game_start.stop();
		}

		game_start = new Task(true) {
			int loop = 0;

			@Override
			protected void execute() {
				// If we don't have enough participants, return as soon as possible from launching new game.
				if (participants.size() < 2) {
					this.stop();
					return;
				}

				if (loop == 18 && !gameStarted) {
					gameStarted = true;
					Dialogue dialogue = new ChatDialogue(npc, null, "FIGHT!");
					for (Player player : getParticipants()) {
						player.open(dialogue);
						player.getActionSender().sendPlayerOption("Attack", 1);
					}
					this.stop();
				}
				loop++;
			}
		};

		// Submit the game start task.
		World.submit(game_start);
	}

	@Override
	public void quit(Player player) {
		player.setSkullIcon(-1);
		player.setAppearanceUpdateRequired(true);

		// Reset combat stuff
		player.getCombatState().getDamageMap().reset();
		Combat.getInstance().resetCombat(player);
		player.setSpecialAttackActive(false);
		player.setSpecialAmount(10);
		player.setCombatTimer(0);
		player.setCanWalk(true);
		player.removeAttribute("isFrozen");
		player.removeAttribute("teleBlocked");
		Poison.appendPoison(player, false, 0);
		Prayer.getInstance().resetAll(player);
		player.getUpdateFlags().sendAnimation(65535, 0);
		player.getSkill().normalize();

		if (waitingPlayers.contains(player)) {
			waitingPlayers.remove(player);
			player.setMinigame(null); // Null the minigame when we exit out of the lobby
		} else if (participants.contains(player)) {
			player.clipTeleport((Position) getEndPosition()[0], (Integer) getEndPosition()[1], (Integer) getEndPosition()[2], (Integer) getEndPosition()[3]);
			participants.remove(player);
			// Fix for logging out players.
			if (player.isLoggedIn()) {
				waitingPlayers.add(player);
			}
			for (Player participant : participants) {
				participant.getActionSender().sendConfig(560, participants.size() - 1);
			}
		}

		// If people leave and only 1 player left make winner of him.
		if (participants.size() == 1) {
			gameWon(participants.get(0));
		}
	}

	@Override
	public void end() {
		
	}

	@Override
	public void movementListener(Player player) {
		
	}

	@Override
	public boolean attackListener(Player player, Entity entity) {
		if (!gameStarted) {
			player.getActionSender().sendMessage("You're not allowed to attack yet!");
		}
		return gameStarted;
	}

	@Override
	public boolean defeatListener(Player who_died, Entity killer) {
		if (killer.isPlayer()) {
			if (killer.getAttribute("totalDefeated") == null) {
				killer.setAttribute("totalDefeated", (int) who_died.getSkill().getCombatLevel());
			} else {
				killer.setAttribute("totalDefeated", (int) ((Integer) killer.getAttribute("totalDefeated") + who_died.getSkill().getCombatLevel()));
			}

			// Remove the player who died from game participants and move to waiting player, no need to set skulls or anything as 
			// death method handles it
			participants.remove(who_died);
			waitingPlayers.add(who_died);

			for (Player participant : participants) {
				participant.getActionSender().sendConfig(560, participants.size() - 1);
			}

			if (participants.size() == 1) {
				/* last player wins */
				gameWon((Player) killer);
			}
		}
		return true;
	}

	@Override
	public boolean sessionListener(Player player) {
		quit(player);
		return true;
	}

	@Override
	public Task getGameCycle() {
		return new Task(48) {
			@Override
			protected void execute() {
				if (!gameStarted && waitingPlayers.size() >= minimumPlayers) {
					start();
				}
			}
		};
	}

	@Override
	public Area getGameArea() {
		return gameArea;
	}

	@Override
	public Area getWaitingArea() {
		return waitingArea;
	}

	@Override
	public Object[] getEndPosition() {
		return endPosition;
	}

	@Override
	public Object[] getStartPosition() {
		return startPosition;
	}

	@Override
	public ArrayList<Player> getParticipants() {
		return participants;
	}

	@Override
	public ArrayList<Player> getWaitingPlayers() {
		return waitingPlayers;
	}

	@Override
	public String getGameName() {
		return gameName;
	}

	@Override
	public int getMinimumGameSize() {
		return minimumPlayers;
	}

	@Override
	public int getGameInterfaceId() {
		return -1;
	}

	/**
	 * 
	 * @return
	 */
	public String getWinnerName() {
		return winnerName;
	}

	/**
	 * Called from WalkToAction when the player passed the hot vent door.
	 * 
	 * @param participant The player that will be added to the game.
	 */
	public void addWaitingPlayer(Player participant) {
		waitingPlayers.add(participant);
		participant.setMinigame(this);
	}

	/**
	 * Indicates that the game has been won by a player.
	 * 
	 * @param player The player that won the game.
	 */
	public void gameWon(Player player) {
		winnerName = "JalYt-Ket-".concat(player.getUsername());

		player.setSkullIcon(1);
		player.setAppearanceUpdateRequired(true);
		player.clipTeleport((Position) getEndPosition()[0], (Integer) getEndPosition()[1], (Integer) getEndPosition()[2], (Integer) getEndPosition()[3]);

		player.getCombatState().getDamageMap().reset();
		Combat.getInstance().resetCombat(player);
		player.setSpecialAttackActive(false);
		player.setSpecialAmount(10);
		player.setCombatTimer(0);
		player.setCanWalk(true);
		player.removeAttribute("isFrozen");
		player.removeAttribute("teleBlocked");
		Poison.appendPoison(player, false, 0);
		Prayer.getInstance().resetAll(player);
		player.getUpdateFlags().sendAnimation(65535, 0);
		player.getSkill().normalize();

		// Award him with tokens for his kills.
		if (player.getAttribute("totalDefeated") != null) {
			player.getInventory().addItem(new Item(6529, (Integer) player.getAttribute("totalDefeated")));
		}

		// Clean the coffer of tokkul gains now.
		player.removeAttribute("totalDefeated");

		participants.remove(player);
		waitingPlayers.add(player);

		// Finally...
		gameStarted = false;
	}

	public void viewOrbs(Player player) {
		// TODO: Orb Viewing
		player.getActionSender().sendSidebarInterface(4, 3209);
	}

	@Override
	public ItemSafety getItemSafety() {
		return ItemSafety.SAFE;
	}

	@Override
	public String getDeadMessage() {
		return null;
	}
}
