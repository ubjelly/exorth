package com.exorth.rs2.content;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.GlobalObjectHandler;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.clip.RSObject;
import com.exorth.rs2.util.clip.RegionClipping;

/**
 * Doors.cfg
 * 
 * @author Rait
 * TODO: Rewrite (Mark)
 */
public class Door extends RSObject {
	public Door(int objectId, int x, int y, int plane, int type, int face) {
		super(objectId, x, y, plane, type, face);
	}

	public void open(Player player) {
		int id = determineId(getId(), getFace(), getPosition());
		int face = determineOrientation(id, getFace(), getPosition()) + 1;
		Position position = determinePosition(id, getFace(), getPosition());
		Door door = new Door(id, position.getX(), position.getY(), position.getZ(), getType(), face);
		GlobalObjectHandler.createGlobalObject(player, door);
		RegionClipping.removeObject(player, getX(), getY(), getHeight());
		RegionClipping.addRSObject(player, getX(), getY(), getHeight(), door);
		player.getActionSender().sendMessage("ID: <col=CC0000>" + getId() + "</col>, Position: (X: <col=CC0000>" + getPosition().getX() + "</col>, Y: <col=CC0000>" + getPosition().getY() + "</col>, Z: <col=CC0000>" + getPosition().getZ() + "</col>), Face: <col=CC0000>" + getFace() + "</col>.");
	}

	private int determineId(int base, int face, Position position) {
		return base;
	}

	private int determineOrientation(int id, int base, Position position) {
		return base;
	}

	private Position determinePosition(int id, int face, Position base) {
		return base;
	}
}