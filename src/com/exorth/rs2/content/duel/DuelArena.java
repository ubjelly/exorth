package com.exorth.rs2.content.duel;

import java.util.ArrayList;
import java.util.Deque;
import java.util.Iterator;

import com.exorth.rs2.content.combat.util.Poison;
import com.exorth.rs2.content.combat.util.Prayer;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.StatementDialogue;
import com.exorth.rs2.content.dialogue.StatementType;
import com.exorth.rs2.content.minigame.impl.DuelSession;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Equipment;
import com.exorth.rs2.model.players.Inventory;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Yeller;
import com.exorth.rs2.model.players.container.Container;
import com.exorth.rs2.model.players.container.ContainerType;
import com.exorth.rs2.network.security.DuelLog;
import com.exorth.rs2.util.Misc;

/**
 * The Duel Arena
 * 
 * @author AkZu & Josh
 * 
 * TODO: Fun weapons, etc.
 * 
 */
public class DuelArena {

	private static DuelArena instance;

	public static DuelArena getInstance() {
		if (instance == null) {
			instance = new DuelArena();
		}
		return instance;
	}

	/**
	 * Some dueling constants
	 */
	public static final short[] screen2_txt_fields = { 8242, 8243, 8244, 8245, 8246, 8247, 8248, 8249, 8251, 8252, 8253 };

	public void handleChallengeRequest(Player player, Player otherPlayer) {
		// Return as some of our players are null.
		if (player == null || !player.isLoggedIn() || otherPlayer == null || !otherPlayer.isLoggedIn()) {
			return;
		}

		// Some check
		if (otherPlayer.getIndex() == player.getIndex()) {
			Yeller.alertStaff(player.getUsername() + " may be using a cheat client! Reason: Dueling themselves.");
			return;
		}

		if (otherPlayer.getAttribute("duel_stage") == "WAITING") {
			player.getActionSender().sendMessage("Sending duel offer...");
			otherPlayer.getActionSender().sendMessage(player.getUsername() + ":duelreq:");
			player.setAttribute("duel_stage", "SENT_REQUEST");
		} else if (otherPlayer.getAttribute("duel_stage") == "SENT_REQUEST" && otherPlayer.getClickId() == player.getIndex()) {
			player.setAttribute("duel_stage", "FIRST_WINDOW");
			otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
			otherPlayer.getUpdateFlags().sendFaceToDirection(player.getPosition());
			player.setAttribute("duel_partner", otherPlayer.getIndex());
			otherPlayer.setAttribute("duel_partner", player.getIndex());
			sendFirstWindow(player, otherPlayer);
		} else {
			player.getActionSender().sendMessage("Sending duel offer...");
			otherPlayer.getActionSender().sendMessage(player.getUsername() + ":duelreq:");
			player.setAttribute("duel_stage", "SENT_REQUEST");
		}
	}

	public void accept_stage_1(Player player) {
		if (player.getAttribute("duel_stage") != "FIRST_WINDOW") {
			return;
		}

		if ((Integer) player.getAttribute("duel_partner") == null) {
			return;
		}

		Player otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
		if (otherPlayer == null) {
			return;
		}

		player.setAttribute("duel_stage", "ACCEPT");
		if (otherPlayer.getAttribute("duel_stage") == "ACCEPT") {
			sendSecondWindow(player, otherPlayer);
			return;
		}

		player.getActionSender().sendString("Waiting for other player...", 6684);
		otherPlayer.getActionSender().sendString("Other player has accepted.", 6684);
	}

	public void accept_stage_2(Player player) {
		if (player.getAttribute("duel_stage") != "SECOND_WINDOW") {
			return;
		}

		Player otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
		if (otherPlayer == null) {
			return;
		}

		player.setAttribute("duel_stage", "ACCEPT");
		if (otherPlayer.getAttribute("duel_stage") == "ACCEPT") {
			player.getActionSender().sendMessage("Accepted stake and duel options.");
			otherPlayer.getActionSender().sendMessage("Accepted stake and duel options.");
			start_duel(player, otherPlayer);
			return;
		}

		player.getActionSender().sendString("Waiting for other player...", 6571);
		otherPlayer.getActionSender().sendString("Other player has accepted.", 6571);
	}

	public void start_duel(Player player, Player otherPlayer) {
		// Arena types are full
		if ((player.getAttribute("duel_button_obstacles") != null && (Integer) World.getInstance().getAttribute("duel_obstacle_c") >= 40) || (player.getAttribute("duel_button_obstacles") == null && (Integer) World.getInstance().getAttribute("duel_empty_c") >= 40)) {
			player.setAttribute("duel_stage", "WAITING");
			otherPlayer.setAttribute("duel_stage", "WAITING");

			declineDuel(player, false);

			Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, "All arenas of this type are full! Select another type of arena and", "try again in a few minutes.");
			player.open(dialogue);
			otherPlayer.open(dialogue);
			return;
		}

		player.setAttribute("duel_stage", "INGAME");
		otherPlayer.setAttribute("duel_stage", "INGAME");

		// A lot of stuff here: Set player hints, etc.

		// Stats before start of the game
		player.getSkill().normalize();
		otherPlayer.getSkill().normalize();

		// No Prayer rule
		if (player.getAttribute("duel_button_prayer") != null) {
			Prayer.getInstance().resetAll(player);
			Prayer.getInstance().resetAll(otherPlayer);
		}

		// Remove any armour if rule(s) are set
		if (player.getAttribute("duel_button_helmet") != null) {
			player.getEquipment().unequip(0);
			otherPlayer.getEquipment().unequip(0);
		}
		if (player.getAttribute("duel_button_cape") != null) {
			player.getEquipment().unequip(1);
			otherPlayer.getEquipment().unequip(1);
		}
		if (player.getAttribute("duel_button_amulet") != null) {
			player.getEquipment().unequip(2);
			otherPlayer.getEquipment().unequip(2);
		}
		if (player.getAttribute("duel_button_arrows") != null) {
			player.getEquipment().unequip(13);
			otherPlayer.getEquipment().unequip(13);
		}
		if (player.getAttribute("duel_button_weapon") != null) {
			player.getEquipment().unequip(3);
			otherPlayer.getEquipment().unequip(3);
		}
		if (player.getAttribute("duel_button_chest") != null) {
			player.getEquipment().unequip(4);
			otherPlayer.getEquipment().unequip(4);
		}
		if (player.getAttribute("duel_button_shield") != null) {
			player.getEquipment().unequip(5);
			otherPlayer.getEquipment().unequip(5);
		}
		if (player.getAttribute("duel_button_legs") != null) {
			player.getEquipment().unequip(7);
			otherPlayer.getEquipment().unequip(7);
		}
		if (player.getAttribute("duel_button_gloves") != null) {
			player.getEquipment().unequip(9);
			otherPlayer.getEquipment().unequip(9);
		}
		if (player.getAttribute("duel_button_boots") != null) {
			player.getEquipment().unequip(10);
			otherPlayer.getEquipment().unequip(10);
		}
		if (player.getAttribute("duel_button_ring") != null) {
			player.getEquipment().unequip(12);
			otherPlayer.getEquipment().unequip(12);
		}

		// 40 pairs of two players allowed.
		Course course = null;

		if (player.getAttribute("duel_button_obstacles") == null) {
			if ((Integer) World.getInstance().getAttribute("duel_empty_a") < 40) {
				course = Course.EMPTY_A;
			} else if ((Integer) World.getInstance().getAttribute("duel_empty_b") < 40) {
				course = Course.EMPTY_B;
			} else {
				course = Course.EMPTY_C;
			}
		} else {
			if ((Integer) World.getInstance().getAttribute("duel_obstacle_a") < 40) {
				course = Course.OBSTACLE_A;
			} else if ((Integer) World.getInstance().getAttribute("duel_obstacle_b") < 40) {
				course = Course.OBSTACLE_B;
			} else {
				course = Course.OBSTACLE_C;
			}
		}

		player.setAttribute("duel_course", "duel_" + course.toString().toLowerCase());
		otherPlayer.setAttribute("duel_course", "duel_" + course.toString().toLowerCase());
		World.getInstance().setAttribute("duel_" + course.toString().toLowerCase(), (Integer) World.getInstance().getAttribute("duel_" + course.toString().toLowerCase()) + 1);
		new DuelSession(player, otherPlayer, course).start();
	}

	public void declineDuel(Player player, boolean logout) {
		System.err.println("Declining duel... " + player);

		if ((Integer) player.getAttribute("duel_partner") == null) {
			return;
		}

		Player otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
		if (otherPlayer == null) {
			return;
		}

		player.getActionSender().removeInterfaces();
		otherPlayer.getActionSender().removeInterfaces();

		if (player.getAttribute("duel_stage") == "INGAME") {
			send_victory(otherPlayer, logout);
		} else if (player.getAttribute("duel_stage") == "VICTORY") {
			give_won_items(player);
		} else {
			giveBackItems(player, otherPlayer);
			player.setAttribute("duel_stage", "WAITING");
			otherPlayer.setAttribute("duel_stage", "WAITING");
			player.removeAttribute("duel_partner");
			otherPlayer.removeAttribute("duel_partner");
			resetAttributes(player);
			resetAttributes(otherPlayer);
		}
	}

	public void send_victory(Player player, boolean logout) {
		ArrayList<Item> pStake = new ArrayList<Item>();
		ArrayList<Item> oStake = new ArrayList<Item>();

		Player otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

		if (otherPlayer == null) {
			return;
		}

		System.err.println("Sending victory... " + player);

		player.setCanWalk(true);
		player.setSpecialAttackActive(false);
		player.setSpecialAmount(10);
		player.setCombatTimer(0);
		player.removeAttribute("isFrozen");
		Poison.appendPoison(player, false, 0);
		Prayer.getInstance().resetAll(player);
		player.getSkill().normalize();

		player.clipTeleport((Position) player.getMinigame().getEndPosition()[0], (Integer) player.getMinigame().getEndPosition()[1], (Integer) player.getMinigame().getEndPosition()[2], (Integer) player.getMinigame().getEndPosition()[3]);

		World.getInstance().setAttribute((String) player.getAttribute("duel_course"), (Integer) World.getInstance().getAttribute((String) player.getAttribute("duel_course")) - 1);

		// Last 50 duels.
		@SuppressWarnings("unchecked")
		Deque<String> last50_duels = (Deque<String>) World.getInstance().getAttribute("last50_duels");

		last50_duels.addFirst(player.getUsername() + " (" + player.getSkill().getCombatLevel() + ") beat " + otherPlayer.getUsername() + " (" + otherPlayer.getSkill().getCombatLevel() + ")");

		if (last50_duels.size() == 50) {
			last50_duels.removeLast();
		}

		World.getInstance().setAttribute("last50_duels", last50_duels);

		player.removeAttribute("duel_course");
		otherPlayer.removeAttribute("duel_course");

		// If we have died, the dead listener will handle these.
		if (logout) {
			otherPlayer.clipTeleport((Position) player.getMinigame().getEndPosition()[0], (Integer) player.getMinigame().getEndPosition()[1], (Integer) player.getMinigame().getEndPosition()[2], (Integer) player.getMinigame().getEndPosition()[3]);
			otherPlayer.setCanWalk(true);
			otherPlayer.setSpecialAttackActive(false);
			otherPlayer.setSpecialAmount(10);
			otherPlayer.setCombatTimer(0);
			otherPlayer.setMinigame(null);
			otherPlayer.removeAttribute("isFrozen");
			Poison.appendPoison(otherPlayer, false, 0);
			Prayer.getInstance().resetAll(otherPlayer);
			otherPlayer.getSkill().normalize();
		}

		player.setMinigame(null);

		Container won_items = new Container(ContainerType.STANDARD, 28);
		for (int kk = 0; kk < otherPlayer.getDuelInventory().size(); kk++) {
			if (otherPlayer.getDuelInventory().get(kk) != null) {
				won_items.add(otherPlayer.getDuelInventory().get(kk));
				oStake.add(otherPlayer.getDuelInventory().get(kk));
			}
		}

		otherPlayer.getDuelInventory().clear();

		if (player.getDuelInventory().size() != 0) {
			// Give the player back his staked items
			for (int kk = 0; kk < player.getDuelInventory().size(); kk++) {
				if (player.getDuelInventory().get(kk) != null) {
					player.getInventory().addItem(player.getDuelInventory().get(kk));
					pStake.add(player.getDuelInventory().get(kk));
				}
			}
		}

		player.getDuelInventory().clear();
		player.setAttribute("duel_won_items", won_items);
		otherPlayer.setAttribute("duel_stage", "WAITING");
		player.getActionSender().sendUpdateItems(6822, won_items.toArray());

		// Reset a lot of stuff
		resetAttributes(player);
		resetAttributes(otherPlayer);

		player.getActionSender().createPlayerHints(0);
		otherPlayer.getActionSender().createPlayerHints(0);

		player.getActionSender().sendString(otherPlayer.getUsername(), 6840);
		player.getActionSender().sendString("" + otherPlayer.getSkill().getCombatLevel(), 6839);

		player.getActionSender().sendMessage("Well done! You have defeated " + otherPlayer.getUsername() + "!");
		player.getActionSender().sendInterface(6733);

		player.setAttribute("duel_stage", "VICTORY");
		recordDuel(player, otherPlayer, pStake, oStake, player.getUsername());
	}

	public void give_won_items(Player player) {
		System.err.println("Giving won items to: " + player);
		Player otherPlayer = (player.getAttribute("duel_partner") == null ? null : World.getPlayer((Integer) player.getAttribute("duel_partner")));

		if (((Container) player.getAttribute("duel_won_items")).size() != 0) {
			// Give the player the won items!
			for (int kk = 0; kk < ((Container) player.getAttribute("duel_won_items")).size(); kk++) {
				if (((Container) player.getAttribute("duel_won_items")).get(kk) != null) {
					player.getInventory().addItem(((Container) player.getAttribute("duel_won_items")).get(kk));
				}
			}
		}

		player.removeAttribute("duel_won_items");

		player.setMinigame(null);

		if (otherPlayer != null) {
			otherPlayer.setMinigame(null);
		}

		resetAttributes(player);
		if (otherPlayer != null) {
			resetAttributes(otherPlayer);
		}

		player.removeAttribute("duel_partner");

		if (otherPlayer != null) {
			otherPlayer.removeAttribute("duel_partner");
		}

		player.setAttribute("duel_stage", "WAITING");
	}

	public void giveBackItems(Player player, Player otherPlayer) {
		for (int i = 0; i < Inventory.SIZE; i++) {
			if (player.getDuelInventory().get(i) != null) {
				Item item = player.getDuelInventory().get(i);
				player.getDuelInventory().remove(item);
				player.getInventory().addItem(item);
			}
		}
		for (int i = 0; i < Inventory.SIZE; i++) {
			if (otherPlayer.getDuelInventory().get(i) != null) {
				Item item = otherPlayer.getDuelInventory().get(i);
				otherPlayer.getDuelInventory().remove(item);
				otherPlayer.getInventory().addItem(item);
			}
		}
	}

	public void offerItem(Player player, int slot, int amount) {
		Player otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));

		if (player == null || !player.isLoggedIn() || otherPlayer == null || !otherPlayer.isLoggedIn()) {
			return;
		}

		if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
			return;
		}

		// The item we're trying to offer to our duel inventory
		Item inv_item = player.getInventory().get(slot);

		if (inv_item == null || amount < 1) {
			return;
		}

		// Corrects the amount
		if (amount > player.getInventory().getItemContainer().getCount(inv_item.getId())) {
			amount = player.getInventory().getItemContainer().getCount(inv_item.getId());
		}

		// If we have any same items with same id as what we're trying to offer...
		Item duel_item = player.getDuelInventory().getById(inv_item.getId());
		// Possible item that our opponent has in their inventory
		Item others_invItem = otherPlayer.getInventory().getItemContainer().getById(inv_item.getId());
		// Possible item that our opponent has in their duel inventory
		Item others_duelItem = otherPlayer.getDuelInventory().getById(inv_item.getId());
		// Possible item that our opponent has equipped in their Equipment
		Item equiped_item = null;

		// Untradeables not allowed...
		if (ItemManager.getInstance().isUntradeable(inv_item.getId())) {
			player.getActionSender().sendMessage("You can't stake that item.");
			return;
		}

		System.err.println("Trying to offer amount: " + amount);

		// Checking space required in inventory containers: Our inventory, partners inventory
		if (inv_item.getDefinition().isStackable()) {
			if (duel_item != null || others_invItem != null || others_duelItem != null) {
				// Item can be wielded
				if (Equipment.getEquipmentSlot(inv_item.getId()) != -1) {
					// Other player has equipped the item
					if (otherPlayer.getEquipment().get(Equipment.getEquipmentSlot(inv_item.getId())) != null) {
						// Equipment slot is disabled...
						if (otherPlayer.getAttribute(equipment_slot_to_string(Equipment.getEquipmentSlot(inv_item.getId()))) != null) {
							equiped_item = otherPlayer.getEquipment().get(Equipment.getEquipmentSlot(inv_item.getId()));
						}
					}
				}

				// We don't have same item in our Duel staking inventory
				// But other has same item in their Inventory
				if (duel_item == null && others_invItem != null) {
					if (Integer.MAX_VALUE - others_invItem.getCount() < amount) {
						amount = Integer.MAX_VALUE - others_invItem.getCount();
					}
				}
				// Else if we have staked item in our Duel staking inventory
				else if (duel_item != null) {
					// And if other has the item in their Inventory
					if (others_invItem != null) {
						if (Integer.MAX_VALUE - (duel_item.getCount() + others_invItem.getCount()) < amount) {
							amount = Integer.MAX_VALUE - (duel_item.getCount() + others_invItem.getCount());
						}
					}
				}

				// What about if other has staked duel item in their duel stake
				// and in inv and we do too
				if (others_duelItem != null) {
					if (duel_item != null) {
						if (others_invItem != null) {
							if (Integer.MAX_VALUE - (others_duelItem.getCount() + others_invItem.getCount() + duel_item.getCount()) < amount) {
								amount = Integer.MAX_VALUE - (others_duelItem.getCount() + others_invItem.getCount() + duel_item.getCount());
							}
						}
					} else {
						if (others_invItem != null) {
							if (Integer.MAX_VALUE - (others_duelItem.getCount() + others_invItem.getCount()) < amount) {
								amount = Integer.MAX_VALUE - (others_duelItem.getCount() + others_invItem.getCount());
							}
						}
					}
				}

				// What about if other has item in their equipment and the slot is disabled?
				if (equiped_item != null) {
					if (Integer.MAX_VALUE - (equiped_item.getCount() + (others_invItem != null ? others_invItem.getCount() : 0) + (others_duelItem != null ? others_duelItem.getCount() : 0)) < amount) {
						amount = Integer.MAX_VALUE - (equiped_item.getCount() + (others_invItem != null ? others_invItem.getCount() : 0) + (others_duelItem != null ? others_duelItem.getCount() : 0));
					}
				}
			}

			if (amount <= 0) {
				player.getActionSender().sendMessage("Max item stack reached.");
				return;
			}
		} else {
			if (amount > otherPlayer.getInventory().getItemContainer().freeSlots()) {
				amount = otherPlayer.getInventory().getItemContainer().freeSlots();
			}

			Container non_stackables = new Container(ContainerType.STANDARD, 28);

			for (Item item : otherPlayer.getDuelInventory().toArray()) {
				if (item != null && !item.getDefinition().isStackable()) {
					non_stackables.add(item);
				}
			}

			for (Item item : player.getDuelInventory().toArray()) {
				if (item != null && !item.getDefinition().isStackable()) {
					non_stackables.add(item);
				}
			}

			if (otherPlayer.getInventory().getItemContainer().freeSlots() <= non_stackables.size()) {
				player.getActionSender().sendMessage("Opponent doesn't have enough inventory space for this stake.");
				return;
			}
		}

		if (!inv_item.getDefinition().isStackable()) {
			for (int i = 0; i < amount; i++) {
				if (amount == 1) {
					player.getInventory().removeItemSlot(slot);
				} else {
					player.getInventory().removeItem(new Item(inv_item.getId(), 1));
				}
			}
		} else {
			player.getInventory().removeItem(new Item(inv_item.getId(), amount));
		}

		if (duel_item != null && inv_item.getDefinition().isStackable()) {
			player.getDuelInventory().set(player.getDuelInventory().getSlotById(inv_item.getId()), new Item(duel_item.getId(), duel_item.getCount() + amount));
		} else {
			player.getDuelInventory().add(new Item(inv_item.getId(), amount));
		}

		player.getActionSender().sendUpdateItems(6669, player.getDuelInventory().toArray());
		otherPlayer.getActionSender().sendUpdateItems(6670, player.getDuelInventory().toArray());

		// Update the items
		player.getActionSender().sendUpdateItems(6574, player.getInventory().getItemContainer().toArray());
		otherPlayer.getActionSender().sendUpdateItems(6574, otherPlayer.getInventory().getItemContainer().toArray());

		player.setAttribute("duel_stage", "FIRST_WINDOW");
		otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");

		player.getActionSender().sendString("", 6684);
		otherPlayer.getActionSender().sendString("", 6684);
	}

	public void removeItem(Player player, int slot, int amount) {
		Player otherPlayer = World.getPlayer((Integer) player.getAttribute("duel_partner"));
		if (player == null || !player.isLoggedIn() || otherPlayer == null || !otherPlayer.isLoggedIn()) {
			return;
		}

		if (player.getAttribute("duel_stage") != "FIRST_WINDOW" && player.getAttribute("duel_stage") != "ACCEPT") {
			return;
		}

		Item itemOnScreen = player.getDuelInventory().get(slot);
		if (itemOnScreen == null) {
			return;
		}

		int itemOnScreenAmount = player.getDuelInventory().getCount(itemOnScreen.getId());
		if (itemOnScreenAmount > amount) {
			itemOnScreenAmount = amount;
		}

		player.getInventory().addItem(new Item(itemOnScreen.getId(), itemOnScreenAmount));
		player.getDuelInventory().remove(new Item(itemOnScreen.getId(), itemOnScreenAmount), slot);

		player.getActionSender().sendUpdateItems(6669, player.getDuelInventory().toArray());
		otherPlayer.getActionSender().sendUpdateItems(6670, player.getDuelInventory().toArray());
		player.getActionSender().sendUpdateItems(6574, player.getInventory().getItemContainer().toArray());
		otherPlayer.getActionSender().sendUpdateItems(6574, otherPlayer.getInventory().getItemContainer().toArray());

		player.setAttribute("duel_stage", "FIRST_WINDOW");
		otherPlayer.setAttribute("duel_stage", "FIRST_WINDOW");
		player.getActionSender().sendString("", 6684);
		otherPlayer.getActionSender().sendString("", 6684);
	}

	private void sendFirstWindow(Player player, Player otherPlayer) {
		// Update the staked items to be empty 
		// (TODO: Modify method to allow null args.. hence we're sending empty "inventories")
		player.getActionSender().sendUpdateItems(6669, player.getDuelInventory().toArray());
		player.getActionSender().sendUpdateItems(6670, player.getDuelInventory().toArray());
		otherPlayer.getActionSender().sendUpdateItems(6669, otherPlayer.getDuelInventory().toArray());
		otherPlayer.getActionSender().sendUpdateItems(6670, otherPlayer.getDuelInventory().toArray());

		// Send the scrollers for both stake item boxes to start from up.
		player.getActionSender().setScrollPosition(6665, 0);
		player.getActionSender().setScrollPosition(6666, 0);
		otherPlayer.getActionSender().setScrollPosition(6665, 0);
		otherPlayer.getActionSender().setScrollPosition(6666, 0);

		// Send the players equipment
		player.getActionSender().sendUpdateItems(13824, player.getEquipment().getItemContainer().toArray());
		otherPlayer.getActionSender().sendUpdateItems(13824, otherPlayer.getEquipment().getItemContainer().toArray());

		// Clear some text
		player.getActionSender().sendString("", 6684);
		otherPlayer.getActionSender().sendString("", 6684);

		// Send opponents name + combat level (colored)
		player.getActionSender().sendString("Dueling with: " + otherPlayer.getUsername() + "		Opponent's Combat Level: " + Misc.getHighightingColor(player.getSkill().getCombatLevel(), otherPlayer.getSkill().getCombatLevel()) + otherPlayer.getSkill().getCombatLevel(), 6671);
		otherPlayer.getActionSender().sendString("Dueling with: " + player.getUsername() + "		Opponent's Combat Level: " + Misc.getHighightingColor(otherPlayer.getSkill().getCombatLevel(), player.getSkill().getCombatLevel()) + player.getSkill().getCombatLevel(), 6671);

		// Update the inventory with Stake 1, 5, 10, All, X options
		player.getActionSender().sendUpdateItems(6574, player.getInventory().getItemContainer().toArray());
		otherPlayer.getActionSender().sendUpdateItems(6574, otherPlayer.getInventory().getItemContainer().toArray());

		// Refresh the configs
		player.getActionSender().sendConfig(286, 0);
		otherPlayer.getActionSender().sendConfig(286, 0);

		// Send the Duel Window #1
		player.getActionSender().sendItfInvOverlay(6575, 6573);
		otherPlayer.getActionSender().sendItfInvOverlay(6575, 6573);
	}

	private void sendSecondWindow(Player player, Player otherPlayer) {
		// We're at the Second screen.
		player.setAttribute("duel_stage", "SECOND_WINDOW");
		otherPlayer.setAttribute("duel_stage", "SECOND_WINDOW");

		// Remove gaps of the containers
		player.getDuelInventory().shift();
		otherPlayer.getDuelInventory().shift();

		// Update the items in second set of inv containers
		player.getActionSender().sendUpdateItems(6509, player.getDuelInventory().toArray());
		player.getActionSender().sendUpdateItems(6502, otherPlayer.getDuelInventory().toArray());
		otherPlayer.getActionSender().sendUpdateItems(6509, otherPlayer.getDuelInventory().toArray());
		otherPlayer.getActionSender().sendUpdateItems(6502, player.getDuelInventory().toArray());
		player.getActionSender().setScrollMax(6500, 20 + (player.getDuelInventory().size() * 12));
		player.getActionSender().setScrollMax(6501, 20 + (otherPlayer.getDuelInventory().size() * 12));
		otherPlayer.getActionSender().setScrollMax(6500, 20 + (otherPlayer.getDuelInventory().size() * 12));
		otherPlayer.getActionSender().setScrollMax(6501, 20 + (player.getDuelInventory().size() * 12));

		// Send the scrollers for both stake item boxes to start from up.
		player.getActionSender().setScrollPosition(6500, 0);
		player.getActionSender().setScrollPosition(6501, 0);
		otherPlayer.getActionSender().setScrollPosition(6500, 0);
		otherPlayer.getActionSender().setScrollPosition(6501, 0);

		// Clear some text
		player.getActionSender().sendString("", 6571);
		otherPlayer.getActionSender().sendString("", 6571);

		player.getActionSender().sendString(player.getDuelInventory().size() == 0 ? "Absolutely nothing!" : "", 6516);
		player.getActionSender().sendString(otherPlayer.getDuelInventory().size() == 0 ? "Absolutely nothing!" : "", 6517);
		otherPlayer.getActionSender().sendString(otherPlayer.getDuelInventory().size() == 0 ? "Absolutely nothing!" : "", 6516);
		otherPlayer.getActionSender().sendString(player.getDuelInventory().size() == 0 ? "Absolutely nothing!" : "", 6517);

		player.getActionSender().sendString("", 8238);
		otherPlayer.getActionSender().sendString("", 8238);
		player.getActionSender().sendString("", 8239);
		otherPlayer.getActionSender().sendString("", 8239);
		player.getActionSender().sendString("", 8240);
		otherPlayer.getActionSender().sendString("", 8240);
		player.getActionSender().sendString("", 8241);
		otherPlayer.getActionSender().sendString("", 8241);

		boolean flag = false, flag1 = false, flag2 = false;

		// Send the rule information text
		if (player.getAttribute("duel_button_helmet") != null || player.getAttribute("duel_button_cape") != null || player.getAttribute("duel_button_amulet") != null || player.getAttribute("duel_button_arrows") != null || player.getAttribute("duel_button_weapon") != null || player.getAttribute("duel_button_chest") != null || player.getAttribute("duel_button_shield") != null || player.getAttribute("duel_button_legs") != null || player.getAttribute("duel_button_gloves") != null || player.getAttribute("duel_button_boots") != null || player.getAttribute("duel_button_ring") != null) {
			player.getActionSender().sendString("Some worn items will be taken off.", 8250);
			otherPlayer.getActionSender().sendString("Some worn items will be taken off.", 8250);
			flag = true;
		}

		if (player.getAttribute("duel_button_drinks") != null) {
			player.getActionSender().sendString("Boosted stats will be restored.", flag ? 8238 : 8250);
			otherPlayer.getActionSender().sendString("Boosted stats will be restored.", flag ? 8238 : 8250);
			flag1 = true;
		}

		if (player.getAttribute("duel_button_prayer") != null) {
			player.getActionSender().sendString("Existing prayers will be stopped.", flag1 && flag ? 8239 : flag1 ? 8238 : 8250);
			otherPlayer.getActionSender().sendString("Existing prayers will be stopped.", flag1 && flag ? 8239 : flag1 ? 8238 : 8250);
			flag2 = true;
		}

		// Nothing has changed.
		if (!flag && !flag1 && !flag2) {
			player.getActionSender().sendString("Nothing will be changed.", 8250);
			otherPlayer.getActionSender().sendString("Nothing will be changed.", 8250);
		}

		for (int kk = 1; kk < screen2_txt_fields.length; kk++) {
			player.getActionSender().sendString("", screen2_txt_fields[kk]);
			otherPlayer.getActionSender().sendString("", screen2_txt_fields[kk]);
		}

		player.getActionSender().sendString("You will fight using normal combat.", screen2_txt_fields[0]);
		otherPlayer.getActionSender().sendString("You will fight using normal combat.", screen2_txt_fields[0]);

		// The texts
		byte current_index_pos = 0;
		/**
		 * You cannot move.
		 * You cannot use Ranged attacks.
		 * You cannot use Melee attacks.
		 * You cannot use special attacks.
		 * You cannot use drinks.
		 * You cannot use food.
		 * You cannot use Prayer.
		 * You cannot use 2h weapons such as bows.
		 * You cannot forfeit.
		 */

		if (player.getAttribute("duel_button_ranged") != null) {
			player.getActionSender().sendString("You cannot use Ranged attacks.", screen2_txt_fields[current_index_pos]);
			otherPlayer.getActionSender().sendString("You cannot use Ranged attacks.", screen2_txt_fields[current_index_pos]);
			current_index_pos++;
		}

		if (player.getAttribute("duel_button_melee") != null) {
			player.getActionSender().sendString("You cannot use Melee attacks.", screen2_txt_fields[current_index_pos]);
			otherPlayer.getActionSender().sendString("You cannot use Melee attacks.", screen2_txt_fields[current_index_pos]);
			current_index_pos++;
		}

		if (player.getAttribute("duel_button_magic") != null) {
			player.getActionSender().sendString("You cannot use Magic attacks.", screen2_txt_fields[current_index_pos]);
			otherPlayer.getActionSender().sendString("You cannot use Magic attacks.", screen2_txt_fields[current_index_pos]);
			current_index_pos++;
		}

		if (player.getAttribute("duel_button_sp_atk") != null) {
			player.getActionSender().sendString("You cannot use Special attacks.", screen2_txt_fields[current_index_pos]);
			otherPlayer.getActionSender().sendString("You cannot use Special attacks.", screen2_txt_fields[current_index_pos]);
			current_index_pos++;
		}

		if (player.getAttribute("duel_button_fun_wep") != null) {
			player.getActionSender().sendString("You can only attack with 'fun' weapons.", screen2_txt_fields[current_index_pos]);
			otherPlayer.getActionSender().sendString("You can only attack with 'fun' weapons.", screen2_txt_fields[current_index_pos]);
			current_index_pos++;
		}

		if (player.getAttribute("duel_button_forfeit") != null) {
			player.getActionSender().sendString("You cannot forfeit the duel.", screen2_txt_fields[current_index_pos]);
			otherPlayer.getActionSender().sendString("You cannot forfeit the duel.", screen2_txt_fields[current_index_pos]);
			current_index_pos++;
		}

		if (player.getAttribute("duel_button_food") != null) {
			player.getActionSender().sendString("You cannot use food.", screen2_txt_fields[current_index_pos]);
			otherPlayer.getActionSender().sendString("You cannot use food.", screen2_txt_fields[current_index_pos]);
			current_index_pos++;
		}

		if (player.getAttribute("duel_button_movement") != null) {
			player.getActionSender().sendString("You cannot move.", screen2_txt_fields[current_index_pos]);
			otherPlayer.getActionSender().sendString("You cannot move.", screen2_txt_fields[current_index_pos]);
			current_index_pos++;
		}

		if (player.getAttribute("duel_button_obstacles") != null) {
			player.getActionSender().sendString("There will be obstacles in the arena.", screen2_txt_fields[current_index_pos]);
			otherPlayer.getActionSender().sendString("There will be obstacles in the arena.", screen2_txt_fields[current_index_pos]);
			current_index_pos++;
		}

		if (player.getAttribute("duel_button_shield") != null) {
			player.getActionSender().sendString("You can't use 2h weapons such as bows.", screen2_txt_fields[current_index_pos]);
			otherPlayer.getActionSender().sendString("You can't use 2h weapons such as bows.", screen2_txt_fields[current_index_pos]);
			current_index_pos++;
		}

		// Send the Duel Window #2
		player.getActionSender().sendInterface(6412);
		otherPlayer.getActionSender().sendInterface(6412);
	}

	public void open_scoreboard(Player player) {
		byte index = 0;
		boolean flag = false;

		@SuppressWarnings("unchecked")
		Deque<String> last50_duels = (Deque<String>) World.getInstance().getAttribute("last50_duels");

		Iterator<String> i = last50_duels.iterator();

		while (i.hasNext()) {
			if (index == 10 && !flag) {
				flag = true;
				index = 0;
			}
			player.getActionSender().sendString("" + i.next(), (flag ? 8578 : 6402) + index);
			index++;
		}
		player.getActionSender().sendInterface(6308);
	}

	private String equipment_slot_to_string(int index) {
		switch (index) {
			case 0:
				return "duel_button_helmet";
			case 1:
				return "duel_button_cape";
			case 2:
				return "duel_button_amulet";
			case 13:
				return "duel_button_arrows";
			case 3:
				return "duel_button_weapon";
			case 4:
				return "duel_button_chest";
			case 5:
				return "duel_button_shield";
			case 7:
				return "duel_button_legs";
			case 9:
				return "duel_button_gloves";
			case 10:
				return "duel_button_boots";
			case 12:
				return "duel_button_ring";
		}
		return null;
	}

	/**
	 * Called for reseting duel attributes at end of duel i.e
	 */
	public void resetAttributes(Player player) {
		player.setAttribute("duel_button_config", 0);
		player.setAttribute("duel_equipment_req_space", 0);
		player.removeAttribute("duel_button_ranged");
		player.removeAttribute("duel_button_melee");
		player.removeAttribute("duel_button_magic");
		player.removeAttribute("duel_button_sp_atk");
		player.removeAttribute("duel_button_fun_wep");
		player.removeAttribute("duel_button_forfeit");
		player.removeAttribute("duel_button_drinks");
		player.removeAttribute("duel_button_food");
		player.removeAttribute("duel_button_prayer");
		player.removeAttribute("duel_button_movement");
		player.removeAttribute("duel_button_obstacles");
		player.removeAttribute("duel_button_helmet");
		player.removeAttribute("duel_button_cape");
		player.removeAttribute("duel_button_amulet");
		player.removeAttribute("duel_button_arrows");
		player.removeAttribute("duel_button_weapon");
		player.removeAttribute("duel_button_chest");
		player.removeAttribute("duel_button_shield");
		player.removeAttribute("duel_button_legs");
		player.removeAttribute("duel_button_gloves");
		player.removeAttribute("duel_button_boots");
		player.removeAttribute("duel_button_ring");
	}
	
	/**
	 * Converts all needed information to a string in order to record duels.
	 */
	private void recordDuel(Player player, Player opponent, ArrayList<Item> playerStake, ArrayList<Item> opponentStake, String winner) {
		String pStake = "";
		String oStake = "";
		
		/**
		 * Get all the items and convert them to a string format.
		 */
		for(int i = 0; i < playerStake.size(); i++) {
			if (i != playerStake.size() - 1) {
				pStake = pStake + playerStake.get(i).getName() + ", " + playerStake.get(i).getCount() + ", ";
			} else {
				pStake = pStake + playerStake.get(i).getName() + ", " + playerStake.get(i).getCount();
			}
		}
		for(int i = 0; i < opponentStake.size(); i++) {
			if (i != playerStake.size() - 1) {
				oStake = oStake + opponentStake.get(i).getName() + ", " + opponentStake.get(i).getCount() + ", ";
			} else {
				oStake = oStake + opponentStake.get(i).getName() + ", " + opponentStake.get(i).getCount();
			}
		}
		
		/**
		 * Set the bet to nothing if it's empty.
		 */
		if (playerStake.size() == 0) {
			pStake = "Nothing";
		}
		if (opponentStake.size() == 0) {
			oStake = "Nothing";
		}
		
		DuelLog.recordDuel(player.getUsername(), opponent.getUsername(), pStake, oStake, winner);
	}
}
