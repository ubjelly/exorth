package com.exorth.rs2.content.quest;

import java.util.Map.Entry;

/**
 * A convenience class for simplifying quest retrieval from the quest database.
 * 
 * @author Joshua Barry
 * 
 */
public class QuestEntry implements Entry<String, Integer> {

	/**
	 * The name of this quest.
	 */
	private final String name;

	/**
	 * The button id of the quest. The one found on the quest tab.
	 */
	private final int button;

	public QuestEntry(String name, int button) {
		this.name = name;
		this.button = button;
	}

	@Override
	public String getKey() {
		return name;
	}

	@Override
	public Integer getValue() {
		return button;
	}

	@Override
	public Integer setValue(Integer value) {
		return button;
	}
}