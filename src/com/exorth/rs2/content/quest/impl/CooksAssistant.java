package com.exorth.rs2.content.quest.impl;

import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;

/**
 * 
 * @author Conner Kendall <Emnesty>
 * 
 */
public class CooksAssistant extends QuestStructure {

	@Override
	public boolean hasRequirements(Player player) {
		return player.isLoggedIn();
	}

	@Override
	public boolean deathListener(Player player) {
		return false;
	}

	@Override
	public boolean defeatListener(Player player) {
		return false;
	}

	@Override
	public boolean dialougeListener(Player player) {
		if (player.getQuestStorage().hasFinished(this)) {
			end(player);
		}
		return true;
	}

	@Override
	public Item[] getRewards() {
		return null;
	}

	@Override
	public String getAttributeName() {
		return "CooksAssistant";
	}

	@Override
	public String getQuestName() {
		return "Cook's Assistant";
	}

	@Override
	public short getQuestPointAward() {
		return 1;
	}

	@Override
	public double[][] getExperienceAward() {
		return new double[][]{ { Skill.COOKING, 300 } };
	}

	@Override
	public String getAdditionalMessage() {
		return null;
	}

	@Override
	public short getFinalStage() {
		return 2;
	}

	@Override
	public short getQuestButton() {
		return 7333;
	}
}
