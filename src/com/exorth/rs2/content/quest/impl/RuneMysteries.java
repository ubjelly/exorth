package com.exorth.rs2.content.quest.impl;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class RuneMysteries extends QuestStructure {

	@Override
	public boolean hasRequirements(Player player) {
		return player.isLoggedIn();
	}

	@Override
	public boolean deathListener(Player player) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean defeatListener(Player player) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean dialougeListener(Player player) {
		Item tali = new Item(1438, 1);
		Item notes = new Item(291, 1);
		if (player.getQuestStorage().hasStarted(this)) {
			switch (player.getQuestStorage().getState(this)) {
				case 1:
					if (((Npc) player.getInteractingEntity()).getDefinition().getName().toLowerCase().contains("duke")) {
						player.getInventory().addItem(tali);
					}
					break;
				case 4:
					player.getInventory().addItem(notes);
					break;
			}
		} else if (player.getQuestStorage().hasFinished(this)) {
			if (player.getInventory().removeItem(notes)) {
				end(player);
			}
		}
		return true;
	}

	@Override
	public Item[] getRewards() {
		return new Item[]{ new Item(1438, 1) };
	}

	@Override
	public String getAttributeName() {
		return "RuneMysteries";
	}

	@Override
	public String getQuestName() {
		return "Rune Mysteries";
	}

	@Override
	public short getQuestPointAward() {
		return 1;
	}

	@Override
	public double[][] getExperienceAward() {
		return new double[][]{};
	}

	@Override
	public short getFinalStage() {
		return 5;
	}

	@Override
	public short getQuestButton() {
		return 7335;
	}

	@Override
	public String getAdditionalMessage() {
		return "Runecrafting skill";
	}
}
