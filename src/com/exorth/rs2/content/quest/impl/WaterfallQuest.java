package com.exorth.rs2.content.quest.impl;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.quest.Quest;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Conner Kendall <Emnesty>
 * 
 */
public class WaterfallQuest extends QuestStructure {

	@Override
	public boolean hasRequirements(Player player) {
		return player.isLoggedIn();
	}

	@Override
	public boolean deathListener(Player player) {
		return false;
	}

	@Override
	public boolean defeatListener(Player player) {
		return false;
	}

	@Override
	public boolean dialougeListener(final Player player) {
		switch (player.getQuestStorage().getState(this)) {
			case 4:
				if (!player.getInventory().getItemContainer().contains(2840)) {
					player.getActionSender().sendMessage("You have lost your key!");
					return true;
				}
				final Npc npc = new Npc(306);
				player.setAttribute("currentQuest", this);
				final Dialogue dialogue = new ChatDialogue(player, null, "Could I take this old pebble?");
				dialogue.add(npc, null, "Oh that, yes have it, it's just some old elven junk I", "believe.");
				dialogue.add(npc, new Task(1, true) {
					@Override
					protected void execute() {
						Item item = new Item(294, 1);
						if (player.getInventory().getItemContainer().hasRoomFor(item)) {
							if (player.getInventory().removeItem(new Item(2840, 1))) {
								if (player.getInventory().addItem(item)) {
									player.getActionSender().sendMessage("You give " + npc.getDefinition().getName() + " the key.");
									player.getQuestStorage().setState((Quest) player.getAttribute("currentQuest"), 5);
								}
							}
						}
						this.stop();
					}
				},
				"Thanks a lot for the key traveller. I think I'll wait in", "here until those goblins get bored and leave.");
				player.getActionSender().sendMessage("You look amongst the junk on the floor.");
				World.submit(new Task(2) {
					@Override
					protected void execute() {
						player.getActionSender().sendMessage("Mixed with the junk on the floor you find Glarial's pebble.");
						World.submit(new Task(2) {
							@Override
							protected void execute() {
								player.open(dialogue);
								this.stop();
							}
						});
						this.stop();
					}
				});
				break;
		}

		if (player.getQuestStorage().hasFinished(this)) {
			end(player);
		}
		return true;
	}

	@Override
	public Item[] getRewards() {
		return new Item[]{ new Item(299, 40), new Item(1601, 2), new Item(2357, 2) };
	}

	@Override
	public String getAttributeName() {
		return "WaterfallQuest";
	}

	@Override
	public String getQuestName() {
		return "Waterfall Quest";
	}

	@Override
	public short getQuestPointAward() {
		return 1;
	}

	@Override
	public double[][] getExperienceAward() {
		return new double[][]{ { Skill.ATTACK, 13750 }, { Skill.STRENGTH, 13750 } };
	}

	@Override
	public String getAdditionalMessage() {
		return null;
	}

	@Override
	public short getFinalStage() {
		return 99;
	}

	@Override
	public short getQuestButton() {
		return 7350;
	}
}
