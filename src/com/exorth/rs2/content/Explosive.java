package com.exorth.rs2.content;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Explosive {
	/**
	 * Explodes the item
	 * 
	 * @param player The player who exploded the item.
	 */
	public static void explode(final Player player) {
		if (player.getAttribute("isExploding") != null) {
			return;
		}

		player.setAttribute("isExploding", (byte) 1);
		player.getUpdateFlags().sendAnimation(827);
		player.getActionSender().sendMessage("You drop the potion...");

		World.submit(new Task(2) {
			@Override
			protected void execute() {
				player.getUpdateFlags().sendForceMessage("Ouch! That hurt.");
				player.getActionSender().sendMessage("It explodes and you are wounded!");
				player.hit(15, 1, false);
				player.removeAttribute("isExploding");
				this.stop();
			}
		});
	}
}