package com.exorth.rs2.content;

import java.util.HashMap;

import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;

/**
 * Runescape skill menus which show requirements pertaining to individual skills.
 * @author Stephen
 */
public class SkillMenus {

	/**
	 * All of the information to be displayed.
	 */
	public enum MenuData {

		ATTACK(8654, new int[] {1, 1, 5, 10, 20, 30, 40, 60, 70, 99}, 
				new Item[] {new Item(1291), new Item(1293), new Item(1295), new Item(1297), new Item(1299), new Item(1301), new Item(1303), new Item(1305), new Item(4151), new Item(9748)}, 
				new String[] {"Bronze", "Iron", "Steel", "Black", "Mithril", "Adamant", "Rune", "Dragon", "Abyssal Whip", "Attack Cape"}),

		DEFENCE(8660, new int[] {1, 1, 5, 10, 20, 30, 40, 60, 99}, 
				new Item[] {new Item(1117), new Item(1115), new Item(1119), new Item(1125), new Item(1121), new Item(1123), new Item(1127), new Item(3140), new Item(9754)}, 
				new String[] {"Bronze", "Iron", "Steel", "Black", "Mithril", "Adamant", "Rune", "Dragon", "Defence Cape"}),

		STRENGTH(8657, new int[] {55, 70, 99}, 
				new Item[] {new Item(4153), new Item(4718), new Item(9751)}, 
				new String[] {"Granite Maul (with 55 Attack)", "Dharok's Greataxe (with 70 Attack)", "Strength Cape"}),

		HITPOINTS(8655, new int[] {99}, 
				new Item[] {new Item(9769)}, 
				new String[] {"Hitpoints Cape"}),

		RANGED(8663, new int[] {1, 10, 20, 30, 40, 50, 70, 70, 99}, 
				new Item[] {new Item(841), new Item(843), new Item(849), new Item(853), new Item(857), new Item(861), new Item(4212), new Item(11235), new Item(9757)}, 
				new String[] {"Shortbow", "Oak Shortbow", "Willow Shortbow", "Maple Shortbow", "Yew Shortbow", "Magic Shortbow", "Crystal Bow (with 50 Agility)", "Dark Bow", "Ranging Cape"}),

		PRAYER(8666, new int[] {99}, 
				new Item[] {new Item(9760)}, 
				new String[] {"Prayer Cape"}),

		MAGIC(8669, new int[] {1, 30, 70, 90, 99}, 
				new Item[] {new Item(577), new Item(4091), new Item(6916), new Item(9097), new Item(9763)}, 
				new String[] {"Wizard Robes", "Mystic Robes", "Infinity Robes", "Lunar Robes", "Magic Cape"}),

		COOKING(8665, new int[] {1, 40, 55, 80, 91, 99}, 
				new Item[] {new Item(315), new Item(379), new Item(373), new Item(385), new Item(391), new Item(9802)}, 
				new String[] {"Shrimp (master at 35)", "Lobster (master at 74)", "Swordfish (master at 85)", "Shark", "Manta Ray", "Cooking Cape"}),

		WOODCUTTING(8671, new int[] {1, 15, 30, 45, 60, 75, 99}, 
				new Item[] {new Item(1511), new Item(1521), new Item(1519), new Item(1517), new Item(1515), new Item(1513), new Item(9808)}, 
				new String[] {"Tree", "Oak Tree", "Willow Tree", "Maple Tree", "Yew", "Magic Tree", "Woodcutting Cape"}),

		FLETCHING(8670, new int[] {1, 1, 20, 35, 50, 65, 80, 99}, 
				new Item[] {new Item(52), new Item(841), new Item(843), new Item(849), new Item(853), new Item(857), new Item(861), new Item(9784)}, 
				new String[] {"Arrow Shafts", "Shortbow", "Oak Shortbow", "Willow Shortbow", "Maple Shortbow", "Yew Shortbow", "Magic Shortbow", "Fletching Cape"}),

		FISHING(8662, new int[] {1, 40, 55, 80, 91, 99}, 
				new Item[] {new Item(317), new Item(377), new Item(371), new Item(383), new Item(389), new Item(9799)}, 
				new String[] {"Shrimp", "Lobster", "Swordfish", "Shark", "Manta Ray", "Fishing Cape"}),

		FIREMAKING(8668, new int[] {1, 15, 30, 45, 60, 75, 99}, 
				new Item[] {new Item(1511), new Item(1521), new Item(1519), new Item(1517), new Item(1515), new Item(1513), new Item(9805)}, 
				new String[] {"Logs", "Oak Logs", "Willow Logs", "Maple Logs", "Yew Logs", "Magic Logs", "Firemaking Cape"}),

		CRAFTING(8667, new int[] {1, 7, 9, 11, 14, 18, 57, 60, 63, 99}, 
				new Item[] {new Item(1059), new Item(1061), new Item(1167), new Item(1063), new Item(1129), new Item(1095), new Item(1065), new Item(1099), new Item(1135), new Item(9781)}, 
				new String[] {"Leather Gloves", "Leather Boots", "Leather Cowl", "Leather Vambraces", "Leather Body", "Leather Chaps", "Green D'hide Vamb", "Green D'hide Chaps", "Green D'hide Body", "Crafting Cape"}),

		SMITHING(8659, new int[] {99}, 
				new Item[] {new Item(9796)}, 
				new String[] {"Smithing Cape"}),

		MINING(8656, new int[] {1, 1, 15, 30, 40, 55, 70, 85, 99}, 
				new Item[] {new Item(436), new Item(438), new Item(440), new Item(453), new Item(444), new Item(447), new Item(449), new Item(451), new Item(9793)}, 
				new String[] {"Copper Ore", "Tin Ore", "Iron", "Coal", "Gold", "Mithril", "Adamantite", "Runite", "Mining Cape"}),

		HERBLORE(8661, new int[] {1, 3, 5, 7, 9, 13, 38, 45, 55, 66, 69, 81, 99}, 
				new Item[] {new Item(121), new Item(169), new Item(3042), new Item(113), new Item(133), new Item(175), new Item(139), new Item(145), new Item(157), new Item(163), new Item(2452), new Item(6685), new Item(9775)}, 
				new String[] {"Attack Potion", "Ranging Potion", "Magic Potion", "Strength Potion", "Defence Potion", "Antipoison", "Prayer Potion", "Super Attack", "Super Strength", "Super Defence", "Antifire", "Saradomin Brew", "Herblore Cape"}),

		AGILITY(8658, new int[] {99}, 
				new Item[] {new Item(9772)}, 
				new String[] {"Agility Cape"}),

		THIEVING(8664, new int[] {99}, 
				new Item[] {new Item(9778)}, 
				new String[] {"Thieving Cape"}), 

		SLAYER(12162, new int[] {1, 20, 30, 40, 60, 65, 70, 75, 80, 85, 99}, 
				new Item[] {new Item(4133), new Item(4135), new Item(4138), new Item(4139), new Item(4142), new Item(4148), new Item(4144), new Item(-1), new Item(-1), new Item(4149), new Item(9787)}, 
				new String[] {"Crawling Hand", "Banshee", "Pyrefiend", "Basilisk", "Jelly", "Nechryael", "Abberrant Spectre", "Asyn Shade", "Khazard Warlord", "Abyssal Demon", "Slayer Cape"}), 

		FARMING(13928, new int[] {99}, 
				new Item[] {new Item(9811)}, 
				new String[] {"Farming Cape"}),

		RUNECRAFTING(8672, new int[] {1, 2, 5, 9, 14, 99}, 
				new Item[] {new Item(6422), new Item(6436), new Item(6424), new Item(6426), new Item(6428), new Item(9766)}, 
				new String[] {"Air Runes", "Mind Runes", "Water Runes", "Earth Runes", "Fire Runes", "Runecrafting Cape"}),

		CONSTRUCTION(6996, new int[] {99}, 
				new Item[] {new Item(9790)}, 
				new String[] {"Construction Cape"});

		/**
		 * The id of the action button.
		 */
		private int actionButton;

		/**
		 * The level requirements.
		 */
		private int[] levelRequirements;

		/**
		 * The items behind displayed.
		 */
		private Item[] items;

		/**
		 * The name of the item.
		 */
		private String[] names;

		MenuData(int actionButton, int[] levelRequirements, Item[] models, String[] names) {
			this.actionButton = actionButton;
			this.levelRequirements = levelRequirements;
			this.items = models;
			this.names = names;
		}

		/**
		 * A map of all the menu data by action button id.
		 */
		private static HashMap<Integer, MenuData> menuData = new HashMap<Integer, MenuData>();

		static {
			for (MenuData data : MenuData.values()) {
				menuData.put(data.getActionButton(), data);
			}
		}

		public static MenuData forId(int actionButton) {
			return menuData.get(actionButton);
		}

		public int getActionButton() {
			return actionButton;
		}

		public int[] getLevelRequirements() {
			return levelRequirements;
		}

		public Item[] getModels() {
			return items;
		}

		public String[] getNames() {
			return names;
		}
	}
	
	/**
	 * Enum containing all the data.
	 */
	private MenuData menu;
	
	/**
	 * The ordinal of the skill from the enum.
	 */
	private int index;
	
	/**
	 * Constructs a skill menu.
	 * @param menu The menu for the selected skill.
	 */
	public SkillMenus(MenuData menu) {
		this.menu = menu;
		index = menu.ordinal();
	}
	
	/**
	 * Moves the buttons to the right of the skill menu. This is a pretty cheezy cheaphax...
	 * A better way would be to do it client sided.
	 * @param player The player opening the menu.
	 */
	public void removeSideButtons(Player player) {
		int[] childIds = {8800, 8844, 8813, 8825, 8828, 8838, 8841, 8850, 8860, 8863, 15294, 15304, 15307};
		
		for (int i = 0; i < childIds.length; i++) {
			player.getActionSender().moveComponent(5000, 5000, childIds[i]);
		}
	}
	
	/**
	 * Constructs the skill guide menu.
	 * @param player The player viewing the guide.
	 */
	public void constructMenu(Player player) {
		/**
		 * Open the interface and prepare the header for the selected skill.
		 */
		player.getActionSender().sendInterface(8714);
		player.getActionSender().sendString(Skill.SKILL_NAME[index], 8716);
		player.getActionSender().sendString("", 8849);

		/**
		 * Remove the sidebar buttons.
		 */
		removeSideButtons(player);

		/**
		 * Populate the menu with data.
		 */
		player.getActionSender().sendUpdateItems(8847, menu.getModels());
		int childLevelNode = 8720;
		int childInfoNode = 8760;
		for (int index = 0; index < menu.getLevelRequirements().length; index++) {
			player.getActionSender().sendString("" + menu.getLevelRequirements()[index], childLevelNode);
			player.getActionSender().sendString(menu.getNames()[index], childInfoNode);
			childLevelNode++;
			childInfoNode++;
		}	
		
		/**
		 * Clear the unused slots.
		 */
		for (int i = childLevelNode; i < 8759; i++) {
			player.getActionSender().sendString("", childLevelNode);
			player.getActionSender().sendString("", childInfoNode);
			childLevelNode++;
			childInfoNode++;
		}
	}
	
}