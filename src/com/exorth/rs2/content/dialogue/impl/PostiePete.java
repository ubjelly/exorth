package com.exorth.rs2.content.dialogue.impl;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.Option;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class PostiePete extends DialogueSession<Npc> {

	/* The main Npc */
	private Npc npc;

	public PostiePete(Player player, Npc[] actors) {
		super(player, actors);
		this.npc = actors[0];
	}

	@Override
	public Dialogue evaluate() {
		Dialogue dialogue = new ChatDialogue(npc, null, "Hey " + player.getUsername() + "!", "It's a good day to play some slots.");

		/* Play slots */
		Option playSlots = new Option("I suppose I could play for a bit...", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(npc, null, "Atta boy, will that be manual or automatic play?");

				/* Play manually */
				Option manual = new Option("Manual", new Task(1, true) {
					@Override
					protected void execute() {
						if (!player.getInventory().getItemContainer().contains(995)) {
							Dialogue dialogue = new ChatDialogue(npc, null, "Aw, looks like you don't have any gold!", "Come back some other time, " + player.getUsername() + ".");
							player.open(dialogue);
							player.getActionSender().sendMessage("You don't have any money to bet!");
							this.stop();
						} else {
							player.getActionSender().sendInterface(671);
						}
						this.stop();
					}
				});
				
				/* Play automatically */
				Option automatic = new Option("Automatic", new Task(1, true) {
					@Override
					protected void execute() {
						if (!player.getInventory().getItemContainer().contains(995)) {
							Dialogue dialogue = new ChatDialogue(npc, null, "Aw, looks like you don't have any gold!", "Come back some other time, " + player.getUsername() + ".");
							player.open(dialogue);
							player.getActionSender().sendMessage("You don't have any money to bet!");
							this.stop();
						} else {
							player.setEnterXInterfaceId(2403);
							player.getActionSender().sendEnterX();
						}
						this.stop();
					}
				});
				
				dialogue.add("Select an Option", manual, automatic);
				player.open(dialogue);
				this.stop();
			}
		});

		/* Don't play */
		Option no = new Option("No thanks, my wife says I have a gambling problem.", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(npc, null, "I wonder who wears the pants in that relationship!");
				player.open(dialogue);
				this.stop();
			}
		});

		dialogue.add("Select an Option", playSlots, no);
		return dialogue;
	}
}
