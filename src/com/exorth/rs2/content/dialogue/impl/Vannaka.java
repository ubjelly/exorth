package com.exorth.rs2.content.dialogue.impl;

import java.util.Random;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.Option;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.content.skills.slayer.Slayer;
import com.exorth.rs2.content.skills.slayer.SlayerTask;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Misc;

public class Vannaka extends DialogueSession<Npc> {

	/* The main Npc */
	private Npc npc;

	public Vannaka(Player player, Npc[] actors) {
		super(player, actors);
		this.npc = actors[0];
	}

	@Override
	public Dialogue evaluate() {
		Dialogue dialogue = new ChatDialogue(npc, null, "Can I help you?");

		/* Get a task */
		Option getTask = new Option("I seek a Slayer task.", new Task(1, true) {
			@Override
			protected void execute() {
				if (player.getSkill().getCombatLevel() < 100) {
					Dialogue dialogue = new ChatDialogue(npc, null, "You need a Combat level of 100 for my tasks.");
					player.open(dialogue);
					this.stop();
					return;
				}
				if (player.getSkillLevel(Skill.SLAYER) < 75) {
					Dialogue dialogue = new ChatDialogue(npc, null, "You need level 75 Slayer for my tasks.");
					player.open(dialogue);
					this.stop();
					return;
				}
				if (player.getKillsLeft() != 0) {
					Dialogue dialogue = new ChatDialogue(npc, null, "You already have a task!");
					player.open(dialogue);
				} else {
					Random random = new Random();
					int amount = Misc.randomMinMax(15, 50);
					player.setSlayer(new Slayer(player, SlayerTask.VAN_TASKS[random.nextInt(SlayerTask.VAN_TASKS.length)].getNpcId(), amount));
					if (player.getSlayer().getTask().getLevelReq() > player.getSkillLevel(Skill.SLAYER)) {
						while (player.getSlayer().getTask().getLevelReq() > player.getSkillLevel(Skill.SLAYER)) {
							player.setSlayer(new Slayer(player, SlayerTask.VAN_TASKS[random.nextInt(SlayerTask.VAN_TASKS.length)].getNpcId(), amount));
						}
					}
					Dialogue dialogue = new ChatDialogue(npc, null, "Your task is to kill " + player.getKillsLeft() + " " + player.getSlayer().getTask().getNpcName() + "s.");
					player.open(dialogue);
				}
				this.stop();
			}
		});

		/* Do nothing */
		Option botherVannaka = new Option("Err, nothing...", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(npc, null, "Exorth players these days.", "Always wasting my time...");
				player.open(dialogue);
				this.stop();
			}
		});

		dialogue.add("Select an Option", getTask, botherVannaka);
		return dialogue;
	}
}
