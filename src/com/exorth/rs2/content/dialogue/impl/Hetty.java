package com.exorth.rs2.content.dialogue.impl;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.Option;
import com.exorth.rs2.content.dialogue.StatementDialogue;
import com.exorth.rs2.content.dialogue.StatementType;
import com.exorth.rs2.content.quest.Quest;
import com.exorth.rs2.content.quest.QuestRepository;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Conner Kendall <Emnesty>
 * 
 * TODO: finish random dialogue.
 */
public class Hetty extends DialogueSession<Npc> {

	public Hetty(Player player, Npc[] actors) {
		super(player, actors);
	}

	@Override
	public Dialogue evaluate() {
		final Npc npc = actors[0];
		final Quest quest = QuestRepository.get("WitchsPotion");

		if (player.getQuestStorage().hasStarted(quest)) {
			if (player.getQuestStorage().getState(quest) == 2) {
				Dialogue dialogue = new ChatDialogue(npc, null, "So have you found the things for the potion?");
				if (!this.hasRequiredItems(new Item(1957, 1), new Item(221, 1), new Item(2146, 1), new Item(300, 1))) {
					dialogue.add(player, null, "Not yet.");
					return dialogue;
				}
				dialogue.add(player, null, "Yes I have everything!");
				dialogue.add(npc, null, "Excellent, can I have them then?");
				Dialogue message = new StatementDialogue(StatementType.NORMAL, new Task(1, true) {
					@Override
					protected void execute() {
						for (short id : new short[] { 1957, 221, 2146, 300 }) {
							player.getInventory().removeItem(new Item(id, 1));
						}
						this.stop();
					}
				},
				"You pass the ingredients to Hetty and she puts them all into her", "cauldron. Hetty closes her eyes and beings to chant. The cauldron", "bubbles myseriously.");
				dialogue.add(message);
				dialogue.add(player, null, "Well, is it ready?");
				dialogue.add(npc, new Task(1, true) {
					@Override
					protected void execute() {
						player.getQuestStorage().setState(quest, 3);
						this.stop();
					}
				},
				"Ok, now drink from the cauldron.");

				player.open(dialogue);
				return dialogue;
			}
		}

		Dialogue dialogue = new ChatDialogue(npc, null, "What would you want with an old woman like me?");
		Option option_1 = new Option("I am in search of a quest.", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(player, null, "I am in search of a quest.");
				dialogue.add(npc, null, "Hmmm... Maybe I can think of something for you.");
				dialogue.add(npc, null, "Would you like to become more proficient in the dark", "arts?");

				Option option_1 = new Option("Yes help me become one with my darker side.", new Task(1, true) {
					@Override
					protected void execute() {
						Dialogue dialogue = new ChatDialogue(player, null, "Yes help me become one with my darker side.");
						player.open(dialogue);
						this.stop();
					}
				});
				Option option_2 = new Option("No I have my principles and honour.", new Task(1, true) {
					@Override
					protected void execute() {
						Dialogue dialogue = new ChatDialogue(player, null, "No I have my principles and honour.");
						player.open(dialogue);
						this.stop();
					}
				});
				Option option_3 = new Option("What, you mean improve my magic?", new Task(1, true) {
					@Override
					protected void execute() {
						Dialogue dialogue = new ChatDialogue(player, null, "What, you mean improve my magic?");
						final Dialogue message = new StatementDialogue(StatementType.NORMAL, "The witch sighs.");
						dialogue.add(message);
						dialogue.add(npc, null, "Yes, improve your magic...");
						dialogue.add(npc, null, "Do you have a sense of drama?");

						Option option_1 = new Option("Yes I'd like to improve my magic.", new Task(1, true) {
							@Override
							protected void execute() {
								Dialogue dialogue = new ChatDialogue(player, new Task(1, true) {
									@Override
									protected void execute() {
										player.setAttribute("currentQuest", quest);
										quest.start(player);
										player.getQuestStorage().setState(quest, 1);
										this.stop();
									}
								},
								"Yes I'd like to improve my magic.");

								dialogue.add(message);
								dialogue.add(npc, null, "Ok I'm going to make a potion to help bring out your", "darker self.");
								dialogue.add(npc, null, "You will need certain ingredients.");
								dialogue.add(player, null, "What do I need?");
								dialogue.add(npc, null, "You need an eye of newt, a rat's tail, an onion... Oh", "and a piece of burnt meant.");
								dialogue.add(player, new Task(1, true) {
									@Override
									protected void execute() {
										player.getQuestStorage().setState(quest, 2);
										this.stop();
									}
								},
								"Great, I'll go and get them.");

								player.open(dialogue);
								this.stop();
							}
						});
						Option option_2 = new Option("No I'm not interested.", new Task(1, true) {
							@Override
							protected void execute() {
								Dialogue dialogue = new ChatDialogue(player, null, "No I'm not interested");
								player.open(dialogue);
								this.stop();
							}
						});
						Option option_3 = new Option("Show me the mysteries of the dark arts...", new Task(1, true) {
							@Override
							protected void execute() {
								Dialogue dialogue = new ChatDialogue(player, null, "Show me the mysteries of the ark arts...");
								player.open(dialogue);
								this.stop();
							}
						});
						dialogue.add("Select an Option", option_1, option_2, option_3);
						player.open(dialogue);
						this.stop();
					}
				});
				player.open(dialogue);
				dialogue.add("Select an Option", option_1, option_2, option_3);
				this.stop();
			}
		});

		Option option_2 = new Option("I've heard that you are a witch.", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(player, null, "I've heard that you are a witch.");
				player.open(dialogue);
				this.stop();
			}
		});
		dialogue.add("Select an Option", option_1, option_2);
		return dialogue;
	}

	private boolean hasRequiredItems(Item... items) {
		for (Item item : items) {
			if (!player.getInventory().getItemContainer().contains(item.getId())) {
				return false;
			}
		}
		return true;
	}
}
