package com.exorth.rs2.content.dialogue.impl;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.Option;
import com.exorth.rs2.content.dialogue.StatementDialogue;
import com.exorth.rs2.content.dialogue.StatementType;
import com.exorth.rs2.content.quest.Quest;
import com.exorth.rs2.content.quest.QuestRepository;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Language;

public class FatherUrhney extends DialogueSession<Npc> {

	public FatherUrhney(Player player, Npc[] actors) {
		super(player, actors);
	}

	@Override
	public Dialogue evaluate() {
		final Npc npc = actors[0];
		final Quest quest = QuestRepository.get("TheRestlessGhost");

		Dialogue dialogue = new ChatDialogue(npc, null, "Go away! I'm meditating!");

		Option option_1 = new Option("Well, that's friendly.", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(player, null, "Well, that's friendly.");
				player.open(dialogue);
				this.stop();
			}
		});
		Option option_2 = new Option("Father Aereck sent me to talk to you.", new Task(1, true) {
			@Override
			protected void execute() {
				player.getQuestStorage().setState(quest, 3);
				Dialogue dialogue = new ChatDialogue(player, null, "Father Aereck sent me to talk to you.");
				dialogue.add(npc, null, "I suppose I'd better talk to you then. What problems", "has he got himself into this time?");
				Option option_1 = new Option("He's got a ghost haunting his graveyard.", new Task(1, true) {
					@Override
					protected void execute() {
						final Dialogue dialogue = new ChatDialogue(player, null, "He's got a ghost haunting his graveyard.");
						dialogue.add(npc, null, "Oh, the silly fool.");
						dialogue.add(npc, null, "I leave town for just five months, and ALREADY he", "can't manage.");
						dialogue.add(npc, null, "(sigh)");
						dialogue.add(npc, null, "Well, I can't go back and exorcise it, I vowed not to", "leave this place. Until I had done a full two yeard of", "prayer and meditation.");
						dialogue.add(npc, null, "Tell you what I can do though; take this amulet.");
						Dialogue message = new StatementDialogue(StatementType.NORMAL, "Father Urhney hands you an amulet.");
						dialogue.add(message);
						dialogue.add(npc, new Task(1, true) {
							@Override
							protected void execute() {
								if (!player.getInventory().hasRoomFor(new Item(1053, 1))) {
									Dialogue badMessage = new StatementDialogue(StatementType.NORMAL, Language.NO_SPACE);
									dialogue.add(badMessage);
									this.stop();
									return;
								} else {
									Item item = new Item(552, 1);
									player.getInventory().addItem(item);
									this.stop();
								}
								player.open(dialogue);
							}
						},
						"It is an Amulet of Ghostspeak.");
						dialogue.add(npc, null, "So called, because when you wear it you can speak to", "ghosts. A lot of ghosts are doomed to be ghosts because", "they have left some important task uncompleted.");
						dialogue.add(npc, null, "Maybe if you know what this task is, you can get rid of", "the ghost. I'm not making any guarantees mind you,", "but it is the best I can do right now.");
						dialogue.add(player, new Task(1, true) {
							@Override
							protected void execute() {
								player.getQuestStorage().setState(quest, 4);
								this.stop();
							}
						},
						"Thank you. I'll give it a try!");
						player.open(dialogue);
						this.stop();
					}
				});
				Option option_2 = new Option("You mean he gets himself into lots of problems?", new Task(1, true) {
					@Override
					protected void execute() {
						Dialogue dialogue = new ChatDialogue(npc, null, "Yes, sadly so... So, what seems to be the trouble", "this time?");
						player.open(dialogue);
						this.stop();
					}
				});
				dialogue.add("Select an Option", option_1, option_2);
				player.open(dialogue);
				this.stop();
			}
		});
		Option option_3 = new Option("I've come to repossess your house.", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(player, null, "I've come to repossess your house.");
				dialogue.add(npc, null, "That can't be so! I built this house with", "my own two hands! I owe no bank. Begone filthy", "thief, you won't fool me!");
				player.open(dialogue);
				this.stop();
			}
		});
		dialogue.add("Select an Option", option_1, option_2, option_3);
		return dialogue;
	}
}
