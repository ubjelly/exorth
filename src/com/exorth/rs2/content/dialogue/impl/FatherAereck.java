package com.exorth.rs2.content.dialogue.impl;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.Option;
import com.exorth.rs2.content.quest.Quest;
import com.exorth.rs2.content.quest.QuestRepository;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Conner Kendall <Emnesty>
 * 
 */
public class FatherAereck extends DialogueSession<Npc> {

	public FatherAereck(Player player, Npc[] actors) {
		super(player, actors);
	}

	@Override
	public Dialogue evaluate() {
		final Npc npc = actors[0];
		final Quest quest = QuestRepository.get("TheRestlessGhost");

		Dialogue dialogue = new ChatDialogue(npc, null, "Welcome to the church of holy Saradomin.");

		Option option_1 = new Option("Who's Saradomin?", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(player, null, "Who's Saradomin?");
				// TODO: (mad) dialogue
				dialogue.add(npc, null, "Goodness to the Gods! How could one", "be so un-educated? I swear it, they really", "need to teach the stories of the God Wars", "once more! Everyones clueless!");
				player.open(dialogue);
				this.stop();
			}
		});
		Option option_2 = new Option("Nice place you've got here.", new Task(1, true) {
			@Override
			protected void execute() {
				// TODO: Etc.. dialogue
				Dialogue dialogue = new ChatDialogue(player, null, "Nice place you've got here.");
				player.open(dialogue);
				this.stop();
			}
		});
		Option option_3 = new Option("I'm looking for a quest.", new Task(1, true) {
			@Override
			protected void execute() {
				player.setAttribute("currentQuest", quest);
				Dialogue dialogue = new ChatDialogue(player, null, "I'm looking for a quest.");
				dialogue.add(npc, null, "That's lucky, I need someone to do a quest for me.");
				Option yes = new Option("Ok, let me help then.", new Task(1, true) {
					@Override
					protected void execute() {
						Dialogue dialogue = new ChatDialogue(player, new Task(1, true) {
							@Override
							protected void execute() {
								quest.start(player);
								player.getQuestStorage().setState(quest, 1);
								this.stop();
							}
						},
						"Ok, let me help then.");

						dialogue.add(npc, null, "Thank you. The problem is, there is a ghost in the", "church graveyard. I would like you to get rid of it.");
						dialogue.add(npc, null, "If you need any help, my friend Father Urhney is an", "expert on ghosts.");
						dialogue.add(npc, null, "I believe he is currently living as a hermit in Limbridge", "swamp. He has a little shack in the south-west of the swamps.");
						dialogue.add(npc, null, "Exit the graveyard through the south gate to reach the", "swamp. I'm sure if you told him that I sent you he'd", "be willing to help.");
						dialogue.add(npc, null, "My name is Father Aereck by the way. Please to", "meet you.");
						dialogue.add(player, null, "Likewise.");
						dialogue.add(npc, null, "Take care travelling through the swamps, I have heard", "they can be quite dangerous.");
						dialogue.add(player, null, "I will, thanks.");
						player.getQuestStorage().setState(quest, 2);
						player.open(dialogue);
						this.stop();
					}
				});
				Option no = new Option("Sorry, I don't have time right now.", new Task(1, true) {
					@Override
					protected void execute() {
						this.stop();
					}
				});
				dialogue.add("Select an Option", yes, no);
				player.open(dialogue);
				this.stop();
			}
		});
		dialogue.add("Select an Option", option_1, option_2, option_3);
		return dialogue;
	}
}
