package com.exorth.rs2.content.dialogue.impl;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.Option;
import com.exorth.rs2.content.dialogue.StatementDialogue;
import com.exorth.rs2.content.dialogue.StatementType;
import com.exorth.rs2.content.quest.Quest;
import com.exorth.rs2.content.quest.QuestRepository;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class Sedridor extends DialogueSession<Npc> {

	public Sedridor(Player player, Npc[] actors) {
		super(player, actors);
	}

	@Override
	public Dialogue evaluate() {
		final Npc npc = actors[0];
		final Quest quest = QuestRepository.get("RuneMysteries");

		// TODO checks for quest completed/started
		Dialogue dialogue = new ChatDialogue(npc, null, "Welcome adventurer, to the world renowned", "Wizards' Tower. How may I help you?");
		if (player.getQuestStorage().hasStarted(quest) && !player.getQuestStorage().hasFinished(quest)) {
			if (player.getQuestStorage().getState(quest) == 4) {
				dialogue.add(npc, null, "Ah, " + player.getUsername() + "." + " How goes your quest? Have you", "delivered the research notes to my friend Aubury yet?");
				dialogue.add(player, null, "Yes, I have, He gave me some research notes", "to pass on to you.");
				dialogue.add(npc, null, "May I have his notes then?");
				dialogue.add(player, null, "Sure. I have them here.");
				dialogue.add(npc, null, "Well, before you hand them over to me, as you", "have been nothing but truthful with me to this point,", "and I admire that in an adventurer, I will let you", "into the secret of our research.");
				dialogue.add(npc, null, "Now as you may or may not know, many", "centuries ago, the wizards at this Tower", "learnt the secret of creating Rune Stones, which", "allowed us to cast Magic very easily.");
				dialogue.add(npc, null, "When this tower was burnt down the secret of", "creating runes was lost to us for all time... except it", "wasn't. Some months ago, while searching these ruins", "for information from the old days,");
				dialogue.add(npc, null, "I came upon a scroll almost destroyed, that detailed a", "magic rock deep in the icefields of the North, closed", "off from access by anything other than magic means.");
				dialogue.add(npc, null, "This rock was called the 'Rune Essence' by the", "magicians who stuied its powers. Apparently, by simply", "breaking a chunk from it, a Rune Stone could be", "fashioned very quickly and easily at certain");
				dialogue.add(npc, null, "elemental altars that were scattered across the land", "back then. Now, this is an interesting little piece of", "history, but not much use to us as modern wizards", "without access to the Rune Essence,");
				dialogue.add(npc, null, "Or these elemental altars. This is where you and", "Aubury come into this story. A few weeks back,", "Aubury discovered in a standard delivery of runes", "to his store, a parchment detailing a");
				dialogue.add(npc, null, "teleportation spell that he had never come across", "before. To his shock, when cast it took him to a", "strange rock he had never encountered before...", "yet that felt strangely familiar...");
				dialogue.add(npc, null, "As I'm sure you have now guessed, he had discovered a", "portal leading to the mythical Rune Essence. As soon as", "he told me of this spell, I saw the importance of his find,");
				dialogue.add(npc, null, "for if we could but find the elemental altars spoken", "of in the ancient texts, we would once more be able", "to create runes as our ancestors had done!, It would", "be the saviour of the wizards' art!");
				dialogue.add(player, null, "I'm still not sure how I fit into", "this little story of yours...");
				dialogue.add(npc, null, "You haven't guessed? This talisman you brought me...", "it is the key to the elemental altar of air! When", "you hold it next, it will direct you towards");
				dialogue.add(npc, null, "the entrance to the long forgotten Air Altar! By", "bringing pieces of the Rune Essence to the Air Temple,", "you will be able to fashion your own Air Runes!");
				dialogue.add(npc, null, "And this is not all! By finding other talismans similar", "to this one, you will eventually be able to craft every", "rune that is available on this world! Just");
				dialogue.add(npc, null, "as our ancestors did! I cannot stress enough what a", "find this is! Now, due to the risks involves of letting", "this mighty power fall into the wrong hands");
				dialogue.add(npc, null, "I will keep the teleport skill to the Rune Essence", "a closely guarded secret, shared only by myself", "and those Magic users around the world", "whom I trust enough to keep it.");
				dialogue.add(npc, null, "This means that if ant evil power should discover", "the talismans required to enter the elemental", "temples, we will be able to prevent their access", "to the Rune Essence and prevent");
				dialogue.add(npc, null, "tragedy befalling this world. I know not where the", "temples are located, nor do I know where the talismans", "have been scattered to in this land, but I now");
				dialogue.add(npc, null, "return your Air Talisman to you. Find the Air", "Temple, and you will be able to charge your Rune", "Essences to become Air Runes at will. Any time");
				dialogue.add(npc, null, "you wish to visit the Rune Essence, speak to me", "or Aubury and we will open a portal to that", "mystical place for you to visit.");
				dialogue.add(player, null, "So only you and Aubury know the teleport", "spell to the Rune Essence?");
				dialogue.add(npc, null, "No... there are others... whom I will tell of your", "authorisation to visit that place. When you speak", "to them, they will know you, and grant you", "access to that place when asked.");
				dialogue.add(npc, null, "Use the Air Talisman to locate the air temple,", "and use any further talismans you find to locate", "the other missing elemental temples.", "Now... my research notes please?");

				Dialogue message = new StatementDialogue(StatementType.NORMAL, new Task(1, true) {
					@Override
					protected void execute() {
						player.setAttribute("currentQuest", quest);
						player.getQuestStorage().setState(quest, 5);
						this.stop();
					}
				}, "You hand the head wizard the research notes.", "He hands you back the Air Talisman.");
				dialogue.add(message);
				return dialogue;
			}
		}

		Option option_1 = new Option("Nothing thanks, I'm just looking around.", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(player, null, "Nothing thanks, I'm just looking around.");
				player.open(dialogue);
				this.stop();
			}
		});
		Option option_2 = new Option("What are you doing down here?", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(player, null, "What are you doing down here?");
				player.open(dialogue);
				this.stop();
			}
		});
		Option option_3 = new Option("I'm looking for the head wizard", new Task(1, true) {
			@Override
			protected void execute() {
				player.setAttribute("currentQuest", quest);
				Dialogue dialogue = new ChatDialogue(player, null, "I'm looking for the head wizard.");
				dialogue.add(npc, null, "Oh, you are, are you?", "And just why would you be doing that?");
				dialogue.add(player, null, "The Duke of Lumbridge sent me to find him. I have", "this weird talisman he found. He said the head wizard", "would be very interested in it.");
				dialogue.add(npc, null, "Did he now? HmmmMMMMMmmmmm.", "Well that IS interesting. Hand it over then adventurer,", "let me see what all the hubbub about it is.", "Just some amulet I'll wager.");
				Option yes = new Option("Ok, here you are.", new Task(1, true) {
					@Override
					protected void execute() {
						Dialogue dialogue = new ChatDialogue(player, null, "Ok, here you are.");
						Dialogue message = new StatementDialogue(StatementType.NORMAL, "You hand the Talisman to the wizard.");
						dialogue.add(message);
						dialogue.add(npc, new Task(1, true) {
							@Override
							protected void execute() {
								Item item = new Item(1438, 1);
								if (player.getInventory().getItemContainer().contains(item.getId()))
									player.getInventory().removeItem(item);
								this.stop();
							}
						}, "Wow! This is... incredible!");
						dialogue.add(npc, null, "Th-this talisman you brought me...! It is the last piece", "of the puzzle, I think! Finally! The legacy of our", "ancestors... it will return to us once more!");
						dialogue.add(npc, null, "I need time to study this, " + player.getUsername() + ". Can you please", "do me this task while I study this talisman you have", "brought me? In the might town of Varrock, which");
						dialogue.add(npc, null, "is located North East of here, there is a certain shop", "that sells magical runes. I have in this package all of the", "research I have done related to the Rune Stones, and");
						dialogue.add(npc, null, "require somebody to take them to the shopkeeper so that", "he may share my research and offer me his insights.", "Do this for me, and bring back what he gives you,");
						dialogue.add(npc, null, "and if my suspicions are correct, I will let you into the", "knowledge of one of the greatest secrets this world has", "ever known! A secret so powerful that it destroyed the");
						dialogue.add(npc, null, "original Wizards' Tower all of those centuries", "ago! My research, combined with this mysterious", "talisman... I cannot believe the answer to", "the mysteries is so close now!");
						dialogue.add(npc, null, "do this thing for me " + player.getUsername() + "." + " Be rewarded in a", "way you can never imagine.");
						Option yes = new Option("Yes, certainly.", new Task(1, true) {
							@Override
							protected void execute() {
								Dialogue dialogue = new ChatDialogue(player, null, "Yes, certainly.");
								dialogue.add(npc, null, "Take this package, and head directly North", "from here, through Draynor village, until you reach", "the Barbarian Village. Then head East from there", "until you reach Varrock.");
								dialogue.add(npc, null, "Once in Varrock, thake this package to the owner of the", "rune shop. His name is Aubury. You may find it", "helpful to ask one of Varrock's citizens for directions,");
								dialogue.add(npc, null, "as Varrock can be a confusing place for the first time", "visitor. He will give you a special item - bring it back to", "me, and I shall show you the mystery of the runes...");
								Dialogue message = new StatementDialogue(StatementType.NORMAL, "The head wizard gives you a package.");
								dialogue.add(message);
								dialogue.add(npc, new Task(1, true) {
									@Override
									protected void execute() {
										if (player.getInventory().addItem(new Item(290, 1))) {
											player.getQuestStorage().setState(quest, 2);
										}
										this.stop();
									}
								}, "Best of luck with your quest, " + player.getUsername() + ".");
								player.open(dialogue);
								this.stop();
							}
						});
						Option no = new Option("No, I'm busy.", new Task(1, true) {
							@Override
							protected void execute() {
								Dialogue dialogue = new ChatDialogue(player, null, "No, I'm busy.");
								player.open(dialogue);
								this.stop();
							}
						});
						dialogue.add("Select an Option", yes, no);
						player.open(dialogue);
						this.stop();
					}
				});
				Option no = new Option("No, I'll only give it to the head wizard.", new Task(1, true) {
					@Override
					protected void execute() {
						Dialogue dialogue = new ChatDialogue(player, null, "No, I'll only give it to the head wizard.");
						player.open(dialogue);
						this.stop();
					}
				});
				dialogue.add("Select an Option", yes, no);
				player.open(dialogue);
				this.stop();
			}
		});
		dialogue.add("Select an Option", option_1, option_2, option_3);
		return dialogue;
	}
}
