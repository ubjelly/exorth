package com.exorth.rs2.content.dialogue.impl;

import com.exorth.rs2.action.impl.TeleportationAction.TeleportType;
import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.Option;
import com.exorth.rs2.content.skills.magic.Teleport;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * Gnome trainer in Varrock that teleports you to the agility course.
 * @author Stephen
 */
public class GnomeTrainer extends DialogueSession<Npc> {

	/* The main Npc */
	private Npc npc;

	/**
	 * 
	 * @param player
	 * @param actors
	 */
	public GnomeTrainer(Player player, Npc[] actors) {
		super(player, actors);
		this.npc = actors[0];
	}

	@Override
	public Dialogue evaluate() {
		Dialogue dialogue = new ChatDialogue(npc, null, "Would you like to exchange your tickets?", "Or do you wish to be teleported to the", "Agility course?");

		/* Trade in tickets */
		Option tickets = new Option("Trade in Tickets", new Task(1, true) {
			@Override
			protected void execute() {
				player.getActionSender().sendInterface(8292);
				this.stop();
			}
		});
		
		/* Teleport to course */
		Option teleport = new Option("Teleport to Course", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(npc, null, "As you wish, " + player.getUsername() + ".");
				player.open(dialogue);
				npc.getUpdateFlags().sendFaceToDirection(player.getPosition());
				Teleport.initiate(player, npc, TeleportType.TELEOTHER, new Position(3003, 3934, 0), 2);
				this.stop();
			}
		});
		dialogue.add("Select an Option", tickets, teleport);
		return dialogue;
	}
}
