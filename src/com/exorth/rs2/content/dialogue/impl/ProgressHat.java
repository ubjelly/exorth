package com.exorth.rs2.content.dialogue.impl;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class ProgressHat extends DialogueSession<Npc> {

	public ProgressHat(Player player, Npc[] actors) {
		super(player, actors);
	}

	@Override
	public Dialogue evaluate() {
		final Npc npc = new Npc(3096);
		Dialogue dialogue = new ChatDialogue(player, null, "Mr. Progress Hat? Hello?");
		dialogue.add(npc, null, "That's me, why are you bothering me human?");
		dialogue.add(player, null, "Sorry, but do you think you could tell me my Pizazz", "Points?");
		dialogue.add(npc, null, "Ok, I suppose it's my job. You have ", player.getTelePizazz() + " Telekinetic, " + player.getAlchemyPizazz() + " Alchemist", player.getEnchantPizazz() + " Enchantment, and " + player.getGravePizazz() + " Graveyard Pizazz Points.");
		dialogue.add(player, null, "Thank you!");
		return dialogue;
	}
}
