package com.exorth.rs2.content.dialogue.impl;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.quest.Quest;
import com.exorth.rs2.content.quest.QuestRepository;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class Golrie extends DialogueSession<Npc> {

	public Golrie(Player player, Npc[] actors) {
		super(player, actors);
	}

	@Override
	public Dialogue evaluate() {
		final Npc npc = actors[0];
		final Quest quest = QuestRepository.get("WaterfallQuest");
		player.setAttribute("currentQuest", quest);
		Dialogue dialogue = new ChatDialogue(player, null, "Hello, is your name Golrie?");
		dialogue.add(npc, null, "That's me. I've been stuck in here for weeks, those", "goblins are trying to steal my family's heirlooms. My", "grandad gave me all sorts of old junk.");
		dialogue.add(player, null, "Do you mind if I have a look?");
		dialogue.add(npc, new Task(1, true) {
			@Override
			protected void execute() {
				player.getQuestStorage().setState(quest, 4);
				this.stop();
			}
		},
		"No, of course not.");
		return dialogue;
	}
}
