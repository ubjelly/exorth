package com.exorth.rs2.content.dialogue.impl;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.Option;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class PartyPete extends DialogueSession<Npc> {

	/* The main Npc */
	private Npc npc;

	public PartyPete(Player player, Npc[] actors) {
		super(player, actors);
		this.npc = actors[0];
	}

	@Override
	public Dialogue evaluate() {
		Dialogue dialogue = new ChatDialogue(npc, null, "Hey " + player.getUsername() + "!", "Care to play a quick game of dice?");

		/* Play dice */
		Option playDice = new Option("Hell yeah!", new Task(1, true) {
			@Override
			protected void execute() {
				if (!player.getInventory().getItemContainer().contains(995)) {
					Dialogue dialogue = new ChatDialogue(npc, null, "Looks like you don't have any gold!", "Come back some other time, " + player.getUsername() + ".");
					player.open(dialogue);
					player.getActionSender().sendMessage("You don't have any money to bet!");
					this.stop();
				} else {
					player.setEnterXInterfaceId(2402);
					player.getActionSender().sendEnterX();
				}
				this.stop();
			}
		});

		/* Don't play */
		Option no = new Option("No thanks, I'm trying to quit.", new Task(1, true) {
			@Override
			protected void execute() {
				Dialogue dialogue = new ChatDialogue(npc, null, "I understand. See you around, " + player.getUsername() + "!");
				player.open(dialogue);
				this.stop();
			}
		});

		dialogue.add("Select an Option", playDice, no);
		return dialogue;
	}
}
