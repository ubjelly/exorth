package com.exorth.rs2.content.dialogue.impl;

import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.Option;
import com.exorth.rs2.content.dialogue.StatementDialogue;
import com.exorth.rs2.content.dialogue.StatementType;
import com.exorth.rs2.content.quest.Quest;
import com.exorth.rs2.content.quest.QuestRepository;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class DukeHoracio extends DialogueSession<Npc> {

	public DukeHoracio(Player player, Npc[] actors) {
		super(player, actors);
	}

	@Override
	public Dialogue evaluate() {
		final Npc npc = (Npc) actors[0];
		final Quest quest = QuestRepository.get("RuneMysteries");
		Dialogue dialogue = new ChatDialogue(npc, null, "Greetings. Welcome to my castle.");
		Option option = new Option("Have you any quests for me?", new Task(1, true) {
			@Override
			protected void execute() {
				player.setAttribute("currentQuest", quest);
				Dialogue dialogue = new ChatDialogue(player, null, "Have you any quests for me?");
				dialogue.add(npc, null, "Well it's not really a quest but I recently discovered", "this strange talisman.");
				dialogue.add(npc, null, "It seems to be mystical and I have never seen anything", "like it before, Would you take it to the head wizard at");
				dialogue.add(npc, null, "the Wizards' Tower for me? It's just south-west of here", "and should not take you very long at all. I would be", "awfully grateful.");
				Option yes = new Option("Sure, no problem.", new Task(1, true) {
					@Override
					protected void execute() {
						Dialogue dialogue = new ChatDialogue(player, new Task(1, true) {
							@Override
							protected void execute() {
								quest.start(player);
								player.getQuestStorage().setState(quest, 1);
								this.stop();
							}
						},
						"Sure, no problem.");
						dialogue.add(npc, null, "Thank you very much, stranger. I am sure the head", "wizard will reward you for such an interesting find.");
						Dialogue message = new StatementDialogue(StatementType.NORMAL, "The Duke hands you an @blu@air talisman.");
						dialogue.add(message);
						player.open(dialogue);
						this.stop();
					}
				});
				Option no = new Option("Not right now", new Task(1, true) {
					@Override
					protected void execute() {
						this.stop();
					}
				});
				dialogue.add("Select an Option", yes, no);
				player.open(dialogue);
				this.stop();
			}
		});
		Option alt_option = new Option("Where can I find money?", new Task(1, true) {
			@Override
			protected void execute() {
				// TODO continue random dialogue.
				this.stop();
			}
		});
		dialogue.add("Select an Option", option, alt_option);
		return dialogue;
	}
}
