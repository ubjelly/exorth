package com.exorth.rs2.content.dialogue;

/**
 * The type of the statement.
 * 
 * @author Rait
 * 
 */
public enum StatementType {
	/**
	 * A normal statement, just click to continue.
	 */
	NORMAL,

	/**
	 * A timed statement, can't be closed via the client.
	 */
	TIMED;
}