package com.exorth.rs2.content;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import com.exorth.rs2.Constants;
import com.exorth.rs2.model.players.BankManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Language;
import com.exorth.rs2.util.Misc;

/**
 * TODO: Replace dialogue By Mikey` of Rune-Server Highly edited by AkZu (80%)
 * TODO: (Mark) Rewrite to make more secure and integrate with `players` SQL table
 */

public class BankPin {
	private Player player;

	public BankPin(Player player) {
		this.player = player;
	}

	// Our Bank PIN
	private int[] bankPin = { -1, -1, -1, -1 };

	// The digits we have so entered so far. Remember to reset!
	private int[] enteredPin = { -1, -1, -1, -1 };

	// Used for double verifying when changing PIN
	private int[] enteredPin2 = { -1, -1, -1, -1 };

	// Pending PIN, when getting a new PIN or changing existing PIN
	private int[] pendingBankPin = { -1, -1, -1, -1 };

	public int[] getPendingBankPin() {
		return pendingBankPin;
	}

	public void setPendingBankPin(int number) {
		String pin = "" + number;
		for (int k = 0; k < 4; k++) {
			pendingBankPin[k] = Integer.parseInt(pin.substring(k, k + 1));
		}
	}

	// The numbers that are displayed in dial pad, random shuffle needed
	private Integer[] bankPinNumbers = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

	// How many days you have to wait after you have changed your PIN or requested to delete it
	private int BankPINChangeDays = 7;

	public int getBankPINChangeDays() {
		return BankPINChangeDays;
	}

	public void setBankPINChangeDays(int days) {
		BankPINChangeDays = days;
	}

	// What digit we are entering
	private int interfaceStatus = -1;

	// When was the PIN you asked for implemented
	private int pinAppendYear = -1, pinAppendDay = -1;

	// pinVerified ? set it to true to access bank
	// changing pin ? TODO: if you have pending PIN
	// deleting pin ? a flag set when asked to delete your pin

	public int getPinAppendDay() {
		return pinAppendDay;
	}

	public void setPinAppendDay(int pinAppendDay) {
		this.pinAppendDay = pinAppendDay;
	}

	public int getPinAppendYear() {
		return pinAppendYear;
	}

	public void setPinAppendYear(int pinAppendYear) {
		this.pinAppendYear = pinAppendYear;
	}

	private boolean bankPinVerified = false, firstPass = false, changingBankPin = false, deletingBankPin = false;

	// What the system allows you to do after successful verification
	private ActionsAfterVerification actionsAfterVerification = ActionsAfterVerification.BANK;

	// What interface stage are you at
	private AtInterfaceStatus atInterfaceStatus = AtInterfaceStatus.NONE;

	public void setAtInterfaceStatus(AtInterfaceStatus atInterfaceStatus) {
		this.atInterfaceStatus = atInterfaceStatus;
	}

	public AtInterfaceStatus getInterfaceStatus() {
		return atInterfaceStatus;
	}

	public void setActionsAfterVerification(ActionsAfterVerification actionsAfterVerification) {
		this.actionsAfterVerification = actionsAfterVerification;
	}

	public void getNextInterface() {
		if (isChangingBankPin()) {
			sendBankPinSettings(2, 2, 0, true);
		} else if (!hasBankPin()) {
			sendBankPinSettings(0, 0, 0, true);
		} else if (hasBankPin()) {
			sendBankPinSettings(1, 0, 0, true);
		}
	}

	// Handle different buttons
	public void handleGeneralButtons(int buttonId) {
		if (atInterfaceStatus == AtInterfaceStatus.NONE) {
			return;
		}

		switch (buttonId) {
			// Set a PIN
			case 15075:
				if (atInterfaceStatus == AtInterfaceStatus.MAIN_SCREEN) {
					sendBankPinSettings(3, -1, 0, false);
				}
				break;
			// "I don't know it" Button
			case 14921:
				if (atInterfaceStatus == AtInterfaceStatus.VERIFYING) {
					player.getActionSender().removeInterfaces();
					if (!isDeletingBankPin()) {
						setDeletingBankPin();
						// player.getDialogue().sendStatement3("@blu@Your PIN will be deleted in "
						// + (BANK_PIN_CHANGE_DAYS - getDaysPassedSincePinChange())
						// + " days.",
						// "If you wish to cancel this change, you may do so by entering",
						// "your PIN correctly next time you attempt to use your bank");
					} else {
						// player.getDialogue().sendStatement2("You have already requested that your PIN be deleted.",
						// "Your Bank PIN will be deleted in " +
						// (BANK_PIN_CHANGE_DAYS - getDaysPassedSincePinChange()) +
						// " days.");
					}
				}
				break;
			// Go back from Confirm screen
			case 15176:
				if (atInterfaceStatus == AtInterfaceStatus.CONFIRM_SCREEN_1 || atInterfaceStatus == AtInterfaceStatus.CONFIRM_SCREEN_2) {
					getNextInterface();
				}
				break;
			// Delete current PIN
			case 15079:
				if (atInterfaceStatus == AtInterfaceStatus.PIN_CHANGE_DELETE) {
					sendBankPinSettings(3, 0, 1, false);
				}
				break;
			// Cancel pending PIN
			case 15082:
				if (atInterfaceStatus == AtInterfaceStatus.CANCEL_PENDING_PIN) {
					clearPendingBankPinRequest();
					if (hasBankPin()) {
						sendBankPinSettings(1, 3, 0, false);
					} else {
						sendBankPinSettings(0, 3, 0, false);
					}
				}
				break;
			// Set new PIN / Delete existing PIN
			case 15171:
				if (atInterfaceStatus == AtInterfaceStatus.CONFIRM_SCREEN_1 || atInterfaceStatus == AtInterfaceStatus.CONFIRM_SCREEN_2) {
					if (atInterfaceStatus == AtInterfaceStatus.CONFIRM_SCREEN_2) {
						deleteBankPin();
						sendBankPinSettings(0, 5, 0, false);
					} else if (atInterfaceStatus == AtInterfaceStatus.CONFIRM_SCREEN_1) {
						startPinInterface(AtInterfaceStatus.CHANGING);
					}
					break;
				}
			// Change PIN
			case 15078:
				if (atInterfaceStatus == AtInterfaceStatus.PIN_CHANGE_DELETE) {
					startPinInterface(AtInterfaceStatus.CHANGING);
				}
				break;
			// Change Recovery Delay
			case 15076:
				if (atInterfaceStatus == AtInterfaceStatus.MAIN_SCREEN || atInterfaceStatus == AtInterfaceStatus.PIN_CHANGE_DELETE) {
					BankPINChangeDays = (BankPINChangeDays == 7 ? 3 : 7);
					player.getActionSender().sendString(BankPINChangeDays + " days", 15107);
					player.getActionSender().sendString("Your Recovery Delay has now\\nbeen set to " + BankPINChangeDays + " days.\\n\\nYou will have to wait this long\\nto delete your PIN if you\\nforget it.", 15038);
				}
				break;
		}
	}

	// Handle digit pressing
	public void clickPinButton(int buttonId) {
		if (atInterfaceStatus == AtInterfaceStatus.NONE && interfaceStatus == -1) {
			return;
		}

		switch (atInterfaceStatus) {
			case VERIFYING:
				for (int i = 0; i < 10; i++) {
					if (buttonId == 14873 + i) {
						verifyPin(i);
					}
				}
	
				List<Integer> list = Arrays.asList(bankPinNumbers);
				Collections.shuffle(list);
	
				for (int i = 0; i < 10; i++) {
					bankPinNumbers[i] = list.get(i);
					player.getActionSender().sendString("" + bankPinNumbers[i], 14883 + i);
				}
				return;
			case CHANGING:
				for (int i = 0; i < 10; i++) {
					if (buttonId == 14873 + i) {
						verifyNewPin(i);
					}
				}
	
				List<Integer> list2 = Arrays.asList(bankPinNumbers);
				Collections.shuffle(list2);
	
				for (int i = 0; i < 10; i++) {
					bankPinNumbers[i] = list2.get(i);
					player.getActionSender().sendString("" + bankPinNumbers[i], 14883 + i);
				}
				return;
			default:
				Logger.getLogger("BankPin").info("Unhandled Bank PIN case : " + atInterfaceStatus);
		}
	}

	/**
	 * Begin the pin entry.
	 * 
	 * @param atInterfaceStatus
	 */
	public void startPinInterface(AtInterfaceStatus atInterfaceStatus) {
		if (player.getAttribute("bankPinEntries") != null && player.getAttribute("bankPinWaitTimer") == null && (Byte) player.getAttribute("bankPinEntries") >= 1) {
			player.setAttribute("bankPinWaitTimer", System.currentTimeMillis());
		} else if (player.getAttribute("bankPinWaitTimer") != null) {
			if (System.currentTimeMillis() - (Long) player.getAttribute("bankPinWaitTimer") < 300000) {
				// player.getDialogue().sendStatement2("@blu@You have entered your Bank PIN multiple times wrong.",
				// "You must wait atleast 5 minutes to use this service again.");
				return;
			} else {
				player.removeAttribute("bankPinWaitTimer");
				player.removeAttribute("bankPinEntries");
			}
		}
		firstPass = false;
		this.atInterfaceStatus = atInterfaceStatus;
		resetBankPinInterface(true);
		sendBankPinVerificationInterface(0, true);
	}

	private static String intToString(int intToChange) {
		if (intToChange == 1) {
			return "First, click the FIRST digit.";
		} else if (intToChange == 2) {
			return "Now click the SECOND digit.";
		} else if (intToChange == 3) {
			return "Time for the THIRD digit.";
		} else if (intToChange == 4) {
			return "Finally, the FOURTH digit.";
		}
		return "First, click the FIRST digit.";
	}

	/**
	 * Update the current stage.
	 * 
	 * @param currentStatus
	 * @param sendInterface
	 */
	private void sendBankPinVerificationInterface(int currentStatus, boolean sendInterface) {
		this.interfaceStatus = currentStatus;

		// Send the unlocked digits amount graphically
		for (int i = 0; i < currentStatus; i++) {
			player.getActionSender().sendString("*", 14913 + i);
		}

		if (currentStatus > 0) {
			player.getActionSender().sendSound(1253, 100, 0);
		}

		// send the order messages what digit next
		player.getActionSender().sendString(intToString(currentStatus + 1), 15313);

		if (sendInterface) {
			player.getActionSender().sendInterface(7424);
		}
	}

	/**
	 * 
	 * @param mode
	 * @param txtSetting
	 * @param txtSetting2
	 * @param sendInterface
	 */
	public void sendBankPinSettings(int mode, int txtSetting, int txtSetting2, boolean sendInterface) {
		final int SET_NEW_PIN = 0, CHANGE_PIN = 1, CANCEL_PENDING_PIN = 2, CONFIRM_NEW_PIN = 3;

		if (txtSetting == 0) {
			player.getActionSender().sendString("Players are reminded\\nthat they should NEVER tell\\nanyone their Bank PINs or\\npasswords, nor should they\\never enter their PINs on any\\nwebsite form.", 15038);
		} else if (txtSetting == 1) {
			player.getActionSender().sendString("Those numbers did not\\nmatch.\\n\\nYour PIN has not been set;\\nplease try again if you wish\\nto set a new PIN.", 15038);
		} else if (txtSetting == 2) {
			player.getActionSender().sendString("You have requested that a\\nPIN be set on your bank\\naccount. This will take effect\\nin 7 days.\\n\\nIf you wish to cancel this PIN,\\nplease use the button on the\\nleft.", 15038);
		} else if (txtSetting == 3) {
			player.getActionSender().sendString("The PIN has been cancelled\\nand will NOT be set\\n\\n" + (hasBankPin() ? "You still have a Bank\\nPIN." : "You still do not have a Bank\\nPIN."), 15038);
		} else if (txtSetting == 4) {
			player.getActionSender().sendString("That number wouldn't be very\\nhard to guess. Please try\\nsomething different!", 15038);
		} else if (txtSetting == 5) {
			player.getActionSender().sendString("You have deleted your PIN!\\n\\nYou can set one anytime\\nyou wish.", 15038);
		} else if (txtSetting == 6) {
			player.getActionSender().sendString("That number was same as\\nyour old one.\\n\\nPlease try something\\ndifferent!", 15038);
		}

		if (changingBankPin) {
			player.getActionSender().sendString("Bank PIN coming soon!", 15105);
		} else if (hasBankPin()) {
			player.getActionSender().sendString("PIN Set", 15105);
		} else if (!hasBankPin()) {
			player.getActionSender().sendString("No PIN Set", 15105);
		}

		player.getActionSender().sendString(BankPINChangeDays + " days", 15107);

		switch (mode) {
			case SET_NEW_PIN:
				atInterfaceStatus = AtInterfaceStatus.MAIN_SCREEN;
				player.getActionSender().showComponent(15108, false);
				player.getActionSender().moveComponent(500, 0, 15171);
				player.getActionSender().moveComponent(500, 0, 15176);
				player.getActionSender().moveComponent(500, 0, 15082);
				player.getActionSender().moveComponent(500, 0, 15079);
				player.getActionSender().moveComponent(500, 0, 15078);
				player.getActionSender().moveComponent(500, 0, 15080);
				player.getActionSender().moveComponent(0, 0, 15076);
				player.getActionSender().moveComponent(0, 0, 15075);
				break;
			case CHANGE_PIN:
				atInterfaceStatus = AtInterfaceStatus.PIN_CHANGE_DELETE;
				player.getActionSender().showComponent(15108, false);
				player.getActionSender().moveComponent(500, 0, 15171);
				player.getActionSender().moveComponent(500, 0, 15176);
				player.getActionSender().moveComponent(500, 0, 15082);
				player.getActionSender().moveComponent(500, 0, 15075);
				player.getActionSender().moveComponent(500, 0, 15080);
				player.getActionSender().moveComponent(0, 6, 15076);
				player.getActionSender().moveComponent(0, 0, 15079);
				player.getActionSender().moveComponent(0, 0, 15078);
				break;
			case CANCEL_PENDING_PIN:
				atInterfaceStatus = AtInterfaceStatus.CANCEL_PENDING_PIN;
				player.getActionSender().showComponent(15108, false);
				player.getActionSender().moveComponent(500, 0, 15171);
				player.getActionSender().moveComponent(500, 0, 15176);
				player.getActionSender().moveComponent(500, 0, 15078);
				player.getActionSender().moveComponent(500, 0, 15080);
				player.getActionSender().moveComponent(500, 0, 15076);
				player.getActionSender().moveComponent(500, 0, 15075);
				player.getActionSender().moveComponent(500, 0, 15079);
				player.getActionSender().moveComponent(0, 0, 15082);
				break;
			case CONFIRM_NEW_PIN:
				if (txtSetting2 == 0) {
					atInterfaceStatus = AtInterfaceStatus.CONFIRM_SCREEN_1;
					player.getActionSender().sendString("Do you really wish to set a PIN on your bank account?", 15110);
					player.getActionSender().sendString("Yes, I really want a Bank PIN. I will never forget it!", 15171);
					player.getActionSender().sendString("No, I might forget it!", 15176);
				} else if (txtSetting2 == 1) {
					atInterfaceStatus = AtInterfaceStatus.CONFIRM_SCREEN_2;
					player.getActionSender().sendString("Do you really wish to delete your PIN on your bank account?", 15110);
					player.getActionSender().sendString("Yes, I really want to delete my Bank PIN!", 15171);
					player.getActionSender().sendString("No, I prefer extra security!", 15176);
				}
				player.getActionSender().showComponent(15108, true);
				player.getActionSender().moveComponent(0, 0, 15171);
				player.getActionSender().moveComponent(0, 0, 15176);
				break;
		}

		if (sendInterface) {
			player.getActionSender().sendInterface(14924);
		}
	}

	public void checkBankPinChangeStatus() {
		if (changingBankPin) {
			int daysPassed = getDaysPassedSincePinChange();
			if (daysPassed >= 7) {
				changeBankPin();
			}
		}
		if (deletingBankPin) {
			int daysPassed = getDaysPassedSincePinChange();
			if (daysPassed >= BankPINChangeDays) {
				deleteBankPin();
			}
		}
	}

	public void changeBankPin() {
		for (int index = 0; index < bankPin.length; index++) {
			bankPin[index] = pendingBankPin[index];
			pendingBankPin[index] = -1;
		}
		changingBankPin = false;
		pinAppendYear = -1;
		pinAppendDay = -1;
		player.getActionSender().sendSound(1262, 100, 0);
		player.getActionSender().sendMessage("Your Bank PIN has been successfully changed!");
	}

	private void deleteBankPin() {
		for (int index = 0; index < bankPin.length; index++) {
			bankPin[index] = -1;
			pendingBankPin[index] = -1;
		}
		deletingBankPin = false;
		pinAppendYear = -1;
		pinAppendDay = -1;
		player.getActionSender().sendSound(1262, 100, 0);
		player.getActionSender().sendMessage("Your Bank PIN has been successfully deleted!");
	}

	private int getDaysPassedSincePinChange() {
		if (Misc.getYear() == pinAppendYear) {
			return (Misc.getDayOfYear() - pinAppendDay);
		} else {
			if ((pinAppendYear % 4 == 0) && (pinAppendYear % 100 != 0) || (pinAppendYear % 400 == 0)) {
				return ((366 - pinAppendDay) + Misc.getDayOfYear());
			} else {
				return ((365 - pinAppendDay) + Misc.getDayOfYear());
			}
		}
	}

	// Resets the digit-entry interface to position zero
	private void resetBankPinInterface(boolean rearrangeNumbers) {
		// Setting the state to start
		interfaceStatus = -1;

		// Clearing the PIN we entered
		for (int k1 = 0; k1 < 4; k1++) {
			enteredPin[k1] = -1;
		}

		player.getActionSender().sendString("First, click the FIRST digit.", 15313);

		for (int i = 0; i < 4; i++) {
			player.getActionSender().sendString("?", 14913 + i);
		}

		if (rearrangeNumbers) {

			// As the Collections.shuffle method needs a list for the parameter, we convert our array into List using the Arrays class.
			List<Integer> list = Arrays.asList(bankPinNumbers);

			// Here, we just simply used the shuffle method of Collections class to shuffle our defined array.
			Collections.shuffle(list);

			// Here. we have the PIN numbers re-arranged
			for (int i = 0; i < 10; i++) {
				bankPinNumbers[i] = list.get(i);
				player.getActionSender().sendString("" + bankPinNumbers[i], 14883 + i);
			}
		}

		// Reset the instructions message and "I don't know it" button state
		if (atInterfaceStatus == AtInterfaceStatus.CHANGING) {
			if (!firstPass) {
				player.getActionSender().sendString("Please choose a new FOUR DIGIT PIN using the buttons below.", 14920);
				player.getActionSender().sendString("Set new PIN", 14923);
			} else {
				player.getActionSender().sendString("Now please enter that number again.", 14920);
				player.getActionSender().sendString("Confirm new PIN", 14923);
			}
			player.getActionSender().moveComponent(500, 0, 14921);
		} else {
			int daysPassed = getDaysPassedSincePinChange();
			if (actionsAfterVerification == ActionsAfterVerification.BANK) {
				player.getActionSender().sendString("Bank of " + Constants.SERVER_NAME, 14923);
			} else if (actionsAfterVerification == ActionsAfterVerification.BANK_PIN_SETTINGS) {
				player.getActionSender().sendString("PIN Settings", 14923);
			}
			if (deletingBankPin) {
				player.getActionSender().sendString("Your Bank PIN will be deleted in " + (BankPINChangeDays - daysPassed) + " days.", 14923);
			} else if (changingBankPin) {
				player.getActionSender().sendString("Your Bank PIN will change in " + (7 - daysPassed) + " days.", 14923);
			}
			player.getActionSender().sendString("Please enter your FOUR DIGIT PIN using the buttons below.", 14920);
			player.getActionSender().moveComponent(0, 0, 14921);
		}
	}

	private void verifyPin(int pinEntered) {
		boolean wrongPin = false;

		if (interfaceStatus == -1) {
			return;
		}

		enteredPin[interfaceStatus] = bankPinNumbers[pinEntered];
		if (interfaceStatus + 1 < 4) {
			sendBankPinVerificationInterface(interfaceStatus + 1, false);
			return;
		} else {
			// If any of the entered digits doesn't match the Bank PIN, deny access
			if (enteredPin[0] != bankPin[0] || enteredPin[1] != bankPin[1] || enteredPin[2] != bankPin[2] || enteredPin[3] != bankPin[3]) {
				wrongPin = true;
			} else {
				wrongPin = false;
			}
		}
		if (wrongPin) {
			player.getActionSender().sendMessage(Language.BANK_PIN_WRONG_PIN);
			player.getActionSender().sendSound(1252, 100, 0);
			player.getActionSender().removeInterfaces();
			if (player.getAttribute("bankPinEntries") == null) {
				player.setAttribute("bankPinEntries", (byte) 0);
			} else {
				player.setAttribute("bankPinEntries", (byte) ((Byte) player.getAttribute("bankPinEntries") + 1));
			}
		} else {
			player.getActionSender().sendMessage(Language.BANK_PIN_CORRECT_PIN);
			player.getActionSender().sendSound(1257, 0, 0);
			player.removeAttribute("bankPinEntries");
			if (isDeletingBankPin()) {
				player.getActionSender().sendMessage("Your Bank PIN delete request has been cancelled.");
				setNotDeletingPin();
			}
			setBankPinVerified(true);
			doActionAfterVerification();
		}
	}

	private void verifyNewPin(int pinEntered) {
		boolean wrongPin = false;

		if (interfaceStatus == -1) {
			return;
		}

		enteredPin[interfaceStatus] = bankPinNumbers[pinEntered];

		if (!firstPass) {
			enteredPin2[interfaceStatus] = enteredPin[interfaceStatus];
		}
		if (interfaceStatus + 1 < 4) {
			sendBankPinVerificationInterface(interfaceStatus + 1, false);
			return;
		} else {
			if (!firstPass) {
				// Error: Same as old PIN
				if (enteredPin2[0] == bankPin[0] && enteredPin2[1] == bankPin[1] && enteredPin2[2] == bankPin[2] && enteredPin2[3] == bankPin[3]) {
					sendBankPinSettings(1, 6, 0, true);
					return;
				}
				// Error: Too easy to guess
				if (enteredPin2[0] == enteredPin2[1] && enteredPin2[1] == enteredPin2[0] && enteredPin2[1] == enteredPin2[2] && enteredPin2[2] == enteredPin2[1] && enteredPin2[3] == enteredPin2[2] && enteredPin2[2] == enteredPin2[3] || (enteredPin2[0] == 1 && enteredPin2[1] == 2 && enteredPin2[2] == 3 && enteredPin2[3] == 4) || (enteredPin2[0] == 0 && enteredPin2[1] == 1 && enteredPin2[2] == 2 && enteredPin2[3] == 3)) {
					if (!hasBankPin()) {
						sendBankPinSettings(0, 4, 0, true);
					} else {
						sendBankPinSettings(1, 4, 0, true);
					}
					return;
				}
				firstPass = true;
				resetBankPinInterface(false);
				sendBankPinVerificationInterface(interfaceStatus + 1, false);
				return;
			}
			// If any of the entered digits doesn't match the Bank PIN, deny access
			if (enteredPin[0] != enteredPin2[0] || enteredPin[1] != enteredPin2[1] || enteredPin[2] != enteredPin2[2] || enteredPin[3] != enteredPin2[3]) {
				wrongPin = true;
			} else {
				wrongPin = false;
			}
		}
		if (wrongPin) {
			if (hasBankPin()) {
				sendBankPinSettings(1, 1, 0, true);
			} else {
				sendBankPinSettings(0, 1, 0, true);
			}
			resetBankPinInterface(true);
			clearPendingBankPinRequest();
			firstPass = false;
		} else {
			firstPass = false;
			for (int k = 0; k < 4; k++) {
				pendingBankPin[k] = enteredPin[k];
			}
			setChangingBankPin();
			checkBankPinChangeStatus();
			sendBankPinSettings(2, 2, 0, true);
		}
	}

	private void doActionAfterVerification() {
		switch (actionsAfterVerification) {
			case BANK:
				atInterfaceStatus = AtInterfaceStatus.NONE;
				BankManager.openBank(player);
				break;
			case BANK_PIN_SETTINGS:
				getNextInterface();
		}
	}

	public void setChangingBankPin() {
		changingBankPin = true;
		pinAppendYear = Misc.getYear();
		pinAppendDay = Misc.getDayOfYear();
	}

	public void setDeletingBankPin() {
		clearPendingBankPinRequest();
		deletingBankPin = true;
		pinAppendYear = Misc.getYear();
		pinAppendDay = Misc.getDayOfYear();
	}

	private void setNotDeletingPin() {
		player.getActionSender().sendSound(1262, 100, 0);
		deletingBankPin = false;
		pinAppendYear = -1;
		pinAppendDay = -1;
	}

	public void clearPendingBankPinRequest() {
		for (int i = 0; i < pendingBankPin.length; i++) {
			pendingBankPin[i] = -1;
		}
		changingBankPin = false;
		pinAppendYear = -1;
		pinAppendDay = -1;
	}

	private void setBankPinVerified(boolean bankPinVerified) {
		this.bankPinVerified = bankPinVerified;
	}

	public boolean hasBankPin() {
		return (bankPin[0] != -1);
	}

	public boolean hasPendingBankPin() {
		return (pendingBankPin[0] != -1);
	}

	public boolean isBankPinVerified() {
		return bankPinVerified;
	}

	public int[] getBankPin() {
		return bankPin;
	}

	// TODO: Saving
	// 3216,9624
	public void setBankPin(int number) {
		String pin = "" + number;
		for (int k = 0; k < 4; k++) {
			bankPin[k] = Integer.parseInt(pin.substring(k, k + 1));
		}
	}

	public boolean isChangingBankPin() {
		return changingBankPin;
	}

	public void setChangingPin(boolean flag) {
		this.changingBankPin = flag;
	}

	public void setDeletingPin(boolean flag) {
		this.deletingBankPin = flag;
	}

	public boolean isDeletingBankPin() {
		return deletingBankPin;
	}

	/**
	 * Main Screen: Set a PIN / Change Recovery Delay 
	 * Confirm Screen #1: Do you want a PIN? Yes/No 
	 * PIN Changing: dialing in the pin we want to change
	 * Cancel Pending PIN: You can cancel the PIN in this interface stage 
	 * PIN Verifying: Dialing in your PIN and accessing features such as Bank and "I don't know it" button 
	 * PIN Change/Delete Screen + Recovery Delay Screen = yYu can access these features in this interface stage
	 * Confirm Screen #2: Do you want to delete your PIN? Yes/No
	 */
	public enum AtInterfaceStatus {
		MAIN_SCREEN, CONFIRM_SCREEN_1, CHANGING, CANCEL_PENDING_PIN, VERIFYING, PIN_CHANGE_DELETE, CONFIRM_SCREEN_2, NONE
	}

	public enum ActionsAfterVerification {
		BANK, BANK_PIN_SETTINGS
	}
}