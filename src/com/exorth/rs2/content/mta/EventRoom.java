package com.exorth.rs2.content.mta;

import com.exorth.rs2.task.Task;

/**
 * An room that has a cyclic event.
 * 
 * @author Joshua Barry
 * 
 */
public abstract class EventRoom {

	public abstract Task getEventCycle();

}
