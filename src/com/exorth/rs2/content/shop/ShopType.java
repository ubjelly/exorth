package com.exorth.rs2.content.shop;

/**
 * The type of a shop.
 * 
 * @author Rait
 * 
 */
public enum ShopType {

	/**
	 * A general store. This type of shop buys anything and then sells it for a
	 * higher price.
	 */
	GENERAL_STORE,

	/**
	 * A specialist shop. This type of shop buys only the type of items that it
	 * sells.
	 */
	SPECIALIST_BUY,

	/**
	 * A type of shop that only sells items.
	 */
	SPECIALIST_NO_BUY;

}
