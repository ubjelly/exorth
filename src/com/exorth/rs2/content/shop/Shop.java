package com.exorth.rs2.content.shop;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Inventory;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.container.Container;
import com.exorth.rs2.model.players.container.ContainerType;

/**
 * Represents a shop.
 * 
 * @author Rait
 * 
 */
public class Shop {

	/**
	 * The identifier of the shop.
	 */
	private int id;

	/**
	 * The currency used in this shop.
	 */
	private int currency;

	/**
	 * The name of the shop.
	 */
	private String name;

	/**
	 * The items of the shop.
	 */
	private Item[] items;

	/**
	 * The shop's present item stock.
	 */
	private Container currentStock;

	/**
	 * The original item stock of the shop.
	 */
	private Container originalStock;

	/**
	 * The type of the shop.
	 */
	private ShopType type;

	/**
	 * Sells an item to a shop.
	 * 
	 * @param item The item to sell.
	 */
	public void sell(Player player, int slot, int amount) {
		Item item = player.getInventory().get(slot);
		if (item == null) {
			player.getActionSender().sendMessage("Error selling item - item is null.");
			return;
		}
		if (item.getId() == currency) {
			player.getActionSender().sendMessage("The shop will not buy that item.");
			return;
		}
		int transferAmount = player.getInventory().getItemContainer().getCount(item.getId());
		if (amount >= transferAmount) {
			amount = transferAmount;
		} else if (transferAmount == 0) {
			return;
		}
		if (!player.getInventory().getItemContainer().contains(item.getId())) {
			player.getActionSender().sendMessage("Error selling item - player's inventory doesn't contain item.");
			return;
		}
		if (type == ShopType.SPECIALIST_BUY) {
			if (!getOriginalStock().contains(item.getId())) {
				player.getActionSender().sendMessage("The shop will not buy that item.");
				return;
			}
		}
		int price = value(player, item, false, true);
		if (price <= 0) {
			player.getActionSender().sendMessage("Error selling item - item price is 0 or less.");
			return;
		}

		int shopSlot = getCurrentStock().contains(item.getId()) ? getCurrentStock().getSlotById(item.getId()) : getCurrentStock().freeSlot();
		if (shopSlot == -1) {
			player.getActionSender().sendMessage("This shop is currently full.");
		} else {
			if (getCurrentStock().get(shopSlot) != null) {
				if (getCurrentStock().get(shopSlot).getCount() + amount < 1) {
					player.getActionSender().sendMessage("This shop is currently full.");
					return;
				}
			}
			long totalAmount = amount;
			long totalValue = value(player, item, false, true);
			long totalPrice = (totalAmount * totalValue);
			if (totalPrice > Integer.MAX_VALUE) {
				amount = (Integer.MAX_VALUE / value(player, item, false, true)) - 1;
			}
			Item reward = new Item(currency, amount * value(player, item, false, true));
			Container temporaryInventory = new Container(ContainerType.STANDARD, Inventory.SIZE);
			for (Item invItem : player.getInventory().getItemContainer().toArray()) {
				temporaryInventory.add(invItem);
			}
			temporaryInventory.remove(new Item(item.getId(), amount));
			if (!temporaryInventory.add(reward)) {
				return;
			}
			player.getInventory().removeItem(new Item(item.getId(), amount));
			getCurrentStock().add(new Item(item.getId(), amount));
			player.getInventory().addItem(reward);
		}
	}

	/**
	 * Buys an item from the shop.
	 * 
	 * @param player The buying player.
	 * @param slot The slot to buy from.
	 * @param amount The amount of the item to buy.
	 */
	public void buy(Player player, int slot, int amount) {
		Item item = getCurrentStock().get(slot);
		if (item == null) {
			return;
		}
		int transferAmount = player.getInventory().getItemContainer().freeSlots();
		if (amount >= transferAmount && !ItemManager.getInstance().getIsStackable(item.getId())) {
			amount = transferAmount;
		} else if (transferAmount == 0) {
			return; // invalid packet, or client out of sync
		}
		if (transferAmount > 200) {
			transferAmount = 200;
			player.getActionSender().sendMessage("You cannot buy more than 200 items at a time.");
		}
		if (getCurrentStock().get(slot).getCount() <= 0) {
			player.getActionSender().sendMessage("Shop is currently out of stock.");
			return;
		}
		if (amount >= getCurrentStock().get(slot).getCount()) {
			amount = getCurrentStock().get(slot).getCount();
		}
		if (!hasCurrency(player, item, amount, false)) {
			player.getActionSender().sendMessage("You don't have enough " + ItemManager.getInstance().getItemName(currency).toLowerCase() + ".");
		} else {
			Item reward = new Item(item.getId(), amount);
			Container temporaryInventory = new Container(ContainerType.STANDARD, Inventory.SIZE);
			for (Item invItem : player.getInventory().getItemContainer().toArray()) {
				temporaryInventory.add(invItem);
			}
			temporaryInventory.remove(new Item(currency, amount * value(player, new Item(item.getId(), amount), false, false)));
			// shop.getCostValue(player, new Item(item.getId(), amount))));
			if (!temporaryInventory.add(reward)) {
				return;
			}
			player.getInventory().removeItem(new Item(currency, amount * value(player, new Item(item.getId(), amount), false, false)));
			player.getInventory().addItem(reward);
			if (isOriginal(reward, slot)) {
				getCurrentStock().removeOrZero(reward);
			} else {
				getCurrentStock().remove(reward);
			}
		}
	}

	private boolean hasCurrency(Player player, Item item, int amt, boolean selling) {
		int final_amount = value(player, item, false, selling);
		if (final_amount == -1) {
			player.getActionSender().sendMessage("Currency Error.");
			return false;
		}
		final_amount *= amt;
		if (player.getInventory().getItemContainer().getById(currency) == null) {
			return false;
		}
		return player.getInventory().getItemContainer().getById(currency).getCount() >= final_amount;
	}

	/**
	 * Gets the value of an item.
	 * 
	 * @param player The player.
	 * @param item The item to value.
	 * @param send Whether to send the price to the player or not.
	 * @param selling Whether we want to get the sell price or not (if not, get
	 *			the buy price).
	 * @return The resulting price.
	 */
	public int value(Player player, Item item, boolean send, boolean selling) {
		if (item == null) {
			player.getActionSender().sendMessage("Error valuing item - valued item is null.");
			return -1;
		}
		int price = item.getDefinition().getShopValue();
		if (price <= 0) {
			player.getActionSender().sendMessage("Error valuing item - valued item's price is 0 or less.");
			return -1;
		}
		if (type.equals(ShopType.GENERAL_STORE)) {
			price = selling ? (int) (price * 0.4 < 1 ? 1 : price * 0.4) : (int) (price * 0.6 < 1 ? 1 : price * 0.6);
		} else {
			price = selling ? (int) (price * 0.6 < 1 ? 1 : price * 0.6) : (int) (price * 0.8 < 1 ? 1 : price * 0.8);
		}
		if (send && !selling) {
			player.getActionSender().sendMessage(item.getDefinition().getName() + ": currently costs " + price + " " + new Item(currency).getDefinition().getName().toLowerCase() + ".");
		}
		boolean message = false;
		if (type == ShopType.GENERAL_STORE) {
			message = true;
		}
		if (type == ShopType.SPECIALIST_BUY) {
			if (getOriginalStock().contains(item.getId()) || getOriginalStock().contains(item.getId())) {
				message = true;
			}
		}
		if (item.getId() == currency) {
			message = false;
		}
		if (send && selling) {
			String shopAdd = "";
			if (price >= 1000 && price < 1000000) {
				shopAdd = "(" + (price / 1000) + "K).";
			} else if (price >= 1000000) {
				shopAdd = "(" + (price / 1000000) + " million).";
			}
			player.getActionSender().sendMessage(message ? item.getDefinition().getName() + ": shop will buy for " + price + " " + ItemManager.getInstance().getItemName(currency).toLowerCase() + " " + shopAdd : "This shop will not buy that item.");
		}
		return price;
	}

	/**
	 * A method for checking whether an item at a given slot was originally in
	 * the stock or not.
	 * 
	 * @param item The item to check for.
	 * @param slot The slot of the checked item.
	 * @return Whether the item is original or not.
	 */
	public boolean isOriginal(Item item, int slot) {
		Item original = getOriginalStock().get(slot);
		if (original == null) {
			return false;
		}
		if (original.getId() != item.getId()) {
			return false;
		}
		return true;
	}

	/**
	 * The normalization method. This is called once every x amount of time to
	 * remove unoriginal items and restore original items.
	 */
	public void normalize() {
		for (int i = 0; i < getCurrentStock().capacity(); i++) {
			Item item_current = getCurrentStock().get(i);
			if (item_current == null) {
				continue;
			}
			Item item_original = getOriginalStock().get(i);
			if (item_original == null) {
				// This item wasn't originally in the stock, so we deduct from
				// the current stack.
				getCurrentStock().remove(new Item(item_current.getId(), 1), i);
			} else {
				// This item was originally in the stock. We check if there is
				// more/less of it than there is meant to be.
				if (item_current.getCount() < item_original.getCount()) {
					getCurrentStock().set(i, new Item(item_current.getId(), item_current.getCount() + 1));
				}
				if (item_current.getCount() > item_original.getCount()) {
					getCurrentStock().set(i, new Item(item_current.getId(), item_current.getCount() - 1));
				}
			}
		}
	}

	/**
	 * Gets the identifier of this shop.
	 * 
	 * @return This shop's identifier.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the currency used in this shop.
	 * 
	 * @return The currency.
	 */
	public int getCurrency() {
		return currency;
	}

	/**
	 * Gets the name of this shop.
	 * 
	 * @return This shop's name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the current stock of this shop.
	 * 
	 * @param stock The stock to set to.
	 */
	public void setCurrentStock(Container stock) {
		this.currentStock = stock;
	}

	/**
	 * Sets the original stock of this shop.
	 * 
	 * @param stock The stock to set to.
	 */
	public void setOriginalStock(Container stock) {
		this.originalStock = stock;
	}

	/**
	 * Gets the list of items to create a container off of.
	 * 
	 * @return The item array.
	 */
	public Item[] getItems() {
		return items;
	}

	/**
	 * Gets the current item stock of this shop.
	 * 
	 * @return The current item stock.
	 */
	public Container getCurrentStock() {
		return currentStock;
	}

	/**
	 * Gets the original item stock of this shop.
	 * 
	 * @return The original item stock.
	 */
	public Container getOriginalStock() {
		return originalStock;
	}

	/**
	 * Gets the type of this shop.
	 * 
	 * @return This shop's type.
	 */
	public ShopType getType() {
		return type;
	}
}
