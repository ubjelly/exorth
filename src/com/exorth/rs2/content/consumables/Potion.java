package com.exorth.rs2.content.consumables;

import com.exorth.rs2.content.combat.util.Poison;
import com.exorth.rs2.content.consumables.PotionLoader.PotionDefinition;
import com.exorth.rs2.content.consumables.PotionLoader.PotionDefinition.PotionTypes;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class Potion {
	public static void drinkPotion(Player player, Item item, int slot) {
		if (player.getAttribute("cantDrink") != null) {
			return;
		}

		if (player.getAttribute("duel_button_drinks") != null) {
			player.getActionSender().sendMessage("You're not allowed to use drinks in this duel session.");
			return;
		}

		PotionDefinition type = PotionLoader.getPotion(item.getId());

		player.setAttribute("cantDrink", (byte) 0);

		if (type.getAffectedStats() != null) {
			for (int i = 0; i < type.getAffectedStats().length; i++) {
				if (type.getPotionType() == PotionTypes.BOOST) {
					int index = type.getAffectedStats()[i];
					int currentLevel = player.getSkill().getLevel()[index];

					if (index == Skill.PRAYER) {
						currentLevel = (int) player.getSkill().getPrayerPoints();
					}

					int actualLevel = player.getSkill().getLevelForXP(player.getSkill().getExp()[index]);

					int levelAfterDrink = actualLevel;

					levelAfterDrink += actualLevel * type.getStatModifiers()[i];
					levelAfterDrink += type.getStatAddons()[i];

					if (currentLevel < actualLevel) {
						player.getSkill().getLevel()[index] += levelAfterDrink - actualLevel;
						player.getSkill().refresh(index);
					} else if (currentLevel < levelAfterDrink) {
						player.getSkill().getLevel()[index] = (byte) levelAfterDrink;
						player.getSkill().refresh(index);
					}
				} else if (type.getPotionType() == PotionDefinition.PotionTypes.RESTORE) {
					int index = type.getAffectedStats()[i];
					int currentLevel = player.getSkill().getLevel()[index];

					if (index == Skill.PRAYER) {
						currentLevel = (int) player.getSkill().getPrayerPoints();
					}

					int actualLevel = player.getSkill().getLevelForXP(player.getSkill().getExp()[index]);

					int levelAfterDrink = currentLevel;

					levelAfterDrink += actualLevel * type.getStatModifiers()[i];
					levelAfterDrink += type.getStatAddons()[i];

					if (currentLevel > actualLevel) {
						continue;
					}

					if (levelAfterDrink <= actualLevel) {
						if (index != Skill.PRAYER) {
							player.getSkill().getLevel()[index] = (byte) levelAfterDrink;
						} else {
							player.getSkill().setPrayerPoints(levelAfterDrink);
						}
						player.getSkill().refresh(index);
					} else {
						if (index != Skill.PRAYER) {
							player.getSkill().getLevel()[index] = player.getSkill().getLevelForXP(player.getSkill().getExp()[index]);
						} else {
							player.getSkill().setPrayerPoints(player.getSkill().getLevelForXP(player.getSkill().getExp()[index]));
						}
						player.getSkill().refresh(index);
					}
				}
			}
		}

		doOtherPotionEffects(player, item.getId());

		player.getUpdateFlags().sendAnimation(829, 0);
		player.getActionSender().sendSound(334, 100, 0);

		if (!item.getDefinition().getName().contains("(1)")) {
			player.getInventory().getItemContainer().set(slot, new Item(type.getPotionIds()[item.getDefinition().getName().contains("(4)") ? 1 : item.getDefinition().getName().contains("(3)") ? 2 : 3]));
			player.getInventory().refresh(slot, player.getInventory().get(slot));
			player.getActionSender().sendMessage("You drink some of your " + type.getPotionName().toLowerCase() + ".");
			player.getActionSender().sendMessage("You have " + (item.getDefinition().getName().contains("(4)") ? "3 doses" : item.getDefinition().getName().contains("(3)") ? "2 doses" : "1 dose") + " of potion left.");
		} else {
			player.getActionSender().sendMessage("You drink some of your " + type.getPotionName().toLowerCase() + ".");
			player.getActionSender().sendMessage("You have finished your potion.");
			player.getInventory().getItemContainer().set(slot, new Item(229));
			player.getInventory().refresh(slot, player.getInventory().get(slot));
		}
		tickAfterDrinking(player);
	}

	private static void doOtherPotionEffects(Player player, int itemId) {
		switch (itemId) {
			case 3032:
			case 3034:
			case 3036:
			case 3038:
				int finalAgility = player.getSkill().getLevelForXP(player.getSkill().getExp()[16]) + 3;
				if (player.getSkill().getLevel()[16] < finalAgility) {
					player.getSkill().getLevel()[16] = (byte) finalAgility;
					player.getSkill().refresh(16);
				}
				break;
			case 2446: // Antipoison
			case 175:
			case 177:
			case 179:
				Poison.appendPoison(player, false, 0);
				player.setPoisonImmunityTimer(300);
				break;
			case 3008: // Energy
			case 3010:
			case 3012:
			case 3014:
				player.setEnergy(player.getEnergy() + 20);
				break;
			case 2448: // Super antipoison
			case 181:
			case 183:
			case 185:
				Poison.appendPoison(player, false, 0);
				player.setPoisonImmunityTimer(600);
				break;
			case 3016: // Super energy
			case 3018:
			case 3020:
			case 3022:
				player.setEnergy(player.getEnergy() + 40);
				break;
			case 5943: // Antipoison+
			case 5945:
			case 5947:
			case 5949:
				Poison.appendPoison(player, false, 0);
				player.setPoisonImmunityTimer(900);
				break;
			case 2452: // Antifire
			case 2454:
			case 2456:
			case 2458:
				player.setFireImmunityTimer(player.getFireImmunityTimer() + 600);
				break;
			case 5952: // Antipoison++
			case 5954:
			case 5956:
			case 5958:
				Poison.appendPoison(player, false, 0);
				player.setPoisonImmunityTimer(1200);
				break;
			case 6685: // Saradomin brew
			case 6687:
			case 6689:
			case 6691:
				Object[][] skillModifiers = {
					{ 0, 0.1 },
					{ 2, 0.1 },
					{ 4, 0.1 },
					{ 6, 0.1 }
				};
				player.getSkill().getLevel()[Skill.DEFENCE] = (byte) (player.getSkill().getLevel()[Skill.DEFENCE] + player.getSkill().getLevelForXP(player.getSkill().getExp()[Skill.DEFENCE]) * 0.15);

				for (int i = 0; i < skillModifiers.length; i++) {
					int levelModifier = (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[(Integer) skillModifiers[i][0]]) * (Double) skillModifiers[i][1]);

					if (player.getSkill().getLevel()[(Integer) skillModifiers[i][0]] - levelModifier <= 1) {
						player.getSkill().getLevel()[(Integer) skillModifiers[i][0]] = 1;
						player.getSkill().refresh((Integer) skillModifiers[i][0]);
						continue;
					}
					player.getSkill().getLevel()[(Integer) skillModifiers[i][0]] = (byte) (player.getSkill().getLevel()[(Integer) skillModifiers[i][0]] - levelModifier);
					player.getSkill().refresh((Integer) skillModifiers[i][0]);
				}

				int maximumDefence = (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[Skill.DEFENCE]) * 0.25) + player.getSkill().getLevelForXP(player.getSkill().getExp()[Skill.DEFENCE]);
				int newDefenceLevel = player.getSkill().getLevel()[Skill.DEFENCE];

				newDefenceLevel += (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[Skill.DEFENCE]) * 0.25);

				if (newDefenceLevel > maximumDefence) {
					player.getSkill().getLevel()[Skill.DEFENCE] = (byte) maximumDefence;
				} else {
					player.getSkill().getLevel()[Skill.DEFENCE] = (byte) newDefenceLevel;
				}

				player.getSkill().refresh(1);

				int maximumHitpoints = (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[Skill.HITPOINTS]) * 0.15) + player.getSkill().getLevelForXP(player.getSkill().getExp()[Skill.HITPOINTS]);
				int newHitpointsLevel = player.getSkill().getLevel()[Skill.HITPOINTS];
				newHitpointsLevel += 20;
				newHitpointsLevel += (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[Skill.HITPOINTS]) * 0.15);

				if (newHitpointsLevel > maximumHitpoints) {
					player.getSkill().getLevel()[Skill.HITPOINTS] = (byte) maximumHitpoints;
				} else {
					player.getSkill().getLevel()[Skill.HITPOINTS] = (byte) newHitpointsLevel;
				}
				player.getSkill().refresh(3);
				break;
			case 2450: // Zamorak brew
			case 189:
			case 191:
			case 193:
				int newLevel,
				maximumLevel;
				Object[][] statModifiers = {
					{ 1, 0.08, 1 },
					{ 3, 0.2, 0 }
				};
				for (int i = 0; i < statModifiers.length; i++) {
					newLevel = player.getSkill().getLevel()[(Integer) statModifiers[i][0]];
					newLevel -= (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[(Integer) statModifiers[i][0]]) * (Double) statModifiers[i][1]);
					newLevel -= (Integer) statModifiers[i][2];
					if (newLevel < 1) {
						player.getSkill().getLevel()[(Integer) statModifiers[i][0]] = 1;
					} else {
						player.getSkill().getLevel()[(Integer) statModifiers[i][0]] = (byte) newLevel;
					}
					player.getSkill().refresh((Integer) statModifiers[i][0]);
				}
				Object[][] statModifiers2 = {
					{ 0, 0.08, 1 },
					{ 2, 0.08, 1 },
					{ 5, 0.08, 20 }
				};
				for (int i = 0; i < statModifiers2.length; i++) {
					maximumLevel = (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[(Integer) statModifiers2[i][0]]) * (Double) statModifiers2[i][1]) + (Integer) statModifiers2[i][2] + player.getSkill().getLevelForXP(player.getSkill().getExp()[(Integer) statModifiers2[i][0]]);
					newLevel = player.getSkill().getLevel()[(Integer) statModifiers2[i][0]];
					newLevel += (int) (player.getSkill().getLevelForXP(player.getSkill().getExp()[(Integer) statModifiers2[i][0]]) * (Double) statModifiers2[i][1]);
					newLevel += (Integer) statModifiers2[i][2];
					if (i == 2 && newLevel > 99) {
						player.getSkill().getLevel()[(Integer) statModifiers2[i][0]] = 99;
					} else if (newLevel > maximumLevel) {
						player.getSkill().getLevel()[(Integer) statModifiers2[i][0]] = (byte) maximumLevel;
					} else {
						player.getSkill().getLevel()[(Integer) statModifiers2[i][0]] = (byte) newLevel;
					}
					player.getSkill().refresh((Integer) statModifiers2[i][0]);
				}
				break;
		}
	}

	private static void tickAfterDrinking(final Player player) {
		World.submit(new Task(2) {
			@Override
			protected void execute() {
				player.removeAttribute("cantDrink");
				this.stop();
			}
		});
	}
}