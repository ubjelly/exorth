package com.exorth.rs2.content.book.impl;

import java.util.ArrayList;

import com.exorth.rs2.content.book.Book;
import com.exorth.rs2.content.book.Page;
import com.exorth.rs2.content.book.ReadingSession;
import com.exorth.rs2.model.players.Player;

/**
 * A quick introduction to Exorth for new players.
 * @author Stephen
 */
public class GettingStarted extends ReadingSession<Player> {

	/**
	 * The player reading the book.
	 */
	private Player reader;
	
	/**
	 * Constructs the 'GettingStarted' book.
	 * @param reader The player reading the book.
	 */
	public GettingStarted(Player reader) {
		super(reader);
		this.reader = reader;
	}

	@Override
	public Book evaluate() {
		String content = 
			"Welcome to Exorth, " + reader.getUsername() + "! This book serves to provide a quick introduction on how to play Exorth. " + 
			"Exorth is an economy based server similar to RuneScape\\, but with higher xp rates and new,\\ original content. There are various ways to earn money in game. " +
			"One way to earn money is by killing monsters and picking up their loot. Loot can be traded to other players for money or other items. " +
			"Another way to earn money is by skilling. Skillers stray away from training combat to focus on other skills such as woodcutting. The items received from skilling can also be sold to other players for money or items. ";

		String[] words = content.split(" ");
		ArrayList<String> content_arrayList = new ArrayList<String>();
		String currLineContent = "";
		String currLineContentTemp = "";
		String currLineContentWord = "";
		int currentLines = 0;
		ArrayList<String> pages_arrayList = new ArrayList<String>();

		for (int i = 0; i < words.length; i++) {
			if (currentLines >= 22) {
				String page_content = content_arrayList.toString();
				pages_arrayList.add(page_content);
				content_arrayList.clear();
				currentLines = 0;
				i--;
			} else {
				currLineContentTemp = currLineContent;
				currLineContentWord = words[i];
				currLineContent = currLineContent + " " + currLineContentWord;
				if (currLineContent.length() >= 25) {
					content_arrayList.add(currLineContentTemp);
					currLineContent = "" + currLineContentWord;
					currentLines += 1;
				}
			}
		}

		content_arrayList.add(currLineContent);
		String page_content_p = content_arrayList.toString();
		pages_arrayList.add(page_content_p);
		content_arrayList.clear();
		String[] page_content = new String[pages_arrayList.size()];
		page_content = pages_arrayList.toArray(page_content);

		Book book = null;
		String init = "";
		String[] parts;
		for (int i = 0; i < page_content.length; i++) {
			if (i == 0) {
				init = page_content[i].substring(2, page_content[i].length() - 1);
				parts = new String[init.split("(?<!\\\\),[ ]+").length];
				parts = init.split("(?<!\\\\),[ ]+");
				for (int x = 0; x < parts.length; x++) {
					parts[x] = parts[x].replace("\\", "");
				}
				Page firstPage = new Page(parts);
				book = new Book("Getting Started", firstPage);
				parts = null;
			} else {
				init = page_content[i].substring(1, page_content[i].length() - 1);
				parts = new String[init.split("(?<!\\\\),[ ]+").length];
				parts = init.split("(?<!\\\\),[ ]+");
				for (int x = 0; x < parts.length; x++) {
					parts[x] = parts[x].replace("\\", "");
				}
				Page page = new Page(parts);
				book.add(page);
				parts = null;
				page = null;
			}
		}
		return book;
	}
}
