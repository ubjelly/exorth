package com.exorth.rs2.content.book.impl;

import java.util.ArrayList;

import com.exorth.rs2.content.book.Book;
import com.exorth.rs2.content.book.Page;
import com.exorth.rs2.content.book.ReadingSession;
import com.exorth.rs2.model.players.Player;

/**
 * A book on Baxtorian.
 * 
 * @author Joshua Barry
 * @author Mko
 */
public class Baxtorian extends ReadingSession<Player> {
	public Baxtorian(Player reader) {
		super(reader);
	}

	@Override
	public Book evaluate() {
		String content = 
		"~The Missing Relics~ " +
		"Many artefacts of elven history were lost after the fourth age\\, following the departure of the elven colonies from these lands. " + 
		"The greatest loss to our collections of elf history were the hidden treasures of Baxtorian\\, specifically the rumoured Chalice of Eternity. " +
		"Some believe these treasures are still unclaimed\\, but it is more commonly believed that dwarf miners recovered the treasure early in the 5th Age. " + 
		"Another great loss was Glarial's pebble\\, a key which allowed her family to visit her tomb. The pebble was taken by a gnome many years ago. " +
		"It is hoped that descendants of that gnome may still have the pebble hidden in their cave under the Tree Gnome Village. " +
		"Unfortunately the maze around that village makes it difficult to contact the gnomes to investigate this matter. " + 
		"~The Fall of Baxtorian~ " +
		"The love between Baxtorian and Glarial was said to have lasted over a century. " +
		"They lived a peaceful life learning and teaching the laws of nature. " +
		"When their homeland in the far west was plunged into chaos by dark forces\\, Baxtorian left on a dangerous campaign that lasted for five years. " +
		"He survived to return to this land\\, but found his people slaughtered and his wife taken by the enemy. " +
		"After years of searching for his love he finally gave up and returned to the home he made for Glarial under the Baxtorian Waterfall. " +
		"Once he entered he never returned. Only he and Glarial had the power to enter the waterfall. " +
		"Since Baxtorian entered\\, no-one else can follow him in\\, it's as if the powers of nature still work to protect his peace. ";

		String[] words = content.split(" ");
		ArrayList<String> content_arrayList = new ArrayList<String>();
		String currLineContent = "";
		String currLineContentTemp = "";
		String currLineContentWord = "";
		int currentLines = 0;
		ArrayList<String> pages_arrayList = new ArrayList<String>();

		for (int i = 0; i < words.length; i++) {
			if (currentLines >= 22) {
				String page_content = content_arrayList.toString();
				pages_arrayList.add(page_content);
				content_arrayList.clear();
				currentLines = 0;
				i--;
			} else {
				currLineContentTemp = currLineContent;
				currLineContentWord = words[i];
				currLineContent = currLineContent + " " + currLineContentWord;
				if (currLineContent.length() >= 25) {
					content_arrayList.add(currLineContentTemp);
					currLineContent = "" + currLineContentWord;
					currentLines += 1;
				}
			}
		}

		content_arrayList.add(currLineContent);
		String page_content_p = content_arrayList.toString();
		pages_arrayList.add(page_content_p);
		content_arrayList.clear();
		String[] page_content = new String[pages_arrayList.size()];
		page_content = pages_arrayList.toArray(page_content);

		Book book = null;
		String init = "";
		String[] parts;
		for (int i = 0; i < page_content.length; i++) {
			if (i == 0) {
				init = page_content[i].substring(2, page_content[i].length() - 1);
				//System.out.println("init: " + init);
				parts = new String[init.split("(?<!\\\\),[ ]+").length];
				parts = init.split("(?<!\\\\),[ ]+");
				for (int x = 0; x < parts.length; x++) {
					parts[x] = parts[x].replace("\\", "");
				}
				Page firstPage = new Page(parts);
				book = new Book("Book on Baxtorian", firstPage);
				parts = null;
			} else {
				init = page_content[i].substring(1, page_content[i].length() - 1);
				//System.out.println("init: " + init);
				parts = new String[init.split("(?<!\\\\),[ ]+").length];
				parts = init.split("(?<!\\\\),[ ]+");
				for (int x = 0; x < parts.length; x++) {
					parts[x] = parts[x].replace("\\", "");
				}
				Page page = new Page(parts);
				book.add(page);
				parts = null;
				page = null;
			}
		}

		return book;
	}
}