package com.exorth.rs2.content.book;

/**
 * 
 * @author Joshua Barry
 * 
 */
public abstract class ReadingSession<T> {
	/**
	 * The main reader.
	 */
	private T reader;

	/**
	 * 
	 * @param player The player who will read the book.
	 */
	public ReadingSession(T reader) {
		this.reader = reader;
	}

	/**
	 * Set up a new book.
	 * 
	 * @return
	 */
	public abstract Book evaluate();

	public T getReader() {
		return reader;
	}
}