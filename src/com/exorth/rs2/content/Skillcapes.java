package com.exorth.rs2.content;

import com.exorth.rs2.model.players.Player;

/**
 * Handles everything related to skillcapes. (Emotes, trimming, TODO: Requirements)
 * 
 * @author AkZu
 */
public class Skillcapes {

	public static void doSkillcape(Player player) {
		Skillcape.ATTACK.doEmote(player);
		Skillcape.STRENGTH.doEmote(player);
		Skillcape.DEFENCE.doEmote(player);
		Skillcape.RANGED.doEmote(player);
		Skillcape.PRAYER.doEmote(player);
		Skillcape.MAGIC.doEmote(player);
		Skillcape.RUNECRAFTING.doEmote(player);
		Skillcape.HITPOINTS.doEmote(player);
		Skillcape.AGILITY.doEmote(player);
		Skillcape.HERBLORE.doEmote(player);
		Skillcape.THIEVING.doEmote(player);
		Skillcape.CRAFTING.doEmote(player);
		Skillcape.FLETCHING.doEmote(player);
		Skillcape.SLAYER.doEmote(player);
		Skillcape.CONSTRUCTION.doEmote(player);
		Skillcape.MINING.doEmote(player);
		Skillcape.SMITHING.doEmote(player);
		Skillcape.FISHING.doEmote(player);
		Skillcape.COOKING.doEmote(player);
		Skillcape.FIREMAKING.doEmote(player);
		Skillcape.WOODCUTTING.doEmote(player);
		Skillcape.FARMING.doEmote(player);
		Skillcape.QUESTPOINT.doEmote(player);
	}

	public static enum Skillcape {
		//TODO: Trimmed Skillcapes + Hoods
		ATTACK(9747, 4959, 823),
		STRENGTH(9750, 4981, 828),
		DEFENCE(9753, 4961, 824),
		RANGED(9756, 4973, 832),
		PRAYER(9759, 4979, 829),
		MAGIC(9762, 4939, 813),
		RUNECRAFTING(9765, 4947, 817),
		HITPOINTS(9768, 4971, 833),
		AGILITY(9771, 4977, 830),
		HERBLORE(9774, 4969, 835),
		THIEVING(9777, 4965, 826),
		CRAFTING(9780, 4949, 818),
		FLETCHING(9783, 4937, 812),
		SLAYER(9786, 4967, 827),
		CONSTRUCTION(9789, 4953, 820),
		MINING(9792, 4941, 814),
		SMITHING(9795, 4943, 815),
		FISHING(9798, 4951, 819),
		COOKING(9801, 4955, 821),
		FIREMAKING(9804, 4975, 831),
		WOODCUTTING(9807, 4957, 822),
		FARMING(9810, 4963, 825),
		QUESTPOINT(9813, 4945, 816);

		private int item;
		private int animation;
		private int gfx;

		private Skillcape(int item, int anim, int gfx) {
			this.item = item;
			this.animation = anim;
			this.gfx = gfx;
		}

		public final void doEmote(Player player) {
			if (player.getEquipment().getItemContainer().getById(this.getItem()) == null && player.getEquipment().getItemContainer().getById(this.getItem() + 1) == null) {
				return;
			}
			player.getUpdateFlags().sendAnimation(this.getAnim(), 0);
			if (player.getGender() == 1 && this.equals(HITPOINTS)) {
				player.getUpdateFlags().sendGraphic(this.getGfx() + 1, 0);
			} else {
				player.getUpdateFlags().sendGraphic(this.getGfx(), 0);
			}
		}

		public final String getData() {
			return toString() + " (Anim: " + animation + ", GFX:" + gfx + ", Item ID: " + item + ")";
		}

		public final int getAnim() {
			return animation;
		}

		public final int getGfx() {
			return gfx;
		}

		public final int getItem() {
			return item;
		}
	}
}