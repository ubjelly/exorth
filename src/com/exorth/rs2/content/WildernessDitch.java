package com.exorth.rs2.content;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author AkZu
 * 
 */
public class WildernessDitch {
	public final static boolean handleDitch(final Player player, int x) {
		if (player.getPosition().getY() > 3520) {
			if (player.getPosition().getY() != 3523) {
				return false;
			}
		} else if (player.getPosition().getY() != 3520) {
			return false;
		}
		if (player.getPosition().getX() != x) {
			return false;
		}

		player.getUpdateFlags().sendFaceToDirection(new Position(x, (player.getPosition().getY() == 3523 ? 3520 : 3523), player.getPosition().getZ()));

		if (!player.canWalk()) {
			return false;
		}

		player.setCanWalk(false);

		player.getUpdateFlags().sendAnimation(6132, 0);
		World.submit(new Task(1) {
			@Override
			protected void execute() {
				player.getUpdateFlags().sendForceMovement(0, (player.getPosition().getY() == 3523 ? -3 : 3), 6, 26, (player.getPosition().getY() == 3523 ? 2 : 0));
				World.submit(new Task(2) {
					@Override
					protected void execute() {
						player.teleport(new Position(player.getPosition().getX(), (player.getPosition().getY() == 3523 ? 3520 : 3523), player.getPosition().getZ()));
						World.submit(new Task(1) {
							@Override
							protected void execute() {
								player.setCanWalk(true);
								this.stop();
							}
						});
						this.stop();
					}
				});
				this.stop();
			}
		});
		return true;
	}
}