package com.exorth.rs2.content.skills.farming;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public enum Chore {

	RAKING(2273, 5341),
	DIBBING(2291, 5343),
	DIGGING(830, 952);

	private int animation;

	private Integer[] tools;

	/**
	 * The foundation of each chore in farming.
	 * 
	 * @param animation The animation displaying in the process.
	 * @param tools The tools used in the process.
	 */
	private Chore(int animation, Integer... tools) {
		this.tools = tools;
		this.animation = animation;
	}

	/**
	 * Public access to each chore based on the stage of order.
	 * 
	 * @param stage
	 * @return
	 */
	public static Chore forStage(int stage) {
		for (Chore chore : Chore.values()) {
			if (stage == chore.ordinal()) {
				return chore;
			}
		}
		return null;
	}

	/**
	 * Gets a chore based on the tool.
	 * 
	 * @param tool
	 * @return
	 */
	public static Chore forTool(int tool) {
		for (Chore chore : Chore.values()) {
			for (int i : chore.getTools()) {
				if (tool == i) {
					return chore;
				}
			}
		}
		return null;
	}

	public int getAnimation() {
		return animation;
	}

	public Integer[] getTools() {
		return tools;
	}
}
