package com.exorth.rs2.content.skills.smithing;

import com.exorth.rs2.action.impl.ProductionAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Misc;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Forging extends ProductionAction {

	Player player;
	Product product;
	int productionAmount;

	public Forging(Player player, Product product, int productionAmount) {
		super(player);
		this.player = player;
		this.product = product;
		this.productionAmount = productionAmount;
	}

	@Override
	public boolean canProduce() {
		if (player.getInventory().getItemContainer().freeSlots() < 1) {
			player.getActionSender().sendMessage("Not enough space in your inventory.");
			return false;
		}
		if (!player.getInventory().getItemContainer().contains(2347)) {
			player.getActionSender().sendMessage("You need a hammer to do this.");
			return false;
		}
		return true;
	}

	@Override
	public int getCycleCount() {
		return 5;
	}

	@Override
	public int getGraphic() {
		return -1;
	}

	@Override
	public int getAnimation() {
		return 898;
	}

	@Override
	public double getExperience() {
		return product.getExperience();
	}

	@Override
	public Item[] getConsumedItems() {
		return product.getMaterials();
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You must have a " + Skill.getSkillName(Skill.SMITHING) + " level of " + product.getLevelRequirement() + " to smith this item.";
	}

	@Override
	public int getProductionCount() {
		return productionAmount;
	}

	@Override
	public int getRequiredLevel() {
		return product.getLevelRequirement();
	}

	@Override
	public Item[] getRewards() {
		return new Item[]{ product.getProduct() };
	}

	@Override
	public int getSkill() {
		return Skill.SMITHING;
	}

	@Override
	public String getPreProductionMessage() {
		return null;
	}

	@Override
	public String getSuccessfulProductionMessage() {
		String name = ItemManager.getInstance().getItemName(product.getProduct().getId()).toLowerCase();
		String prefix = product.getProduct().getCount() > 1 ? Integer.toString(product.getProduct().getCount()) : Misc.getArticle(name);
		return "You hammer the " + name.split(" ")[0] + " and make " + prefix + " " + name + ".";
	}

	@Override
	public String getInsufficientResourcesMessage() {
		return "You need more bars to do this.";
	}
}
