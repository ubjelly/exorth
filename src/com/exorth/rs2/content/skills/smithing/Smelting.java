package com.exorth.rs2.content.skills.smithing;

import com.exorth.rs2.action.impl.ProductionAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.util.Misc;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class Smelting extends ProductionAction {

	/**
	 * The bar to smelt.
	 */
	private Bar bar;

	/**
	 * The amount of bars to smelt.
	 */
	private int productionAmount;

	/**
	 * 
	 * @param entity
	 * @param bar
	 * @param productionAmount
	 */
	public Smelting(Entity entity, Bar bar, int productionAmount) {
		super(entity);
		this.bar = bar;
		this.productionAmount = productionAmount;
	}

	@Override
	public boolean canProduce() {
		return true;
	}

	@Override
	public int getCycleCount() {
		return 4;
	}

	@Override
	public int getGraphic() {
		return -1;
	}

	@Override
	public int getAnimation() {
		return 899;
	}

	@Override
	public double getExperience() {
		return bar.getExperience();
	}

	@Override
	public Item[] getConsumedItems() {
		return bar.getOres();
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You need a " + Skill.getSkillName(Skill.SMITHING) + " level of " + getRequiredLevel() + " to smelt these ores.";
	}

	@Override
	public int getProductionCount() {
		return productionAmount;
	}

	@Override
	public int getRequiredLevel() {
		return bar.getLevelRequired();
	}

	@Override
	public Item[] getRewards() {
		return new Item[]{ new Item(bar.getId()) };
	}

	@Override
	public int getSkill() {
		return Skill.SMITHING;
	}

	@Override
	public String getPreProductionMessage() {
		String str = "You place the ores into the furnace...";
		return str;
	}

	@Override
	public String getSuccessfulProductionMessage() {
		String itemName = ItemManager.getInstance().getItemName(bar.getId()).toLowerCase();
		return "You retrieve " + Misc.getArticle(itemName) + " " + itemName + " from the furnace.";
	}

	@Override
	public String getInsufficientResourcesMessage() {
		return "You do not have the required ores to smelt this.";
	}
}
