package com.exorth.rs2.content.skills.smithing;

import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;

public class SmithingFrame {

	public static void sendInterface(Player player, Bar type) {
		// Level requirements
		int dagger = Product.forId(dagger(type).getId()).getLevelRequirement();
		int axe = Product.forId(axe(type).getId()).getLevelRequirement();
		int chainBody = Product.forId(chainBody(type).getId()).getLevelRequirement();
		int medHelm = Product.forId(medHelm(type).getId()).getLevelRequirement();
		int dartTips = Product.forId(dartTips(type).getId()).getLevelRequirement();
		int sword = Product.forId(sword(type).getId()).getLevelRequirement();
		int mace = Product.forId(mace(type).getId()).getLevelRequirement();
		int plateLegs = Product.forId(plateLegs(type).getId()).getLevelRequirement();
		int fullHelm = Product.forId(fullHelm(type).getId()).getLevelRequirement();
		int arrowTips = Product.forId(arrowTips(type).getId()).getLevelRequirement();
		int scimitar = Product.forId(scimitar(type).getId()).getLevelRequirement();
		int warHammer = Product.forId(warHammer(type).getId()).getLevelRequirement();
		int plateSkirt = Product.forId(plateSkirt(type).getId()).getLevelRequirement();
		int squareShield = Product.forId(squareShield(type).getId()).getLevelRequirement();
		int throwingKnives = Product.forId(throwingKnives(type).getId()).getLevelRequirement();
		int longSword = Product.forId(longSword(type).getId()).getLevelRequirement();
		int battleAxe = Product.forId(battleAxe(type).getId()).getLevelRequirement();
		int plateBody = Product.forId(plateBody(type).getId()).getLevelRequirement();
		int kiteShield = Product.forId(kiteShield(type).getId()).getLevelRequirement();
		int twoHandedSword = Product.forId(twoHandedSword(type).getId()).getLevelRequirement();
		int claws = Product.forId(claws(type).getId()).getLevelRequirement();

		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= dagger ? "@whi@Dagger" : "@bla@Dagger", 1094);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= axe ? "@whi@Axe" : "@bla@Axe", 1091);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= chainBody ? "@whi@Chain body" : "@bla@Chain body", 1098);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= medHelm ? "@whi@Medium helm" : "@bla@Medium helm", 1102);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= dartTips ? "@whi@Dart tips" : "@bla@Dart tips", 1107);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= sword ? "@whi@Sword" : "@bla@Sword", 1085);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= mace ? "@whi@Mace" : "@bla@Mace", 1093);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= plateLegs ? "@whi@Plate legs" : "@bla@Plate legs", 1099);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= fullHelm ? "@whi@Full helm" : "@bla@Full helm", 1103);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= arrowTips ? "@whi@Arrowtips" : "@bla@Arrowtips", 1108);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= scimitar ? "@whi@Scimitar" : "@bla@Scimitar", 1087);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= warHammer ? "@whi@Warhammer" : "@bla@Warhammer", 1083);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= plateSkirt ? "@whi@Plate skirt" : "@bla@Plate skirt", 1100);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= squareShield ? "@whi@Square shield" : "@bla@Square shield", 1104);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= throwingKnives ? "@whi@Throwing knives" : "@bla@Throwing Knives", 1106);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= longSword ? "@whi@Long sword" : "@bla@Long sword", 1086);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= battleAxe ? "@whi@Battle axe" : "@bla@Battle axe", 1092);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= plateBody ? "@whi@Plate body" : "@bla@Plate body", 1101);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= kiteShield ? "@whi@Kite shield" : "@bla@Kite shield", 1105);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= twoHandedSword ? "@whi@2 hand sword" : "@bla@2 hand sword", 1088);
		player.getActionSender().sendString(player.getSkillLevel(Skill.SMITHING) >= claws ? "@whi@Claws" : "@bla@Claws", 8429);

		player.getActionSender().sendConfig(210, player.getInventory().getItemContainer().getCount(type.getId()));

		//Row 1
		player.getActionSender().sendUpdateItem(0,1119, dagger(type));
		player.getActionSender().sendUpdateItem(0, 1120, axe(type));
		player.getActionSender().sendUpdateItem(0, 1121, chainBody(type));
		player.getActionSender().sendUpdateItem(0, 1122, medHelm(type));
		player.getActionSender().sendUpdateItem(0, 1123, dartTips(type));

		//Row 2
		player.getActionSender().sendUpdateItem(1,1119, sword(type));
		player.getActionSender().sendUpdateItem(1, 1120, mace(type));
		player.getActionSender().sendUpdateItem(1, 1121, plateLegs(type));
		player.getActionSender().sendUpdateItem(1, 1122, fullHelm(type));
		player.getActionSender().sendUpdateItem(1, 1123, arrowTips(type));

		//Row 3
		player.getActionSender().sendUpdateItem(2,1119, scimitar(type));
		player.getActionSender().sendUpdateItem(2, 1120, warHammer(type));
		player.getActionSender().sendUpdateItem(2, 1121, plateSkirt(type));
		player.getActionSender().sendUpdateItem(2, 1122, squareShield(type));
		player.getActionSender().sendUpdateItem(2, 1123, throwingKnives(type));

		//Row 4
		player.getActionSender().sendUpdateItem(3,1119, longSword(type));
		player.getActionSender().sendUpdateItem(3, 1120, battleAxe(type));
		player.getActionSender().sendUpdateItem(3, 1121, plateBody(type));
		player.getActionSender().sendUpdateItem(3, 1122, kiteShield(type));

		//Row 5
		player.getActionSender().sendUpdateItem(4,1119, twoHandedSword(type));
		player.getActionSender().sendUpdateItem(4, 1120, claws(type));

		player.getActionSender().sendString("", 1135);
		player.getActionSender().sendString("", 1134);
		player.getActionSender().sendString("", 11461);
		player.getActionSender().sendString("", 11459);
		player.getActionSender().sendString("", 1132);
		player.getActionSender().sendString("", 1096);
		player.getActionSender().sendString("", 13358);
		player.getActionSender().sendString("", 13357);

		player.getActionSender().sendInterface(994);
	}

	private static Item dagger(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1205);
		if (type == Bar.IRON)
			return new Item(1203);
		if (type == Bar.STEEL)
			return new Item(1207);
		if (type == Bar.MITHRIL)
			return new Item(1209);
		if (type == Bar.ADAMANT)
			return new Item(1211);
		if (type == Bar.RUNITE)
			return new Item(1213);
		return null;
	}
	
	private static Item axe(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1351);
		if (type == Bar.IRON)
			return new Item(1349);
		if (type == Bar.STEEL)
			return new Item(1353);
		if (type == Bar.MITHRIL)
			return new Item(1355);
		if (type == Bar.ADAMANT)
			return new Item(1357);
		if (type == Bar.RUNITE)
			return new Item(1359);
		return null;
	}
	
	private static Item chainBody(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1103);
		if (type == Bar.IRON)
			return new Item(1101);
		if (type == Bar.STEEL)
			return new Item(1105);
		if (type == Bar.MITHRIL)
			return new Item(1109);
		if (type == Bar.ADAMANT)
			return new Item(1111);
		if (type == Bar.RUNITE)
			return new Item(1113);
		return null;
	}
	
	private static Item medHelm(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1139);
		if (type == Bar.IRON)
			return new Item(1137);
		if (type == Bar.STEEL)
			return new Item(1141);
		if (type == Bar.MITHRIL)
			return new Item(1143);
		if (type == Bar.ADAMANT)
			return new Item(1145);
		if (type == Bar.RUNITE)
			return new Item(1147);
		return null;
	}
	
	private static Item dartTips(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(819, 10);
		if (type == Bar.IRON)
			return new Item(820, 10);
		if (type == Bar.STEEL)
			return new Item(821, 10);
		if (type == Bar.MITHRIL)
			return new Item(822, 10);
		if (type == Bar.ADAMANT)
			return new Item(823, 10);
		if (type == Bar.RUNITE)
			return new Item(824, 10);
		return null;
	}
	
	private static Item sword(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1277);
		if (type == Bar.IRON)
			return new Item(1279);
		if (type == Bar.STEEL)
			return new Item(1281);
		if (type == Bar.MITHRIL)
			return new Item(1285);
		if (type == Bar.ADAMANT)
			return new Item(1287);
		if (type == Bar.RUNITE)
			return new Item(1289);
		return null;
	}
	
	private static Item mace(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1422);
		if (type == Bar.IRON)
			return new Item(1420);
		if (type == Bar.STEEL)
			return new Item(1424);
		if (type == Bar.MITHRIL)
			return new Item(1428);
		if (type == Bar.ADAMANT)
			return new Item(1430);
		if (type == Bar.RUNITE)
			return new Item(1432);
		return null;
	}
	
	private static Item plateLegs(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1075);
		if (type == Bar.IRON)
			return new Item(1067);
		if (type == Bar.STEEL)
			return new Item(1069);
		if (type == Bar.MITHRIL)
			return new Item(1071);
		if (type == Bar.ADAMANT)
			return new Item(1073);
		if (type == Bar.RUNITE)
			return new Item(1079);
		return null;
	}
	
	private static Item fullHelm(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1155);
		if (type == Bar.IRON)
			return new Item(1153);
		if (type == Bar.STEEL)
			return new Item(1157);
		if (type == Bar.MITHRIL)
			return new Item(1159);
		if (type == Bar.ADAMANT)
			return new Item(1161);
		if (type == Bar.RUNITE)
			return new Item(1163);
		return null;
	}
	
	private static Item arrowTips(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(39, 15);
		if (type == Bar.IRON)
			return new Item(40, 15);
		if (type == Bar.STEEL)
			return new Item(41, 15);
		if (type == Bar.MITHRIL)
			return new Item(42, 15);
		if (type == Bar.ADAMANT)
			return new Item(43, 15);
		if (type == Bar.RUNITE)
			return new Item(44, 15);
		return null;
	}
	
	private static Item scimitar(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1321);
		if (type == Bar.IRON)
			return new Item(1323);
		if (type == Bar.STEEL)
			return new Item(1325);
		if (type == Bar.MITHRIL)
			return new Item(1329);
		if (type == Bar.ADAMANT)
			return new Item(1331);
		if (type == Bar.RUNITE)
			return new Item(1333);
		return null;
	}
	
	private static Item warHammer(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1337);
		if (type == Bar.IRON)
			return new Item(1335);
		if (type == Bar.STEEL)
			return new Item(1339);
		if (type == Bar.MITHRIL)
			return new Item(1343);
		if (type == Bar.ADAMANT)
			return new Item(1345);
		if (type == Bar.RUNITE)
			return new Item(1347);
		return null;
	}
	
	private static Item plateSkirt(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1087);
		if (type == Bar.IRON)
			return new Item(1081);
		if (type == Bar.STEEL)
			return new Item(1083);
		if (type == Bar.MITHRIL)
			return new Item(1085);
		if (type == Bar.ADAMANT)
			return new Item(1091);
		if (type == Bar.RUNITE)
			return new Item(1093);
		return null;
	}
	
	private static Item squareShield(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1173);
		if (type == Bar.IRON)
			return new Item(1175);
		if (type == Bar.STEEL)
			return new Item(1177);
		if (type == Bar.MITHRIL)
			return new Item(1181);
		if (type == Bar.ADAMANT)
			return new Item(1183);
		if (type == Bar.RUNITE)
			return new Item(1185);
		return null;
	}
	
	private static Item throwingKnives(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(864, 5);
		if (type == Bar.IRON)
			return new Item(863, 5);
		if (type == Bar.STEEL)
			return new Item(865, 5);
		if (type == Bar.MITHRIL)
			return new Item(866, 5);
		if (type == Bar.ADAMANT)
			return new Item(867, 5);
		if (type == Bar.RUNITE)
			return new Item(868, 5);
		return null;
	}
	
	private static Item longSword(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1291);
		if (type == Bar.IRON)
			return new Item(1293);
		if (type == Bar.STEEL)
			return new Item(1295);
		if (type == Bar.MITHRIL)
			return new Item(1299);
		if (type == Bar.ADAMANT)
			return new Item(1301);
		if (type == Bar.RUNITE)
			return new Item(1303);
		return null;
	}
	
	private static Item battleAxe(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1375);
		if (type == Bar.IRON)
			return new Item(1363);
		if (type == Bar.STEEL)
			return new Item(1365);
		if (type == Bar.MITHRIL)
			return new Item(1369);
		if (type == Bar.ADAMANT)
			return new Item(1371);
		if (type == Bar.RUNITE)
			return new Item(1373);
		return null;
	}
	
	private static Item plateBody(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1117);
		if (type == Bar.IRON)
			return new Item(1115);
		if (type == Bar.STEEL)
			return new Item(1119);
		if (type == Bar.MITHRIL)
			return new Item(1121);
		if (type == Bar.ADAMANT)
			return new Item(1123);
		if (type == Bar.RUNITE)
			return new Item(1127);
		return null;
	}
	
	private static Item kiteShield(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1189);
		if (type == Bar.IRON)
			return new Item(1191);
		if (type == Bar.STEEL)
			return new Item(1193);
		if (type == Bar.MITHRIL)
			return new Item(1197);
		if (type == Bar.ADAMANT)
			return new Item(1199);
		if (type == Bar.RUNITE)
			return new Item(1201);
		return null;
	}
	
	private static Item twoHandedSword(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(1307);
		if (type == Bar.IRON)
			return new Item(1309);
		if (type == Bar.STEEL)
			return new Item(1311);
		if (type == Bar.MITHRIL)
			return new Item(1315);
		if (type == Bar.ADAMANT)
			return new Item(1317);
		if (type == Bar.RUNITE)
			return new Item(1319);
		return null;
	}
	
	private static Item claws(Bar type) {
		if (type == Bar.BRONZE)
			return new Item(3095);
		if (type == Bar.IRON)
			return new Item(3096);
		if (type == Bar.STEEL)
			return new Item(3097);
		if (type == Bar.MITHRIL)
			return new Item(3099);
		if (type == Bar.ADAMANT)
			return new Item(3100);
		if (type == Bar.RUNITE)
			return new Item(3101);
		return null;
	}
}
