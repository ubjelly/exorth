package com.exorth.rs2.content.skills.runecrafting;

import com.exorth.rs2.action.impl.ProductionAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Misc;

/**
 * Runecrafting is a magical action that happens when a player activates the
 * runecrafting altar. The player must be carrying the appropriate talisman to
 * match the alter and runes they are trying to craft.
 * 
 * TODO: Talisman locate.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class Runecraft extends ProductionAction {

	/**
	 * The rune type we will craft.
	 */
	private Rune rune;

	/**
	 * Amount of runes we will be crafting.
	 */
	private int amount;

	private int essence;

	public Runecraft(Entity entity, Rune rune) {
		super(entity);
		this.rune = rune;
		this.essence = ((Player) getEntity()).getInventory().getItemContainer().contains(7936) ? 7936 : 1436;
		this.amount = ((Player) getEntity()).getInventory().getItemContainer().getCount(essence);
	}

	public int getEssence() {
		return essence;
	}

	/**
	 * 
	 * @return The amount of essence in the player's inventory.
	 */
	public int getEssenceAmount() {
		return amount;
	}

	@Override
	public boolean canProduce() {
		return 0 < getEssenceAmount();
	}

	@Override
	public int getCycleCount() {
		return 6;
	}

	@Override
	public int getGraphic() {
		return 186;
	}

	@Override
	public int getAnimation() {
		return 791;
	}

	@Override
	public double getExperience() {
		return rune.getExperience();
	}

	@Override
	public Item[] getConsumedItems() {
		return new Item[]{ new Item(getEssence(), getEssenceAmount()) };
	}

	public String getInsufficientResourcesMessage() {
		return "You need essences to perform this action.";
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You need a " + Skill.SKILL_NAME[getSkill()] + " level of " + rune.getLevelRequired() + " to craft runes here.";
	}

	@Override
	public int getProductionCount() {
		return 1;
	}

	@Override
	public int getRequiredLevel() {
		return rune.getLevelRequired();
	}

	@Override
	public Item[] getRewards() {
		return new Item[]{ new Item(rune.getReward(), amount) };
	}

	@Override
	public int getSkill() {
		return Skill.RUNECRAFTING;
	}

	@Override
	public String getPreProductionMessage() {
		return null;
	}

	@Override
	public String getSuccessfulProductionMessage() {
		String itemName = getRewards()[0].getDefinition().getName().toLowerCase();
		String prefix = (getEssenceAmount() == 1) ? Misc.getArticle(itemName) : " ";
		String suffix = (getEssenceAmount() == 1) ? "." : "s.";
		return "You bind the temple's power into" + prefix + itemName + suffix;
	}
}
