package com.exorth.rs2.content.skills.fishing;

import com.exorth.rs2.action.impl.EntityInteraction;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.StatementDialogue;
import com.exorth.rs2.content.dialogue.StatementType;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Language;
import com.exorth.rs2.util.Misc;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Fishing extends EntityInteraction {

	/**
	 * The fish we are trying to catch.
	 */
	private Fishable fish;

	/**
	 * The fishing equipment we will be using.
	 */
	private Tools tool;

	/**
	 * Determine whether we are fishing or not.
	 */
	private boolean started;

	/**
	 * The current cycles remaining.
	 */
	private int currentCycles = 0;

	/**
	 * Used to keep the animation playing, see {@code execute}
	 */
	private int difference = 0;

	/**
	 * 
	 * @param player The player who is fishing.
	 * @param fish The fish we are trying to catch.
	 * @param tool The tool the player will be fishing with.
	 */
	public Fishing(Player player, Fishable fish, Tools tool) {
		super(player, 0);
		this.fish = fish;
		this.tool = tool;
		this.started = false;
	}

	/**
	 * Determine whether or not the player can fish.
	 * 
	 * @return The players ability to fish.
	 */
	private boolean isAbleToFish() {
		Player player = (Player) getEntity();

		if (player.getSkillLevel(Skill.FISHING) < getRequiredLevel()) {
			Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, "You need a Fishing level of " + getRequiredLevel() + " to fish here.");
			player.open(dialogue);
			return false;
		}

		if (!player.getInventory().getItemContainer().contains(fish.getToolId())) {
			player.getActionSender().sendMessage("You need a " + ItemManager.getInstance().getItemName(fish.getToolId()).toLowerCase() + " to fish here.");
			return false;
		}

		if (this.getConsumedItems() != null) {
			if (!player.getInventory().getItemContainer().contains(getConsumedItems()[0].getId())) {
				player.getActionSender().sendMessage("You do not have any " + ItemManager.getInstance().getItemName(fish.getBaitRequired()).toLowerCase() + ".");
				return false;
			}
		}

		if (!player.getInventory().hasRoomFor(getRewards()[0])) {
			player.getActionSender().sendMessage(Language.NO_SPACE);
			return false;
		}

		return true;
	}

	/**
	 * 
	 * @return The amount of cycles
	 */
	public int getCycleCount() {
		int level = fish.getRequiredLevel();
		double var1 = Math.ceil((level * 50 - getEntity().getSkillLevel(Skill.FISHING) * 10));
		double val2 = level * 0.0625 * 3;
		double val3 = Misc.random((int) (level * 0.0625));
		double val4 = Misc.random(level - (7 + Misc.random(4)));
		double cycle = var1 / val2 * val3 + val4;
		if (getEntity().getAttribute("isDebugging") != null) {
			System.out.println("Variables: \n" + "VAR1: " + var1 + "\nVAL2: " + val2 + "\nVAL3: " + val3 + "\nVAL4: " + val4);
			System.out.println("Cycle: " + cycle + " >> [ (" + var1 + "/" + val2 + ") * (" + val3 + " + " + val4 + ") ]");
		}
		if (cycle < 1) {
			cycle = 1;
		}
		return (int) Math.round(cycle);
	}

	public boolean isStarted() {
		return started;
	}

	public int getCurrentCycles() {
		return currentCycles;
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You need a " + Skill.SKILL_NAME[Skill.FISHING] + " level of " + getRequiredLevel() + " to catch " + ItemManager.getInstance().getItemName(fish.getRawFishId()).toLowerCase().replace("raw ", "") + ".";
	}

	@Override
	public String getInteractionMessage() {
		return "You attempt to catch a " + ItemManager.getInstance().getItemName(fish.getRawFishId()).toLowerCase().replace("raw ", "") + ".";
	}

	@Override
	public String getSuccessfulInteractionMessage() {
		String prefix = (ItemManager.getInstance().getItemName(fish.getRawFishId()).endsWith("s")) ? "some" : (Misc.getArticle(ItemManager.getInstance().getItemName(fish.getRawFishId())));
		return "You catch " + prefix + " " + ItemManager.getInstance().getItemName(fish.getRawFishId()).toLowerCase().replace("raw ", "") + ".";
	}

	@Override
	public String getUnsuccessfulInteractionMessage() {
		return "Unused";
	}

	@Override
	public Npc getInteractingNpc() {
		return (Npc) getEntity().getInteractingEntity();
	}

	@Override
	public Item[] getConsumedItems() {
		if (fish.getBaitRequired() != -1) {
			return new Item[]{ new Item(fish.getBaitRequired()) };
		}
		return null;
	}

	@Override
	public Item[] getRewards() {
		return new Item[]{ new Item(fish.getRawFishId()) };
	}

	@Override
	public int getRequiredLevel() {
		return fish.getRequiredLevel();
	}

	@Override
	public CancelPolicy getCancelPolicy() {
		return CancelPolicy.ALWAYS;
	}

	@Override
	public StackPolicy getStackPolicy() {
		return StackPolicy.NEVER;
	}

	@Override
	public AnimationPolicy getAnimationPolicy() {
		return AnimationPolicy.RESET_ALL;
	}

	@Override
	public void stop() {
		super.stop();
		if (getEntity().removeAttribute("tempRandom") != null) {
			getEntity().removeAttribute("tempRandom");
		}
		getEntity().getUpdateFlags().sendAnimation(-1);
	}

	@Override
	public void execute() {
		Player player = (Player) getEntity();

		/*
		 * First check if the player can fish.
		 */
		if (!isAbleToFish()) {
			this.stop();
			return;
		}

		/*
		 * Start the event rolling.
		 */
		if (!isStarted()) {
			this.started = true;
			this.currentCycles = getCycleCount();
			player.getActionSender().sendMessage(getInteractionMessage());
			player.getUpdateFlags().sendAnimation(tool.getAnimationId(), 0);
		}

		/*
		 * Keep the animation playing if the cycle count is large. We have come
		 * to determine the fishing animation plays for a server time of
		 * approximately 20 cycles. To be safe, we replace after 15.
		 */
		if (currentCycles - difference++ >= 15) {
			difference = 0;
			player.getUpdateFlags().sendAnimation(tool.getAnimationId(), 0);
		}

		/*
		 * Reward the player on a randomized condition.
		 */
		int factor = Misc.random(player.getSkillLevel(Skill.FISHING));
		int fluke = Misc.random(fish.getRequiredLevel() == 1 ? 3 : fish.getRequiredLevel());
		if (player.isDebugging()) {
			player.getActionSender().sendMessage("Factor :: Fluke --->> " + factor + " :: " + fluke + ".");
		}
		if (factor > fluke) {
			player.setAttribute("tempRandom", Misc.generatedValue(tool.getOutcomes()));
			this.fish = (player.getSkillLevel(Skill.FISHING) >= Fishable.forId((int) player.getAttribute("tempRandom")).getRequiredLevel()) ? Fishable.forId((int) player.getAttribute("tempRandom")) : this.fish;

			if (player.getInventory().addItem(this.getRewards()[0])) {
				if (this.getConsumedItems() != null) {
					player.getInventory().removeItem(this.getConsumedItems()[0]);
				}
				player.getActionSender().sendMessage(getSuccessfulInteractionMessage());
				player.getSkill().addExperience(Skill.FISHING, fish.getExperience());
			}
		}

		/*
		 * Finish up.
		 */
		if (this.currentCycles > 0) {
			this.currentCycles--;
		} else {
			this.stop();
		}
	}
}
