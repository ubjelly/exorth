package com.exorth.rs2.content.skills.fishing;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Joshua Barry <Ares>
 * @author Dominick Jones <Jones>
 * 
 */
public enum Tools {

	SMALL_NET(303, 621, new int[]{ 317, 3150, 321, 5004, 7994 }),
	BIG_NET(305, 621, new int[]{ 353, 341, 363 }),
	CRAYFISH_CAGE(13431, 619, new int[]{ 13435 }),
	FISHING_ROD(307, 622, new int[]{ 327, 345, 349, 3379, 5001, 2148 }),
	FLYFISHING_ROD(309, 622, new int[]{ 335, 331 }),
	KARAMBWAN_POT(3157, -1, new int[]{ 3142 }),
	HARPOON(311, 618, new int[]{ 359, 371 }),
	LOBSTER_POT(301, 619, new int[]{ 377 });

	private int toolId;

	private int animation;

	private int[] outcomes;

	/**
	 * 
	 * @param toolId The item id of the tool we will be using to catch fish.
	 * @param outcomes An array of item id's of possible reward outcomes.
	 */
	private Tools(int toolId, int animation, int[] outcomes) {
		this.toolId = toolId;
		this.outcomes = outcomes;
		this.animation = animation;
	}

	public int getToolId() {
		return toolId;
	}

	public int getAnimationId() {
		return animation;
	}

	public int[] getOutcomes() {
		return outcomes;
	}

	public static Tools forId(int id) {
		return tools.get(id);
	}

	private static Map<Integer, Tools> tools = new HashMap<Integer, Tools>();

	static {
		for (Tools tool : Tools.values()) {
			tools.put(tool.getToolId(), tool);
		}
	}
}
