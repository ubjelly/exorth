package com.exorth.rs2.content.skills.fishing;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all data relevant to fishing tools.
 * 
 * @author Joshua Barry <Ares>
 * @author Dominick Jones <Jones>
 * 
 */
public enum Fishable {

	SHRIMP(317, 303, 1, 10, -1),
	CRAYFISH(13435, 13431, 1, 10, -1),
	KARAMBWANJI(3150, 303, 5, 5, -1),
	SARDINE(327, 307, 5, 20, 313),
	HERRING(345, 307, 10, 30, 313),
	ANCHOVIES(321, 303, 15, 40, -1),
	MACKEREL(353, 305, 16, 20, -1),
	TROUT(335, 309, 20, 50, 314),
	COD(341, 305, 23, 45, -1),
	PIKE(349, 307, 25, 60, 313),
	SLIMY_EEL(3379, 307, 28, 65, 313),
	SALMON(331, 309, 30, 70, 314),
	FROG_SPAWN(5004, 303, 33, 75, -1),
	TUNA(359, 311, 35, 80, -1),
	CAVE_EEL(5001, 307, 38, 80, 313),
	LOBSTER(377, 301, 40, 90, -1),
	BASS(363, 305, 46, 100, -1),
	SWORDFISH(371, 311, 50, 100, -1),
	LAVA_EEL(2148, 307, 53, 30, 313),
	MONKFISH(7944, 303, 62, 120, -1),
	KARAMBWAN(3142, 3157, 65, 100, -1),
	SHARK(383, 311, 76, 110, -1),
	SEA_TURTLE(395, -1, 79, 38, -1),
	MANTA_RAY(390, -1, 81, 46, -1);

	private int rawFishId;

	private int toolId;

	private int levelRequired;

	private int baitRequired;

	private double experienceGain;

	/**
	 * 
	 * @param rawFishId The id of the fish you will catch
	 * @param toolId The id of the tool needed to catch the fish
	 * @param levelRequired The level required to catch the fish
	 * @param experienceGain The experience gained from catching the fish.
	 */
	private Fishable(int rawFishId, int toolId, int levelRequired, double experienceGain, int baitRequired) {
		this.rawFishId = rawFishId;
		this.toolId = toolId;
		this.levelRequired = levelRequired;
		this.experienceGain = experienceGain;
		this.baitRequired = baitRequired;
	}

	public int getRawFishId() {
		return rawFishId;
	}

	public int getToolId() {
		return toolId;
	}

	public int getRequiredLevel() {
		return levelRequired;
	}

	public double getExperience() {
		return experienceGain;
	}

	public int getBaitRequired() {
		return baitRequired;
	}

	private static Map<Integer, Fishable> fish = new HashMap<Integer, Fishable>();

	static {
		for (Fishable fishes : Fishable.values()) {
			fish.put((int) fishes.getRawFishId(), fishes);
		}
	}

	public static Fishable forId(int rawFishId) {
		return fish.get(rawFishId);
	}
}
