package com.exorth.rs2.content.skills.agility;

import com.exorth.rs2.Constants;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.clip.PathFinder;

/**
 * The actions taken to complete obstacles.
 * @author Stephen
 */
public class TackleObstacle {

	/**
	 * The agility ticket item.
	 */
	private final static Item AGILITY_TICKET = new Item(2996);
	
	/**
	 * The current cycle amount.
	 */
	private static int cycleAmount = 0;

	/**
	 * Prepares the player's look for the obstacle.
	 * @param player The player who is interacting with the obstacle.
	 * @param obstacle The obstacle interacted with.
	 */
	private static void setAppearance(Player player, Obstacle obstacle) {
		player.setAttribute("tempStandAnim", obstacle.getAnim());
		player.setAttribute("tempWalkAnim", obstacle.getAnim());
		player.setAttribute("tempRun", player.getAttribute("isRunning"));
		player.removeAttribute("isRunning");
		player.setAppearanceUpdateRequired(true);
		player.getMovementHandler().setRunPath(false);
	}

	/**
	 * Resets the player's look to the normal look.
	 * @param player The player to reset the appearance.
	 */
	private static void resetAppearance(Player player) {
		player.setAttribute("isRunning", player.getAttribute("tempRun"));
		player.removeAttribute("tempRun");
		player.removeAttribute("tempStandAnim");
		player.removeAttribute("tempWalkAnim");
		player.setAppearanceUpdateRequired(true);
	}

	
	/**
	 * The obstacle pipe at the wildy agility course.
	 * @param player The player who is interacting with the obstacle.
	 * @param obstacle The obstacle interacted with.
	 */
	public static void handlePipe(final Player player, final Obstacle obstacle) {
		World.submit(new Task(1) {
			@Override
			protected void execute() {
				if (!player.getPosition().equals(obstacle.getStartPos())) {
					setCycleAmount(getCycleAmount() + 1);
					PathFinder.getSingleton().moveTo(player, obstacle.getStartPos().getX(), obstacle.getStartPos().getY());
					if (getCycleAmount() == 10){
						setCycleAmount(0);
						this.stop();
					}
					return;
				}
				setCycleAmount(0);
				setAppearance(player, obstacle);
				player.getMovementHandler().walk(obstacle.getEndPos());
				player.getActionSender().sendMessage(obstacle.getMessage());
				World.submit(new Task(obstacle.getTick()) {
					@Override
					protected void execute() {
						resetAppearance(player);
						player.getSkill().addExperience(Skill.AGILITY, obstacle.getXp());
						player.setCourseState(1);
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	/**
	 * The rope swing at the wildy agility course.
	 * @param player The player who is interacting with the obstacle.
	 * @param obstacle The obstacle interacted with.
	 */
	public static void handleRopeSwing(final Player player, final Obstacle obstacle) {
		World.submit(new Task(1) {
			@Override
			protected void execute() {
				if (!player.getPosition().equals(obstacle.getStartPos())) {
					setCycleAmount(getCycleAmount() + 1);
					PathFinder.getSingleton().moveTo(player, obstacle.getStartPos().getX(), obstacle.getStartPos().getY());
					if (getCycleAmount() == 10){
						setCycleAmount(0);
						this.stop();
					}
					return;
				}
				setCycleAmount(0);
				setAppearance(player, obstacle);
				player.getMovementHandler().walk(obstacle.getEndPos());
				player.getActionSender().sendMessage(obstacle.getMessage());
				World.submit(new Task(obstacle.getTick()) {
					@Override
					protected void execute() {
						/**
						 * Handle failing chance
						 */
						double chance = 70 - player.getSkill().getLevel()[16];
						double roll = Math.random() * 100;
						boolean willFail = false;
						if (roll < chance) {
							willFail = true;
						}
						if (willFail == true) {
							player.getActionSender().sendMessage("You accidentally lose concentration and fall!");
							player.teleport(new Position(3003, 10355));
							player.hit(10, 1, false);
							resetAppearance(player);
						} else {
							player.getMovementHandler().walk(obstacle.getEndPos());
							World.submit(new Task(2) {
								protected void execute() {
									resetAppearance(player);
									player.getSkill().addExperience(Skill.AGILITY, obstacle.getXp());
									if (player.getCourseState() == 1) {
										player.setCourseState(2);
									}
									this.stop();
								}
							});
						}
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	/**
	 * The stepping stones at the wildy agility course.
	 * @param player The player who is interacting with the obstacle.
	 * @param obstacle The obstacle interacted with.
	 */
	public static void handleSteppingStones(final Player player, final Obstacle obstacle) {
		World.submit(new Task(1) {
			@Override
			protected void execute() {
				if (!player.getPosition().equals(obstacle.getStartPos())) {
					setCycleAmount(getCycleAmount() + 1);
					PathFinder.getSingleton().moveTo(player, obstacle.getStartPos().getX(), obstacle.getStartPos().getY());
					if (getCycleAmount() == 10){
						setCycleAmount(0);
						this.stop();
					}
					return;
				}
				setCycleAmount(0);
				player.getActionSender().sendMessage("Initiating action.");
				setAppearance(player, obstacle);
				//TODO: Make player jump one rock at a time - fuck that lol
				player.getMovementHandler().walk(obstacle.getEndPos());
				player.getActionSender().sendMessage(obstacle.getMessage());
				World.submit(new Task(obstacle.getTick()) {
					@Override
					protected void execute() {
						resetAppearance(player);
						player.getSkill().addExperience(Skill.AGILITY, obstacle.getXp());
						if (player.getCourseState() == 2)
							player.setCourseState(3);
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	/**
	 * The log balance at the wildy agility course.
	 * @param player The player who is interacting with the obstacle.
	 * @param obstacle The obstacle interacted with.
	 */
	public static void handleLogBalance(final Player player, final Obstacle obstacle) {
		World.submit(new Task(1) {
			@Override
			protected void execute() {
				if (!player.getPosition().equals(obstacle.getStartPos())) {
					setCycleAmount(getCycleAmount() + 1);
					PathFinder.getSingleton().moveTo(player, obstacle.getStartPos().getX(), obstacle.getStartPos().getY());
					if (getCycleAmount() == 10){
						setCycleAmount(0);
						this.stop();
					}
					return;
				}
				setCycleAmount(0);
				player.getMovementHandler().walk(obstacle.getEndPos());
				setAppearance(player, obstacle);
				player.getActionSender().sendMessage(obstacle.getMessage());
				World.submit(new Task(obstacle.getTick()) {
					@Override
					protected void execute() {
						/**
						 * Handle failing chance
						 */
						double chance = 70 - player.getSkill().getLevel()[16];
						double roll = Math.random() * 100;
						boolean willFail = false;
						if (roll < chance) {
							willFail = true;
						}
						if (willFail == true) {
							player.getActionSender().sendMessage("You fall off the log!");
							player.teleport(new Position(2999, 10345));
							player.hit(10, 1, false);
							resetAppearance(player);
						} else {
							World.submit(new Task(4) {
								protected void execute() {
									resetAppearance(player);
									player.getSkill().addExperience(Skill.AGILITY, obstacle.getXp());
									if (player.getCourseState() == 3)
										player.setCourseState(4);
									this.stop();
								}
							});
						}
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	/**
	 * The rock climb at the wildy agility course.
	 * @param player The player who is interacting with the obstacle.
	 * @param obstacle The obstacle interacted with.
	 */
	public static void handleRockClimb(final Player player, final Obstacle obstacle) {
		World.submit(new Task(1) {
			@Override
			protected void execute() {
				if (!player.getPosition().equals(obstacle.getStartPos())) {
					setCycleAmount(getCycleAmount() + 1);
					PathFinder.getSingleton().moveTo(player, obstacle.getStartPos().getX(), obstacle.getStartPos().getY());
					if (getCycleAmount() == 10){
						setCycleAmount(0);
						this.stop();
					}
					return;
				}
				setCycleAmount(0);
				player.getMovementHandler().walk(obstacle.getEndPos());
				setAppearance(player, obstacle);
				player.getActionSender().sendMessage(obstacle.getMessage());
				World.submit(new Task(obstacle.getTick()) {
					@Override
					protected void execute() {
						resetAppearance(player);
						if (player.getCourseState() == 4) {
							player.setCourseState(0);
							player.getSkill().addExperience(Skill.AGILITY, obstacle.getXp());
							if (player.getInventory().hasRoomFor(AGILITY_TICKET)) {
								player.getInventory().addItem(AGILITY_TICKET);
							} else {
								player.getActionSender().sendMessage("You have no room in your inventory for your reward!");
								ItemManager.getInstance().createGroundItem(player, player.getUsername(), AGILITY_TICKET, player.getPosition(), Constants.GROUND_START_TIME_DROP);
							}
							player.getActionSender().sendMessage("You have completed the entire course!");
						}
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	/**
	 * Gets the cycle amount.
	 * @return The cycle amount.
	 */
	public static int getCycleAmount() {
		return cycleAmount;
	}

	/**
	 * Sets the cycle amount.
	 * @param cycleAmount The amount to set it to.
	 */
	public static void setCycleAmount(int cycleAmount) {
		TackleObstacle.cycleAmount = cycleAmount;
	}
}
