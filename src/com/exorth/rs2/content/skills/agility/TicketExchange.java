package com.exorth.rs2.content.skills.agility;

import java.util.HashMap;

import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;

/**
 * Handles Brimhaven agility ticket exchange, cuz.
 * @author Stephen
 */

public class TicketExchange {

	/**
	 * The id of a ticket.
	 */
	private final static int TICKET_ID = 2996;

	/**
	 * The ticket multiplyer.
	 */
	private static int ticketMultiplyer = 700;

	public enum Ticket {

		ONE_TICKET(8387, 1, -1),

		TEN_TICKETS(8389, 10, -1),

		TWENTY_FIVE_TICKETS(8390, 25, -1),

		ONE_HUNDRED_TICKETS(8391, 100, -1),

		ONE_THOUSAND_TICKETS(8392, 1000, -1),

		TOADFLAX(8382, 3, 2998),

		SNAPDRAGON(8393, 10, 3000),

		PIRATE_HOOK(8381, 800, 2997);

		/**
		 * The action button id.
		 */
		private int actionButton;

		/**
		 * The amount of tickets required.
		 */
		private int amount;

		/**
		 * The item given as a reward.
		 */
		private int itemReward;

		Ticket(int actionButton, int amount, int itemReward) {
			this.actionButton = actionButton;
			this.amount = amount;
			this.itemReward = itemReward;
		}

		/**
		 * A hashmap of all the ticket options.
		 */
		private static HashMap<Integer, Ticket> tickets = new HashMap<Integer, Ticket>();

		/**
		 * Populate the tickets hashmap.
		 */
		static {
			for (Ticket ticket : Ticket.values()) {
				tickets.put(ticket.actionButton, ticket);
			}
		}

		public static Ticket forID(int actionButton) {
			return tickets.get(actionButton);
		}

		public int getActionButton() {
			return actionButton;
		}

		public int getAmount() {
			return amount;
		}

		public int getItemReward() {
			return itemReward;
		}
	}

	public static void exchangeTickets(Player player, Ticket ticket) {
		if (player.getInventory().getItemContainer().contains(TICKET_ID, ticket.amount)) {
			if (ticket.itemReward == -1) { //XP rewards
				player.getInventory().removeItem(new Item(TICKET_ID, ticket.amount));
				player.getSkill().addExperience(Skill.AGILITY, ticket.amount * ticketMultiplyer);
			} else {
				Item reward = new Item(ticket.getItemReward());
				if (player.getInventory().hasRoomFor(reward)) {
					player.getInventory().removeItem(new Item(TICKET_ID, ticket.amount));
					player.getInventory().addItem(reward);
				} else {
					player.getActionSender().sendMessage("There's no room in your inventory for your reward!");
				}
			}
		} else {
			player.getActionSender().sendMessage("You don't have enough tickets for this reward!");
		}
	}
}
