package com.exorth.rs2.content.skills.agility;

import java.util.HashMap;
import com.exorth.rs2.model.Position;

/**
 * All the data needed for agility obstacles.
 * @author Stephen
 */
public enum Obstacle {

	WILDERNESS_PIPE(2288, 844, new Position(3004, 3937), new Position(3004, 3950), 5, 150, "You squeeze through the pipe..."),

	WILDERNESS_ROPE_SWING(2283, 1501, new Position(3005, 3953), new Position(3005, 3958), 3, 150, "You don't need to use a rope with your agility skills!"),

	WILDERNESS_STEPPING_STONES(2311, 769, new Position(3002, 3960), new Position(2996, 3960), 6, 150, "You carefully cross the stepping stones."),

	WILDERNESS_LOG_BALANCE(2297, 762, new Position(3002, 3945), new Position(2994, 3945), 4, 150, "You carefully cross the log."),

	WILDERNESS_ROCKS(2328, 737, new Position(2994, 3937), new Position(2994, 3932), 4, 950, "You scurry up the rocks.");

	/**
	 * The id of the object.
	 */
	private int id;

	/**
	 * The id of the animation.
	 */
	private int anim;

	/**
	 * The position the player is starting at.
	 */
	private Position startPos;

	/**
	 * The position the player will end up at.
	 */
	private Position endPos;

	/**
	 * The amount of ticks for the event.
	 */
	private int tick;

	/**
	 * The experience gained.
	 */
	private int xp;

	/**
	 * The message sent on initiation of the obstacle.
	 */
	private String message;

	Obstacle(int id, int anim, Position startPos, Position endPos, int tick, int xp, String message) {
		this.id = id;
		this.anim = anim;
		this.startPos = startPos;
		this.endPos = endPos;
		this.tick = tick;
		this.xp = xp;
		this.message = message;
	}

	/**
	* Creates a hashmap of 'obstacles' based on their object ID.
	*/
	private static HashMap<Integer, Obstacle> obstacles = new HashMap<Integer, Obstacle>();

	static {
		for (Obstacle obstacle : Obstacle.values()) {
			obstacles.put(obstacle.getId(), obstacle);
		}
	}

	/**
 	* @param id The ID of the object we are attempting to tackle.
	* @return The obstacle with the corresponding object ID.
	*/
	public static Obstacle forId(int id) {
		return obstacles.get(id);
	}

	/**
	 * Gets the object's id.
	 * @return The object's id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the animation to be played.
	 * @return The animation.
	 */
	public int getAnim() {
		return anim;
	}

	/**
	 * Gets the position to start at.
	 * @return The position.
	 */
	public Position getStartPos() {
		return startPos;
	}

	/**
	 * Gets the position to end up at.
	 * @return The position.
	 */
	public Position getEndPos() {
		return endPos;
	}

	/**
	 * Gets the amount of ticks.
	 * @return The tick.
	 */
	public int getTick() {
		return tick;
	}

	/**
	 * Gets the amount of xp rewarded.
	 * @return The tick.
	 */
	public int getXp() {
		return xp;
	}

	/**
	 * Gets the message.
	 * @return The message.
	 */
	public String getMessage() {
		return message;
	}
}