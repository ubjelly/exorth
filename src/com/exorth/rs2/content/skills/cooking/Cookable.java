package com.exorth.rs2.content.skills.cooking;

import java.util.HashMap;
import java.util.Map;

import com.exorth.rs2.model.items.Item;

/**
 * All the items that are able to be cooked.
 * @author Stephen
 */
public enum Cookable {

	//-1 Represents items that can never be successfully cooked 100% of the time.
	SHRIMP(new Item(317, 1), new Item(315, 1), new Item(323, 1), 30, 1, 35),

	LOBSTER(new Item(377, 1), new Item(379, 1), new Item(381, 1), 120, 40, 74),

	SWORDFISH(new Item(371, 1), new Item(373, 1), new Item(375, 1), 140, 55, 86),

	SHARK(new Item(383, 1), new Item(385, 1), new Item(387, 1), 210, 80, -1),

	MANTA_RAY(new Item(389, 1), new Item(391, 1), new Item(393, 1), 216, 91, -1);

	/**
	 * The item being cooked.
	 */
	private Item raw;

	/**
	 * The result, a 'cooked' product.
	 */
	private Item cooked;
	
	/**
	 * The result, a 'burnt' product.
	 */
	private Item burnt;

	/**
	 * The amount of experience gained from the 'cooking' task.
	 */
	private double experience;

	/**
	 * The level required to perform the 'cooking' task.
	 */
	private int requiredLevel;
	
	/**
	 * The level required to yield a 100% success rate.
	 */
	private int masteredLevel;

	Cookable(Item raw, Item cooked, Item burnt, int experience, int requiredLevel, int masteredLevel) {
		this.raw = raw;
		this.cooked = cooked;
		this.burnt = burnt;
		this.experience = experience;
		this.requiredLevel = requiredLevel;
		this.masteredLevel = masteredLevel;
	}
	
	/**
	 * A mapping of the cookable items, called by the raw item.
	 */
	private static Map<Integer, Cookable> cookableItems = new HashMap<Integer, Cookable>();

	static {
		for (Cookable cookable : Cookable.values()) {
			cookableItems.put(cookable.getRawItem().getId(), cookable);
		}
	}
	
	public static Cookable forId(int id) {
		return cookableItems.get(id);
	}
	
	public Item getRawItem() {
		return raw;
	}

	public Item getCookedItem() {
		return cooked;
	}
	
	public Item getBurntItem() {
		return burnt;
	}

	public int getRequiredLevel() {
		return requiredLevel;
	}
	
	public int getMasteredLevel() {
		return masteredLevel;
	}

	public double getExperience() {
		return experience;
	}
}
