package com.exorth.rs2.content.skills.cooking;

import java.util.Random;

import com.exorth.rs2.action.impl.ProductionAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Player;

/**
 * The action of cooking something.
 * @author Stephen
 * @author Mko
 */
public class CookingAction extends ProductionAction {

	/**
	 * The player doing the action.
	 */
	Player player;

	/**
	 * The 'product' being produced.
	 */
	Cookable cookable;

	/**
	 * The 'product' being produced.
	 */
	static String itemName;

	/**
	 * The amount to produce.
	 */
	int productionCount;

	/**
	 * Determines if the 'product' will be burnt.
	 */
	boolean willBurn;

	/**
	 * Construct a 'CookingAction'.
	 * @param entity The player doing the action.
	 */
	public CookingAction(Player player, Cookable cookable, int productionCount) {
		super(player);
		this.player = player;
		this.cookable = cookable;
		this.productionCount = productionCount;
		itemName = cookable.getCookedItem().getName().toLowerCase();
	}

	/**
	 * Is an entity able to produce the 'product'.
	 */
	@Override
	public boolean canProduce() {
		if (getEntity().isPlayer()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * If a player doesn't have the required resources for the 'product'.
	 */
	@Override
	public String getInsufficientResourcesMessage() {
		return "You do not have enough " + itemName + "s to continue cooking.";
	}

	/**
	 * The amount of ticks the action requires.
	 */
	@Override
	public int getCycleCount() {
		return 5;
	}

	/**
	 * The id of the graphic. In this case -1 because there is no graphic.
	 */
	@Override
	public int getGraphic() {
		return -1;
	}

	/**
	 * The animation of the action.
	 */
	@Override
	public int getAnimation() {
		return 896;
	}

	/**
	 * The experience rewarded after the action is completed.
	 */
	@Override
	public double getExperience() {
		return cookable.getExperience();
	}

	/**
	 * The items required for the action.
	 */
	@Override
	public Item[] getConsumedItems() {
		return new Item[]{ new Item(cookable.getRawItem().getId()) };
	}

	/**
	 * The message sent when a player's level is too low.
	 */
	@Override
	public String getInsufficentLevelMessage() {
		return "You need to have a Cooking level of " + getRequiredLevel() + " to cook " + itemName + ".";
	}

	/**
	 * How many 'products' there will be.
	 */
	@Override
	public int getProductionCount() {
		return productionCount;
	}

	/**
	 * The level required to produce the 'product'.
	 */
	@Override
	public int getRequiredLevel() {
		return cookable.getRequiredLevel();
	}

	/**
	 * The item received after the action.
	 */
	@Override
	public Item[] getRewards() {
		if (willBurn() == true) {
			willBurn = true;
			return new Item[]{ new Item(cookable.getBurntItem().getId()) };
		} else {
			willBurn = false;
			return new Item[]{ new Item(cookable.getCookedItem().getId()) };
		}
	}

	/**
	 * The skill the action is associated with.
	 */
	@Override
	public int getSkill() {
		return Skill.COOKING;
	}

	/**
	 * The message sent before producing a product.
	 */
	@Override
	public String getPreProductionMessage() {
		return null;
	}

	/**
	 * The message sent on a successful production.
	 */
	@Override
	public String getSuccessfulProductionMessage() {
		if (willBurn == true) {
			return "You burn the " + itemName + ".";
		} else {
			return "You successfully cook the " + itemName + ".";
		}
	}

	/**
	 * Send the correct model on the cooking interface.
	 */
	public static void prepareInterface(Player player, int itemModel) {
		/*player.getActionSender().sendItemOnInterface(13716, 200, itemModel);
		Cookable item = Cookable.forId(itemModel);
		Item cookedItemName = item.getCookedItem();
		String finalItemName = cookedItemName.getName();
		player.getActionSender().sendString(finalItemName, 13717);
		player.getActionSender().sendChatInterface(1743);*/
		player.getActionSender().sendString("\\n \\n \\n \\n" + ItemManager.getInstance().getItemName(Cookable.forId(itemModel).getCookedItem().getId()), 13717);
		player.getActionSender().moveComponent(0, -5, 13716);
		player.getActionSender().sendItemOnInterface(13716, 175, Cookable.forId(itemModel).getCookedItem().getId());
		player.getActionSender().sendChatInterface(1743);
	}
	
	/**
	 * The formula for calculating whether or not the player will burn their 'product'.
	 * @return probability The chance the user has to successfully cook their item.
	 */
	 private double successRate() {
		//Calculate the cooking success probability.
		double probability = 52.0;
		double factorA = cookable.getMasteredLevel() - cookable.getRequiredLevel();
		double probabilityModifier = probability / factorA;
		double factorB = player.getSkillLevel(Skill.COOKING) - cookable.getRequiredLevel();
		probability -= factorB / probabilityModifier;

		//Boost by equipment
		/*
		 * Might need to be reworked...
		if (player.getEquipment().playerGloves().getId() == 775) {
			player.getActionSender().sendMessage("Probability Prior: " + probability + ".");
			probability += 5;
		}*/

		if (player.getSkillLevel(Skill.COOKING) >= cookable.getMasteredLevel() && cookable.getMasteredLevel() != -1) {
			probability = 100;
		}

		if (player.isDebugging()) {
			player.getActionSender().sendMessage("Success Probability: <col=CC0000>" + probability + "</col>.");
		}
		return probability;
	}

	/**
	 * Determines if the player burnt their 'product'.
	 */
	private boolean willBurn() {
		Random random = new Random();
		if (random.nextDouble() * 100 <= successRate()) {
			return false;
		} else {
			return true;
		}
	}
}
