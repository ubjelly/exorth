package com.exorth.rs2.content.skills.slayer;

import java.util.HashMap;

/**
 * All the possible tasks a player can receive.
 * @author Stephen
 */
public enum SlayerTask {

	//TODO - Fix xp rates.
	//TODO - Multiple NPC IDs for same NPC Name
	CRAWLING_HAND(1656, 1, 100, "Crawling Hand"),

	BANSHEE(1612, 20, 200, "Banshee"),

	PYREFIEND(1633, 30, 300, "Pyrefiend"),

	BASILISK(1616, 40, 400, "Basilisk"),

	JELLY(1637, 60, 500, "Jellie"),

	NECHRYAEL(1613, 65, 600, "Nechryael"),

	ABBERRANT_SPECTRE(1604, 70, 700, "Abberant Spectre"),

	ASYN_SHADE(1248, 75, 800, "Asyn Shade"),

	KHAZARD_WARLORD(477, 80, 900, "Khazard Warlord"),

	ABYSSAL_DEMON(1615, 85, 1000, "Abyssal Demon");

	/**
	 * The tasks for Mazchna.
	 */
	public static final SlayerTask[] MAZ_TASKS;

	/**
	 * The tasks for Vannaka.
	 */
	public static final SlayerTask[] VAN_TASKS;

	/**
	 * The id of the NPC.
	 */
	private int npcId;

	/**
	 * The level required to attack the NPC.
	 */
	private int levelReq;

	/**
	 * The slayer XP rewarded.
	 */
	private int xp;

	/**
	 * The name of the npc.
	 */
	private String npcName;

	SlayerTask(int npcId, int levelReq, int xp, String npcName) {
		this.npcId = npcId;
		this.levelReq = levelReq;
		this.xp = xp;
		this.npcName = npcName;
		
	}

	/**
	* Creates a hashmap of 'slayer tasks' based on their NPC ID.
	*/
	private static HashMap<Integer, SlayerTask> tasks = new HashMap<Integer, SlayerTask>();

	static {
		for (SlayerTask task : SlayerTask.values()) {
			tasks.put(task.getNpcId(), task);
		}
		MAZ_TASKS = new SlayerTask[] {CRAWLING_HAND, BANSHEE, PYREFIEND, BASILISK};
		VAN_TASKS = new SlayerTask[] {JELLY, NECHRYAEL, ABBERRANT_SPECTRE, ASYN_SHADE, KHAZARD_WARLORD, ABYSSAL_DEMON};
	}

	public static SlayerTask forId(int id) {
		return tasks.get(id);
	}

	public int getNpcId() {
		return npcId;
	}

	public int getLevelReq() {
		return levelReq;
	}

	public int getXp() {
		return xp;
	}

	public String getNpcName() {
		return npcName;
	}
}