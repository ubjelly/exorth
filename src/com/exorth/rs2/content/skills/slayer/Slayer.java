package com.exorth.rs2.content.skills.slayer;

import com.exorth.rs2.model.players.Player;

/**
 * Handles anything to do with a SlayerTask.
 * @author Stephen
 */
public class Slayer {

	private Player player;
	private SlayerTask task;
	private int npc;
	private int amount;

	public Slayer(Player player, int npc, int amount) {
		this.player = player;
		task = SlayerTask.forId(npc);
		this.npc = npc;
		this.amount = amount;
		assignTask();
	}

	private void assignTask() {
		if (player == null)
			return;
		
		player.setSlayerTask(npc);
		player.setKillsLeft(amount);
	}

	public String checkTask() {
		return "Your current assignment is: " + task.getNpcName() + "s; only " + player.getKillsLeft() + " more to go.";
	}

	public SlayerTask getTask() {
		return task;
	}
}
