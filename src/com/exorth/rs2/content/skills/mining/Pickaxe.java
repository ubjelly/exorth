package com.exorth.rs2.content.skills.mining;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains the data for the pickaxe.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum Pickaxe {

	BRONZE(1265, 1, 625),
	IRON(1267, 5, 626),
	STEEL(1269, 11, 627),
	MITHRIL(1273, 21, 629),
	ADAMANT(1271, 31, 628),
	RUNE(1275, 41, 624);

	/**
	 * The item id of this pick axe.
	 */
	private int id;

	/**
	 * The level required to use this pick axe.
	 */
	private int level;

	/**
	 * The 'id' value of the animation to cast.
	 */
	private int animation;

	/**
	 * A list of pick axes.
	 */
	private static List<Pickaxe> pickaxes = new ArrayList<Pickaxe>();

	/**
	 * Populates the pick axe map.
	 */
	static {
		for (Pickaxe pickaxe : Pickaxe.values()) {
			pickaxes.add(pickaxe);
		}
	}

	/**
	 * Gets the list of pick axes.
	 * 
	 * @return The list of pick axes.
	 */
	public static List<Pickaxe> getPickaxes() {
		return pickaxes;
	}

	private Pickaxe(int id, int level, int animation) {
		this.id = id;
		this.level = level;
		this.animation = animation;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the level
	 */
	public int getRequiredLevel() {
		return level;
	}

	/**
	 * @return the animation
	 */
	public int getAnimation() {
		return animation;
	}
}
