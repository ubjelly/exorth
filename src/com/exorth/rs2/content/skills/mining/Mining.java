package com.exorth.rs2.content.skills.mining;

import com.exorth.rs2.action.impl.HarvestingAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Language;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.clip.RSObject;

/**
 * Handles ore mining. A player hacks at the rock with their pickaxe in attempt
 * to collect a bundle of ore's from the rock. There is a chance to find random
 * gem's inside ores, as well, TODO: gem's can be mined in Shilo Village.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class Mining extends HarvestingAction {

	/**
	 * The rock we are trying to mine from.
	 */
	private RSObject rock = null;

	/**
	 * The ore we are trying to collect.
	 */
	private Ore ore = null;

	/**
	 * The pickaxe we should be carrying.
	 */
	private Pickaxe pick = null;

	/**
	 * Initialize our data.
	 * 
	 * @param entity The player who is Mining.
	 * @param object The rock that we are trying to Mine.
	 */
	public Mining(Entity entity, RSObject object) {
		super(entity);
		this.rock = object;
		this.ore = Ore.forId(object.getId());
	}

	@Override
	public int getCycleCount() {
		int level = ore.getRequiredLevel();
		if (this.getGameObjectMaxHealth() == -1) {
			level = 25;
		}
		int modifier = pick.getRequiredLevel();
		if (modifier == 1) {
			modifier = 5;
		}
		double cycleCount = 1;
		cycleCount = Math.ceil((level * 50 - getEntity().getSkillLevel(getSkill()) * 10) / modifier * 0.0625 * 3);
		if (cycleCount < 1) {
			cycleCount = 1;
		}
		return (int) cycleCount;
	}

	@Override
	public RSObject getGlobalObject() {
		return rock;
	}

	@Override
	public RSObject getReplacementObject() {
		return new RSObject(ore.getReplacementRock(), getGlobalObject().getX(), getGlobalObject().getY(), getEntity().getPosition().getZ(), getGlobalObject().getType(), getGlobalObject().getFace());
	}

	@Override
	public int getObjectRespawnTimer() {
		return ore.getRespawnTimer();
	}

	@Override
	public Item getReward() {
		switch (ore) {
			case SHILO_GEM_ROCK_OLD_REGULAR:
			case SHILO_GEM_ROCK_GREY_TALL:
			case SHILO_GEM_ROCK_GREY_FLAT:
			case SHILO_GEM_ROCK_GREY_REGULAR:
			case SHILO_GEM_ROCK_DARK_GREY_TALL:
			case SHILO_GEM_ROCK_DARK_GREY_FLAT:
			case SHILO_GEM_ROCK_DARK_GREY_REGULAR:
			case SHILO_GEM_ROCK_GREY_TALL_2:
			case SHILO_GEM_ROCK_GREY_FLAT_2:
			case SHILO_GEM_ROCK_GREY_REGULAR_2:
				getEntity().setAttribute("shiloGem", Misc.generatedValue(new int[]{ 1629, 1627, 1625, 1623, 1621, 1619, 1617 }));
				return new Item((int) (getEntity().getAttribute("shiloGem")));
			default:
				return new Item(ore.getOreId());
		}
	}

	@Override
	public int getSkill() {
		return Skill.MINING;
	}

	@Override
	public int getRequiredLevel() {
		return ore.getRequiredLevel();
	}

	@Override
	public double getExperience() {
		return ore.getExperience();
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You need a " + Skill.SKILL_NAME[getSkill()] + " level of " + getRequiredLevel() + " to mine this ore.";
	}

	@Override
	public String getHarvestStartedMessage() {
		return "You swing your pick at the rock...";
	}

	@Override
	public String getSuccessfulHarvestMessage() {
		switch (ore) {
			case SHILO_GEM_ROCK_OLD_REGULAR:
			case SHILO_GEM_ROCK_GREY_TALL:
			case SHILO_GEM_ROCK_GREY_FLAT:
			case SHILO_GEM_ROCK_GREY_REGULAR:
			case SHILO_GEM_ROCK_DARK_GREY_TALL:
			case SHILO_GEM_ROCK_DARK_GREY_FLAT:
			case SHILO_GEM_ROCK_DARK_GREY_REGULAR:
			case SHILO_GEM_ROCK_GREY_TALL_2:
			case SHILO_GEM_ROCK_GREY_FLAT_2:
			case SHILO_GEM_ROCK_GREY_REGULAR_2:
				String item = ItemManager.getInstance().getItemName((int) getEntity().getAttribute("shiloGem")).toLowerCase().replace("uncut ", "");
				return "You just mined " + Misc.getArticle(item).concat(" ") + Misc.capitalizeFirst(item) + "!";
			default:
				return "You manage to mine some " + getReward().getDefinition().getName().toLowerCase() + ".";
		}
	}

	@Override
	public String getInventoryFullMessage() {
		return Language.NO_SPACE;
	}

	@Override
	public int getAnimationId() {
		return pick.getAnimation();
	}

	@Override
	public boolean canHarvest() {
		Player player = (Player) getEntity();
		if (getGlobalObject() == null) {
			player.getActionSender().sendMessage("Oops, this ore needs a minor configuration!");
			player.getActionSender().sendMessage("Please type ::pos and file a bug report online with your position.");
			return false;
		}

		for (Pickaxe pickaxe : Pickaxe.values()) {
			if (player.getInventory().getItemContainer().contains(pickaxe.getId()) || player.getEquipment().getItemContainer().contains(pickaxe.getId()) && player.getSkillLevel(getSkill()) > pickaxe.getRequiredLevel()) {
				this.pick = pickaxe;
				break;
			}
		}

		if (pick == null) {
			player.getActionSender().sendMessage("You do not have a pickaxe to use.");
			return false;
		}

		if (!player.getInventory().hasRoomFor(getReward())) {
			player.getActionSender().sendMessage(getInventoryFullMessage());
			return false;
		}
		return true;
	}

	@Override
	public int getGameObjectMaxHealth() {
		return ore.getOreCount();
	}

	@Override
	public int getRespawnRate() {
		return 3;
	}
}
