package com.exorth.rs2.content.skills.mining;

import java.util.HashMap;
import java.util.Map;

import com.exorth.rs2.util.Misc;

public enum Ore {

	ESS(2491, -1, 7936, 1, 5, 1000, -1),
	CLAY_OLD_TALL(2108, 450, 434, 1, 5, 2, 1),
	CLAY_OLD_REGULAR(2109, 451, 434, 1, 5, 2, 1),
	CLAY_RUST_TALL(9711, 9723, 434, 1, 5, 2, 1),
	CLAY_RUST_FLAT(9712, 9724, 434, 1, 5, 2, 1),
	CLAY_RUST_REGULAR(9713, 9725, 434, 1, 5, 2, 1),
	CLAY_DARKBROWN_TALL(11948, 11555, 434, 1, 5, 2, 1),
	CLAY_DARKBROWN_FLAT(11949, 11556, 434, 1, 5, 2, 1),
	CLAY_DARKBROWN_REGULAR(11950, 11557, 434, 1, 5, 2, 1),
	COPPER_OLD_TALL(2090, 450, 436, 1, 17.5, 6, 1),
	COPPER_OLD_REGULAR(2091, 451, 436, 1, 17.5, 6, 1),
	COPPER_RUST_TALL(9708, 9723, 436, 1, 17.5, 6, 1),
	COPPER_RUST_FLAT(9709, 9724, 436, 1, 17.5, 6, 1),
	COPPER_RUST_REGULAR(9710, 9725, 436, 1, 17.5, 6, 1),
	COPPER_LIGHTBROWN_TALL(11936, 11552, 436, 1, 17.5, 6, 1),
	COPPER_LIGHTBROWN_FLAT(11937, 11553, 436, 1, 17.5, 6, 1),
	COPPER_LIGHTBROWN_REGULAR(11938, 11554, 436, 1, 17.5, 6, 1),
	COPPER_DARKBROWN_TALL(11960, 11555, 436, 1, 17.5, 6, 1),
	COPPER_DARKBROWN_FLAT(11961, 11556, 436, 1, 17.5, 6, 1),
	COPPER_DARKBROWN_REGULAR(11962, 11557, 436, 1, 17.5, 6, 1),
	COPPER_LIGHTGREY_TALL(14906, 14915, 436, 1, 17.5, 6, 1),
	COPPER_LIGHTGREY_FLAT(14907, 14916, 436, 1, 17.5, 6, 1),
	COPPER_GREY_TALL(21284, 21296, 436, 1, 17.5, 6, 1),
	COPPER_GREY_FLAT(21285, 21297, 436, 1, 17.5, 6, 1),
	COPPER_GREY_REGULAR(21286, 21298, 436, 1, 17.5, 6, 1),
	TIN_OLD_TALL(2094, 450, 438, 1, 17.5, 6, 1),
	TIN_OLD_REGULAR(2095, 451, 438, 1, 17.5, 6, 1),
	TIN_GREENDARKGREY_TALL(6945, 6947, 438, 1, 17.5, 6, 1),
	TIN_GREENDARKGREY_FLAT(6946, 6948, 438, 1, 17.5, 6, 1),
	TIN_RUST_TALL(9714, 9723, 438, 1, 17.5, 6, 1),
	TIN_RUST_FLAT(9715, 9724, 438, 1, 17.5, 6, 1),
	TIN_RUST_REGULAR(9716, 9725, 438, 1, 17.5, 6, 1),
	TIN_LIGHTBROWN_TALL(11933, 11552, 438, 1, 17.5, 6, 1),
	TIN_LIGHTBROWN_FLAT(11934, 11553, 438, 1, 17.5, 6, 1),
	TIN_LIGHTBROWN_REGULAR(11935, 11554, 438, 1, 17.5, 6, 1),
	TIN_DARKBROWN_TALL(11957, 11555, 438, 1, 17.5, 6, 1),
	TIN_DARKBROWN_FLAT(11958, 11556, 438, 1, 17.5, 6, 1),
	TIN_DARKBROWN_REGULAR(11959, 11557, 438, 1, 17.5, 6, 1),
	TIN_LIGHTGREY_TALL(14902, 14915, 438, 1, 17.5, 6, 1),
	TIN_LIGHTGREY_FLAT(14903, 14916, 438, 1, 17.5, 6, 1),
	TIN_GREY_TALL(21293, 21296, 438, 1, 17.5, 6, 1),
	TIN_GREY_FLAT(21294, 21297, 438, 1, 17.5, 6, 1),
	TIN_GREY_REGULAR(21295, 21298, 438, 1, 17.5, 6, 1),
	BLURITE_TALL(10583, 10585, 668, 10, 25, 10, 1),
	BLURITE_FLAT(10584, 10586, 668, 10, 25, 10, 1),
	BLURITE_REGULAR(2110, 10587, 668, 10, 25, 10, 1),
	IRON_OLD_TALL(2092, 450, 440, 15, 35, 15, 1),
	IRON_OLD_REGULAR(2093, 451, 440, 15, 35, 15, 1),
	IRON_GREENDARKGREY_TALL(6943, 6947, 440, 15, 35, 15, 1),
	IRON_GREENDARKGREY_FLAT(6944, 6948, 440, 15, 35, 15, 1),
	IRON_RUST_TALL(9717, 9723, 440, 15, 35, 15, 1),
	IRON_RUST_FLAT(9718, 9724, 440, 15, 35, 15, 1),
	IRON_RUST_REGULAR(9719, 9725, 440, 15, 35, 15, 1),
	IRON_DARKBROWN_TALL(11954, 11555, 440, 15, 35, 15, 1),
	IRON_DARKBROWN_FLAT(11955, 11556, 440, 15, 35, 15, 1),
	IRON_DARKBROWN_REGULAR(11956, 11557, 440, 15, 35, 15, 1),
	IRON_DARKGREY_TALL(14856, 14832, 440, 15, 35, 15, 1),
	IRON_DARKGREY_FLAT(14857, 14833, 440, 15, 35, 15, 1),
	IRON_DARKGREY_REGULAR(14858, 14834, 440, 15, 35, 15, 1),
	IRON_LIGHTGREY_TALL(14900, 14915, 440, 15, 35, 15, 1),
	IRON_LIGHTGREY_FLAT(14901, 14916, 440, 15, 35, 15, 1),
	IRON_LIGHTGREY_TALL_2(14913, 14915, 440, 15, 35, 15, 1),
	IRON_LIGHTGREY_FLAT_2(14914, 14916, 440, 15, 35, 15, 1),
	IRON_GREY_TALL(21281, 21296, 440, 15, 35, 15, 1),
	IRON_GREY_FLAT(21282, 21297, 440, 15, 35, 15, 1),
	IRON_GREY_REGULAR(21283, 21298, 440, 15, 35, 15, 1),
	SILVER_OLD_TALL(2100, 450, 442, 20, 40, 100, 1),
	SILVER_OLD_REGULAR(2101, 451, 442, 20, 40, 100, 1),
	SILVER_DARK_DARK_GREY_TALL(15579, 15582, 442, 20, 40, 100, 1),
	SILVER_DARK_DARK_GREY_FLAT(15580, 15583, 442, 20, 40, 100, 1),
	SILVER_DARK_DARK_GREY_REGULAR(15581, 15584, 442, 20, 40, 100, 1),
	SILVER_LIGHT_LIGHT_GREY_TALL(16998, 17007, 442, 20, 40, 100, 1),
	SILVER_LIGHT_LIGHT_GREY_FLAT(16999, 17008, 442, 20, 40, 100, 1),
	SILVER_LIGHT_LIGHT_GREY_REGULAR(17000, 17009, 442, 20, 40, 100, 1),
	GOLD_OLD_TALL(2098, 450, 444, 40, 65, 100, 1),
	GOLD_OLD_REGULAR(2099, 451, 444, 40, 65, 100, 1),
	GOLD_GREY_TALL(2609, 21296, 444, 40, 65, 100, 1),
	GOLD_GREY_FLAT(2610, 21297, 444, 40, 65, 100, 1),
	GOLD_GREY_REGULAR(2611, 21298, 444, 40, 65, 100, 1),
	GOLD_RUST_TALL(9720, 9723, 444, 40, 65, 100, 1),
	GOLD_RUST_FLAT(9721, 9724, 444, 40, 65, 100, 1),
	GOLD_RUST_REGULAR(9722, 9725, 444, 40, 65, 100, 1),
	GOLD_DARKBROWN_TALL(11951, 11555, 444, 40, 65, 100, 1),
	GOLD_DARKBROWN_FLAT(11952, 11556, 444, 40, 65, 100, 1),
	GOLD_DARKBROWN_REGULAR(11953, 11557, 444, 40, 65, 100, 1),
	GOLD_LIGHTGREY_TALL(14904, 14915, 444, 40, 65, 100, 1),
	GOLD_LIGHTGREY_FLAT(14905, 14916, 444, 40, 65, 100, 1),
	GOLD_DARK_DARK_GREY_TALL(15576, 15582, 444, 40, 65, 100, 1),
	GOLD_DARK_DARK_GREY_FLAT(15577, 15583, 444, 40, 65, 100, 1),
	GOLD_DARK_DARK_GREY_REGULAR(15578, 15584, 444, 40, 65, 100, 1),
	GOLD_LIGHT_LIGHT_GREY_TALL(17001, 17007, 444, 40, 65, 100, 1),
	GOLD_LIGHT_LIGHT_GREY_FLAT(17002, 17008, 444, 40, 65, 100, 1),
	GOLD_LIGHT_LIGHT_GREY_REGULAR(17003, 17009, 444, 40, 65, 100, 1),
	GOLD_GREY_TALL_2(21290, 21296, 444, 40, 65, 100, 1),
	GOLD_GREY_FLAT_2(21291, 21297, 444, 40, 65, 100, 1),
	GOLD_GREY_REGULAR_2(21292, 21298, 444, 40, 65, 100, 1),
	COAL_OLD_TALL(2096, 450, 453, 30, 50, 50, 3),
	COAL_OLD_REGULAR(2097, 451, 453, 30, 50, 50, 3),
	COAL_LIGHTBROWN_TALL(11930, 11552, 453, 30, 50, 50, 3),
	COAL_LIGHTBROWN_FLAT(11931, 11553, 453, 30, 50, 50, 3),
	COAL_LIGHTBROWN_REGULAR(11932, 11554, 453, 30, 50, 50, 3),
	COAL_DARKBROWN_TALL(11963, 11555, 453, 30, 50, 50, 3),
	COAL_DARKBROWN_FLAT(11964, 11556, 453, 30, 50, 50, 3),
	COAL_DARKBROWN_REGULAR(11965, 11557, 453, 30, 50, 50, 3),
	COAL_DARKGREY_TALL(14850, 14832, 453, 30, 50, 50, 3),
	COAL_DARKGREY_FLAT(14851, 14833, 453, 30, 50, 50, 3),
	COAL_DARKGREY_REGULAR(14852, 14834, 453, 30, 50, 50, 3),
	COAL_GREENGREY_TALL(15246, 15249, 453, 30, 50, 50, 3),
	COAL_GREENGREY_FLAT(15247, 15250, 453, 30, 50, 50, 3),
	COAL_GREENGREY_REGULAR(15248, 15251, 453, 30, 50, 50, 3),
	COAL_GREY_TALL(21287, 21296, 453, 30, 50, 50, 3),
	COAL_GREY_FLAT(21288, 21297, 453, 30, 50, 50, 3),
	COAL_GREY_REGULAR(21289, 21298, 453, 30, 50, 50, 3),
	MITHRIL_OLD_TALL(2102, 450, 447, 55, 80, 200, 4),
	MITHRIL_OLD_REGULAR(2103, 451, 447, 55, 80, 200, 4),
	MITHRIL_LIGHTBROWN_TALL(11942, 11552, 447, 55, 80, 200, 4),
	MITHRIL_LIGHTBROWN_FLAT(11943, 11553, 447, 55, 80, 200, 4),
	MITHRIL_LIGHTBROWN_REGULAR(11944, 11554, 447, 55, 80, 200, 4),
	MITHRIL_DARKBROWN_TALL(11945, 11555, 447, 55, 80, 200, 4),
	MITHRIL_DARKBROWN_FLAT(11946, 11556, 447, 55, 80, 200, 4),
	MITHRIL_DARKBROWN_REGULAR(11947, 11557, 447, 55, 80, 200, 4),
	MITHRIL_DARKGREY_TALL(14853, 14832, 447, 55, 80, 200, 4),
	MITHRIL_DARKGREY_FLAT(14854, 14833, 447, 55, 80, 200, 4),
	MITHRIL_DARKGREY_REGULAR(14855, 14834, 447, 55, 80, 200, 4),
	MITHRIL_GREY_TALL(21278, 21296, 447, 55, 80, 200, 4),
	MITHRIL_GREY_FLAT(21279, 21297, 447, 55, 80, 200, 4),
	MITHRIL_GREY_REGULAR(21280, 21298, 447, 55, 80, 200, 4),
	ADAMANTITE_OLD_TALL(2104, 450, 449, 70, 95, 400, 6),
	ADAMANTITE_OLD_REGULAR(2105, 451, 449, 70, 95, 400, 6),
	ADAMANTITE_LIGHTBROWN_TALL(11939, 11552, 449, 70, 95, 400, 6),
	ADAMANTITE_LIGHTBROWN_FLAT(11940, 11553, 449, 70, 95, 400, 6),
	ADAMANTITE_LIGHTBROWN_REGULAR(11941, 11554, 449, 70, 95, 400, 6),
	ADAMANTITE_DARKGREY_TALL(14862, 14832, 449, 70, 95, 400, 6),
	ADAMANTITE_DARKGREY_FLAT(14863, 14833, 449, 70, 95, 400, 6),
	ADAMANTITE_DARKGREY_REGULAR(14864, 14834, 449, 70, 95, 400, 6),
	ADAMANTITE_GREY_TALL(21275, 21296, 449, 70, 95, 400, 6),
	ADAMANTITE_GREY_FLAT(21276, 21297, 449, 70, 95, 400, 6),
	ADAMANTITE_GREY_REGULAR(21277, 21298, 449, 70, 95, 400, 6),
	RUNITE_OLD_TALL(2106, 450, 451, 85, 125, 1000, 8),
	RUNITE_OLD_REGULAR(2107, 451, 451, 85, 125, 1000, 8),
	RUNITE_GREENDARKGREY_TALL(6669, 6947, 451, 85, 125, 1000, 8),
	RUNITE_GREENDARKGREY_FLAT(6670, 6948, 451, 85, 125, 1000, 8),
	RUNITE_DARKGREY_TALL(14859, 14832, 451, 85, 125, 1000, 8),
	RUNITE_DARKGREY_FLAT(14860, 14833, 451, 85, 125, 1000, 8),
	RUNITE_DARKGREY_REGULAR(14861, 14834, 451, 85, 125, 1000, 8),
	SHILO_GEM_ROCK_OLD_REGULAR(2111, 451, 1627, 1, 0, 1000, 1),
	SHILO_GEM_ROCK_GREY_TALL(17004, 17007, 1627, 1, 0, 1000, 1),
	SHILO_GEM_ROCK_GREY_FLAT(17005, 17008, 1627, 1, 0, 1000, 1),
	SHILO_GEM_ROCK_GREY_REGULAR(17006, 17009, 1627, 1, 0, 1000, 1),
	SHILO_GEM_ROCK_DARK_GREY_TALL(23560, 23563, 1627, 1, 0, 1000, 1),
	SHILO_GEM_ROCK_DARK_GREY_FLAT(23561, 23564, 1627, 1, 0, 1000, 1),
	SHILO_GEM_ROCK_DARK_GREY_REGULAR(23562, 23565, 1627, 1, 0, 1000, 1),
	SHILO_GEM_ROCK_GREY_TALL_2(23566, 23569, 1627, 1, 0, 1000, 1),
	SHILO_GEM_ROCK_GREY_FLAT_2(23567, 23570, 1627, 1, 0, 1000, 1),
	SHILO_GEM_ROCK_GREY_REGULAR_2(23568, 23571, 1627, 1, 0, 1000, 1);
	//Empty Old Rocks: 453 (Very Tall), 450 (Tall), 451 + 452 (Regular)
	//Empty White Rocks: 10585 (Tall), 10586 (Flat), 10587 (Regular)
	//Empty Light Light Grey Rocks: 17007 (Tall), 17008 (Flat), 17009 (Regular)
	//Empty Light Grey Rocks: 14915 (Tall), 14916 (Flat), N/A (Regular)
	//Empty Grey Rocks: 21296 (Tall), 21297 (Flat), 21298 (Regular)
	//Empty Dark Grey Rocks: 14832 (Tall), 14833 (Flat), 14834 (Regular)
	//Empty Dark Dark Grey Rocks: 15582 (Tall), 15583 (Flat), 15584 (Regular)
	//Empty Green-Dark Grey Rocks:  6947 (Tall), 6948 (Flat), N/A (Regular)
	//Empty Green-Grey Rocks: 15249 (Tall), 15250 (Flat), 15251 (Regular)
	//Empty Rust Rocks: 9723 (Tall), 9724 (Flat), 9725 (Regular)
	//Empty Light Brown Rocks: 11552 (Tall), 11553 (Flat), 11554 (Regular)
	//Empty Dark Brown Rocks: 11555 (Tall), 11556 (Flat), 11557 (Regular)

	/**
	 * The Object ID of this rock.
	 */
	private int rockId;

	/**
	 * The Object ID of the rock to replace this rock when depleted.
	 */
	private int replacementRockId;

	/**
	 * The ore rewarded for each mine of this rock.
	 */
	private int oreId;

	/**
	 * The level required to mine this rock.
	 */
	private int level;

	/**
	 * The experience granted for mining this rock.
	 */
	private double experience;

	/**
	 * The time it takes for this rock to respawn.
	 */
	private int respawnTimer;

	/**
	 * The amount of ores this rock contains.
	 */
	private int oreCount;

	/**
	 * A map of object ids to rocks.
	 */
	private static Map<Integer, Ore> rocks = new HashMap<Integer, Ore>();

	static {
		for (Ore rock : Ore.values()) {
			int object = rock.getRockId();
			rocks.put(object, rock);
		}
	}

	/**
	 * Gets a rock by an Object ID.
	 * 
	 * @param object The Object ID.
	 * @return The rock, or <code>null</code> if the object is not a rock.
	 */
	public static Ore forId(int object) {
		return rocks.get(object);
	}

	/**
	 * Creates a rock.
	 * 
	 * @param ore The ore obtained.
	 * @param level The level requirement.
	 * @param experience
	 * @param respawnTimer
	 * @param oreCount
	 * @param object
	 * @param replacementRock
	 */
	private Ore(int rockId, int replacementRockId, int oreId, int level, double experience, int respawnTimer, int oreCount) {
		this.rockId = rockId;
		this.replacementRockId = replacementRockId;
		this.oreId = oreId;
		this.level = level;
		this.experience = experience;
		this.respawnTimer = respawnTimer;
		this.oreCount = oreCount;
	}

	/**
	 * Gets the ore id.
	 * 
	 * @return The ore id.
	 */
	public int getOreId() {
		return oreId;
	}

	/**
	 * Gets the required level.
	 * 
	 * @return The required level.
	 */
	public int getRequiredLevel() {
		return level;
	}

	/**
	 * Gets the experience.
	 * 
	 * @return The experience.
	 */
	public double getExperience() {
		return experience;
	}

	/**
	 * @return the respawnTimer
	 */
	public int getRespawnTimer() {
		return respawnTimer;
	}

	/**
	 * @return the oreCount
	 */
	public int getOreCount() {
		return oreCount;
	}

	/**
	 * Gets the object id.
	 * 
	 * @return The object id.
	 */
	public int getRockId() {
		return rockId;
	}

	/**
	 * @return the replacementRock
	 */
	public int getReplacementRock() {
		return replacementRockId;
	}

	public static int getRandomGem(int[] gems) {
		return Misc.generatedValue(gems);
	}
}
