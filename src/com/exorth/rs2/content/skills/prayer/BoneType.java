package com.exorth.rs2.content.skills.prayer;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents different types of bones.
 * TODO: add the rest of the bones and give the first one the correct exp rate.
 * 
 * @author Joshua Barry
 * 
 */
public enum BoneType {

	NORMAL_BONES(526, 4.5),
	WOLF_BONES(2859, 4.5),
	BURNT_BONES(528, 4.5),
	MONKEY_BONES(3179, 5),
	BAT_BONES(530, 5.3),
	BIG_BONES(532, 15),
	JOGRE_BONES(3125, 15),
	ZOGRE_BONES(4812, 22.5),
	SHAIKAHAN_BONES(3123, 25),
	BABYDRAGON_BONES(534, 30),
	WVYERN_BONES(6812, 50),
	DRAGON_BONES(536, 72),
	DAGANNOTH_BONES(6729, 125),
	FAYRG_BONES(4830, 128),
	RAURG_BONES(4832, 134),
	OURG_BONES(4834, 140);

	private int itemId;

	private double experience;

	private BoneType(int itemId, double experience) {
		this.itemId = itemId;
		this.experience = experience;
	}

	public int getItemId() {
		return itemId;
	}

	public double getExperience() {
		return experience;
	}

	private static Map<Integer, BoneType> bones = new HashMap<Integer, BoneType>();

	static {
		for (BoneType boneType : BoneType.values()) {
			bones.put(boneType.getItemId(), boneType);
		}
	}

	public static BoneType forId(int id) {
		return bones.get(id);
	}
}
