package com.exorth.rs2.content.skills.prayer;

import com.exorth.rs2.action.Action;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * An action performed when the player buries bones.
 * 
 * @author Joshua Barry
 * 
 */
public class BurialAction extends Action {

	/**
	 * The type of bones being buried.
	 */
	private BoneType boneType;

	/**
	 * 
	 * @param entity
	 * @param boneType
	 */
	public BurialAction(Entity entity, BoneType boneType) {
		super(entity, 0);
		this.boneType = boneType;
	}

	public boolean canBury() {
		return ((Player) getEntity()).getInventory().getItemContainer().contains(boneType.getItemId());
	}

	@Override
	public CancelPolicy getCancelPolicy() {
		return CancelPolicy.ALWAYS;
	}

	@Override
	public StackPolicy getStackPolicy() {
		return StackPolicy.ALWAYS;
	}

	@Override
	public AnimationPolicy getAnimationPolicy() {
		return AnimationPolicy.RESET_ALL;
	}

	@Override
	public void execute() {
		final Player player = (Player) getEntity();

		if (player.getAttribute("isSkilling") != null) {
			this.stop();
			return;
		}

		player.setAttribute("isSkilling", (byte) 1);
		player.getUpdateFlags().sendAnimation(827);
		player.getActionSender().sendMessage("You bury the bones.");

		World.submit(new Task(true) {
			@Override
			protected void execute() {
				player.removeAttribute("isSkilling");
				if (player.getInventory().removeItem(new Item(boneType.getItemId()))) {
					player.getSkill().addExperience(Skill.PRAYER, boneType.getExperience());
				}
				this.stop();
			}
		});
		this.stop();
	}
}
