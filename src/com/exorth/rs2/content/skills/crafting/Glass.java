package com.exorth.rs2.content.skills.crafting;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Joshua Barry <Ares>
 * @author Dominick Jones <Jones>
 * 
 */
public enum Glass {

	MOLTEN_GLASS(1775, 1, 20, 1781),
	BEER_GLASS(1919, 1, 17.5, 1775),
	CANDLE_LANTERN(4527, 4, 19, 1775),
	OIL_LAMP(4522, 12, 25, 1775),
	VIAL(229, 33, 35, 1775),
	FISHBOWL(6667, 42, 42.5, 1775),
	UNPOWERED_ORB(567, 46, 52.5, 1775),
	LANTERN_LENS(4542, 49, 55, 1775),
	LIGHT_ORB(10973, 87, 70, 1775);

	private int rewardId;

	private int levelRequired;

	private double experience;

	private int materialId;

	/**
	 * 
	 * @param rewardId The item id of the rewarded item.
	 * @param levelRequired The level required to craft this item.
	 * @param experience The experience gained from crafting this item.
	 * @param materialId The item id of the material to be consumed.
	 */
	private Glass(int rewardId, int levelRequired, double experience, int materialId) {
		this.rewardId = rewardId;
		this.levelRequired = levelRequired;
		this.experience = experience;
		this.materialId = materialId;

	}

	public int getRewardId() {
		return rewardId;
	}

	public int getRequiredLevel() {
		return levelRequired;
	}

	public double getExperience() {
		return experience;
	}

	public int getMaterialId() {
		return materialId;
	}

	public static Map<Integer, Glass> glass = new HashMap<Integer, Glass>();

	static {
		for (Glass glassType : Glass.values()) {
			glass.put(glassType.getRewardId(), glassType);
		}
	}

	public static Glass forReward(int id) {
		return glass.get(id);
	}
}
