package com.exorth.rs2.content.skills.crafting;

import com.exorth.rs2.action.impl.ProductionAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.util.Misc;

/**
 * Armour Creation takes place when a player uses a needle on a tanned leather
 * hide. The player is then able to create leather armour with that hide.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class ArmourCreation extends ProductionAction {

	/**
	 * The amount of armour we will create.
	 */
	private int creationAmount;

	/**
	 * The type of armor we will craft.
	 */
	private Craftable craftable;

	public ArmourCreation(Entity entity, int creationAmount, Craftable craft) {
		super(entity);
		this.creationAmount = creationAmount;
		this.craftable = craft;
	}

	@Override
	public boolean canProduce() {
		return 0 < getRequiredAmount();
	}

	@Override
	public int getCycleCount() {
		return 4;
	}

	@Override
	public int getGraphic() {
		return -1;
	}

	@Override
	public int getAnimation() {
		return 1249;
	}

	@Override
	public double getExperience() {
		return craftable.getExperience();
	}

	@Override
	public Item[] getConsumedItems() {
		return new Item[]{ new Item(1734, 1), new Item(craftable.getItemId()) };
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You need a " + Skill.SKILL_NAME[getSkill()] + " level of " + this.getRequiredLevel() + " to create " + ItemManager.getInstance().getItemName(getRewards()[0].getId()) + ".";
	}

	@Override
	public int getProductionCount() {
		return creationAmount;
	}

	@Override
	public int getRequiredLevel() {
		return craftable.getRequiredLevel();
	}

	public int getRequiredAmount() {
		return craftable.getRequiredAmount();
	}

	@Override
	public Item[] getRewards() {
		return new Item[]{ new Item(craftable.getOutcome()) };
	}

	@Override
	public int getSkill() {
		return Skill.CRAFTING;
	}

	@Override
	public String getPreProductionMessage() {
		return null;
	}

	@Override
	public String getSuccessfulProductionMessage() {
		String itemName = ItemManager.getInstance().getItemName(getRewards()[0].getId()).toLowerCase();
		String prefix = Misc.getArticle(itemName);
		if (itemName.contains("glove") || itemName.contains("boot") || itemName.contains("vamb") || itemName.contains("chap")) {
			prefix = "a pair of";
		} else if (itemName.endsWith("s")) {
			prefix = "some";
		}
		return "You make " + prefix + " " + itemName + ".";
	}

	@Override
	public String getInsufficientResourcesMessage() {
		String itemName = ItemManager.getInstance().getItemName(getConsumedItems()[0].getId()).toLowerCase();
		return "You need at least " + getRequiredAmount() + " " + itemName + " to craft this." ;
	}
}
