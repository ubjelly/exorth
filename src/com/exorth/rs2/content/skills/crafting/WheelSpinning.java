package com.exorth.rs2.content.skills.crafting;

import com.exorth.rs2.action.impl.ProductionAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;

/**
 * Wheel spinning is the action in which a player spins a natural product such
 * as Flax or Wool with what is known a "Spinning Wheel" to yield a new product
 * such as Bow String or a Ball of Wool.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class WheelSpinning extends ProductionAction {

	/**
	 * The item we are going to spin.
	 */
	private Spinnable spinnable;

	/**
	 * How many times we are going to spin the wheel.
	 */
	private int productionCount;

	/**
	 * 
	 * @param entity
	 * @param i
	 */
	public WheelSpinning(Entity entity, int i, Spinnable spin) {
		super(entity);
		this.productionCount = i;
		this.spinnable = spin;
	}

	@Override
	public boolean canProduce() {
		if (getEntity().isPlayer()) {
			return true;
		}
		return false;
	}

	@Override
	public int getCycleCount() {
		return 4;
	}

	@Override
	public int getGraphic() {
		return -1;
	}

	@Override
	public int getAnimation() {
		return 894;
	}

	@Override
	public double getExperience() {
		return spinnable.getExperience();
	}

	@Override
	public Item[] getConsumedItems() {
		return new Item[]{ spinnable.getItem() };
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You need a " + Skill.SKILL_NAME[getSkill()] + " level of " + getRequiredLevel() + " to spin " + ItemManager.getInstance().getItemName(spinnable.getOutcome().getId()).toLowerCase() + ".";
	}

	@Override
	public int getProductionCount() {
		return productionCount;
	}

	@Override
	public int getRequiredLevel() {
		return spinnable.getRequiredLevel();
	}

	@Override
	public Item[] getRewards() {
		return new Item[]{ spinnable.getOutcome() };
	}

	@Override
	public int getSkill() {
		return Skill.CRAFTING;
	}

	@Override
	public String getPreProductionMessage() {
		return null;
	}

	@Override
	public String getSuccessfulProductionMessage() {
		return "You spin the " + ItemManager.getInstance().getItemName((getConsumedItems()[0]).getId()).toLowerCase() + " into a " + ItemManager.getInstance().getItemName((getRewards()[0]).getId()).toLowerCase() + ".";
	}

	@Override
	public String getInsufficientResourcesMessage() {
		// TODO Auto-generated method stub
		return null;
	}
}
