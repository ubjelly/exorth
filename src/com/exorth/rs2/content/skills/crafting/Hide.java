package com.exorth.rs2.content.skills.crafting;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all relevant data to hide tanning.
 * 
 * @author Joshua Barry <Ares>
 * @author D.J <Pulse>
 * 
 */
public enum Hide {

	/**
	 * Soft Leather
	 * 
	 */
	COWHIDE_LEATHER(1739, 1741, 1, new int[]{ 0, 2 }, new int[]{}),
	/**
	 * Hard Leather
	 * 
	 */
	COWHIDE_HARDLEATHER(1739, 1743, 28, new int[]{ 3, 5 }, new int[]{}),
	/**
	 * Snake Hide
	 * 
	 */
	SNAKEHIDE(6287, 6289, 45, new int[]{ 15, 25 }, new int[]{ 6322, 6324, 6326, 6328, 6330 }),
	/**
	 * Snake Hide (Swamp)
	 * 
	 */
	SNAKEHIDE_SWAMP(7801, 6289, 45, new int[]{ 20, 45 }, new int[]{ 6322, 6324, 6326, 6328, 6330 }),
	/**
	 * Green Dragon Leather
	 * 
	 */
	GREEN_LEATHER(1753, 1745, 57, new int[]{ 15, 25 }, new int[]{ 1135, 1065, 1099 }),
	/**
	 * Blue Dragon Leather
	 * 
	 */
	BLUE_LEATHER(1751, 2505, 66, new int[]{ 20, 45 }, new int[]{ 2499, 2487, 2493 }),
	/**
	 * Red Dragon Leather
	 * 
	 */
	RED_LEATHER(1749, 2507, 73, new int[]{ 20, 45 }, new int[]{ 2501, 2489, 2495 }),
	/**
	 * Black Dragon Leather
	 * 
	 */
	BLACK_LEATHER(1747, 2509, 79, new int[]{ 20, 45 }, new int[]{ 2503, 2491, 2497 });

	private int itemId;

	private int outcome;

	private int requiredLevel;

	private int[] coins;

	private int[] craftableOutcomes;

	private Hide(int itemId, int outcome, int requiredLevel, int[] coins, int[] craftableOutcomes) {
		this.itemId = itemId;
		this.outcome = outcome;
		this.requiredLevel = requiredLevel;
		this.coins = coins;
		this.craftableOutcomes = craftableOutcomes;
	}

	public int getItemId() {
		return itemId;
	}

	public int[] getCoins() {
		return coins;
	}

	public int getOutcome() {
		return outcome;
	}

	public int getRequiredLevel() {
		return requiredLevel;
	}

	public int[] getCraftableOutcomes() {
		return craftableOutcomes;
	}

	private static Map<Integer, Hide> hides = new HashMap<Integer, Hide>();

	static {
		for (Hide hide : Hide.values()) {
			hides.put(hide.getItemId(), hide);
		}
	}

	public static Hide forId(int id) {
		return hides.get(id);
	}

	private static Map<Integer, Hide> hideRewards = new HashMap<Integer, Hide>();

	static {
		for (Hide hide : Hide.values()) {
			hideRewards.put(hide.getOutcome(), hide);
		}
	}

	/**
	 * As the same Item ID for Cowhide is used for two different types of leathers, we use this to call it by reward.
	 * 
	 * @param id
	 * @return
	 */
	public static Hide forReward(int id) {
		return hideRewards.get(id);
	}
}
