package com.exorth.rs2.content.skills.crafting;

/**
 * 
 * Defines a type of crafting which later helps our application determine which
 * action to proceed with at runtime.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum CraftingType {

	ARMOUR_CREATION, GEM_CRAFTING, LEATHER_TANNING, WHEEL_SPINNING

}
