package com.exorth.rs2.content.skills.crafting;

import com.exorth.rs2.action.impl.ProductionAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.items.Item;

/**
 * Glass Melting takes places when a player uses Soda ash on a furnace, and
 * turns it to Molten glass.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class GlassMelting extends ProductionAction {

	private int productionCount;

	private Glass glass;

	public GlassMelting(Entity entity, int productionCount, Glass glass) {
		super(entity);
		this.productionCount = productionCount;
		this.glass = glass;
	}

	@Override
	public boolean canProduce() {
		return true;
	}

	@Override
	public int getCycleCount() {
		return 2;
	}

	@Override
	public int getGraphic() {
		return -1;
	}

	@Override
	public int getAnimation() {
		return 899;
	}

	@Override
	public double getExperience() {
		return glass.getExperience();
	}

	@Override
	public Item[] getConsumedItems() {
		return new Item[]{ new Item(1783), new Item(glass.getMaterialId()) };
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You need a " + Skill.SKILL_NAME[getSkill()] + " level of " + getRequiredLevel() + " to melt glass.";
	}

	@Override
	public int getProductionCount() {
		return productionCount;
	}

	@Override
	public int getRequiredLevel() {
		return glass.getRequiredLevel();
	}

	@Override
	public Item[] getRewards() {
		return new Item[]{ new Item(glass.getRewardId()) };
	}

	@Override
	public int getSkill() {
		return Skill.CRAFTING;
	}

	@Override
	public String getPreProductionMessage() {
		return null;
	}

	@Override
	public String getSuccessfulProductionMessage() {
		return "You heat the sand and soda ash in the furnace to make glass.";
	}

	@Override
	public String getInsufficientResourcesMessage() {
		// TODO Auto-generated method stub
		return null;
	}
}
