package com.exorth.rs2.content.skills.crafting;

import com.exorth.rs2.action.impl.ProductionAction;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.items.Item;

/**
 * Jewelry Creation takes place when a player moulds gold into a ring or amulet
 * or attaches string or other materials to create different types of jewels.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class JewelryCreation extends ProductionAction {

	public JewelryCreation(Entity entity) {
		super(entity);
	}

	@Override
	public boolean canProduce() {
		return getEntity().isPlayer();
	}

	@Override
	public int getCycleCount() {
		return 0;
	}

	@Override
	public int getGraphic() {
		return -1;
	}

	@Override
	public int getAnimation() {
		return 0;
	}

	@Override
	public double getExperience() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Item[] getConsumedItems() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsufficentLevelMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getProductionCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getRequiredLevel() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public Item[] getRewards() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getSkill() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getPreProductionMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSuccessfulProductionMessage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getInsufficientResourcesMessage() {
		// TODO Auto-generated method stub
		return null;
	}
}
