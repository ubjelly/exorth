package com.exorth.rs2.content.skills.crafting;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all data relevant to craftable goods.
 * 
 * @author Joshua Barry <Ares>
 * @author Mko
 * 
 */
public enum Craftable {

	LEATHER_GLOVES(1741, 1059, 1, 13.8, 1),
	LEATHER_BOOTS(1741, 1061, 7, 16.25, 1), 
	LEATHER_COWL(1741, 1167, 9, 18.5, 1),
	LEATHER_VAMBS(1741, 1063, 11, 22, 1),
	LEATHER_BODY(1741, 1129, 14, 25, 1),
	LEATHER_CHAPS(1741, 1095, 18, 27, 1),
	COIF(1741, 1169, 38, 37, 1),
	SNAKESKIN_BOOTS(6289, 6328, 45, 43, 6),
	SNAKESKIN_VAMB(6289, 6330, 47, 46, 8),
	SNAKESKIN_BANDANA(6289, 6326, 48, 50, 5),
	SNAKESKIN_CHAPS(6289, 6324, 51, 54, 12),
	SNAKESKIN_BODY(6289, 6322, 53, 56, 15),
	GREEN_VAMBS(1745, 1065, 57, 62, 1),
	GREEN_CHAPS(1745, 1099, 60, 124, 2),
	GREEN_BODY(1745, 1135, 63, 186, 3),
	BLUE_VAMBS(2505, 2487, 66, 70, 1),
	BLUE_CHAPS(2505, 2493, 68, 140, 2),
	BLUE_BODY(2505, 2499, 71, 210, 3),
	RED_VAMBS(2507, 2489, 73, 78, 1),
	RED_CHAPS(2507, 2495, 75, 156, 2),
	RED_BODY(2507, 2501, 77, 234, 3),
	BLACK_VAMBS(2509, 2491, 79, 86, 1),
	BLACK_CHAPS(2509, 2497, 82, 172, 2),
	BLACK_BODY(2509, 2503, 84, 258, 3);

	/**
	 * The hide Item ID.
	 */
	private int itemId; 

	/**
	 * The outcome Item ID.
	 */
	private int outcome;

	/**
	 * The required level.
	 */
	private int requiredLevel;

	/**
	 * The experience gained.
	 */
	private double experience;

	/**
	 * The amount of thread required.
	 */
	private int requiredAmount;

	//TODO: Maybe make another variable for required amount of hides?
	//TODO: Also remove the correct amount of thread each time an item is produced.
	
	/**
	 * 
	 * @param itemId
	 * @param outcome
	 * @param requiredLevel
	 * @param experience
	 * @param requiredAmount
	 */
	private Craftable(int itemId, int outcome, int requiredLevel, double experience, int requiredAmount) {
		this.itemId = itemId;
		this.outcome = outcome;
		this.requiredLevel = requiredLevel;
		this.experience = experience;
		this.requiredAmount = requiredAmount;
	}

	public int getItemId() {
		return itemId;
	}

	public int getOutcome() {
		return outcome;
	}

	public int getRequiredAmount() {
		return requiredAmount;
	}

	public int getRequiredLevel() {
		return requiredLevel;
	}

	public double getExperience() {
		return experience;
	}

	private static Map<Integer, Craftable> craftables = new HashMap<Integer, Craftable>();

	static {
		for (Craftable craftable : Craftable.values()) {
			craftables.put(craftable.getItemId(), craftable);
		}
	}

	public static Craftable forId(int id) {
		return craftables.get(id);
	}

	private static Map<Integer, Craftable> craftableRewards = new HashMap<Integer, Craftable>();

	static {
		for (Craftable craftable : Craftable.values()) {
			craftableRewards.put(craftable.getOutcome(), craftable);
		}
	}

	public static Craftable forReward(int id) {
		return craftableRewards.get(id);
	}
}
