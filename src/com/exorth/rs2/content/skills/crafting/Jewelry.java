package com.exorth.rs2.content.skills.crafting;

import java.util.HashMap;
import java.util.Map;

import com.exorth.rs2.model.items.Item;

/**
 * 
 * @author Joshua Barry <Ares>
 * @author Pulse D. Jones <Jones>
 * 
 */
public enum Jewelry {

	AMULET_GOLD(1673, 8, 30, new int[]{ 2357 }),
	AMULET_SAPPHIRE(1675, 20, 35, new int[]{ 2357, 1607 }),
	AMULET_EMERALD(1677, 30, 40, new int[]{ 2357, 1605 }),
	AMULET_RUBY(1679, 50, 80, new int[]{ 2357, 1603 }),
	AMULET_DIAMOND(1681, 70, 95, new int[]{ 2357, 1601 }),
	AMULET_DRAGONSTONE(1683, 80, 145, new int[]{ 2357, 1615 }),
	AMULET_ONYX(6579, 90, 160, new int[]{ 2357, 6573 }),
	GOLD_AMULET(1692, 1, 4, new int[]{ 1673, 1759 }),
	GOLD_RING(1635, 5, 15, new int[]{ 2357 }),
	GOLD_NECKLACE(1654, 6, 20, new int[]{ 2357 }),
	GOLD_BRACELET(11069, 7, 25, new int[]{ 2357 }),
	SAPPHIRE_AMULET(1694, 24, 65, new int[]{ 1675, 1759 }),
	SAPPHIRE_RING(1637, 20, 40, new int[]{ 2357, 1607 }),
	SAPPHIRE_NECKLACE(1656, 22, 55, new int[]{ 2357, 1607 }),
	SAPPHIRE_BRACELET(11072, 23, 60, new int[]{ 2357, 1607 }),
	EMERALD_AMULET(1696, 31, 70, new int[]{ 1677, 1759 }),
	EMERALD_RING(1639, 27, 55, new int[]{ 2357, 1605 }),
	EMERALD_NECKLACE(1658, 29, 60, new int[]{ 1605, 2357 }),
	EMERALD_BRACELET(11076, 29, 65, new int[]{ 1605, 2357 }),
	RUBY_AMULET(1698, 50, 85, new int[]{ 1759, 1679 }),
	RUBY_RING(1641, 34, 70, new int[]{ 2357, 1603 }),
	RUBY_NECKLACE(1660, 40, 75, new int[]{ 2357, 1603 }),
	RUBY_BRACELET(11085, 42, 80, new int[]{ 2357, 1603 }),
	DIAMOND_AMULET(1731, 70, 100, new int[]{ 1681, 1759 }),
	DIAMOND_RING(1643, 43, 85, new int[]{ 2357, 1601 }),
	DIAMOND_NECKLACE(1662, 56, 90, new int[]{ 2357, 1601 }),
	DIAMOND_BRACELET(11092, 58, 95, new int[]{ 2357, 1601 }),
	DRAGONSTONE_AMULET(1702, 80, 150, new int[]{ 1683, 1759 }),
	DRAGONSTONE_RING(1645, 55, 95, new int[]{ 2357, 1615 }),
	ONYX_AMULET(6585, 90, 165, new int[]{ 6579, 1759 }), 
	ONYX_RING(6575, 67, 115, new int[]{ 6573, 2357 });

	private Item reward;

	private int levelRequired;

	private double experienceGain;

	private int[] materialsRequired;

	/**
	 * 
	 * @param rewardId The id of the reward item.
	 * @param levelRequired The level required to make this jewelry
	 * @param experienceGain The experienced gained.
	 * @param materialsRequired The materials required.
	 */
	private Jewelry(int rewardId, int levelRequired, double experienceGain, int[] materialsRequired) {
		this.reward = new Item(rewardId);
		this.levelRequired = levelRequired;
		this.experienceGain = experienceGain;
		this.materialsRequired = materialsRequired;
	}

	/**
	 * 
	 * @return
	 */
	public Item getReward() {
		return reward;
	}

	/**
	 * 
	 * @return
	 */
	public int getRequiredLevel() {
		return levelRequired;
	}

	/**
	 * 
	 * @return
	 */
	public double getExperience() {
		return this.experienceGain;
	}

	/**
	 * 
	 * @return
	 */
	public int[] getMaterialsRequired() {
		return materialsRequired;
	}

	private static Map<Integer, Jewelry> jewelry = new HashMap<Integer, Jewelry>();

	static {
		for (Jewelry jewel : Jewelry.values()) {
			jewelry.put(jewel.getReward().getId(), jewel);
		}
	}

	/**
	 * Call a Jewelry item by it's reward.
	 * 
	 * @param id The item id of the reward.
	 * @return
	 */
	public static Jewelry forReward(int id) {
		return jewelry.get(id);
	}
}
