package com.exorth.rs2.content.skills.fletching;

import com.exorth.rs2.action.impl.ProductionAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;

/**
 * 
 * 
 * @author Joshua Barry
 * 
 */
public class HeadlessArrowCreation extends ProductionAction {

	int creationAmount;

	public HeadlessArrowCreation(Entity entity, int creationAmount) {
		super(entity);
		this.creationAmount = creationAmount;
	}

	/**
	 * Get the creation amount.
	 * 
	 * @return the creation amount.
	 */
	public int getCreationAmount() {
		return creationAmount;
	}

	@Override
	public boolean canProduce() {
		Player player = (Player) getEntity();

		if (player.getInventory().getItemContainer().getCount(52) <= 0) {
			player.getActionSender().sendMessage("You do not have any arrow shafts!");
			return false;
		} else if (player.getInventory().getItemContainer().getCount(314) <= 0) {
			player.getActionSender().sendMessage("You do not have any feathers!");
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int getCycleCount() {
		return 0;
	}

	@Override
	public int getGraphic() {
		return -1;
	}

	@Override
	public int getAnimation() {
		return -1;
	}

	@Override
	public double getExperience() {
		return 15;
	}

	@Override
	public Item[] getConsumedItems() {
		return new Item[]{ new Item(52, getCreationAmount()), new Item(314, getCreationAmount()) };
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You need a Fletching level of " + getRequiredLevel() + " to make headless arrows.";
	}

	@Override
	public int getProductionCount() {
		return 1;
	}

	@Override
	public int getRequiredLevel() {
		return 1;
	}

	@Override
	public Item[] getRewards() {
		return new Item[]{ new Item(53, getCreationAmount()) };
	}

	@Override
	public int getSkill() {
		return Skill.FLETCHING;
	}

	@Override
	public String getPreProductionMessage() {
		return null;
	}

	@Override
	public String getSuccessfulProductionMessage() {
		return "You attach the feathers to the arrow shafts.";
	}

	@Override
	public String getInsufficientResourcesMessage() {
		return "You do not have enough arrow shafts or feathers to do this.";
	}
}
