package com.exorth.rs2.content.skills.fletching;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all data relevant to logs that a player can fletch into a greater
 * good.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum FletchableLogs {

	NORMAL(1511, new int[]{ 52, 50, 48, 9440 }, new int[]{ 1, 5, 10, 9 }, new double[]{ 5, 10, 20, 24 }),
	OAK(1521, new int[]{ 54, 56, 9442 }, new int[]{ 20, 24, 25 }, new double[]{ 16.5, 30, 25 }),
	WILLOW(1519, new int[]{ 60, 58, 9444 }, new int[]{ 35, 39, 40 }, new double[]{ 30, 35, 35 }),
	TEAK(6333, new int[]{ 9446 }, new int[]{ 46 }, new double[]{ 27 }),
	MAPLE(1517, new int[]{ 64, 62, 9448 }, new int[]{ 50, 54, 55 }, new double[]{ 35, 40, 40 }),
	MAHOGANY(6332, new int[]{ 9450 }, new int[]{ 61 }, new double[]{ 41 }),
	YEW(1515, new int[]{ 68, 66, 9452 }, new int[]{ 65, 69, 70 }, new double[]{ 67.5, 70, 75 }),
	MAGIC(1513, new int[]{ 72, 70 }, new int[]{ 80, 85 }, new double[]{ 83.25, 91.5 });

	/**
	 * The id of the logs
	 */
	private int logId;

	/**
	 * The items displayed on the fletching interface.
	 */
	private int[] item;

	/**
	 * The level required to fletch the items on the fletching interface.
	 */
	private int[] level;

	/**
	 * The experience granted for the first item on the fletching interface.
	 */
	private double[] experience;

	/**
	 * A map of item ids to logs.
	 */
	private static Map<Integer, FletchableLogs> logs = new HashMap<Integer, FletchableLogs>();

	/**
	 * Populates the log map.
	 */
	static {
		for (FletchableLogs log : FletchableLogs.values()) {
			logs.put(log.logId, log);
		}
	}

	/**
	 * Gets a log by an item id.
	 * 
	 * @param item The item id.
	 * @return The Log, or <code>null</code> if the object is not a log.
	 */
	public static FletchableLogs forId(int item) {
		return logs.get(item);
	}

	private FletchableLogs(int logId, int[] item, int[] level, double[] experience) {
		this.logId = logId;
		this.item = item;
		this.level = level;
		this.experience = experience;
	}

	/**
	 * @return the experience
	 */
	public double[] getExperience() {
		return experience;
	}

	/**
	 * @return the items
	 */
	public int[] getRewards() {
		return item;
	}

	/**
	 * @return the level
	 */
	public int[] getRequiredLevel() {
		return level;
	}

	/**
	 * @return the logId
	 */
	public int getLogId() {
		return logId;
	}
}
