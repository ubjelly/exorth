package com.exorth.rs2.content.skills.fletching;

import com.exorth.rs2.action.impl.ProductionAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.items.Item;

/**
 * Bow Creation occurs when a player attaches a Bow string to an unstrung bow.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public class BowCreation extends ProductionAction {

	/**
	 * The amount of bows to create.
	 */
	private int productionCount;

	/**
	 * The bow to create.
	 */
	private UnstrungBows bow;

	public BowCreation(Entity entity, int productionCount, UnstrungBows bow) {
		super(entity);
		this.productionCount = productionCount;
		this.bow = bow;
	}

	/**
	 * Create the correct animation per bow.
	 * 
	 * @param bow The bow you are trying to string.
	 * @return the animation for stringing the bow.
	 */
	public int getStringingAnimation(UnstrungBows bow) {
		switch (bow.getItemId()) {
			case 48:
				return 6684;
			case 50:
				return 6678;
			case 54:
				return 6679;
			case 56:
				return 6685;
			case 58:
				return 6686;
			case 60:
				return 6680;
			case 62:
				return 6687;
			case 64:
				return 6681;
			case 66:
				return 6688;
			case 68:
				return 6682;
			case 70:
				return 6689;
			case 72:
				return 6683;
			default:
				return -1;
		}
	}

	@Override
	public boolean canProduce() {
		if (getEntity().isPlayer()) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public int getCycleCount() {
		return 9;
	}

	@Override
	public int getGraphic() {
		return -1;
	}

	@Override
	public int getAnimation() {
		return getStringingAnimation(bow);
	}

	@Override
	public double getExperience() {
		return bow.getExperience();
	}

	@Override
	public Item[] getConsumedItems() {
		return new Item[]{ new Item(1777, 1), new Item(bow.getItemId(), 1) };
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You need a level of " + bow.getLevelRequired() + " to string this bow.";
	}

	@Override
	public int getProductionCount() {
		return productionCount;
	}

	@Override
	public int getRequiredLevel() {
		return bow.getLevelRequired();
	}

	@Override
	public Item[] getRewards() {
		return new Item[]
		{ new Item(bow.getProduct(), 1) };
	}

	@Override
	public int getSkill() {
		return Skill.FLETCHING;
	}

	@Override
	public String getPreProductionMessage() {
		return null;
	}

	@Override
	public String getSuccessfulProductionMessage() {
		return "You attach the string to the bow.";
	}

	@Override
	public String getInsufficientResourcesMessage() {
		// TODO Auto-generated method stub
		return null;
	}
}
