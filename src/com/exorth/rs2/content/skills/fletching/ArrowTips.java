package com.exorth.rs2.content.skills.fletching;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all data relevant to arrow tips.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum ArrowTips {

	BRONZE(39, 882, 1, 2.6),
	IRON(40, 884, 15, 3.8),
	STEEL(41, 886, 30, 6.3),
	MITHRIL(42, 888, 45, 8.8),
	ADAMANT(43, 890, 60, 11.3),
	RUNE(44, 892, 75, 13.8),
	DRAGON(11237, 11212, 90, 16.3);

	/**
	 * The id
	 */
	private int itemId;

	/**
	 * The reward;
	 */
	private int reward;

	/**
	 * The level required.
	 */
	private int levelRequired;

	/**
	 * The experience granted.
	 */
	private double experience;

	/**
	 * A map of item ids to arrow tips.
	 */
	private static Map<Integer, ArrowTips> arrowtips = new HashMap<Integer, ArrowTips>();

	/**
	 * Populates the arrowtips map.
	 */
	static {
		for (ArrowTips arrowtip : ArrowTips.values()) {
			arrowtips.put(arrowtip.itemId, arrowtip);
		}
	}

	/**
	 * Gets an arrow tip by an item id.
	 * 
	 * @param item The item id.
	 * @return The ArrowTip, or <code>null</code> if the object is not a arrow
	 *		 tip.
	 */
	public static ArrowTips forId(int item) {
		return arrowtips.get(item);
	}

	/**
	 * 
	 * @param itemId
	 * @param reward
	 * @param levelRequired
	 * @param experience
	 */
	private ArrowTips(int itemId, int reward, int levelRequired, double experience) {
		this.itemId = itemId;
		this.reward = reward;
		this.levelRequired = levelRequired;
		this.experience = experience;
	}

	/**
	 * @return the experience
	 */
	public double getExperience() {
		return experience;
	}

	/**
	 * @return the id
	 */
	public int getItemId() {
		return itemId;
	}

	/**
	 * @return the levelRequired
	 */
	public int getLevelRequired() {
		return levelRequired;
	}

	/**
	 * @return the reward
	 */
	public int getReward() {
		return reward;
	}
}
