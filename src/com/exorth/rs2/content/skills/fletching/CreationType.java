package com.exorth.rs2.content.skills.fletching;

/**
 * 
 * @author Joshua Barry
 * 
 */
public enum CreationType {

	ARROW_CREATION, BOW_CREATION, CROSSBOW_CREATION, HEADLESS_ARROW_CREATION, UNSTRUNG_BOW_CREATION

}
