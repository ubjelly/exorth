package com.exorth.rs2.content.skills.fletching;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains all data relevant to unstrung bows.
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum UnstrungBows {

	NORMAL_SHORTBOW(50, 841, 1, 5),
	NORMAL_LONGBOW(48, 839, 10, 10),
	OAK_SHORTBOW(54, 843, 20, 16.5),
	OAK_LONGBOW(56, 845, 25, 25),
	WILLOW_SHORTBOW(60, 849, 35, 33.25),
	WILLOW_LONGBOW(58, 847, 40, 41.5),
	MAPLE_SHORTBOW(64, 853, 50, 50),
	MAPLE_LONGBOW(62, 851, 55, 58.2),
	YEW_SHORTBOW(68, 857, 65, 67.5),
	YEW_LONGBOW(66, 855, 70, 75),
	MAGIC_SHORTBOW(72, 861, 80, 83.2),
	MAGIC_LONGBOW(70, 859, 85, 91.5);

	/**
	 * The unstrung bow
	 */
	private final int itemId;

	/**
	 * The finished product
	 */
	private final int product;

	/**
	 * The level required for making a bow
	 */
	private final int levelRequired;

	/**
	 * The experience gained from making the bow
	 */
	private final double experience;

	/**
	 * A {@link Map} of the {@link UnstrungBows}s in this enum
	 */
	private static final Map<Integer, UnstrungBows> bow = new HashMap<Integer, UnstrungBows>();

	/**
	 * Fill in the values in the {@link bow}s {@link HashMap} to make it easier
	 * to find back the values
	 */
	static {
		for (UnstrungBows b : UnstrungBows.values()) {
			bow.put(b.getItemId(), b);
		}
	}

	public static UnstrungBows forId(int item) {
		return bow.get(item);
	}

	/**
	 * Constructs a new {@link UnstrungBows}s object w/ given parameters
	 * 
	 * @param item The unstrung bow
	 * @param product The strung bow
	 * @param levelRequired The level required for a bow
	 * @param experience The experience gained for a bow
	 */
	UnstrungBows(int itemId, int product, int levelRequired, double experience) {
		this.itemId = itemId;
		this.product = product;
		this.levelRequired = levelRequired;
		this.experience = experience;
	}

	/**
	 * The experience gained from crafting a bow
	 * 
	 * @return The experience gained from crafting a bow
	 */
	public double getExperience() {
		return experience;
	}

	/**
	 * The unstrung bows that need stringing
	 * 
	 * @return the bows that need to be strung
	 */
	public int getItemId() {
		return itemId;
	}

	/**
	 * The level that is required for a bow
	 * 
	 * @return The certain level for a required bow
	 */
	public int getLevelRequired() {
		return levelRequired;
	}

	/**
	 * The finished product or "strung bow"
	 * 
	 * @return The finished product
	 */
	public int getProduct() {
		return product;
	}
}
