package com.exorth.rs2.content.skills.magic;

import com.exorth.rs2.content.combat.magic.Magic;
import com.exorth.rs2.model.players.Player;

/**
 * Charge spell
 * 
 * @author AkZu
 * 
 */
public class Charge {

	private static void sendError(Player player) {
		player.getActionSender().sendMessage("You need to be wearing a god staff and a cape of your chosen god,");
		player.getActionSender().sendMessage("in order to cast this spell!");
	}

	public static void handleCharge(Player player) {
		if (!Magic.getInstance().hasRequiredLevel(player, 80)) {
			return;
		}

		if (player.getAttribute("charge") != null) {
			if (System.currentTimeMillis() - (Long) player.getAttribute("charge") < 420000) {
				player.getActionSender().sendMessage("You are still affected by a magical charge.");
				return;
			} else {
				player.removeAttribute("charge");
				player.setAttribute("chargeCoolDown", System.currentTimeMillis());
			}
		}

		if (player.getAttribute("chargeCoolDown") != null) {
			if (System.currentTimeMillis() - (Long) player.getAttribute("chargeCoolDown") < 60000) {
				player.getActionSender().sendMessage("You must wait 1 minute to cast this spell again.");
				return;
			} else {
				player.removeAttribute("chargeCoolDown");
			}
		}

		if (player.getEquipment().get(1) == null || player.getEquipment().get(3) == null) {
			sendError(player);
			return;
		}

		switch (player.getEquipment().get(1).getId()) {
			case 2412:
			case 2413:
			case 2414:
				break;
			default:
				sendError(player);
				return;
		}

		switch (player.getEquipment().get(3).getId()) {
			case 2415:
				if (player.getEquipment().get(1).getId() != 2412) {
					sendError(player);
					return;
				}
				break;
			case 2416:
				if (player.getEquipment().get(1).getId() != 2413) {
					sendError(player);
					return;
				}
				break;
			case 2417:
				if (player.getEquipment().get(1).getId() != 2414) {
					sendError(player);
					return;
				}
				break;
			default:
				sendError(player);
				return;
		}

		if (!Magic.getInstance().removeRunes(player, new int[]{ -1, 565, 3, 554, 3, 556, 3 })) {
			return;
		}

		player.setAttribute("charge", System.currentTimeMillis());
		player.getUpdateFlags().sendGraphic(111, 5, 100);
		player.getUpdateFlags().sendAnimation(811);
		player.getActionSender().sendMessage("You feel charged with magic power.");
		player.getSkill().addExperience(6, 180);
	}
}
