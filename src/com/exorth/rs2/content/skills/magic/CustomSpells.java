package com.exorth.rs2.content.skills.magic;

import com.exorth.rs2.content.combat.util.Poison;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class CustomSpells {
	
	private static CustomSpells instance;

	public static CustomSpells getInstance() {
		if (instance == null) {
			instance = new CustomSpells();
		}
		return instance;
	}

	private void applyFlamingArrows(final Player player) {
		if (!player.usingFlamingArrows()) {
			player.setFlamingArrows(true);
			World.submit(new Task(120) { // ~1min 15sec
				@Override
				protected void execute() {
					player.setFlamingArrows(false);
					player.getActionSender().sendMessage("Your flaming arrows have worn off.");
					this.stop();
				}
			});
		}
	}

	private void applySteroids(final Player player) {
		if (!player.usingSteroids()) {
			player.setSteroids(true);
			World.submit(new Task(120) { // ~1min 15sec
				@Override
				protected void execute() {
					player.setSteroids(false);
					player.getActionSender().sendMessage("Your steroids have worn off.");
					this.stop();
				}
			});
		}
	}

	private void applyCure(final Player player) {
		if (!player.getCureOnCool()) {
			player.setCureOnCool(true);
			if (player.isPoisoned()) {
				Poison.appendPoison(player, false, 0);
				player.getActionSender().sendMessage("You have been cured of poison.");
			}
			World.submit(new Task(500) {
				@Override
				protected void execute() {
					player.setCureOnCool(false);
					this.stop();
				}
			});
		}
	}

	private void applyHeal(final Player player) {
		if (!player.getHealOnCool()) {
			player.setHealOnCool(true);
			int playerHitpointsLevel = player.getSkill().getLevelForXP(player.getSkill().getExp()[3]);
			int currentHitpoints = player.getSkill().getLevel()[3];
			int healAmount = 30;
			if (currentHitpoints != playerHitpointsLevel) {
				if (playerHitpointsLevel - healAmount <= currentHitpoints) { //Don't want to go over player's hp level
					player.getSkill().getLevel()[3] = (byte) playerHitpointsLevel;
				} else {
					player.getSkill().getLevel()[3] += (byte) healAmount;
				}
				player.getActionSender().sendMessage("You gain some health back.");
				player.getSkill().refresh(3);
			}
			World.submit(new Task(700) {
				@Override
				protected void execute() {
					player.setHealOnCool(false);
					this.stop();
				}
			});
		}
	}

	/**
	 * Cast enchantment spell
	 * @param player
	 * @param spell
	 */
	public void castEnhancement(final Player player, Spell spell) {
		if(!canCastSpell(player, spell)) {
			return;
		}

		//TODO: Add delay or similar to fix spam clicking spells
		for (int i=0; i<spell.getRunesReq().length; i++) {
			Item item = new Item(spell.getRunesReq()[i], spell.getRuneAmounts()[i]);
			player.getInventory().removeItem(item);
			//TODO: Perhaps create check on removeItem, exit code if returns false?
		}

		Player otherPlayer = null;
		for (Player other : World.getPlayers()) {
			if (other != null && other.getPosition().isWithinMultiDistance(player.getPosition())) {
				otherPlayer = other;
				otherPlayer.getUpdateFlags().sendAnimation(spell.getAnimation());
				otherPlayer.getUpdateFlags().sendGraphic(spell.getGfx());
				otherPlayer.getUpdateFlags().sendFaceToDirection(player.getPosition());
			}
		}

		removeRunes(player, spell);
		drainPrayer(player, spell);

		switch (spell.getId()) {
			case 17009: //Flaming Arrows
				applyFlamingArrows(player);
				break;
			case 17011: //Steroids
				applySteroids(player);
				break;
			case 17013: //Cure
				applyCure(player);
				break;
			case 17015: //Heal
				applyHeal(player);
				break;
			case 17017: //Multi Flaming Arrows
				if (otherPlayer != null) {
					otherPlayer.getUpdateFlags().sendForceMessage("Flame squaaaaaad!");
					applyFlamingArrows(otherPlayer);
				}
				applyFlamingArrows(player);
				break;
			case 17019: //Multi Steroids
				if (otherPlayer != null) {
					otherPlayer.getUpdateFlags().sendForceMessage("Leeeeeroy!");
					applySteroids(otherPlayer);
				}
				applySteroids(player);
				break;
			case 17021: //Multi Cure
				if (otherPlayer != null) {
					otherPlayer.getUpdateFlags().sendForceMessage("Gotta love dem cures from " + player.getUsername() + ".");
					applyCure(otherPlayer);
				}
				applyCure(player);
				break;
			case 17023: //Multi Heal
				if (otherPlayer != null) {
					otherPlayer.getUpdateFlags().sendForceMessage("My niqqa " + player.getUsername() + ".");
					applyHeal(otherPlayer);
				}
				applyHeal(player);
				break;
		}
		player.getUpdateFlags().sendAnimation(spell.getAnimation());
		player.getUpdateFlags().sendGraphic(spell.getGfx());
	}

	/**
	 * Checks if player can cast spell
	 * @param player - The player
	 * @param spell - The spell
	 */
	public static boolean canCastSpell(Player player, Spell spell) {
		/*
		 * Check Magic Level
		 */
		if (player.getSkillLevel(Skill.MAGIC) < spell.getMageLevelReq()) {
			player.getActionSender().sendMessage("You need a Magic level of "+spell.getMageLevelReq()+" or higher to use this spell.");
			return false;
		}
		
		/*
		 * Check Prayer Level
		 */
		if (player.getSkillLevel(Skill.PRAYER) < spell.getPrayerLevelReq()) {
			player.getActionSender().sendMessage("You need a Prayer level of "+spell.getPrayerLevelReq()+" or higher to use this spell.");
			return false;
		}
		
		/*
		 * Check player has runes
		 */
		int[] reqRunes = spell.getRunesReq();
		int[] reqRuneAmounts = spell.getRuneAmounts();
		for (int i=0; i<reqRunes.length; i++) {
			if (!player.getInventory().getItemContainer().contains(reqRunes[i], reqRuneAmounts[i])) {
				player.getActionSender().sendMessage("You do not have the required runes to cast this spell.");
				return false;
			}
		}

		/**
		 * Check spells with timers
		 */
		switch (spell.getId()) {
			case 17009:
			case 17017:
				if (player.usingFlamingArrows()) {
					player.getActionSender().sendMessage("You must wait until your flaming arrows wear off before using this spell again!");
					return false;
				}
				break;
			case 17011:
			case 17019:
				if (player.usingSteroids()) {
					player.getActionSender().sendMessage("You must wait until your steroids wear off before using this spell again.");
					return false;
				}
				break;
			case 17013:
			case 17021:
				if (player.getCureOnCool()) {
					player.getActionSender().sendMessage("You must wait at least 5 minutes before using this spell again.");
					return false;
				}
				break;
			case 17015:
			case 17023:
				if (player.getHealOnCool()) {
					player.getActionSender().sendMessage("You must wait at least 7 minutes before using this spell again.");
					return false;
				}
				break;
		} 
		return true;
	}

	/**
	 * Removes runes needed to cast a spell.
	 * @param player The player casting the spell.
	 * @param spell The spell being cast.
	 */
	private void removeRunes(Player player, Spell spell) {
		int[] reqRunes = spell.getRunesReq();
		int[] reqRuneAmounts = spell.getRuneAmounts();
		for (int i=0; i<reqRunes.length; i++) {
			player.getInventory().removeItem(new Item(reqRunes[i], reqRuneAmounts[i]));
		}
	}

	/**
	 * Removes prayer points needed to cast a spell.
	 * @param player The player casting the spell.
	 * @param spell The spell being cast.
	 */
	private void drainPrayer(Player player, Spell spell) {
		player.getSkill().setPrayerPoints(player.getSkill().getPrayerPoints() - spell.getPrayerLevelReq());
		player.getSkill().refresh(5);
	}
}
