package com.exorth.rs2.content.skills.magic;

import java.util.HashMap;

/**
 * Handles the enhancement spells of Exorth
 * @author Stephen
 * @author UnitHAF
 */

public enum Spell {

	/**
	 * Flaming Arrows
	 */
	FLAMING_ARROWS(17009, 70, 15, new int[] {554, 561, 566}, new int[] {5, 2, 2}, 500, 1818, 343, "Let fire rain upon them!"),

	/**
	 * Steroids
	 */
	STEROIDS(17011, 70, 15, new int[] {555, 561, 566}, new int[] {5, 2, 2}, 500, 1746, 247, "WOOHOO! That's the stuff!"),

	/**
	 * Cure
	 */
	CURE(17013, 50, 10, new int[] {566, 561}, new int[] {5, 10}, 250, 1818, 568, "I got better."),

	/**
	 * Heal
	 */
	HEAL(17015, 80, 25, new int[] {566, 565}, new int[] {15, 15}, 1000, 1818, 246, "My health feels replenished."),

	/**
	 * Multi Flaming Arrows
	 */
	MULTI_FLAMING_ARROWS(17017, 90, 30, new int[] {554, 561, 566}, new int[] {15, 6, 6}, 1000, 1818, 343, "Let's give 'em hell boys!"),

	/**
	 * Multi Steroids
	 */
	MULTI_STEROIDS(17019, 90, 30, new int[] {555, 561, 566}, new int[] {15, 6, 6}, 1000, 1746, 247, "WOOHOO! That's the stuff!"),

	/**
	 * Multi Cure
	 */
	MULTI_CURE(17021, 70, 20, new int[] {566, 561}, new int[] {10, 20}, 500, 1818, 568, "By the power of Saradomin, let these illnesses be expelled!"),

	/**
	 * Multi Heal
	 */
	MULTI_HEAL(17023, 95, 50, new int[] {566, 565}, new int[] {30, 30}, 1500, 1818, 246, "Time to get back at it!");

	/**
	 * The action button id.
	 */
	private int id;

	/**
	 * The magic level required.
	 */
	private int mageLevelReq;

	/**
	 * The prayer drain.
	 */
	private int prayerLevelReq;

	/**
	 * The runes required.
	 */
	private int[] runesReq;

	/**
	 * The amount of runes required.
	 */
	private int[] runeAmounts;

	/**
	 * The xp rewarded.
	 */
	private int xpRewarded;

	/**
	 * The cast animation.
	 */
	private int animation;

	/**
	 * The cast graphic.
	 */
	private int gfx;

	/**
	 * The message on cast.
	 */
	private String castMessage;

	/**
	 * Constructor for spell
	 * @param id The action button id
	 * @param mageLevelReq The magic level required to cast the spell
	 * @param prayerDrain The amount the spell drains from prayer
	 * @param runesReq The required runes to cast the spell
	 * @param runeAmounts The amounts of runes required to cast
	 * @param xpRewarded The xp rewarded
	 * @param animation The animation played on cast
	 * @param gfx The gfx played on cast
	 */
	Spell(int id, int mageLevelReq, int prayerLevelReq, int[] runesReq, int[] runeAmounts, int xpRewarded, int animation, int gfx, String castMessage) {
		this.id = id;
		this.mageLevelReq = mageLevelReq;
		this.prayerLevelReq = prayerLevelReq;
		this.runesReq = runesReq;
		this.runeAmounts = runeAmounts;
		this.xpRewarded = xpRewarded;
		this.animation = animation;
		this.gfx = gfx;
		this.castMessage = castMessage;
	}

	/**
	 * A hashmap of all the spells.
	 */
	private static HashMap<Integer, Spell> spells = new HashMap<Integer, Spell>();

	/**
	 * Populate the spells hashmap.
	 */
	static {
		for (Spell spell : Spell.values()) {
			spells.put(spell.id, spell);
		}
	}

	/**
	 * Gets Spells for button ID
	 * @param buttonid
	 * @return Spells or null if does not exist
	 */

	public static Spell forID(int id) {
		return spells.get(id);
	}

	public int getId() {
		return id;
	}

	public int getMageLevelReq() {
		return mageLevelReq;
	}

	public int getPrayerLevelReq() {
		return prayerLevelReq;
	}

	public int[] getRunesReq() {
		return runesReq;
	}

	public int[] getRuneAmounts() {
		return runeAmounts;
	}

	public int getAnimation() {
		return animation;
	}

	public int getGfx() {
		return gfx;
	}

	public int getXpRewarded() {
		return xpRewarded;
	}

	public String getCastMessage() {
		return castMessage;
	}
}
