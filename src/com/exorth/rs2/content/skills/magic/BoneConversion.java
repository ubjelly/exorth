package com.exorth.rs2.content.skills.magic;

import java.util.HashMap;
import java.util.Map;

import com.exorth.rs2.content.combat.magic.Magic;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;

/**
 * Handles the conversion of bones to bananas or peaches, depending on the spell
 * the player casts.
 * 
 * @author Joshua Barry
 * @author AkZu
 * 
 */
public class BoneConversion {

	private static int[] convertables = { 526, 532, 6904, 6905, 6906, 6907 };

	/**
	 * A mapping of the items we currently poses.
	 */
	private static Map<Integer, Integer> bones = new HashMap<Integer, Integer>();

	/**
	 * 
	 * @param player The player who converts the bones.
	 * @param spellId The spell which determines the product.
	 */
	public static void convert(final Player player, int spellId) {
		for (int bone : convertables) {
			if (player.getInventory().getItemContainer().contains(bone)) {
				bones.put(bone, player.getInventory().getItemContainer().getCount(bone));
			}
		}

		if (bones.size() < 1) {
			player.getActionSender().sendMessage("You don't have any bones!");
			return;
		}

		int product = 0;

		switch (spellId) {
			case 1159:
				if (!Magic.getInstance().removeRunes(player, new int[]{ -1, 557, 2, 555, 2, 561, 1 })) {
					return;
				}
				product = 1963;
				break;
			case 15877:
				if (!Magic.getInstance().removeRunes(player, new int[]{ -1, 561, 2, 555, 4, 557, 4 })) {
					return;
				}
				product = 6883;
				break;
			default:
				return;
		}

		player.getUpdateFlags().sendAnimation(722);
		player.getUpdateFlags().sendGraphic(148, 0, 0);
		player.getActionSender().sendSound(204, 100, 0);
		player.getSkill().addExperience(Skill.MAGIC, spellId == 1159 ? 25 : 35.5);
		int amount = 0;
		for (Map.Entry<Integer, Integer> entry : bones.entrySet()) {
			int key = entry.getKey();
			int value = entry.getValue();
			player.getInventory().getItemContainer().remove(new Item(key, value));
			amount += value;
		}
		player.getInventory().getItemContainer().add(new Item(product, amount));
		player.getInventory().refresh();
		player.getActionSender().sendMessage("You turn all your held bones to " + (product == 6883 ? "peaches" : "bananas") + ".");
		bones.clear();
	}
}
