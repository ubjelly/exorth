package com.exorth.rs2.content.skills.magic;

import com.exorth.rs2.content.combat.magic.Magic;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.area.Areas;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Language;

/**
 * Handles everything related to teleporting
 * 
 * TODO: Combination runes
 * 
 * URGENT: Home teleport: Deny the teleport if walked/performed any actions.
 * 
 * @author AkZu & Jacob
 * 
 */

public class Teleportation {

	private static Teleportation instance = null;

	public static Teleportation getInstance() {
		if (instance == null) {
			instance = new Teleportation();
		}
		return instance;
	}

	// buttonId, x, y, z,
	// "size" = (Distance from coords, how big rectangle u can tele to)
	// if x and y is both 0 and size is 3, you can be teled to
	// -3x, -3y to +3x, +3y :P
	// spellbook required to use
	// lvl required to use
	// xp rewarded
	private static final Object[][] TELEPORTS = {
		{ 17041, 2966, 3382, 0, 0, 17000, 0, 0 }, // Falador
		{ 17043, 2596, 3408, 0, 0, 17000, 0, 0 }, // Fishing Guild
		{ 17045, 3103, 3249, 0, 0, 17000, 0, 0 }, // Draynor
		{ 17047, 2851, 2961, 0, 0, 17000, 0, 0 }, // Shilo
		{ 17049, 3087, 3492, 0, 0, 17000, 0, 0 }, // Edgeville
		{ 17051, 3213, 3424, 0, 0, 17000, 0, 0 }, // Varrock
		{ 17053, 1924, 4638, 0, 0, 17000, 0, 0 }, // Azerith
		{ 17055, 3363, 3304, 0, 0, 17000, 0, 0 } // Minigame
	};

	// supports for 4 items (runes) as for teleports only, combat spells will do xml perhaps
	private static int[][] RUNES_REQUIRED = {
		{ 17041, 0, 0, 0, 0 },
		{ 17043, 0, 0, 0, 0 }, 
		{ 17045, 0, 0, 0, 0 },
		{ 17047, 0, 0, 0, 0 },
		{ 17049, 0, 0, 0, 0 },
		{ 17051, 0, 0, 0, 0 },
		{ 17053, 0, 0, 0, 0 },
		{ 17055, 0, 0, 0, 0 } 
	};

	public void activateTeleportButton(final Player player, int buttonId, boolean isPremium) {
		// check for mass-clicking
		if (player.getAttribute("cant_teleport") != null) {
			return;
		}
		if (isPremium && !player.getPremium()) {
			player.getActionSender().sendMessage("This spell is for Premium Members only.");
		}
			
		if (player.getMinigame() != null) {
			player.getActionSender().sendMessage("You can't teleport whilst playing a minigame.");
			return;
		}

		if (player.getAttribute("teleBlocked") != null) {
			if (System.currentTimeMillis() - (Long) player.getAttribute("teleBlocked") < 300000) {
				player.getActionSender().sendMessage("You are teleport blocked and can't teleport!");
				return;
			} else {
				player.removeAttribute("teleBlocked");
			}
		}

		for (int i = 0; i < TELEPORTS.length; i++) {
			if (buttonId == (Integer) TELEPORTS[i][0]) {
				final int index = i;
				if (Areas.getWildernessLevel(player) > 20) {
					player.getActionSender().sendMessage(Language.CANT_TELE_20);
					return;
				}
				if (!Magic.getInstance().hasRequiredLevel(player, (Integer) TELEPORTS[index][6])) {
					return;
				}
				if (!Magic.getInstance().removeRunes(player, RUNES_REQUIRED[i])) {
					return;
				}
				
				player.getUpdateFlags().sendAnimation(714, 0);
				player.getUpdateFlags().sendGraphic(308, 50, 100);

				player.setAttribute("cant_teleport", 0);
				World.submit(new Task((2)) {
					@Override
					protected void execute() {
						player.clipTeleport((Integer) TELEPORTS[index][1], (Integer) TELEPORTS[index][2], (Integer) TELEPORTS[index][3], (Integer) TELEPORTS[index][4]);
						player.getUpdateFlags().sendAnimation(-1);
						this.stop();
					}
				});
				
				World.submit(new Task((5)) {
					@Override
					protected void execute() {
						player.removeAttribute("cant_teleport");
						this.stop();
					}
				});
				break;
			}
		}
	}
}
