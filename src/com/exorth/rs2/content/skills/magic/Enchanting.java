package com.exorth.rs2.content.skills.magic;

import java.util.HashMap;
import java.util.Map;

import com.exorth.rs2.content.combat.magic.Magic;
import com.exorth.rs2.content.mta.EnchantmentChamber;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Misc;

/**
 * 
 * @author AkZu
 * @author Joshua Barry
 * 
 */
public class Enchanting {

	public enum Enchant {
		SAPPHIRE_CYLINDER(6898, 6902, 7, 5, 719, 114, 207, 1),
		SAPPHIRE_CUBE(6899, 6902, 7, 5, 719, 114, 207, 1),
		SAPPHIRE_RING(1637, 2550, 7, 18, 719, 114, 207, 1),
		SAPPHIRE_AMULET(1694, 1727, 7, 18, 719, 114, 207, 1),
		SAPPHIRE_NECKLACE(1656, 3853, 7, 18, 719, 114, 207, 1),

		EMERALD_RING(1639, 2552, 27, 37, 719, 114, 207, 2),
		EMERALD_AMULET(1696, 1729, 27, 37, 719, 114, 207, 2),
		EMERALD_NECKLACE(1658, 5521, 27, 37, 719, 114, 207, 2),

		RUBY_RING(1641, 2568, 47, 59, 720, 115, 209, 3),
		RUBY_AMULET(1698, 1725, 47, 59, 720, 115, 209, 3),
		RUBY_NECKLACE(1660, 11194, 47, 59, 720, 115, 209, 3),

		DIAMOND_RING(1643, 2570, 57, 67, 720, 115, 209, 4),
		DIAMOND_AMULET(1700, 2570, 57, 67, 720, 115, 209, 4),
		DIAMOND_NECKLACE(1662, 11090, 57, 67, 720, 115, 209, 4),

		DRAGONSTONE_RING(1645, 2572, 68, 78, 721, 116, 208, 5),
		DRAGONSTONE_AMULET(1702, 1712, 68, 78, 721, 116, 208, 5),
		DRAGONSTONE_NECKLACE(1664, 11105, 68, 78, 721, 116, 208, 5),

		ONYX_RING(6575, 6583, 87, 97, 721, 452, 208, 6),
		ONYX_AMULET(6581, 6585, 87, 97, 721, 452, 208, 6),
		ONYX_NECKLACE(6577, 11128, 87, 97, 721, 452, 208, 6);

		int unenchanted, enchanted, levelReq, xpGiven, anim, gfx, sound, reqEnchantmentLevel;

		private Enchant(int unenchanted, int enchanted, int levelReq, int xpGiven, int anim, int gfx, int sound, int reqEnchantmentLevel) {
			this.unenchanted = unenchanted;
			this.enchanted = enchanted;
			this.levelReq = levelReq;
			this.xpGiven = xpGiven;
			this.anim = anim;
			this.gfx = gfx;
			this.sound = sound;
			this.reqEnchantmentLevel = reqEnchantmentLevel;
		}

		public int getUnenchanted() {
			return unenchanted;
		}

		public int getEnchanted() {
			return enchanted;
		}

		public int getLevelReq() {
			return levelReq;
		}

		public int getXp() {
			return xpGiven;
		}

		public int getAnim() {
			return anim;
		}

		public int getGFX() {
			return gfx;
		}

		public int getSound() {
			return sound;
		}

		public int getELevel() {
			return reqEnchantmentLevel;
		}

		private static final Map<Integer, Enchant> enc = new HashMap<Integer, Enchant>();

		public static Enchant forId(int itemID) {
			return enc.get(itemID);
		}

		static {
			for (Enchant en : Enchant.values()) {
				enc.put(en.getUnenchanted(), en);
			}
		}
	}

	private enum EnchantSpell {
		/**
		 * This bit is useless at the moment since we're
		 * using custom spellbook that handles all enchantments
		 * in one spell :P
		 * ~ Stephen
		 */
		SAPPHIRE(1155, new int[] { -1, 555, 1, 564, 1 }),
		EMERALD(1165, new int[] { -1, 556, 3, 564, 1 }),
		RUBY(1176, new int[] { -1, 554, 5, 564, 1 }),
		DIAMOND(1180, new int[] { -1, 557, 10, 564, 1 }),
		DRAGONSTONE(1187, new int[] { -1, 555, 15, 557, 15, 564, 1 }),
		ONYX(6003, new int[] { -1, 557, 20, 554, 20, 564, 1 });

		int spell;
		int[] reqRunes;

		private EnchantSpell(int spell, int[] reqRunes) {
			this.spell = spell;
			this.reqRunes = reqRunes;
		}

		public int getSpell() {
			return spell;
		}

		public int[] getRuneReqs() {
			return reqRunes;
		}

		public static final Map<Integer, EnchantSpell> spells = new HashMap<Integer, EnchantSpell>();

		public static EnchantSpell forId(int id) {
			return spells.get(id);
		}

		static {
			for (EnchantSpell en : EnchantSpell.values()) {
				spells.put(en.getSpell(), en);
			}
		}
	}

	private static int getEnchantmentLevel(int spellID) {
		switch (spellID) {
			case 1155: // Lvl-1 enchant sapphire
				return 1;
			case 1165: // Lvl-2 enchant emerald
				return 2;
			case 1176: // Lvl-3 enchant ruby
				return 3;
			case 1180: // Lvl-4 enchant diamond
				return 4;
			case 1187: // Lvl-5 enchant dragonstone
				return 5;
			case 6003: // Lvl-6 enchant onyx
				return 6;
		}
		return 0;
	}

	private static String getEnchantmentName(int spellID) {
		switch (spellID) {
			case 1155: // Lvl-1 enchant sapphire
				return "Sapphire";
			case 1165: // Lvl-2 enchant emerald
				return "Emerald";
			case 1176: // Lvl-3 enchant ruby
				return "Ruby";
			case 1180: // Lvl-4 enchant diamond
				return "Diamond";
			case 1187: // Lvl-5 enchant dragonstone
				return "Dragonstone";
			case 6003: // Lvl-6 enchant onyx
				return "Onyx";
		}
		return null;
	}

	public static void handleEnchanting(final Player player, int spellId, int slot) {
		final Item item = player.getInventory().get(slot);

		if (item == null) {
			return;
		}

		if (!(Boolean) player.getAttribute("canCastMagicOnItems")) {
			return;
		}

		Enchant enchant = Enchant.forId(item.getId());
		EnchantSpell spell = EnchantSpell.forId(spellId);

		if (enchant == null || spell == null) {
			return;
		}

		if (player.getSkill().getLevel()[Skill.MAGIC] < enchant.getLevelReq()) {
			player.getActionSender().sendMessage("You need a Magic level of " + enchant.getLevelReq() + " to cast this spell.");
			return;
		}

		if (getEnchantmentLevel(spellId) != enchant.getELevel()) {
			player.getActionSender().sendMessage("You can only enchant " + getEnchantmentName(spellId) + " jewelry with this spell.");
			return;
		}

		if (!Magic.getInstance().removeRunes(player, new int[] {557, 20, 554, 20, 564, 1})) {
			return;
		}

		player.setAttribute("canCastMagicOnItems", false);

		player.getUpdateFlags().sendAnimation(enchant.getAnim());
		player.getActionSender().sendSound(enchant.getSound(), 100, 0);
		player.getUpdateFlags().sendGraphic(enchant.getGFX(), 0, 100);

		player.getInventory().removeItem(item);
		player.getInventory().addItem(new Item(enchant.getEnchanted(), 1));
		player.getSkill().addExperience(Skill.MAGIC, enchant.getXp());
		String itemName = ItemManager.getInstance().getItemName(enchant.getEnchanted());
		String prefix = Misc.getArticle(itemName);
		player.getActionSender().sendMessage("You enchant your " + ItemManager.getInstance().getItemName(enchant.getUnenchanted()) + " and receive" + prefix + itemName + ".");

		/*
		 * Mage training arena related below.
		 */
		for (int enchantedItem : new int[]{ 6898, 6899, 6900, 6901 })
			if (enchantedItem == item.getId()) {
				int points = 1;
				if (player.getAttribute("enchants") == null) {
					player.setAttribute("enchants", 1);
				} else {
					if (((Integer) player.getAttribute("enchants")) >= 9) {
						// reward a bonus on the 10th enchant.
						points = enchant.getLevelReq();
						player.removeAttribute("enchants");
					} else {
						player.setAttribute("enchants", ((Integer) player.getAttribute("enchants")) + 1);
					}
				}

				if (item.getId() == EnchantmentChamber.getBonusId()) {
					points += 1;
					player.getActionSender().sendMessage("You get 1 bonus point!");
				}

				player.setEnchantPizazz(player.getEnchantPizazz() + points);
				player.getActionSender().sendString(Integer.toString(player.getEnchantPizazz()), 15921);
			}

		World.submit(new Task(1) {
			@Override
			protected void execute() {
				player.setAttribute("canCastMagicOnItems", true);
				stop();
			}
		});
	}
}
