package com.exorth.rs2.content.skills.magic;

import com.exorth.rs2.content.combat.magic.Magic;
import com.exorth.rs2.content.mta.AlchemistPlayground;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Language;
import com.exorth.rs2.util.Misc;

/**
 * Pretty much done except you have to gather more alchemy prices!
 * 
 * @author AkZu
 * @author Joshua Barry
 * 
 */
public class Alchemy {

	private static final int[] LOW_ALCHEMY_RUNES = { -1, 554, 3, 561, 1 };
	private static final int[] HIGH_ALCHEMY_RUNES = { -1, 554, 5, 561, 1 };

	/**
	 * 
	 * @param player
	 * @param spellId
	 * @param slot
	 */
	public static void alchemise(final Player player, int spellId, int slot) {
		final Item item = player.getInventory().get(slot);

		if (item == null) {
			return;
		}

		boolean isDummyItem = item.getId() >= 6893 && item.getId() <= 6897;
		int coinId = isDummyItem ? 8890 : 995;

		if (item.getId() == coinId) {
			player.getActionSender().sendMessage("You can't cast this spell on coins.");
			return;
		}

		if (!(Boolean) player.getAttribute("canCastMagicOnItems")) {
			return;
		}

		// TODO: High alchemy
		if (spellId == 1178 || spellId == 17007) {
			if (player.getSkill().getLevel()[Skill.MAGIC] < 55) {
				player.getActionSender().sendMessage("You need a Magic level of 55 to cast this spell.");
				return;
			}

			if (!player.getInventory().hasRoomFor(new Item(coinId, (item.getDefinition().isNote() ? ItemManager.getInstance().getItemDefinitions()[item.getDefinition().getParentId()].getHighAlchValue() : isDummyItem ? getMtaValue(item.getId()) : item.getDefinition().getHighAlchValue())))) {
				player.getActionSender().sendMessage(Language.NO_SPACE);
				return;
			}

			if (item.getId() == AlchemistPlayground.getBonusId()) {
				player.getActionSender().sendMessage("Item converted for free!");
			} else if (!Magic.getInstance().removeRunes(player, HIGH_ALCHEMY_RUNES)) {
				return;
			}

			player.getUpdateFlags().sendAnimation(713);
			player.getUpdateFlags().sendGraphic(113, 0, 100);
			player.getActionSender().sendSound(223, 100, 0);

			player.setAttribute("canCastMagicOnItems", false);

			if (player.getInventory().get(slot).getCount() > 1) {
				player.getInventory().get(slot).setCount(player.getInventory().get(slot).getCount() - 1);
				player.getInventory().refresh(slot, player.getInventory().get(slot));
			} else {
				player.getInventory().removeItemSlot(slot);
			}

			player.getInventory().addItem(new Item(coinId, (item.getDefinition().isNote() ? ItemManager.getInstance().getItemDefinitions()[item.getDefinition().getParentId()].getHighAlchValue() : isDummyItem ? getMtaValue(item.getId()) : item.getDefinition().getHighAlchValue())));

			World.submit(new Task(4) {
				@Override
				protected void execute() {
					player.getSkill().addExperience(Skill.MAGIC, 65);
					player.setAttribute("canCastMagicOnItems", true);
					stop();
				}
			});
		}

		// TODO: Low alchemy
		if (spellId == 1162) {
			if (player.getSkill().getLevel()[Skill.MAGIC] < 21) {
				player.getActionSender().sendMessage("You need a magic level of 21 to cast this spell.");
				return;
			}

			if (!player.getInventory().hasRoomFor(new Item(coinId, (item.getDefinition().isNote() ? ItemManager.getInstance().getItemDefinitions()[item.getDefinition().getParentId()].getLowAlchValue() : isDummyItem ? getMtaValue(item.getId()) : item.getDefinition().getLowAlchValue())))) {
				player.getActionSender().sendMessage(Language.NO_SPACE);
				return;
			}

			if (!Magic.getInstance().removeRunes(player, LOW_ALCHEMY_RUNES)) {
				return;
			}

			player.getUpdateFlags().sendAnimation(712);
			player.getUpdateFlags().sendGraphic(112, 0, 100);
			player.getActionSender().sendSound(224, 100, 0);

			player.setAttribute("canCastMagicOnItems", false);

			if (player.getInventory().get(slot).getCount() > 1) {
				player.getInventory().get(slot).setCount(player.getInventory().get(slot).getCount() - 1);
				player.getInventory().refresh(slot, player.getInventory().get(slot));
			} else {
				player.getInventory().removeItemSlot(slot);
			}

			player.getInventory().addItem(new Item(coinId, (item.getDefinition().isNote() ? ItemManager.getInstance().getItemDefinitions()[item.getDefinition().getParentId()].getLowAlchValue() : isDummyItem ? getMtaValue(item.getId()) : item.getDefinition().getLowAlchValue())));

			World.submit(new Task(1) {
				@Override
				protected void execute() {
					player.getSkill().addExperience(Skill.MAGIC, 31);
					player.setAttribute("canCastMagicOnItems", true);
					stop();
				}
			});
		}
	}

	private static int getMtaValue(int id) {
		return Misc.random(30);
	}
}
