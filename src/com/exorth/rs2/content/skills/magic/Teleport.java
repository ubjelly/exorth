package com.exorth.rs2.content.skills.magic;

import com.exorth.rs2.action.Action;
import com.exorth.rs2.action.impl.TeleportationAction;
import com.exorth.rs2.action.impl.TeleportationAction.TeleportType;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.Position;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Teleport {

	/* Kingdoms */
	public static final Position VARROCK = new Position(3212, 3423, 0);
	public static final Position LUMBRIDGE = new Position(3224, 3219, 0);
	public static final Position CAMELOT = new Position(2757, 3477, 0);
	public static final Position ARDOUGNE = new Position(2662, 3306, 0);
	public static final Position FIGHT_PITS = new Position(2399, 5178, 0);
	public static final Position DUEL_ARENA = new Position(3367, 3268, 0);

	/* Skilling Areas */
	public static final Position DRAYNOR = new Position(3079, 3251, 0);
	public static final Position CATHERBY = new Position(2804, 3434, 0);

	/* Wilderness Areas */
	public static final Position VARROCK_WILDERNESS = new Position(3250, 3515, 0);
	public static final Position EDGEVILLE_WILDERNESS = new Position(3087, 3516, 0);

	/**
	 * 
	 * @param entity
	 * @param type
	 * @param position
	 * @param offset
	 */
	public static void initiate(Entity entity, TeleportType type, Position position, int offset) {
		Action action = new TeleportationAction(entity, type, position, offset);
		entity.getActionDeque().addAction(action);
	}

	public static void initiate(Entity entity, Entity caster, TeleportType type, Position position, int offset) {
		Action action = new TeleportationAction(entity, caster, type, position, offset);
		entity.getActionDeque().addAction(action);
	}
}
