package com.exorth.rs2.content.skills.firemaking;

import java.util.HashMap;
import java.util.Map;

/**
 * Contains data relevant to logs the player is capable of burning.
 * 
 * 
 * To make pyre logs, players need sacred oil, created during the Shades of
 * Mort'ton Quest. Creating sacred oil necessitates the purchase of olive oil
 * from Razmire Keelgan in Mort'ton, and necessitates the rebuild and
 * restoration of the temple in the Shades of Mort'ton quest. Upon restoring the
 * temple, players can buy Olive oil and ignite a fire in the centre of the
 * temple with it, converting the olive oil into sacred oil.
 * 
 * The pyre log itself results from the combination of the sacred oil and a log.
 * Once a player obtains a log and sacred oil, they can use the sacred oil with
 * the log, which will turn it into a pyre log. Players can then light the pyre
 * logs on the funeral pyres to the eastside of Mort'ton. However, players also
 * need to have Shade remains, the remains from dead shades of Mort'on. Only at
 * the funeral pyres can the player ignite pyre logs. Burning pyre logs aids
 * players in obtaining the key to a chest which contains fine cloth, useful for
 * Splitbark equipment.
 * 
 * After completing the Legacy of Seergaze quest, players can burn Vyrewatch
 * remains in the Paterdomus Columbarium with pyre logs. Players can only burn
 * Vyrewatch corpses with teak pyre logs or better.
 * 
 * @author Joshua Barry
 * 
 */
public enum LogType
{

	NORMAL_LOGS(1511, 1, 40),
	ACHEY_LOGS(2862, 1, 40),
	OAK_LOGS(1521, 15, 60),
	WILLOW_LOGS(1519, 30, 90),
	TEAK_LOGS(6333, 35, 105),
	ARCTIC_PINE_LOGS(10810, 42, 125),
	MAPLE_LOGS(1517, 45, 135),
	MAHOGANY_LOGS(6332, 50, 157.5),
	YEW_LOGS(1515, 60, 202.5), 
	MAGIC_LOGS(1513, 75, 303.8),

	PYRE_LOGS(3438, 5, 50.5), // Sacred oil drops: 2
	OAK_PYRE_LOGS(3440, 20, 70), // Sacred oil drops: 2
	WILLOW_PYRE_LOGS(3442, 35, 100), // Sacred oil drops: 3
	TEAK_PYRE_LOGS(6211, 40, 120), // Sacred oil drops: 3
	ARCTIC_PYRE_LOGS(10808, 47, 158), // Sacred oil drops: 3
	MAPLE_PYRE_LOGS(3444, 50, 175), // Sacred oil drops: 3
	MAHOGANY_PYRE_LOGS(6213, 55, 210), // Sacred oil drops: 3
	YEW_PYRE_LOGS(3446, 65, 225), // Sacred oil drops: 4
	MAGIC_PYRE_LOGS(3448, 80, 404.5), // Sacred oil drops: 4

	RED_LOGS(7404, 1, 40),
	GREEN_LOGS(7405, 1, 40),
	BLUE_LOGS(7406, 1, 40),
	WHITE_LOGS(10328, 1, 40),
	PURPLE_LOGS(10329, 1, 40);

	/**
	 * The item id.
	 */
	private int item;

	/**
	 * The level requirement.
	 */
	private int levelRequirement;

	/**
	 * The gained experience.
	 */
	private double experience;

	private static Map<Integer, LogType> logs = new HashMap<Integer, LogType>();

	/**
	 * 
	 * @param item The item that represents logs.
	 * @param levelRequirement The level required to burn the logs.
	 * @param experience The experience gained from burning the logs.
	 */
	private LogType(int itemId, int levelRequirement, double experience) {
		this.item = itemId;
		this.levelRequirement = levelRequirement;
		this.experience = experience;
	}

	public int getId() {
		return item;
	}

	public int getLevelRequirement() {
		return levelRequirement;
	}

	public double getExperience() {
		return experience;
	}

	public static LogType forId(int id) {
		return logs.get(id);
	}

	static {
		for (LogType log : LogType.values()) {
			logs.put(log.getId(), log);
		}
	}
}
