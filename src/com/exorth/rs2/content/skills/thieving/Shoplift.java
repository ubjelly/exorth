package com.exorth.rs2.content.skills.thieving;

import com.exorth.rs2.action.Action;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.object.CustomObject;
import com.exorth.rs2.model.players.GlobalObjectHandler;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Language;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.clip.ObjectDefinition;
import com.exorth.rs2.util.clip.RSObject;

/**
 * The event in which an entity steals from a stall.
 * 
 * @author Joshua Barry
 * 
 */
public class Shoplift extends Action {

	/**
	 * The player who is stealing.
	 */
	private Player player;

	/**
	 * The dummy object that represents an 'empty' version of the stall after we
	 * swipe the items.
	 */
	private CustomObject dummy;

	/**
	 * The object in it's restored state.
	 */
	private CustomObject restored;

	/**
	 * The definitions of the object we are stealing from.
	 */
	private ObjectDefinition definition;

	/**
	 * The type of stall we are stealing from.
	 */
	private StallType stall;

	/**
	 * 
	 * @param player The acting player.
	 * @param object The object associated with the clean stall.
	 * @param ticks The amount of ticks before executing the service.
	 */
	public Shoplift(Player player, RSObject object, int ticks) {
		super(player, ticks);
		this.player = player;
		this.dummy = new CustomObject(getReplaceobject(object), object.getPosition(), object.getFace(), object.getType());
		this.restored = new CustomObject(object.getId(), object.getPosition(), object.getFace(), object.getType());
		this.stall = StallType.forId(object.getId());
		this.definition = ObjectDefinition.forId(object.getId());
	}

	private int getReplaceobject(RSObject object) {
		if (object.getId() == 4877) {
			return 620;
		}
		return 634;
	}

	@Override
	public CancelPolicy getCancelPolicy() {
		return CancelPolicy.ALWAYS;
	}

	@Override
	public StackPolicy getStackPolicy() {
		return StackPolicy.NEVER;
	}

	@Override
	public AnimationPolicy getAnimationPolicy() {
		return AnimationPolicy.RESET_ALL;
	}

	@Override
	public void execute() {
		if (!allow()) {
			this.stop();
			return;
		}

		final int randomId = getReward();
		int amount = getAmount();
		final String itemName = ItemManager.getInstance().getItemName(randomId);
		final String prefix = "" + amount;
		final String stallName = definition.getName().toLowerCase();
		final Item item = new Item(randomId, amount);

		player.getUpdateFlags().sendAnimation(0x340);
		player.getActionSender().sendMessage("You attempt to steal from the " + stallName + "...");

		World.submit(new Task(2) {
			@Override
			protected void execute() {
				GlobalObjectHandler.createGlobalObject(player, dummy);

				if (player.getInventory().addItem(item)) {
					player.getActionSender().sendMessage("You steal " + prefix + " " + itemName + " from the " + stallName + ".");
					player.getSkill().addExperience(Skill.THIEVING, stall.getExperience());
					World.submit(new Task(stall.getRespawnTime()) {
						@Override
						protected void execute() {
							GlobalObjectHandler.createGlobalObject(player, restored);
							this.stop();
						}
					});
				}
				this.stop();
			}
		});
		this.stop();
	}

	private int getReward() {
		if (stall.getObjectId() == 2564) {
			double[] chances = { 100, 55, 35, 20, 10, 5 };
			int[] herbs = {199, 203, 207, 209, 213, 215 };
			double roll = Math.random() * 100;
			int item = -1;
			for (int i2 = 0; i2 < herbs.length; i2++) {
				if (roll < chances[i2]) {
					item = herbs[i2];
				}
			}
			return item;
		}
		if (stall.getObjectId() == 2562) {
			double[] chances = { 100, 60, 30, 5, 0.1, 0.05 };
			int[] gems = {1623, 1621, 1619, 1617, 1631, 6571 };
			double roll = Math.random() * 100;
			int item = -1;
			for (int i2 = 0; i2 < gems.length; i2++) {
				if (roll < chances[i2]) {
					item = gems[i2];
				}
			}
			return item;
		}
		return Misc.generatedValue(stall.getRewards());
	}

	private int getAmount() {
		if (stall.getObjectId() == 2563) {
			return 1 + Misc.random(19);
		}
		return 1;
	}

	/**
	 * Determines whether the player is allowed to steal or not.
	 * 
	 * @return
	 */
	private boolean allow() {
		if (!player.getPremium() && stall.getPremium()) {
			player.getActionSender().sendMessage("You must be a Premium Member to steal from here!");
			return false;
		}
		if (player.getSkill().getLevel()[Skill.THIEVING] < stall.getLevelRequirement()) {
			player.getActionSender().sendMessage("You need a " + Skill.getSkillName(Skill.THIEVING) + " level of " + stall.getLevelRequirement() + " to steal from this stall.");
			return false;
		} else if (player.getInventory().getItemContainer().freeSlots() < 1) {
			player.getActionSender().sendMessage(Language.NO_SPACE);
			return false;
		} else if (player.getAttribute("isStunned") != null) {
			player.getActionSender().sendMessage("You're stunned!");
			return false;
		}
		return true;
	}
}
