package com.exorth.rs2.content.skills.thieving;

import java.util.HashMap;
import java.util.Map;

/**
 * An enumeration of stalls that the player can steal from.
 * 
 * @author Joshua Barry
 * 
 */
public enum StallType {

	BAKERY_STALL(false, 2561, 10, 6, 80, 2309, 1891, 1901),
	FUR_STALL(false, 2563, 1, 6, 60, 314), 
	MAGIC_STALL(false, 4877, 40, 80, 200, 554, 555, 557, 556),
	SPICE_STALL(true, 2564, 80, 50, 500, 249, 253, 257, 259, 263, 265), 
	GEM_STALL(false, 2562, 92, 200, 1000, 1617, 1619, 1621, 1623, 1631, 6571);

	private int objectId;
	private int levelRequirement;
	private int respawnTime;
	private double experience;
	private Integer[] rewards;
	private boolean isPremium;

	private static Map<Integer, StallType> stalls = new HashMap<Integer, StallType>();

	/**
	 * 
	 * @param objectId
	 * @param levelRequired
	 * @param respawnTime
	 * @param experience
	 * @param rewards
	 */
	private StallType(boolean isPremium, int objectId, int levelRequired, int respawnTime, double experience, Integer... rewards) {
		this.objectId = objectId;
		this.levelRequirement = levelRequired;
		this.respawnTime = respawnTime;
		this.experience = experience;
		this.rewards = rewards;
		this.isPremium = isPremium;
	}

	public int getObjectId() {
		return objectId;
	}

	public int getLevelRequirement() {
		return levelRequirement;
	}

	public int getRespawnTime() {
		return respawnTime;
	}

	public double getExperience() {
		return experience;
	}

	public Integer[] getRewards() {
		return rewards;
	}

	public boolean getPremium() {
		return isPremium;
	}

	static {
		for (StallType stall : StallType.values()) {
			stalls.put(stall.getObjectId(), stall);
		}
	}

	public static StallType forId(int objectId) {
		return stalls.get(objectId);
	}
}
