package com.exorth.rs2.content.skills.thieving;

import java.util.HashMap;
import java.util.Map;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.util.Misc;

/**
 * Contains all relevant data to thieving npc's.
 * 
 * @author Joshua Barry <Ares>
 * @author Dominick Jones <Jones>
 */

public enum ThieveableNpcs {

	/*
	 * TODO: Gnome Woman 169, Gnome Child 159,
	 */
	CIVILANS(new int[]{ 1, 2, 3, 4 }, 1, 8, new Item[]{ new Item(995, 3) }, 4, 1), 
	FARMERS(new int[]{ 7 }, 9, 14.5, new Item[]{ new Item(995, 9) }, new Item[]{ new Item(5318, 1) }, 5, 1),
	FEMALE_HAM_MEMBER(new int[]{ 1715 }, 15, 18.5, new Item[]{ new Item(995, 3) }, 4, 3),
	MALE_HAM_MEMBER(new int[]{ 1714 }, 20, 22.2, new Item[]{ new Item(995, 3) }, 4, 3),
	WARRIOR_WOMAN(new int[]{ 15 }, 25, 26, new Item[]{ new Item(995, 3) }, 5, 3),
	ROGUE(new int[]{ 187 }, 32, 36.5, new Item[]{ new Item(995, 3) }, 5, 3),
	CAVE_GOBLIN(new int[]{ 5752, 5753, 5754, 5755, 5756, 5757, 5758, 5759, 5760, 5761, 5762, 5763, 5764, 5765, 5766, 5767, 5768, 5769 }, 36, 40, new Item[]{ new Item(995, 3) }, 5, 3),
	MASTER_FARMER(new int[]{ 2234 }, 38, 43, new Item[]{ new Item(995, 3) }, 5, 3),
	GUARD(new int[]{ 9 }, 40, 46.8, new Item[]{ new Item(995, 3) }, 5, 3),
	FREMENNIK(new int[]{ 2462 }, 45, 65, new Item[]{ new Item(995, 3) }, 5, 3),
	BEARDED_POLLNIVNIAN_BANDIT(new int[]{ 1880 }, 45, 79.4, new Item[]{ new Item(995, 3) }, 5, 3),
	DESERT_BANDIT(new int[]{ /* Need The IDs */}, 53, 79.4, new Item[]{ new Item(995, 3) }, 4, 5),
	KNIGHTPOLLNIVNIAN_BANDIT(new int[]{ /* Need The IDs */}, 65, 137.5, new Item[]{ new Item(995, 3) }, 5, 3),
	WATCHMAN(new int[]{ /* Need The IDs */}, 55, 84.3, new Item[]{ new Item(995, 3) }, 5, 3),
	MENAPHITE_THUG(new int[]{ 1904, 1905 }, 65, 137.5, new Item[]{ new Item(995, 3) }, 5, 3),
	PALADIN(new int[]{ 2256 }, 75, 198.3, new Item[]{ new Item(995, 3) }, 5, 3),
	GNOME(new int[]{ 66, 67, 68 }, 80, 273.3, new Item[]{ new Item(995, Misc.randomMinMax(200, 300)) }, 6, 5),
	ELF(new int[]{ /* Need The IDs */}, 85, 353.3, new Item[]{ new Item(995, Misc.randomMinMax(280, 350)) }, 6, 5);

	/**
	 * The npc's id.
	 */
	private int[] npcId;

	/**
	 * The level needed to thieve.
	 */
	private int levelRequirement;

	/**
	 * The experience gained.
	 */
	private double expGain;

	/**
	 * The items looted.
	 */
	private Item[] normalLoot;

	/**
	 * The rare items looted.
	 */
	private Item[] rareLoot;

	/**
	 * [10:39:09 PM] Lunch Box: 4 sec stuns : All H.A.M. Members such as Man/Woman/Guard
	 * [10:39:18 PM] Lunch Box: 6 sec stuns: Hero, Elf
	 * [10:39:37 PM] Lunch Box: 5 sec stuns: Everyone else.
	 * 
	 * The stun timer.
	 */
	private int stunTimer;

	/**
	 * The stun damage.
	 */
	private int stunDamage;

	public static Map<Integer, ThieveableNpcs> thieveNpc = new HashMap<Integer, ThieveableNpcs>();

	public static ThieveableNpcs forId(int id) {
		return thieveNpc.get(id);
	}

	static {
		for (ThieveableNpcs npc : ThieveableNpcs.values()) {
			for (int npcId : npc.getNpcId()) {
				thieveNpc.put(npcId, npc);
			}
		}
	}

	private ThieveableNpcs(int[] npcId, int levelRequirement, double expGain, Item[] normalLoot, int stunTimer, int stunDamage) {
		this.npcId = npcId;
		this.levelRequirement = levelRequirement;
		this.expGain = expGain;
		this.normalLoot = normalLoot;
		this.stunTimer = stunTimer;
		this.stunDamage = stunDamage;
	}

	private ThieveableNpcs(int[] npcId, int levelRequirement, double expGain, Item[] normalLoot, Item[] rareLoot, int stunTimer, int stunDamage) {
		this.npcId = npcId;
		this.levelRequirement = levelRequirement;
		this.expGain = expGain;
		this.normalLoot = normalLoot;
		this.rareLoot = rareLoot;
		this.stunTimer = stunTimer;
		this.stunDamage = stunDamage;
	}

	/**
	 * Gets the npcId.
	 * 
	 * @return The npcId.
	 */
	public int[] getNpcId() {
		return npcId;
	}

	/**
	 * Gets the Level Requirement.
	 * 
	 * @return The level Requirement.
	 */
	public int getLevelRequirement() {
		return levelRequirement;
	}

	public double getExpGain() {
		return expGain;
	}

	public Item[] getNormalLoot() {
		return normalLoot;
	}

	public Item[] getRareLoot() {
		return rareLoot;
	}

	public int getStunTimer() {
		return stunTimer;
	}

	public int stunDamage() {
		return stunDamage;
	}
}
