package com.exorth.rs2.content.skills.herblore;

import java.util.HashMap;
import java.util.Map;

import com.exorth.rs2.model.items.Item;

/**
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum SecondaryIngredient {

	/**
	 * Eye of Newt - Attack Potion.
	 */
	EYE_OF_NEWT(1, 221, new Item(91, 1), 121, 3, 25),
	/**
	 * Unicorn Horn Dust - Antipoison.
	 */
	UNICORN_HORN_DUST(2, 235, new Item(93, 1), 175, 5, 37),
	/**
	 * Limpwurt Root - Strength Potion.
	 */
	LIMPWURT_ROOT(3, 225, new Item(95, 1), 115, 12, 50),
	/**
	 * Red Spider's Eggs - Restore Potion.
	 */
	RED_SPIDERS_EGGS(4, 223, new Item(97, 1), 127, 22, 62),
	/**
	 * Chocolate Dust - Energy Potion.
	 */
	CHOCOLATE_DUST(5, 1975, new Item(97, 1), 3010, 26, 67),
	/**
	 * White Berries - Defence Potion.
	 */
	WHITE_BERRIES(6, 239, new Item(99, 1), 133, 30, 75),
	/**
	 * Snape Grass - Prayer Potion.
	 */
	SNAPE_GRASS(7, 231, new Item(99, 1), 139, 38, 87),
	/**
	 * Eye of Newt - Super Attack Potion.
	 */
	EYE_OF_NEWT_2(8, 221, new Item(101, 1), 145, 45, 100),
	/**
	 * Mort Myre Fungi - Super Energy Potion.
	 */
	MORT_MYRE_FUNGI(9, 2970, new Item(103, 1), 3018, 52, 117),
	/**
	 * Limpwurt Root - Super Strength Potion.
	 */
	LIMPWURT_ROOT_2(10, 255, new Item(105, 1), 157, 55, 125),
	/**
	 * Red Spider's Eggs - Super Restore Potion.
	 */
	RED_SPIDERS_EGGS_2(11, 223, new Item(127, 1), 3026, 63, 142),
	/**
	 * White Berries - Super Defence Potion.
	 */
	WHITE_BERRIES_2(12, 239, new Item(107, 1), 163, 66, 150),
	/**
	 * Wine of Zamorak - Ranging Potion.
	 */
	WINE_OF_ZAMORAK(13, 245, new Item(109, 1), 169, 72, 162),
	/**
	 * Jangerberries - Zamorak Brew.
	 */
	JANGERBERRIES(14, 247, new Item(111, 1), 189, 78, 175),
	/**
	 * Crushed Bird Nest - Saradomin Brew.
	 */
	CRUSHED_BIRD_NEST(15, 6693, new Item(2998, 1), 6687, 81, 180);

	/**
	 * The id.
	 */
	private int id;

	/**
	 * The index.
	 */
	private int index;

	/**
	 * The requiredItem.
	 */
	private Item requiredItem;

	/**
	 * The reward.
	 */
	private int reward;

	/**
	 * The level.
	 */
	private int level;

	/**
	 * The experience.
	 */
	private int exp;

	/**
	 * A map of object ids to primary ingredients.
	 */
	private static Map<Integer, SecondaryIngredient> ingredients = new HashMap<Integer, SecondaryIngredient>();

	/**
	 * Populates the secondary ingredient map.
	 */
	static {
		for (SecondaryIngredient ingredient : SecondaryIngredient.values()) {
			ingredients.put(ingredient.index, ingredient);
		}
	}

	/**
	 * Gets a secondary ingredient by an item id.
	 * 
	 * @param item The item id.
	 * @return The SecondaryIngredient, or <code>null</code> if the object is not
	 *		 a SecondaryIngredient.
	 */
	public static SecondaryIngredient forId(int item) {
		return ingredients.get(item);
	}

	private SecondaryIngredient(int index, int id, Item requiredItem, int reward, int level, int exp) {
		this.index = index;
		this.id = id;
		this.requiredItem = requiredItem;
		this.level = level;
		this.reward = reward;
		this.exp = exp;
	}

	/**
	 * Gets the exp.
	 * 
	 * @return The exp.
	 */
	public int getExperience() {
		return exp;
	}

	/**
	 * Gets the id.
	 * 
	 * @return The id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the index.
	 * 
	 * @return The index.
	 */
	public int getIndex() {
		return index;
	}

	/**
	 * Gets the required id.
	 * 
	 * @return The required id.
	 */
	public Item getRequiredItem() {
		return requiredItem;
	}

	/**
	 * Gets the required level.
	 * 
	 * @return The required level.
	 */
	public int getRequiredLevel() {
		return level;
	}

	/**
	 * Gets the reward.
	 * 
	 * @return The reward.
	 */
	public int getReward() {
		return reward;
	}
}
