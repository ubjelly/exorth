package com.exorth.rs2.content.skills.herblore;

/**
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum IngredientType {

	PRIMARY_INGREDIENT, SECONDARY_INGREDIENT

}
