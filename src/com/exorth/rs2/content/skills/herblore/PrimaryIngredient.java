package com.exorth.rs2.content.skills.herblore;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Joshua Barry <Ares>
 * 
 */
public enum PrimaryIngredient {

	GUAM(249, 91, 3),
	MARRENTILL(251, 93, 5),
	TARROMIN(253, 95, 12),
	HARRALANDER(255, 97, 22),
	RANARR(257, 99, 30),
	IRIT(259, 101, 45),
	AVANTOE(261, 103, 50),
	KWUARM(263, 105, 55),
	CADANTINE(265, 107, 66),
	DWARF_WEED(267, 109, 72),
	TORSTOL(269, 111, 78),
	TOADFLAX(2998, 3002, 34),
	SNAPDRAGON(3000, 3004, 63);

	/**
	 * The id.
	 */
	private int id;

	/**
	 * The reward.
	 */
	private int reward;

	/**
	 * The level.
	 */
	private int level;

	/**
	 * A map of object ids to primary ingredients.
	 */
	private static Map<Integer, PrimaryIngredient> ingredients = new HashMap<Integer, PrimaryIngredient>();

	/**
	 * Populates the log map.
	 */
	static {
		for (PrimaryIngredient ingredient : PrimaryIngredient.values()) {
			ingredients.put(ingredient.id, ingredient);
		}
	}

	/**
	 * Gets a log by an item id.
	 * 
	 * @param item The item id.
	 * @return The PrimaryIngredient, or <code>null</code> if the object is not
	 *		 a PrimaryIngredient.
	 */
	public static PrimaryIngredient forId(int primaryIngredientId) {
		return ingredients.get(primaryIngredientId);
	}

	private PrimaryIngredient(int id, int reward, int level) {
		this.id = id;
		this.level = level;
		this.reward = reward;
	}

	/**
	 * Gets the id.
	 * 
	 * @return The id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the required level.
	 * 
	 * @return The required level.
	 */
	public int getRequiredLevel() {
		return level;
	}

	/**
	 * Gets the reward.
	 * 
	 * @return The reward.
	 */
	public int getReward() {
		return reward;
	}
}
