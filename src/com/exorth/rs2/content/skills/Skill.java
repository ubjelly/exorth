package com.exorth.rs2.content.skills;

import com.exorth.rs2.Constants;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Misc;

//TODO Chat interfaces/Level ups
public class Skill {

	private Player player;

	public static final int SKILL_COUNT = 22;
	public static final double MAXIMUM_EXP = 200000000;

	private byte[] level = new byte[SKILL_COUNT];
	private double[] exp = new double[SKILL_COUNT];

	/**
	 * Prayer points are stored in a double, so to avoid making every skill a
	 * double, we simply use prayerPoints.
	 */
	private double prayerPoints;

	public static final String[] SKILL_NAME = {
		"Attack", "Defence", "Strength", "Hitpoints", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting",
		"Fletching", "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblore", "Agility", "Thieving",
		"Slayer", "Farming", "Runecrafting", "Construction"
	};

	public static final short 
		ATTACK = 0, DEFENCE = 1, STRENGTH = 2, HITPOINTS = 3, RANGED = 4, PRAYER = 5, MAGIC = 6, COOKING = 7, WOODCUTTING = 8,
		FLETCHING = 9, FISHING = 10, FIREMAKING = 11, CRAFTING = 12, SMITHING = 13, MINING = 14, HERBLORE = 15, AGILITY = 16, THIEVING = 17,
		SLAYER = 18, FARMING = 19, RUNECRAFTING = 20, CONSTRUCTION = 21;

	public Skill(Player player) {
		this.player = player;
		for (int i = 0; i < level.length; i++) {
			level[i] = 1;
			exp[i] = 0;
		}
		level[3] = 10;
		exp[3] = 1154;
		prayerPoints = 1;
	}

	public int[][] CHAT_INTERFACES = {
		{ ATTACK, 6247, 6248, 6249 },
		{ DEFENCE, 6253, 6254, 6255 },
		{ STRENGTH, 6206, 2607, 2608 },
		{ HITPOINTS, 6216, 6217, 6218 },
		{ RANGED, 4443, 5453, 6114 },
		{ PRAYER, 6242, 6243, 6244 },
		{ MAGIC, 6211, 6212, 6213 },
		{ COOKING, 6226, 6227, 6228 },
		{ WOODCUTTING, 4272, 4273, 4274 },
		{ FLETCHING, 6231, 6232, 6233 },
		{ FISHING, 6258, 6259, 6260 },
		{ FIREMAKING, 4282, 4283, 4284 },
		{ CRAFTING, 6263, 6264, 6265 },
		{ SMITHING, 6221, 6222, 6223 },
		{ MINING, 4416, 4417, 4438 },
		{ HERBLORE, 6237, 6238, 6239 },
		{ AGILITY, 4277, 4278, 4279 },
		{ THIEVING, 4261, 4263, 4264 },
		{ SLAYER, 12122, 12123, 12124 },
		//{ FARMING, 4887, 4889, 4890 }, MISSING
		{ RUNECRAFTING, 4267, 4268, 4269 },
		{ CONSTRUCTION, 3442, 6248, 6249 }
	};

	//TODO: Add Combat Level refresh
	public void refresh() {
		for (byte i = 0; i < level.length; i++) {
			if (level[i] > 117) {
				level[i] = 118;
			} else if (level[i] < 1) {
				level[i] = 0;
			}
			if (i != PRAYER) {
				player.getActionSender().sendSkill(i, level[i], exp[i]);
			} else {
				if (prayerPoints <= 0.0) {
					prayerPoints = 0;
				}
				player.getActionSender().sendSkill(i, (int) getPrayerPoints(), exp[i]);
			}
		}
	}

	public void refresh(int skill) {
		if (level[skill] > 117) {
			level[skill] = 118;
		} else if (level[skill] < 1) {
			level[skill] = 0;
		}
		if (skill != PRAYER) {
			player.getActionSender().sendSkill(skill, level[skill], exp[skill]);
		} else {
			if (prayerPoints <= 0.0) {
				prayerPoints = 0;
			}
			player.getActionSender().sendSkill(skill, (int) getPrayerPoints(), exp[skill]);
		}
	}

	/**
	 * Determines the level achieved with <code>exp</code> experience in a level.
	 * 
	 * @param exp The experience we're interested in.
	 * @return lvl The level achieved.
	 */
	public byte getLevelForXP(double exp) {
		int points = 0;
		int output = 0;
		for (int lvl = 1; lvl <= 99; lvl++) {
			points += Math.floor(lvl + 300.0 * Math.pow(2.0, lvl / 7.0));
			output = (int) Math.floor(points / 4);
			if (output >= exp + 1) {
				return (byte) lvl;
			}
		}
		return 99;
	}

	/**
	 * Determines the XP required to achieve <code>level</code>.
	 * 
	 * @param level The Level we're interested in.
	 * @return xp The XP required.
	 */
	public int getXPForLevel(int level) {
		int points = 0;
		int output = 0;
		for (int lvl = 1; lvl <= level; lvl++) {
			points += Math.floor(lvl + 300.0 * Math.pow(2.0, lvl / 7.0));
			if (lvl >= level) {
				return output;
			}
			output = (int) Math.floor(points / 4);
		}
		return 0;
	}

	public int getTotalLevel() {
		int total = 0;
		for (int i = 0; i < level.length; i++) {
			total += getLevelForXP(i);
		}
		return total;
	}

	/**
	 * Adds experience.
	 * 
	 * @param skill the Skill
	 * @param xp The experience to add.
	 * 
	 * TODO Always use this method in LIVE game so we don't encourage bugs.
	 */
	public void addExperience(int skill, double xp) {
		// TODO: Easy?
		xp *= Constants.EXP_RATE[skill];
		xp *= Constants.GLOBAL_EXP_RATE;

		byte oldLevel = getLevelForXP(exp[skill]);
		exp[skill] += xp;
		if (exp[skill] > MAXIMUM_EXP) {
			exp[skill] = MAXIMUM_EXP;
		}
		byte newLevel = getLevelForXP(exp[skill]);
		byte levelDiff = (byte) (newLevel - oldLevel);
		if (levelDiff > 0) {
			level[skill] += levelDiff;
			player.getUpdateFlags().sendGraphic(199);
			player.setAppearanceUpdateRequired(true);
			sendLevelUpMessage(skill);
		}
		refresh(skill);
	}

	private void sendLevelUpMessage(int skill) {
		// TODO Fix a, an ? "an Attack level"
		// Your _Ranging_ level is now 99.
		String name = SKILL_NAME[skill];
		final String line1 = "Congratulations, you just advanced " + Misc.getArticle(name) + " " + name + " level.";
		final String line2 = "Your " + name + " level is now " + getLevelForXP(exp[skill]) + ".";
		player.getActionSender().sendMessage(line1);
		for (int[] chatData : CHAT_INTERFACES) {
			if (chatData[0] == skill) {
				if (skill != RANGED && skill != MINING && skill != THIEVING && skill != FARMING && skill != CONSTRUCTION) {
					player.getActionSender().sendString("@blu@" + line1, chatData[1] + 1);
					player.getActionSender().sendString(line2, chatData[1] + 2);
				} else {
					player.getActionSender().sendString("@blu@" + line1, chatData[2]);
					player.getActionSender().sendString(line2, chatData[3]);
				}
				player.getActionSender().sendChatInterface(chatData[1]);
			}
		}
	}

	public int getCombatLevel() {
		byte attack = getLevelForXP(exp[ATTACK]);
		byte defence = getLevelForXP(exp[DEFENCE]);
		byte strength = getLevelForXP(exp[STRENGTH]);
		byte hp = getLevelForXP(exp[HITPOINTS]);
		byte prayer = getLevelForXP(exp[PRAYER]);
		byte ranged = getLevelForXP(exp[RANGED]);
		byte magic = getLevelForXP(exp[MAGIC]);
		byte slayer = getLevelForXP(exp[SLAYER]);
		byte combatLevel = 3;
		combatLevel = (byte) (((defence + hp + Math.floor(prayer / 2)) * 0.2535) + 1);
		double melee = (attack + strength) * 0.325;
		double ranger = Math.floor(ranged * 1.5) * 0.325;
		double mage = Math.floor(magic * 1.5) * 0.325;
		if (melee >= ranger && melee >= mage) {
			combatLevel += melee;
		} else if (ranger >= melee && ranger >= mage) {
			combatLevel += ranger;
		} else if (mage >= melee && mage >= ranger) {
			combatLevel += mage;
		}
		if (player.getSkill().getLevel()[Skill.SLAYER] != 1) {
			double slayerAddition = (0.120 * slayer);
			combatLevel += (int) slayerAddition;
		}
		if (combatLevel == 127 && slayer == 1) { //Idk why it does this but cheapfix
			return 126;
		}
		if (combatLevel <= 138) {
			return combatLevel;
		} else {
			return 138;
		}
	}

	/**
	 * Increases skill level.
	 * 
	 * @param skill The skill index
	 * @param amount The amount to increase level
	 */
	public void increaseLevel(int skill, int amount) {
		if (skill != PRAYER) {
			level[skill] += amount;
		} else {
			prayerPoints += amount;
		}
		refresh(skill);
	}

	/**
	 * Raises skill level one time only (Example: boosting with beer to 100/99 Woodcutting)
	 * 
	 * @param skill The skill index
	 * @param amount The amount to increase level
	 */
	public void increaseLevelOnce(int skill, int amount) {
		if (skill != PRAYER) {
			if (level[skill] + amount >= (getLevelForXP(exp[skill]) + amount)) {
				level[skill] = (byte) (getLevelForXP(exp[skill]) + amount);
			} else {
				level[skill] += amount;
			}
		} else {
			if (prayerPoints + amount >= (getLevelForXP(exp[skill]) + amount)) {
				prayerPoints = (getLevelForXP(exp[skill]) + amount);
			} else {
				prayerPoints += amount;
			}
		}
		refresh(skill);
	}

	/**
	 * Decreases skill level.
	 * 
	 * @param skill The skill index
	 * @param amount The amount to decrease level
	 */
	public void decreaseLevel(int skill, int amount) {
		if (skill != PRAYER) {
			level[skill] -= amount;
		} else {
			prayerPoints -= amount;
		}
		refresh(skill);
	}

	/**
	 * Decreases skill level one time only (Example: an effect of some spell that gets you down 95/99 Magic)
	 * 
	 * @param index The skill index
	 * @param amount The amount to decrease level
	 */
	public void decreaseLevelOnce(int skill, int amount) {
		if (skill != PRAYER) {
			if (level[skill] > (getLevelForXP(exp[skill]) - amount)) {
				if (level[skill] - amount <= (getLevelForXP(exp[skill]) - amount)) {
					level[skill] = (byte) (getLevelForXP(exp[skill]) - amount);
				} else {
					level[skill] -= amount;
				}
			} else {
				if (prayerPoints > (getLevelForXP(exp[skill]) - amount)) {
					if (prayerPoints - amount <= (getLevelForXP(exp[skill]) - amount)) {
						prayerPoints = (getLevelForXP(exp[skill]) - amount);
					} else {
						prayerPoints -= amount;
					}
				}
			}
			refresh(skill);
		}
	}

	/**
	 * Restores all player stats to normal values
	 */
	public void normalize() {
		for (byte i = 0; i < level.length; i++) {
			level[i] = getLevelForXP(exp[i]);
			if (i != PRAYER) {
				player.getActionSender().sendSkill(i, level[i], exp[i]);
			} else {
				prayerPoints = getLevelForXP(exp[i]);
				player.getActionSender().sendSkill(i, (int) getPrayerPoints(), exp[i]);
			}
		}
	}

	public byte[] getLevel() {
		return level;
	}

	public double[] getExp() {
		return exp;
	}

	public void setPrayerPoints(double points) {
		prayerPoints = points;
	}

	public double getPrayerPoints() {
		return prayerPoints;
	}

	public static String getSkillName(short skillId) {
		return SKILL_NAME[skillId];
	}
}
