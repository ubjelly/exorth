package com.exorth.rs2.content.skills.woodcutting;

import java.util.Random;

import com.exorth.rs2.action.impl.HarvestingAction;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Language;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.clip.RSObject;

/**
 * 
 * Felling is the correct term for the act in which a player who holds an axe chops down a tree.
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class Felling extends HarvestingAction {

	private RSObject object;
	private Tree tree;
	private Axe axe;
	private final Random random = new Random();

	public Felling(Entity entity, RSObject object) {
		super(entity);
		this.setObject(object);
		this.setTree(Tree.forId(object.getId()));
	}

	@Override
	public int getCycleCount() {
		int skill = (int) (getEntity().getSkillLevel(getSkill()) * 1.3);
		int level = getTree().getLevelRequired();
		int modifier = getAxe().getLevelRequirement();
		int randomAmt = random.nextInt(3);
		double cycleCount = 1;
		cycleCount = Math.ceil((level * 50 - skill * 10) / modifier * 0.25 - randomAmt * 4);
		if (cycleCount < 1) {
			cycleCount = 1;
		}
		return (int) cycleCount;
	}

	@Override
	public RSObject getGlobalObject() {
		return object;
	}

	@Override
	public RSObject getReplacementObject() {
		//return new RSObject(1342, object.getPosition().getX(), object.getPosition().getY(), object.getPosition().getZ(), object.getType(), object.getFace());
		return new RSObject(tree.getStumpId(), object.getPosition().getX(), object.getPosition().getY(), object.getPosition().getZ(), object.getType(), object.getFace());
	}
	

	@Override
	public int getObjectRespawnTimer() {
		return tree.getRespawnTime();
	}

	@Override
	public Item getReward() {
		return new Item(tree.getLogId(), 1);
	}

	@Override
	public int getSkill() {
		return Skill.WOODCUTTING;
	}

	@Override
	public int getRequiredLevel() {
		return tree.getLevelRequired();
	}

	@Override
	public double getExperience() {
		return tree.getExperienceGain();
	}

	@Override
	public String getInsufficentLevelMessage() {
		return "You need a " + Skill.SKILL_NAME[getSkill()] + " level of " + getRequiredLevel() + " to chop this tree.";
	}

	@Override
	public String getHarvestStartedMessage() {
		return "You swing your axe at the tree...";
	}

	@Override
	public String getSuccessfulHarvestMessage() {
		return "You get some " + getReward().getDefinition().getName().toLowerCase() + ".";
	}

	@Override
	public String getInventoryFullMessage() {
		return Language.NO_SPACE;
	}

	@Override
	public int getAnimationId() {
		return getAxe().getAnimationId();
	}

	@Override
	public boolean canHarvest() {
		Player player = (Player) getEntity();
		for (Axe axe : Axe.values()) {
			if (player.getInventory().getItemContainer().contains(axe.getItemId()) || player.getEquipment().getItemContainer().contains(axe.getItemId()) && getEntity().getSkillLevel(getSkill()) > axe.getLevelRequirement()) {
				this.axe = axe;
			}
		}
		if (axe == null) {
			player.getActionSender().sendMessage("You don't have an axe to use.");
			return false;
		}
		if (tree.getLevelRequired() > player.getSkillLevel(Skill.WOODCUTTING)) {
			player.getActionSender().sendMessage("You need a Woodcutting level of " + tree.getLevelRequired() + " to cut this tree.");
			return false;
		}
		if (!player.getInventory().hasRoomFor(getReward())) {
			((Player) getEntity()).getActionSender().sendMessage(getInventoryFullMessage());
			return false;
		}
		return true;
	}

	@Override
	public int getGameObjectMaxHealth() {
		/*if (tree.equals(Tree.NORMAL_TREE)) {
			return 1;
		}*/
		int count = Misc.random(24) - Misc.random(tree.getLogCount()) * 2;
		if (count <= 0) {
			count = 1;
		}
		return count;
	}

	public Tree getTree() {
		return tree;
	}

	public void setTree(Tree tree) {
		this.tree = tree;
	}

	public Axe getAxe() {
		return axe;
	}

	public void setAxe(Axe axe) {
		this.axe = axe;
	}

	public RSObject getObject() {
		return object;
	}

	public void setObject(RSObject object) {
		this.object = object;
	}

	@Override
	public int getRespawnRate() {
		return tree.getRespawnTime();
	}
}
