package com.exorth.rs2.content.skills.woodcutting;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * @author Mko
 * 
 */
public enum Tree {

	//TODO: Perhaps increase respawn time of trees?
	//TODO: Some trees are missing correct stump IDs.
	NORMAL_1(1276, 1342, 1, 25, 2, 1511, 1),
	NORMAL_2(1277, 1343, 1, 25, 2, 1511, 1),
	NORMAL_3(1278, 1344, 1, 25, 2, 1511, 1),
	NORMAL_4(1279, 1345, 1, 25, 2, 1511, 1),
	NORMAL_5(1280, 1343, 1, 25, 2, 1511, 1),
	NORMAL_6(1330, 1345, 1, 25, 2, 1511, 1),
	NORMAL_7(1331, 1345, 1, 25, 2, 1511, 1),
	NORMAL_8(1332, 1345, 1, 25, 2, 1511, 1),
	NORMAL_9(2409, 1345, 1, 25, 2, 1511, 1),
	NORMAL_10(3033, 1345, 1, 25, 2, 1511, 1),
	NORMAL_11(3034, 1345, 1, 25, 2, 1511, 1),
	NORMAL_12(3036, 1345, 1, 25, 2, 1511, 1),
	NORMAL_ELF_1(3879, 3880, 1, 25, 2, 1511, 1), //Elven Tree?
	NORMAL_ELF_2(3881, 3880, 1, 25, 2, 1511, 1), //Elven Tree?
	NORMAL_ELF_3(3882, 3880, 1, 25, 2, 1511, 1), //Elven Tree?
	NORMAL_ELF_4(3883, 3884, 1, 25, 2, 1511, 1), //Elven Tree? - TODO: Stump or Tree needs to be decreased/increased in Size, respectively (either/or)
	EVERGREEN_1(1315, 1342, 1, 25, 2, 1511, 1),
	EVERGREEN_2(1316, 1355, 1, 25, 2, 1511, 1), //Larger version of 1315
	EVERGREEN_3(1318, 1355, 1, 25, 2, 1511, 1), //1316 with Snow
	EVERGREEN_4(1319, 1355, 1, 25, 2, 1511, 1), //1316 with Snow shifted
	DEAD_1(1282, 12733, 1, 25, 2, 1511, 1),
	DEAD_2(1283, 12733, 1, 25, 2, 1511, 1),
	DEAD_3(1284, 12733, 1, 25, 2, 1511, 1),
	DEAD_4(1285, 12733, 1, 25, 2, 1511, 1),
	DEAD_5(1286, 1351, 1, 25, 2, 1511, 1),
	DEAD_6(1289, 1353, 1, 25, 2, 1511, 1), //Same as DEAD_4, but Darker
	DEAD_7(1290, 1354, 1, 25, 2, 1511, 1), //Distorted White Dead Tree
	DEAD_8(1291, 23054, 1, 25, 2, 1511, 1), //Dead Tree with Snow
	DEAD_9(1365, 1352, 1, 25, 2, 1511, 1), //Dead Tree with a green tinge
	DEAD_10(1383, 1358, 1, 25, 2, 1511, 1), //Dead Tree with no Roots
	DEAD_11(1384, 1359, 1, 25, 2, 1511, 1), //Dead Tree with no Roots and Burned
	DEAD_12(3035, 12733, 1, 25, 2, 1511, 1),
	DEAD_13(5902, 12733, 1, 25, 2, 1511, 1),
	DEAD_14(5903, 1351, 1, 25, 2, 1511, 1),
	DEAD_15(5904, 1351, 1, 25, 2, 1511, 1),
	JUNGLE_TREE_1(4818, 4819, 1, 25, 2, 1511, 1),
	JUNGLE_TREE_2(4820, 4821, 1, 25, 2, 1511, 1),
	DYING_TREE(24168, 24169, 1, 25, 2, 1511, 1),
	ACHEY(2023, 3371, 1, 25, 2, 2862, 1),
	OAK_1(1281, 1356, 15, 37.5, 4, 1521, 1),
	OAK_2(3037, 1346, 15, 37.5, 4, 1521, 1), //TODO: Stump needs to be GREATLY shrunk
	WILLOW_1(1308, 7399, 30, 67.5, 6, 1519, 1),
	WILLOW_2(5551, 5554, 30, 67.5, 6, 1519, 1),
	WILLOW_3(5552, 5554, 30, 67.5, 6, 1519, 1), //Stump isn't exact, none exist (found so far)
	WILLOW_4(5553, 5554, 30, 67.5, 6, 1519, 1), //Stump isn't exact, none exist (found so far)
	TEAK(9036, 9037, 35, 85, 7, 6333, 1),
	MAPLE_1(1307, 7400, 45, 100, 8, 1517, 1),
	MAPLE_2(4674, 7400, 45, 100, 8, 1517, 1),
	HOLLOW_SMALL(2289, 2310, 45, 112.5, 8, 3239, 1), //XP is in-between Maples/Mahogany
	HOLLOW_BIG(4060, 4061, 45, 112.5, 8, 3239, 1), //XP is in-between Maples/Mahogany
	MAHOGANY(9034, 9035, 50, 125, 9, 6332, 1),
	ARCTIC_PINE(21273, 21274, 54, 140.2, 9, 10810, 1),
	YEW(1309, 7402, 60, 175, 10, 1515, 1),
	MAGIC(1306, 7401, 75, 250, 12, 1513, 1),
	DRAMEN_TREE(1292, 1513 /* Null */, 36, 0, 2, 771, 1),
	VINES_1(5103, 1513 /* Null */, 34, 0, 2, -1, 0),
	VINES_2(5104, 1513 /* Null */, 34, 0, 2, -1, 0),
	VINES_3(5105, 1513 /* Null */, 34, 0, 2, -1, 0),
	VINES_4(5106, 1513 /* Null */, 34, 0, 2, -1, 0),
	VINES_5(5107, 1513 /* Null */, 34, 0, 2, -1, 0);

	/**
	 * The tree id.
	 */
	private int treeId;

	/**
	 * The stump id.
	 */
	private int stumpId;

	/**
	 * The level req.
	 */
	private int levelRequired;

	/**
	 * The exp gain.
	 */
	private double experienceGain;

	/**
	 * The time used to respawn a tree.
	 */
	private int respawnTime;

	/**
	 * The log id.
	 */
	private int logId;

	/**
	 * The log count
	 */
	private int logCount;

	/**
	 * A map of object ids to trees.
	 */
	private static Map<Integer, Tree> trees = new HashMap<Integer, Tree>();

	static {
		for (Tree tree : Tree.values()) {
			int object = tree.getTreeId();
			trees.put(object, tree);
		}
	}

	/**
	 * Gets a tree by an Object ID.
	 * 
	 * @param object The Object ID.
	 * @return The tree, or <code>null</code> if the object is not a tree.
	 */
	public static Tree forId(int object) {
		return trees.get(object);
	}

	/**
	 * Creates a tree.
	 * 
	 * @param treeId The tree.
	 * @param stumpId The stump Id.
	 * @param levelReq The level req.
	 * @param expGain The exp gained.
	 * @param respawnTime The respawn time.
	 * @param logId The log Id.
	 * @param logCount The log count.
	 */
	private Tree(int treeId, int stumpId, int levelReq, double expGain, int respawnTime, int logId, int logCount) {
		this.treeId = treeId;
		this.stumpId = stumpId;
		this.levelRequired = levelReq;
		this.experienceGain = expGain;
		this.respawnTime = respawnTime;
		this.logId = logId;
		this.logCount = logCount;
	}

	/**
	 * Gets the tree id.
	 * 
	 * @return the tree id
	 */
	public int getTreeId() {
		return treeId;
	}

	/**
	 * Gets the stump id.
	 * 
	 * @return the stump
	 */
	public int getStumpId() {
		return stumpId;
	}

	/**
	 * Gets the level requirement.
	 * 
	 * @return the level
	 */
	public int getLevelRequired() {
		return levelRequired;
	}

	/**
	 * Gets the experience gained.
	 * 
	 * @return the experience gain.
	 */
	public double getExperienceGain() {
		return experienceGain;
	}

	/**
	 * Gets the respawn time.
	 * 
	 * @return the respawn time
	 */
	public int getRespawnTime() {
		return respawnTime;
	}

	/**
	 * Gets the log id.
	 * 
	 * @return the log
	 */
	public int getLogId() {
		return logId;
	}

	/**
	 * Gets the log count.
	 * 
	 * @return the log count.
	 */
	public int getLogCount() {
		return logCount;
	}
}