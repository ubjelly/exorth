package com.exorth.rs2.content;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Misc;

/**
 * Used to 'shear' an npc to a naked form.
 * 
 * @author Joshua Barry
 * 
 */
public class SheepShearer implements Shearer<Npc> {
	private static SheepShearer instance;

	/**
	 * The ID of the shears needed to shear the NPC.
	 */
	private static final int SHEARS_ID = 1735;

	/**
	 * The ID of the wool you get from from the NPC.
	 */
	private static final int WOOL_ID = 1737;

	/**
	 * The ID of the animation that is performed when shearing.
	 */
	private static final int ANIMATION_ID = 893;

	/**
	 * 
	 * @param player
	 * @param n
	 */
	@Override
	public void shear(final Player player, final Npc n) {
		if (!player.getInventory().getItemContainer().contains(SHEARS_ID)) {
			player.getActionSender().sendMessage("You must have a pair of " + ItemManager.getInstance().getItemName(SHEARS_ID).toLowerCase() + " to shear a " + n.getDefinition().getName().toLowerCase() + ".");
			return;
		} else {
			int magicNumber = Misc.random(8);
			System.out.println(magicNumber + " is the magic value!");
			player.getUpdateFlags().sendAnimation(ANIMATION_ID);
			if (magicNumber > 1) { // chance of failure.
				World.submit(new Task(4) {
					@Override
					protected void execute() {
						n.setAttribute("TRANSFORM_ID", n.getDefinition().getId() - 1);
						n.setAttribute("TRANSFORM_UPDATE", 0);
						if (player.getInventory().addItem(new Item(WOOL_ID))) {
							player.getActionSender().sendMessage("You get some " + ItemManager.getInstance().getItemName(WOOL_ID) + ".");
						}
						this.stop();
					}
				});
				World.submit(new Task(10) {
					@Override
					protected void execute() {
						n.setAttribute("TRANSFORM_ID", n.getDefinition().getId());
						n.setAttribute("TRANSFORM_UPDATE", 0);
						this.stop();
					}
				});
			} else {
				n.getUpdateFlags().sendForceMessage("BAA!");
				// TODO: Implement movementHandler to NPC and make this NPC walk away.
			}
		}
	}

	public static SheepShearer getSingleton() {
		if (instance == null) {
			instance = new SheepShearer();
		}
		return instance;
	}
}