package com.exorth.rs2.content;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.area.Areas;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Language;
import com.exorth.rs2.util.Misc;

/**
 * This can be used for Tele Tabs. NOTE: Decided to stay up all night and do
 * some of these crazy little content updates ^^
 * 
 * @author Jacob Ford
 */
public class TeleportTabs {
	public static TeleportTabs instance = null;

	public static TeleportTabs getInstance() {
		if (instance == null) {
			instance = new TeleportTabs();
		}
		return instance;
	}

	/**
	 * Enum of the teleport tabs.
	 */
	private static enum TeleportTab {
		VARROCK(8007, 3213, 3424),
		LUMBRIDGE(8008, 3222, 3218),
		FALADOR(8009, 2964, 3379),
		CAMELOT(8010, 2757, 3477),
		ARDOUGNE(8011, 2661, 3305),
		WATCHTOWER(8012, 2549, 3112);

		/**
		 * The tab ID.
		 */
		private int tabletId;

		/**
		 * The x-axis.
		 */
		private int posX;

		/**
		 * The y-axis.
		 */
		private int posY;

		/**
		 * Teleports
		 * 
		 * @param tabletId The tab.
		 * @param posX The x-axis.
		 * @param posY The y-axis.
		 */
		private TeleportTab(int tabletId, int posX, int posY) {
			this.tabletId = tabletId;
			this.posX = posX;
			this.posY = posY;
		}

		/**
		 * Gets the tab id
		 * 
		 * @return The tab.
		 */
		private int getTabletId() {
			return tabletId;
		}

		/**
		 * Gets the x-axis.
		 * 
		 * @return The x-axis.
		 */
		private int getPostionX() {
			return posX;
		}

		/**
		 * Gets the y-axis.
		 * 
		 * @return The y-axis.
		 */
		private int getPostionY() {
			return posY;
		}

		/**
		 * Gets the location name.
		 * 
		 * @return The name.
		 */
		private final String getLocation() {
			return Misc.optimizeText(toString().toLowerCase());
		}

		private static TeleportTab getTabletID(int tabletId) {
			for (TeleportTab tab : TeleportTab.values()) {
				if (tab.getTabletId() == tabletId) {
					return tab;
				}
			}
			return null;
		}
	}

	/**
	 * Activating the teleport tab.
	 * 
	 * @param player The player.
	 * @param item The tab.
	 * @param inventorySlot The slot.
	 */
	public void activateTeleportTab(final Player player, final int item, final int inventorySlot) {
		final TeleportTab tab = TeleportTab.getTabletID(item);
		if (tab == null || item != tab.getTabletId()) {
			return;
		}
		if (player.getMinigame() != null) {
			player.getActionSender().sendMessage("You can't teleport whilst playing a minigame.");
			return;
		}
		if (player.getAttribute("cant_teleport") != null) {
			return;
		}
		if (player.getAttribute("teleBlocked") != null) {
			if (System.currentTimeMillis() - (Long) player.getAttribute("teleBlocked") < 300000) {
				player.getActionSender().sendMessage("You are teleport blocked and can't teleport!");
				return;
			} else {
				player.removeAttribute("teleBlocked");
			}
		}
		if (Areas.getWildernessLevel(player) > 20) {
			player.getActionSender().sendMessage(Language.CANT_TELE_20);
			return;
		}
		player.getInventory().removeItem(new Item(tab.getTabletId(), 1));
		player.getActionSender().sendMessage("You break the teleport tab and go to " + tab.getLocation().toString().toLowerCase() + ".");
		player.getUpdateFlags().sendAnimation(4731);
		player.getUpdateFlags().sendGraphic(678, 0);
		player.setAttribute("cant_teleport", 0);
		World.submit(new Task(2) {
			@Override
			public void execute() {
				player.clipTeleport(tab.getPostionX(), tab.getPostionY(), 0, 4);
				player.getUpdateFlags().sendAnimation(-1);
				this.stop();
			}
		});
		World.submit(new Task(4) {
			@Override
			protected void execute() {
				player.removeAttribute("cant_teleport");
				this.stop();
			}
		});
	}
}