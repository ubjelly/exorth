package com.exorth.rs2.content;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.clip.PathFinder;

/**
 * Handles climbing up a flight of stairs or a ladder.
 * 
 * @author Mko
 * 
 */
public class Climbable {

	private static int cycleAmount = 0;

	public enum Direction {
		DOWN(-1),
		UP(1);

		private int heightMod;

		/**
		 * 
		 * @param heightMod
		 */
		private Direction(int heightMod) {
			this.heightMod = heightMod;
		}

		public int getHeightMod() {
			return heightMod;
		}
	}

	/**
	 * Climb (with stairs)
	 * <p>
	 * For <code>stairPosition</code>, be sure to have the tile be near the stairs, not the stairs' actual Position
	 * 
	 * @param player The player
	 * @param stairPosition The position directly in front of the flight of stairs
	 * @param destination The position the player will be teleported to upon interaction with the stairs
	 * @param size ?
	 */
	public static void climb(final Player player, final Position stairPosition, final Position destination, final int size) {
		int stairId = player.getClickId();
		switch (stairId) {
			default:
				World.submit(new Task(1) {
					@Override
					protected void execute() {
						if (!(player.getPosition().equals(stairPosition))) {
							setCycleAmount(getCycleAmount() + 1);
							PathFinder.getSingleton().moveTo(player, stairPosition.getX(), stairPosition.getY());
							player.getActionSender().sendMessage("Approaching object...");
							return;
						}
						if (getCycleAmount() == 20){
							setCycleAmount(0);
							player.getActionSender().sendMessage("Aborting action.");
							this.stop();
						}
						setCycleAmount(0);
						player.getActionSender().sendMessage("Initiating action.");
						if (size > 0) {
							player.clipTeleport(destination, size);
						} else {
							player.teleport(destination);
						}
						this.stop();
					}
				});
				break;
		}
	}

	/**
	 * Climb (with ladders)
	 * 
	 * For <code>ladderPosition</code>, be sure to have the tile be near the ladder, not the ladder's actual Position
	 * 
	 * @param player The player
	 * @param ladderPosition The position directly in front of the ladder
	 * @param destination The position the player will be teleported to upon interaction with the ladder
	 */
	public static void climb(final Player player, final Position ladderPosition, final Position destination) {
		int objectId = player.getClickId();
		int x = player.getPosition().getX();
		int y = player.getPosition().getY();
		int z = player.getPosition().getZ();
		final int anim = 828;

		switch (objectId) {
			// TODO: Implement in WalkToActions
			// Related to Barrows
			case 6702:
				x = 3565;
				y = 3289;
				z = 0;
				break;
			// End Barrows
			/*
			def objectOptionOne_bone_chain(player):
				Climbable.climb(player, Position(3081, 3421, 0), 0)
			def objectOptionOne_rope(player):
				if player.getClick() == 2017 and player.getClickY() == 5210:
					Climbable.climb(player, Position(3081, 3421, 0), 0)
			def objectOptionOne_boney_ladder(player):
				Climbable.climb(player, Position(2145, 5284, 0), 2)
			 */
			// Related to Stronghold of Security
			case 16148: //Floor 1 - Center + Entry Ladder
				x = 3081;
				y = 3421;
				z = 0;
				break;
			case 16146: //Floor 1 - Quick Escape Ladder
				x = 3081;
				y = 3421;
				z = 0;
				break;
			case 16149: //Floor 1 - Descent Ladder
				x = 2042;
				y = 5245;
				z = 0;
				break;
			case 16078: //Floor 2 - Center + Quick Escape Ladder
			case 16080: //Floor 2 - Entry Ladder
				x = 1902;
				y = 5223;
				z = 0;
				break;
			case 16081: //Floor 2 - Descent Ladder
				x = 2122;
				y = 5251;
				z = 0;
				break;
			case 16112: //Floor 3 - Center Ladder
			case 16114: //Floor 3 - Entry Ladder
				x = 2026;
				y = 5217;
				z = 0;
				break;
			case 16115: //Floor 3 - Descent Ladder
				x = 2294;
				y = 5215;
				z = 0;
				break;
			case 16048: //Floor 4 - Center Ladder
			case 16049: //Floor 4 - Entry Ladder
				x = 2147;
				y = 5284;
				z = 0;
				break;
			// End Stronghold of Security
		}

		World.submit(new Task(1) {
			@Override
			protected void execute() {
				if (!(player.getPosition().equals(ladderPosition))) {
					setCycleAmount(getCycleAmount() + 1);
					PathFinder.getSingleton().moveTo(player, ladderPosition.getX(), ladderPosition.getY());
					player.getActionSender().sendMessage("Approaching object...");
					return;
				}
				setCycleAmount(0);
				player.getActionSender().sendMessage("Initiating action.");
				if (getCycleAmount() == 20){
					setCycleAmount(0);
					player.getActionSender().sendMessage("Aborting action.");
					this.stop();
				}
				player.getUpdateFlags().sendAnimation(anim);
				World.submit(new Task(3) {
					@Override
					protected void execute() {
						player.teleport(destination);
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	public static int getCycleAmount() {
		return cycleAmount;
	}

	public static void setCycleAmount(int cycleAmount) {
		Climbable.cycleAmount = cycleAmount;
	}
}