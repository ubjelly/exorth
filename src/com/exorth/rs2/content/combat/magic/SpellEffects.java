package com.exorth.rs2.content.combat.magic;

import com.exorth.rs2.content.combat.util.FreezeEntity;
import com.exorth.rs2.content.combat.util.Poison;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Misc;

/**
 * 
 * @author AkZu
 * 
 */
public class SpellEffects {
	/**
	 * Applying any extra effects (poisoning, freezing, etc) after the attack.
	 */
	public static void applyMagicEffects(Entity attacker, final Entity victim, int magicIndex, int hit, boolean victimMagePray) {
		if (attacker.isPlayer()) {
			int spellId = SpellLoader.getSpellDefinitions()[magicIndex].getSpellId();

			switch (spellId) {
				case 1190: // Saradomin Strike, lowers 1 Prayer point per successful hit
					// cast.
					if (victim.isPlayer() && victim.getSkillLevel(Skill.PRAYER) > 0) {
						victim.getSkill().setPrayerPoints(victim.getSkill().getPrayerPoints() - 1);
						if (victim.isPlayer()) {
							victim.getSkill().refresh(5);
							((Player) victim).getActionSender().sendMessage("Your prayer has been drained!");
							// TODO: Drain Prayer points
						}
					}
					break;
				case 1191: // Claws of Guthix, lowers 5% (once) of victim's defence level
					if (victim.isPlayer()) {
						byte finalDef = (byte) (((Player) victim).getSkill().getLevelForXP(((Player) victim).getSkill().getExp()[1]) - (int) (((Player) victim).getSkill().getLevelForXP(((Player) victim).getSkill().getExp()[1]) * 0.05));
						if (((Player) victim).getSkill().getLevel()[1] > finalDef) {
							((Player) victim).getSkill().getLevel()[1] = finalDef;
							((Player) victim).getSkill().refresh(1);
						}
					}
					break;
				case 1192: // Flames of Zamorak, lowers victim's Magic level by 5 points (only once)
					if (victim.isPlayer()) {
						byte finalMagic = (byte) (((Player) victim).getSkill().getLevelForXP(((Player) victim).getSkill().getExp()[6]) - 5);
						if (((Player) victim).getSkill().getLevel()[6] > finalMagic) {
							((Player) victim).getSkill().getLevel()[6] = finalMagic;
							((Player) victim).getSkill().refresh(6);
							((Player) victim).getActionSender().sendMessage("You feel your magical powers weaken.");
						}
					}
					break;
				case 12445: // Tele block
					if (victim.isNpc()) {
						return;
					}
					victim.setAttribute("teleBlocked", System.currentTimeMillis() + (victimMagePray ? 150000 : 0));
					((Player) victim).getActionSender().sendMessage("A teleport block has been cast on you!");
					break;
				case 1572: // Bind
				case 1582: // Snare
				case 1592: // Entangle
					int time = (spellId == 1572 ? 8 : (spellId == 1582 ? 16 : 24));
					if (victim.isPlayer()) {
						if (victimMagePray) {
							time /= 2;
						}
					}
					FreezeEntity.freezeEntity(victim, time, time / 2, false);
					break;
				case 12891: // Ice barrage
					FreezeEntity.freezeEntity(victim, 34, 10, true);
					break;
				case 12871: // Ice blitz
					FreezeEntity.freezeEntity(victim, 25, 8, true);
					break;
				case 12881: // Ice burst
					FreezeEntity.freezeEntity(victim, 17, 5, true);
					break;
				case 12861: // Ice rush
					FreezeEntity.freezeEntity(victim, 9, 4, true);
					break;
				case 12929: // Blood barrage
				case 12911: // Blood blitz
				case 12919: // Blood burst
				case 12901: // Blood rush
					byte finalHitpoints = (byte) (((Player) attacker).getSkill().getLevel()[3] + (hit / 4));
					if (finalHitpoints != ((Player) attacker).getSkill().getLevel()[3] && finalHitpoints < ((Player) attacker).getSkill().getLevelForXP(((Player) attacker).getSkill().getExp()[3])) {
						((Player) attacker).getSkill().getLevel()[3] = finalHitpoints;
						((Player) attacker).getActionSender().sendMessage("You drain some of your opponent's health.");
						if (victim.isPlayer()) {
							((Player) victim).getActionSender().sendMessage("Some of your health has been drained.");
						}
						((Player) attacker).getSkill().refresh(3);
					}
					break;
				case 13023: // Shadow barrage
					if (victim.isPlayer()) {
						byte finalAttack = (byte) (((Player) victim).getSkill().getLevelForXP(((Player) victim).getSkill().getExp()[0]) - (int) (((Player) victim).getSkill().getLevelForXP(((Player) victim).getSkill().getExp()[0]) * 0.15));
						if (((Player) victim).getSkill().getLevel()[0] > finalAttack) {
							((Player) victim).getSkill().getLevel()[0] = finalAttack;
							((Player) victim).getSkill().refresh(0);
						}
					}
					break;
				case 12999: // Shadow blitz
				case 13011: // Shadow burst
				case 12987: // Shadow rush
					if (victim.isPlayer()) {
						byte finalAttack = (byte) (((Player) victim).getSkill().getLevelForXP(((Player) victim).getSkill().getExp()[0]) - (int) (((Player) victim).getSkill().getLevelForXP(((Player) victim).getSkill().getExp()[0]) * 0.1));
						if (((Player) victim).getSkill().getLevel()[0] > finalAttack) {
							((Player) victim).getSkill().getLevel()[0] = finalAttack;
							((Player) victim).getSkill().refresh(0);
						}
					}
					break;
				case 12975: // Smoke barrage
				case 12951: // Smoke blitz
					if (Misc.random(9) == 0) {
						Poison.appendPoison(victim, true, 4);
					}
					break;
				case 12963: // Smoke burst
				case 12939: // Smoke rush
					if (Misc.random(7) == 0) {
						Poison.appendPoison(victim, true, 2);
					}
					break;
				case 1153: // Confuse
					if (victim.isPlayer()) {
						Player otherPlayer = (Player) victim;
						otherPlayer.getSkill().getLevel()[0] -= (otherPlayer.getSkill().getLevelForXP(otherPlayer.getSkill().getExp()[0]) * 5 / 100);
						((Player) attacker).getActionSender().sendMessage("You weaken your opponent.");
						((Player) victim).getActionSender().sendMessage("Your Attack level has been reduced!");
					}
					break;
			}
		}
	}
}