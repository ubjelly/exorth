package com.exorth.rs2.content.combat.ranged;

import com.exorth.rs2.content.combat.ranged.RangedData.ArrowType;
import com.exorth.rs2.content.combat.ranged.RangedData.BowType;
import com.exorth.rs2.content.combat.ranged.RangedData.RangeWeaponType;

/**
 * A class that loads every single thing to do with, Arrows, Knives, & all other
 * ranged projectiles.
 * 
 * @author Jacob & AkZu.
 */
public class RangeProjectiles {
	/**
	 * The projectile ID. (e.g arrow, knife)
	 */
	private int[] ID;

	/**
	 * The arrowType e.g BRONZE_ARROW
	 */
	private ArrowType arrowType;

	/**
	 * The rangeWeaponType e.g MITHRIL_THROWNAXE
	 */
	private RangeWeaponType rangeWeaponType;

	/**
	 * The bowType e.g MAPLE_LONGBOW
	 */
	private BowType bowType;

	public int[] getID() {
		return ID;
	}

	public BowType getBowType() {
		return bowType;
	}

	public ArrowType getArrowType() {
		return arrowType;
	}

	public RangeWeaponType getRangeWeaponType() {
		return rangeWeaponType;
	}
}