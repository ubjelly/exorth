package com.exorth.rs2.content.combat.util;

import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.players.Player;

// TODO: Implement how skulling actually works correctly
public class Skulling {
	public static void skullEntity(Entity attacker, Entity victim) {
		if (attacker.isPlayer() && victim.isPlayer()) {
			Player player = (Player) attacker;
			if (!player.isSkulled() && !attacker.getCombatState().getDamageMap().getTotalDamages().containsKey(victim)) {
				player.setSkulled(true);
			}
		}
	}
}