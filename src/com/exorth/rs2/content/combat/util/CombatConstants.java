package com.exorth.rs2.content.combat.util;

import com.exorth.rs2.content.combat.ranged.RangedData.ArrowType;

public class CombatConstants {
	/**
	 * Combat constants
	 */
	public static final int MELEE_VOID_HELM = 11665;
	public static final int RANGE_VOID_HELM = 11664;
	public static final int MAGE_VOID_HELM = 11663;

	public static final int VOID_CHEST = 8839;
	public static final int VOID_LEGS = 8840;
	public static final int VOID_GLOVES = 8842;

	public static final int SALVE_AMMY = 4081;
	public static final int SALVE_AMMY_E = 10588;

	public static final int[] DHAROK_HELM = { 4716, 4880, 4881, 4882, 4883 };
	public static final int[] DHAROK_CHEST = { 4720, 4892, 4893, 4894, 4895 };
	public static final int[] DHAROK_LEGS = { 4722, 4898, 4899, 4900, 4901 };
	public static final int[] DHAROK_AXE = { 4718, 4886, 4887, 4888, 4889 };

	public static final int[] OBSIDIAN_WEAPONS = { 6523, 6525, 6526, 6527, 6528 };
	public static final int OBSIDIAN_NECKLACE = 11128;

	public static final int[] BLACK_MASK = { 8901, 8903, 8905, 8907, 8909, 8911, 8913, 8913, 8915, 8917, 8919, 8921 };

	/**
	 * Returns the pullback graphic id for used with dark bow.
	 */
	public static int getPullBackDarkBow(ArrowType arrowType) {
		switch (arrowType) {
			case BRONZE_ARROW:
				return 1104;
			case IRON_ARROW:
				return 1105;
			case STEEL_ARROW:
				return 1106;
			case MITHRIL_ARROW:
				return 1107;
			case ADAMANT_ARROW:
				return 1108;
			case RUNE_ARROW:
				return 1109;
			case DRAGON_ARROW:
				return 1111;
			default:
				System.out.println("UNHANDLED ARROW TYPE");
				break;
		}
		return -1;
	}

	/**
	 * Special attack data WeaponId, energy_needed,
	 */
	public static final int[][] SPECIAL_ATTACK_DATA = {
		{ 4151, 50 },
		{ 11694, 50 },
		{ 11696, 100 },
		{ 11698, 100 },
		{ 11700, 100 },
		{ 1215, 25 },
		{ 1231, 25 },
		{ 5680, 25 },
		{ 5698, 25 },
		{ 4587, 55 },
		{ 805, 10 },
		{ 6739, 100 },
		{ 7158, 60 },
		{ 6746, 50 },
		{ 8872, 75 },
		{ 6724, 100 },
		{ 3101, 25 },
		{ 1434, 25 },
		{ 1305, 25 },
		{ 3204, 30 },
		{ 1249, 100 },
		{ 1263, 100 },
		{ 3176, 100 },
		{ 5716, 100 },
		{ 5730, 100 },
		{ 861, 55 },
		{ 11235, 55 },
		{ 1377, 100 },
		{ 35, 100 },
		{ 4153, 50 }
	};

	public static int[] getSpecialAttackData(int weaponId) {
		for (int i = 0; i < SPECIAL_ATTACK_DATA.length; i++) {
			if (SPECIAL_ATTACK_DATA[i][0] == weaponId) {
				return SPECIAL_ATTACK_DATA[i];
			}
		}
		return null;
	}

	/**
	 * The weapon interfaces with their attack bonuses (stab, slash, crush)
	 * TODO: Tweak and and check all works.
	 * 
	 * ORDER ACCURATE - AGGRESSIVE - AGGRESSIVE_1 - DEFENSIVE - CONTROLLED_1 -
	 * CONTROLLED_2 - CONTROLLED_3
	 * 
	 * [ID] 0, [BONUS] 1-7 [STYLE POSITION] 8-15 [CONFIG] 15-22 [TYPE] 22 e.g
	 * melee-0, range-1 [SPECIAL BAR] 23, so we can disable it from f2p weapons.
	 */
	public static final int[][] WEAPON_INTERFACE_DATA = {
		// Melee
		{ 328, 2, 2, -1, 2, -1, -1, -1, 0, 1, 3, 3, 0, 1, 3, 1, 2, -1, 0, -1, -1, -1, 0, -1 },
		{ 425, 2, 2, -1, 2, -1, -1, -1, 0, 1, 3, 3, 0, 1, 3, 1, 2, -1, 0, -1, -1, -1, 0, 7474 },
		{ 776, 1, 0, 2, 1, -1, -1, -1, 0, 1, 2, 3, 0, 1, 2, 1, 2, 0, 3, -1, -1, -1, 0, -1 },
		{ 1698, 1, 1, 2, 1, -1, -1, -1, 0, 1, 2, 3, 0, 1, 2, 1, 2, 0, 3, -1, -1, -1, 0, 7499 },
		{ 2276, 0, 0, 1, 0, -1, -1, -1, 0, 1, 2, 3, 0, 1, 2, 1, 2, 0, 3, -1, -1, -1, 0, 7574 },
		{ 2423, 1, 1, -1, 1, -1, -1, 0, 0, 1, 6, 3, 0, 1, 6, 1, 2, -1, 3, -1, -1, 0, 0, 7599 },
		{ 3796, 2, 2, -1, 2, -1, -1, 0, 0, 1, 6, 3, 0, 1, 6, 1, 2, -1, 3, -1, -1, 0, 0, 7624 },
		{ 4679, -1, -1, -1, 0, 0, 1, 2, 4, 5, 6, 3, 4, 5, 6, -1, -1, -1, 3, 1, 2, 0, 0, 7674 },
		{ 4705, 1, 1, 2, 1, -1, -1, -1, 0, 1, 2, 3, 0, 1, 2, 1, 2, 0, 3, -1, -1, -1, 0, 7699 },
		{ 5570, 0, 0, 2, 0, -1, -1, -1, 0, 1, 2, 3, 0, 1, 2, 1, 2, 0, 3, -1, -1, -1, 0, -1 },
		{ 5855, 2, 2, -1, 2, -1, -1, -1, 0, 1, 3, 3, 0, 1, 3, 1, 2, -1, 0, -1, -1, -1, 0, -1 },
		{ 6103, 2, 2, -1, 2, -1, -1, -1, 0, 1, 3, 3, 0, 1, 3, 1, 2, -1, 0, -1, -1, -1, 0, -1 },
		{ 7762, 1, 1, -1, 1, -1, -1, 0, 0, 1, 6, 3, 0, 1, 6, 1, 2, -1, 3, -1, -1, 0, 0, 7800 },
		{ 8460, -1, 1, -1, 0, 0, -1, -1, 4, 1, 3, 3, 4, 1, 3, -1, 2, -1, 0, 1, -1, -1, 0, 8493 },
		{ 12290, 1, -1, -1, 1, -1, 1, -1, 0, 5, 3, 3, 0, 5, 3, 1, -1, -1, 0, -1, 2, -1, 0, 12323 },
		// End of Melee
		// Ranged
		{ 1749, 4, 4, -1, 4, -1, -1, -1, 0, 1, 3, 3, 0, 1, 3, 1, 2, -1, 0, -1, -1, -1, 1, 7524 },
		{ 1764, 4, 4, -1, 4, -1, -1, -1, 0, 1, 3, 3, 0, 1, 3, 1, 2, -1, 0, -1, -1, -1, 1, 7549 },
		{ 4446, 4, 4, -1, 4, -1, -1, -1, 0, 1, 3, 3, 0, 1, 3, 1, 2, -1, 0, -1, -1, -1, 1, 7649 }
		// End of Ranged
	};
}