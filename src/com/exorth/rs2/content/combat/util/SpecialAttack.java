package com.exorth.rs2.content.combat.util;

import com.exorth.rs2.Constants;
import com.exorth.rs2.content.combat.Combat;
import com.exorth.rs2.content.combat.ranged.RangedDataLoader;
import com.exorth.rs2.content.combat.ranged.RangedData.ArrowType;
import com.exorth.rs2.content.combat.ranged.RangedData.BowType;
import com.exorth.rs2.content.combat.util.Weapons.WeaponLoader;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Language;
import com.exorth.rs2.util.Misc;

// TODO: Needs revamping.
public class SpecialAttack {
	private static SpecialAttack instance = null;

	public static SpecialAttack getInstance() {
		if (instance == null) {
			instance = new SpecialAttack();
		}
		return instance;
	}

	public void performSpecialAttack(final Player attacker, final Entity victim) {
		if (attacker == null || victim == null) {
			return;
		}
		if (attacker.getEquipment().getItemContainer().get(3) == null) {
			return;
		}
		if (!Combat.getInstance().meetsAttackRequirements(attacker, victim)) {
			Combat.getInstance().resetCombat(attacker);
			return;
		}

		int weaponId = attacker.getEquipment().get(3).getId();
		final int distance = Misc.getDistance(attacker.getPosition(), victim.getPosition());
		int anim = 0;

		switch (weaponId) {
			case 4153:
				return;
			case 11694:
				attacker.getUpdateFlags().sendGraphic(1222, 0, 100);
				anim = 7074;
				break;
			case 11696:
				attacker.getUpdateFlags().sendGraphic(1223, 0, 100);
				anim = 7073;
				break;
			case 11698:
				attacker.getUpdateFlags().sendGraphic(1220, 0, 100);
				anim = 7071;
				break;
			case 11700:
				attacker.getUpdateFlags().sendGraphic(1221, 0, 100);
				anim = 7070;
				break;
			case 1215:
			case 1231:
			case 5680:
			case 5698:
				Combat.getInstance().delayHit(attacker, victim, 1, true, true, DetermineHit.calculateHit(attacker, victim, true), attacker.getCombatState().getCombatStyle(), attacker.getAttackType(), 0, null, null);
				attacker.getUpdateFlags().sendGraphic(252, 0, 100);
				anim = 1062;
				break;
			case 4151:
				if (victim.isPlayer()) {
					attacker.setEnergy(attacker.getEnergy() + (int) (((Player) victim).getEnergy() * .1));
					((Player) victim).setEnergy((int) (((Player) victim).getEnergy() * .9));
				}
				victim.getUpdateFlags().sendGraphic(341, 0, 100);
				anim = 1658;
				break;
			case 4587:
				attacker.getUpdateFlags().sendGraphic(347, 0, 100);
				if (victim.isPlayer()) {
					if (((Player) victim).getActivePrayers()[Prayer.PROTECT_FROM_MAGIC] || ((Player) victim).getActivePrayers()[Prayer.PROTECT_FROM_RANGED] || ((Player) victim).getActivePrayers()[Prayer.PROTECT_FROM_MELEE]) {
						victim.setAttribute("dScimEffect", (byte) 0);

						Prayer.getInstance().activatePrayer((Player) victim, 16, true);
						Prayer.getInstance().activatePrayer((Player) victim, 17, true);
						Prayer.getInstance().activatePrayer((Player) victim, 18, true);

						((Player) victim).setPrayerIcon(-1);
						((Player) victim).setAppearanceUpdateRequired(true);

						World.submit(new Task(8) {
							@Override
							protected void execute() {
								victim.removeAttribute("dScimEffect");
								this.stop();
							}
						});
					}
				}
				anim = 1872;
				break;
			case 805:
				anim = 1068;
				break;
			case 6739:
				attacker.getUpdateFlags().sendGraphic(479, 0, 100);
				victim.getSkill().decreaseLevel(1, (int) (victim.getSkillLevel(1) * 0.13));
				victim.getSkill().decreaseLevel(6, (int) (victim.getSkillLevel(6) * 0.13));
				anim = 2876;
				break;
			case 7158:
				attacker.getUpdateFlags().sendGraphic(559, 0, 100);
				anim = 3157;
				break;
			case 6746:
				attacker.getUpdateFlags().sendGraphic(483, 0, 0);
				victim.getSkill().decreaseLevelOnce(0, (int) (victim.getSkillLevel(0) * (victim.isDemon() ? 0.1 : 0.05)));
				victim.getSkill().decreaseLevelOnce(2, (int) (victim.getSkillLevel(2) * (victim.isDemon() ? 0.1 : 0.05)));
				victim.getSkill().decreaseLevelOnce(1, (int) (victim.getSkillLevel(1) * (victim.isDemon() ? 0.1 : 0.05)));
				anim = 2890;
				break;
			case 8872:
				anim = 2890;
				break;
			case 6724:
				anim = -1;
				break;
			case 3101:
				attacker.getUpdateFlags().sendGraphic(274, 0, 100);
				anim = 923;
				break;
			case 1434:
				attacker.getUpdateFlags().sendGraphic(251, 0, 100);
				anim = 1060;
				break;
			case 1305:
				attacker.getUpdateFlags().sendGraphic(248, 0, 100);
				anim = 1058;
				break;
			case 3204:
				attacker.getUpdateFlags().sendGraphic(285, 0, 100);
				anim = 1203;
				break;
			case 1249:
			case 1263:
			case 3176:
			case 5716:
			case 5730:
				attacker.getUpdateFlags().sendGraphic(253, 0, 100);
				anim = 405;
				break;
			case 861: // TODO: Fine tune!
				int attackerX = attacker.getPosition().getX(),
				attackerY = attacker.getPosition().getY();
				int victimX = victim.getPosition().getX(),
				victimY = victim.getPosition().getY();
				final int offsetX = (attackerY - victimY) * -1;
				final int offsetY = (attackerX - victimX) * -1;
				final int projectileId = 249;

				attacker.getUpdateFlags().sendAnimation(1074, 0);
				attacker.getUpdateFlags().sendGraphic(256, 0, 100);
				int speed = 28 + (distance * 3);

				World.sendProjectile(attacker.getPosition(), offsetX, offsetY, projectileId, 45, 34, speed, 28, 13, 64, 50, victim.isNpc() ? victim.getIndex() + 1 : -victim.getIndex() - 1);
				speed = 55 + (distance * 3);
				World.sendProjectile(attacker.getPosition(), offsetX, offsetY, projectileId, 45, 34, speed, 55, 15, 64, 50, victim.isNpc() ? victim.getIndex() + 1 : -victim.getIndex() - 1);
				Combat.getInstance().delayHit(attacker, victim, (distance == 1 ? 1 : 2), false, false, DetermineHit.calculateHit(attacker, victim, true), attacker.getCombatState().getCombatStyle(), attacker.getAttackType(), 0, attacker.getEquipment().get(3), attacker.getEquipment().get(13));
				Combat.getInstance().delayHit(attacker, victim, (distance == 1 ? 1 : 2), false, false, DetermineHit.calculateHit(attacker, victim, true), attacker.getCombatState().getCombatStyle(), attacker.getAttackType(), 0, attacker.getEquipment().get(3), attacker.getEquipment().get(13));

				World.submit(new Task(1, true) {
					@Override
					protected void execute() {
						attacker.getUpdateFlags().sendGraphic(256, 3, 100);
						this.stop();
					}
				});

				World.submit(new Task(1) {
					@Override
					protected void execute() {
						Item weapon = attacker.getEquipment().get(3);
						BowType bowType = RangedDataLoader.getProjectileId(weapon.getId()).getBowType();
						Combat.getInstance().removeAmmo(attacker, bowType == null ? 3 : 13, 1);
						attacker.getEquipment().refresh();
						this.stop();
					}
				});
				break;
			case 11235: // TODO: Dark bow
				attackerX = attacker.getPosition().getX();
				attackerY = attacker.getPosition().getY();
				victimX = victim.getPosition().getX();
				victimY = victim.getPosition().getY();
				final int offsetX2 = (attackerY - victimY) * -1;
				final int offsetY2 = (attackerX - victimX) * -1;

				attacker.getUpdateFlags().sendAnimation(426, 0);

				ArrowType arrowType = null;

				if (attacker.getEquipment().get(13) != null) {
					arrowType = RangedDataLoader.getProjectileId(attacker.getEquipment().get(13).getId()).getArrowType();
				}
	
				attacker.getUpdateFlags().sendGraphic(CombatConstants.getPullBackDarkBow(arrowType), 0, 100);
				speed = 46 + (distance * 3) + (distance == 3 ? -4 : 0) + (distance >= 4 ? 2 : 0);
				World.sendProjectile(attacker.getPosition(), offsetX2, offsetY2, (arrowType == ArrowType.DRAGON_ARROW ? 1099 : 1101), 45, 28, speed, 42, 10, (arrowType == ArrowType.DRAGON_ARROW ? 95 : 50), 50, victim.isNpc() ? victim.getIndex() + 1 : -victim.getIndex() - 1);
				speed = 61 + (distance * 3) + (distance == 3 ? -5 : 0) + (distance >= 4 ? 2 : 0);
				World.sendProjectile(attacker.getPosition(), offsetX2, offsetY2, (arrowType == ArrowType.DRAGON_ARROW ? 1099 : 1102), 45, 28, speed, 54, 15, (arrowType == ArrowType.DRAGON_ARROW ? 95 : 50), 50, victim.isNpc() ? victim.getIndex() + 1 : -victim.getIndex() - 1);
				Combat.getInstance().delayHit(attacker, victim, (distance == 1 ? 1 : 2), false, true, 8 + DetermineHit.calculateHit(attacker, victim, true), attacker.getCombatState().getCombatStyle(), attacker.getAttackType(), 0, attacker.getEquipment().get(3), attacker.getEquipment().get(13));
				Combat.getInstance().delayHit(attacker, victim, (distance == 1 ? 1 : 2), false, true, 8 + DetermineHit.calculateHit(attacker, victim, true), attacker.getCombatState().getCombatStyle(), attacker.getAttackType(), 0, attacker.getEquipment().get(3), attacker.getEquipment().get(13));
				break;
		}

		if (weaponId == 1434 || weaponId == 6739) {
			Combat.getInstance().delayHit(attacker, victim, 1, false, true, DetermineHit.calculateHit(attacker, victim, true), attacker.getCombatState().getCombatStyle(), attacker.getAttackType(), 0, null, null);
		} else if ((weaponId != 1249 || weaponId != 1263 || weaponId != 3176 || weaponId != 5716 || weaponId != 5730) && weaponId != 861 && weaponId != 11235) {
			Combat.getInstance().delayHit(attacker, victim, 1, true, true, DetermineHit.calculateHit(attacker, victim, true), attacker.getCombatState().getCombatStyle(), attacker.getAttackType(), 0, null, null);
		}
		if (weaponId != 861 && weaponId != 11235) {
			attacker.getUpdateFlags().sendAnimation(anim, 0);
		}
		attacker.setSpecialAttackActive(false);
		attacker.setSpecialAmount(attacker.getSpecialAmount() - ((double) CombatConstants.getSpecialAttackData(weaponId)[1] / 10));
		attacker.getActionSender().updateSpecialBar();
	}

	public boolean specialActivated(Entity attacker) {
		return attacker.isPlayer() && ((Player) attacker).isSpecialAttackActive();
	}

	public void addSpecialBar(Player player) {
		int weaponId = -1;

		if (player.getEquipment().get(3) != null) {
			weaponId = player.getEquipment().get(3).getId();
		}

		if (WeaponLoader.getWeapon(weaponId) != null) {
			for (int index = 0; index < CombatConstants.WEAPON_INTERFACE_DATA.length; index++) {
				if (CombatConstants.WEAPON_INTERFACE_DATA[index][0] == WeaponLoader.getWeapon(weaponId).getInterfaceId() && CombatConstants.WEAPON_INTERFACE_DATA[index][23] != -1) {
					player.getActionSender().showComponent(CombatConstants.WEAPON_INTERFACE_DATA[index][23], (CombatConstants.getSpecialAttackData(weaponId) == null ? false : true));
				}
			}
		}
	}

	public void clickSpecialBar(Player player, int buttonId) {
		if (player.getEquipment().getItemContainer().get(3) == null) {
			return;
		}
		if (player.getAttribute("duel_button_sp_atk") != null) {
			player.getActionSender().sendMessage("You cannot use Special attacks in this duel session.");
			return;
		}

		int weaponId = player.getEquipment().getItemContainer().get(3).getId();

		switch (buttonId) {

		case 7462: // TODO: GMaul
			if ((50 / 10) <= player.getSpecialAmount() && player.isInstigatingAttack() && player.getCombatingEntity() != null) {
				if (!Combat.getInstance().meetsAttackRequirements(player, player.getCombatingEntity())) {
					Combat.getInstance().resetCombat(player);
					return;
				}
				player.setSpecialAttackActive(!player.isSpecialAttackActive());
				player.getUpdateFlags().sendGraphic(340, 0, 100);
				Combat.getInstance().delayHit(player, player.getCombatingEntity(), 1, true, true, DetermineHit.calculateHit(player, player.getCombatingEntity(), true), player.getCombatState().getCombatStyle(), player.getAttackType(), 0, null, null);
				player.getUpdateFlags().sendAnimation(1667, 0);
				player.setSpecialAmount(player.getSpecialAmount() - (50 / 10));
				player.setSpecialAttackActive(!player.isSpecialAttackActive());
				player.getActionSender().updateSpecialBar();
			}
			break;
		case 7487:// 1698
			if ((100 / 10) <= player.getSpecialAmount() && weaponId == 1377) {
				player.getUpdateFlags().sendGraphic(246, 0, 0);
				player.getUpdateFlags().sendForceMessage("Raarrrrrrgggggghhhhhhh!");
				player.getUpdateFlags().sendAnimation(1056, 0);
				player.setSpecialAttackActive(false);
				player.getSkill().getLevel()[Skill.STRENGTH] += 10 + (0.25 * (0.1 * player.getSkill().getLevel()[Skill.MAGIC]) + (0.1 * player.getSkill().getLevel()[Skill.RANGED]) + (0.1 * player.getSkill().getLevel()[Skill.DEFENCE]) + (0.1 * player.getSkill().getLevel()[Skill.ATTACK]));
				player.getSkill().getLevel()[Skill.ATTACK] *= 0.9;
				player.getSkill().getLevel()[Skill.DEFENCE] *= 0.9;
				player.getSkill().getLevel()[Skill.RANGED] *= 0.9;
				player.getSkill().getLevel()[Skill.MAGIC] *= 0.9;
				player.getSkill().refresh();
				player.setSpecialAmount(player.getSpecialAmount() - (100 / 10));
			}
			break;
		case 7537:// 1764 <- range
			if (weaponId == 861 || weaponId == 11235) {
				if (player.getEquipment().get(Constants.EQUIPMENT_SLOT_ARROWS) == null) {
					player.getActionSender().sendMessage(Language.NO_RANGED_AMMO);
					break;
				} else if (player.getEquipment().get(Constants.EQUIPMENT_SLOT_ARROWS).getCount() < 2) {
					player.getActionSender().sendMessage(Language.NOT_ENOUGH_ARROWS);
					break;
				}
			}
		case 7562:// 2276
		case 7612:// 3796
		case 7587: // 2423
		case 7662:// 4679
		case 7687:// 4705
		case 7788:// 7762
		case 8481:// 8460
		case 12311:// 12290
			if (buttonId == 7587 && (100 / 10) <= player.getSpecialAmount() && weaponId == 35) {
				player.getUpdateFlags().sendGraphic(247, 0, 0);
				player.getUpdateFlags().sendAnimation(1057, 0);
				player.getUpdateFlags().sendForceMessage("For Camelot!");
				player.setSpecialAttackActive(false);
				player.getSkill().getLevel()[Skill.DEFENCE] += 8;
				player.getSkill().refresh();
				player.setSpecialAmount(player.getSpecialAmount() - (100 / 10));
				player.getActionSender().updateSpecialBar();
				break;
			}

			if (CombatConstants.getSpecialAttackData(weaponId) == null) {
				break;
			}

			if (((double) CombatConstants.getSpecialAttackData(weaponId)[1] / 10) <= player.getSpecialAmount()) {
				player.setSpecialAttackActive(!player.isSpecialAttackActive());
				player.getActionSender().updateSpecialBar();
			} else {
				player.setSpecialAttackActive(false);
				player.getActionSender().updateSpecialBar();
				player.getActionSender().sendMessage(Language.OUT_OF_SPEC);
			}
			break;
		}
	}
}