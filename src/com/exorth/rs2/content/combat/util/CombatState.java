package com.exorth.rs2.content.combat.util;

import java.util.HashMap;
import java.util.Map;

import com.exorth.rs2.content.skills.Skill;

public class CombatState {
	/**
	 * The damage map of this entity.
	 */
	private DamageMap damageMap = new DamageMap();

	/**
	 * Gets the damage map of this entity.
	 * 
	 * @return The damage map.
	 */
	public DamageMap getDamageMap() {
		return damageMap;
	}

	/**
	 * The entity whose combat state is this.
	 */
	public static enum CombatStyle {
		ACCURATE(0, new int[]
		{ Skill.ATTACK }, new double[]
		{ 4 }),

		AGGRESSIVE(1, new int[]
		{ Skill.STRENGTH }, new double[]
		{ 4 }),

		AGGRESSIVE_1(2, new int[]
		{ Skill.STRENGTH }, new double[]
		{ 4 }),

		DEFENSIVE(3, new int[]
		{ Skill.DEFENCE }, new double[]
		{ 4 }),

		CONTROLLED_1(4, new int[]
		{ Skill.ATTACK, Skill.STRENGTH, Skill.DEFENCE }, new double[]
		{ 1.33, 1.33, 1.33 }),

		CONTROLLED_2(5, new int[]
		{ Skill.ATTACK, Skill.STRENGTH, Skill.DEFENCE }, new double[]
		{ 1.33, 1.33, 1.33 }),

		CONTROLLED_3(6, new int[]
		{ Skill.ATTACK, Skill.STRENGTH, Skill.DEFENCE }, new double[]
		{ 1.33, 1.33, 1.33 }),

		AUTOCAST(7, new int[]
		{ Skill.MAGIC }, new double[]
		{ 2 }),

		DEFENSIVE_AUTOCAST(8, new int[]
		{ Skill.MAGIC, Skill.DEFENCE }, new double[]
		{ 1.33, 1 });

		/**
		 * A map of combat styles.
		 */
		private static Map<Integer, CombatStyle> combatStyles = new HashMap<Integer, CombatStyle>();

		/**
		 * Gets a combat style by its ID.
		 * 
		 * @param combatStyle The combat style ID.
		 * @return The combat style, or <code>null</code> if the ID is not a combat style.
		 */
		public static CombatStyle forId(int combatStyle) {
			return combatStyles.get(combatStyle);
		}

		/**
		 * Populates the combat style map.
		 */
		static {
			for (CombatStyle combatStyle : CombatStyle.values()) {
				combatStyles.put(combatStyle.id, combatStyle);
			}
		}

		/**
		 * The combat style's id.
		 */
		private int id;

		/**
		 * The skills this combat style adds experience to a skill.
		 */
		private int[] skills;

		/**
		 * The amounts of experience this combat style adds.
		 */
		private double[] experiences;

		private CombatStyle(int id, int[] skills, double[] experiences) {
			this.id = id;
			this.skills = skills;
			this.experiences = experiences;
		}

		/**
		 * Gets the combat style's id.
		 * 
		 * @return The combat style's id.
		 */
		public int getId() {
			return id;
		}

		/**
		 * Gets the skills this attack type adds experience to.
		 * 
		 * @return The skills this attack type adds experience to.
		 */
		public int[] getSkills() {
			return skills;
		}

		/**
		 * Gets a skill this attack type adds experience to by its index.
		 * 
		 * @param index The skill index.
		 * @return The skill this attack type adds experience to by its index.
		 */
		public int getSkill(int index) {
			return skills[index];
		}

		/**
		 * Gets the experience amounts this attack type adds.
		 * 
		 * @return The experience amounts this attack type adds.
		 */
		public double[] getExperiences() {
			return experiences;
		}

		/**
		 * Gets an amount of experience this attack type adds by its index.
		 * 
		 * @param index The experience index.
		 * @return The amount of experience this attack type adds by its index.
		 */
		public double getExperience(int index) {
			return experiences[index];
		}
	}

	/**
	 * The entity's combat style.
	 */
	private CombatStyle combatStyle = CombatStyle.ACCURATE;

	public void setCombatStyle(CombatStyle combatStyle) {
		this.combatStyle = combatStyle;
	}

	public CombatStyle getCombatStyle() {
		return combatStyle;
	}
}