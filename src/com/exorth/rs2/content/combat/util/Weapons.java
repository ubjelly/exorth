package com.exorth.rs2.content.combat.util;

/**
 * Handles all data loading related to weapons
 * loads from xml.
 * 
 * TODO: Make it load equipment turn anims aswell in equipEmotes..
 *
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.exorth.rs2.Constants;
import com.exorth.rs2.content.combat.util.CombatState.CombatStyle;
import com.exorth.rs2.io.XStreamController;
import com.exorth.rs2.model.players.Player;

public class Weapons {
	private int[] id;
	private int[] equipEmotes = new int[3];
	private int[] attackEmotes = new int[8];
	private int[] sounds = new int[8];
	private int blockEmote;
	private boolean is2h;
	private int speed;
	private int interfaceId;

	public int[] getId() {
		return id;
	}

	public int getStandEmote() {
		if (equipEmotes == null) {
			return 0;
		}
		return equipEmotes[0];
	}

	public int getWalkEmote() {
		if (equipEmotes == null) {
			return 0;
		}
		return equipEmotes[1];
	}

	public int getRunEmote() {
		if (equipEmotes == null) {
			return 0;
		}
		return equipEmotes[2];
	}

	public int getSound(int fightStyle) {
		if (sounds == null) {
			return -1;
		}
		return sounds[fightStyle];
	}

	public int getAttackEmote(int fightStyle) {
		if (attackEmotes == null) {
			return -1;
		}
		return attackEmotes[fightStyle];
	}

	public int getBlockEmote() {
		return blockEmote;
	}

	public int getAttackSpeed() {
		return speed;
	}

	public int getInterfaceId() {
		return interfaceId;
	}

	public boolean getIs2h() {
		return is2h;
	}

	public static class WeaponLoader {
		private final static Logger logger = Logger.getLogger(Weapons.class.getName());
		private static Map<Integer, Weapons> weapons;

		public static Weapons getWeapon(int id) {
			return weapons.get(id);
		}

		@SuppressWarnings("unchecked")
		public static void loadWeapons() throws FileNotFoundException {
			weapons = new HashMap<Integer, Weapons>();
			XStreamController.getInstance();

			List<Weapons> loaded = (ArrayList<Weapons>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/combat/weapons.xml"));
			for (Weapons weapon : loaded) {
				for (int id : weapon.getId()) {
					weapons.put(id, weapon);
				}
			}
			logger.info("Loaded " + weapons.size() + " Weapon definitions.");
		}
	}

	public static int getWeaponSpeed(Player player) {
		int speed = 4;
		if (player.getEquipment().getItemContainer().get(3) == null) {
			return speed;
		}
		int weaponId = player.getEquipment().getItemContainer().get(3).getId();

		if (WeaponLoader.getWeapon(weaponId) != null) {
			speed = WeaponLoader.getWeapon(weaponId).getAttackSpeed();
		}
		return speed;
	}

	public static int getAttackAnimation(Player player) {
		int animation = 422;
		if (player.getEquipment().getItemContainer().get(3) == null) {
			if (player.getCombatState().getCombatStyle() == CombatStyle.AGGRESSIVE) {
				return 423;
			} else {
				return animation;
			}
		}
		int weaponId = player.getEquipment().getItemContainer().get(3).getId();

		if (WeaponLoader.getWeapon(weaponId) != null) {
			animation = WeaponLoader.getWeapon(weaponId).getAttackEmote(player.getCombatState().getCombatStyle().getId());
		} else {
			return animation;
		}
		return animation;
	}

	public static int getBlockAnimation(Player player) {
		int animation = 424;
		if (player.getEquipment().getItemContainer().get(5) == null) {
			if (player.getEquipment().getItemContainer().get(3) == null) {
				return animation;
			}
			int weaponId = player.getEquipment().getItemContainer().get(3).getId();
			if (WeaponLoader.getWeapon(weaponId) != null && WeaponLoader.getWeapon(weaponId).getBlockEmote() > 0) {
				animation = WeaponLoader.getWeapon(weaponId).getBlockEmote();
			}
		} else {
			int shieldId = player.getEquipment().getItemContainer().get(5).getId();
			if (WeaponLoader.getWeapon(shieldId) != null && WeaponLoader.getWeapon(shieldId).getBlockEmote() > 0) {
				animation = WeaponLoader.getWeapon(shieldId).getBlockEmote();
			} else {
				return 1156;
			}
		}
		return animation;
	}

	public static int getWalkOrRunAnimation(Player player, boolean isRunning) {
		int returnAnim = 0x333;
		int weaponId = player.getEquipment().getItemContainer().get(3).getId();
		switch (weaponId) {
			case 4565: // Basket of Eggs
				return 1836;
			case 6818: // Bowsword animation from original Winterlove based servers
				return 1765;
			case 4084:
				if (isRunning) {
					return 1467;
				}
				return 1468;
			default:
				if (WeaponLoader.getWeapon(weaponId) != null && (WeaponLoader.getWeapon(weaponId).getWalkEmote() != 0 || WeaponLoader.getWeapon(weaponId).getRunEmote() != 0)) {
					if (!isRunning) {
						returnAnim = WeaponLoader.getWeapon(weaponId).getWalkEmote();
					} else {
						returnAnim = WeaponLoader.getWeapon(weaponId).getRunEmote();
					}
				} else if (!isRunning) {
					return returnAnim;
				} else {
					return 0x338;
				}
		}
		return returnAnim;
	}

	public static int getStandAnimation(Player player) {
		int returnAnim = 0x328;
		if (player.getEquipment().getItemContainer().get(3) == null) {
			return 0x338;
		}
		int weaponId = player.getEquipment().getItemContainer().get(3).getId();

		switch (weaponId) {
			case 4084:
				return 1461;
			default:
				if (WeaponLoader.getWeapon(weaponId) != null && WeaponLoader.getWeapon(weaponId).getStandEmote() != 0) {
					returnAnim = WeaponLoader.getWeapon(weaponId).getStandEmote();
				}
		}
		return returnAnim;
	}

	public static boolean isTwoHanded(int itemId) {
		boolean twoHanded = false;
		if (WeaponLoader.getWeapon(itemId) != null) {
			twoHanded = WeaponLoader.getWeapon(itemId).getIs2h();
		}
		return twoHanded;
	}

	public static int getWeaponInterface(Player player) {
		int weaponInterface = 5855;
		if (player.getEquipment().getItemContainer().get(3) == null) {
			return weaponInterface;
		}
		int weaponId = player.getEquipment().getItemContainer().get(3).getId();
		if (WeaponLoader.getWeapon(weaponId) != null) {
			weaponInterface = WeaponLoader.getWeapon(weaponId).getInterfaceId();
		}
		return weaponInterface;
	}
}