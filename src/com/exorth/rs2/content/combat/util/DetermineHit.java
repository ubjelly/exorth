package com.exorth.rs2.content.combat.util;

import com.exorth.rs2.content.combat.magic.Magic;
import com.exorth.rs2.content.combat.magic.SpellLoader;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Misc;

/**
 * +1 to hits as they are exclusive
 * 
 * @author AkZu
 * 
 */
public class DetermineHit {
	public static int determineHit(Entity attacker, Entity victim, int hit_amount, boolean isSpecial) {
		double chance = Misc.random(100);
		double offensive = 0.0;
		double defensive = 0.0;

		if (attacker.getAttackType() == Entity.AttackTypes.MAGIC) {
			offensive = CombatFormula.getMagicAccuracy(attacker, false);
			defensive = CombatFormula.getMagicAccuracy(victim, true);
		} else if (attacker.getAttackType() == Entity.AttackTypes.RANGED) {
			offensive = CombatFormula.getRangedAccuracy(attacker, isSpecial, false);
			defensive = CombatFormula.getRangedAccuracy(victim, isSpecial, true);

			// Item arrows = ((Player) attacker).getEquipment().get(13);
			// ArrowType arrowType = null;

			// if (arrows != null)
			// arrowType =
			// RangedDataLoader.getProjectileId(arrows.getId()).getArrowType();

			/*
			 * if (arrowType != null && Misc.random(10) == 3) { // Diamond bolts
			 * if (arrowType == ArrowType.DIAMOND_BOLT) { offensive *= 1.3; if
			 * (chance < accuracy) victim.getUpdateFlags().sendGraphic(758); } }
			 */
		} else if (attacker.getAttackType() == Entity.AttackTypes.MELEE) {
			offensive = CombatFormula.getMeleeAccuracy(attacker, victim, isSpecial, false);
			defensive = CombatFormula.getMeleeAccuracy(attacker, victim, isSpecial, true);
		}

		double accuracy = (offensive < defensive ? Math.round(((offensive - 1) / (2 * defensive)) * 100) : Math.round((1 - (defensive + 1) / (2 * offensive)) * 100));

		if (attacker.isPlayer() && ((Player) attacker).isDebugging()) {
			((Player) attacker).getActionSender().sendMessage("[<col=CC0000>Hit Chance<col=0>]:<col=255> " + (int) accuracy + "%");
		}

		return chance > accuracy ? 0 : hit_amount;
	}

	public static int calculateHit(Entity attacker, Entity victim, boolean isSpecial) {
		int hit = 0;

		if (attacker.isPlayer()) {
			if (attacker.getAttackType() == Entity.AttackTypes.MAGIC) {
				switch (SpellLoader.getSpellDefinitions()[Magic.getInstance().getMagicIndex(attacker)].getSpellId()) {
					case 1190:
					case 1191:
					case 1192:
						hit = 1 + Misc.random(SpellLoader.getSpellDefinitions()[Magic.getInstance().getMagicIndex(attacker)].getMaxHit() - 1 + (attacker.getAttribute("charge") != null ? 10 : 0));
						break;
					default:
						hit = 1 + Misc.random(SpellLoader.getSpellDefinitions()[Magic.getInstance().getMagicIndex(attacker)].getMaxHit() - 1);
						break;
				}
			} else if (attacker.getAttackType() == Entity.AttackTypes.RANGED) {
				hit = Misc.random((int) CombatFormula.getRangedMaxHit(attacker, isSpecial));
			} else if (attacker.getAttackType() == Entity.AttackTypes.MELEE) {
				hit = Misc.random((int) CombatFormula.getMeleeMaxHit(attacker, victim, isSpecial));
			}
		} else {
			hit = Misc.random(((Npc) attacker).getDefinition().getMaxHit() + 1);
		}
		return hit;
	}
}