package com.exorth.rs2.content.combat.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.exorth.rs2.Constants;
import com.exorth.rs2.io.XStreamController;

/**
 * @author AkZu
 * @author Joshua Barry
 * 
 */
public class LevelRequirements {
	private int[] id;
	private int[] levelId;
	private int[] requiredLevel;

	public int[] getId() {
		return id;
	}

	public int[] getLevelId() {
		return levelId;
	}

	public int[] getRequiredLevel() {
		return requiredLevel;
	}

	public static class LevelRequirementLoader {
		private static Map<Integer, LevelRequirements> reqs;

		public static LevelRequirements getRequirement(int id) {
			return reqs.get(id);
		}
		@SuppressWarnings("unchecked")
		public static void loadRequirements() throws FileNotFoundException {
			reqs = new HashMap<Integer, LevelRequirements>();
			XStreamController.getInstance();

			List<LevelRequirements> loaded = (ArrayList<LevelRequirements>) XStreamController.getXStream().fromXML(new FileInputStream(Constants.CONFIG_DIRECTORY + "content/combat/levelrequirements.xml"));
			for (LevelRequirements requirement : loaded) {
				for (int id : requirement.getId()) {
					reqs.put(id, requirement);
				}
			}
			Logger.getAnonymousLogger().info("Loaded " + reqs.size() + " Level requirement definitions.");
		}
	}
}