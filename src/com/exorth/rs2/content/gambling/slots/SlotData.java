package com.exorth.rs2.content.gambling.slots;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import com.exorth.rs2.Server;
import com.exorth.rs2.model.players.Yeller;
import com.exorth.rs2.util.SQLConnector;

/**
 * Handles the saving and loading of the slots.
 * @author Stephen
 */
public class SlotData {

	private final static Logger logger = Logger.getLogger(SlotData.class.getName());

	private static int slotsBalance;
	private static int jackpot;

	public static void loadData() {
		try {
			SQLConnector.serverConnection();
			String query = "SELECT * FROM casino";
			ResultSet loading = SQLConnector.serverConn.createStatement().executeQuery(query);
			while (loading.next()) {
				slotsBalance = loading.getInt("slotsbalance");
				jackpot = loading.getInt("jackpot");
			}
			loading.close();
			Server.getSingleton().setSlotsEnabled(true);
		} catch(SQLException e) {
			logger.severe("Unable to load slot data!");
			e.printStackTrace();
			Server.getSingleton().setSlotsEnabled(false);
			Yeller.alertStaff("Slots are disabled, alert an administrator.");
		}
	}
	
	public static void saveData() {
		try {
			SQLConnector.serverConnection();
			Statement statement = SQLConnector.serverConn.createStatement();
			String query = "UPDATE casino SET slotsbalance ='" + getSlotsBalance() + "', jackpot ='" + getJackpot() + "'";
			statement.executeUpdate(query);
			statement.close();
			Server.getSingleton().setSlotsEnabled(true);
		} catch (Exception e) {
			logger.severe("Unable to save slot data!");
			e.printStackTrace();
			Server.getSingleton().setSlotsEnabled(false);
			Yeller.alertStaff("Slots are disabled, alert an administrator.");
		}
	}

	public static int getSlotsBalance() {
		return slotsBalance;
	}

	public void updateSlotsBalance(int update) {
		slotsBalance += update;
	}

	public void setSlotsBalance(int newBalance) {
		slotsBalance = newBalance;
	}

	public static int getJackpot() {
		return jackpot;
	}

	public void updateJackpot(int update) {
		jackpot += update;
	}

	public void setJackpot(int newJackpot) {
		jackpot = newJackpot;
	}
}
