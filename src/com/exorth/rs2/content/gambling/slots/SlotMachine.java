package com.exorth.rs2.content.gambling.slots;

import java.util.ArrayList;

import com.exorth.rs2.Server;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Yeller;
import com.exorth.rs2.task.Task;

/**
 * Virtual slot machine -- ya dig?
 * @author Some stuff stolen from <code>Exorth</code>
 * @author Stephen
 */
public class SlotMachine {

	ArrayList<Symbol> symbols = new ArrayList<Symbol>();
	SlotData sd = Server.getSlotData();

	private static final Item SLOT_COST = new Item(995, 1000);

	private int plays = 0;

	public SlotMachine() {
		symbols.add(new Symbol(1, "X", new int[] {0, 1, 2, 3, 4, 5, 6, 7, 50}));
		symbols.add(new Symbol(2, "5", new int[] {13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 11}));
		symbols.add(new Symbol(3, "4", new int[] {25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 9, 8}));
		symbols.add(new Symbol(4, "3", new int[] {38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 24, 10}));
		symbols.add(new Symbol(5, "2", new int[] {51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 12}));
		symbols.add(new Symbol(6, "7", new int[] {63, 62}));
		sd.loadData();
	}

	public Spin spin() {
		Symbol[] symbols = new Symbol[3];
		for (int i = 0; i < 3; i++) {
			int number = (int)((Math.random() * 999999999) + 1);
			int stop = number % 64;
			Symbol s = translate(stop);
			symbols[i] = s;
		}
		return new Spin(symbols);
	}

	private Symbol translate(int stop) {
		for (Symbol s : symbols) {
			if (s.check(stop)) {
				return s;
			}
		}
		return null;
	}

	public void playSlotsManual(Player player, SlotMachine slots) {
		if (!Server.getSingleton().slotsEnabled()) {
			player.getActionSender().sendMessage("Slots have been temporarily disabled, we apologize for the inconvienance.");
			return;
		}
		if (!player.getInventory().getItemContainer().contains(995)) {
			player.getActionSender().sendMessage("You don't have any gold to play with.");
			return;
		}

		Spin s = slots.spin();

		if (!canPlay(player)) {
			return;
		}

		player.getInventory().removeItem(SLOT_COST);
		sd.updateSlotsBalance(SLOT_COST.getCount());
		sd.updateJackpot((int) Math.round(SLOT_COST.getCount() * .25));

		int win = s.getWinnings();
		player.getActionSender().sendString("EXORTH SLOTS CO - JACKPOT: Manual play", 13896);
		player.getActionSender().sendString(s.getSymbols()[0].output(), 13884);
		player.getActionSender().sendString(s.getSymbols()[1].output(), 13885);
		player.getActionSender().sendString(s.getSymbols()[2].output(), 13886);

		if (win > 0) {
			if (win < 240000) {
				player.getInventory().addItem(new Item(995, s.getWinnings()));
				sd.setSlotsBalance(sd.getSlotsBalance() - s.getWinnings());
				player.getActionSender().sendString(s.getWinnings() + "gp", 17501);
			} else {
				player.getActionSender().sendMessage("You have won the jackpot! There is a 15% tax on winnings.");
				int amount = (int) (s.getWinnings() * 0.85);
				player.getInventory().addItem(new Item(995, amount));
				player.getActionSender().sendMessage("You receive " + amount + " gp.");
				player.getActionSender().sendString("Jackpot!", 17501);
				jackpotWon(player);
			}
		} else {
			player.getActionSender().sendString("0gp", 17501);
		}
		sd.saveData();
	}
	
	public void playSlotsAutomatic(final Player player, final SlotMachine slots, final int spins) {
		if (!player.getInventory().getItemContainer().contains(995)) {
			player.getActionSender().sendMessage("You don't have any gold to play with.");
			return;
		}

		player.getActionSender().sendInterface(671);
		player.getActionSender().sendString("EXORTH SLOTS CO - JACKPOT: " + spins + " plays left", 13896);

		World.submit(new Task(4) {
			@Override
			public void execute() {
				if (!canPlay(player)) {
					return;
				}

				int playsLeft = spins - plays;
				Spin s = slots.spin();

				int win = s.getWinnings();

				player.getInventory().removeItem(SLOT_COST);
				sd.updateSlotsBalance(SLOT_COST.getCount());
				sd.updateJackpot((int) Math.round(SLOT_COST.getCount() * .25));

				player.getActionSender().sendString("EXORTH SLOTS CO - JACKPOT: " + playsLeft + " plays left", 13896);
				player.getActionSender().sendString(s.getSymbols()[0].output(), 13884);
				player.getActionSender().sendString(s.getSymbols()[1].output(), 13885);
				player.getActionSender().sendString(s.getSymbols()[2].output(), 13886);

				setPlays(plays + 1);

				if (win > 0) {
					if (win < 240000) {
						player.getInventory().addItem(new Item(995, s.getWinnings()));
						sd.setSlotsBalance(sd.getSlotsBalance() - s.getWinnings());
						player.getActionSender().sendString(s.getWinnings() + "gp", 17501);
					} else {
						player.getActionSender().sendMessage("You have won the jackpot! There is a 15% tax on winnings.");
						int amount = (int) (s.getWinnings() * 0.85);
						player.getInventory().addItem(new Item(995, amount));
						player.getActionSender().sendMessage("You receive " + amount + " gp.");
						player.getActionSender().sendString("Jackpot!", 17501);
						jackpotWon(player);
					}
				} else {
					player.getActionSender().sendString("0gp", 17501);
				}
				sd.saveData();
				if (plays <= spins) {
					return;
				} else {
					setPlays(0);
					player.getActionSender().sendMessage("You have run out of spins.");
					this.stop();
				}
			}
		});
	}

	private void setPlays(int number) {
		this.plays = number;
	}

	public void jackpotWon(Player player) {
		Yeller.shout(player.getName() + " has won the " + sd.getJackpot() + " gp jackpot at the slots!");
		sd.setJackpot(0);
		if (sd.getJackpot() >= 240000) {
			sd.updateSlotsBalance((int) (sd.getJackpot() * 0.15));
		}
	}

	private boolean canPlay(Player player) {
		if (!player.getInventory().getItemContainer().contains(SLOT_COST)) {
			player.getActionSender().removeInterfaces();
			player.getActionSender().sendMessage("You don't have enough gold to play!");
			return false;
		}
		return true;
	}
}
