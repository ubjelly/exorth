package com.exorth.rs2.content.gambling.dice;

import com.exorth.rs2.Constants;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Player;

/**
 * The dicing action that takes place when a player is betting Party Pete.
 * @author Stephen
 * @author Mark
 */
public class PeteDicing extends DicingAction {

	/**
	 * The player dicing Pete.
	 */
	private Player player;

	/**
	 * The player's roll.
	 */
	private int playerRoll;

	/**
	 * Pete's roll.
	 */
	private int peteRoll;

	/**
	 * The player's bet.
	 */
	private Item playerBet;

	/**
	 * Constructs a dicing game with Party Pete.
	 * @param player The player dicing Pete.
	 */
	public PeteDicing(Player player, int playerBet) {
		this.player = player;
		playerRoll = super.getRoll();
		peteRoll = super.getRoll();
		this.playerBet = new Item(995, playerBet);
	}

	/**
	 * Does the player meet the prerequisites to play?
	 */
	private boolean canDice() {
		if (!player.getInventory().getItemContainer().contains(playerBet)) {
			player.getActionSender().sendMessage("You don't have enough money to bet that much!");
			player.getActionSender().removeInterfaces();
			return false;
		} else if (!player.getInventory().getItemContainer().contains(995)) {
			player.getActionSender().sendMessage("You don't have any money to bet!");
			return false;
		}
		return true;
	}

	/**
	 * The dicing game!
	 */
	public void playPete() {
		if (!canDice()) {
			return;
		}
		configureInterface(player, player.getUsername(), "Party Pete", playerRoll, peteRoll);
	
		player.getInventory().removeItem(playerBet);
		if (playerRoll > peteRoll) {
			player.getActionSender().sendString("You win!", super.GAME_RESULT);
			int maxCashDiff = Item.MAX_AMOUNT - (player.getInventory().getItemContainer().getCount(995) + (playerBet.getCount() * 2));
			if (maxCashDiff < 0) {
				if (player.getBank().getById(995) != null) {
					maxCashDiff = Item.MAX_AMOUNT - (player.getBank().getById(995).getCount() + (playerBet.getCount() * 2));
				}
				if (player.getBank().getById(995) != null && maxCashDiff < 0) {
					player.getActionSender().sendMessage("Max cash twice!? What a legend!");
					player.getActionSender().sendMessage("However, your winnings have been dropped, and you have been refunded your bet.");
					ItemManager.getInstance().createGroundItem(player, player.getUsername(), new Item(995, playerBet.getCount()), player.getPosition(), Constants.GROUND_START_TIME_DROP);
					player.getInventory().addItem(playerBet);
				} else {
					player.getBank().add(new Item (995, playerBet.getCount() * 2));
					player.getInventory().addItem(playerBet);
					player.getActionSender().sendMessage("Your inventory does not contain enough space for your winnings.");
					player.getActionSender().sendMessage("As a result it will be added to your bank.");
				}
			} else {
				player.getInventory().addItem(new Item(995, playerBet.getCount() * 2));
			}
		} else if (peteRoll > playerRoll) {
			player.getActionSender().sendString("You lose!", super.GAME_RESULT);
		} else {
			player.getActionSender().sendString("It's a tie!", super.GAME_RESULT);
			player.getInventory().addItem(playerBet);
		}
	}
}
