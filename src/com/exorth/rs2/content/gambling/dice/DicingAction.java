package com.exorth.rs2.content.gambling.dice;

import java.util.Random;

import com.exorth.rs2.model.players.Player;

/**
 * A custom dicing minigame. A player can either play against Party Pete where they can bet any amount of money and
 * Pete will match the bet. Upon winning the player will receive both of the bets, losing results in the player losing their
 * bet, and a tie results in the player getting their bet back.
 * 
 * Another option is to dice another player. When dicing another player, each of the participants will be able to bet numerous
 * items that WILL NOT have to be monetary equivalents. The same rules apply as dicing with Party Pete. The player who wins will
 * receive all of the items, while the losing player receives nothing. In the case of a tie, each player is rewarded with
 * the items they bet.
 * 
 * @author Stephen
 */
public class DicingAction {

	/**
	 * The dicing interface.
	 */
	public final int DICING_INTERFACE = 6675;
	
	/**
	 * The betting interface, aka trading interface.
	 */
	public final int BETTING_INTERFACE = 3323;
	
	/**
	 * The inventory id.
	 */
	public final int INVENTORY_ID = 3321;

	/**
	 * The child id of the versing header.
	 */
	public final int VERSING_HEADER = 7815;

	/**
	 * The child id of the player playing header.
	 */
	public final int PLAYING_HEADER = 8399;

	/**
	 * The child id of the opponents roll.
	 */
	public final int OPPONENT_ROLL = 8424;

	/**
	 * The child id of the player's roll.
	 */
	public final int PLAYER_ROLL = 8425;
	
	/**
	 * The child id of the game result.
	 */
	public final int GAME_RESULT = 8426;

	/**
	 * The rolls a player is able to get.
	 */
	private int[] possibleRolls = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};

	/**
	 * Simulates a roll.
	 * @return A random index of possibleRolls.
	 */
	public int getRoll() {
		Random random = new Random();
		return possibleRolls[random.nextInt(possibleRolls.length)];
	}
	
	/**
	 * Sends the interface and replaces the default text on the dicing interface.
	 * @param Player The player the interface is being sent to.
	 * @param playerName The name of the player who is playing.
	 * @param opponentName The name of the player's opponent.
	 * @param playerRoll The roll of the player.
	 * @param opponentRoll The roll of the opponent.
	 */
	public void configureInterface(Player player, String playerName, String opponentName, int playerRoll, int opponentRoll) {
		player.getActionSender().sendInterface(DICING_INTERFACE);
		player.getActionSender().sendString(opponentName, VERSING_HEADER);
		player.getActionSender().sendString(playerName, PLAYING_HEADER);
		player.getActionSender().sendString("" + opponentRoll, OPPONENT_ROLL);
		player.getActionSender().sendString("" + playerRoll, PLAYER_ROLL);
	}
}
