package com.exorth.rs2.content.gambling.dice;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Inventory;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Yeller;
import com.exorth.rs2.model.players.Player.DiceStage;
import com.exorth.rs2.network.security.DiceLog;
import com.exorth.rs2.util.NameUtility;

/**
 * The dicing action that takes place when a player is betting another player.
 * I like quaaludes.
 * TODO: Class is basically done, last bug is duplicating items coming from somewhere.
 * @author Stephen
 */
public class PlayerDicing extends DicingAction {
	
	/**
	 * The player's roll.
	 */
	private int playerRoll;
	
	/**
	 * The items the player is betting.
	 */
	private ArrayList<Item> playerBet = new ArrayList<Item>();
	
	/**
	 * The opponent's roll.
	 */
	private int opponentRoll;
	
	/**
	 * The items the opponent is betting.
	 */
	private ArrayList<Item> opponentBet = new ArrayList<Item>();
	
	/**
	 * Handles the sending and accepting/declining process of a dice game.
	 */
	public void handleDiceRequest(final Player player, final Player opponent) {
		if (player == null || !player.isLoggedIn() || opponent == null || !opponent.isLoggedIn()) {
			return;
		}
		
		/**
		 * In the event someone successfully creates a cheat client...
		 */
		if (opponent.getIndex() == player.getIndex()) {
			Yeller.alertStaff(player.getName() + " may be using a cheat client! Reason: Dicing themselves.");
			return;
		}
		
		if (opponent.getDiceStage().equals(DiceStage.WAITING)) {
			player.getActionSender().sendMessage("Sending dice offer...");
			opponent.getActionSender().sendMessage(NameUtility.uppercaseFirstLetter(player.getUsername()) + ":dicereq:");
			player.setDiceStage(DiceStage.SEND_REQUEST);
		} else if (opponent.getDiceStage().equals(DiceStage.SEND_REQUEST) && opponent.getClickId() == player.getIndex()) {
			player.setDiceStage(DiceStage.SEND_REQUEST_ACCEPT);
			opponent.setDiceStage(DiceStage.SEND_REQUEST_ACCEPT);
			sendBettingWindow(player, opponent);
		} else {
			player.getActionSender().sendMessage("Sending dice offer...");
			opponent.getActionSender().sendMessage(NameUtility.uppercaseFirstLetter(player.getUsername()) + ":dicereq:");
			player.setDiceStage(DiceStage.WAITING);
		}
	}
	
	/**
	 * Refreshes the window which displays the bets and names of the players involved in the game.
	 */
	private void refresh(Player player, Player opponent) {
		player.getActionSender().sendUpdateItems(3322, player.getInventory().getItemContainer().toArray());
		opponent.getActionSender().sendUpdateItems(3322, opponent.getInventory().getItemContainer().toArray());

		player.getInventory().refresh();
		opponent.getInventory().refresh();

		player.getActionSender().sendUpdateItems(3415, player.getDice().toArray());
		player.getActionSender().sendUpdateItems(3416, opponent.getDice().toArray());

		opponent.getActionSender().sendUpdateItems(3415, opponent.getDice().toArray());
		opponent.getActionSender().sendUpdateItems(3416, player.getDice().toArray());

		player.getActionSender().sendString("", 3431);
		opponent.getActionSender().sendString("", 3431);

		player.getActionSender().sendString("@lre@Dicing With: " + opponent.getUsername(), 3417);
		player.getActionSender().sendString(opponent.getUsername(), 8482);
		player.getActionSender().sendString("has " + opponent.getInventory().getItemContainer().freeSlots() + " free", 8483);
		player.getActionSender().sendString("inventory slots", 8484);

		opponent.getActionSender().sendString("@lre@Dicing With: " + player.getUsername(), 3417);
		opponent.getActionSender().sendString(player.getUsername(), 8482);
		opponent.getActionSender().sendString("has " + player.getInventory().getItemContainer().freeSlots() + " free", 8483);
		opponent.getActionSender().sendString("inventory slots", 8484);
	}
	
	/**
	 * Sends the interface used for betting items. In this case, just using the default trading interface.
	 */
	public void sendBettingWindow(Player player, Player opponent) {
		refresh(player, opponent);
		player.getActionSender().sendItfInvOverlay(BETTING_INTERFACE, INVENTORY_ID);
		opponent.getActionSender().sendItfInvOverlay(BETTING_INTERFACE, INVENTORY_ID);
	}
	
	/**
	 * Declines a dicing game.
	 */
	public void declineBet(Player player) {
		if (!(player.getDiceStage().equals(DiceStage.ACCEPT) || player.getDiceStage().equals(DiceStage.SECOND_DICE_WINDOW) || player.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT))) {
			return;
		}
		
		Player opponent = World.getPlayer(player.getClickId());

		if (opponent == null) {
			return;
		}
		
		if (opponent.getClickId() != player.getIndex()) {
			return;
		}
		
		if (opponent.getDiceStage().equals(DiceStage.ACCEPT) || opponent.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT) || opponent.getDiceStage().equals(DiceStage.SECOND_DICE_WINDOW)) {
			opponent.getActionSender().sendMessage("Other player has declined the bet.");
			player.getActionSender().sendMessage("You decline the bet.");
		}
		
		player.getActionSender().removeInterfaces();
		opponent.getActionSender().removeInterfaces();

		player.setDiceStage(DiceStage.WAITING);
		opponent.setDiceStage(DiceStage.WAITING);

		giveBackItems(player);

		player.setClickId(-1);
	}
	
	/**
	 * Gives back the items already in the betting interface.
	 */
	private void giveBackItems(Player player) {
		Player opponent = World.getPlayer(player.getClickId());

		for (int i = 0; i < Inventory.SIZE; i++) {
			if (player.getDice().get(i) != null) {
				Item item = player.getDice().get(i);
				if (item != null) {
					player.getDice().remove(item);
					player.getInventory().addItem(item);
				}
			}
		}
		for (int i = 0; i < Inventory.SIZE; i++) {
			if (opponent.getDice().get(i) != null) {
				Item item = opponent.getDice().get(i);
				if (item != null) {
					opponent.getDice().remove(item);
					opponent.getInventory().addItem(item);
				}
			}
		}
	}
	
	/**
	 * Sends an item to the betting interface and removes it from the player's inventory.
	 * @param slot The inventory slot of the item being bet.
	 * @param betItem The item being bet.
	 * @param amount The amount of the item being bet.
	 */
	public void betItem(Player player, int slot, int betItem, int amount) {
		Player opponent = World.getPlayer(player.getClickId());

		if (opponent.getClickId() != player.getIndex()) {
			return;
		}

		if (player.getDiceStage().equals(DiceStage.SECOND_DICE_WINDOW)) {
			return;
		}
		
		if (player == null || !player.isLoggedIn() || opponent == null || !opponent.isLoggedIn()) {
			return;
		}
		
		if (betItem == -1) {
			return;
		}
		
		Item inv = player.getInventory().getItemContainer().get(slot);
		int invAmount = player.getInventory().getItemContainer().getCount(betItem);
		if (inv == null) {
			return;
		}
		
		if (inv.getId() != betItem) {
			return;
		}
		
		if (inv.getId() <= 0 || betItem <= 0 || inv.getCount() < 1 || amount < 1) {
			return;
		}

		if (ItemManager.getInstance().isUntradeable(betItem)) {
			player.getActionSender().sendMessage("You can't bet that item.");
			return;
		}

		if (invAmount > amount) {
			invAmount = amount;
		}
		
		if (inv.getDefinition().isStackable()) {
			player.getInventory().removeItemSlot(slot, amount);
		} else {
			for (int i = 0; i < invAmount; i++) {
				if (invAmount == 1) {
					player.getInventory().removeItemSlot(slot);
				} else {
					player.getInventory().removeItem(new Item(betItem, 1));
				}
			}
		}
		
		int betAmount = player.getDice().getCount(betItem);
		if (betAmount > 0 && inv.getDefinition().isStackable()) {
			player.getDice().set(player.getDice().getSlotById(inv.getId()), new Item(betItem, betAmount + invAmount));
		} else {
			player.getDice().add(new Item(inv.getId(), invAmount));
		}
		
		refresh(player, opponent);
		player.setDiceStage(DiceStage.SEND_REQUEST_ACCEPT);
		opponent.setDiceStage(DiceStage.SEND_REQUEST_ACCEPT);
	}
	
	/**
	 * Removes an item that has already been sent to the betting interface.
	 * @param slot The inventory slot of the item being bet.
	 * @param betItem The item being bet.
	 * @param amount The amount of the item being bet.
	 */
	public void removeBetItem(Player player, int slot, int betItem, int amount) {
		Player opponent = World.getPlayer(player.getClickId());

		if (opponent.getClickId() != player.getIndex()) {
			return;
		}

		if (player.getDiceStage().equals(DiceStage.SECOND_DICE_WINDOW)) {
			return;
		}
		
		if (player == null || !player.isLoggedIn() || opponent == null || !opponent.isLoggedIn()) {
			return;
		}
		
		if (betItem == -1 || amount == 0) {
			return;
		}
		
		Item itemOnScreen = player.getDice().get(slot);
		int itemOnScreenAmount = player.getDice().getCount(betItem);
		if (itemOnScreen == null || itemOnScreen.getId() <= 0 || itemOnScreen.getId() != betItem) {
			return;
		}
		
		if (itemOnScreenAmount > amount) {
			itemOnScreenAmount = amount;
		}
		
		player.getInventory().addItem(new Item(itemOnScreen.getId(), itemOnScreenAmount));
		player.getDice().remove(new Item(betItem, itemOnScreenAmount), slot);
		refresh(player, opponent);
		player.setDiceStage(DiceStage.SEND_REQUEST_ACCEPT);
		opponent.setDiceStage(DiceStage.SEND_REQUEST_ACCEPT);
	}
	
	/**
	 * Moves each player to the second window of the betting interface.
	 */
	public void acceptStageOne(Player player) {
		Player opponent = World.getPlayer(player.getClickId());

		if (opponent.getClickId() != player.getIndex()) {
			return;
		}

		if (player == null || !player.isLoggedIn() || opponent == null || !opponent.isLoggedIn()) {
			return;
		}

		if (opponent.getInventory().getItemContainer().freeSlots() < player.getDice().size()) {
			player.getActionSender().sendMessage("Other player doesn't have enough inventory space for this bet.");
			return;
		}
		if (player.getInventory().getItemContainer().freeSlots() < opponent.getDice().size()) {
			player.getActionSender().sendMessage("You don't have enough inventory space for this bet.");
			return;
		}

		player.setDiceStage(DiceStage.ACCEPT);

		if (!opponent.getDiceStage().equals(DiceStage.ACCEPT)) {
			player.getActionSender().sendString("Waiting for other player...", 3431);
			opponent.getActionSender().sendString("Other player accepted.", 3431);
		} else {
			sendConfirmationWindow(player);
			sendConfirmationWindow(opponent);
		}
	}
	
	/**
	 * Moves each player to the dicing game.
	 */
	public void acceptStageTwo(Player player) {
		if (!player.getDiceStage().equals(DiceStage.SECOND_DICE_WINDOW)) {
			return;
		}

		Player opponent = World.getPlayer(player.getClickId());

		if (player == null || !player.isLoggedIn() || opponent == null || !opponent.isLoggedIn()) {
			return;
		}

		if (opponent.getClickId() != player.getIndex()) {
			return;
		}

		player.setDiceStage(DiceStage.ACCEPT);

		if (!opponent.getDiceStage().equals(DiceStage.ACCEPT)) {
			player.getActionSender().sendString("@cya@Waiting for other player...", 3535);
			opponent.getActionSender().sendString("@cya@Other player accepted.", 3535);
		} else {
			for (int i = 0; i < Inventory.SIZE; i++) {
				Item newItems = player.getDice().get(i);
				if (newItems == null) {
					continue;
				}
				playerBet.add(newItems);
			}
			for (int i = 0; i < Inventory.SIZE; i++) {
				Item newItems = opponent.getDice().get(i);
				if (newItems == null) {
					continue;
				}
				opponentBet.add(newItems);
			}

			player.setDiceStage(DiceStage.WAITING);
			opponent.setDiceStage(DiceStage.WAITING);

			player.getActionSender().sendMessage("You accept the bet.");
			opponent.getActionSender().sendMessage("You accept the bet.");

			player.getActionSender().removeInterfaces();
			opponent.getActionSender().removeInterfaces();
			play(player, opponent);
		}
	}
	
	/**
	 * The second window of the betting interface allowing players to double check the items they have bet.
	 */
	public void sendConfirmationWindow(Player player) {
		Player opponent = World.getPlayer(player.getClickId());
		StringBuilder dice = new StringBuilder();
		boolean empty = true;

		for (int i = 0; i < Inventory.SIZE; i++) {
			Item item = player.getDice().get(i);
			String prefix = "";
			Locale locale = Locale.US;
			String s = null;
			if (item != null) {
				empty = false;
				if (item.getCount() >= 1000 && item.getCount() < 10000000) {
					s = NumberFormat.getNumberInstance(locale).format(item.getCount());
					prefix = "@cya@" + NumberFormat.getNumberInstance(locale).format((item.getCount() / 1000)) + "K @whi@(" + s + ")";
				} else if (item.getCount() >= 10000000) {
					s = NumberFormat.getNumberInstance(locale).format(item.getCount());
					prefix = "@mon@" + (item.getCount() / 1000000) + "M (" + s + ")";
				} else {
					s = NumberFormat.getNumberInstance(locale).format(item.getCount());
					prefix = s;
				}
				dice.append(item.getDefinition().getName());
				if (item.getCount() > 1) {
					dice.append(" x ");
					dice.append(prefix);
				}
				dice.append("\\n");
			}
		}

		if (empty) {
			dice.append("Absolutely nothing!");
		}

		player.getActionSender().sendString(dice.toString(), 3557);
		dice = new StringBuilder();
		Locale locale = Locale.US;
		String s = null;
		empty = true;
		for (int i = 0; i < Inventory.SIZE; i++) {
			Item item = opponent.getDice().get(i);
			String prefix = "";
			if (item != null) {
				empty = false;
				if (item.getCount() >= 1000 && item.getCount() < 10000000) {
					s = NumberFormat.getNumberInstance(locale).format(item.getCount());
					prefix = "@cya@" + NumberFormat.getNumberInstance(locale).format((item.getCount() / 1000)) + "K @whi@(" + s + ")";
				} else if (item.getCount() >= 10000000) {
					s = NumberFormat.getNumberInstance(locale).format(item.getCount());
					prefix = "@mon@" + (item.getCount() / 1000000) + "M (" + s + ")";
				} else {
					s = NumberFormat.getNumberInstance(locale).format(item.getCount());
					prefix = s;
				}
				dice.append(item.getDefinition().getName());
				if (item.getCount() > 1) {
					dice.append(" x ");
					dice.append(prefix);
				}
				dice.append("\\n");
			}
		}

		if (empty) {
			dice.append("Absolutely nothing!");
		}

		player.getActionSender().sendString(dice.toString(), 3558);

		player.getActionSender().sendString("Betting with:\\n" + opponent.getUsername(), 2422);
		opponent.getActionSender().sendString("Betting with:\\n" + player.getUsername(), 2422);

		refresh(player, opponent);

		player.getActionSender().sendItfInvOverlay(3443, 3213);
		opponent.getActionSender().sendItfInvOverlay(3443, 3213);

		player.setDiceStage(DiceStage.SECOND_DICE_WINDOW);
		opponent.setDiceStage(DiceStage.SECOND_DICE_WINDOW);

		player.getActionSender().sendString("Are you sure you want to accept this bet?", 3535);
		opponent.getActionSender().sendString("Are you sure you want to accept this bet?", 3535);
	}
	
	/**
	 * In the event of a player disconnecting, refund any items that may have been lost otherwise.
	 */
	public void handleDisconnect(Player player) {
		Player opponent = World.getPlayer(player.getClickId());

		if (opponent.getClickId() != player.getIndex()) {
			return;
		}

		if (opponent.getDiceStage().equals(DiceStage.ACCEPT) || opponent.getDiceStage().equals(DiceStage.SEND_REQUEST_ACCEPT) || opponent.getDiceStage().equals(DiceStage.SECOND_DICE_WINDOW)) {
			opponent.getActionSender().sendMessage("Other player has declined the bet.");
			player.getActionSender().sendMessage("You decline the bet.");
		}

		player.getActionSender().removeInterfaces();
		opponent.getActionSender().removeInterfaces();

		player.setDiceStage(DiceStage.WAITING);
		opponent.setDiceStage(DiceStage.WAITING);

		giveBackItems(player);
		
		player.getInventory().refresh();
		opponent.getInventory().refresh();
		player.setClickId(-1);
	}
	
	/**
	 * The dicing game between the players.
	 */
	public void play(Player player, Player opponent) {
		String winner = "";
		
		/**
		 * Clear the dicing container for both players.
		 */
		player.getDice().clear();
		opponent.getDice().clear();
		
		/**
		 * Generate the rolls.
		 */
		playerRoll = getRoll();
		opponentRoll = getRoll();
		
		/**
		 * Configure the interface for each player.
		 */
		configureInterface(player, player.getUsername(), opponent.getUsername(), playerRoll, opponentRoll);
		configureInterface(opponent, opponent.getUsername(), player.getUsername(), opponentRoll, playerRoll);

		if (playerRoll > opponentRoll) {
			winner = player.getUsername();
			player.getActionSender().sendString("You win!", GAME_RESULT);
			opponent.getActionSender().sendString("You lose!", GAME_RESULT);
			
			/**
			 * Add the player's winnings to their inventory.
			 */
			for (Item winnings : opponentBet) {
				player.getInventory().addItem(winnings);
			}
			
			/**
			 * Give back the items they bet.
			 */
			for (Item item : playerBet) {
				player.getInventory().addItem(item);
			}
		} else if (opponentRoll > playerRoll) {
			winner = opponent.getUsername();
			player.getActionSender().sendString("You lose!", GAME_RESULT);
			opponent.getActionSender().sendString("You win!", GAME_RESULT);
			
			/**
			 * Add the opponent's winnings to their inventory.
			 */
			for (Item winnings : playerBet) {
				opponent.getInventory().addItem(winnings);
			}
			
			/**
			 * Give back the items they bet.
			 */
			for (Item item : opponentBet) {
				opponent.getInventory().addItem(item);
			}
		} else {
			winner = "Tie";
			player.getActionSender().sendString("It's a tie!", GAME_RESULT);
			opponent.getActionSender().sendString("It's a tie!", GAME_RESULT);
			
			/**
			 * Refund the items for both players.
			 */
			for (Item pBet : playerBet) {
				player.getInventory().addItem(pBet);
			}
			for (Item oBet : opponentBet) {
				opponent.getInventory().addItem(oBet);
			}
		}
		
		/**
		 * Log the bets.
		 */
		recordBet(player, opponent, playerBet, opponentBet, winner);
		
		/**
		 * Clear the bet lists.
		 */
		playerBet.clear();
		opponentBet.clear();
	}
	
	/**
	 * Sends the bet to the DiceLog class.
	 * @param player The player who initiated the game.
	 * @param opponent The opponent.
	 * @param playerBet The bet of the player in string form.
	 * @param opponentBet The bet of the opponent in string form.
	 * @param winner The winner of the game.
	 */
	private void recordBet(Player player, Player opponent, ArrayList<Item> playerBet, ArrayList<Item> opponentBet, String winner) {
		String pBet = "";
		String oBet = "";
				
		/**
		 * Get all the items and convert them to a string format.
		 */
		for (int i = 0; i < playerBet.size(); i++) {
			if (i != playerBet.size() - 1) {
				pBet = pBet + playerBet.get(i).getName() + ", " + playerBet.get(i).getCount() + ", ";
			} else {
				pBet = pBet + playerBet.get(i).getName() + ", " + playerBet.get(i).getCount();
			}
		}
		for (int i = 0; i < opponentBet.size(); i++) {
			if (i != playerBet.size() - 1) {
				oBet = oBet + opponentBet.get(i).getName() + ", " + playerBet.get(i).getCount() + ", ";
			} else {
				oBet = oBet + opponentBet.get(i).getName() + ", " + playerBet.get(i).getCount();
			}
		}
		
		/**
		 * Set the bet to nothing if it's empty.
		 */
		if (playerBet.size() == 0) {
			pBet = "Nothing";
		}
		if (opponentBet.size() == 0) {
			oBet = "Nothing";
		}
	
		
		DiceLog.recordBet(player.getUsername(), opponent.getUsername(), pBet, oBet, winner);
	}
}
