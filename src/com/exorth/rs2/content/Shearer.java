package com.exorth.rs2.content;

import com.exorth.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry
 * 
 */
public interface Shearer<T> {
	/**
	 * Shears an actor to a naked state.
	 * 
	 * @param player The player shearing the NPC
	 * @param actor The unknown, sheered.
	 */
	public void shear(Player player, T actor);
}