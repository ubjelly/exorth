package com.exorth.rs2.util.clip;

import java.util.HashMap;

import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.region.Region;

/**
 * 
 * @author Killamess
 * 
 */
public class TileControl {

	private static TileControl singleton = null;

	public static TileControl getSingleton() {
		if (singleton == null) {
			singleton = new TileControl();
		}
		return singleton;
	}

	public static Tile generate(int x, int y, int z) {
		return new Tile(x, y, z);
	}

	private HashMap<Entity, Position[]> occupiedLocations = new HashMap<Entity, Position[]>();

	public Position[] getOccupiedLocations(Entity mob) {
		return this.occupiedLocations.get(mob);
	}

	public boolean locationOccupied(Position[] locations, Entity mob) {
		if ((locations == null) || (mob == null)) {
			return true;
		}

		Position[] npcLocations = null;
		for (Region r : World.getInstance().getRegionManager().getSurroundingRegions(mob.getPosition())) {
			for (Entity entity : r.getMobs()) {
				if (entity.isNpc()) {
					Npc npc = (Npc) entity;
					if ((mob.isNpc()) && ((npc == null) || (npc == mob))) {
						continue;
					}
					npcLocations = getOccupiedLocations(npc);
					if (npcLocations != null) {
						for (Position loc : locations) {
							for (Position loc2 : npcLocations) {
								if (loc.equals(loc2)) {
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	}

	public static Position[] getHoveringTiles(Entity entity) {
		return getHoveringTiles(entity, entity.getPosition());
	}

	public void setOccupiedLocation(Entity mob, Position[] locations) {
		if ((mob == null) || (locations == null)) {
			return;
		}

		this.occupiedLocations.remove(mob);
		this.occupiedLocations.put(mob, locations);
	}

	public static Position[] getHoveringTiles(Entity entity, Position location) {
		if (entity == null) {
			return null;
		}

		int buf = 1;
		int offset = 0;

		if (entity.isNpc()) {
			buf = ((Npc) entity).getDefinition().getSize();
		}

		Position[] locations = new Position[buf * buf];
		if (locations.length == 1) {
			locations[offset] = location;
		} else {
			for (int x = 0; x < buf; x++) {
				for (int y = 0; y < buf; y++) {
					locations[(offset++)] = new Position(location.getX() + x, location.getY() + y, location.getZ());
				}
			}
		}
		return locations;
	}

	public static int calculateDistance(Position pointA, Position pointB) {
		int offsetX = Math.abs(pointA.getX() - pointB.getX());
		int offsetY = Math.abs(pointA.getY() - pointB.getY());
		return offsetX > offsetY ? offsetX : offsetY;
	}

	public static Tile[] getTiles(Entity entity) {
		int size = 1, tileCount = 0;

		if (entity.isNpc()) {
			Npc npc = (Npc) entity;
			size = npc.getDefinition().getSize();
		}

		Tile[] tiles = new Tile[size * size];

		if (tiles.length == 1) {
			tiles[0] = generate(entity.getPosition().getX(), entity.getPosition().getY(), entity.getPosition().getZ());
		} else {
			for (int x = 0; x < size; x++) {
				for (int y = 0; y < size; y++) {
					tiles[tileCount++] = generate(entity.getPosition().getX() + x, entity.getPosition().getY() + y, entity.getPosition().getZ());
				}
			}
		}
		return tiles;
	}

	public static Tile[] getTiles(Entity entity, int[] location) {
		int size = 1, tileCount = 0;

		if (entity.isNpc()) {
			Npc npc = (Npc) entity;
			size = npc.getDefinition().getSize();
		}

		Tile[] tiles = new Tile[size * size];

		if (tiles.length == 1) {
			tiles[0] = generate(location[0], location[1], location[2]);
		} else {
			for (int x = 0; x < size; x++) {
				for (int y = 0; y < size; y++) {
					tiles[tileCount++] = generate(location[0] + x, location[1] + y, location[2]);
				}
			}
		}
		return tiles;
	}

	public static int calculateDistance(Entity entity, Entity following) {
		Tile[] tiles = getTiles(entity);

		int[] location = new int[] { entity.getPosition().getX(), entity.getPosition().getY(), entity.getPosition().getZ() };
		int[] pointer = new int[tiles.length];

		int lowestCount = 20, count = 0;

		for (Tile newTiles : tiles) {
			if (newTiles.getTile() == location) {
				pointer[count++] = 0;
			} else {
				pointer[count++] = calculateDistance(newTiles, following);
			}
		}
		for (int element : pointer) {
			if (element < lowestCount) {
				lowestCount = element;
			}
		}
		return lowestCount;
	}

	public static int calculateDistance(Tile location, Entity other) {
		int X = Math.abs(location.getTile()[0] - other.getPosition().getX());
		int Y = Math.abs(location.getTile()[1] - other.getPosition().getY());
		return X > Y ? X : Y;
	}

	public static int calculateDistance(int[] location, Entity other) {
		int X = Math.abs(location[0] - other.getPosition().getX());
		int Y = Math.abs(location[1] - other.getPosition().getY());
		return X > Y ? X : Y;
	}

	public static int calculateDistance(int[] location, int[] other) {
		int X = Math.abs(location[0] - other[0]);
		int Y = Math.abs(location[1] - other[1]);
		return X > Y ? X : Y;
	}

	public static int[] currentLocation(Entity entity) {
		int[] currentLocation = new int[3];
		if (entity != null) {
			currentLocation[0] = entity.getPosition().getX();
			currentLocation[1] = entity.getPosition().getY();
			currentLocation[2] = entity.getPosition().getZ();
		}
		return currentLocation;
	}

	public static int[] currentLocation(Tile tileLocation) {
		int[] currentLocation = new int[3];

		if (tileLocation != null) {
			currentLocation[0] = tileLocation.getTile()[0];
			currentLocation[1] = tileLocation.getTile()[1];
			currentLocation[2] = tileLocation.getTile()[2];
		}
		return currentLocation;
	}
}
