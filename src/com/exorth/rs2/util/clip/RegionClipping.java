package com.exorth.rs2.util.clip;

import java.io.*;
import java.util.logging.Logger;
import java.util.zip.*;

import com.exorth.rs2.Constants;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.Player;

/**
 * 
 * @author Joshua Barry
 * @author AkZu
 * 
 */
public class RegionClipping {

	/**
	 * Add clipping. WARNING: When we're in 2nd map region it will use per
	 * player instance data.
	 * 
	 * @param player give null if world-wide clipping
	 * @param x
	 * @param y
	 * @param height
	 * @param shift
	 */
	public static void addClipping(Entity e, int x, int y, int height, int shift, boolean floor_maps) {
		int[][][] clips = null;

		if (e != null && e.getAttribute("2nd_map_region_clip") != null) {
			clips = (int[][][]) e.getAttribute("2nd_map_region_clip");
		}

		int regionX = x >> 3;
		int regionY = y >> 3;
		int regionId = ((regionX / 8) << 8) + (regionY / 8);

		int regionAbsX = (regionId >> 8) * 64;
		int regionAbsY = (regionId & 0xff) * 64;

		if (clips != null) {
			clips[height][x - regionAbsX][y - regionAbsY] |= shift;
			e.setAttribute("2nd_map_region_clip", clips);
		} else {
			// Global world clipping
			if (regionId < 0 || regionId > 16000 || regions[regionId] == null) {
				return;
			}

			if (floor_maps && regions[regionId].clips_floormaps[height] == null) {
				regions[regionId].clips_floormaps[height] = new int[64][64];
			}

			if (regions[regionId].clips[height] == null) {
				regions[regionId].clips[height] = new int[64][64];
			}

			if (floor_maps) {
				regions[regionId].clips_floormaps[height][x - regionAbsX][y - regionAbsY] |= shift;
			}

			regions[regionId].clips[height][x - regionAbsX][y - regionAbsY] |= shift;
		}
	}

	public static void addClipping(Entity e, int x, int y, int height, int shift) {
		addClipping(e, x, y, height, shift, false);
	}

	/**
	 * Remove clipping. WARNING: When we're in 2nd map region it will use per
	 * player instance data.
	 * 
	 * @param player give null if world-wide clipping
	 * @param x
	 * @param y
	 * @param height
	 * @param shift
	 */
	public static void removeClipping(Entity e, int x, int y, int height, int shift) {
		int[][][] clips = null;

		if (e != null && e.getAttribute("2nd_map_region_clip") != null) {
			clips = (int[][][]) e.getAttribute("2nd_map_region_clip");
		}

		int regionX = x >> 3;
		int regionY = y >> 3;
		int regionId = ((regionX / 8) << 8) + (regionY / 8);

		int regionAbsX = (regionId >> 8) * 64;
		int regionAbsY = (regionId & 0xff) * 64;

		if (clips != null) {
			if (clips[height] == null) {
				return;
			}
			clips[height][x - regionAbsX][y - regionAbsY] &= 16777215 - shift;
			e.setAttribute("2nd_map_region_clip", clips);
		} else {
			// Global world clipping
			if (regionId < 0 || regionId > 16000 || regions[regionId] == null || regions[regionId].clips[height] == null) {
				return;
			}
			regions[regionId].clips[height][x - regionAbsX][y - regionAbsY] &= 16777215 - shift;
		}
	}

	/**
	 * Get clipping. WARNING: When we're in 2nd map region it will use per
	 * player instance data.
	 * 
	 * @param player give null if world-wide clipping
	 * @param x
	 * @param y
	 * @param height
	 * @param shift
	 */
	public static int getClipping(Entity entity, int x, int y, int height, boolean floor_maps) {
		int[][][] clips = null;

		if (entity != null && entity.getAttribute("2nd_map_region_clip") != null) {
			clips = (int[][][]) entity.getAttribute("2nd_map_region_clip");
		}

		int regionX = x >> 3;
		int regionY = y >> 3;
		int regionId = ((regionX / 8) << 8) + (regionY / 8);

		int regionAbsX = (regionId >> 8) * 64;
		int regionAbsY = (regionId & 0xff) * 64;

		if (clips != null) {
			if (clips[height] == null) {
				return 0;
			}
			return clips[height][x - regionAbsX][y - regionAbsY];
		} else {
			// Global world clipping
			if (regionId < 0 || regionId > 16000 || regions[regionId] == null) {
				return 0;
			}

			if (regions[regionId].clips[height] == null) {
				return 0;
			}

			if (floor_maps && regions[regionId].clips_floormaps[height] == null) {
				return 0;
			}

			if (floor_maps) {
				return regions[regionId].clips_floormaps[height][x - regionAbsX][y - regionAbsY];
			}

			return regions[regionId].clips[height][x - regionAbsX][y - regionAbsY];
		}
	}

	public static int getClipping(Entity entity, int x, int y, int height) {
		return getClipping(entity, x, y, height, false);
	}

	// All our regions
	public static RegionClipping[] regions;
	// Clipping flags for this region
	private int[][][] clips = new int[4][][];
	// Clipping flags (floor_maps) for this region
	private int[][][] clips_floormaps = new int[4][][];
	// Registered objects for this region
	private RSObject[][][] rsObjects;

	public static void addClippingForVariableObject(Player player, int x, int y, int height, int type, int direction, boolean flag) {
		if (type == 0) {
			// Normal walls
			if (direction == 0) {
				// Wall tile-west?
				addClipping(player, x, y, height, 128);
				addClipping(player, x - 1, y, height, 8);
			} else if (direction == 1) {
				// Wall tile-north?
				addClipping(player, x, y, height, 2);
				addClipping(player, x, y + 1, height, 32);
			} else if (direction == 2) {
				// Wall tile-east?
				addClipping(player, x, y, height, 8);
				addClipping(player, x + 1, y, height, 128);
			} else if (direction == 3) {
				// Wall tile-south?
				addClipping(player, x, y, height, 32);
				addClipping(player, x, y - 1, height, 2);
			}
			// Diagonal walls
		} else if (type == 1 || type == 3) {
			if (direction == 0) {
				// Wall north-west
				addClipping(player, x, y, height, 1);
				addClipping(player, x - 1, y, height, 16);
			} else if (direction == 1) {
				// Wall north-east
				addClipping(player, x, y, height, 4);
				addClipping(player, x + 1, y + 1, height, 64);
			} else if (direction == 2) {
				// Wall south-east
				addClipping(player, x, y, height, 16);
				addClipping(player, x + 1, y - 1, height, 1);
			} else if (direction == 3) {
				// Wall south-west
				addClipping(player, x, y, height, 64);
				addClipping(player, x - 1, y - 1, height, 4);
			}
		} else if (type == 2) {
			if (direction == 0) {
				// Blocked north and west
				addClipping(player, x, y, height, 130);
				addClipping(player, x - 1, y, height, 8);
				addClipping(player, x, y + 1, height, 32);
			} else if (direction == 1) {
				// Blocked north and east
				addClipping(player, x, y, height, 10);
				addClipping(player, x, y + 1, height, 32);
				addClipping(player, x + 1, y, height, 128);
			} else if (direction == 2) {
				// Blocked south and east
				addClipping(player, x, y, height, 40);
				addClipping(player, x + 1, y, height, 128);
				addClipping(player, x, y - 1, height, 2);
			} else if (direction == 3) {
				// Blocked south and west
				addClipping(player, x, y, height, 160);
				addClipping(player, x, y - 1, height, 2);
				addClipping(player, x - 1, y, height, 8);
			}
		}
		if (flag) {
			if (type == 0) {
				if (direction == 0) {
					addClipping(player, x, y, height, 0x10000);
					addClipping(player, x - 1, y, height, 4096);
				}
				if (direction == 1) {
					addClipping(player, x, y, height, 1024);
					addClipping(player, x, y + 1, height, 16384);
				}
				if (direction == 2) {
					addClipping(player, x, y, height, 4096);
					addClipping(player, x + 1, y, height, 0x10000);
				}
				if (direction == 3) {
					addClipping(player, x, y, height, 16384);
					addClipping(player, x, y - 1, height, 1024);
				}
			}
			if (type == 1 || type == 3) {
				if (direction == 0) {
					addClipping(player, x, y, height, 512);
					addClipping(player, x - 1, y + 1, height, 8192);
				}
				if (direction == 1) {
					addClipping(player, x, y, height, 2048);
					addClipping(player, x + 1, y + 1, height, 32768);
				}
				if (direction == 2) {
					addClipping(player, x, y, height, 8192);
					addClipping(player, x + 1, y - 1, height, 512);
				}
				if (direction == 3) {
					addClipping(player, x, y, height, 32768);
					addClipping(player, x - 1, y - 1, height, 2048);
				}
			}
			if (type == 2) {
				if (direction == 0) {
					addClipping(player, x, y, height, 0x10400);
					addClipping(player, x - 1, y, height, 4096);
					addClipping(player, x, y + 1, height, 16384);
				}
				if (direction == 1) {
					addClipping(player, x, y, height, 5120);
					addClipping(player, x, y + 1, height, 16384);
					addClipping(player, x + 1, y + 1, height, 0x10000);
				}
				if (direction == 2) {
					addClipping(player, x, y, height, 20480);
					addClipping(player, x + 1, y, height, 0x10000);
					addClipping(player, x, y - 1, height, 1024);
				}
				if (direction == 3) {
					addClipping(player, x, y, height, 0x14000);
					addClipping(player, x, y - 1, height, 1024);
					addClipping(player, x - 1, y, height, 4096);
				}
			}
		}
	}

	private static void removeClippingForVariableObject(Player player, int x, int y, int height, int type, int direction, boolean flag) {
		if (type == 0) {
			if (direction == 0) {
				removeClipping(player, x, y, height, 128);
				removeClipping(player, x - 1, y, height, 8);
			} else if (direction == 1) {
				removeClipping(player, x, y, height, 2);
				removeClipping(player, x, y + 1, height, 32);
			} else if (direction == 2) {
				removeClipping(player, x, y, height, 8);
				removeClipping(player, x + 1, y, height, 128);
			} else if (direction == 3) {
				removeClipping(player, x, y, height, 32);
				removeClipping(player, x, y - 1, height, 2);
			}
		} else if (type == 1 || type == 3) {
			if (direction == 0) {
				removeClipping(player, x, y, height, 1);
				removeClipping(player, x - 1, y, height, 16);
			} else if (direction == 1) {
				removeClipping(player, x, y, height, 4);
				removeClipping(player, x + 1, y + 1, height, 64);
			} else if (direction == 2) {
				removeClipping(player, x, y, height, 16);
				removeClipping(player, x + 1, y - 1, height, 1);
			} else if (direction == 3) {
				removeClipping(player, x, y, height, 64);
				removeClipping(player, x - 1, y - 1, height, 4);
			}
		} else if (type == 2) {
			if (direction == 0) {
				removeClipping(player, x, y, height, 130);
				removeClipping(player, x - 1, y, height, 8);
				removeClipping(player, x, y + 1, height, 32);
			} else if (direction == 1) {
				removeClipping(player, x, y, height, 10);
				removeClipping(player, x, y + 1, height, 32);
				removeClipping(player, x + 1, y, height, 128);
			} else if (direction == 2) {
				removeClipping(player, x, y, height, 40);
				removeClipping(player, x + 1, y, height, 128);
				removeClipping(player, x, y - 1, height, 2);
			} else if (direction == 3) {
				removeClipping(player, x, y, height, 160);
				removeClipping(player, x, y - 1, height, 2);
				removeClipping(player, x - 1, y, height, 8);
			}
		}
		if (flag) {
			if (type == 0) {
				if (direction == 0) {
					removeClipping(player, x, y, height, 65536);
					removeClipping(player, x - 1, y, height, 4096);
				} else if (direction == 1) {
					removeClipping(player, x, y, height, 1024);
					removeClipping(player, x, y + 1, height, 16384);
				} else if (direction == 2) {
					removeClipping(player, x, y, height, 4096);
					removeClipping(player, x + 1, y, height, 65536);
				} else if (direction == 3) {
					removeClipping(player, x, y, height, 16384);
					removeClipping(player, x, y - 1, height, 1024);
				}
			}
			if (type == 1 || type == 3) {
				if (direction == 0) {
					removeClipping(player, x, y, height, 512);
					removeClipping(player, x - 1, y + 1, height, 8192);
				} else if (direction == 1) {
					removeClipping(player, x, y, height, 2048);
					removeClipping(player, x + 1, y + 1, height, 32768);
				} else if (direction == 2) {
					removeClipping(player, x, y, height, 8192);
					removeClipping(player, x + 1, y + 1, height, 512);
				} else if (direction == 3) {
					removeClipping(player, x, y, height, 32768);
					removeClipping(player, x - 1, y - 1, height, 2048);
				}
			} else if (type == 2) {
				if (direction == 0) {
					removeClipping(player, x, y, height, 66560);
					removeClipping(player, x - 1, y, height, 4096);
					removeClipping(player, x, y + 1, height, 16384);
				} else if (direction == 1) {
					removeClipping(player, x, y, height, 5120);
					removeClipping(player, x, y + 1, height, 16384);
					removeClipping(player, x + 1, y, height, 65536);
				} else if (direction == 2) {
					removeClipping(player, x, y, height, 20480);
					removeClipping(player, x + 1, y, height, 65536);
					removeClipping(player, x, y - 1, height, 1024);
				} else if (direction == 3) {
					removeClipping(player, x, y, height, 81920);
					removeClipping(player, x, y - 1, height, 1024);
					removeClipping(player, x - 1, y, height, 4096);
				}
			}
		}
	}

	/**
	 * Gets RSObject. WARNING: When we're in 2nd map region it will use per
	 * player instance data.
	 */
	public static RSObject getRSObject(Player player, Position position) {
		int x = position.getX();
		int y = position.getY();
		int height = position.getZ();

		int regionX = x >> 3;
		int regionY = y >> 3;
		int regionId = (regionX / 8 << 8) + regionY / 8;

		int regionAbsX = (regionId >> 8) * 64;
		int regionAbsY = (regionId & 255) * 64;

		RSObject[][][] rsObjects = null;

		if (player != null && player.getAttribute("2nd_map_region_objects") != null) {
			rsObjects = (RSObject[][][]) player.getAttribute("2nd_map_region_objects");
		}

		if (rsObjects != null) {
			if (rsObjects[height] == null) {
				return null;
			}
			return rsObjects[height][x - regionAbsX][y - regionAbsY];
		} else {
			// Global world region objects
			if (regionId < 0 || regionId > 16000 || regions[regionId].rsObjects == null) {
				return null;
			}

			if (regions[regionId].rsObjects[height] == null) {
				return null;
			}

			return regions[regionId].rsObjects[height][x - regionAbsX][y - regionAbsY];
		}
	}

	/**
	 * Gets a RSObject.
	 * <p>
	 * WARNING: When we're in 2nd map region it will use per player instance data.
	 * <p>
	 * TODO: Add another check for clippable objects to prevent returning <code>null</code>.
	 */
	public static RSObject getRSObject(Player player, int x, int y, int height) {
		int regionX = x >> 3;
		int regionY = y >> 3;
		int regionId = (regionX / 8 << 8) + regionY / 8;

		int regionAbsX = (regionId >> 8) * 64;
		int regionAbsY = (regionId & 255) * 64;

		RSObject[][][] rsObjects = null;

		if (player != null && player.getAttribute("2nd_map_region_objects") != null) {
			rsObjects = (RSObject[][][]) player.getAttribute("2nd_map_region_objects");
		}

		if (rsObjects != null) {
			if (rsObjects[height] == null) {
				return null;
			}
			return rsObjects[height][x - regionAbsX][y - regionAbsY];
		} else {
			// Global world region objects
			if (regionId < 0 || regionId > 16000 || regions[regionId].rsObjects == null) {
				return null;
			}

			if (regions[regionId].rsObjects[height] == null) {
				return null;
			}

			return regions[regionId].rsObjects[height][x - regionAbsX][y - regionAbsY];
		}
	}

	/**
	 * Adds RSObject. WARNING: When we're in 2nd map region it will use per
	 * player instance data.
	 */
	public static void addRSObject(Player player, int x, int y, int height, RSObject object) {
		int regionX = x >> 3;
		int regionY = y >> 3;
		int regionId = (regionX / 8 << 8) + regionY / 8;

		int regionAbsX = (regionId >> 8) * 64;
		int regionAbsY = (regionId & 255) * 64;

		RSObject[][][] rsObjects = null;

		int xLength;
		int yLength;

		if (player != null && player.getAttribute("2nd_map_region_objects") != null) {
			rsObjects = (RSObject[][][]) player.getAttribute("2nd_map_region_objects");
		}

		if (rsObjects != null) {
			if (rsObjects[height] == null) {
				rsObjects[height] = new RSObject[64][64];
			}

			RSObject oldObject = null;

			/*
			 * if (object != null) { ObjectDefinition r =
			 * ObjectDefinition.forId(object.getId()); if (object.getFace() != 1
			 * && object.getFace() != 3) { xLength = r.xLength(); yLength =
			 * r.yLength(); } else { xLength = r.yLength(); yLength =
			 * r.xLength(); } if (xLength > 1 || yLength > 1) { for (int i = 0;
			 * i < xLength; i++) { for (int i2 = 0; i2 < yLength; i2++) { int
			 * regionX2 = x + i >> 3; int regionY2 = y + i2 >> 3; int regionId2
			 * = (regionX2 / 8 << 8) + regionY2 / 8; int regionAbsX2 =
			 * (regionId2 >> 8) * 64; int regionAbsY2 = (regionId2 & 255) * 64;
			 * oldObject = rsObjects[height][x + i - regionAbsX2][y + i2 -
			 * regionAbsY2] = object; if (oldObject == null || object == null ||
			 * ObjectDefinition.forId(object.getId()).hasActions())
			 * rsObjects[height][x + i - regionAbsX2][y + i2 - regionAbsY2] =
			 * object; } } } }
			 */

			oldObject = rsObjects[height][x - regionAbsX][y - regionAbsY];

			if (oldObject == null || object == null || ObjectDefinition.forId(object.getId()).hasActions()) {
				rsObjects[height][x - regionAbsX][y - regionAbsY] = object;
			}
		} else {
			// Global world region objects
			if (regions[regionId].rsObjects == null) {
				regions[regionId].rsObjects = new RSObject[4][][];
			}

			if (regions[regionId].rsObjects[height] == null) {
				regions[regionId].rsObjects[height] = new RSObject[64][64];
			}

			RSObject oldObject = null;

			if (object != null) {
				ObjectDefinition r = ObjectDefinition.forId(object.getId());

				if (object.getFace() != 1 && object.getFace() != 3) {
					xLength = r.xLength();
					yLength = r.yLength();
				} else {
					xLength = r.yLength();
					yLength = r.xLength();
				}

				if (xLength > 1 || yLength > 1) {
					for (int i = 0; i < xLength; i++) {
						for (int i2 = 0; i2 < yLength; i2++) {
							int regionX2 = x + i >> 3;
							int regionY2 = y + i2 >> 3;
							int regionId2 = (regionX2 / 8 << 8) + regionY2 / 8;

							int regionAbsX2 = (regionId2 >> 8) * 64;
							int regionAbsY2 = (regionId2 & 255) * 64;

							if (regions[regionId2] != null) {
								if (regions[regionId2].rsObjects == null) {
									regions[regionId2].rsObjects = new RSObject[4][][];
								}

								if (regions[regionId2].rsObjects[height] == null) {
									regions[regionId2].rsObjects[height] = new RSObject[64][64];
								}

								oldObject = regions[regionId2].rsObjects[height][x + i - regionAbsX2][y + i2 - regionAbsY2];

								if (oldObject == null || object == null || ObjectDefinition.forId(object.getId()).hasActions()) {
									regions[regionId2].rsObjects[height][x + i - regionAbsX2][y + i2 - regionAbsY2] = object;
								}
							}
						}
					}
				}
			}
			oldObject = regions[regionId].rsObjects[height][x - regionAbsX][y - regionAbsY];

			if (oldObject == null || object == null || ObjectDefinition.forId(object.getId()).hasActions()) {
				regions[regionId].rsObjects[height][x - regionAbsX][y - regionAbsY] = object;
			}
		}
	}

	/**
	 * 
	 * @param x
	 * @param y
	 * @param height
	 * @param xLength
	 * @param yLength
	 * @param flag
	 */
	public static void addClippingForSolidObject(Player player, int x, int y, int height, int xLength, int yLength, boolean flag) {
		int clipping = 256;
		if (flag) {
			clipping += 0x20000;
		}
		for (int i = x; i < x + xLength; i++) {
			for (int i2 = y; i2 < y + yLength; i2++) {
				addClipping(player, i, i2, height, clipping);
			}
		}
	}

	private static void removeClippingForSolidObject(Player player, int x, int y, int height, int xLength, int yLength, boolean flag) {
		int clipping = 256;
		if (flag) {
			clipping += 0x20000;
		}
		for (int i = x; i < x + xLength; i++) {
			for (int i2 = y; i2 < y + yLength; i2++) {
				removeClipping(player, i, i2, height, clipping);
			}
		}
	}

	public static void addObject(Player player, int objectId, int x, int y, int height, int type, int direction) {
		ObjectDefinition def = ObjectDefinition.forId(objectId);

		if (def == null) {
			return;
		}

		int xLength;
		int yLength;

		if (direction != 1 && direction != 3) {
			xLength = def.xLength();
			yLength = def.yLength();
		} else {
			xLength = def.yLength();
			yLength = def.xLength();
		}

		if (type == 22) {
			if (def.hasActions() && def.isSolid()) {
				addClipping(player, x, y, height, 0x200000);
				addRSObject(player, x, y, height, new RSObject(objectId, x, y, height, type, direction));
			}
		} else if (type >= 9) {
			if (def.isSolid()) {
				addClippingForSolidObject(player, x, y, height, xLength, yLength, def.isShootable());
				addRSObject(player, x, y, height, new RSObject(objectId, x, y, height, type, direction));
			}
		} else if (type >= 0 && type <= 3) {
			if (def.isSolid()) {
				addClippingForVariableObject(player, x, y, height, type, direction, def.isShootable());
				addRSObject(player, x, y, height, new RSObject(objectId, x, y, height, type, direction));
			}
		}
	}

	public static void removeObject(Player player, int x, int y, int height) {
		RSObject oldObj = getRSObject(player, x, y, height);

		addRSObject(player, x, y, height, null);

		if (oldObj != null) {
			ObjectDefinition def = ObjectDefinition.forId(oldObj.getId());
			int xLength;
			int yLength;
			if (oldObj.getFace() != 1 && oldObj.getFace() != 3) {
				xLength = def.xLength();
				yLength = def.yLength();
			} else {
				xLength = def.yLength();
				yLength = def.xLength();
			}
			if (oldObj.getType() == 22) {
				if (def.hasActions() && def.isSolid()) {
					removeClipping(player, x, y, height, 0x200000);
				}
			} else if (oldObj.getType() >= 9) {
				if (def.isSolid()) {
					removeClippingForSolidObject(player, x, y, height, xLength, yLength, def.isSolid());
				}
			} else if (oldObj.getType() >= 0 && oldObj.getType() <= 3) {
				if (def.isSolid()) {
					removeClippingForVariableObject(player, x, y, height, oldObj.getType(), oldObj.getFace(), def.isSolid());
				}
			}
		}
	}

	/**
	 * 
	 * @param attacker
	 * @param victim
	 * @param xLength
	 * @param yLength
	 * @return
	 */
	public static boolean canMove(Entity attacker, int startX, int startY, int endX, int endY, int height, int xLength, int yLength) {
		int diffX = endX - startX;
		int diffY = endY - startY;
		int max = Math.max(Math.abs(diffX), Math.abs(diffY));
		for (int ii = 0; ii < max; ii++) {
			int currentX = endX - diffX;
			int currentY = endY - diffY;
			for (int i = 0; i < xLength; i++) {
				for (int i2 = 0; i2 < yLength; i2++) {
					if (diffX < 0 && diffY < 0) {
						if ((getClipping(attacker, currentX + i - 1, currentY + i2 - 1, height) & 0x128010e) != 0 || (getClipping(attacker, currentX + i - 1, currentY + i2, height) & 0x1280108) != 0 || (getClipping(attacker, currentX + i, currentY + i2 - 1, height) & 0x1280102) != 0) {
							return false;
						}
					} else if (diffX > 0 && diffY > 0) {
						if ((getClipping(attacker, currentX + i + 1, currentY + i2 + 1, height) & 0x12801e0) != 0 || (getClipping(attacker, currentX + i + 1, currentY + i2, height) & 0x1280180) != 0 || (getClipping(attacker, currentX + i, currentY + i2 + 1, height) & 0x1280120) != 0) {
							return false;
						}
					} else if (diffX < 0 && diffY > 0) {
						if ((getClipping(attacker, currentX + i - 1, currentY + i2 + 1, height) & 0x1280138) != 0 || (getClipping(attacker, currentX + i - 1, currentY + i2, height) & 0x1280108) != 0 || (getClipping(attacker, currentX + i, currentY + i2 + 1, height) & 0x1280120) != 0) {
							return false;
						}
					} else if (diffX > 0 && diffY < 0) {
						if ((getClipping(attacker, currentX + i + 1, currentY + i2 - 1, height) & 0x1280183) != 0 || (getClipping(attacker, currentX + i + 1, currentY + i2, height) & 0x1280180) != 0 || (getClipping(attacker, currentX + i, currentY + i2 - 1, height) & 0x1280102) != 0) {
							return false;
						}
					} else if (diffX > 0 && diffY == 0) {
						if ((getClipping(attacker, currentX + i + 1, currentY + i2, height) & 0x1280180) != 0) {
							return false;
						}
					} else if (diffX < 0 && diffY == 0) {
						if ((getClipping(attacker, currentX + i - 1, currentY + i2, height) & 0x1280108) != 0) {
							return false;
						}
					} else if (diffX == 0 && diffY > 0) {
						if ((getClipping(attacker, currentX + i, currentY + i2 + 1, height) & 0x1280120) != 0) {
							return false;
						}
					} else if (diffX == 0 && diffY < 0) {
						if ((getClipping(attacker, currentX + i, currentY + i2 - 1, height) & 0x1280102) != 0) {
							return false;
						}
					}
				}
			}
			if (diffX < 0) {
				diffX++;
			} else if (diffX > 0) {
				diffX--;
			}
			if (diffY < 0) {
				diffY++;
			} else if (diffY > 0) {
				diffY--;
			}
		}
		return true;
	}

	public static boolean blockedNorth(Entity e, Position pos) {
		return (getClipping(e, pos.getX(), pos.getY() + 1, pos.getZ()) & 0x1280120) != 0;
	}

	public static boolean blockedEast(Entity e, Position pos) {
		return (getClipping(e, pos.getX() + 1, pos.getY(), pos.getZ()) & 0x1280180) != 0;
	}

	public static boolean blockedSouth(Entity e, Position pos) {
		return (getClipping(e, pos.getX(), pos.getY() - 1, pos.getZ()) & 0x1280102) != 0;
	}

	public static boolean blockedWest(Entity e, Position pos) {
		return (getClipping(e, pos.getX() - 1, pos.getY(), pos.getZ()) & 0x1280108) != 0;
	}

	public static boolean blockedNorthEast(Entity e, Position pos) {
		return (getClipping(e, pos.getX() + 1, pos.getY() + 1, pos.getZ()) & 0x12801e0) != 0;
	}

	public static boolean blockedNorthWest(Entity e, Position pos) {
		return (getClipping(e, pos.getX() - 1, pos.getY() + 1, pos.getZ()) & 0x1280138) != 0;
	}

	public static boolean blockedSouthEast(Entity e, Position pos) {
		return (getClipping(e, pos.getX() + 1, pos.getY() - 1, pos.getZ()) & 0x1280183) != 0;
	}

	public static boolean blockedSouthWest(Entity e, Position pos) {
		return (getClipping(e, pos.getX() - 1, pos.getY() - 1, pos.getZ()) & 0x128010e) != 0;
	}

	public static void load() {
		try {
			File f = new File(Constants.CONFIG_DIRECTORY.concat("world/map_index.dat"));
			byte[] buffer = new byte[(int) f.length()];

			DataInputStream dis = new DataInputStream(new FileInputStream(f));
			dis.readFully(buffer);
			dis.close();

			ByteStream in = new ByteStream(buffer);

			int size = in.length() / 7;

			regions = new RegionClipping[16000]; // Total region size 15160

			short[] regionIds = new short[size];
			short[] mapGroundFileIds = new short[size];
			short[] mapObjectsFileIds = new short[size];
			// byte[] isMembers = new byte[size];

			for (int i = 0; i < size; i++) {
				regionIds[i] = (short) in.getUShort();
				mapGroundFileIds[i] = (short) in.getUShort();
				mapObjectsFileIds[i] = (short) in.getUShort();
				/* isMembers[i] = (byte) */
				in.getUByte();
			}

			for (int i = 0; i < size; i++) {
				regions[regionIds[i]] = new RegionClipping();
			}

			// Our dynamic map regions will spawn here (4000, 4000)
			regions[15934] = new RegionClipping();

			for (int i = 0; i < size; i++) {
				byte[] file1 = getBuffer(new File(Constants.CONFIG_DIRECTORY + "world/map/" + mapObjectsFileIds[i] + ".gz"));
				byte[] file2 = getBuffer(new File(Constants.CONFIG_DIRECTORY + "world/map/" + mapGroundFileIds[i] + ".gz"));

				if (file1 == null || file2 == null) {
					continue;
				}

				try {
					loadMaps(regionIds[i], new ByteStream(file1), new ByteStream(file2));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			Logger.getLogger("Region").info("Loaded cache regions successfully.");
		} catch (Exception e) {
			
		}
	}

	private static void loadMaps(int regionId, ByteStream str1, ByteStream str2) {
		int absX = (regionId >> 8) * 64;
		int absY = (regionId & 0xff) * 64;
		int[][][] someArray = new int[4][64][64];
		for (int i = 0; i < 4; i++) {
			for (int i2 = 0; i2 < 64; i2++) {
				for (int i3 = 0; i3 < 64; i3++) {
					while (true) {
						int v = str2.getUByte();
						if (v == 0) {
							break;
						} else if (v == 1) {
							str2.skip(1);
							break;
						} else if (v <= 49) {
							str2.skip(1);
						} else if (v <= 81) {
							someArray[i][i2][i3] = v - 49;
						}
					}
				}
			}
		}
		for (int i = 0; i < 4; i++) {
			for (int i2 = 0; i2 < 64; i2++) {
				for (int i3 = 0; i3 < 64; i3++) {
					if ((someArray[i][i2][i3] & 1) == 1) {
						int height = i;
						if ((someArray[1][i2][i3] & 2) == 2) {
							height--;
						}
						if (height >= 0 && height <= 3) {
							addClipping(null, absX + i2, absY + i3, height, 0x200000, true);
						}
					}
				}
			}
		}
		int objectId = -1;
		int incr;
		while ((incr = str1.getUSmart()) != 0) {
			objectId += incr;
			int location = 0;
			int incr2;
			while ((incr2 = str1.getUSmart()) != 0) {
				location += incr2 - 1;
				int localX = (location >> 6 & 0x3f);
				int localY = (location & 0x3f);
				int height = location >> 12;
				int objectData = str1.getUByte();
				int type = objectData >> 2;
				int direction = objectData & 0x3;

				if (localX < 0 || localX >= 64 || localY < 0 || localY >= 64) {
					continue;
				}

				if ((someArray[1][localX][localY] & 2) == 2) {
					height--;
				}

				if (height >= 0 && height <= 3) {
					addObject(null, objectId, absX + localX, absY + localY, height, type, direction);
				}
			}
		}
	}

	public static byte[] getBuffer(File f) throws Exception {
		if (!f.exists()) {
			return null;
		}

		byte[] buffer = new byte[(int) f.length()];

		DataInputStream dis = new DataInputStream(new FileInputStream(f));
		dis.readFully(buffer);
		dis.close();

		byte[] gzipInputBuffer = new byte[999999];
		int bufferlength = 0;

		GZIPInputStream gzip = new GZIPInputStream(new ByteArrayInputStream(buffer));
		do {
			if (bufferlength == gzipInputBuffer.length) {
				System.out.println("Error inflating data.\nGZIP buffer overflow.");
				break;
			}

			int readByte = gzip.read(gzipInputBuffer, bufferlength, gzipInputBuffer.length - bufferlength);

			if (readByte == -1) {
				break;
			}

			bufferlength += readByte;
		}
		while (true);

		byte[] inflated = new byte[bufferlength];

		System.arraycopy(gzipInputBuffer, 0, inflated, 0, bufferlength);
		buffer = inflated;

		if (buffer.length < 10) {
			return null;
		}
		return buffer;
	}
}
