package com.exorth.rs2.util.clip;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class ByteStream {

	private int pointer;
	private byte[] buffer;

	public ByteStream(byte[] buffer) {
		this.buffer = buffer;
		this.pointer = 0;
	}

	public void skip(int length) {
		pointer += length;
	}

	public void setPointer(int position) {
		pointer = position;
	}

	public void setOffset(long position) {
		pointer = (int) position;
	}

	public int length() {
		return buffer.length;
	}

	public byte getByte() {
		return buffer[pointer++];
	}

	public int getUByte() {
		return buffer[pointer++] & 0xff;
	}

	public int getShort() {
		int val = (getByte() << 8) + getByte();
		if (val > 32767) {
			val -= 0x10000;
		}
		return val;
	}

	public int getUShort() {
		return (getUByte() << 8) + getUByte();
	}

	public int getInt() {
		return (getUByte() << 24) + (getUByte() << 16) + (getUByte() << 8) + getUByte();
	}

	public long getLong() {
		return (getUByte() << 56) + (getUByte() << 48) + (getUByte() << 40) + (getUByte() << 32) + (getUByte() << 24) + (getUByte() << 16) + (getUByte() << 8) + getUByte();
	}

	public int getUSmart() {
		int i = buffer[pointer] & 0xff;
		if (i < 128) {
			return getUByte();
		} else {
			return getUShort() - 32768;
		}
	}

	public String getNString() {
		int offset = pointer;
		while (buffer[pointer++] != 0) {
			/* Empty */
		}
		return new String(buffer, offset, pointer - offset - 1);
	}

	public byte[] getBytes() {
		int offset = pointer;
		while (buffer[pointer++] != 10) {
			/* Empty */
		}
		byte[] data = new byte[pointer - offset - 1];
		System.arraycopy(buffer, offset, data, offset - offset, pointer - 1 - offset);
		return data;
	}

	public byte[] read(int length) {
		byte[] b = new byte[length];
		for (int i = 0; i < length; i++) {
			b[i] = buffer[pointer++];
		}
		return b;
	}
}
