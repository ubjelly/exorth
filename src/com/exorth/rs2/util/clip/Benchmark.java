package com.exorth.rs2.util.clip;

/**
 * A simple class used to benchmark time.
 * 
 * @author Blake Beaupain
 */
public class Benchmark {

	/** The start time. */
	private long startTime;

	/** The stop time. */
	private long stopTime;

	/**
	 * Starts the benchmark.
	 */
	public void start() {
		startTime = System.currentTimeMillis();
	}

	/**
	 * Stops the benchmark.
	 */
	public void stop() {
		stopTime = System.currentTimeMillis();
	}

	/**
	 * Gets the benchmark time, in milliseconds.
	 * 
	 * @return The benchmark time
	 */
	public long getTime() {
		return stopTime - startTime;
	}
}
