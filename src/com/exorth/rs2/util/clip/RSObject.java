package com.exorth.rs2.util.clip;

import com.exorth.rs2.model.Position;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class RSObject {

	private int id;
	private int x;
	private int y;
	private byte plane;
	private Position position;
	private byte type;
	private byte face;

	/**
	 * The maximum amount of health this object has (for trees).
	 */
	private short maxHealth;

	/**
	 * The current health this object has remaining (for trees).
	 */
	private short currentHealth;

	public RSObject(int objectId, int x, int y, int plane, int type, int face) {
		this.id = objectId;
		this.x = x;
		this.y = y;
		this.plane = (byte) plane;
		this.type = (byte) type;
		this.face = (byte) face;
	}

	/**
	 * 
	 * @param objectId The ID of the object.
	 * @param position The position the object is at.
	 * @param type The type of object.
	 * @param face The direction it faces.
	 */
	public RSObject(int objectId, Position position, int type, int face) {
		this.id = objectId;
		this.position = position;
		this.type = (byte) type;
		this.face = (byte) face;
	}

	public Position getPosition() {
		return position;
	}

	public int getFace() {
		return this.face;
	}

	public int getType() {
		return this.type;
	}

	public int getId() {
		return this.id;
	}

	/**
	 * @return the maxHealth
	 */
	public int getMaxHealth() {
		return maxHealth;
	}

	/**
	 * @param maxHealth the maxHealth to set
	 */
	public void setMaxHealth(int maxHealth) {
		this.maxHealth = (short) maxHealth;
		this.currentHealth = (short) maxHealth;
	}

	/**
	 * @return the currentHealth
	 */
	public int getCurrentHealth() {
		return currentHealth;
	}

	/**
	 * @param currentHealth the currentHealth to set
	 */
	public void setCurrentHealth(int currentHealth) {
		this.currentHealth = (short) currentHealth;
	}

	/**
	 * @param currentHealth the currentHealth to set
	 */
	public void decreaseCurrentHealth(int amount) {
		this.currentHealth -= amount;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public byte getHeight() {
		return plane;
	}
}
