package com.exorth.rs2.util;

/**
 * 
 * @author AkZu
 * @author Jacob Ford
 * 
 */
public class Language {

	// The shop has run out of stock.
	// Rune arrow: currently costs 400 coins.
	// You're stunned!

	/**
	 * Your shield absorbs most of the dragon fire! Your potion protects you
	 * from the heat of the dragon's breath!
	 */

	/**
	 * You don't have enough inventory space to withdraw that many. You don't
	 * have enough bank space to deposit that many. (? not sure) You don't have
	 * enough inventory space to hold that item.
	 * 
	 * You have run out of prayer points, you must recharge at an altar.
	 */

	/*
	 * Additional not implemented yet but do not forget : Your ring of dueling
	 * has x uses left. Well done, you've completed the Treasure trails!
	 */

	/*
	 * Around oct 18, 2006
	 * 
	 * Item: Prayer potion (4)
	 * 
	 * You drink some of your restore prayer potion. You have 3 doses of potion
	 * left.
	 * 
	 * You have 1 dose of potion left.
	 * 
	 * You have finished your potion.
	 */

	/*
	 * Congratulations! You are now a master of Attack. Why not visit Ajjat at
	 * the Warriors' Guild? He has something special that is only avaible to
	 * true masters of the Attack skill!
	 */

	/**
	 * Prayer
	 */
	public static final String PRAYER_DROP = "You have run out of Prayer points; you can recharge at an altar.";
	public static final String OUT_OF_PRAYER = "You need to recharge your Prayer at an altar.";

	/**
	 * Combat
	 */
	public static final String NOT_IN_WILDERNESS = "You must be in the Wilderness in order to attack players.";
	public static final String VICTIM_NOT_IN_WILDERNESS = "You can't attack players who aren't in the Wilderness.";
	public static final String VICTIM_IN_COMBAT = "Someone else is already fighting your opponent.";
	public static final String ME_IN_COMBAT = "I'm already under attack.";
	public static final String OUT_OF_SPEC = "You do not have enough special energy left.";
	public static final String LEVEL_DIFFERENCE = "Your level difference is too great!";
	public static final String MOVE_DEEPER_TO_WILDERNESS = "You need to move deeper into the Wilderness.";
	public static final String DEAD = "Oh dear, you have died!";
	public static final String BLOCK_DRAGONFIRE = "Your shield absorbs most of the dragon fire!";
	// TODO For gmaul spec at least
	public static final String NOT_ATTACKING_ANYTHING = "You're not fighting anything.";

	/**
	 * Dueling
	 */
	// TODO This is 2 line chat interface
	public static final String ALL_ARENAS_FULL = "All arenas of this type are full! Select another type of arena, try\\nagain in a few minutes or try a different server.";

	public static final String SEND_DUEL_OFFER = "Sending duel offer...";
	public static final String ACCEPTED_STAKE = "Accepted stake and duel options.";
	public static final String DUEL_NOT_STARTED = "The duel has not started yet!";
	public static final String STAKE_WON = "Well done! You have defeated";

	/**
	 * Poison
	 */
	public static final String POISONED = "You have been poisoned!";
	public static final String POISON_WEAR_OFF = "The poison damaging you wears off..";

	/**
	 * Ranged
	 */
	public static final String OUT_OF_AMMO = "You have run out of ammo.";
	public static final String NOT_ENOUGH_ARROWS = "You do not have enough arrows.";
	public static final String WRONG_AMMO = "You can't use that ammo with this bow.";
	public static final String NO_RANGED_AMMO = "There is no ammo left in your quiver.";

	/**
	 * Magic
	 */
	public static final String CANT_AUTOCAST_ANCIENT = "You can't autocast Ancient magicks with this staff!";
	public static final String CANT_AUTOCAST_MODERN = "You can only autocast Ancient magicks with this staff!";
	public static final String BONES_TO_PEACHES = "You can only learn this spell from the Mage Training Arena.";
	public static final String CANT_TELE_20 = "You can't teleport above level 20 in the Wilderness.";
	public static final String CANT_TELE_30 = "You can't use this spell to teleport above level 30 in the Wilderness.";
	public static final String FROZEN = "You have been frozen!";
	public static final String MAGICAL_FORCE = "A magical force stops you from moving.";
	public static final String ALREADY_BIND = "Your target is already held by a magical force.";
	public static final String CHARGE_SPELL = "You feel charged with magic power.";
	public static final String IMMUNE_SPELL = "Your target is currently immune to that spell.";

	/**
	 * Random events
	 */
	// This is a 2line chat interface
	/*
	 * There has been a fault in the teleportation matrix. Please operate the
	 * odd appendage out to be forwarded to your destination.
	 */

	// Skill cape 1 line chat
	// You need to be wearing a Skill Cape in order to perform that emote.

	/**
	 * General
	 */
	public static final String CANT_REACH = "I can't reach that!";
	public static final String CONGRATZ = "Congratulations, you just advanced a"; // strength
	// level.
	public static final String NO_SPACE = "You don't have enough space in your inventory.";
	public static final String CANT_DO = "You can't do that at this moment.";
	public static final String IS_BUSY = "Other player is busy at the moment.";
	public static final String TRADING_WILDY = "Trading in the Wilderness is dangerous - you might get killed!";

	/**
	 * Bank
	 */
	public static final String CANT_WITHDRAW_AS_NOTE = "This item can't be withdrawn as a note.";
	public static final String BANK_ACCOUNT_FULL = "You don't have enough space in your bank account.";

	/**
	 * Bank PIN
	 */
	public static final String BANK_PIN_WRONG_PIN = "You have entered wrong PIN.";
	public static final String BANK_PIN_CORRECT_PIN = "You have correctly entered your PIN.";
}
