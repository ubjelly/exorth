package com.exorth.rs2.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class SQLConnector {

	 /**
	 * MySQL connection
	 * TODO: Rewrite (Mark)
	 */
	public static Connection forumConn = null;
	public static Statement forumStm;
	public static Connection serverConn = null;
	public static Statement serverStm;
	
	public static void forumConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			forumConn = DriverManager.getConnection("jdbc:mysql://play.ubernation.org/un3_site", "root", "fab4exorth");
			forumStm = forumConn.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void serverConnection() {
		try {
			
			if(serverConn != null && !serverConn.isClosed() && !serverStm.isClosed())
				return;
			
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			serverConn = DriverManager.getConnection("jdbc:mysql://localhost/exorth", "root", "");
			serverStm = serverConn.createStatement();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
