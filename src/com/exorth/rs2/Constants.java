package com.exorth.rs2;

public class Constants {

	public static final String SERVER_NAME = "Exorth";

	public static final String[] WELCOME_SCREEN_MESSAGES = {
		"Please report any bugs or glitches you may find and experience.", "We are currently under development!"
	};

	public static final int[] WELCOME_SCREEN_TEXTS = {
		17513, 15821, 15814, 15803, 15793, 15776, 15769, 15491
	};

	public static final int WELCOME_SCREEN_ITF = 0;

	// Represents individual modifiers for exp rates.
	// Index0 corresponds to Skill ID: 0 (Attack), Index1 corresponds to Skill ID: 1 (Defence), etc.
	public static final double[] EXP_RATE = {
		1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0
	};

	// Changes the amount of xp gained regardless of skill ID.
	public static final double GLOBAL_EXP_RATE = 10;

	// Networking
	public static boolean ACCEPT_CONNECTIONS = true;

	/**
	 * The starting position upon login.
	 */
	public static final int START_X = 2966;
	public static final int START_Y = 3382;
	public static final int START_Z = 0;

	public static final int MAX_PLAYERS = 2000;
	public static final int MAX_NPCS = 8192;
	public static final int MAX_ITEMS = 11791;
	public static final int MAX_ITEM_COUNT = Integer.MAX_VALUE;

	public static final int GROUND_START_TIME = 350;
	public static final int GROUND_START_TIME_DROP = 200;
	public static final int GROUND_START_TIME_UNTRADEABLE = 300;
	public static final int SHOW_ALL_GROUND_ITEMS = 250;

	public static final int LOGIN_RESPONSE_OK = 2;
	public static final int LOGIN_RESPONSE_INVALID_CREDENTIALS = 3;
	public static final int LOGIN_RESPONSE_ACCOUNT_DISABLED = 4;
	public static final int LOGIN_RESPONSE_ACCOUNT_ONLINE = 5;
	public static final int LOGIN_RESPONSE_UPDATED = 6;
	public static final int LOGIN_RESPONSE_WORLD_FULL = 7;
	public static final int LOGIN_RESPONSE_LOGIN_SERVER_OFFLINE = 8;
	public static final int LOGIN_RESPONSE_LOGIN_LIMIT_EXCEEDED = 9;
	public static final int LOGIN_RESPONSE_BAD_SESSION_ID = 10;
	public static final int LOGIN_RESPONSE_PLEASE_TRY_AGAIN = 11;
	public static final int LOGIN_RESPONSE_NEED_MEMBERS = 12;
	public static final int LOGIN_RESPONSE_COULD_NOT_COMPLETE_LOGIN = 13;
	public static final int LOGIN_RESPONSE_SERVER_BEING_UPDATED = 14;
	public static final int LOGIN_RESPONSE_LOGIN_ATTEMPTS_EXCEEDED = 16;
	public static final int LOGIN_RESPONSE_MEMBERS_ONLY_AREA = 17;

	public static final int EQUIPMENT_SLOT_HEAD = 0;
	public static final int EQUIPMENT_SLOT_CAPE = 1;
	public static final int EQUIPMENT_SLOT_AMULET = 2;
	public static final int EQUIPMENT_SLOT_WEAPON = 3;
	public static final int EQUIPMENT_SLOT_CHEST = 4;
	public static final int EQUIPMENT_SLOT_SHIELD = 5;
	public static final int EQUIPMENT_SLOT_LEGS = 7;
	public static final int EQUIPMENT_SLOT_HANDS = 9;
	public static final int EQUIPMENT_SLOT_FEET = 10;
	public static final int EQUIPMENT_SLOT_RING = 12;
	public static final int EQUIPMENT_SLOT_ARROWS = 13;

	public static final int APPEARANCE_SLOT_CHEST = 0;
	public static final int APPEARANCE_SLOT_ARMS = 1;
	public static final int APPEARANCE_SLOT_LEGS = 2;
	public static final int APPEARANCE_SLOT_HEAD = 3;
	public static final int APPEARANCE_SLOT_HANDS = 4;
	public static final int APPEARANCE_SLOT_FEET = 5;
	public static final int APPEARANCE_SLOT_BEARD = 6;

	public static final int SHOP_UPDATE_TICK = 10;

	/**
	 * Duel arena config orders
	 */
	public final static int[] duel_rule_configs = {
		1, 2, 16, 32, 64, 128, 256, 512, 1024, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 2097152, 8388608, 16777216, 67108864, 134217728
	};

	/**
	 * The indexes used to send correct icon to the client interface settings.
	 */
	public final static String[] MAGIC_SPELL_NAMES = {
		"Wind strike", "", "Water strike", "", "Earth strike", "", "Fire strike", "",
		"Wind bolt", "", "", "Water bolt", "", "", "Earth bolt", "", "", "Fire bolt", "", "",
		"Wind blast", "", "", "Water blast", "", "Earth blast", "", "", "", "Fire blast", "",
		"Wind wave", "", "Water wave", "", "", "Earth wave", "Fire wave", "", "", "", "", "",
		"Ice rush", "Ice burst", "Ice blitz", "Ice barrage",
		"Smoke rush", "Smoke burst", "Smoke blitz", "Smoke barrage",
		"Blood rush", "Blood burst", "Blood blitz", "Blood barrage",
		"Shadow rush", "Shadow burst", "Shadow blitz", "Shadow barrage"
	};

	/**
	 * Same index order to get their value.
	 */
	public static final String BONUS_NAME[] = {
		"Stab", "Slash", "Crush", "Magic", "Range", "Stab", "Slash", "Crush", "Magic", "Range", "Strength", "Prayer"
	};

	public static final int[] PACKET_LENGTHS = {
		0, 0, 0, 1, -1, 0, 0, 0, 0, 0, // 0
		0, 0, 0, 0, 8, 0, 6, 2, 2, 0, // 10
		0, 2, 0, 6, 0, 12, 0, 0, 0, 0, // 20
		0, 0, 0, 0, 0, 8, 4, 0, 0, 2, // 30
		2, 6, 0, 6, 0, -1, 0, 0, 0, 0, // 40
		0, 0, 0, 12, 0, 0, 0, 8, 8, 0, // 50
		0, 8, 0, 0, 0, 0, 0, 0, 0, 0, // 60
		6, 0, 2, 2, 8, 6, 0, -1, 0, 6, // 70
		0, 0, 0, 0, 0, 1, 4, 6, 0, 0, // 80
		0, 0, 0, 0, 0, 3, 0, 0, -1, 0, // 90
		0, 13, 0, -1, 0, 0, 0, 0, 0, 0,// 100
		0, 0, 0, 0, 0, 0, 0, 6, 0, 0, // 110
		1, 0, 6, 0, 0, 0, -1, 0, 2, 6, // 120
		0, 4, 6, 8, 0, 6, 0, 0, 0, 2, // 130
		0, 0, 0, 0, 0, 6, 0, 0, 0, 0, // 140
		0, 0, 1, 2, 0, 2, 6, 0, 0, 0, // 150
		0, 0, 0, 0, -1, -1, 0, 0, 0, 0,// 160
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, // 170
		0, 8, 0, 3, 0, 2, 0, 0, 8, 1, // 180
		0, 0, 12, 0, 0, 0, 0, 0, 0, 0, // 190
		2, 0, 0, 0, 0, 0, 0, 0, 4, 0, // 200
		4, 0, 0, 0, 7, 8, 0, 0, 10, 0, // 210
		0, 0, 0, 0, 0, 0, -1, 0, 6, 0, // 220
		1, 0, 0, 0, 6, 0, 6, 8, 1, 0, // 230
		0, 4, 0, 0, 0, 0, -1, 0, -1, 4,// 240
		0, 0, 6, 6, 0, 0, 0 // 250
	};

	public static final int EMPTY_OBJECT = 6951;

	/**
	 * Configuration storage directory.
	 */
	public static final String CONFIG_DIRECTORY = "./config/";
}