package com.exorth.rs2.task.impl;

import com.exorth.rs2.Server;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * Updates the jackpot value for players.
 * @author Stephen
 */
public class Jackpot extends Task {

	/**
	 * The player who is receiving the update.
	 */
	private Player player;

	/**
	 * Constructs a task that runs every 60 cycles.
	 */
	public Jackpot(Player player) {
		super(60);
		this.player = player;
	}

	/**
	 * The execution of the task.
	 */
	@Override
	protected void execute() {
		if (player == null || !player.isLoggedIn()) {
			this.stop();
			return;
		}
		
		player.getActionSender().sendString("The current jackpot is " + Server.getSlotData().getJackpot() + " gp!", 17502);
	}
}
