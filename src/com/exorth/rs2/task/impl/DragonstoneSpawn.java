package com.exorth.rs2.task.impl;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.GroundItem;
import com.exorth.rs2.task.Task;

/**
 * Spawns 6 dragonstone's in the Enchantment Chamber every 5 minutes.
 * 
 * @author Joshua Barry
 * 
 */
public class DragonstoneSpawn extends Task {

	Item dragonstone = new Item(1050);

	GroundItem groundItem = new GroundItem("wut", dragonstone, new Position(3222, 3222), false);

	public DragonstoneSpawn() {
		super(5);
	}

	@Override
	protected void execute() {
		// TODO: create 6 ground items in random locations across the
		// enchantment chamber.
	}
}
