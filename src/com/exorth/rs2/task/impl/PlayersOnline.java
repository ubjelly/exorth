package com.exorth.rs2.task.impl;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * Sends the amount of players online through a custom packet.
 * @author Stephen
 */
public class PlayersOnline extends Task {

	/**
	 * The player who is receiving the player count.
	 */
	private Player player;

	/**
	 * Constructs a task that runs each cycle.
	 */
	public PlayersOnline(Player player) {
		super(1);
		this.player = player;
	}

	/**
	 * The execution of the task.
	 */
	@Override
	protected void execute() {
		if (player == null || !player.isLoggedIn()) {
			this.stop();
			return;
		}
		
		player.getActionSender().sendWalkableInterface(18000);
		
		/**
		 * TODO: Finish the custom packet.
		 * This system in the source is garbage lol.
		 */
		//player.getActionSender().sendPlayersOnline(World.playerAmount());
	}
}
