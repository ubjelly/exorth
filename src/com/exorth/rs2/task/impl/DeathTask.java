package com.exorth.rs2.task.impl;

import com.exorth.rs2.content.combat.util.Prayer;
import com.exorth.rs2.listeners.impl.DefaultDeathSituation;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class DeathTask extends Task {

	/**
	 * The entity who gets killed.
	 */
	public Entity entity;

	private byte applyDeathTimer = 6;

	/**
	 * Creates the event to cycle every cycle.
	 */
	public DeathTask(Entity entity) {
		super(true);
		this.entity = entity;
	}

	@Override
	protected void execute() {
		// <AkZu> I clear this at dead method so we can process alot of stuff
		// with this as combating entity is set to null after a while
		// of not in combat

		if (applyDeathTimer == 6) {
			if (entity.getCombatingEntity() != null) {
				entity.getCombatingEntity().setCombatTimer(0);
			}
			entity.setAttribute("combat_opponent", entity.getCombatingEntity());
			entity.getUpdateFlags().faceEntity(65535);
		}

		if (applyDeathTimer > -1) {
			applyDeathTimer--;
		}

		if (applyDeathTimer == 3) {
			entity.getUpdateFlags().sendAnimation(entity.isPlayer() ? 836 : ((Npc) entity).getDefinition().getDeathAnimation(), entity.isNpc() ? ((Npc) entity).getDefinition().getDeathAnimDelay() : 0);
		} else if (applyDeathTimer == 1) {
			if (entity.isPlayer()) {
				Prayer.getInstance().applyRetributionPrayer(((Player) entity));
			}
		} else if (applyDeathTimer == 0) {
			new DefaultDeathSituation(entity).die();
			this.stop();
		}
	}
}
