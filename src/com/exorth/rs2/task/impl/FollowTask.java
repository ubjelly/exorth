package com.exorth.rs2.task.impl;

import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.Following;
import com.exorth.rs2.task.Task;

public class FollowTask extends Task {

	/**
	 * The mob for who we are following.
	 */
	public Entity entity;

	/**
	 * Creates the event to cycle every cycle.
	 */
	public FollowTask(Entity entity) {
		super(true); // Immediate
		this.entity = entity;
	}

	@Override
	protected void execute() {
		if (entity.getFollowingEntity() != null && entity.getAttribute("isDead") == null) {
			Following.getInstance().followEntity(entity);
		}
	}
}
