package com.exorth.rs2.task.impl;

import com.exorth.rs2.content.shop.Shop;
import com.exorth.rs2.content.shop.ShopManager;
import com.exorth.rs2.task.Task;

/**
 * A task for normalizing the shops.
 * 
 * @author Rait
 * 
 */
public class ShopNormalizationTask extends Task {

	public ShopNormalizationTask() {
		super(10);
	}

	@Override
	protected void execute() {
		for (Shop shop : ShopManager.getShops()) {
			shop.normalize();
		}
	}
}
