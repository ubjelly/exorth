package com.exorth.rs2.task.impl;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Some dude of R-S for the formula TODO Tweak it.
 * 
 */
public class RunRestore extends Task {

	/**
	 * The player who's run energy we're restoring.
	 */
	private Player player;

	/**
	 * Creates the task to run every cycle.
	 */
	public RunRestore(Player player) {
		super(1);
		this.player = player;
	}

	@Override
	protected void execute() {
		if (player == null || !player.isLoggedIn()) {
			this.stop();
			return;
		}

		// If we can't restore anymore energy, just return.
		if (player.getEnergy() == 100.0 || player.getAttribute("canRestoreEnergy") == null) {
			return;
		}

		player.setEnergy(player.getEnergy() + (0.096 * Math.pow(Math.E, 0.0162569486104454583293005993255170468638949631744294 * player.getSkill().getLevel()[16])));
	}
}
