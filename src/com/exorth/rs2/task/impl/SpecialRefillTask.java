package com.exorth.rs2.task.impl;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * The special energy refill task.
 * 
 * @author AkZu & Jacob
 */
public class SpecialRefillTask extends Task {

	/**
	 * The mob for who we are refilling the special energy.
	 */
	public Player player;

	/**
	 * Creates the event to cycle every 30 sec.
	 */
	public SpecialRefillTask(Player player) {
		super(50);
		this.player = player;
	}

	@Override
	protected void execute() {
		if (!player.isLoggedIn()) {
			this.stop();
			return;
		}
		player.setSpecialAmount(player.getSpecialAmount() + 1.0);
		if (player.getSpecialAmount() > 9.9) {
			player.setSpecialRefillTask(null);
			this.stop();
		}
	}
}
