package com.exorth.rs2.task.impl;

import com.exorth.rs2.content.combat.util.Poison;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Language;

public class PoisonTask extends Task {

	/**
	 * The mob for who we are poisoning.
	 */
	public Entity entity;

	/**
	 * Creates the event to cycle every cycle.
	 */
	public PoisonTask(Entity entity) {
		super(1);
		this.entity = entity;
	}

	@Override
	protected void execute() {
		if ((entity.isPlayer() && !((Player) entity).isLoggedIn()) || !entity.isPoisoned()) {
			stop();
			return;
		}

		if (entity.isPoisoned()) {
			if (entity.getPoisonHitTimer() == 0) {
				entity.hit(entity.getPoisonDamage(), 2, false);
				entity.setPoisonHitTimer(Poison.POISON_HIT_TIMER);
			} else {
				entity.setPoisonHitTimer(entity.getPoisonHitTimer() - 1);
			}

			for (int i = 1; i < 10; i++) {
				if (entity.getPoisonedTimer() == (Poison.POISON_HIT_TIMER * 5) * i) {
					entity.setPoisonDamage(entity.getPoisonDamage() - 1);
				}
			}

			if (entity.getPoisonDamage() == 1) {
				Poison.appendPoison(entity, false, 0);
				if (entity instanceof Player) {
					Player player = (Player) entity;
					player.getActionSender().sendMessage(Language.POISON_WEAR_OFF);
				}
			}

			entity.setPoisonedTimer(entity.getPoisonedTimer() + 1);
		}

		if (entity.getPoisonImmunityTimer() > 0) {
			entity.setPoisonImmunityTimer(entity.getPoisonImmunityTimer() - 1);
		}
	}
}
