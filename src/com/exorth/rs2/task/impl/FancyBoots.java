package com.exorth.rs2.task.impl;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class FancyBoots extends Task {

	private Player player;

	private Item boots;

	public FancyBoots(Player player) {
		super(1, true);
		this.player = player;
		this.boots = new Item(9005);
	}

	@Override
	protected void execute() {
		player.getInventory().addItem(this.boots);
		this.stop();
	}
}