package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class TreeFall extends Task {

	private Player player;

	public TreeFall(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.clipTeleport(new Position(2528, 3415, 0), 2);
		player.hit(8, 1, false);
		player.getUpdateFlags().sendForceMessage("Ouch!");
		player.getActionSender().sendMessage("You are washed up by the river side.");
		this.stop();
	}
}
