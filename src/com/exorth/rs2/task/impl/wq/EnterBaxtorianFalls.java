package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class EnterBaxtorianFalls extends Task {

	private Player player;

	public EnterBaxtorianFalls(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.clipTeleport(new Position(2574, 9864, 0), 2);
		player.getActionSender().sendMessage("You walk through the door.");
		this.stop();
	}
}
