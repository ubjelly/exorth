package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class SlabSlide extends Task {

	private Player player;

	public SlabSlide(Player player) {
		super(1);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.getActionSender().sendMessage("The stone slab slides back revealing a ladder down.");
		World.submit(new EnterGlarialsTomb(player));
		this.stop();
	}
}
