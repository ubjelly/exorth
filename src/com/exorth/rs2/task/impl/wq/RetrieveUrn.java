package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class RetrieveUrn extends Task {

	private Player player;

	public RetrieveUrn(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		Item urn = new Item(296, 1);
		if (player.getInventory().getItemContainer().hasRoomFor(urn)) {
			if (player.getInventory().addItem(urn)) {
				player.getActionSender().sendMessage("You take the urn and close the coffin.");
			}
		}
		this.stop();
	}
}
