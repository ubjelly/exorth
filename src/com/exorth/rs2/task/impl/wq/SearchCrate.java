package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class SearchCrate extends Task {

	private Player player;

	public SearchCrate(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		int id = 0;
		if (player.getClickId() == 1990) {
			id = 2840;
		} else {
			id = 2838;
		}
		Item key = new Item(id, 1);
		if (player.getInventory().getItemContainer().hasRoomFor(key)) {
			if (player.getInventory().addItem(key)) {
				player.getActionSender().sendMessage("and find a large key.");
			}
		}
		this.stop();
	}
}
