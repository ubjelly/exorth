package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class InsertPebble extends Task {

	private Player player;

	public InsertPebble(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.getActionSender().sendMessage("It fits perfectly.");
		World.submit(new TombCreak(player));
		this.stop();
	}
}
