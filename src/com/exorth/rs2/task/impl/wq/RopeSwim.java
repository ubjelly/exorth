package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class RopeSwim extends Task {

	private Player player;

	public RopeSwim(Player player) {
		super(5);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.removeAttribute("tempWalkAnim");
		player.setAppearanceUpdateRequired(true);
		this.stop();
	}
}
