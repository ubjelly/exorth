package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class GlarialsTomb extends Task {

	private Player player;

	public GlarialsTomb(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.getActionSender().sendMessage("Inside you find an urn full of ashes.");
		World.submit(new RetrieveUrn(player));
		this.stop();
	}
}
