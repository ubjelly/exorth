package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class LassoBoulder extends Task {

	private Player player;

	public LassoBoulder(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.getMovementHandler().addToPath(new Position(2512, 3469, 0));
		this.stop();
	}
}
