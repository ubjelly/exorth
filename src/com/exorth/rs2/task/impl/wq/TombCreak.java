package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class TombCreak extends Task {

	private Player player;

	public TombCreak(Player player) {
		super(1);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.getActionSender().sendMessage("You hear a loud creak.");
		World.submit(new SlabSlide(player));
		this.stop();
	}
}
