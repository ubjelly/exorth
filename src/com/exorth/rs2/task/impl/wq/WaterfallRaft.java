package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class WaterfallRaft extends Task {

	private Player player;

	public WaterfallRaft(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.getActionSender().sendMessage("The raft is pulled down stream by strong currents.");
		player.teleport(new Position(2512, 3480, 0));
		this.stop();
	}
}
