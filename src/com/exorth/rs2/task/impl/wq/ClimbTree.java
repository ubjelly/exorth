package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class ClimbTree extends Task {

	private Player player;

	public ClimbTree(Player player) {
		//super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.teleport(new Position(2511, 3463, 0));
		player.getActionSender().sendMessage("and let your self down on to the ledge.");
		this.stop();
	}
}
