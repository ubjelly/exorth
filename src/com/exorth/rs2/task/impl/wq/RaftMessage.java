package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class RaftMessage extends Task {

	private Player player;

	public RaftMessage(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.getActionSender().sendMessage("and push off down stream.");
		World.submit(new WaterfallRaft(player));
		this.stop();
	}
}
