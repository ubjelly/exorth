package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class EnterGlarialsTomb extends Task {

	private Player player;

	public EnterGlarialsTomb(Player player) {
		super(1);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.clipTeleport(new Position(2554, 9844, 0), 2);
		this.stop();
	}

}
