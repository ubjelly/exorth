package com.exorth.rs2.task.impl.wq;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

public class WaterfallBarrel extends Task {

	private Player player;

	public WaterfallBarrel(Player player) {
		super(3);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.clipTeleport(new Position(2528, 3415, 0), 2);
		this.stop();
	}
}
