package com.exorth.rs2.task.impl;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * The delay between collecting items.
 * 
 * @author Mko
 *
 */
public class CollectionDelay extends Task {

	private Player player;

	public CollectionDelay(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.removeAttribute("canTake");
		this.stop();
	}
}