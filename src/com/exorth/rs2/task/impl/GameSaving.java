package com.exorth.rs2.task.impl;

import com.exorth.rs2.io.AccountManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.network.VbIntegration;
import com.exorth.rs2.task.Task;

public class GameSaving extends Task {

	/**
	 * The player who's account we're saving.
	 */
	private Player player;

	/**
	 * Creates the task to run every cycle.
	 */
	public GameSaving(Player player) {
		super(30);
		this.player = player;
	}

	@Override
	protected void execute() {
		if (player == null || !player.isLoggedIn()) {
			this.stop();
			return;
		}
		try {
			AccountManager.save(player);
			VbIntegration.saveCharactertoSQL(player);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
