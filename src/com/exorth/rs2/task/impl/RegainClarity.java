package com.exorth.rs2.task.impl;

import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * Handles the event of undoing the ::confuse command
 * @author Mko
 *
 */
public class RegainClarity extends Task {

	private Player player;

	/**
	 * 
	 * @param player The player to be confused.
	 */
	public RegainClarity(Player player) {
		super(5);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.setCanWalk(true);
		player.getActionSender().removeInterfaces();
		player.getActionSender().sendMessage("You have regained clarity.");
		this.stop();
	}
}
