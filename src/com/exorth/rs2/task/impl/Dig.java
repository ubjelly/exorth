package com.exorth.rs2.task.impl;

import java.util.HashMap;
import java.util.Map;

import com.exorth.rs2.content.clue.Clue;
import com.exorth.rs2.content.clue.Type;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.area.Area;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.task.Task;

/**
 * Handles a Digging Task
 * 
 * @author Mko
 *
 */
public class Dig extends Task {

	private Player player;

	public Dig(Player player) {
		super(2);
		this.player = player;
	}

	@Override
	protected void execute() {
		player.getUpdateFlags().sendAnimation(-1);
		
		final HashMap<Area, Position> tombs = new HashMap<Area, Position>();
		tombs.put(new Area(new Position(3562, 3286, 0), new Position(3567, 3291, 0)), new Position(3557, 9703, 3)); //Ahrim
		tombs.put(new Area(new Position(3574, 3279, 0), new Position(3579, 3286, 0)), new Position(3535, 9704, 3)); //Guthan
		tombs.put(new Area(new Position(3572, 3295, 0), new Position(3577, 3301, 0)), new Position(3556, 9718, 3)); //Dharok
		tombs.put(new Area(new Position(3563, 3272, 0), new Position(3568, 3279, 0)), new Position(3546, 9684, 3)); //Karil
		tombs.put(new Area(new Position(3550, 3279, 0), new Position(3556, 3286, 0)), new Position(3568, 9683, 3)); //Torag
		tombs.put(new Area(new Position(3553, 3294, 0), new Position(3560, 3301, 0)), new Position(3578, 9706, 3)); //Verac

		for (Map.Entry<Area, Position> tomb : tombs.entrySet()) {
			Area area = tomb.getKey();
			Position position = tomb.getValue();
			if (area.isInArea(player.getPosition())) {
				player.teleport(position);
				player.getActionSender().sendMessage("You've broken into a crypt!");
				player.getActionSender().sendMinimapBlackout(2);
			}
		}

		for (com.exorth.rs2.content.clue.Map map : com.exorth.rs2.content.clue.Map.values()) {
			if (map.getArea().isInArea(player.getPosition())) {
				if (player.getInventory().getItemContainer().contains(map.getScrollId())) {
					Item scroll = new Item(map.getScrollId());
					if (player.getInventory().removeItem(scroll)) {
						Clue clue = new Clue(map.getDifficulty(), Type.MAPS);
						clue.complete(player);
					}
				}
			}
		}
		this.stop();
	}

}
