package com.exorth.rs2.task.impl;

import com.exorth.rs2.task.Task;

/**
 * Creates a new task which ticks for a specified time in seconds.
 * @author Stephen
 */
public abstract class SecondIntervalCounter extends Task {
	
	/**
	 * Constructs a new counter.
	 * @param duration The duration in seconds.
	 * @param immediate Whether or not to submit the task immediately.
	 */
	public SecondIntervalCounter(int duration, boolean immediate) {
		super(60 * duration, immediate);
	}

	/**
	 * Constructs a new counter.
	 * @param duration The duration in minutes.
	 */
	public SecondIntervalCounter(int duration) {
		super(60 * duration);
	}
}
