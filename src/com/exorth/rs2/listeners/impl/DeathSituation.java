package com.exorth.rs2.listeners.impl;

import java.text.DecimalFormat;
import java.util.List;
import java.util.Random;

import com.exorth.rs2.Constants;
import com.exorth.rs2.content.combat.Combat;
import com.exorth.rs2.content.combat.util.Poison;
import com.exorth.rs2.content.combat.util.Prayer;
import com.exorth.rs2.content.minigame.ItemSafety;
import com.exorth.rs2.content.minigame.hordedefence.Zombie;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.listeners.DeathListener;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.npcs.Boss;
import com.exorth.rs2.model.npcs.Drop;
import com.exorth.rs2.model.npcs.DropLoader;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.npcs.Npc.WalkType;
import com.exorth.rs2.model.npcs.NpcLoot;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Yeller;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.util.Misc;

/**
 * This is a skeletal death situation, it is the back bone to all other death
 * situations, taking the combined data and producing the result.
 * 
 * @author Joshua Barry
 * 
 */
public abstract class DeathSituation implements DeathListener {

	Boss boss;
	protected abstract Entity getEntity();
	protected abstract Object[] getRespawnPosition();
	protected abstract String getDeadMessage();
	protected abstract ItemSafety getItemSafety();

	@Override
	public void die() {
		// The killer who killed this entity.
		final Entity killer = getEntity().getAttribute("combat_opponent");

		// TODO: NPC's death....
		if (getEntity().isNpc()) {
			final Npc npc = (Npc) getEntity();

			//Horde minigame
			if (npc.getNpcId() == 73) {
				Zombie.die(npc);
				return;
			}
			
			// Wait until hidden timer is complete
			World.submit(new Task(npc.getDefinition().getHiddenTimer()) {
				@Override
				protected void execute() {
					// Drop the loot...
					if (killer != null && killer.isPlayer()) {
						/**
						 * Remove people who didn't do most damage
						 */
						npc.getCombatState().getDamageMap().removeInvalidEntries();

						final Player looter = (Player) ((npc.getCombatState().getDamageMap().highestDamage() != null) ? npc.getCombatState().getDamageMap().highestDamage() : null);

						if (looter != null) {
							Random random = new Random();
							final Position loot_position = new Position(npc.getPosition().getX(), npc.getPosition().getY(), npc.getPosition().getZ());

							List<Drop> drops = DropLoader.npcDrops.get(npc.getNpcId());
							final double roll = Double.valueOf(random.nextDouble());

							if (drops != null) {
								/**
								 * Handles items with a drop rate of 100%
								 */
								for (Drop drop : drops) {
									final Item droppedItem = new Item(drop.getItemID(), drop.getAmount());
									if (drop.getDropChance() >= 1) {
										World.submit(new Task(1) {
											@Override
											protected void execute() {
												ItemManager.getInstance().createGroundItem(looter, looter.getUsername(), droppedItem, loot_position, Constants.GROUND_START_TIME_DROP);
												this.stop();
											}
										});
									}
								}
								/**
								 * Handles items with a drop rate of less than 100%
								 */
								int randomRare = random.nextInt(drops.size());
								final Drop rareItem = drops.get(randomRare);
								final Item randomRareItem = new Item(rareItem.getItemID(), rareItem.getAmount());
								if (rareItem != null && rareItem.getDropChance() < 1) {
									World.submit(new Task(1) {
										@Override
										protected void execute() {
											if (roll < rareItem.getDropChance()){
											ItemManager.getInstance().createGroundItem(looter, looter.getUsername(), randomRareItem, loot_position, Constants.GROUND_START_TIME_DROP);
											}
											this.stop();
										}
									});
								}
							}
						}
						
						//Check to see if NPC was a boss
						Boss boss = new Boss(npc.getNpcId());
						if (boss.isBoss()) {
							Yeller.shout(boss.getName() + " has been slain by " + looter.getUsername() + " (Combat Level: " + looter.getSkill().getCombatLevel() + ")");
						}
						
						//Check to see if NPC was a slayer task
						if (npc.getNpcId() == looter.getSlayerTask()) {
							if (looter.getKillsLeft() >= 1) {
								looter.setKillsLeft(looter.getKillsLeft() - 1);
							} else {
								looter.getActionSender().sendMessage("Well done, you have completed your Slayer Task! Return to a Slayer Master for a new task.");
								looter.setSlayerTask(0);
								looter.setKillsLeft(0);
							}
							looter.getSkill().addExperience(Skill.SLAYER, looter.getSlayer().getTask().getXp());
						}
					}

					// NPC doesn't respawn anymore...Unregister it.
					if (npc.getDefinition().getRespawnTimer() == 0) {
						System.out.println("Unregistering NPC - " + npc.getName() + " " + npc.getNpcId());
						World.unregister(npc);
						return;
					}

					// Teleport the NPC away from sight for a while
					// TODO: If re-spawnable...
					npc.teleport(new Position(0, 0, 0));

					// Remove some more attributes
					npc.getCombatState().getDamageMap().reset(); 
					// Clear the damage map
					npc.removeAttribute("isFrozen");
					npc.removeAttribute("cantBeFrozen");
					Poison.appendPoison(npc, false, 0);
					npc.setCombatTimer(0);
					npc.getUpdateFlags().sendAnimation(65535, 0);
					npc.setCanWalk(true);

					// Reset combat
					Combat.getInstance().resetCombat(npc);

					// TODO: Restore skills
					npc.setAttribute("current_hp", npc.getDefinition().getCombatLevel(3));

					// Respawn the NPC
					World.submit(new Task(npc.getDefinition().getRespawnTimer()) {
						@Override
						protected void execute() {
							npc.removeAttribute("isDead");

							if (npc.getWalkType() == WalkType.WALK) {
								npc.clipTeleport(new Position(npc.getSpawnPosition().getX(), npc.getSpawnPosition().getY(), npc.getSpawnPosition().getZ()), 0, npc.getMaxWalkingArea().getX(), npc.getMaxWalkingArea().getY());
							} else {
								npc.teleport(npc.getSpawnPosition());
							}

							// Finally...
							npc.removeAttribute("combat_opponent");
							npc.actions_after_spawn();
							this.stop();
						}
					});
					this.stop();
				}
			});
		}

		// TODO: Players death...
		if (getEntity().isPlayer()) {
			final Player player = (Player) getEntity();

			if (player.getMinigame() != null && killer != null && killer.isPlayer()) {
				player.getMinigame().defeatListener(player, killer);
			}

			// Send kill messages if you're not inside minigame
			if (player.getMinigame() == null && killer != null && killer.isPlayer()) {
				Player otherPlayer = (Player) killer;

				String[] randomKillMessages =
				{ "You have defeated " + player.getUsername() + "!", player.getUsername() + " won't cross your path again!", "Good fight, " + player.getUsername() + ".", player.getUsername() + " will feel that in Lumbridge.", "Can anyone defeat you? Certainly not " + player.getUsername() + ".", player.getUsername() + " falls before your might.", "A humiliating defeat for " + player.getUsername() + ".", "You were clearly a better fighter than " + player.getUsername() + ".", player.getUsername() + " has won a free ticket to Lumbridge.", "It's all over for " + player.getUsername() + ".", "With a crushing blow you finish " + player.getUsername() + ".", player.getUsername() + " regrets the day they met you in combat.", player.getUsername() + " didn't stand a chance against you." };

				otherPlayer.getActionSender().sendMessage(randomKillMessages[Misc.random(randomKillMessages.length)]);
				Combat.getInstance().resetCombat(otherPlayer);
			}
			if (killer.isNpc() && !killer.isPlayer()) {
				boss = new Boss(((Npc)killer).getNpcId());
			}
			// Drop loot if it's allowed
			if (getItemSafety() == ItemSafety.UNSAFE &&(!killer.isNpc() || boss.isBoss())) {
				// First remove the invalid entries from players who has hit
				// this player, so only latest hits with most damage dealt will
				// get loot.
				player.getCombatState().getDamageMap().removeInvalidEntries();

				final Entity looter = (Entity) (player.getCombatState().getDamageMap().highestDamage() != null ? player.getCombatState().getDamageMap().highestDamage() : player);

				player.dropLoot(looter.isNpc() ? null : ((Player) looter));
			}

			Combat.getInstance().fixAttackStyles(player, player.getEquipment().get(3));
			player.getActionSender().sendMessage(getDeadMessage());
			player.getCombatState().getDamageMap().reset();
			// Clear the damage map.
			Combat.getInstance().resetCombat(player);

			player.setSpecialAttackActive(false);
			player.setSpecialAmount(10);
			player.setCombatTimer(0);
			player.setCanWalk(true);
			player.removeAttribute("isFrozen");
			player.removeAttribute("teleBlocked");
			player.removeAttribute("charge");
			player.removeAttribute("cant_teleport");
			player.removeAttribute("chargeCoolDown");
			player.setSkulled(false);
			Poison.appendPoison(player, false, 0);
			Prayer.getInstance().resetAll(player);
			player.getUpdateFlags().sendAnimation(65535, 0);
			player.getActionSender().sendWalkableInterface(-1);
			player.getSkill().normalize();
			player.removeAttribute("isDead");

			// Teleport the player
			player.clipTeleport((Position) getRespawnPosition()[0], (Integer) getRespawnPosition()[1], (Integer) getRespawnPosition()[2], (Integer) getRespawnPosition()[3]);

			// Null the player's minigame (if any) when they die.
			if (player.getMinigame() != null && !player.getMinigame().getGameName().equals("Fight Pits")) {
				player.setMinigame(null);
			}

			// Null the combating entity, that's used for death method
			player.removeAttribute("combat_opponent");
		}
	}
}
