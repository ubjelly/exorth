package com.exorth.rs2.listeners;

/**
 * 
 * @author Joshua Barry
 * 
 */
public interface DeathListener {

	public abstract void die();

}
