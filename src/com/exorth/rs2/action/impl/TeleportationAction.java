package com.exorth.rs2.action.impl;

import com.exorth.rs2.action.Action;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.task.Task;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * 
 */
public class TeleportationAction extends Action {

	private TeleportType type;

	private Position position;

	private int offset;

	// Used for teleother
	private Entity caster;

	/**
	 * 
	 * @param entity
	 * @param type
	 * @param position
	 * @param offset
	 */
	public TeleportationAction(Entity entity, TeleportType type, Position position, int offset) {
		super(entity, 0);
		this.type = type;
		this.position = position;
		this.offset = 0;
	}

	/**
	 * 
	 * @param entity The player who teleports
	 * @param caster The caster used in teleother
	 * @param type The type of teleporation
	 * @param position The position they end up at
	 * @param offset The offset of the position
	 */
	public TeleportationAction(Entity entity, Entity caster, TeleportType type, Position position, int offset) {
		super(entity, 0);
		this.type = type;
		this.position = position;
		this.offset = 0;
		this.caster = caster;
	}

	@Override
	public CancelPolicy getCancelPolicy() {
		return CancelPolicy.ALWAYS;
	}

	@Override
	public StackPolicy getStackPolicy() {
		return StackPolicy.ALWAYS;
	}

	@Override
	public AnimationPolicy getAnimationPolicy() {
		return AnimationPolicy.RESET_ALL;
	}

	@Override
	public void execute() {
		switch (type) {
			case MODERN:
				getEntity().getUpdateFlags().sendAnimation(type.getAnim(), 7);
				getEntity().getUpdateFlags().sendGraphic(type.getGfx(), 0, 100);
				break;
			case ANCIENT:
				getEntity().getUpdateFlags().sendAnimation(type.getAnim(), 0);
				getEntity().getUpdateFlags().sendGraphic(type.getGfx(), 0, 0);
				break;
			case TABLET:
				getEntity().getUpdateFlags().sendAnimation(type.getAnim(), 0);
				getEntity().getUpdateFlags().sendGraphic(type.getGfx(), 0, 0);
				break;
			case TELEOTHER:
				System.out.println("Caster: " + caster.getName() + ".");
				caster.getUpdateFlags().sendAnimation(type.getOtherAnim());
				caster.getInteractingEntity().getUpdateFlags().sendGraphic(type.getOtherGfx(), 0, 100);
				getEntity().getUpdateFlags().sendAnimation(type.getAnim(), 0);
				getEntity().getUpdateFlags().sendGraphic(type.getGfx(), 0, 0);
				break;
		}
		getEntity().setAttribute("cant_teleport", 0);

		int ticks = (type.ordinal() + 3);

		World.submit(new Task(ticks) {
			@Override
			protected void execute() {
				getEntity().clipTeleport(position, offset);
				switch (type) {
					case MODERN:
						getEntity().getUpdateFlags().sendAnimation(715, 1);
					default:
						getEntity().removeAttribute("cant_teleport");
						break;
				}
				this.stop();
			}
		});

		/* end the teleportation action */
		this.stop();
	}

	/**
	 * 
	 * @author Joshua Barry <Sneakyhearts>
	 * 
	 */
	public enum TeleportType {
		MODERN(714, 111),
		ANCIENT(1979, 392),
		TABLET(4731, 678),
		TELEOTHER(714, 342, 1818, 343);

		private int anim;

		private int gfx;

		private int otherAnim;

		private int otherGfx;

		/**
		 * 
		 * @param anim
		 * @param gfx
		 */
		private TeleportType(int anim, int gfx) {
			this.anim = anim;
			this.gfx = gfx;
		}

		/**
		 * 
		 * @param anim
		 * @param gfx
		 */
		private TeleportType(int anim, int gfx, int otherAnim, int otherGfx) {
			this.anim = anim;
			this.gfx = gfx;
			this.otherAnim = otherAnim;
			this.otherGfx = otherGfx;

		}

		public int getAnim() {
			return anim;
		}

		public int getGfx() {
			return gfx;
		}

		public int getOtherAnim() {
			return otherAnim;
		}

		public int getOtherGfx() {
			return otherGfx;
		}
	}
}