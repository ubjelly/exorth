package com.exorth.rs2.io;

/*
 * This file is part of RuneSource.
 *
 * RuneSource is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RuneSource is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with RuneSource. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;

import com.exorth.rs2.Server;
import com.exorth.rs2.content.combat.util.CombatState.CombatStyle;
import com.exorth.rs2.content.quest.Quest;
import com.exorth.rs2.content.quest.QuestRepository;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.BankManager;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.Player.MagicBookTypes;

/**
 * Static utility methods for saving and loading players.
 * 
 * @author Joshua Barry
 * @author blakeman8192
 * TODO: Perhaps remove saving accounts to .dats, and change to MySQL? (Mark)
 */
public class AccountManager {

	/** The directory where players are saved. */
	public static final String directory = "./accounts/";

	/**
	 * 1/1/2013 by akzu made it so to save space if characters are made and
	 * logged out without designing them so it only saves pass and rights and
	 * coords
	 */
	public static void save(Player player) throws Exception {
		File file = new File(directory);

		if (!file.exists() && !file.mkdir()) {
			throw new FileNotFoundException();
		}

		BufferedWriter filewriter = new BufferedWriter(new FileWriter(file.getAbsolutePath() + File.separator + player.getDatabaseId() + ".dat"));

		// TODO For bugs check this
		if (player.getAttribute("hasDesigned") != null) {
			/**
			 * Quest states
			 */
			for (Quest quest : QuestRepository.QUEST_DATABASE.values()) {
				if (player.getQuestStorage().hasStarted(quest) || player.getQuestStorage().hasFinished(quest)) {
					filewriter.write("QUEST_" + quest.getQuestButton() + "_STATE = ", 0, ("QUEST_" + quest.getQuestButton() + "_STATE = ").length());
					filewriter.write("" + player.getQuestStorage().getState(quest), 0, ("" + player.getQuestStorage().getState(quest)).length());
					filewriter.newLine();
				}
			}

			/**
			 * Settings stuff
			 */
			filewriter.write("MOUSE_BUTTONS = ", 0, ("MOUSE_BUTTONS = ").length());
			filewriter.write("" + player.getAttribute("mouseButtons"), 0, ("" + player.getAttribute("mouseButtons")).length());
			filewriter.newLine();

			filewriter.write("CHAT_EFFECTS = ", 0, ("CHAT_EFFECTS = ").length());
			filewriter.write("" + player.getAttribute("chatEffects"), 0, ("" + player.getAttribute("chatEffects")).length());
			filewriter.newLine();

			filewriter.write("SPLIT_PRIVATE = ", 0, ("SPLIT_PRIVATE = ").length());
			filewriter.write("" + player.getAttribute("splitPrivate"), 0, ("" + player.getAttribute("splitPrivate")).length());
			filewriter.newLine();

			filewriter.write("ACCEPT_AID = ", 0, ("ACCEPT_AID = ").length());
			filewriter.write("" + player.getAttribute("acceptAid"), 0, ("" + player.getAttribute("acceptAid")).length());
			filewriter.newLine();

			filewriter.write("RUN_ENERGY = ", 0, ("RUN_ENERGY = ").length());
			filewriter.write("" + player.getAttribute("runEnergy"), 0, ("" + player.getAttribute("runEnergy")).length());
			filewriter.newLine();

			/**
			 * Combat stuff
			 */
			if (player.shouldAutoRetaliate()) {
				filewriter.write("AUTO_RETALIATE", 0, ("AUTO_RETALIATE").length());
				filewriter.newLine();
			}

			if (player.getMagicBookType() != MagicBookTypes.MODERN) {
				filewriter.write("SPELLBOOK = 1", 0, ("SPELLBOOK = 1").length());
				filewriter.newLine();
			}

			for (int state = 0; state <= 10; state++) {
				if (player.getAttribute(("crystalBow" + state)) != null) {
					filewriter.write("CRYSTAL_BOW_" + state + " = ", 0, ("CRYSTAL_BOW_" + state + " = ").length());
					filewriter.write("" + player.getAttribute(("crystalBow" + state)), 0, ("" + player.getAttribute(("crystalBow" + state))).length());
					filewriter.newLine();
				}
			}

			if (player.getAttribute("teleBlocked") != null && System.currentTimeMillis() - (Long) player.getAttribute("teleBlocked") < 300000) {
				filewriter.write("TELEBLOCK = ", 0, ("TELEBLOCK = ").length());
				filewriter.write("" + player.getAttribute("teleBlocked"), 0, ("" + player.getAttribute("teleBlocked")).length());
				filewriter.newLine();
			}

			if (player.getAttribute("charge") != null && System.currentTimeMillis() - (Long) player.getAttribute("charge") < 420000) {
				filewriter.write("CHARGE = ", 0, ("CHARGE = ").length());
				filewriter.write("" + player.getAttribute("charge"), 0, ("" + player.getAttribute("charge")).length());
				filewriter.newLine();
			}

			if (player.getAttribute("chargeCoolDown") != null && System.currentTimeMillis() - (Long) player.getAttribute("chargeCoolDown") < 60000) {
				filewriter.write("CHARGE_COOL_DOWN = ", 0, ("CHARGE_COOL_DOWN = ").length());
				filewriter.write("" + player.getAttribute("chargeCoolDown"), 0, ("" + player.getAttribute("chargeCoolDown")).length());
				filewriter.newLine();
			}

			if (player.getAttribute("homeTeleTimer") != null && System.currentTimeMillis() - (Long) player.getAttribute("homeTeleTimer") < 1800000) {
				filewriter.write("HOME_TELE_TIMER = ", 0, ("HOME_TELE_TIMER = ").length());
				filewriter.write("" + player.getAttribute("homeTeleTimer"), 0, ("" + player.getAttribute("homeTeleTimer")).length());
				filewriter.newLine();
			}

			filewriter.write("SPECIAL_AMOUNT = ", 0, ("SPECIAL_AMOUNT = ").length());
			filewriter.write("" + player.getSpecialAmount(), 0, ("" + player.getSpecialAmount()).length());
			filewriter.newLine();

			filewriter.write("COMBAT_STYLE = ", 0, ("COMBAT_STYLE = ").length());
			if (player.getCombatState().getCombatStyle() == CombatStyle.AUTOCAST || player.getCombatState().getCombatStyle() == CombatStyle.DEFENSIVE_AUTOCAST) {
				filewriter.write("" + player.getAttribute("oldCombatStyle"), 0, ("" + player.getAttribute("oldCombatStyle")).length());
			} else {
				filewriter.write("" + player.getCombatState().getCombatStyle().getId(), 0, ("" + player.getCombatState().getCombatStyle().getId()).length());
			}
			filewriter.newLine();

			if (player.isPoisoned()) {
				filewriter.write("POISONED = ", 0, ("POISONED = ").length());
				filewriter.write("" + player.isPoisoned(), 0, ("" + player.isPoisoned()).length());
				filewriter.newLine();
				filewriter.write("POISON_DAMAGE = ", 0, ("POISON_DAMAGE = ").length());
				filewriter.write("" + player.getPoisonDamage(), 0, ("" + player.getPoisonDamage()).length());
				filewriter.newLine();
				filewriter.write("POISON_TIMER = ", 0, ("POISON_TIMER = ").length());
				filewriter.write("" + player.getPoisonedTimer(), 0, ("" + player.getPoisonedTimer()).length());
				filewriter.newLine();
				filewriter.write("POISON_HIT_TIMER = ", 0, ("POISON_HIT_TIMER = ").length());
				filewriter.write("" + player.getPoisonHitTimer(), 0, ("" + player.getPoisonHitTimer()).length());
				filewriter.newLine();
				filewriter.write("POISON_IMMUNITY_TIMER = ", 0, ("POISON_IMMUNITY_TIMER = ").length());
				filewriter.write("" + player.getPoisonImmunityTimer(), 0, ("" + player.getPoisonImmunityTimer()).length());
				filewriter.newLine();
			}

			/**
			 * Welcome screen stuff
			 */
			filewriter.write("LAST_LOGIN_IP = ", 0, ("LAST_LOGIN_IP = ").length());
			filewriter.write(player.getLastIp(), 0, (player.getLastIp()).length());
			filewriter.newLine();

			filewriter.write("LAST_LOGIN_DATE = ", 0, ("LAST_LOGIN_DATE = ").length());
			filewriter.write(player.getLastLoginDay(), 0, (player.getLastLoginDay()).length());
			filewriter.newLine();

			/**
			 * Bank PIN stuff
			 */
			if (player.getBankPin().hasBankPin()) {
				filewriter.write("BANK_PIN = ", 0, ("BANK_PIN = ").length());
				filewriter.write("" + player.getBankPin().getBankPin()[0], 0, ("" + player.getBankPin().getBankPin()[0]).length());
				filewriter.write("" + player.getBankPin().getBankPin()[1], 0, ("" + player.getBankPin().getBankPin()[1]).length());
				filewriter.write("" + player.getBankPin().getBankPin()[2], 0, ("" + player.getBankPin().getBankPin()[2]).length());
				filewriter.write("" + player.getBankPin().getBankPin()[3], 0, ("" + player.getBankPin().getBankPin()[3]).length());
				filewriter.newLine();
			}

			if (player.getBankPin().hasPendingBankPin()) {
				filewriter.write("PENDING_BANK_PIN = ", 0, ("PENDING_BANK_PIN = ").length());
				filewriter.write("" + player.getBankPin().getPendingBankPin()[0], 0, ("" + player.getBankPin().getPendingBankPin()[0]).length());
				filewriter.write("" + player.getBankPin().getPendingBankPin()[1], 0, ("" + player.getBankPin().getPendingBankPin()[1]).length());
				filewriter.write("" + player.getBankPin().getPendingBankPin()[2], 0, ("" + player.getBankPin().getPendingBankPin()[2]).length());
				filewriter.write("" + player.getBankPin().getPendingBankPin()[3], 0, ("" + player.getBankPin().getPendingBankPin()[3]).length());
				filewriter.newLine();
			}

			if (player.getBankPin().getBankPINChangeDays() != 7) {
				filewriter.write("BANK_PIN_CHANGE_DAYS = ", 0, ("BANK_PIN_CHANGE_DAYS = ").length());
				filewriter.write("" + player.getBankPin().getBankPINChangeDays(), 0, ("" + player.getBankPin().getBankPINChangeDays()).length());
				filewriter.newLine();
			}

			if (player.getAttribute("bankPinWaitTimer") != null) {
				filewriter.write("BANK_PIN_WAIT_TIMER = ", 0, ("BANK_PIN_WAIT_TIMER = ").length());
				filewriter.write("" + player.getAttribute("bankPinWaitTimer"), 0, ("" + player.getAttribute("bankPinWaitTimer")).length());
				filewriter.newLine();
			}

			if (player.getBankPin().getPinAppendDay() > -1) {
				filewriter.write("PIN_APPEND_DAY = ", 0, ("PIN_APPEND_DAY = ").length());
				filewriter.write("" + player.getBankPin().getPinAppendDay(), 0, ("" + player.getBankPin().getPinAppendDay()).length());
				filewriter.newLine();
			}

			if (player.getBankPin().getPinAppendYear() > -1) {
				filewriter.write("PIN_APPEND_YEAR = ", 0, ("PIN_APPEND_YEAR = ").length());
				filewriter.write("" + player.getBankPin().getPinAppendYear(), 0, ("" + player.getBankPin().getPinAppendYear()).length());
				filewriter.newLine();
			}

			if (player.getBankPin().isChangingBankPin()) {
				filewriter.write("PIN_CHANGING = ", 0, ("PIN_CHANGING = ").length());
				filewriter.write("" + player.getBankPin().isChangingBankPin(), 0, ("" + player.getBankPin().isChangingBankPin()).length());
				filewriter.newLine();
			}

			if (player.getBankPin().isDeletingBankPin()) {
				filewriter.write("PIN_DELETING = ", 0, ("PIN_DELETING = ").length());
				filewriter.write("" + player.getBankPin().isDeletingBankPin(), 0, ("" + player.getBankPin().isDeletingBankPin()).length());
				filewriter.newLine();
			}

			for (int i = 0; i < player.getAppearance().length; i++) {
				filewriter.write("APPEARANCE[" + i + "] = ", 0, ("APPEARANCE[" + i + "] = ").length());
				filewriter.write("" + player.getAppearance()[i], 0, ("" + player.getAppearance()[i]).length());
				filewriter.newLine();
			}

			for (int i = 0; i < player.getColors().length; i++) {
				filewriter.write("APPEARANCE_COLORS[" + i + "] = ", 0, ("APPEARANCE_COLORS[" + i + "] = ").length());
				filewriter.write("" + player.getColors()[i], 0, ("" + player.getColors()[i]).length());
				filewriter.newLine();
			}

			for (int i = 0; i < 28; i++) {
				Item item = player.getInventory().getItemContainer().get(i);
				if (item == null) {
					continue;
				}
				filewriter.write("INVENTORY_ITEM[" + i + "] = ", 0, ("INVENTORY_ITEM[" + i + "] = ").length());
				filewriter.write("" + item.getId(), 0, ("" + item.getId()).length());
				filewriter.newLine();
				filewriter.write("INVENTORY_AMOUNT[" + i + "] = ", 0, ("INVENTORY_AMOUNT[" + i + "] = ").length());
				filewriter.write("" + item.getCount(), 0, ("" + item.getCount()).length());
				filewriter.newLine();
			}

			for (int i = 0; i < 14; i++) {
				Item item = player.getEquipment().getItemContainer().get(i);
				if (item == null) {
					continue;
				}
				filewriter.write("EQUIPMENT_ITEM[" + i + "] = ", 0, ("EQUIPMENT_ITEM[" + i + "] = ").length());
				filewriter.write("" + item.getId(), 0, ("" + item.getId()).length());
				filewriter.newLine();
				filewriter.write("EQUIPMENT_AMOUNT[" + i + "] = ", 0, ("EQUIPMENT_AMOUNT[" + i + "] = ").length());
				filewriter.write("" + item.getCount(), 0, ("" + item.getCount()).length());
				filewriter.newLine();
			}

			for (int i = 0; i < BankManager.SIZE; i++) {
				Item item = player.getBank().get(i);
				if (item == null) {
					continue;
				}
				filewriter.write("BANK_ITEM[" + i + "] = ", 0, ("BANK_ITEM[" + i + "] = ").length());
				filewriter.write("" + item.getId(), 0, ("" + item.getId()).length());
				filewriter.newLine();
				filewriter.write("BANK_AMOUNT[" + i + "] = ", 0, ("BANK_AMOUNT[" + i + "] = ").length());
				filewriter.write("" + item.getCount(), 0, ("" + item.getCount()).length());
				filewriter.newLine();
			}

			for (int i = 0; i < player.getFriends().length; i++) {
				if (player.getFriends()[i] == 0) {
					break;
				}
				filewriter.write("FRIEND[" + i + "] = ", 0, ("FRIEND[" + i + "] = ").length());
				filewriter.write("" + player.getFriends()[i], 0, ("" + player.getFriends()[i]).length());
				filewriter.newLine();
			}

			for (int i = 0; i < player.getIgnores().length; i++) {
				if (player.getIgnores()[i] == 0) {
					break;
				}
				filewriter.write("IGNORE[" + i + "] = ", 0, ("IGNORE[" + i + "] = ").length());
				filewriter.write("" + player.getIgnores()[i], 0, ("" + player.getIgnores()[i]).length());
				filewriter.newLine();
			}
		}
		filewriter.close();
	}

	/**
	 * Loads the player (and sets the loaded attributes).
	 * 
	 * @param player the player to load.
	 */
	public static void load(Player player) throws Exception {
		// Check if the password is correct
		BufferedReader file;
		try {
			file = new BufferedReader(new FileReader(directory + player.getDatabaseId() + ".dat"));
		} catch (FileNotFoundException e) {
			// Set the password as this user is newly registered.

			/* sets all staff members to their appropriate ranks */
			/*
			for (Rights rights : Rights.values()) {
				for (String member : rights.getMembers()) {
					if (player.getUsername().equalsIgnoreCase(member)) {
						player.setRights(rights);
						break;
					}
				}
			}
			*/
			if (Server.getSingleton() != null) {
				Server.getSingleton().appendLoginDeque(player);
			}
			return;
		}
		String line = file.readLine();
		String index;
		String data;
		while (line != null) {
			data = line.substring(line.indexOf("= ") + 1).trim();
			index = line.substring(line.indexOf("[") + 1).replaceAll("] = " + data, "");
			if (line.startsWith("QUEST_")) {
				Quest quest = QuestRepository.get(Integer.parseInt(line.split("_")[1].split("_")[0]));
				player.getQuestStorage().setState(quest, Short.parseShort(data));
			} else if (line.startsWith("MOUSE_BUTTONS")) {
				player.setAttribute("mouseButtons", Byte.parseByte(data));
			} else if (line.startsWith("CHAT_EFFECTS")) {
				player.setAttribute("chatEffects", Byte.parseByte(data));
			} else if (line.startsWith("SPLIT_PRIVATE")) {
				player.setAttribute("splitPrivate", Byte.parseByte(data));
			} else if (line.startsWith("ACCEPT_AID")) {
				player.setAttribute("acceptAid", Byte.parseByte(data));
			} else if (line.startsWith("RUN_ENERGY")) {
				player.setAttribute("runEnergy", Double.parseDouble(data));
			} else if (line.startsWith("AUTO_RETALIATE")) {
				player.setAutoRetaliate(true);
			} else if (line.startsWith("SPELLBOOK")) {
				if (Integer.parseInt(data) == 1) {
					player.setMagicBookType(MagicBookTypes.ANCIENT);
				}
			} else if (line.startsWith("CRYSTAL_BOW")) {
				for (int state = 0; state <= 10; state++) {
					if (line.startsWith("CRYSTAL_BOW_" + state + " =")) {
						player.setAttribute(("crystalBow" + state), Short.parseShort(data));
					}
				}
			} else if (line.startsWith("TELEBLOCK")) {
				player.setAttribute("teleBlocked", Long.parseLong(data));
			} else if (line.startsWith("CHARGE =")) {
				player.setAttribute("charge", Long.parseLong(data));
			} else if (line.startsWith("CHARGE_COOL_DOWN")) {
				player.setAttribute("chargeCoolDown", Long.parseLong(data));
			} else if (line.startsWith("HOME_TELE_TIMER")) {
				player.setAttribute("homeTeleTimer", (Long.parseLong(data)));
			} else if (line.startsWith("SPECIAL_AMOUNT")) {
				player.setSpecialAmount(Double.parseDouble(data));
			} else if (line.startsWith("COMBAT_STYLE")) {
				player.getCombatState().setCombatStyle(CombatStyle.forId(Integer.parseInt(data)));
			} else if (line.startsWith("POISONED")) {
				player.setPoisoned(true);
			} else if (line.startsWith("POISON_DAMAGE")) {
				player.setPoisonDamage(Integer.parseInt(data));
			} else if (line.startsWith("POISON_TIMER")) {
				player.setPoisonedTimer(Integer.parseInt(data));
			} else if (line.startsWith("POISON_HIT_TIMER")) {
				player.setPoisonHitTimer(Integer.parseInt(data));
			} else if (line.startsWith("POISON_IMMUNITY_TIMER")) {
				player.setPoisonImmunityTimer(Integer.parseInt(data));
			} else if (line.startsWith("LAST_LOGIN_IP")) {
				player.setLastIp(data);
			} else if (line.startsWith("LAST_LOGIN_DATE")) {
				player.setLastLoginDay(data);
			} else if (line.startsWith("BANK_PIN_WAIT_TIMER =")) {
				player.setAttribute("bankPinWaitTimer", Long.parseLong(data));
			} else if (line.startsWith("BANK_PIN =")) {
				player.getBankPin().setBankPin(Integer.parseInt(data));
			} else if (line.startsWith("PENDING_BANK_PIN =")) {
				player.getBankPin().setPendingBankPin(Integer.parseInt(data));
			} else if (line.startsWith("BANK_PIN_CHANGE_DAYS")) {
				player.getBankPin().setBankPINChangeDays(Integer.parseInt(data));
			} else if (line.startsWith("PIN_APPEND_DAY")) {
				player.getBankPin().setPinAppendDay(Integer.parseInt(data));
			} else if (line.startsWith("PIN_APPEND_YEAR")) {
				player.getBankPin().setPinAppendYear(Integer.parseInt(data));
			} else if (line.startsWith("PIN_CHANGING")) {
				player.getBankPin().setChangingPin(Boolean.parseBoolean(data));
			} else if (line.startsWith("PIN_DELETING")) {
				player.getBankPin().setDeletingPin(Boolean.parseBoolean(data));
			} else if (line.startsWith("APPEARANCE[")) {
				player.getAppearance()[Integer.parseInt(index)] = (Short.parseShort(data));
			} else if (line.startsWith("APPEARANCE_COLORS")) {
				player.getColors()[Integer.parseInt(index)] = (Byte.parseByte(data));
			} else if (line.startsWith("INVENTORY_ITEM")) {
				player.getInventory().getItemContainer().set(Byte.parseByte(index), (new Item((Short.parseShort(data)), 1)));
			} else if (line.startsWith("INVENTORY_AMOUNT")) {
				player.getInventory().get(Integer.parseInt(index)).setCount(Integer.parseInt(data));
			} else if (line.startsWith("EQUIPMENT_ITEM")) {
				player.getEquipment().getItemContainer().set(Integer.parseInt(index), (new Item((Integer.parseInt(data)), 1)));
			} else if (line.startsWith("EQUIPMENT_AMOUNT")) {
				player.getEquipment().get(Integer.parseInt(index)).setCount(Integer.parseInt(data));
			} else if (line.startsWith("BANK_ITEM")) {
				player.getBank().set(Integer.parseInt(index), (new Item((Integer.parseInt(data)), 1)));
			} else if (line.startsWith("BANK_AMOUNT")) {
				player.getBank().get(Integer.parseInt(index)).setCount(Integer.parseInt(data));
			} else if (line.startsWith("FRIEND")) {
				player.getFriends()[Integer.parseInt(index)] = Long.parseLong(data);
			} else if (line.startsWith("IGNORE")) {
				player.getIgnores()[Integer.parseInt(index)] = Long.parseLong(data);
			}
			line = file.readLine();
		}
		file.close();

		// Check if the password is correct
		/*
		if (!player.getTempPassword().equalsIgnoreCase(player.getPassword())) {
			player.setReturnCode(Constants.LOGIN_RESPONSE_INVALID_CREDENTIALS);
			player.sendLoginResponse();
			player.disconnect();
			return;
		}
		*/
		if (Server.getSingleton() != null) {
			Server.getSingleton().appendLoginDeque(player);
		}
	}
}
