package com.exorth.rs2;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.logging.Logger;

import com.exorth.rs2.content.gambling.slots.SlotData;
import com.exorth.rs2.content.minigame.impl.CastleWars;
import com.exorth.rs2.content.minigame.impl.FightPits;
import com.exorth.rs2.content.minigame.partyroom.PartyRoom;
import com.exorth.rs2.content.mta.AlchemistPlayground;
import com.exorth.rs2.content.mta.Arena;
import com.exorth.rs2.content.mta.EnchantmentChamber;
import com.exorth.rs2.content.mta.EventRoom;
import com.exorth.rs2.io.XStreamController;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.Equipment;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.container.Container;
import com.exorth.rs2.model.players.container.ContainerType;
import com.exorth.rs2.network.DedicatedReactor;
import com.exorth.rs2.network.packet.PacketManager;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.task.TaskScheduler;
import com.exorth.rs2.task.impl.Jackpot;
import com.exorth.rs2.task.impl.ShopNormalizationTask;
import com.exorth.rs2.util.ShutDownListener;
import com.exorth.rs2.util.clip.ObjectDefinition;
import com.exorth.rs2.util.clip.RegionClipping;

/**
 * The main core of RuneSource.
 * 
 * @author blakeman8192
 * @author Joshua Barry
 */
public class Server implements Runnable {
	/**
	 * Global access to the server.
	 */
	private static Server singleton;

	/**
	 * A simple logger.
	 */
	private static final Logger logger = Logger.getAnonymousLogger();

	/**
	 * A linked blocking deque.
	 */
	private final BlockingDeque<Player> loginDeque = new LinkedBlockingDeque<Player>();
	
	/**
	 * A linked blocking deque.
	 */
	private static final BlockingDeque<Player> disconnectedPlayers = new LinkedBlockingDeque<Player>();

	private Selector selector;
	private InetSocketAddress address;
	private ServerSocketChannel channel;
	private static final TaskScheduler scheduler = new TaskScheduler();
	
	/**
	 * Slots information.
	 */
	private static SlotData sd = new SlotData();
	
	/**
	 * Enable/disable the slots.
	 */
	private boolean slotsEnabled;
	
	/**
	 * Minigames
	 */
	private CastleWars castleWars;

	private FightPits fightPits;

	private EventRoom playground;

	private EventRoom chamber;
	
	/**
	 * Global access to the party room class.
	 */
	private static PartyRoom partyRoom;
	
	/**
	 * The main method.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		setSingleton(new Server());
		new Thread(getSingleton()).start();
		partyRoom = new PartyRoom();
	}

	public static BlockingDeque<Player> getDisconnectedPlayers() {
		return disconnectedPlayers;
	}

	public void appendLoginDeque(Player player) {
		loginDeque.add(player);
	}

	@Override
	public void run() {
		try {
			long time = System.currentTimeMillis();
			Thread.currentThread().setName("GameEngine");

			address = new InetSocketAddress("0.0.0.0", 43594);
			logger.info("Starting " + Constants.SERVER_NAME + " on " + address + "...");

			// Loading the shutdown hook
			Thread shutdownListener = new ShutDownListener();
			Runtime.getRuntime().addShutdownHook(shutdownListener);
			logger.info("Shutdown hook enabled.");

			PacketManager.loadPackets();

			// Load configuration.
			Equipment.sortEquipmentSlotDefinitions();

			// Load all XStream related files.
			XStreamController.loadAllFiles();

			// Load item spawns
			ItemManager.getInstance().spawnGroundItems();

			// Load clipping stuff
			ObjectDefinition.load();
			RegionClipping.load();

			castleWars = new CastleWars();
			fightPits = new FightPits();
			playground = new AlchemistPlayground();
			chamber = new EnchantmentChamber();
			Arena.init(playground.getEventCycle(), chamber.getEventCycle());

			scheduler.submit(new ShopNormalizationTask());

			// Init world attributes - Akzu
			World.getInstance().initAttributes();

			// Load slots data - Stephen
			SlotData.loadData();

			// Start up and get running!
			startup();
			logger.info("Exorth is now online! Load time: " + (System.currentTimeMillis() - time) + "ms");
			scheduler.submit(new Task() {
				@Override
				protected void execute() {
					// long time = System.currentTimeMillis();
					cycle();
					// System.err.println("Task Amt: " + scheduler.getTasks().size() + " Cycle: " + (System.currentTimeMillis() - time));
				}
			});
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	/**
	 * Starts the server up.
	 * 
	 * @throws IOException
	 */
	private void startup() throws IOException {
		// Initialize the networking objects.
		selector = Selector.open();
		channel = ServerSocketChannel.open();
		DedicatedReactor.setInstance(new DedicatedReactor(Selector.open()));
		DedicatedReactor.getInstance().start();

		// ...and configure them!
		channel.configureBlocking(false);
		channel.socket().bind(address);

		synchronized (DedicatedReactor.getInstance()) {
			DedicatedReactor.getInstance().getSelector().wakeup();
			channel.register(DedicatedReactor.getInstance().getSelector(), SelectionKey.OP_ACCEPT);
		}
	}

	/**
	 * Accepts any incoming connections.
	 * 
	 * @throws IOException
	 */
	public static void accept(SelectionKey key) throws IOException {
		ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();

		// Accept the socket channel.
		SocketChannel channel = serverChannel.accept();
		if (channel == null) {
			return;
		}

		// Make sure we can allow this connection.
		if (!HostGateway.enter(channel.socket().getInetAddress().getHostAddress())) {
			channel.close();
			return;
		}

		// Set up the new connection.
		channel.configureBlocking(false);
		SelectionKey newKey = channel.register(key.selector(), SelectionKey.OP_READ);
		Player player = new Player(newKey);
		newKey.attach(player);
	}

	private void cycle() {
		int loggedIn = 0;
		while (!loginDeque.isEmpty() && loggedIn++ < 50) {
			Player player = loginDeque.poll();
			try {
				player.finishLogin();
			} catch (Exception ex) {
				ex.printStackTrace();
				player.disconnect();
			}
		}
		try {
			selector.selectNow();
			for (SelectionKey selectionKey : selector.selectedKeys()) {
				if (selectionKey.isValid()) {
					if (selectionKey.isReadable()) {
						// Tell the client to handle the packet.
						PacketManager.handleIncomingData((Player) selectionKey.attachment());
					}
				}
			}
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			World.process();
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
		synchronized (disconnectedPlayers) {
			for (Iterator<Player> players = disconnectedPlayers.iterator(); players.hasNext();) {
				Player player = players.next();
				if (player.isInCombat()) {
					player.setAttribute("xLogTimer", System.currentTimeMillis());
					continue;
				} else if (player.getAttribute("xLogTimer") != null && (System.currentTimeMillis() - (Long) player.getAttribute("xLogTimer")) < 10000) {
					continue;
				}
				player.logout();
				players.remove();
			}
		}
	}

	/**
	 * Set a singleton of this server model, if one has not been already set.
	 * 
	 * @param singleton an instance of the server model.
	 */
	public static void setSingleton(Server singleton) {
		if (Server.singleton != null) {
			throw new IllegalStateException("Singleton already set!");
		}
		Server.singleton = singleton;
	}

	public static Server getSingleton() {
		return singleton;
	}

	public Selector getSelector() {
		return selector;
	}

	public static TaskScheduler getScheduler() {
		return scheduler;
	}

	public CastleWars getCastleWars() {
		return castleWars;
	}

	public FightPits getFightPits() {
		return fightPits;
	}
	
	public static SlotData getSlotData() {
		return sd;
	}
	
	public void setSlotsEnabled(boolean flag) {
		slotsEnabled = flag;
	}
	
	public boolean slotsEnabled() {
		return slotsEnabled;
	}
	
	public static PartyRoom getPartyRoom() {
		return partyRoom;
	}
}