package com.exorth.rs2.model.players;

import java.util.Iterator;

import com.exorth.rs2.Constants;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player.LoginStages;
import com.exorth.rs2.network.StreamBuffer;
import com.exorth.rs2.network.StreamBuffer.ByteOrder;
import com.exorth.rs2.network.StreamBuffer.OutBuffer;
import com.exorth.rs2.network.StreamBuffer.ValueType;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.NameUtility;

/**
 * Provides static utility methods for updating players.
 * 
 * @author blakeman8192
 */
public final class PlayerUpdating {

	/**
	 * Updates the player.
	 * 
	 * @param player the player
	 */
	public static void update(Player player) {
		// XXX: The buffer sizes may need to be tuned.
		StreamBuffer.OutBuffer out = StreamBuffer.newOutBuffer(8192);// 2048
		StreamBuffer.OutBuffer block = StreamBuffer.newOutBuffer(4096);// 1024

		// Initialize the update packet.
		out.writeVariableShortPacketHeader(player.getEncryptor(), 81);
		out.setAccessType(StreamBuffer.AccessType.BIT_ACCESS);

		// Update this player.
		PlayerUpdating.updateLocalPlayerMovement(player, out);
		if (player.getUpdateFlags().isUpdateRequired()) {
			PlayerUpdating.updateState(null, player, block, false, true);
		}

		// Update other local players.
		out.writeBits(8, player.getLocalPlayers().size());
		for (Iterator<Player> i = player.getLocalPlayers().iterator(); i.hasNext();) {
			Player other = i.next();
			if (other.getPosition().isWithinDistance(player.getPosition()) && other.getLoginStage() == LoginStages.LOGGED_IN && !other.needsPlacement()) {
				PlayerUpdating.updateOtherPlayerMovement(other, out);
				if (other.getUpdateFlags().isUpdateRequired()) {
					PlayerUpdating.updateState(player, other, block, false, false);
				}
			} else {
				out.writeBit(true);
				out.writeBits(2, 3);
				i.remove();
			}
		}

		// Update the local player list.+
		// System.err.println(World.getInstance().getRegionManager().getLocalPlayers(player));
		// for (int i = 0; i < World.getPlayers().length; i++) {
		for (Player other : World.getInstance().getRegionManager().getLocalPlayers(player)) {
			if (player.getLocalPlayers().size() >= 255) {
				// Player limit has been reached.
				break;
			}
			// Player other = World.getPlayers()[i];
			if (other == null || other == player || other.getLoginStage() != LoginStages.LOGGED_IN) {
				continue;
			}
			if (!player.getLocalPlayers().contains(other) && other.getPosition().isWithinDistance(player.getPosition())) {
				player.getLocalPlayers().add(other);
				PlayerUpdating.addPlayer(out, player, other);
				PlayerUpdating.updateState(player, other, block, true, false);
			}
		}

		// Append the attributes block to the main packet.
		if (block.getBuffer().position() > 0) {
			out.writeBits(11, 2047);
			out.setAccessType(StreamBuffer.AccessType.BYTE_ACCESS);
			out.writeBytes(block.getBuffer());
		} else {
			out.setAccessType(StreamBuffer.AccessType.BYTE_ACCESS);
		}

		// Finish the packet and send it.
		out.finishVariableShortPacketHeader();
		player.send(out.getBuffer());
	}

	/**
	 * Appends the state of a player's chat to a buffer.
	 * 
	 * @param player the player
	 * @param out the buffer
	 */
	public static void appendChat(Player player, StreamBuffer.OutBuffer out) {
		out.writeShort(((player.getChatColor() & 0xff) << 8) + ((player.getAttribute("chatEffectsMode") == null ? 0 : (Byte) player.getAttribute("chatEffectsMode")) & 0xff), StreamBuffer.ByteOrder.LITTLE);
		out.writeByte(player.getRights());
		out.writeByte(player.getChatText().length, StreamBuffer.ValueType.C);
		out.writeBytesReverse(player.getChatText());
	}

	/**
	 * Appends the state of a player's appearance to a buffer.
	 * 
	 * @param player the player
	 * @param out the buffer
	 */
	public static void appendAppearance(Player player, StreamBuffer.OutBuffer out) {
		StreamBuffer.OutBuffer block = StreamBuffer.newOutBuffer(128);
		block.writeByte(player.getGender()); // Gender
		block.writeByte(player.getPrayerIcon());
		block.writeByte(player.getSkullIcon());
		if (player.getHumanToNpc() < 0) {
			// Head
			if (player.getEquipment().getItemContainer().isSlotUsed(Constants.EQUIPMENT_SLOT_HEAD)) {
				block.writeShort(0x200 + player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_HEAD).getId());
			} else {
				block.writeByte(0);
			}

			// Cape
			if (player.getEquipment().getItemContainer().isSlotUsed(Constants.EQUIPMENT_SLOT_CAPE)) {
				block.writeShort(0x200 + player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_CAPE).getId());
			} else {
				block.writeByte(0);
			}

			// Amulet
			if (player.getEquipment().getItemContainer().isSlotUsed(Constants.EQUIPMENT_SLOT_AMULET)) {
				block.writeShort(0x200 + player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_AMULET).getId());
			} else {
				block.writeByte(0);
			}

			// Weapon
			if (player.getEquipment().getItemContainer().isSlotUsed(Constants.EQUIPMENT_SLOT_WEAPON)) {
				block.writeShort(0x200 + player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_WEAPON).getId());
			} else {
				block.writeByte(0);
			}

			// Chest
			if (player.getEquipment().getItemContainer().isSlotUsed(Constants.EQUIPMENT_SLOT_CHEST)) {
				block.writeShort(0x200 + player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_CHEST).getId());
			} else {
				block.writeShort(0x100 + player.getAppearance()[Constants.APPEARANCE_SLOT_CHEST]);
			}

			// Shield
			if (player.getEquipment().getItemContainer().isSlotUsed(Constants.EQUIPMENT_SLOT_SHIELD)) {
				block.writeShort(0x200 + player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_SHIELD).getId());
			} else {
				block.writeByte(0);
			}

			Item chest = player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_CHEST);
			if (chest != null) {
				if (!Equipment.isPlatebody(chest.getId())) {
					block.writeShort(0x100 + player.getAppearance()[Constants.APPEARANCE_SLOT_ARMS]);
				} else {
					block.writeShort(0x200 + chest.getId());
				}
			} else {
				block.writeShort(0x100 + player.getAppearance()[Constants.APPEARANCE_SLOT_ARMS]);
			}

			// Legs
			if (player.getEquipment().getItemContainer().isSlotUsed(Constants.EQUIPMENT_SLOT_LEGS)) {
				block.writeShort(0x200 + player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_LEGS).getId());
			} else {
				block.writeShort(0x100 + player.getAppearance()[Constants.APPEARANCE_SLOT_LEGS]);
			}

			// Head (with a hat already on)
			Item helm = player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_HEAD);
			if (helm != null) {
				if (!Equipment.isFullMask(helm.getId()) && !Equipment.isMedHelm(helm.getId())) {
					block.writeShort(0x100 + player.getAppearance()[Constants.APPEARANCE_SLOT_HEAD]);
				} else {
					block.writeByte(0);
				}
			} else {
				block.writeShort(0x100 + player.getAppearance()[Constants.APPEARANCE_SLOT_HEAD]);
			}

			// Hands
			if (player.getEquipment().getItemContainer().isSlotUsed(Constants.EQUIPMENT_SLOT_HANDS)) {
				block.writeShort(0x200 + player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_HANDS).getId());
			} else {
				block.writeShort(0x100 + player.getAppearance()[Constants.APPEARANCE_SLOT_HANDS]);
			}

			// Feet
			if (player.getEquipment().getItemContainer().isSlotUsed(Constants.EQUIPMENT_SLOT_FEET)) {
				block.writeShort(0x200 + player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_FEET).getId());
			} else {
				block.writeShort(0x100 + player.getAppearance()[Constants.APPEARANCE_SLOT_FEET]);
			}

			// Beard
			if (helm != null) {
				if (!Equipment.isFullMask(helm.getId()) && !Equipment.isFaceMask(helm.getId()) && player.getGender() == 0) {
					block.writeShort(0x100 + player.getAppearance()[Constants.APPEARANCE_SLOT_BEARD]);
				} else {
					block.writeByte(0);
				}
			} else {
				if (player.getGender() == 0) {
					block.writeShort(0x100 + player.getAppearance()[Constants.APPEARANCE_SLOT_BEARD]);
				} else {
					block.writeByte(0);
				}
			}

			// Ring
			if (player.getEquipment().getItemContainer().isSlotUsed(Constants.EQUIPMENT_SLOT_RING)) {
				block.writeShort(0x200 + player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_RING).getId());
			} else {
				block.writeByte(0);
			}
		} else {
			block.writeShort(-1);
			block.writeShort(player.getHumanToNpc());
		}

		// Player colors
		block.writeByte(player.getColors()[0]);
		block.writeByte(player.getColors()[1]);
		block.writeByte(player.getColors()[2]);
		block.writeByte(player.getColors()[3]);
		block.writeByte(player.getColors()[4]);
		// Movement animations
		Item item = player.getEquipment().getItemContainer().get(Constants.EQUIPMENT_SLOT_WEAPON);
		int runAnim = player.getAttribute("tempRunAnim") == null ? player.getEquipment().getRunAnim(item) : (Integer) player.getAttribute("tempRunAnim");
		int standAnim = player.getAttribute("tempStandAnim") == null ? player.getEquipment().getStandAnim(item) : (Integer) player.getAttribute("tempStandAnim");
		int standTurnAnim = player.getEquipment().getStandAnim(item) == 7047 ? 7046 : 0x337;
		int turn90cw = player.getEquipment().getStandAnim(item) == 7047 ? 7044 : 0x335;
		int turn90ccw = player.getEquipment().getStandAnim(item) == 7047 ? 7043 : 0x336;
		int turn180 = player.getEquipment().getStandAnim(item) == 7047 ? 7045 : 0x334;
		int walkAnim = player.getAttribute("tempWalkAnim") == null ? player.getEquipment().getWalkAnim(item) : (Integer) player.getAttribute("tempWalkAnim");
		block.writeShort(standAnim); // stand
		block.writeShort(standTurnAnim); // stand turn
		block.writeShort(walkAnim);
		block.writeShort(turn180); // turn 180
		block.writeShort(turn90cw); // turn 90 cw
		block.writeShort(turn90ccw); // turn 90 ccw
		block.writeShort(runAnim); // run
		block.writeLong(NameUtility.nameToLong(player.getUsername()));
		block.writeByte(player.getSkill().getCombatLevel()); // Combat level.
		block.writeShort(0); // skill lvl in burthope games room
		// Append the block length and the block to the packet.
		out.writeByte(block.getBuffer().position(), StreamBuffer.ValueType.C);
		out.writeBytes(block.getBuffer());
	}

	/**
	 * Adds a player to the local player list of another player.
	 * 
	 * @param out the packet to write to
	 * @param player the host player
	 * @param other the player being added
	 */
	public static void addPlayer(StreamBuffer.OutBuffer out, Player player, Player other) {
		out.writeBits(11, other.getIndex()); // Server slot
		out.writeBit(true); // Yes, an update is required
		out.writeBit(true); // Discard walking queue(?)

		// Write the relative position.
		Position delta = Misc.delta(player.getPosition(), other.getPosition());
		out.writeBits(5, delta.getY());
		out.writeBits(5, delta.getX());
	}

	/**
	 * Updates movement for this local player. The difference between this
	 * method and the other player method is that this will make use of sector
	 * 2,3 to place the player in a specific position while sector 2,3 is not
	 * present in updating of other players (it simply flags local list removal
	 * instead).
	 * 
	 * @param player
	 * @param out
	 */
	public static void updateLocalPlayerMovement(Player player, StreamBuffer.OutBuffer out) {
		boolean updateRequired = player.getUpdateFlags().isUpdateRequired();
		if (player.needsPlacement()) { // Do they need placement?
			out.writeBit(true); // Yes, there is an update.
			int posX = player.getPosition().getLocalX(player.getLastKnownRegion());
			int posY = player.getPosition().getLocalY(player.getLastKnownRegion());
			appendPlacement(out, posX, posY, player.getPosition().getZ(), player.isResetMovementQueue(), updateRequired);
		} else { // No placement update, check for movement.
			int pDir = (Integer) (player.getAttribute("primary_dir") == null ? -1 : player.getAttribute("primary_dir"));
			int sDir = (Integer) (player.getAttribute("secondary_dir") == null ? -1 : player.getAttribute("secondary_dir"));
			if (pDir != -1) { // If they moved.
				out.writeBit(true); // Yes, there is an update.
				if (sDir != -1) { // If they ran.
					appendRun(out, pDir, sDir, updateRequired);
				} else { // Movement but no running - they walked.
					appendWalk(out, pDir, updateRequired);
				}
			} else { // No movement.
				if (updateRequired) { // Does the state need to be updated?
					out.writeBit(true); // Yes, there is an update.
					appendStand(out);
				} else { // No update whatsoever.
					out.writeBit(false);
				}
			}
		}
	}

	/**
	 * Updates the movement of a player for another player (does not make use of
	 * sector 2,3).
	 * 
	 * @param player the player
	 * @param out the packet
	 */
	public static void updateOtherPlayerMovement(Player player, StreamBuffer.OutBuffer out) {
		boolean updateRequired = player.getUpdateFlags().isUpdateRequired();
		int pDir = (Integer) (player.getAttribute("primary_dir") == null ? -1 : player.getAttribute("primary_dir"));
		int sDir = (Integer) (player.getAttribute("secondary_dir") == null ? -1 : player.getAttribute("secondary_dir"));
		if (pDir != -1) { // If they moved.
			out.writeBit(true); // Yes, there is an update.
			if (sDir != -1) { // If they ran.
				appendRun(out, pDir, sDir, updateRequired);
			} else { // Movement but no running - they walked.
				appendWalk(out, pDir, updateRequired);
			}
		} else { // No movement.
			if (updateRequired) { // Does the state need to be updated?
				out.writeBit(true); // Yes, there is an update.
				appendStand(out);
			} else { // No update whatsoever.
				out.writeBit(false);
			}
		}
	}

	/**
	 * Updates the state of a player.
	 * 
	 * @param otherPlayer the player
	 * @param block the block
	 */
	public static void updateState(Player player, Player otherPlayer, OutBuffer block, boolean forceAppearance, boolean noChat) {
		int mask = 0x0;
		if (otherPlayer.getUpdateFlags().isForceMovementUpdateRequired()) {
			mask |= 0x400;
		}
		if (otherPlayer.getUpdateFlags().isGraphicsUpdateRequired()) {
			mask |= 0x100;
		}
		if (otherPlayer.getUpdateFlags().isAnimationUpdateRequired()) {
			mask |= 0x8;
		}
		if (otherPlayer.getUpdateFlags().isForceChatUpdate()) {
			mask |= 0x4;
		}
		if (otherPlayer.getUpdateFlags().isChatUpdateRequired() && !noChat) {
			mask |= 0x80;
		}
		if (otherPlayer.getUpdateFlags().isEntityFaceUpdate()) {
			mask |= 0x1;
		}
		if (otherPlayer.isAppearanceUpdateRequired() || forceAppearance) {
			mask |= 0x10;
		}
		if (otherPlayer.getUpdateFlags().isFaceToDirection()) {
			mask |= 0x2;
		}
		if (otherPlayer.getUpdateFlags().isHitUpdate()) {
			mask |= 0x20;
		}
		if (otherPlayer.getUpdateFlags().isHitUpdate2()) {
			mask |= 0x200;
		}
		if (mask >= 0x100) {
			mask |= 0x40;
			block.writeShort(mask, StreamBuffer.ByteOrder.LITTLE);
		} else {
			block.writeByte(mask);
		}
		if (otherPlayer.getUpdateFlags().isForceMovementUpdateRequired()) {
			Position myPos = (player == null ? otherPlayer.getLastKnownRegion() : player.getLastKnownRegion());
			Position pos = otherPlayer.getPosition();
			block.writeByte(pos.getLocalX(myPos), ValueType.S);
			block.writeByte(pos.getLocalY(myPos), ValueType.S);
			block.writeByte(pos.getLocalX(myPos) + otherPlayer.getUpdateFlags().getForceMovementEndX(), ValueType.S);
			block.writeByte(pos.getLocalY(myPos) + otherPlayer.getUpdateFlags().getForceMovementEndY(), ValueType.S);
			block.writeShort(otherPlayer.getUpdateFlags().getForceMovementSpeed1(), ValueType.A, ByteOrder.LITTLE);
			block.writeShort(otherPlayer.getUpdateFlags().getForceMovementSpeed2(), ValueType.A);
			block.writeByte(otherPlayer.getUpdateFlags().getForceMovementDirection(), ValueType.S);
		}
		if (otherPlayer.getUpdateFlags().isGraphicsUpdateRequired()) {
			block.writeShort(otherPlayer.getUpdateFlags().getGraphicsId(), StreamBuffer.ByteOrder.LITTLE);
			block.writeInt(otherPlayer.getUpdateFlags().getGraphicsDelay());
		}
		if (otherPlayer.getUpdateFlags().isAnimationUpdateRequired()) {
			block.writeShort(otherPlayer.getUpdateFlags().getAnimationId(), StreamBuffer.ByteOrder.LITTLE);
			block.writeByte(otherPlayer.getUpdateFlags().getAnimationDelay(), StreamBuffer.ValueType.C);
		}
		if (otherPlayer.getUpdateFlags().isForceChatUpdate()) {
			block.writeString(otherPlayer.getUpdateFlags().getForceChatMessage());
			block.writeByte(otherPlayer.getRights());
		}
		if (otherPlayer.getUpdateFlags().isChatUpdateRequired() && !noChat) {
			appendChat(otherPlayer, block);
		}
		if (otherPlayer.getUpdateFlags().isEntityFaceUpdate()) {
			block.writeShort(otherPlayer.getUpdateFlags().getEntityFaceIndex(), StreamBuffer.ByteOrder.LITTLE);
		}
		if (otherPlayer.isAppearanceUpdateRequired() || forceAppearance) {
			appendAppearance(otherPlayer, block);
		}
		if (otherPlayer.getUpdateFlags().isFaceToDirection()) {
			block.writeShort(otherPlayer.getUpdateFlags().getFace().getX() * 2 + 1, StreamBuffer.ValueType.A, StreamBuffer.ByteOrder.LITTLE);
			block.writeShort(otherPlayer.getUpdateFlags().getFace().getY() * 2 + 1, StreamBuffer.ByteOrder.LITTLE);
		}
		if (otherPlayer.getUpdateFlags().isHitUpdate()) {
			block.writeByte(otherPlayer.getUpdateFlags().getDamage());
			block.writeByte(otherPlayer.getUpdateFlags().getHitType(), StreamBuffer.ValueType.A);
			block.writeByte(otherPlayer.getSkill().getLevel()[Skill.HITPOINTS], StreamBuffer.ValueType.C);
			block.writeByte(otherPlayer.getSkill().getLevelForXP((int) otherPlayer.getSkill().getExp()[Skill.HITPOINTS]));
		}
		if (otherPlayer.getUpdateFlags().isHitUpdate2()) {
			block.writeByte(otherPlayer.getUpdateFlags().getDamage2());
			block.writeByte(otherPlayer.getUpdateFlags().getHitType2(), StreamBuffer.ValueType.S);
			block.writeByte(otherPlayer.getSkill().getLevel()[Skill.HITPOINTS]);
			block.writeByte(otherPlayer.getSkill().getLevelForXP((int) otherPlayer.getSkill().getExp()[Skill.HITPOINTS]), StreamBuffer.ValueType.C);
		}
	}

	/**
	 * Appends the stand version of the movement section of the update packet
	 * (sector 2,0). Appending this (instead of just a zero bit) automatically
	 * assumes that there is a required attribute update afterwards.
	 * 
	 * @param out the buffer to append to
	 */
	public static void appendStand(StreamBuffer.OutBuffer out) {
		out.writeBits(2, 0); // 0 - no movement.
	}

	/**
	 * Appends the walk version of the movement section of the update packet
	 * (sector 2,1).
	 * 
	 * @param out the buffer to append to
	 * @param direction the walking direction
	 * @param attributesUpdate whether or not a player attributes update is required
	 */
	public static void appendWalk(StreamBuffer.OutBuffer out, int direction, boolean attributesUpdate) {
		out.writeBits(2, 1); // 1 - walking.
		// Append the actual sector.
		out.writeBits(3, direction);
		out.writeBit(attributesUpdate);
	}

	/**
	 * Appends the walk version of the movement section of the update packet
	 * (sector 2,2).
	 * 
	 * @param out the buffer to append to
	 * @param direction the walking direction
	 * @param direction2 the running direction
	 * @param attributesUpdate whether or not a player attributes update is
	 *			required
	 */
	public static void appendRun(StreamBuffer.OutBuffer out, int direction, int direction2, boolean attributesUpdate) {
		out.writeBits(2, 2); // 2 - running.
		// Append the actual sector.
		out.writeBits(3, direction);
		out.writeBits(3, direction2);
		out.writeBit(attributesUpdate);
	}

	/**
	 * Appends the player placement version of the movement section of the
	 * update packet (sector 2,3). Note that by others this was previously
	 * called the "teleport update".
	 * 
	 * @param out the buffer to append to
	 * @param localX the local X coordinate
	 * @param localY the local Y coordinate
	 * @param z the Z coordinate
	 * @param discardMovementQueue whether or not the client should discard the
	 *			movement queue
	 * @param attributesUpdate whether or not a plater attributes update is
	 *			required
	 */
	public static void appendPlacement(StreamBuffer.OutBuffer out, int localX, int localY, int z, boolean discardMovementQueue, boolean attributesUpdate) {
		out.writeBits(2, 3); // 3 - placement.
		// Append the actual sector.
		out.writeBits(2, z);
		out.writeBit(discardMovementQueue);
		out.writeBit(attributesUpdate);
		out.writeBits(7, localY);
		out.writeBits(7, localX);
	}

	/**
	 * Append facing on login and whenever new player is added
	 */
	public static void appendFace(StreamBuffer.OutBuffer out) {
		out.writeBits(2, 4); // 4 - face direction.
	}
}
