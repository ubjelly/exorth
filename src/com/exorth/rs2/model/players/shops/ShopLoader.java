package com.exorth.rs2.model.players.shops;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.SQLConnector;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * This class will serve solely to load the data from the db in order to be used in the shop container.
 * @author Stephen
 * @author Brendan Dodd
 */
public class ShopLoader {
	
	/**
	 * The items a player is selling.
	 */
	private ArrayList<Item> items = new ArrayList<Item>();
	
	/**
	 * A raw string of item ids loaded from the db.
	 */
	private String rawItems;
	
	/**
	 * An array of item ids that have been parsed into individual items.
	 */
	private String[] parsedItems;
	
	/**
	 * A raw string of item amounts loaded from the db.
	 */
	private String rawAmounts;
	
	/**
	 * An array of item amounts that have been parsed into individual items.
	 */
	private String[] parsedAmounts;
	
	/**
	 * The cost of the items.
	 */
	private ArrayList<Integer> costs = new ArrayList<Integer>();
	
	/**
	 * A raw string of item costs loaded from the db.
	 */
	private String rawCosts;
	
	/**
	 * An array of item costs that have been parsed into individual items.
	 */
	private String[] parsedCosts;
	
	public static boolean playerHasShop(Player player) {
		try {
			if(player == null)
				return false;
			
			int dbId = player.getDatabaseId();
			
			SQLConnector.serverConnection();
			String query = "SELECT * FROM player_shops WHERE ownerid='"+dbId+"'";
			ResultSet rs = SQLConnector.serverConn.createStatement().executeQuery(query);
			if(rs.next())
				return true;
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
		
	/**
	 * Loads a player shop for the specified index.
	 * @param index The db index of the shop owner.
	 */
	@SuppressWarnings("unchecked")
	public static ShopData fetchShop(Player owner) {
		int dbId = owner.getDatabaseId();
		Gson gson = new Gson();
		ShopData shopData = new ShopData();
		try {
			SQLConnector.serverConnection();
			String query = "SELECT * FROM player_shops WHERE ownerid='"+dbId+"'";
			ResultSet rs = SQLConnector.serverConn.createStatement().executeQuery(query);
			if(rs.next()) {
				shopData.setItems((ArrayList<ShopItem>) gson.fromJson(rs.getString("data"), new TypeToken<List<ShopItem>>(){}.getType()));
			}
			rs.close();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return shopData;
	}
	
	public static boolean saveShop(Player owner, ShopData shop) {
		int ownerId = owner.getDatabaseId();
		Gson gson = new Gson();
		try {
			String query = "UPDATE player_shops SET data = ? WHERE ownerid='"+ownerId+"'";
			PreparedStatement statement = SQLConnector.serverConn.prepareStatement(query);
			
			statement.setString(1, gson.toJson(shop.getItems()));
			
			return statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
	/**
	 * Parses the raw format of the strings and puts them into arrays for easier access.
	 */
	private void parseData() {
		if (rawItems != "" && rawCosts != "") {
			parsedItems = rawItems.split(", ");
			parsedAmounts = rawAmounts.split(", ");
			parsedCosts = rawCosts.split(", ");
		}
		
		for (int i = 0; i < parsedItems.length; i++) {
			items.add(new Item(Integer.valueOf(parsedItems[i]), Integer.valueOf(parsedAmounts[i])));
			costs.add(Integer.valueOf(parsedCosts[i]));
		}
	}
	
	/**
	 * Sets the items array list.
	 * @param items The items to set it to.
	 */
	public void setItems(ArrayList<Item> items) {
		this.items = items;
	} 
	
	/**
	 * Gets the items array list.
	 * @return The items array list.
	 */
	//public static ArrayList<Item> getItems() {
	//	return items;
	//}
	
	/**
	 * Sets the costs array list.
	 * @param costs The costs to set it to.
	 */
	public void setCost(ArrayList<Integer> costs) {
		this.costs = costs;
	}
	
	/**
	 * Gets the costs array list.
	 * @return The cost array list.
	 */
	public ArrayList<Integer> getCosts() {
		return costs;
	}
}
