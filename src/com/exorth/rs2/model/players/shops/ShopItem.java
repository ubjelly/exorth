package com.exorth.rs2.model.players.shops;

import java.util.List;

/**
 * @author Brendan Dodd
 */
public class ShopItem {

	private List<Integer> itemId;
	private List<Integer> stackSize;
	private List<Integer> itemCost;
	
	public List<Integer> getItemId() {
		return itemId;
	}
	
	public List<Integer> getStackSize() {
		return stackSize;
	}
	
	public List<Integer> getItemCost() {
		return itemCost;
	}
	
	public void setItemId(List<Integer> itemId) {
		this.itemId = itemId;
	}
	
	public void setStackSize(List<Integer> stackSize) {
		this.stackSize = stackSize;
	}
	
	public void setItemCost(List<Integer> itemCost) {
		this.itemCost = itemCost;
	}
	
	
}
