package com.exorth.rs2.model.players.shops;

import java.util.List;

/**
 * @author Brendan Dodd
 */
public class ShopData {
	
	private int ownerId;
	private List<ShopItem> items;
	
	/** 
	 * Getters & Setters 
	 */
	
	public int getOwnerId() {
		return ownerId;
	}
	
	public List<ShopItem> getItems() {
		return items;
	}
	
	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}
	
	public void setItems(List<ShopItem> items) {
		this.items = items;
	}
	
}
