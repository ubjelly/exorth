package com.exorth.rs2.model.players.shops;

import java.util.HashMap;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.players.container.Container;
import com.exorth.rs2.model.players.container.ContainerType;

/**
 * Created its own package because it's a fairly large feature so I don't believe it belongs with the normal shops.
 * 
 * Basically, each player will have the ability to own there own shop. To make it not op, every item a player sells through
 * their shop instead of selling it by trading another player could be taxed (just an idea). Anyways, there will be one
 * container in the class which will be utilized for each player. The player's items in the shop will be stored in the server
 * db and will contain the player's index to determine the owner of the shop. This may seem like a complex system to make,
 * but I actually think it will be rather simple. LETS DO THIS BOYS.
 * 
 * @author Stephen
 */
public class PlayerShop {

	/**
	 * The owner of the shop being viewed.
	 */
	private Player shopOwner;
	
	/**
	 * The player viewing the shop.
	 */
	private Player viewer;
	
	/**
	 * How many items the player will be allowed to have in their shop.
	 * Just an idea, we could have it so they could buy more slots? Like upgrading their shop.
	 * Max shop size can only be 40.
	 */
	private int shopSize;
	
	/**
	 * The container for the shop.
	 */
	private Container shop;
	
	/**
	 * Map for all the items a player is selling.
	 * <Item> Obviously the item.
	 * <Integer> Cost of the item.
	 */
	private HashMap<Item, Integer> shopContents;
	
	/**
	 * Raw shop data from SQL
	 */
	private ShopData shopData;
	
	/**
	 * Construct a shop.
	 * @param shopSize The size of the shop.
	 */
	public PlayerShop(Player viewer, Player shopOwner) {
		this.shopOwner = this.shopOwner;
		this.viewer = viewer;
		shop = new Container(ContainerType.STANDARD, 40);
	}
	
	private void viewShop(Player player) {
		//loader.loadShop(player.getDatabaseId());
		fillMap();
		player.getActionSender().sendInterface(3824);
		fillShop(player);
	}
	
	/**
	 * Fills the hashmap.
	 */
	private void fillMap() {
		//for (int i = 0; i < ShopLoader.getItems().size(); i++) {
		//	shopContents.put(loader.getItems().get(i), loader.getCosts().get(i));
		//}
	}
	
	/**
	 * Fills the shop with the items the player has stored in it.
	 */
	public void fillShop(Player player) {
		/**
		 * TODO:
		 * Going to have to have a method that converts a string of item ids and puts
		 * them into an arraylist of items in order to fill the container.
		 */
		//for (Item item : loader.getItems()) {
		//	shop.add(item);
		//}
		player.getActionSender().sendUpdateItems(3900, shop.toArray());
	}
	
	/**
	 * Rewards the viewer with the item, the player with the money, and removes the item from
	 * the container.
	 * @param player The player purchasing the item.
	 * @param slot The slot of the item being purchased.
	 * @param itemId The id of the item.
	 * @param amount The amount of the item.
	 */
	public void purchaseItem(Player player, int slot, int itemId, int amount) {
		/**
		 * TODO:
		 * Maybe instead of an arraylist listed in the previous method, we need to use a hashmap
		 * in order to save the cost of the item.
		 */
	}
}
