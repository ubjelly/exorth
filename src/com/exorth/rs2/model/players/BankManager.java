package com.exorth.rs2.model.players;

import com.exorth.rs2.Constants;
import com.exorth.rs2.content.BankPin.ActionsAfterVerification;
import com.exorth.rs2.content.BankPin.AtInterfaceStatus;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.util.Language;

/**
 * @author AkZu
 * @author Joshua Barry
 * 
 */
public class BankManager {

	public static final int SIZE = 360;

	public static void openBank(Player player) {
		if (player.getBankPin().hasBankPin()) {
			if (!player.getBankPin().isBankPinVerified()) {
				player.getBankPin().setActionsAfterVerification(ActionsAfterVerification.BANK);
				player.getBankPin().startPinInterface(AtInterfaceStatus.VERIFYING);
				return;
			}
		}
		player.getActionSender().sendConfig(304, 0);
		player.getActionSender().sendConfig(115, 0);
		Item[] inventory = player.getInventory().getItemContainer().toArray();
		Item[] bank = player.getBank().toArray();
		player.getActionSender().sendUpdateItems(5064, inventory);
		player.getActionSender().sendItfInvOverlay(5292, 5063);
		player.getActionSender().sendUpdateItems(5382, bank);
		player.setAttribute("isBanking", true);
	}

	/**
	 * TODO: Under reword 20/12/2012
	 */
	public static void bankItem(Player player, int slot, int bankItem, int bankAmount) {
		if (player.getAttribute("isBanking") == null || !(Boolean) player.getAttribute("isBanking")) {
			return;
		}

		// fetch the real item player trying to deposit
		Item inventoryItem = player.getInventory().get(slot);

		// check if it exists
		if (inventoryItem == null || inventoryItem.getId() != bankItem || bankAmount < 1) {
			return;
		}

		// corrects the amount
		if (bankAmount > player.getInventory().getItemContainer().getCount(inventoryItem.getId())) {
			bankAmount = player.getInventory().getItemContainer().getCount(inventoryItem.getId());
		}

		boolean isNote = inventoryItem.getDefinition().isNote();

		// un-note it
		int transferId = isNote ? inventoryItem.getDefinition().getParentId() : inventoryItem.getId();

		int freeSlot = player.getBank().freeSlot();

		// check bank space
		if (freeSlot == -1 && !player.getBank().contains(transferId)) {
			player.getActionSender().sendMessage(Language.BANK_ACCOUNT_FULL);
			return;
		}

		int bankCount = player.getBank().getCount(transferId);

		// check if you have space
		if (bankCount != 0) {
			if (Constants.MAX_ITEM_COUNT - bankCount < bankAmount) {
				bankAmount = Constants.MAX_ITEM_COUNT - bankCount;
				if (bankAmount == 0) {
					player.getActionSender().sendMessage(Language.BANK_ACCOUNT_FULL);
					return;
				}
			}
		}

		// remove inventory item
		// TODO: Need looping for non stackable item removing?
		if (!inventoryItem.getDefinition().isStackable()) {
			for (int i = 0; i < bankAmount; i++) {
				if (bankAmount == 1) {
					player.getInventory().removeItemSlot(slot);
				} else {
					player.getInventory().removeItem(new Item(bankItem, 1));
				}
			}
		} else {
			player.getInventory().removeItem(new Item(bankItem, bankAmount));
		}

		if (bankCount == 0) {
			player.getBank().add(new Item(transferId, bankAmount));
		} else {
			player.getBank().set(player.getBank().getSlotById(transferId), new Item(transferId, bankCount + bankAmount));
		}
		Item[] bankItems = player.getBank().toArray();
		player.getInventory().refresh(5064);
		player.getActionSender().sendUpdateItems(5382, bankItems);
	}

	/**
	 * TODO: Under reword 20/12/2012 Stackable bug... have 2.1b cash and
	 * withdraw 10m then 2b you only get 2b something to do with hasRoomFor in
	 * Container class...
	 */
	public static void withdrawItem(Player player, int slot, int bankItemToWithdraw, int bankAmount) {
		if (!(Boolean) player.getAttribute("isBanking")) {
			return;
		}

		Item bankItem = player.getBank().get(slot);

		// Check if bank has the item player trying to withdraw
		if (bankItem == null || bankItem.getId() != bankItemToWithdraw || bankAmount < 1) {
			return;
		}

		// corrects the amount
		if (bankAmount > bankItem.getCount()) {
			bankAmount = bankItem.getCount();
		}

		boolean isStackable = bankItem.getDefinition().isStackable();

		int transferId = player.getAttribute("BANK_MODE_NOTE") != null ? bankItem.getDefinition().getNoteId() : bankItem.getId();

		// Check if player has enough space
		// checks if the withdrawn item is going to be noted and player already has it in inventory already + stackable support
		if (player.getInventory().getItemContainer().freeSlot() == -1 && (!(player.getAttribute("BANK_MODE_NOTE") != null && player.getInventory().getItemContainer().contains(bankItem.getDefinition().getNoteId()))) && (!(isStackable && player.getInventory().getItemContainer().contains(bankItemToWithdraw)))) {
			player.getActionSender().sendMessage(Language.NO_SPACE);
			return;
		}

		// check if it can be withdraw as note
		if (player.getAttribute("BANK_MODE_NOTE") != null && bankItem.getDefinition().getNoteId() == 0) {
			player.getActionSender().sendMessage(Language.CANT_WITHDRAW_AS_NOTE);
			return;
		}

		// add so many items as he got space
		if (player.getAttribute("BANK_MODE_NOTE") == null && !isStackable && bankAmount > player.getInventory().getItemContainer().freeSlots()) {
			bankAmount = player.getInventory().getItemContainer().freeSlots();
		}

		// check if you have space
		if (player.getInventory().getItemContainer().getById(transferId) != null) {
			if (Constants.MAX_ITEM_COUNT - player.getInventory().getItemContainer().getById(transferId).getCount() < bankAmount) {
				bankAmount = Constants.MAX_ITEM_COUNT - player.getInventory().getItemContainer().getById(transferId).getCount();
				if (bankAmount == 0) {
					player.getActionSender().sendMessage(Language.NO_SPACE);
				}
			}
		}
		player.getInventory().addItem(new Item(transferId, bankAmount));
		player.getBank().remove(new Item(bankItem.getId(), bankAmount), slot);
		Item[] bankItems = player.getBank().toArray();
		player.getInventory().refresh(5064);
		player.getActionSender().sendUpdateItems(5382, bankItems);
	}

	public static void handleBankOptions(Player player, int fromSlot, int toSlot) {
		if (!(Boolean) player.getAttribute("isBanking")) {
			return;
		}

		if (toSlot < player.getBank().size()) {
			if (player.getAttribute("BANK_MODE_INSERT") != null) {
				player.getBank().insert(fromSlot, toSlot);
			} else {
				player.getBank().swap(fromSlot, toSlot);
			}
		}

		Item[] bankItems = player.getBank().toArray();
		player.getActionSender().sendUpdateItems(5382, bankItems);
	}
}
