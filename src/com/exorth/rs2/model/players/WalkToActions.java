package com.exorth.rs2.model.players;

import java.util.logging.Logger;

import com.exorth.rs2.Server;
import com.exorth.rs2.content.Climbable;
import com.exorth.rs2.content.Crates;
import com.exorth.rs2.content.Door;
import com.exorth.rs2.content.SheepShearer;
import com.exorth.rs2.content.WildernessDitch;
import com.exorth.rs2.content.dialogue.ChatDialogue;
import com.exorth.rs2.content.dialogue.Dialogue;
import com.exorth.rs2.content.dialogue.DialogueSession;
import com.exorth.rs2.content.dialogue.Option;
import com.exorth.rs2.content.dialogue.StatementDialogue;
import com.exorth.rs2.content.dialogue.StatementType;
import com.exorth.rs2.content.dialogue.impl.*;
import com.exorth.rs2.content.duel.DuelArena;
import com.exorth.rs2.content.minigame.Minigame;
import com.exorth.rs2.content.minigame.barrows.Barrows;
import com.exorth.rs2.content.minigame.barrows.Brother;
import com.exorth.rs2.content.minigame.impl.CastleWars;
import com.exorth.rs2.content.minigame.impl.CastleWars.Team;
import com.exorth.rs2.content.minigame.impl.FightCaves;
import com.exorth.rs2.content.minigame.partyroom.DropParty;
import com.exorth.rs2.content.mta.AlchemistPlayground;
import com.exorth.rs2.content.mta.Arena;
import com.exorth.rs2.content.mta.CreatureGraveyard;
import com.exorth.rs2.content.mta.EnchantmentChamber;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.object.CustomObject;
import com.exorth.rs2.model.players.shops.PlayerShop;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.task.impl.CollectionDelay;
import com.exorth.rs2.task.impl.FancyBoots;
import com.exorth.rs2.task.impl.FighterBoots;
import com.exorth.rs2.task.impl.wq.ClimbTree;
import com.exorth.rs2.task.impl.wq.EnterBaxtorianFalls;
import com.exorth.rs2.task.impl.wq.GlarialsTomb;
import com.exorth.rs2.task.impl.wq.InsertPebble;
import com.exorth.rs2.task.impl.wq.LassoBoulder;
import com.exorth.rs2.task.impl.wq.RaftMessage;
import com.exorth.rs2.task.impl.wq.SearchCrate;
import com.exorth.rs2.task.impl.wq.TreeFall;
import com.exorth.rs2.task.impl.wq.WaterfallBarrel;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.clip.ObjectDefinition;
import com.exorth.rs2.util.clip.RSObject;
import com.exorth.rs2.util.clip.RegionClipping;
import com.exorth.rs2.content.quest.Quest;
import com.exorth.rs2.content.quest.QuestRepository;
import com.exorth.rs2.content.shop.ShopManager;
import com.exorth.rs2.content.skills.agility.Obstacle;
import com.exorth.rs2.content.skills.agility.TackleObstacle;
import com.exorth.rs2.content.skills.cooking.Cookable;
import com.exorth.rs2.content.skills.cooking.CookingAction;
import com.exorth.rs2.content.skills.crafting.Glass;
import com.exorth.rs2.content.skills.crafting.GlassMelting;
import com.exorth.rs2.content.skills.crafting.Spinnable;
import com.exorth.rs2.content.skills.crafting.WheelSpinning;
import com.exorth.rs2.content.skills.fishing.Fishable;
import com.exorth.rs2.content.skills.fishing.Fishing;
import com.exorth.rs2.content.skills.fishing.Tools;
import com.exorth.rs2.content.skills.mining.Mining;
import com.exorth.rs2.content.skills.runecrafting.Rune;
import com.exorth.rs2.content.skills.runecrafting.Runecraft;
import com.exorth.rs2.content.skills.smithing.Bar;
import com.exorth.rs2.content.skills.smithing.SmithingFrame;
import com.exorth.rs2.content.skills.thieving.Pickpocketing;
import com.exorth.rs2.content.skills.thieving.Shoplift;
import com.exorth.rs2.content.skills.woodcutting.Felling;

/**
 * Actions that happen after the player walks within distance.
 * 
 * @author AkZu
 * @author Joshua Barry
 * TODO: Clean up (Mark)
 */
public class WalkToActions {

	private static final Logger logger = Logger.getAnonymousLogger();
	private static Actions actions = Actions.OBJECT_FIRST_CLICK;

	public static enum Actions {
		OBJECT_FIRST_CLICK, OBJECT_SECOND_CLICK, OBJECT_THIRD_CLICK, ITEM_ON_OBJECT,
		NPC_FIRST_CLICK, NPC_SECOND_CLICK, NPC_THIRD_CLICK
	}

	public static void doActions(Player player, Object... objects) {
		switch (actions) {
			case OBJECT_FIRST_CLICK:
				logger.info("First object click...");
				doObjectFirstClick(player);
				break;
			case OBJECT_SECOND_CLICK:
				logger.info("Second object click...");
				doObjectSecondClick(player);
				break;
			case OBJECT_THIRD_CLICK:
				logger.info("Third object click...");
				doObjectThirdClick(player);
				break;
			case NPC_FIRST_CLICK:
				logger.info("First NPC click...");
				doNpcFirstClick(player);
				break;
			case NPC_SECOND_CLICK:
				logger.info("Second NPC click...");
				doNpcSecondClick(player);
				break;
			case NPC_THIRD_CLICK:
				logger.info("Third NPC click...");
				doNpcThirdClick(player);
				break;
			case ITEM_ON_OBJECT:
				logger.info("Item on object...");
				doItemOnObject(player, (Item) objects[0]);
				break;
		}
	}

	public static void doObjectFirstClick(final Player player) {
		final int id = player.getClickId();
		final int x = player.getClickX();
		final int y = player.getClickY();
		final int z = player.getPosition().getZ();
		final ObjectDefinition def = ObjectDefinition.forId(id);
		final int orig_distance = Misc.getDistance(player.getPosition(), new Position(x, y));
		final RSObject object = RegionClipping.getRSObject(player, x, y, z);
		final Position object_position = new Position(x, y, z);

		player.getUpdateFlags().sendFaceToDirection(new Position(x, y, z));

		player.setWalkToAction(new Task(1, true) {
			@Override
			protected void execute() {
				switch (id) {
					case 3406: //Lever to start drop party
						DropParty dp = new DropParty(player);
						dp.dropBalloons(player);
					break;
					/* Popping balloons :P */
					case 115:
					case 116:
					case 117:
					case 118:
					case 119:
					case 120:
						Server.getPartyRoom().popBalloon(player, object_position);
						break;
					case 1733: { //Staircase...
						String action = def.actions[0].toLowerCase();
						Position stairPosition = null;
						Position destination = null;
						if (object_position.equals(new Position(3058, 3376))) { //...in East Falador to go into Dwarven Mine
							stairPosition = new Position(3061, 3377, 0);
							destination = new Position(3058, 9777, 0);
						} else {
							player.getActionSender().sendMessage("This stair has no destination. Please report to an Administrator.");
						}
						if (action.contains("down") && stairPosition != null && destination != null) {
							Climbable.climb(player, stairPosition, destination, 0);
						}
						this.stop();
						break;
					}
					case 1734: { //Staircase...
						String action = def.actions[0].toLowerCase();
						Position stairPosition = null;
						Position destination = null;
						if (object_position.equals(new Position(3059, 9776, 0))) { //...in Dwarven Mine to go to East Falador
							stairPosition = new Position(3058, 9777, 0);
							destination = new Position(3061, 3377, 0);
						}
						if (action.contains("up") && stairPosition != null && destination != null) {
							Climbable.climb(player, stairPosition, destination, 0);
						}
						this.stop();
						break;
					}
					case 1755: { //Ladder...
						String action = def.actions[0].toLowerCase();
						Position ladderPosition = null;
						Position destination = null;
						if (object_position.equals(new Position(3005, 10363, 0))) { //...in Wilderness Agility From Rope Swing + Log Balance Failure Dungeon
							ladderPosition = new Position(3005, 10362, 0);
							destination = new Position(3005, 3962, 0);
						} else if (object_position.equals(new Position(3237, 9858, 0))) { //...in Varrock Sewers From East Varrock Manhole
							ladderPosition = new Position(3237, 9859, 0);
							destination = new Position(3237, 3459, 0);
						} else if (object_position.equals(new Position(3008, 9550, 0))) { //...in Asgarnia Ice Dungeon from North Falador Dusty Key Trapdoor
							ladderPosition = new Position(3007, 9550, 0);
							destination = new Position(3001, 3404, 0);
						} else {
							player.getActionSender().sendMessage("This ladder has no destination. Please report to an Administrator.");
							this.stop();
						}
						if (action.contains("up") && ladderPosition != null && destination != null) {
							Climbable.climb(player, ladderPosition, destination);
						}
						this.stop();
						break;
					}
					case 14758: { //Ladder...
						String action = def.actions[0].toLowerCase();
						Position ladderPosition = null;
						Position destination = null;
						if (object_position.equals(new Position(3005, 3963, 0))) { //...in Wilderness Agility To Rope Swing + Log Balance Failure Dungeon
							ladderPosition = new Position(3005, 3962, 0);
							destination = new Position(3005, 10362, 0);
						} else {
							player.getActionSender().sendMessage("This ladder has no destination. Please report to an Administrator.");
						}
						if (action.contains("down") && ladderPosition != null && destination != null) {
							Climbable.climb(player, ladderPosition, destination);
						}
						this.stop();
						break;
					}
					//Begin Wilderness Agility
					case 2288:
					case 2283:
					case 2311:
					case 2297:
					case 2328:
						Obstacle obstacle = Obstacle.forId(id);
						if (obstacle.getId() == 2288) {
							TackleObstacle.handlePipe(player, obstacle);
						}
						if (obstacle.getId() == 2283) {
							TackleObstacle.handleRopeSwing(player, obstacle);
						}
						if (obstacle.getId() == 2311) {
							TackleObstacle.handleSteppingStones(player, obstacle);
						}
						if (obstacle.getId() == 2297) {
							TackleObstacle.handleLogBalance(player, obstacle);
						}
						if (obstacle.getId() == 2328) {
							TackleObstacle.handleRockClimb(player, obstacle);
						}
						this.stop();
						break;
					//End Wilderness Agility
					case 23271: //Wilderness Ditch
						if (WildernessDitch.handleDitch(player, x)) {
							actions = null;
							this.stop();
							return;
						}
						break;
				}
				if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), getObjectSize(player.getClickId()))) {
					return;
				}
				final String objName = (def.getName() == null ? Integer.toString(id) : def.getName().toLowerCase());

				//This Task is designed so the player must walk to the object before the action is performed.
				World.submit(new Task(orig_distance <= getObjectSize(player.getClickId()) ? 1 : 2, true) {
					@SuppressWarnings("rawtypes")
					@Override
					protected void execute() {
							switch (id) {
								//Begin Teleports
								case 881: //Manhole
									if (object_position.equals(new Position(3237, 3458, 0))) {
										player.teleport(new Position(3237, 9859, 0));
									}
									break;
								case 6434: //Dusty Key Trapdoor in North Falador to Asgarnia Ice Dungeon
									if (player.getInventory().getItemContainer().contains(1590)
										|| player.getInventory().getItemContainer().contains(3741)
										|| player.getInventory().getItemContainer().contains(1839)) {
										player.teleport(new Position(3007, 9550, 0));
									} else {
										player.getActionSender().sendMessage("You need a dusty key to get in here.");
									}
									break;
								case 10596: //Icy Cavern - Entrance
									if (player.getInventory().getItemContainer().contains(3741)
										|| player.getInventory().getItemContainer().contains(1839)) {
										player.teleport(new Position(3056, 9555, 0));
									} else {
										player.getActionSender().sendMessage("You need a frozen key to enter.");
									}
									break;
								case 10595: //Icy Cavern - Exit
									player.teleport(new Position(3056, 9562, 0));
									break;
								case 5947: //Black Hole to Gargoyle Boss Area
									if (player.getInventory().getItemContainer().contains(1839)) {
										player.teleport(new Position(1889, 5366, 2));
										player.getActionSender().sendMessage("The metal key activates the portal.");
									} else {
										player.getActionSender().sendMessage("It's a black hole.");
									}
									break;
								case 12356: //Portals in Gargoyle Boss Area
									if (object_position.equals(new Position(1889, 5355, 2)) //West Portal
										|| object_position.equals(new Position(1900, 5345, 2)) //South Portal
										|| object_position.equals(new Position(1899, 5366, 2)) //North Portal
										|| object_position.equals(new Position(1910, 5356, 2))) { //East Portal
										player.teleport(new Position(3027, 9557, 0));
									}
									break;
								case 7319: //Portal...
									if (object_position.equals(new Position(2967, 3409, 0))) { //...north of Falador to Rune Essence Mine
										player.teleport(new Position(2911, 4832, 0));
									}
									break;
								case 2492: //Portal...
									if (object_position.equals(new Position(2932, 4854, 0))
										|| object_position.equals(new Position(2885, 4850, 0))
										|| object_position.equals(new Position(2889, 4813, 0))
										|| object_position.equals(new Position(2933, 4815, 0))) { //...in Rune Essence Mine
										player.teleport(new Position(2967, 3408, 0));
									}
									break;
								//End Teleports
								case 1276: //NORMAL_1
								case 1277: //NORMAL_2
								case 1278: //NORMAL_3
								case 1279: //NORMAL_4
								case 1280: //NORMAL_5
								case 1330: //NORMAL_6
								case 1331: //NORMAL_7
								case 1332: //NORMAL_8
								case 2409: //NORMAL_9
								case 3033: //NORMAL_10
								case 3034: //NORMAL_11
								case 3036: //NORMAL_12
								case 3879: //NORMAL_ELF_1
								case 3881: //NORMAL_ELF_2
								case 3882: //NORMAL_ELF_3
								case 3883: //NORMAL_ELF_4
								case 1315: //EVERGREEN_1
								case 1316: //EVERGREEN_2
								case 1318: //EVERGREEN_3
								case 1319: //EVERGREEN_4
								case 1282: //DEAD_1
								case 1283: //DEAD_2
								case 1284: //DEAD_3
								case 1285: //DEAD_4
								case 1286: //DEAD_5
								case 1289: //DEAD_6
								case 1290: //DEAD_7
								case 1291: //DEAD_8
								case 1365: //DEAD_9
								case 1383: //DEAD_10
								case 1384: //DEAD_11
								case 3035: //DEAD_12
								case 5902: //DEAD_13
								case 5903: //DEAD_14
								case 5904: //DEAD_15
								case 4818: //JUNGLE_TREE_1
								case 4820: //JUNGLE_TREE_2
								case 24168: //DYING_TREE
								case 2023: //ACHEY
								case 1281: //OAK_1
								case 3037: //OAK_2
								case 1308: //WILLOW_1
								case 5551: //WILLOW_2
								case 5552: //WILLOW_3
								case 5553: //WILLOW_4
								case 9036: //TEAK
								case 1307: //MAPLE_1
								case 4674: //MAPLE_2
								case 2289: //HOLLOW_SMALL
								case 4060: //HOLLOW_BIG
								case 9034: //MAHOGANY
								case 21273: //ARCTIC_PINE
								case 1309: //YEW
								case 1306: //MAGIC
								case 1292: //DRAMEN_TREE
								case 5103: //VINES_1
								case 5104: //VINES_2
								case 5105: //VINES_3
								case 5106: //VINES_4
								case 5107: //VINES_5
								//Begin Woodcutting
									player.getActionDeque().clearAllActions();
									player.getActionDeque().addAction(new Felling(player, object));
									break;
								//End Woodcutting
								//Begin Mining
								case 2491: //ESS
								case 2108: //CLAY_OLD_TALL
								case 2109: //CLAY_OLD_REGULAR
								case 9711: //CLAY_RUST_TALL
								case 9712: //CLAY_RUST_FLAT
								case 9713: //CLAY_RUST_REGULAR
								case 11948: //CLAY_DARKBROWN_TALL
								case 11949: //CLAY_DARKBROWN_FLAT
								case 11950: //CLAY_DARKBROWN_REGULAR
								case 2090: //COPPER_OLD_TALL
								case 2091: //COPPER_OLD_REGULAR
								case 9708: //COPPER_RUST_TALL
								case 9709: //COPPER_RUST_FLAT
								case 9710: //COPPER_RUST_REGULAR
								case 11936: //COPPER_LIGHTBROWN_TALL
								case 11937: //COPPER_LIGHTBROWN_FLAT
								case 11938: //COPPER_LIGHTBROWN_REGULAR
								case 11960: //COPPER_DARKBROWN_TALL
								case 11961: //COPPER_DARKBROWN_FLAT
								case 11962: //COPPER_DARKBROWN_REGULAR
								case 14906: //COPPER_LIGHTGREY_TALL
								case 14907: //COPPER_LIGHTGREY_FLAT
								case 21284: //COPPER_GREY_TALL
								case 21285: //COPPER_GREY_FLAT
								case 21286: //COPPER_GREY_REGULAR
								case 2094: //TIN_OLD_TALL
								case 2095: //TIN_OLD_REGULAR
								case 6945: //TIN_GREENDARKGREY_TALL
								case 6946: //TIN_GREENDARKGREY_FLAT
								case 9714: //TIN_RUST_TALL
								case 9715: //TIN_RUST_FLAT
								case 9716: //TIN_RUST_REGULAR
								case 11933: //TIN_LIGHTBROWN_TALL
								case 11934: //TIN_LIGHTBROWN_FLAT
								case 11935: //TIN_LIGHTBROWN_REGULAR
								case 11957: //TIN_DARKBROWN_TALL
								case 11958: //TIN_DARKBROWN_FLAT
								case 11959: //TIN_DARKBROWN_REGULAR
								case 14902: //TIN_LIGHTGREY_TALL
								case 14903: //TIN_LIGHTGREY_FLAT
								case 21293: //TIN_GREY_TALL
								case 21294: //TIN_GREY_FLAT
								case 21295: //TIN_GREY_REGULAR
								case 10583: //BLURITE_TALL
								case 10584: //BLURITE_FLAT
								case 2110: //BLURITE_REGULAR
								case 2092: //IRON_OLD_TALL
								case 2093: //IRON_OLD_REGULAR
								case 6943: //IRON_GREENDARKGREY_TALL
								case 6944: //IRON_GREENDARKGREY_FLAT
								case 9717: //IRON_RUST_TALL
								case 9718: //IRON_RUST_FLAT
								case 9719: //IRON_RUST_REGULAR
								case 11954: //IRON_DARKBROWN_TALL
								case 11955: //IRON_DARKBROWN_FLAT
								case 11956: //IRON_DARKBROWN_REGULAR
								case 14856: //IRON_DARKGREY_TALL
								case 14857: //IRON_DARKGREY_FLAT
								case 14858: //IRON_DARKGREY_REGULAR
								case 14900: //IRON_LIGHTGREY_TALL
								case 14901: //IRON_LIGHTGREY_FLAT
								case 14913: //IRON_LIGHTGREY_TALL_2
								case 14914: //IRON_LIGHTGREY_FLAT_2
								case 21281: //IRON_GREY_TALL
								case 21282: //IRON_GREY_FLAT
								case 21283: //IRON_GREY_REGULAR
								case 2100: //SILVER_OLD_TALL
								case 2101: //SILVER_OLD_REGULAR
								case 15579: //SILVER_DARK_DARK_GREY_TALL
								case 15580: //SILVER_DARK_DARK_GREY_FLAT
								case 15581: //SILVER_DARK_DARK_GREY_REGULAR
								case 16998: //SILVER_LIGHT_LIGHT_GREY_TALL
								case 16999: //SILVER_LIGHT_LIGHT_GREY_FLAT
								case 17000: //SILVER_LIGHT_LIGHT_GREY_REGULAR
								case 2098: //GOLD_OLD_TALL
								case 2099: //GOLD_OLD_REGULAR
								case 2609: //GOLD_GREY_TALL
								case 2610: //GOLD_GREY_FLAT
								case 2611: //GOLD_GREY_REGULAR
								case 9720: //GOLD_RUST_TALL
								case 9721: //GOLD_RUST_FLAT
								case 9722: //GOLD_RUST_REGULAR
								case 11951: //GOLD_DARKBROWN_TALL
								case 11952: //GOLD_DARKBROWN_FLAT
								case 11953: //GOLD_DARKBROWN_REGULAR
								case 14904: //GOLD_LIGHTGREY_TALL
								case 14905: //GOLD_LIGHTGREY_FLAT
								case 15576: //GOLD_DARK_DARK_GREY_TALL
								case 15577: //GOLD_DARK_DARK_GREY_FLAT
								case 15578: //GOLD_DARK_DARK_GREY_REGULAR
								case 17001: //GOLD_LIGHT_LIGHT_GREY_TALL
								case 17002: //GOLD_LIGHT_LIGHT_GREY_FLAT
								case 17003: //GOLD_LIGHT_LIGHT_GREY_REGULAR
								case 21290: //GOLD_GREY_TALL_2
								case 21291: //GOLD_GREY_FLAT_2
								case 21292: //GOLD_GREY_REGULAR_2
								case 2096: //COAL_OLD_TALL
								case 2097: //COAL_OLD_REGULAR
								case 11930: //COAL_LIGHTBROWN_TALL
								case 11931: //COAL_LIGHTBROWN_FLAT
								case 11932: //COAL_LIGHTBROWN_REGULAR
								case 11963: //COAL_DARKBROWN_TALL
								case 11964: //COAL_DARKBROWN_FLAT
								case 11965: //COAL_DARKBROWN_REGULAR
								case 14850: //COAL_DARKGREY_TALL
								case 14851: //COAL_DARKGREY_FLAT
								case 14852: //COAL_DARKGREY_REGULAR
								case 15246: //COAL_GREENGREY_TALL
								case 15247: //COAL_GREENGREY_FLAT
								case 15248: //COAL_GREENGREY_REGULAR
								case 21287: //COAL_GREY_TALL
								case 21288: //COAL_GREY_FLAT
								case 21289: //COAL_GREY_REGULAR
								case 2102: //MITHRIL_OLD_TALL
								case 2103: //MITHRIL_OLD_REGULAR
								case 11942: //MITHRIL_LIGHTBROWN_TALL
								case 11943: //MITHRIL_LIGHTBROWN_FLAT
								case 11944: //MITHRIL_LIGHTBROWN_REGULAR
								case 11945: //MITHRIL_DARKBROWN_TALL
								case 11946: //MITHRIL_DARKBROWN_FLAT
								case 11947: //MITHRIL_DARKBROWN_REGULAR
								case 14853: //MITHRIL_DARKGREY_TALL
								case 14854: //MITHRIL_DARKGREY_FLAT
								case 14855: //MITHRIL_DARKGREY_REGULAR
								case 21278: //MITHRIL_GREY_TALL
								case 21279: //MITHRIL_GREY_FLAT
								case 21280: //MITHRIL_GREY_REGULAR
								case 2104: //ADAMANTITE_OLD_TALL
								case 2105: //ADAMANTITE_OLD_REGULAR
								case 11939: //ADAMANTITE_LIGHTBROWN_TALL
								case 11940: //ADAMANTITE_LIGHTBROWN_FLAT
								case 11941: //ADAMANTITE_LIGHTBROWN_REGULAR
								case 14862: //ADAMANTITE_DARKGREY_TALL
								case 14863: //ADAMANTITE_DARKGREY_FLAT
								case 14864: //ADAMANTITE_DARKGREY_REGULAR
								case 21275: //ADAMANTITE_GREY_TALL
								case 21276: //ADAMANTITE_GREY_FLAT
								case 21277: //ADAMANTITE_GREY_REGULAR
								case 2106: //RUNITE_OLD_TALL
								case 2107: //RUNITE_OLD_REGULAR
								case 6669: //RUNITE_GREENDARKGREY_TALL
								case 6670: //RUNITE_GREENDARKGREY_FLAT
								case 14859: //RUNITE_DARKGREY_TALL
								case 14860: //RUNITE_DARKGREY_FLAT
								case 14861: //RUNITE_DARKGREY_REGULAR
								case 2111: //SHILO_GEM_ROCK_OLD_REGULAR
								case 17004: //SHILO_GEM_ROCK_GREY_TALL
								case 17005: //SHILO_GEM_ROCK_GREY_FLAT
								case 17006: //SHILO_GEM_ROCK_GREY_REGULAR
								case 23560: //SHILO_GEM_ROCK_DARK_GREY_TALL
								case 23561: //SHILO_GEM_ROCK_DARK_GREY_FLAT
								case 23562: //SHILO_GEM_ROCK_DARK_GREY_REGULAR
								case 23566: //SHILO_GEM_ROCK_GREY_TALL_2
								case 23567: //SHILO_GEM_ROCK_GREY_FLAT_2
								case 23568: //SHILO_GEM_ROCK_GREY_REGULAR_2
									player.getActionDeque().clearAllActions();
									player.getActionDeque().addAction(new Mining(player, RegionClipping.getRSObject(player, x, y, z)));
									break;
								case 453: //Empty Old Rock - Very Tall
								case 450: //Empty Old Rock - Tall
								case 451: //Empty Old Rock - Regular
								case 452: //Empty Old Rock - Regular 
								case 10585: //Empty White Rock - Tall
								case 10586: //Empty White Rock - Flat
								case 10587: //Empty White Rock - Regular
								case 17007: //Empty Light Light Grey Rock - Tall
								case 17008: //Empty Light Light Grey Rock - Flat
								case 17009: //Empty Light Light Grey Rock - Regular
								case 14915: //Empty Light Grey Rock - Tall
								case 14916: //Empty Light Grey Rock - Flat
								case 21296: //Empty Grey Rock - Tall
								case 21297: //Empty Grey Rock - Flat
								case 21298: //Empty Grey Rock - Regular
								case 14832: //Empty Dark Grey Rock - Tall
								case 14833: //Empty Dark Grey Rock - Flat
								case 14834: //Empty Dark Grey Rock - Regular
								case 15582: //Empty Dark Dark Grey Rock - Tall
								case 15583: //Empty Dark Dark Grey Rock - Flat
								case 15584: //Empty Dark Dark Grey Rock - Regular
								case 6947: //Empty Green-Dark Grey Rock - Tall
								case 6948: //Empty Green-Dark Grey Rock - Flat
								case 15249: //Empty Green-Grey Rock - Tall
								case 15250: //Empty Green-Grey Rock - Flat
								case 15251: //Empty Green-Grey Rock - Regular
								case 9723: //Empty Rust Rock - Tall
								case 9724: //Empty Rust Rock - Flat
								case 9725: //Empty Rust Rock - Regular
								case 11552: //Empty Light Brown Rock - Tall
								case 11553: //Empty Light Brown Rock - Flat
								case 11554: //Empty Light Brown Rock - Regular
								case 11555: //Empty Dark Brown Rock - Tall
								case 11556: //Empty Dark Brown Rock - Flat
								case 11557: //Empty Dark Brown Rock - Regular
									player.getActionSender().sendMessage("There are currently no ores in this rock.");
									break;
								//End Mining
								//Begin Fight Pits
								case 9367: //Hot Vent Door
								case 9368: //Hot Vent Door
								case 9369: //Hot Vent Door
									if (object_position.equals(new Position(2399, 5176, 0))) {
										if (player.getPosition().getY() > y) {
											Server.getSingleton().getFightPits().addWaitingPlayer(player);
											player.teleport(new Position(2399, 5175, 0));
										} else {
											player.teleport(new Position(2399, 5177, 0));
											Server.getSingleton().getFightPits().quit(player);
										}
									} else if (object_position.equals(new Position(2399, 5168, 0))) {
										if (player.getPosition().getY() < y) {
											Server.getSingleton().getFightPits().quit(player);
										}
									}
									break;
								case 9356:
									Npc npc = new Npc(2617);
									FightCaves caves = new FightCaves(player, npc);
									caves.start();
									break;
								//End Fight Pits
								/*
								//Begin Converted py
								case "sarcophagus":
									int tomb = player.getClickId();
									Brother brother = Brother.forTomb(tomb);
									if (brother != null) {
										if (brother == player.getAttribute("hidden_tomb")) {
											Barrows.requestHiddenTomb(player);
											break;
										}
										Barrows.awaken(brother, player);
									}
									break;
								case "cave entrance":
									if (player.getClickId() == 9357) {
										Minigame minigame = player.getAttribute("soloMinigame");
										if (minigame != null) {
											minigame.quit(player);
										}
										player.clipTeleport(2439, 5170, 0, 2);
									}
									break;
								//Start Stronghold of Security
								case "gate of war": {
									Npc npc = new Npc(4377);
									Npc[] npcParam = { npc };
									DialogueSession d = new SecurityGate(player, npcParam);
									d.initiate();
									break;
								}
								case "rickety door": {
									Npc npc = new Npc(4378);
									Npc[] npcParam = { npc };
									DialogueSession d = new SecurityGate(player, npcParam);
									d.initiate();
									break;
								}
								case "oozing barrier": {
									Npc npc = new Npc(4379);
									Npc[] npcParam = { npc };
									DialogueSession d = new SecurityGate(player, npcParam);
									d.initiate();
									break;
								}
								case "portal of death": {
									Npc npc = new Npc(4380);
									Npc[] npcParam = { npc };
									DialogueSession d = new SecurityGate(player, npcParam);
									d.initiate();
									break;
								}
								case "gift of peace": {
									String claimed = "You have already claimed your reward from this level.";
									String msg = "The box hinges creak and appear to be forming audible words...";
									Item item = new Item(995, 10000);
									Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, msg);
									dialogue.add(StatementType.NORMAL, "...congratulations adventurer, you have been deemed worthy of this", "reward. You have also unlocked the Flap emote!");
									player.open(dialogue);
									player.getInventory().addItem(item);
									player.getActionSender().sendConfig(753, 1);
									break;
								}
								case "grain of plenty": {
									String claimed = "You have already claimed your reward from this level.";
									String msg = "The grain shifts in the sack, sighing audible words...";
									Item item = new Item(995, 15000);
									Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, msg);
									dialogue.add(StatementType.NORMAL, "...congratulations adventurer, you have been deemed worthy of this", "reward. You have also unlocked the Slap Head emote!");
									player.open(dialogue);
									player.getInventory().addItem(item);
									player.getActionSender().sendConfig(754, 1);
									break;
								}
								case "box of health": {
									String claimed = "You have already claimed your reward from this level.";
									String msg = "The box hinges creak and appear to be forming audible words...";
									Item item = new Item(995, 25000);
									Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, msg);
									dialogue.add(StatementType.NORMAL, "...congratulations adventurer, you have been deemed worthy of this", "reward. You have also unlocked the Idea emote!");
									player.open(dialogue);
									player.getInventory().addItem(item);
									player.getActionSender().sendConfig(751, 1).sendMessage("You feel refreshed and renewed.");
									player.getSkill().normalize();
									break;
								}
								case "cradle of life": {
									if (player.getInventory().getItemContainer().contains(9005) || player.getInventory().getItemContainer().contains(9006) || player.getEquipment().getItemContainer().contains(9005) || player.getEquipment().getItemContainer().contains(9006) || player.getBank().contains(9005) || player.getBank().contains(9006)) {
										Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, "You have already claimed your reward from this level.");
										player.open(dialogue);
										break;
									}
									Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, "As your hand touches the cradle, you hear a voice in your head of a", "million dead adventurers...");
									dialogue.add(StatementType.NORMAL, "....welcome adventurer... you have a choice....");
									dialogue.add(StatementType.NORMAL, "They will both protect your feet exactly the same, however they look", "very different. You can always come back and get another pair if", "you lose them, or even swap them for the other style!");
									Option fancy = new Option("Fancy Boots", new FancyBoots(player));
									Option fight = new Option("Fighting Boots", new FighterBoots(player));
									dialogue.add("Select an Option", fancy, fight);
									player.open(dialogue);
									break;
								}
								//End Stronghold of Security
								//Begin Waterfall Quest
								case 2020: //Dead tree above entrance for Waterfall Quest
									player.getActionSender().sendMessage("You slip and tumble over the waterfall.");
									World.submit(new TreeFall(player));
									break;
								case "ledge": {
									Quest quest = QuestRepository.get("WaterfallQuest");
									if (player.getQuestStorage().hasStarted(quest)) {
										Item item = new Item(295, 1);
										if (player.getInventory().getItemContainer().contains(item) || player.getEquipment().getItemContainer().contains(item)) {
											player.getActionSender().sendMessage("The door begins to open.");
											World.submit(new EnterBaxtorianFalls(player));
										} else {
											player.hit(5, 1, false);
											player.getActionSender().sendMessage("You can not enter.");
										}
									}
									break;
								}
								case "glarials tomb": {
									Quest quest = QuestRepository.get("WaterfallQuest");
									if (player.getClickId() == 1993) {
										if (player.getClickX() == 2542 && player.getClickY() == 9811) {
											player.getActionSender().sendMessage("You search the coffin.");
											if (player.getQuestStorage().hasStarted(quest)) {
												World.submit(new GlarialsTomb(player));
											}
										}
									}
									break;
								}
								case "bookcase": {
									if (player.getClickId() == 1989 && player.getPosition().getZ() == 1) {
										Quest quest = QuestRepository.get("WaterfallQuest");
										if (player.getQuestStorage().hasStarted(quest)) {
											Item book = new Item(292, 1);
											if (player.getInventory().getItemContainer().contains(book.getId()) || player.getBank().contains(book.getId())) {
												player.getActionSender().sendMessage("You already have a copy of the " + ItemManager.getInstance().getItemName(book.getId()).toLowerCase() + ".");
												break;
											}
											if (player.getInventory().getItemContainer().hasRoomFor(book)) {
												player.getInventory().addItem(book);
											}
										} else { 
											player.getActionSender().sendMessage("You search the bookcase, but find nothing of interest.");
										}
									}
									break;
								}
								case "raft": {
									Quest quest = QuestRepository.get("WaterfallQuest");
									if (player.getQuestStorage().hasStarted(quest)) {
										player.getActionSender().sendMessage("You board the small raft.");
										World.submit(new RaftMessage(player));
									}
									break;
								}
								case "barrel": {
									player.getActionSender().sendMessage("You climb in the barrel and start rocking.");
									player.getActionSender().sendMessage("The barrel falls off the ledge.");
									World.submit(new WaterfallBarrel(player));
									break;
								}
								case "crate": {
									String msg = "You search the crate but find nothing.";
									if (player.getClickId() == 1990 || player.getClickId() == 1999) {
										player.getActionSender().sendMessage("You search the crate...");
										if (player.getClickX() == 2548 && player.getClickY() == 9565 || player.getClickX() == 2589 && player.getClickY() == 9888) {
											Quest quest = QuestRepository.get("WaterfallQuest");
											if (player.getQuestStorage().hasStarted(quest)) {
												World.submit(new SearchCrate(player));
											} else {
												player.getActionSender().sendMessage(msg);
											}
										}
									} else {
										player.getActionSender().sendMessage(msg);
									}
									break;
								}
								case "door": {
									player.getActionSender().sendMessage(player.getClickId() + " is the door ID at " + player.getClickX() + "-" + player.getClickY() + ".");
									if (player.getClickId() == 1991) {
										Quest quest = QuestRepository.get("WaterfallQuest");
										if (player.getQuestStorage().hasStarted(quest)) {
											if (player.getInventory().getItemContainer().contains(2840)) {
												player.clipTeleport(player.getPosition().getX(), (player.getPosition().getY() + 3), player.getPosition().getZ(), 2);
											}
										}
									}
									break;
								}
								//End Waterfall Quest
								case "closed bank chest": {
									int x = player.getClickX();
									int y = player.getClickY();
									int z = player.getPosition().getZ();
									RSObject obj = RegionClipping.getRSObject(player, x, y, z);
									RSObject new_obj = new RSObject(obj.getId() + 1, x, y, z, obj.getType(), obj.getFace());
									GlobalObjectHandler.createGlobalObject(player, new_obj);
									break;
								}
								case "closed chest": {
									//TODO: Make this chest open first
									if (player.getClickId() == 1994) {
										if (player.getClickX() == 2530 && player.getClickY() == 9844) {
											//Used to be new GlobalObject...
											RSObject obj = new RSObject(379, new Position(player.getClickX(), player.getClickY(), player.getPosition().getZ()), 1, 10);
											GlobalObjectHandler.createGlobalObject(player, obj);
										}
									}
									break;
								}
								//Begin Mage Training Arena (mta)
								case "doorway": {
									int y = 0;
									if (player.getPosition().getX() == 3363) {
										if (player.getPosition().getY() == 3298) {
											y = player.getPosition().getY() + 2;
										} else if (player.getPosition().getY() == 3300) {
											y = player.getPosition().getY() - 2;
										}
										player.teleport(new Position(player.getPosition().getX(), y, 0));
									}
									break;
								}
								case "exit teleport": {
									boolean inChamber = Arena.getChamberArea().isInArea(player.getPosition());
									boolean inAlchemy = Arena.getAlchemyArea().isInArea(player.getPosition());
									player.clipTeleport(3363, 3318, 0, 1);
									if (inChamber) {
										Arena.getEnchanters().remove(player);
										player.getActionSender().showComponent(EnchantmentChamber.getCurrentComponent(), false);
									} else if (inAlchemy) {
										Arena.getAlchemists().remove(player);
										player.getActionSender().showComponent(AlchemistPlayground.getCurrentComponent(), false);
									} else {
										int amt = 0;
										if (player.getAttribute("foodDeposits") == null) {
											amt = 0;
										} else {
											player.getAttribute("foodDeposits");
										}
										Arena.getGraveyardMembers().remove(player);
										Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, "In this trip you gave " + amt + " fruit and you earned " + player.getGravePizazz() + " Graveyard Pizazz Points!");
										player.open(dialogue);
										player.removeAttribute("foodDeposits");
									}
									break;
								}
								case "alchemists teleport":
									player.clipTeleport(3366, 9623, 2, 1);
									Arena.getAlchemists().add(player);
									AlchemistPlayground.updateAlchemistInterface(player);
									player.getActionSender().sendMessage("You've entered the Alchemists' Playground.");
									break;
								case "enchanters teleport":
									player.clipTeleport(3363, 9649, 0, 1);
									Arena.getEnchanters().add(player);
									EnchantmentChamber.updateEnchantInterface(player);
									player.getActionSender().sendMessage("You've entered the Enchantment Chamber.");
									break;
								case "graveyard teleport":
									player.clipTeleport(3364, 9640, 1, 1);
									Arena.getGraveyardMembers().add(player);
									CreatureGraveyard.updateGraveyardInterface(player);
									player.getActionSender().sendMessage("You've entered the Creature Graveyard.");
									break;
								case "bones": {
									int collected = player.getAttribute("bonesCollected");
									Item item = null;
									if (collected < 4 || collected >= 16) {
										if (collected >= 16) {
											collected = 0;
										}
										item = new Item(6904, 1);
									} else if (collected >= 4 && collected < 8) {
										item = new Item(6905, 1);
									} else if (collected >= 8 && collected < 12) {
										item = new Item(6906, 1);
									} else if (collected >= 12 && collected < 16) {
										item = new Item(6907, 1);
									}
									if (player.getInventory().hasRoomFor(item)) {
										if (player.getInventory().addItem(item)) {
											player.setAttribute("canTake", 0);
											World.submit(new CollectionDelay(player));
											player.getUpdateFlags().sendAnimation(827);
											player.setAttribute("bonesCollected", collected + 1);
										}
									}
									break;
								}
								case "food chute": {
									int banana_count = player.getInventory().getItemContainer().getCount(1963);
									int peach_count = player.getInventory().getItemContainer().getCount(6883);
									if (banana_count > 0) {
										Item item = player.getInventory().getItemContainer().getById(1963);
										for (int i = 0; i < banana_count; i++) {
											player.getInventory().getItemContainer().remove(item);
										}
										player.getInventory().refresh();
									}
									if (peach_count > 0) {
										Item item = player.getInventory().getItemContainer().getById(6883);
										for (int i = 0; i < peach_count; i++) {
											player.getInventory().getItemContainer().remove(item);
										}
										player.getInventory().refresh();
									}
									String pre = player.getAttribute("foodChute");
									int pre_deposit = 0;
									if (pre == null) {
										pre_deposit = 0;
									} else {
										pre_deposit = Integer.parseInt(pre);
									}
									int total = pre_deposit + banana_count + peach_count;
									if (total >= 16) {
										int points = total / 16;
										player.setGravePizazz(player.getGravePizazz() + points);
										CreatureGraveyard.updateGraveyardInterface(player);
										if (pre != null) {
											player.removeAttribute("foodChute");
										}
										if (total > 16) {
											int extra = total - (16 * points);
											player.setAttribute("foodChute", extra);
										}
									} else {
										if (total == 0) {
											break;
										}
										player.setAttribute("foodChute", total);
									}
									//Store Overall attribute
									String deps = player.getAttribute("foodDeposits");
									int amt = 0;
									if (deps == null) {
										amt = banana_count + peach_count;
									} else {
										amt = banana_count + peach_count + Integer.parseInt(deps);
									}
									player.setAttribute("foodDeposits", amt);
									break;
								}
								case "cube pile": {
									Item item = new Item(6899, 1);
									if (player.getAttribute("canTake") == null) {
										if (player.getInventory().hasRoomFor(item)) {
											if (player.getInventory().addItem(item)) {
												player.setAttribute("canTake", 0);
												World.submit(new CollectionDelay(player));
												player.getUpdateFlags().sendAnimation(827);
											}
										}
									}
									break;
								}
								case "cylinder pile": {
									Item item = new Item(6898, 1);
									if (player.getAttribute("canTake") == null) {
										if (player.getInventory().hasRoomFor(item)) {
											if (player.getInventory().addItem(item)) {
												player.setAttribute("canTake", 0);
												World.submit(new CollectionDelay(player));
												player.getUpdateFlags().sendAnimation(827);
											}
										}
									}
									break;
								}
								case "icosahedron pile": {
									Item item = new Item(6900, 1);
									if (player.getAttribute("canTake") == null) {
										if (player.getInventory().hasRoomFor(item)) {
											if (player.getInventory().addItem(item)) {
												player.setAttribute("canTake", 0);
												World.submit(new CollectionDelay(player));
												player.getUpdateFlags().sendAnimation(827);
											}
										}
									}
									break;
								}
								case "pentamid pile": {
									Item item = new Item(6901, 1);
									if (player.getAttribute("canTake") == null) {
										if (player.getInventory().hasRoomFor(item)) {
											if (player.getInventory().addItem(item)) {
												player.setAttribute("canTake", 0);
												World.submit(new CollectionDelay(player));
												player.getUpdateFlags().sendAnimation(827);
											}
										}
									}
									break;
								}
								case "hole": {
									if (Arena.getChamberArea().isInArea(player.getPosition())) {
										int id = 6902;
										if (player.getInventory().getItemContainer().contains(id)) {
											Item item = player.getInventory().getItemContainer().getById(id);
											int amt = player.getInventory().getItemContainer().getCount(id);
											for (int i = 0; i < amt; i++) {
												player.getInventory().getItemContainer().remove(item);
											}
											player.getInventory().refresh();
											if (amt >= 20) {
												int[] rewards = { 560, 564, 565 };
												Item reward = new Item(Misc.generatedValue(rewards), 3);
												if (player.getInventory().addItem(reward)) {
													Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, "Congratulations! You've been rewarded with an item for your efforts");
													player.open(dialogue);
													player.getActionSender().sendMessage("Congratulations! You've been rewarded with " + reward.getCount() + " " + ItemManager.getInstance().getItemName(reward.getId()).toLowerCase() + "for your efforts.");
												}
											}
										}
									}
									break;
								}
								case "cupboard": {
									Integer content = AlchemistPlayground.getCupboardItems().get(player.getClickId());
									if (content == null) {
										player.getActionSender().sendMessage("The cupboard is empty.");
										break;
									}
									Item item = new Item(AlchemistPlayground.getCupboardItems().get(player.getClickId()), 1);
									if (player.getAttribute("canTake") == null) {
										if (player.getInventory().hasRoomFor(item)) {
											if (player.getInventory().addItem(item)) {
												player.getActionSender().sendMessage("You found: " + ItemManager.getInstance().getItemName(item.getId()) + ".");
												player.setAttribute("canTake", 0);
												World.submit(new CollectionDelay(player));
											}
										}
									}
									break;
								}
								case "coin collector": {
									//For every 100 coins deposited in the coin collector, the player will gain one pizazz point.
									//The player also gains two magic experience per coin deposited, and ten coins (deposited straight to the player's bank) for every 100 deposited.
									if (player.getInventory().getItemContainer().contains(8890)) {
										Item item = player.getInventory().getItemContainer().getById(8890);
										if (item.getCount() >= 12000) {
											if (player.getInventory().removeItem(item)) {
												Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, "Your deposit exceeds the maximum amount allowed! As a", "result, you receive no reward.");
												player.open(dialogue);
											}
											break;
										}
										int points = (int) (item.getCount() * 0.01);
										int exp = (int) (item.getCount() * 2);
										int gp = (int) (item.getCount() * 0.1); //10%
										if (player.getInventory().removeItem(item)) {
											Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, "You've just deposited " + item.getCount() + " coins, earning you " + points + " Alchemist Pizazz", "Points and " + exp + " Magic XP. So far you're taking " + gp + " coins as a reward", "when you leave!");
											player.open(dialogue);
											points = player.getAlchemyPizazz() + points;
											player.setAlchemyPizazz(points);
											AlchemistPlayground.updateAlchemistInterface(player);
											player.getBank().add(new Item(995, gp));
										}
									}
									break;
								}
								//End Mage Training Arena (mta)
								case "cauldron": {
									if (player.getClickX() == 2967 && player.getClickY() == 3205)  {
										Quest quest = QuestRepository.get("WitchsPotion");
										if (player.getQuestStorage().hasStarted(quest)) {
											if (player.getQuestStorage().getState(quest) == 3) {
												player.setAttribute("currentQuest", quest);
												Dialogue dialogue = new StatementDialogue(StatementType.NORMAL, "You drink from the cauldron, it tastes horrible! You feel yourself", "imbued with power.");
												player.getQuestStorage().setState(quest, 4);
												player.open(dialogue);
											}
										}
									}
									break;
								}
								case "bank booth":
									BankManager.openBank(player);
									break;
								case "scoreboard":
									DuelArena.getInstance().open_scoreboard(player);
									break;
								case "trapdoor":
									if (player.getAttribute("duel_button_forfeit") != null) {
										player.getActionSender().sendMessage("Forfeiting has been disabled for this duel session.");
										break;
									}
									if (player.getAttribute("duel_countdown") != null) {
										player.getActionSender().sendMessage("You can't forfeit the duel before the duel countdown is over.");
										break;
									}
									player.getActionSender().sendMessage("You forfeit the duel.");
									DuelArena.getInstance().declineDuel(player, true);
									break;
								//Begin Castle Wars
								case "table": {//Castle Wars bandage table
									Item item = null;
									int obj = player.getClickId();
									if (obj == 4458) {
										item = new Item(4049);
									} else if (obj == 4459) {
										item = new Item(4051);
									} else if (obj == 4460) {
										item = new Item(4043);
									} else if (obj == 4461) {
										item = new Item(4053);
									} else if (obj == 4462) {
										item = new Item(4047);
									} else if (obj == 4463) {
										item = new Item(4045);
									} else if (obj == 4464) {
										item = new Item(1265);
									}
									player.getInventory().addItem(item);
									break;
								}
								case "energy barrier": {//Castle Wars base Energy Barrier
									int barrier_x = player.getClickX();
									int barrier_y = player.getClickY();
									int player_x = player.getPosition().getX();
									int player_y = player.getPosition().getY();
									int plane = player.getPosition().getZ();
									RSObject obj = RegionClipping.getRSObject(player, barrier_x, barrier_y, plane);
									int face = obj.getFace();
									player.getActionSender().sendMessage(face + " is the energy barrier's face direction.");
									if (face == 2) {
										if (barrier_x < player_x) {
											player_x -= 1;
										} else {
											player_x += 1;
										}
									} else if (face == 3) {
										if (barrier_y <= player_y) {
											player_y -= 1;
										} else {
											player_y += 1;
										}
									}
									player.getMovementHandler().walk(new Position(player_x, player_y, plane));
									break;
								}
								case "bank chest":
									BankManager.openBank(player);
									break;
								case "saradomin portal": {
									Item hood = player.getEquipment().getItemContainer().get(0);
									Item cape = player.getEquipment().getItemContainer().get(1);
									if (hood != null || cape != null) {
										player.getActionSender().sendMessage("You can't wear hats, capes or helms in the arena.");
										break;
									}
									Server.getSingleton().getCastleWars().join(player, Team.SARADOMIN);
									int x = Misc.randomMinMax(0, 100);
									if (x > 85) {
										int[] npcs = { 708, 709, 43 };
										int id = Misc.generatedValue(npcs);
										Server.getSingleton().getCastleWars().transform(player, id);
									}
									break;
								}
								case "guthix portal": {
									int sp = Server.getSingleton().getCastleWars().getSaradominPlayers().size();
									int zp = Server.getSingleton().getCastleWars().getZamorakPlayers().size();
									if (sp > zp) { 
										//TODO: Put player in Zamorak Portal
										//objectOptionOne_zamorak_portal(player);
									} else {
										if (sp == zp) {
											int x = Misc.randomMinMax(1, 3);
											if (x > 1) {
												//TODO: Put player in Zamorak Portal
												//objectOptionOne_zamorak_portal(player);
											} else {
												//TODO: Put player in Saradomin Portal
												//objectOptionOne_saradomin_portal(player);
											}
										} else {
											//TODO: Put player in Saradomin Portal
											//objectOptionOne_saradomin_portal(player);
										}
									}
									break;
								}
								case "zamorak portal": {
									Item hood = player.getEquipment().getItemContainer().get(0);
									Item cape = player.getEquipment().getItemContainer().get(1);
									if (hood != null || cape != null) {
										player.getActionSender().sendMessage("You can't wear hats, capes or helms in the arena.");
										break;
									}
									Server.getSingleton().getCastleWars().join(player, Team.SARADOMIN);
									int x = Misc.randomMinMax(0, 100);
									if (x > 85) {
										int[] npcs = { 708, 709, 43 };
										int id = Misc.generatedValue(npcs);
										Server.getSingleton().getCastleWars().transform(player, id);
									}
									break;
								}
								case "portal": {
									CastleWars cw = Server.getSingleton().getCastleWars();
									Team team = null;
									if (cw.getSaradominWaiters().contains(player) || cw.getSaradominPlayers().contains(player)) {
										team = Team.SARADOMIN;
									} else if (cw.getZamorakWaiters().contains(player) || cw.getZamorakPlayers().contains(player)) {
										team = Team.ZAMORAK;
									}
									if (team == null) {
										break;
									}
									cw.leave(player, team);
									break;
								}
								case "saradomin standard": {
									RSObject pole = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ());
									player.getActionSender().sendMessage(pole + " is the pole.");
									RSObject obj = new RSObject(4377, pole.getPosition(), pole.getType(), pole.getFace());
									player.getActionSender().sendMessage(obj + " is the object.");
									GlobalObjectHandler.createGlobalObject(player, obj);
									Item banner = new Item(4037, 1);
									player.getEquipment().getItemContainer().set(3, banner);
									player.getEquipment().refresh(3, banner);
									player.setAppearanceUpdateRequired(true);
									player.getEquipment().setBonus();
									break;
								}
								case "zamorak standard": {
									RSObject pole = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ());
									player.getActionSender().sendMessage(pole + " is the pole.");
									RSObject obj = new RSObject(4378, pole.getPosition(), pole.getType(), pole.getFace());
									player.getActionSender().sendMessage(obj + " is the object.");
									GlobalObjectHandler.createGlobalObject(player, obj);
									Item banner = new Item(4039, 1);
									player.getEquipment().getItemContainer().set(3, banner);
									player.getEquipment().refresh(3, banner);
									player.setAppearanceUpdateRequired(true);
									player.getEquipment().setBonus();
									break;
								}
								//End Castle Wars
								case "crates":
									Crates.search(player);
									break;
								//Begin Gnome Agility
								/*
								case "log balance":
									handleObstacleForceWalk(player, 8, "You walk carefully across the slippery log...", "...You make it safely to the other side.");
									break;
								case "obstacle net": {
									Position pos = null;
									if (player.getPosition().getX() > 2480) {
										Obstacle obstacle = new Obstacle.forObjectId(player.getClickId());
										pos = Position(player.getPosition().getX(), obstacle.getPosition().getY(), player.getPosition().getZ());
									}
									handleObstacleTeleport(player, 2, pos, "You climb the netting...");
									break;
								}
								case "tree branch":
									if (player.getPosition().getZ() == 1) {
										handleObstacleTeleport(player, 2, null, "You climb the tree...", "...To the platform above.");
									} else if (player.getPosition().getZ() == 2) {
										handleObstacleTeleport(player, 2, null, "You climb the tree...", "You land on the ground.");
									}
									break;
								case "balancing rope":
									handleObstacleForceWalk(player, 8, "You carefully cross the tightrope.");
									break;
								case "obstacle pipe":
									handleObstacleForceMove(player, 1, 20, 40, Misc.EAST, "You squeeze through the pipe...");
									TackleObstacle.obstaclePipeExit(player, 3, 0, 5);
									break;
								def handleObstacleForceMove(player, ticks, speed, altspeed, dir, *msgs):
									obstacle = Obstacle.forObjectId(player.getClickId())
									TackleObstacle.obstacleForceMovement(player, obstacle, ticks, speed, altspeed, dir, msgs)
									TackleObstacle.updateCourseState(player, obstacle, 'gnomeAgilityCourseState')
								
								def handleObstacleForceWalk(player, ticks, *msgs):
									obstacle = Obstacle.forObjectId(player.getClickId())
									TackleObstacle.obstacleForceWalk(player, obstacle, ticks, msgs)
									TackleObstacle.updateCourseState(player, obstacle, 'gnomeAgilityCourseState')
								
								def handleObstacleTeleport(player, ticks, pos, *msgs):
									obstacle = Obstacle.forObjectId(player.getClickId())
									TackleObstacle.obstacleTeleport(player, obstacle, ticks, pos, msgs)
									TackleObstacle.updateCourseState(player, obstacle, 'gnomeAgilityCourseState')
								*/
								//End Gnome Agility
								//Begin Runecrafting
								case 2486:
									Rune rune = Rune.forId(556);
									player.getActionDeque().addAction(new Runecraft(player, rune));
									break;
								case 2481:
									rune = Rune.forId(557);
									player.getActionDeque().addAction(new Runecraft(player, rune));
									break;
								case 2482:
									rune = Rune.forId(554);
									player.getActionDeque().addAction(new Runecraft(player, rune));
									break;
								case 2487:
									rune = Rune.forId(562);
									player.getActionDeque().addAction(new Runecraft(player, rune));
									break;
								case 2485:
									rune = Rune.forId(563);
									player.getActionDeque().addAction(new Runecraft(player, rune));
									break;
								case 2488:
									rune = Rune.forId(560);
									player.getActionDeque().addAction(new Runecraft(player, rune));
									break;
								case 2490:
									rune = Rune.forId(565);
									player.getActionDeque().addAction(new Runecraft(player, rune));
									break;
								case 2489:
									rune = Rune.forId(566);
									player.getActionDeque().addAction(new Runecraft(player, rune));
									break;
								case 2484:
									rune = Rune.forId(564);
									player.getActionDeque().addAction(new Runecraft(player, rune));
									break;
								//End Runecrafting
								//Begin Stronghold of Security
								case 16154:
									if (object_position.equals(new Position(3081, 3420, 0))) {
										player.teleport(new Position(1860, 5244, 0));
									}
									break;
								//End Stronghold of Security
								//Web Slashing
								case 733: //Web in Level 55 Wilderness, North of Ardougne Teleport Lever
									//Creates a null object to replace the Web
									GlobalObjectHandler.createGlobalObject(player, new CustomObject(9851, new Position(x, y, z), 0, 10));
									RegionClipping.removeObject(player, x, y, z);
									break;
								//Doors
								case 190:
								case 1516:
								case 1519:
								case 1530:
								case 1531:
								case 1533:
								case 1534:
								case 1536:
								case 1596:
								case 1597:
								case 1967:
								case 1968:
								case 2398:
								case 2399:
								case 9523:
								case 11616:
								case 11617:
								case 11624:
								case 11625:
								case 11707:
								case 11708:
								case 11712:
								case 11714:
								case 11716:
								case 11721:
								case 12348:
								case 12349:
								case 12350:
								case 13001:
								case 15535:
								case 15536:
								case 24375:
								case 24376:
								case 24378:
								case 24379:
								case 24381:
								case 24384:
								case 23489:
								case 26808:
								case 26906:
								case 26908:
								case 26910:
								case 26913:
								case 26916:
									final Door door = new Door(object.getId(), object.getX(), object.getY(), object.getHeight(), object.getType(), object.getFace());
									door.open(player);
									break;
								case 10284:
									RSObject chest = new RSObject(6775, object_position, object.getType(), object.getFace());
									GlobalObjectHandler.createGlobalObject(player, chest);
									//TODO: Fix - When clicking on chest more than once, more enemies spawn
									Barrows.openChest(player);
									break;
								default:
									logger.severe("Unhandled Object First Click: [ID: " + id + ", X: " + x + ", Y: " + y + "].");
									if (player.isDebugging()) {
										player.getActionSender().sendMessage("Unhandled Object First Click: [ID: <col=CC0000>" + id + "</col>, X: <col=CC0000>" + x + "</col>, Y: <col=CC0000>" + y + "</col>].");
									}
									break;
							}
						actions = null;
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	public static void doObjectSecondClick(final Player player) {
		final int id = player.getClickId();
		final int x = player.getClickX();
		final int y = player.getClickY();
		final ObjectDefinition def = ObjectDefinition.forId(id);
		final int orig_distance = Misc.getDistance(player.getPosition(), new Position(x, y));
		final RSObject object = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ());

		player.getUpdateFlags().sendFaceToDirection(new Position(x, y, player.getPosition().getZ()));

		player.setWalkToAction(new Task(1, true) {
			@Override
			protected void execute() {
				if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), getObjectSize(player.getClickId()))) {
					return;
				}

				final String objName = (def.getName() == null ? Integer.toString(id) : def.getName().toLowerCase());

				World.submit(new Task(orig_distance == 1 ? 1 : 2, true) {
					@Override
					protected void execute() {
						// TODO: AKZU
						if (def.actions != null && def.actions.length > 1 && def.actions[1].startsWith("Climb-up")) {
							System.err.println("Climb-up");
						} else if (def.actions != null && def.actions.length > 1 && def.actions[1].startsWith("Climb-down")) {
							System.err.println("Climb-down");
						}

						if (id == 3193 || objName.toLowerCase().startsWith("bank") && !objName.toLowerCase().contains("deposit")) {
							BankManager.openBank(player);
						} else {
							switch (objName) {
								//TODO: Mining Prospecting?
								//Begin Smithing
								case "furnace":
									player.getActionSender().sendItemOnInterface(2406, 150, 2351);
									player.getActionSender().sendItemOnInterface(2407, 150, 2355);
									player.getActionSender().sendItemOnInterface(2409, 150, 2353);
									player.getActionSender().sendItemOnInterface(2410, 150, 2357);
									player.getActionSender().sendItemOnInterface(2411, 150, 2359);
									player.getActionSender().sendItemOnInterface(2412, 150, 2361);
									player.getActionSender().sendItemOnInterface(2413, 150, 2363);
									player.getActionSender().sendChatInterface(2400);
									break;
								//End Smithing
								//Begin Thieving
								case "magic stall":
								case "baker's stall":
								case "gem stall":
								case "spice stall":
								case "fur stall":
								case "silver stall":
								case "silk stall":
								case "general stall":
								case "scimitar stall":
								case "food stall":
								case "crafting stall":
								case "vegetable stall":
								case "fish stall":
								case "market stall":
								case "seed stall":
								case "tea stall":
								case "counter":
									player.getActionDeque().addAction(new Shoplift(player, object, 0));
									break;
								//End Thieving
								case "open chest": {
									if (player.getClickId() == 379) {
										if (player.getClickX() == 2530 && player.getClickY() == 9844) {
											Quest  quest = QuestRepository.get("WaterfallQuest");
											if (player.getQuestStorage().hasStarted(quest)) {
												Item item = new Item(295, 1);
												if (player.getInventory().getItemContainer().hasRoomFor(item)) {
													if (player.getInventory().addItem(item)) {
														 player.getActionSender().sendMessage("You search the chest and find a small amulet.");
													}
												}
											}
										}
									}
									break;
								}
								case "spinning wheel":
									Spinnable spin = Spinnable.forId(1737);
									player.getActionDeque().addAction(new WheelSpinning(player, 1, spin));
									break;
							}
							switch (id) {
								case 2418:
									Server.getPartyRoom().openChest(player);
									break;
								default:
									logger.severe("Unhandled Object Second Click: [ID: " + id + ", X: " + x + ", Y: " + y + "].");
									if (player.isDebugging()) {
										player.getActionSender().sendMessage("Unhandled Object Second Click: [ID: <col=CC0000>" + id + "</col>, X: <col=CC0000>" + x + "</col>, Y: <col=CC0000>" + y + "</col>].");
									}
							}
						}
						actions = null;
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	public static void doObjectThirdClick(final Player player) {
		final int id = player.getClickId();
		final int x = player.getClickX();
		final int y = player.getClickY();
		final ObjectDefinition def = ObjectDefinition.forId(id);
		final int orig_distance = Misc.getDistance(player.getPosition(), new Position(x, y));
		final RSObject object = RegionClipping.getRSObject(player, player.getClickX(), player.getClickY(), player.getPosition().getZ());

		player.getUpdateFlags().sendFaceToDirection(new Position(x, y, player.getPosition().getZ()));

		player.setWalkToAction(new Task(1, true) {
			@Override
			protected void execute() {
				if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), getObjectSize(player.getClickId()))) {
					return;
				}

				final String objName = (def.getName() == null ? "" : def.getName().toLowerCase());

				World.submit(new Task(orig_distance == 1 ? 1 : 2, true) {
					@Override
					protected void execute() {
						switch (id) {
						default:
							logger.severe("Unhandled Object Third Click: [ID: " + id + ", X: " + x + ", Y: " + y + "].");
							if (player.isDebugging()) {
								player.getActionSender().sendMessage("Unhandled Object Third Click: [ID: <col=CC0000>" + id + "</col>, X: <col=CC0000>" + x + "</col>, Y: <col=CC0000>" + y + "</col>].");
							}
						}
						actions = null;
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	private static void doNpcFirstClick(final Player player) {
		final int x = player.getClickX();
		final int y = player.getClickY();

		final Npc npc = World.getNpc(player.getNpcClickIndex());

		// We're already interacting with this npc.
		if (player.getInteractingEntity() != null && player.getInteractingEntity() == npc) {
			return;
		}

		player.setWalkToAction(new Task(1, true) {
			@Override
			protected void execute() {
				int offset = npc.getDefinition().getSize();

				switch (npc.getDefinition().getId()) {
					case 305:
						offset += 10;
						break;
				}

				if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), offset)) {
					return;
				}

				World.submit(new Task(1, true) {
					@SuppressWarnings("rawtypes")
					@Override
					protected void execute() {
						player.setInteractingEntity(npc);
						npc.setInteractingEntity(player);
						npc.getUpdateFlags().sendFaceToDirection(player.getPosition());

						npc.setAttribute("IS_BUSY", (byte) 1);
						switch (npc.getDefinition().getName().toLowerCase()) {
							case "makeover mage":
								player.getActionSender().sendInterface(3559);
								break;
							case "gnome trainer": {
								Npc[] npcParam = { npc };
								DialogueSession d = new GnomeTrainer(player, npcParam);
								d.initiate();
								break;
							}
							case "mazchna": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Mazchna(player, npcParam);
								d.initiate();
								break;
							}
							case "vannaka": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Vannaka(player, npcParam);
								d.initiate();
								break;
							}
							case "party pete": {
								Npc[] npcParam = { npc };
								DialogueSession d = new PartyPete(player, npcParam);
								d.initiate();
								break;
							}
							case "postie pete": {
								Npc[] npcParam = { npc };
								DialogueSession d = new PostiePete(player, npcParam);
								d.initiate();
								break;
							}
							case "wise old man": {
								Npc[] npcParam = { npc };
								DialogueSession d = new WiseOldMan(player, npcParam);
								d.initiate();
								break;
							}
							case "golrie": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Golrie(player, npcParam);
								d.initiate();
								break;
							}
							case "almera": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Almera(player, npcParam);
								d.initiate();
								break;
							}
							case "aubury": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Aubury(player, npcParam);
								d.initiate();
								break;
							}
							case "doric": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Doric(player, npcParam);
								d.initiate();
								break;
							}
							case "duke horacio": {
								Npc[] npcParam = { npc };
								DialogueSession d = new DukeHoracio(player, npcParam);
								d.initiate();
								break;
							}
							case "sedridor": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Sedridor(player, npcParam);
								d.initiate();
								break;
							}
							case "father aereck": {
								Npc[] npcParam = { npc };
								DialogueSession d = new FatherAereck(player, npcParam);
								d.initiate();
								break;
							}
							case "father urhney": {
								Npc[] npcParam = { npc };
								DialogueSession d = new FatherUrhney(player, npcParam);
								d.initiate();
								break;
							}
							case "hetty": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Hetty(player, npcParam);
								d.initiate();
								break;
							}
							case "cook": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Cook(player, npcParam);
								d.initiate();
								break;
							}
							case "hudon": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Hudon(player, npcParam);
								d.initiate();
								break;
							}
							case "hadley": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Hadley(player, npcParam);
								d.initiate();
								break;
							}
							case "fred the farmer": {
								Npc[] npcParam = { npc };
								DialogueSession d = new FredTheFarmer(player, npcParam);
								d.initiate();
								break;
							}
							case "kaqemeex": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Kaqemeex(player, npcParam);
								d.initiate();
								break;
							}
							case "sanfew": {
								Npc[] npcParam = { npc };
								DialogueSession d = new Sanfew(player, npcParam);
								d.initiate();
								break;
							}
							case "sheep": {
								SheepShearer.getSingleton().shear(player, npc);
								break;
							}
							case "charmed warrior": {
								Dialogue d = new ChatDialogue(player, null, "Is anybody there?");
								d.add(npc, null, "What do you think?");
								player.open(d);
								break;
							}
							case "man": {
								String[][] strings = {
									{ "Sorry, were you speaking to me? I was daydreaming.", "Hello, anyway." },
									{ "Hello, I'm glad to see an adventurer about.", "There's an increase in goblins hanging around the area." },
									{ "Hello to you too, adventurer." },
									{ "Welcome to Lumbridge." },
									{ "Hey, do you like my clothes? They're new." },
									{ "Don't come near me, I have a cold!" }
								};
								int idx = Misc.random(strings.length);
								String[] message = strings[idx];
								Dialogue dialogue = new ChatDialogue(npc, null, message);
								player.open(dialogue);
								break;
							}
							case "woman": {
								String[][] strings = {
									{ "I can't believe that Lachtopher boy.", "He tried to borrow money from me again." },
									{ "You're not from around here, are you?", "I can see it in your eyes." },
									{ "Hello to you too, adventurer." },
									{ "Welcome to Lumbridge." },
									{ "Hey, do you like my clothes? They're new." },
									{ "You've chosen a lovely day to visit us. Welcome."}
								};
								int idx = Misc.random(strings.length);
								String[] message = strings[idx];
								Dialogue dialogue = new ChatDialogue(npc, null, message);
								player.open(dialogue);
								break;
							}
							case "fishing spot": {
								if (npc.getNpcId() == 316) { //Small Net Fishing
									Tools tool = Tools.forId(303);
									Fishable fish = Fishable.forId(tool.getOutcomes()[0]);
									player.getActionDeque().addAction(new Fishing(player, fish, tool));
								} else if (npc.getNpcId() == 324) { //Lobster Fishing
									Tools tool = Tools.forId(301);
									Fishable fish = Fishable.forId(tool.getOutcomes()[0]);
									player.getActionDeque().addAction(new Fishing(player, fish, tool));
								} else if (npc.getNpcId() == 334) { //Big Net Fishing
									Tools tool = Tools.forId(305);
									Fishable fish = Fishable.forId(tool.getOutcomes()[0]);
									player.getActionDeque().addAction(new Fishing(player, fish, tool));
								}
								break;
							}
							case "master farmer":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							default:
								String[][] strings = {
									{ "Hello there, lovely day, isn't it?" },
									{ "Welcome to Exorth, please enjoy your stay." },
									{ "Howdy! Were you looking for help?", "I heard The Wise Old Man in Falador city offers", "a great deal of knowledge and assistance." }
								};
								int idx = Misc.random(strings.length);
								String[] message = strings[idx];
								Dialogue dialogue = new ChatDialogue(npc, null, message);
								player.open(dialogue);
								logger.severe("Unhandled NPC First Click: [ID: " + npc.getDefinition().getId() + ", X: " + npc.getPosition().getX() + ", Y: " + npc.getPosition().getY() + "].");
								if (player.isDebugging()) {
									player.getActionSender().sendMessage("Unhandled NPC First Click: [ID: <col=CC0000>" + npc.getDefinition().getId() + "</col>, X: <col=CC0000>" + npc.getPosition().getX() + "</col>, Y: <col=CC0000>" + npc.getPosition().getY() + "</col>].");
								}
								break;
						}
						actions = null;
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	/**
	 * 
	 * @param player
	 */
	private static void doNpcSecondClick(final Player player) {
		final int x = player.getClickX();
		final int y = player.getClickY();

		final Npc npc = World.getNpc(player.getNpcClickIndex());

		// We're already interacting with this npc.
		if (player.getInteractingEntity() != null && player.getInteractingEntity() == npc) {
			return;
		}

		player.setWalkToAction(new Task(1, true) {
			@Override
			protected void execute() {
				int offset = npc.getDefinition().getSize();

				switch (npc.getDefinition().getId()) {
					case 494:
						offset += 0;
						break;
				}

				if (player.isDebugging()) {
					player.getActionSender().sendMessage("Offset: <col=CC0000>" + offset + "</col>, NPC ID: <col=CC0000>" + npc.getDefinition().getId() + "</col>.");
				}
				logger.info("Offset: " + offset + ", NPC ID: " + npc.getDefinition().getId() + ".");

				if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), offset)) {
					return;
				}

				World.submit(new Task(1, true) {
					@Override
					protected void execute() {
						player.setInteractingEntity(npc);
						npc.setInteractingEntity(player);

						if (player.isDebugging()) {
							logger.info("Click ID: " + player.getClickId() + ", NPC (Name: " + npc.getDefinition().getName() + ", ID: " + npc.getNpcId() + ")");
						}

						npc.getUpdateFlags().sendFaceToDirection(player.getPosition());

						switch (npc.getDefinition().getName().toLowerCase()) {
							case "banker":
								BankManager.openBank(player);
								break;
							case "shopkeeper":
								if (npc.getDefinition().getId() == 520) {
									ShopManager.open(player, 1); //Lumbridge Shopkeeper
								}
								break;
							case "shop assistant":
								if (npc.getDefinition().getId() == 521) {
									ShopManager.open(player, 1); //Lumbridge Shop Assistant
								}
								break;
							case "man":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							case "woman":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							case "farmer":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							case "ham member":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							case "warrior woman":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							case "rogue":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							case "cave goblin":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							case "guard":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							case "hero":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							case "gnome":
								player.getActionDeque().addAction(new Pickpocketing(player, npc, 0));
								break;
							case "ellis": //Leather tanning
								player.getActionSender().sendString("Soft leather", 14777);
								player.getActionSender().sendString("@gre@ 1 Coin", 14785);
								player.getActionSender().sendString("Hard leather", 14778);
								player.getActionSender().sendString("@gre@ 3 Coins", 14786);
								player.getActionSender().sendString("Snake skin", 14779);
								player.getActionSender().sendString("@gre@ 15 Coins", 14787);
								player.getActionSender().sendString("Snake skin", 14780);
								player.getActionSender().sendString("@gre@ 20 Coins", 14788);
								player.getActionSender().sendString("Green D'hide", 14781);
								player.getActionSender().sendString("@gre@ 20 Coins", 14789);
								player.getActionSender().sendString("Blue D'hide", 14782);
								player.getActionSender().sendString("@gre@ 20 Coins", 14790);
								player.getActionSender().sendString("Red D'hide", 14783);
								player.getActionSender().sendString("@gre@ 20 Coins", 14791);
								player.getActionSender().sendString("Black D'hide", 14784);
								player.getActionSender().sendString("@gre@ 20 Coins", 14792);
								player.getActionSender().sendItemOnInterface(14769, 220, 1739);
								player.getActionSender().sendItemOnInterface(14770, 220, 1739);
								player.getActionSender().sendItemOnInterface(14771, 220, 6287);
								player.getActionSender().sendItemOnInterface(14772, 220, 7801);
								player.getActionSender().sendItemOnInterface(14773, 220, 1753);
								player.getActionSender().sendItemOnInterface(14774, 220, 1751);
								player.getActionSender().sendItemOnInterface(14775, 220, 1749);
								player.getActionSender().sendItemOnInterface(14776, 220, 1747);
								player.getActionSender().sendInterface(14670);
								break;
							case "sbott": //Leather Tanning
								player.getActionSender().sendString("Soft leather", 14777);
								player.getActionSender().sendString("@gre@ 2 Coins", 14785);
								player.getActionSender().sendString("Hard leather", 14778);
								player.getActionSender().sendString("@gre@ 5 Coins", 14786);
								player.getActionSender().sendString("Snake skin", 14779);
								player.getActionSender().sendString("@gre@ 25 Coins", 14787);
								player.getActionSender().sendString("Snake skin", 14780);
								player.getActionSender().sendString("@gre@ 45 Coins", 14788);
								player.getActionSender().sendString("Green D'hide", 14781);
								player.getActionSender().sendString("@gre@ 45 Coins", 14789);
								player.getActionSender().sendString("Blue D'hide", 14782);
								player.getActionSender().sendString("@gre@ 45 Coins", 14790);
								player.getActionSender().sendString("Red D'hide", 14783);
								player.getActionSender().sendString("@gre@ 45 Coins", 14791);
								player.getActionSender().sendString("Black D'hide", 14784);
								player.getActionSender().sendString("@gre@ 45 Coins", 14792);
								player.getActionSender().sendItemOnInterface(14769, 220, 1739);
								player.getActionSender().sendItemOnInterface(14770, 220, 1739);
								player.getActionSender().sendItemOnInterface(14771, 220, 6287);
								player.getActionSender().sendItemOnInterface(14772, 220, 7801);
								player.getActionSender().sendItemOnInterface(14773, 220, 1753);
								player.getActionSender().sendItemOnInterface(14774, 220, 1751);
								player.getActionSender().sendItemOnInterface(14775, 220, 1749);
								player.getActionSender().sendItemOnInterface(14776, 220, 1747);
								player.getActionSender().sendInterface(14670);
								break;
							case "tanner": //Leather Tanning
								//2824 - Models : 14769-14776
								player.getActionSender().sendString("Soft leather", 14777);
								player.getActionSender().sendString("@gre@ 1 Coin", 14785);
								player.getActionSender().sendString("Hard leather", 14778);
								player.getActionSender().sendString("@gre@ 3 Coins", 14786);
								player.getActionSender().sendString("Snake skin", 14779);
								player.getActionSender().sendString("@gre@ 15 Coins", 14787);
								player.getActionSender().sendString("Snake skin", 14780);
								player.getActionSender().sendString("@gre@ 20 Coins", 14788);
								player.getActionSender().sendString("Green D'hide", 14781);
								player.getActionSender().sendString("@gre@ 20 Coins", 14789);
								player.getActionSender().sendString("Blue D'hide", 14782);
								player.getActionSender().sendString("@gre@ 20 Coins", 14790);
								player.getActionSender().sendString("Red D'hide", 14783);
								player.getActionSender().sendString("@gre@ 20 Coins", 14791);
								player.getActionSender().sendString("Black D'hide", 14784);
								player.getActionSender().sendString("@gre@ 20 Coins", 14792);
								player.getActionSender().sendItemOnInterface(14769, 220, 1739);
								player.getActionSender().sendItemOnInterface(14770, 220, 1739);
								player.getActionSender().sendItemOnInterface(14771, 220, 6287);
								player.getActionSender().sendItemOnInterface(14772, 220, 7801);
								player.getActionSender().sendItemOnInterface(14773, 220, 1753);
								player.getActionSender().sendItemOnInterface(14774, 220, 1751);
								player.getActionSender().sendItemOnInterface(14775, 220, 1749);
								player.getActionSender().sendItemOnInterface(14776, 220, 1747);
								player.getActionSender().sendInterface(14670);
								break;
							case "fishing spot":
								if (npc.getNpcId() == 313 || npc.getNpcId() == 324 || npc.getNpcId() == 334) { //Harpoon Fishing
									Tools tool = Tools.forId(311);
									Fishable fish = Fishable.forId(Misc.generatedValue(tool.getOutcomes()));
									player.getActionDeque().addAction(new Fishing(player, fish, tool));
								} else if (npc.getNpcId() == 316) { //Bait Fishing [Rod]
									Tools tool = Tools.forId(307);
									Fishable fish = Fishable.forId(Misc.generatedValue(tool.getOutcomes()));
									player.getActionDeque().addAction(new Fishing(player, fish, tool));
								}
							case "fadli":
								BankManager.openBank(player);
								break;
							case "surgeon general tafani":
								player.getActionSender().sendMessage("SGT: " + player.getInteractingEntity() + ".");
								break;
							default:
								logger.severe("Unhandled NPC Second Click: [ID: " + npc.getDefinition().getId() + ", X: " + npc.getPosition().getX() + ", Y: " + npc.getPosition().getY() + "].");
								if (player.isDebugging()) {
									player.getActionSender().sendMessage("Unhandled NPC Second Click: [ID: <col=CC0000>" + npc.getDefinition().getId() + "</col>, X: <col=CC0000>" + npc.getPosition().getX() + "</col>, Y: <col=CC0000>" + npc.getPosition().getY() + "</col>].");
								}
								break;
						}
						actions = null;
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	private static void doNpcThirdClick(final Player player) {
		final int x = player.getClickX();
		final int y = player.getClickY();

		final Npc npc = World.getNpc(player.getNpcClickIndex());

		// We're already interacting with this npc.
		if (player.getInteractingEntity() != null && player.getInteractingEntity() == npc) {
			return;
		}

		player.setWalkToAction(new Task(1, true) {
			@Override
			protected void execute() {
				final Npc npc = World.getNpc(player.getNpcClickIndex());

				if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), npc.getDefinition().getSize())) {
					return;
				}

				World.submit(new Task(1, true) {
					@Override
					protected void execute() {
						player.setInteractingEntity(npc);
						npc.setInteractingEntity(player);

						if (player.isDebugging()) {
							logger.info("Click ID: " + player.getClickId() + ", NPC (Name: " + npc.getDefinition().getName() + ", ID: " + npc.getNpcId() + ")");
						}

						npc.getUpdateFlags().sendFaceToDirection(player.getPosition());

						switch (player.getClickId()) {
							default:
								logger.severe("Unhandled NPC Third Click: [ID: " + npc.getDefinition().getId() + ", X: " + npc.getPosition().getX() + ", Y: " + npc.getPosition().getY() + "].");
								if (player.isDebugging()) {
									player.getActionSender().sendMessage("Unhandled NPC Third Click: [ID: <col=CC0000>" + npc.getDefinition().getId() + "</col>, X: <col=CC0000>" + npc.getPosition().getX() + "</col>, Y: <col=CC0000>" + npc.getPosition().getY() + "</col>].");
								}
								break;
						}
						actions = null;
						this.stop();
					}
				});
				this.stop();
			}
		});
	}

	private static void doItemOnObject(final Player player, final Item item) {
		//TODO: Review to make sure these cases do not cause exceptions or glitches
		final int x = player.getClickX();
		final int y = player.getClickY();
		player.setWalkToAction(new Task(1, true) {
			@Override
			protected void execute() {
				/*
				if (!player.getMovementHandler().walkToAction(new Position(x, y, player.getPosition().getZ()), getObjectSize(player.getClickId()))) {
					logger.severe("CANNOT PREFORM ITEM ON OBJECT!!!");
					return;
				}
				//TODO: TEST
				if (player.getPosition().getX() - x < 0) {
					PathFinder.getSingleton().moveTo(player, x + 1, y);
					return;
				} else if (player.getPosition().getX() - x > 0) {
					PathFinder.getSingleton().moveTo(player, x - 1, y);
					return;
				}
				 */
				switch (player.getClickId()) {
					case 2728:
						if (Cookable.forId(item.getId()) == null) {
							player.getActionSender().sendMessage("You cannot cook that.");
						} else {
							player.setCookingQueue(item.getId());
							CookingAction.prepareInterface(player, item.getId());
						}
						break;
					case 114:
						if (Cookable.forId(item.getId()) == null) {
							player.getActionSender().sendMessage("You cannot cook that.");
						} else {
							player.setCookingQueue(item.getId());
							CookingAction.prepareInterface(player, item.getId());
						}
						break;
					case 2859:
						if (Cookable.forId(item.getId()) == null) {
							player.getActionSender().sendMessage("You cannot cook that.");
						} else {
							player.setCookingQueue(item.getId());
							CookingAction.prepareInterface(player, item.getId());
						}
						break;
					case 2783:
						player.setCurrentBar(Bar.forBarId(item.getId()));
						SmithingFrame.sendInterface(player, player.getCurrentBar());
						/*player.getActionSender().sendConfig(211, 20); //Bars
						player.getActionSender().sendConfig(210, 99); //Level
						player.getActionSender().sendInterface(994); //Interface
						//player.getActionSender().sendConfig(262, 1); //Oil
						//player.getActionSender().sendConfig(261, 1); //Dart tips*/
						break;
					case 11666: //Furnace
						player.getActionDeque().addAction(new GlassMelting(player, (short) 1, Glass.forReward(1775)));
						break;
					case 11759:
						if (player.getInventory().getItemContainer().contains(229)) {
							player.getActionSender().sendMessage("You dip your vial into the fountain...");
							player.getUpdateFlags().sendAnimation(827, 0);
							World.submit(new Task(2) {
								@Override
								protected void execute() {
									player.getActionSender().sendMessage("...and you fill it up.");
									player.getInventory().addItemToSlot(new Item(227), player.getInventory().getItemContainer().getSlotById(229));
									this.stop();
								}
							});
						}
						break;
					case 2638:
						player.getActionSender().sendMessage("You dip your amulet into the fountain...");
						player.getUpdateFlags().sendAnimation(827, 0);
						for (int i = 0; i < Inventory.SIZE; i++) {
							int[] glorys = { 1704, 1706, 1708, 1710, 1712 };
							for (int glory : glorys) {
								if (player.getInventory().getItemContainer().contains(glory)) {
									player.getInventory().addItemToSlot(new Item(1712, 1), player.getInventory().getItemContainer().getSlotById(glory));
								}
							}
						}
						break;
					case 2142: //Cauldron of Thunder
						int itemId = item.getId();
						int index = 0;
						boolean found = false;
						int[] regularMeats = { 2132, 2134, 2136, 2138 };
						int[] enchantedMeats = { 522, 523, 524, 525 };
						for (int i = 0; i < regularMeats.length; i++) {
							if (regularMeats[i] == itemId) {
								found = true;
								index = i;
							}
						}
						if (found != false) {
							player.getInventory().removeItem(item);
							player.getInventory().addItem(new Item(enchantedMeats[index], item.getCount()));
							player.getActionSender().sendMessage("You dip the " + ItemManager.getInstance().getItemName(itemId) + " in the cauldron.");
						}
						break;
					case 10803:
						if (Arena.getChamberArea().isInArea(player.getPosition())) {
							if (player.getInventory().getItemContainer().contains(6902)) {
								Item item = player.getInventory().getItemContainer().getById(6902);
								if (player.getInventory().removeItem(item)) {
									player.getActionSender().sendMessage("TEST ME");
								}
							}
						}
					//Begin Waterfall Quest
					case 2020:
						player.getActionSender().sendMessage("You tie the old rope to the tree.");
						World.submit(new ClimbTree(player));
						break;
					case 1992:
						player.getActionSender().sendMessage("You place the pebble in the gravestone's small indent.");
						World.submit(new InsertPebble(player));
						break;
					case 1996:
						if (item.getId() == 954) {
							player.getUpdateFlags().sendAnimation(774);
							player.getUpdateFlags().sendGraphic(67);
							player.setAttribute("tempWalkAnim", 776);
							player.setAppearanceUpdateRequired(true);
							World.submit(new LassoBoulder(player));
						}
						break;
					case 2004:
						player.getActionSender().sendMessage("HANDLE ME PLZ NEEDS PROPER X COORDS q_q");
						break;
					//End Waterfall Quest
					default:
						logger.severe("Unhandled Item on Object: [Item ID: " + item.getId() + ", Object ID: " + player.getClickId() + ", X: " + x + ", Y: " + y + "].");
						if (player.isDebugging()) {
							player.getActionSender().sendMessage("Unhandled Item on Object: [Item ID: " + item.getId() + ", Object ID: <col=CC0000>" + player.getClickId() + "</col>, X: <col=CC0000>" + x + "</col>, Y: <col=CC0000>" + y + "</col>].");
						}
						break;
				}
				actions = null;
				this.stop();
			}
		});
	}

	public static int getObjectSize(int objectId) {
		return ObjectDefinition.forId(objectId).width;
	}

	public static void setActions(Actions actions) {
		WalkToActions.actions = actions;
	}

	public static Actions getActions() {
		return actions;
	}
}
