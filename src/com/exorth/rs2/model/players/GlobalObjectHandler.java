package com.exorth.rs2.model.players;

import java.util.logging.Logger;

import com.exorth.rs2.model.World;
import com.exorth.rs2.model.object.CustomObject;
import com.exorth.rs2.util.clip.RSObject;

/**
 * 
 * @author Joshua Barry
 * 
 */
public class GlobalObjectHandler {

	private static final Logger logger = Logger.getLogger(GlobalObjectHandler.class.getName());

	/**
	 * 
	 * @param player
	 * @param object
	 */
	public static void createGlobalObject(Player player, RSObject object) {
		for (Player players : World.getPlayers()) {
			if (players == null) {
				continue;
			}
			if (object.getPosition().isWithinDistance(player.getPosition())) {
				players.getActionSender().sendObject(object);
				logger.info("New object created at " + object.getPosition() + " by " + player.getUsername());
			}
		}
	}

	public static void createGlobalObject(Player player, CustomObject object) {
		for (Player players : World.getPlayers()) {
			if (players == null) {
				continue;
			}
			if (object.getPosition().isWithinDistance(player.getPosition())) {
				players.getActionSender().sendObject(object);
				logger.info("New object created at " + object.getPosition() + " by " + player.getUsername());
			}
		}
	}

	public static RSObject sendObjects() {
		return new RSObject(2106, 3234, 3229, 0, 0, 10);
	}
}
