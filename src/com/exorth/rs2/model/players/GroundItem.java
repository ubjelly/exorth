package com.exorth.rs2.model.players;

import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.region.Region;

public class GroundItem {

	private String owner; // Name of the holder of this ground item
	private Item item; // The item
	private Position pos; // Position of this ground item
	private boolean respawn; // If the item is respawnable
	private Region region; // The region this item is in.
	private boolean isGlobal; // If the item is global
	private int time; // The time how long the item is available until others to see / removed.

	// Creates a new ground item
	public GroundItem(String owner, Item item, Position pos, boolean respawn) {
		this.owner = owner;
		this.item = item;
		this.pos = pos;
		this.region = World.getInstance().getRegionManager().getRegionByLocation(this.pos);
		this.respawn = respawn;
	}

	public void setRespawn(boolean groundSpawn) {
		this.respawn = groundSpawn;
	}

	public boolean isRespawn() {
		return respawn;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Item getItem() {
		return item;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public String getOwner() {
		return owner;
	}

	public void setGlobal(boolean isGlobal) {
		this.isGlobal = isGlobal;
	}

	public boolean isGlobal() {
		return isGlobal;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public int getTime() {
		return time;
	}

	public void setPos(Position pos) {
		this.pos = pos;
	}

	public Position getPos() {
		return pos;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

	public Region getRegion() {
		return region;
	}

	@Override
	public String toString() {
		return "[GroundItem]: [Owner: " + this.owner + "] " + this.pos + " " + this.region;
	}
}
