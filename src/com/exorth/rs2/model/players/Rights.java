package com.exorth.rs2.model.players;

/**
 * 
 * @author Joshua Barry <Sneakyhearts>
 * TODO: Revise (Mark)
 */
public enum Rights {

	//TODO: Must make sure these correspond with @cr3@ in RSClient.java
	PLAYER, // Ordinal = 0 - The standard player.
	PREMIUM, // Ordinal = 1 - The Premium player.
	FORUM_MODERATOR, // Ordinal = 2 - The Forum moderators.
	PLAYER_MODERATOR, // Ordinal = 3 - The Player moderators.
	SERVER_ADMINISTRATOR, // Ordinal = 4 - The server administrators.
	ADMINISTRATOR("Stephen", "Exorth"); // Ordinal = 5 - The Administrators.
	/*
	PLAYER, // The standard player of the game.
	MODERATOR("Camper", "Synergy"), // The player moderators.
	LOCAL_MODERATOR("Pulse", "Moubile", "Anval"), // The server administrators.
	ADMINISTRATOR("Stephen", "Fabrice", "Lars", "Gug1st", "Masterlop"); // The owners of the server.
	*/


	//Group Members
	private String[] members;

	private Rights(String... members) {
		this.members = members;
	}

	public String[] getMembers() {
		return members;
	}

}
