package com.exorth.rs2.model.players;

import com.exorth.rs2.model.World;

/**
 * Sends a yell message across entire server.
 * @author Stephen
 *
 */
public class Yeller {

	/**
	 * Displays the inputted message across server.
	 * @param message The message you wish to display.
	 */
	public static void shout(String message) {
		for (Player p : World.getPlayers()) {
			if (p == null) {
				continue;
			}
			p.getActionSender().sendMessage(message);
		}
	}

	/**
	 * Alerts all staff with a message.
	 * @param message The message to the staff.
	 */
	public static void alertStaff(String message) {
		for (Player p : World.getPlayers()) {
			if (p == null) {
				continue;
			}
			if (p.getRights() >= 1)
				p.getActionSender().sendMessage(message);
		}
	}
}
