package com.exorth.rs2.model.players;

import com.exorth.rs2.model.items.Item;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.players.container.Container;
import com.exorth.rs2.model.players.container.ContainerType;
import com.exorth.rs2.util.Language;

public class Inventory {

	private Player player;
	public static final int SIZE = 28;
	public static final int DEFAULT_INVENTORY_INTERFACE = 3214;

	private Container container = new Container(ContainerType.STANDARD, SIZE);

	public Inventory(Player player) {
		this.player = player;
	}

	private int count;

	/**
	 * The number of items.
	 */
	public int getCount() {
		return count;
	}

	public void refresh() {
		Item[] inv = container.toArray();
		player.getActionSender().sendUpdateItems(DEFAULT_INVENTORY_INTERFACE, inv);
	}

	public void refresh(int inventoryId) {
		Item[] inv = container.toArray();
		player.getActionSender().sendUpdateItems(inventoryId, inv);
	}

	public void refresh(int slot, Item item) {
		player.getActionSender().sendUpdateItem(slot, DEFAULT_INVENTORY_INTERFACE, item);
	}

	public boolean addItem(Item item) {
		if (item == null) {
			return false;
		}
		if (!container.hasRoomFor(item)) {
			player.getActionSender().sendMessage(Language.NO_SPACE);
			return false;
		}
		container.add(new Item(item.getId(), item.getCount()));
		refresh();
		return true;
	}

	public void addItemToSlot(Item item, int slot) {
		if (item == null || slot == -1) {
			return;
		}
		container.set(slot, item);
		refresh(slot, item);
	}

	/**
	 * Removes the Item from the container
	 * @param item Item
	 * @return false if item does not exist
	 * TODO: Make it remove the item in the clicked slot, not the first slot the item occurs in the inventory
	 */
	public boolean removeItem(Item item) {
		if (item == null || item.getId() == -1) {
			return false;
		}
		if (!container.contains(item.getId())) {
			return false;
		}
		container.remove(item);
		refresh();
		return true;
	}

	public boolean removeItemSlot(int slot) {
		if (slot == -1) {
			return false;
		}
		if (container.get(slot) == null) {
			return false;
		}
		container.remove(container.get(slot), slot);
		refresh(slot, null);
		return true;
	}
	
	public boolean removeItemSlot(int slot, int amount) {
		if (slot == -1) {
			return false;
		}
		if (container.get(slot) == null) {
			return false;
		}
		Item removalItem = new Item(container.get(slot).getId(), amount);
		container.remove(removalItem, slot);
		refresh(slot, null);
		return true;
	}

	/**
	 * 
	 * @param items
	 * @param removeItems if you wanna delete them, otherwise just check if they
	 *			exist
	 * @return
	 */
	public boolean removeItems(int[] items, boolean removeItems) {
		int[] itemsToRemove = new int[items.length];

		for (int i = 0; i < items.length; i += 2) {
			if (getItemContainer().getCount(items[i]) < items[i + 1]) {
				String itemName = ItemManager.getInstance().getItemName(items[i]).toLowerCase();
				player.getActionSender().sendMessage("You do not have enough " + itemName + ".");
				return false;
			}

			itemsToRemove[i] = items[i];
			itemsToRemove[i + 1] = items[i + 1];
		}

		if (!removeItems) {
			return true;
		}

		for (int i = 0; i < itemsToRemove.length; i += 2) {
			if (itemsToRemove[i] > 0) {
				removeItem(new Item(itemsToRemove[i], itemsToRemove[i + 1]));
			}
		}
		return true;
	}

	public void swap(int fromSlot, int toSlot) {
		container.swap(fromSlot, toSlot);
		refresh();
	}

	public boolean hasRoomFor(Item item) {
		return container.hasRoomFor(item);
	}

	//TODO: Create hasItemInInventory() method

	/**
	 * Gets the item from inventory container
	 * 
	 * @param slot The slot
	 * @return The item, or <code>null</code> if it could not be found.
	 */
	public Item get(int slot) {
		return container.get(slot);
	}

	public Container getItemContainer() {
		return container;
	}
}
