package com.exorth.rs2.model.players.container;

/**
 * The type of container.
 * 
 * @author Graham Edgecombe
 * 
 */
public enum ContainerType {

	/**
	 * A standard container such as inventory.
	 */
	STANDARD,

	/**
	 * A container which always stacks, e.g. the bank, regardless of the item.
	 */
	ALWAYS_STACK,

	/**
	 * A container which never stacks, e.g. items on death, regardless of the
	 * item.
	 */
	NEVER_STACK;

}