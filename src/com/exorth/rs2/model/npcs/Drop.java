package com.exorth.rs2.model.npcs;

public class Drop {

	private int itemID;
	private int amount;
	private double dropChance;

	public Drop(int itemID, int amount, double dropChance) {
		this.itemID = itemID;
		this.amount = amount;
		this.dropChance = dropChance;
	}

	public int getItemID() {
		return itemID;
	}

	public int getAmount() {
		return amount;
	}

	public double getDropChance() {
		return dropChance;
	}
}
