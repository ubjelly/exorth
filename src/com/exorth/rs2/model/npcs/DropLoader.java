package com.exorth.rs2.model.npcs;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.exorth.rs2.Constants;

/**
 * Handles npc drops loaded via xml.
 * @author Stephen
 * @author unitHAF
 */
public class DropLoader {

	public static HashMap<Integer, List<Drop>> npcDrops = new HashMap<Integer, List<Drop>>();
	private final static Logger logger = Logger.getLogger(DropLoader.class.getName());

	public static void loadDrops() {
		try {
			File dropFile = new File(Constants.CONFIG_DIRECTORY + "npcs/npcdrops.xml");	
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbFactory.newDocumentBuilder();
			Document doc = db.parse(dropFile);

			doc.getDocumentElement().normalize();			 
			NodeList nodeList = doc.getElementsByTagName("npc");

			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				/** NPC **/
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					List<Drop> drops = new ArrayList<Drop>();
					Element element = (Element) node;
					int npcId = Integer.parseInt(element.getAttribute("npcid"));
					NodeList dropsNode = node.getChildNodes();
					/** DROPS **/
					for (int j = 0; j < dropsNode.getLength(); j++) {
						Node dropNode = dropsNode.item(j);
						if (dropNode.getNodeType() == Node.ELEMENT_NODE && dropNode.getNodeName().equals("drop")) {
							Element drop = (Element) dropNode;
							int itemId = Integer.parseInt(drop.getChildNodes().item(1).getTextContent());
							int itemAmount = Integer.parseInt(drop.getChildNodes().item(3).getTextContent());
							double itemPercent = Double.parseDouble(drop.getChildNodes().item(5).getTextContent());
							drops.add(new Drop(itemId, itemAmount, itemPercent));
						}
					}
					npcDrops.put(npcId, drops);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.severe("Unable to load drops!");
		}
	}
}