package com.exorth.rs2.model.npcs;

import com.exorth.rs2.content.combat.Combat;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.task.impl.DeathTask;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.clip.PathFinder;

/**
 * A non-player-character.
 * 
 * // Timers: Hidden = how long does it to perform actions such as drop loot //
 * Respawn = how long does it take to respawn this npc again to its //
 * spawnPosition
 */
public class Npc extends Entity {

	private NpcDefinition definition;

	// These are used for npc spawn loader
	private int npcId;
	// Most top right coord -- If WalkType is WALK
	private Position maxWalk;
	// The spawn pos -- Most bottom left coord, if you're using WalkType Walk
	// and setting maxium walking area to most top right pos.
	private Position spawnPosition;
	// The face direction upon spawning --- if WalkType is STAND -- 0 North, 1
	// East, 2 South, 3 West
	private int faceType;

	// The walking type, is the npc allowed to walk?
	private WalkType walkType = WalkType.STAND;

	/**
	 * Create a new Npc
	 */
	public Npc(int npcId) {

		// Grab the npc definition via NPC_ID and initialize the attributes
		this.definition = NpcLoader.getDefinition(npcId);
		initAttributes();

		this.npcId = npcId;

		// Update the appearance
		getUpdateFlags().setUpdateRequired(true);

		// Set the npc's hp
		setAttribute("current_hp", getDefinition().getCombatLevel(3));
	}

	/**
	 * Call this after World.register(npc)
	 */
	public void actions_after_spawn() {
		// Facing.
		if (walkType == WalkType.STAND)
			getUpdateFlags().sendFaceToDirection(new Position(getSpawnPosition().getX() + (faceType == 1 ? 1 : (faceType == 3 ? -1 : 0)), getSpawnPosition().getY() + (faceType == 0 ? 1 : (faceType == 2 ? -1 : 0))));
	}

	@Override
	public void initAttributes() {
		
	}

	@Override
	public void process() {
		if (getWalkType() == WalkType.WALK && getAttribute("IS_BUSY") == null) {
			if (Misc.random(10) == 0) {
				int x = spawnPosition.getX() + Misc.random(1 + maxWalk.getX());
				int y = spawnPosition.getY() + Misc.random(1 + maxWalk.getY());
				PathFinder.getSingleton().findRoute(this, x, y, true, 1, 1);
			}
		}

		getMovementHandler().process();
		Combat.getInstance().combatTick(this);
	}

	@Override
	public void reset() {
		getUpdateFlags().reset();
		removeAttribute("transform_update");
		removeAttribute("primary_dir");
	}

	@Override
	public void hit(int damage, int hitType, boolean isSpecial) {
		if (getAttribute("isDead") != null && !isSpecial) {
			return;
		}

		if (damage > (Integer) getAttribute("current_hp")) {
			damage = (Integer) getAttribute("current_hp");
		}

		setAttribute("current_hp", ((Integer) getAttribute("current_hp") - damage));

		if (!getUpdateFlags().isHitUpdate()) {
			getUpdateFlags().setDamage(damage);
			getUpdateFlags().setHitType(hitType);
			getUpdateFlags().setHitUpdate(true);
		} else {
			getUpdateFlags().setDamage2(damage);
			getUpdateFlags().setHitType2(hitType);
			getUpdateFlags().setHitUpdate2(true);
		}

		setHitType(hitType);

		if (getAttribute("isDead") == null && (Integer) getAttribute("current_hp") <= 0) {
			setAttribute("isDead", (byte) 0);
			World.submit(new DeathTask(this));
		}
	}

	public void sendTransform(int transformId) {
		// Flag the update
		setAttribute("transform_update", 0);
		setAttribute("transform_id", transformId);
		setNpcId(transformId);
		getUpdateFlags().setUpdateRequired(true);
	}

	public Position getMaxWalkingArea() {
		return maxWalk;
	}

	public void setMaxWalkingArea(Position maxWalk) {
		this.maxWalk = maxWalk;
	}

	public WalkType getWalkType() {
		return walkType;
	}

	public void setWalkType(WalkType walkType) {
		this.walkType = walkType;
	}

	public NpcDefinition getDefinition() {
		return definition;
	}

	public void setSpawnPosition(Position pos) {
		this.spawnPosition = pos;
		this.getPosition().setAs(pos);
	}

	public Position getSpawnPosition() {
		return spawnPosition;
	}

	public enum WalkType {
		STAND, WALK
	}

	public void setNpcId(int npcId) {
		this.npcId = npcId;
	}

	public int getNpcId() {
		return npcId;
	}

	public void setFaceType(int faceType) {
		this.faceType = faceType;
	}

	public int getFacingDirection() {
		return faceType;
	}

	@Override
	public Skill getSkill() {
		return null;
	}
}
