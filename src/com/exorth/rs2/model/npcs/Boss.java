package com.exorth.rs2.model.npcs;

/**
 * Contains all information pertaining to bosses.
 * @author Stephen
 *
 */
public class Boss {

	/**
	 * Temporary
	 */
	private int[] bossId = {1125, 35, 1198, 930, 50, 1851, 795, 1246, 1610, 4510};
	private String[] bossName = {"Dad", "Warrior", "Dire Wolf", "Ungadulu", "King Black Dragon", "Arzinian Avatar of Strength", "Ice Queen", "Riyl Shade", "Gargoyle", "Me"};

	private int npcId;

	public Boss(int npcId) {
		this.npcId = npcId;
	}

	/**
	 * Checks to see if the NPC is a boss;
	 * @return
	 */
	public boolean isBoss() {
		for (int i = 0; i < bossId.length; i++) {
			if (npcId == bossId[i]) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Gets the appropriate name for the npcId
	 * @return the name of the boss
	 */
	public String getName() {
		String name = "";
		for (int i=0; i < bossId.length; i++) {
			if (npcId == bossId[i]) {
				name = bossName[i];
			}
		}
		return name;
	}
}
