package com.exorth.rs2.model.region;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.exorth.rs2.model.Entity;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;

/**
 * Manages the world regions.
 * 
 * @author Graham Edgecombe
 * 
 */
public class RegionManager {

	/**
	 * The region size.
	 */
	public static final int REGION_SIZE = 32;

	/**
	 * The active (loaded) region map.
	 */
	private Map<RegionCoordinates, Region> activeRegions = new HashMap<RegionCoordinates, Region>();

	/**
	 * Gets a local game object.
	 * 
	 * @param location The object's location.
	 * @param id The object's id.
	 * @return The <code>GameObject</code> or <code>null</code> if no game
	 *		 object was found to be existent.
	 */
	/*
	 * public GameObject getGameObject(Location location, int id) { Region[]
	 * regions = getSurroundingRegions(location); for (Region region : regions)
	 * { for (GameObject object : region.getGameObjects()) { if
	 * (object.getLocation().equals(location) && object.getDefinition().getId()
	 * == id) { return object; } } } return null; }
	 */

	/**
	 * Gets the local mobs around an entity.
	 * 
	 * @param mob The entity.
	 * @return The collection of local mobs.
	 */
	public Collection<Entity> getLocalMobs(Entity mob) {
		List<Entity> localMobs = new LinkedList<Entity>();
		Region[] regions = getSurroundingRegions(mob.getPosition());
		for (Region region : regions) {
			for (Entity mobs : region.getMobs()) {
				if (mobs.getPosition().isWithinDistance(mob.getPosition())) {
					localMobs.add(mobs);
				}
			}
		}
		return Collections.unmodifiableCollection(localMobs);
	}

	/**
	 * Gets the local NPCs around an entity.
	 * 
	 * @param mob The entity.
	 * @return The collection of local NPCs.
	 */
	public Collection<Npc> getLocalNpcs(Entity mob) {
		List<Npc> localNpcs = new LinkedList<Npc>();
		Region[] regions = getSurroundingRegions(mob.getPosition());
		for (Region region : regions) {
			for (Entity entity : region.getMobs()) {
				if (entity.isNpc()) {
					Npc npc = (Npc) entity;
					if (npc.getPosition().isWithinDistance(mob.getPosition())) {
						localNpcs.add(npc);
					}
				}
			}
		}
		return Collections.unmodifiableCollection(localNpcs);
	}

	/**
	 * Gets the local players around an entity.
	 * 
	 * @param mob The entity.
	 * @return The collection of local players.
	 */
	public Collection<Player> getLocalPlayers(Entity mob) {
		List<Player> localPlayers = new LinkedList<Player>();
		Region[] regions = getSurroundingRegions(mob.getPosition());
		for (Region region : regions) {
			for (Entity entity : region.getMobs()) {
				if (entity.isPlayer()) {
					Player player = (Player) entity;
					if (player.getPosition().isWithinDistance(mob.getPosition())) {
						localPlayers.add(player);
					}
				}
			}
		}
		return Collections.unmodifiableCollection(localPlayers);
	}

	/**
	 * Gets a region by its x and y coordinates.
	 * 
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 * @return The region.
	 */
	public Region getRegion(int x, int y) {
		RegionCoordinates key = new RegionCoordinates(x, y);
		if (activeRegions.containsKey(key)) {
			return activeRegions.get(key);
		} else {
			Region region = new Region(key);
			activeRegions.put(key, region);
			return region;
		}
	}

	/**
	 * Gets a region by location.
	 * 
	 * @param location The location.
	 * @return The region.
	 */
	public Region getRegionByLocation(Position location) {
		return getRegion(location.getX() / REGION_SIZE, location.getY() / REGION_SIZE);
	}

	/**
	 * Gets the regions surrounding a location.
	 * 
	 * @param location The location.
	 * @return The regions surrounding the location.
	 */
	public Region[] getSurroundingRegions(Position location) {
		int regionX = location.getX() / REGION_SIZE;
		int regionY = location.getY() / REGION_SIZE;

		Region[] surrounding = new Region[9];
		surrounding[0] = getRegion(regionX, regionY);
		surrounding[1] = getRegion(regionX - 1, regionY - 1);
		surrounding[2] = getRegion(regionX + 1, regionY + 1);
		surrounding[3] = getRegion(regionX - 1, regionY);
		surrounding[4] = getRegion(regionX, regionY - 1);
		surrounding[5] = getRegion(regionX + 1, regionY);
		surrounding[6] = getRegion(regionX, regionY + 1);
		surrounding[7] = getRegion(regionX - 1, regionY + 1);
		surrounding[8] = getRegion(regionX + 1, regionY - 1);
		return surrounding;
	}
}
