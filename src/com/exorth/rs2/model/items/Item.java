package com.exorth.rs2.model.items;

import com.exorth.rs2.model.items.ItemManager.ItemDefinition;

/**
 * Represents a single item.
 * 
 * @author Graham Edgecombe
 * 
 */
public class Item {

	public static int MAX_AMOUNT = 2147483647;

	/**
	 * The id.
	 */
	private short id;

	/**
	 * The number of items.
	 */
	private int count;

	/**
	 * Creates a single item.
	 * 
	 * @param id The id.
	 */
	public Item(int id) {
		this(id, 1);
	}

	/**
	 * Sets the count of the item.
	 * 
	 * @param count The count to set to.
	 */
	public void setCount(int count) {
		this.count = count;
	}

	/**
	 * Creates a stacked item.
	 * 
	 * @param id The id.
	 * @param count The number of items.
	 * @throws IllegalArgumentException if count is negative.
	 */
	public Item(int id, int count) {
		if (count < 0) {
			throw new IllegalArgumentException("Count cannot be negative.");
		}
		this.id = (short) id;
		this.count = count;
	}

	/**
	 * Gets the item id.
	 * 
	 * @return The item id.
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the count.
	 * 
	 * @return The count.
	 */
	public int getCount() {
		return count;
	}

	/**
	 * Gets the item's definition.
	 * 
	 * @return The definition of this item.
	 */
	public ItemDefinition getDefinition() {
		return ItemManager.getInstance().getItemDefinitions()[id];
	}

	/**
	 * Gets the item's name. 
	 * 
	 * @return The item name.
	 */
	public String getName() {
		return ItemManager.getInstance().getItemDefinitions()[id].getName();
	}

	@Override
	public String toString() {
		return "Item (ID: " + id + ", Count: " + count + ").";
	}
}
