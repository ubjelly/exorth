package com.exorth.rs2.model.object;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.exorth.rs2.io.XStreamController;
import com.exorth.rs2.model.Position;
import com.exorth.rs2.model.World;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.clip.RSObject;
import com.exorth.rs2.util.clip.RegionClipping;

public class CustomObjectManager {

	private static List<CustomObject> objects = new ArrayList<CustomObject>();
	private static final Logger logger = Logger.getAnonymousLogger();

	@SuppressWarnings("unchecked")
	public static void loadObjects() throws FileNotFoundException {
		logger.info("Loading global objects...");
		List<CustomObject> list = (List<CustomObject>) XStreamController.getXStream().fromXML(new FileInputStream("./config/content/objects.xml"));
		for (CustomObject CustomObject : list) {
			objects.add(CustomObject);
		}
		logger.info("Loaded " + list.size() + " objects.");
	}

	/**
	 * Creates an object for everyone online.
	 * 
	 * @param player
	 */
	public static void createGlobalObject(Player player) {
		for (Player players : World.getPlayers()) {
			if (players == null) {
				continue;
			}
			for (CustomObject object : objects) {
				if (Misc.getDistance(players.getPosition(), object.getPosition()) <= 60) {
					players.getActionSender().sendObject(object);
					RegionClipping.addRSObject(player, object.getPosition().getX(), object.getPosition().getY(), object.getPosition().getZ(), new RSObject(object.getId(), object.getPosition().getX(), object.getPosition().getY(), object.getPosition().getZ(), object.getFace(), object.getType()));
					RegionClipping.addObject(player, object.getId(), object.getPosition().getX(), object.getPosition().getY(), object.getPosition().getZ(), object.getType(), object.getFace());
				}
			}
		}
	}
	
	/**
	 * Creates a global object.
	 * @param object The object to create.
	 */
	public static void createGlobalObject(RSObject object) {
		for (Player player : World.getPlayers()) {
			if (player == null) continue;
			
			if (Misc.getDistance(player.getPosition(), object.getPosition()) <= 60) {
				player.getActionSender().sendObject(object);
				RegionClipping.addRSObject(player, object.getPosition().getX(), object.getPosition().getY(), object.getPosition().getZ(), object);
				RegionClipping.addObject(player, object.getId(), object.getPosition().getX(), object.getPosition().getY(), object.getPosition().getZ(), object.getType(), object.getFace());
			}
		}
		
	}
	
	/**
	 * Removes a global object.
	 * @param object The object to remove.
	 */
	public static void removeGlobalObject(Position position) {
		for (Player player : World.getPlayers()) {
			if (player == null) continue;
			
			player.getActionSender().removeObject(position);
		}
	}

	/**
	 * Creates an object just for yourself.
	 * Specified the object id, position, face, and type
	 * @param player
	 */
	public static void createObject(Player player, int id, Position position, int face, int type) {
		player.getActionSender().sendObject(new CustomObject(id, position, face, type));
	}
	
	/**
	 * Removes an object for the specified player.
	 * @param player The player to remove the object for.
	 * @param position The position of the object.
	 */
	public static void removeObject(Player player, Position position) {
		player.getActionSender().removeObject(position);
	}

	public static void setObjects(List<CustomObject> objects) {
		CustomObjectManager.objects = objects;
	}

	public static List<CustomObject> getObjects() {
		return objects;
	}
}
