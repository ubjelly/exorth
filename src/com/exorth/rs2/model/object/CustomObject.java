package com.exorth.rs2.model.object;

import com.exorth.rs2.model.Position;

public class CustomObject {

	private int id;
	private Position position;
	private int face;
	private int type;

	/**
	 * The maximum amount of health this object has (for trees).
	 */
	private short maxHealth;

	/**
	 * The current health this object has remaining (for trees).
	 */
	private short currentHealth;

	public CustomObject(int id, Position position, int face, int type) {
		this.id = id;
		this.position = position;
		this.face = face;
		this.type = type;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Position getPosition() {
		return position;
	}

	public void setFace(int face) {
		this.face = face;
	}

	public int getFace() {
		return face;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getType() {
		return type;
	}

	/**
	 * @return the maxHealth
	 */
	public int getMaxHealth() {
		return maxHealth;
	}

	/**
	 * @param maxHealth the maxHealth to set
	 */
	public void setMaxHealth(int maxHealth) {
		this.maxHealth = (short) maxHealth;
		this.currentHealth = (short) maxHealth;
	}

	/**
	 * @return the currentHealth
	 */
	public int getCurrentHealth() {
		return currentHealth;
	}

	/**
	 * @param currentHealth the currentHealth to set
	 */
	public void setCurrentHealth(int currentHealth) {
		this.currentHealth = (short) currentHealth;
	}

	/**
	 * @param currentHealth the currentHealth to set
	 */
	public void decreaseCurrentHealth(int amount) {
		this.currentHealth -= amount;
	}
}
