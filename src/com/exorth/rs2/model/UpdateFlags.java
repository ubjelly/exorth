package com.exorth.rs2.model;

public class UpdateFlags {

	private boolean isUpdateRequired;
	private boolean chatUpdateRequired;
	private boolean isForceChatUpdate;
	private String forceChatMessage;
	private boolean graphicsUpdateRequired;
	private int graphicsId;
	private int graphicsDelay;
	private boolean animationUpdateRequired;
	private int animationId;
	private int animationDelay;
	private boolean entityFaceUpdate;
	private int entityFaceIndex = -1;
	private boolean faceToDirection;
	private Position face;
	private int faceDirection;
	private boolean hitUpdate;
	private boolean forceMovementUpdate;
	private boolean hitUpdate2;
	private int damage;
	private int damage2;
	private int hitType;
	private int hitType2;
	private int forceMovementEndX, forceMovementEndY, forceMovementSpeed1, forceMovementSpeed2, forceMovementDirection;

	/**
	 * 
	 * @param x The x you end to
	 * @param y The y you end to
	 * @param speed1 Speed1
	 * @param speed2 Speed2
	 * @param direction Direction you want face
	 */
	public void sendForceMovement(final int x, final int y, final int speed1, final int speed2, final int direction) {
		this.setForceMovementEndX(x);
		this.setForceMovementEndY(y);
		this.setForceMovementSpeed1(speed1);
		this.setForceMovementSpeed2(speed2);
		this.setForceMovementDirection(direction);
		forceMovementUpdate = true;
		isUpdateRequired = true;
	}

	public void resetForceMovement() {
		setForceMovementEndX(setForceMovementEndY(setForceMovementSpeed1(setForceMovementSpeed2(setForceMovementDirection(0)))));
	}

	public void sendGraphic(int graphicsId) {
		this.graphicsId = graphicsId;
		graphicsUpdateRequired = true;
		isUpdateRequired = true;
	}

	public void sendGraphic(UpdateFlags graphicsId) {
		graphicsUpdateRequired = true;
		isUpdateRequired = true;
	}

	public void sendGraphic(int graphicsId, int graphicsDelay) {
		this.graphicsId = graphicsId;
		this.graphicsDelay = graphicsDelay;
		graphicsUpdateRequired = true;
		isUpdateRequired = true;
	}

	public void sendGraphic(int graphicsId, int graphicsDelay, int height) {
		this.graphicsId = graphicsId;
		this.graphicsDelay = (65536 * height) + graphicsDelay;
		graphicsUpdateRequired = true;
		isUpdateRequired = true;
	}

	public void sendAnimation(int animationId) {
		this.animationId = animationId;
		this.animationDelay = 0;
		animationUpdateRequired = true;
		isUpdateRequired = true;
	}

	public void sendAnimation(UpdateFlags animation) {
		animationUpdateRequired = true;
		isUpdateRequired = true;
	}

	public void sendAnimation(int animationId, int animationDelay) {
		this.animationId = animationId;
		this.animationDelay = animationDelay;
		animationUpdateRequired = true;
		isUpdateRequired = true;
	}

	public void faceEntity(int entityFaceIndex) {
		this.entityFaceIndex = entityFaceIndex;
		entityFaceUpdate = true;
		isUpdateRequired = true;
	}

	public void sendFaceToDirection(Position face) {
		this.face = face;
		faceToDirection = true;
		isUpdateRequired = true;
	}

	public void sendHit(int damage, int hitType) {
		this.damage = damage;
		this.hitType = hitType;
		hitUpdate = true;
		isUpdateRequired = true;
	}

	public void sendForceMessage(String forceChatMessage) {
		this.forceChatMessage = forceChatMessage;
		isForceChatUpdate = true;
		isUpdateRequired = true;
	}

	public void reset() {
		isForceChatUpdate = false;
		chatUpdateRequired = false;
		graphicsUpdateRequired = false;
		animationUpdateRequired = false;
		entityFaceUpdate = false;
		faceToDirection = false;
		forceMovementUpdate = false;
		hitUpdate = false;
		hitUpdate2 = false;
	}

	public void setUpdateRequired(boolean isUpdateRequired) {
		this.isUpdateRequired = isUpdateRequired;
	}

	public boolean isUpdateRequired() {
		return isUpdateRequired;
	}

	public void setChatUpdateRequired(boolean chatUpdateRequired) {
		this.chatUpdateRequired = chatUpdateRequired;
	}

	public boolean isChatUpdateRequired() {
		return chatUpdateRequired;
	}

	public void setForceChatUpdate(boolean isForceChatUpdate) {
		this.isForceChatUpdate = isForceChatUpdate;
	}

	public boolean isForceChatUpdate() {
		return isForceChatUpdate;
	}

	public void setForceChatMessage(String forceChatMessage) {
		this.forceChatMessage = forceChatMessage;
	}

	public String getForceChatMessage() {
		return forceChatMessage;
	}

	public void setGraphicsUpdateRequired(boolean graphicsUpdateRequired) {
		this.graphicsUpdateRequired = graphicsUpdateRequired;
	}

	public boolean isGraphicsUpdateRequired() {
		return graphicsUpdateRequired;
	}

	public void setGraphicsId(int graphicsId) {
		this.graphicsId = graphicsId;
	}

	public int getGraphicsId() {
		return graphicsId;
	}

	public void setGraphicsDelay(int graphicsDelay) {
		this.graphicsDelay = graphicsDelay;
	}

	public int getGraphicsDelay() {
		return graphicsDelay;
	}

	public void setAnimationUpdateRequired(boolean animationUpdateRequired) {
		this.animationUpdateRequired = animationUpdateRequired;
	}

	public boolean isAnimationUpdateRequired() {
		return animationUpdateRequired;
	}

	public void setAnimationId(int animationId) {
		this.animationId = animationId;
	}

	public int getAnimationId() {
		return animationId;
	}

	public void setAnimationDelay(int animationDelay) {
		this.animationDelay = animationDelay;
	}

	public int getAnimationDelay() {
		return animationDelay;
	}

	public void setEntityFaceUpdate(boolean entityFaceUpdate) {
		this.entityFaceUpdate = entityFaceUpdate;
	}

	public boolean isEntityFaceUpdate() {
		return entityFaceUpdate;
	}

	public void setEntityFaceIndex(int entityFaceIndex) {
		this.entityFaceIndex = entityFaceIndex;
	}

	public int getEntityFaceIndex() {
		return entityFaceIndex;
	}

	public void setFaceToDirection(boolean faceToDirection) {
		this.faceToDirection = faceToDirection;
	}

	public boolean isFaceToDirection() {
		return faceToDirection;
	}

	public void setFace(Position face) {
		this.face = face;
	}

	public Position getFace() {
		return face;
	}

	public void setHitUpdate(boolean hitUpdate) {
		this.hitUpdate = hitUpdate;
	}

	public boolean isHitUpdate() {
		return hitUpdate;
	}

	public void setHitUpdate2(boolean hitUpdate2) {
		this.hitUpdate2 = hitUpdate2;
	}

	public boolean isForceMovementUpdateRequired() {
		return forceMovementUpdate;
	}

	public boolean isHitUpdate2() {
		return hitUpdate2;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage2(int damage2) {
		this.damage2 = damage2;
	}

	public int getDamage2() {
		return damage2;
	}

	public void setHitType(int hitType) {
		this.hitType = hitType;
	}

	public int getHitType() {
		return hitType;
	}

	public void setHitType2(int hitType2) {
		this.hitType2 = hitType2;
	}

	public int getHitType2() {
		return hitType2;
	}

	public void setFaceDirection(int faceDirection) {
		this.faceDirection = faceDirection;
	}

	public int getFaceDirection() {
		return faceDirection;
	}

	public void setForceMovementEndX(int forceMovementEndX) {
		this.forceMovementEndX = forceMovementEndX;
	}

	public int getForceMovementEndX() {
		return forceMovementEndX;
	}

	public int setForceMovementEndY(int forceMovementEndY) {
		this.forceMovementEndY = forceMovementEndY;
		return forceMovementEndY;
	}

	public int getForceMovementEndY() {
		return forceMovementEndY;
	}

	public int setForceMovementSpeed1(int forceMovementSpeed1) {
		this.forceMovementSpeed1 = forceMovementSpeed1;
		return forceMovementSpeed1;
	}

	public int getForceMovementSpeed1() {
		return forceMovementSpeed1;
	}

	public int setForceMovementSpeed2(int forceMovementSpeed2) {
		this.forceMovementSpeed2 = forceMovementSpeed2;
		return forceMovementSpeed2;
	}

	public int getForceMovementSpeed2() {
		return forceMovementSpeed2;
	}

	public int setForceMovementDirection(int forceMovementDirection) {
		this.forceMovementDirection = forceMovementDirection;
		return forceMovementDirection;
	}

	public int getForceMovementDirection() {
		return forceMovementDirection;
	}
}
