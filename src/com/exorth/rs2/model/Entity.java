package com.exorth.rs2.model;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import com.exorth.rs2.Constants;
import com.exorth.rs2.action.ActionDeque;
import com.exorth.rs2.content.combat.magic.Magic;
import com.exorth.rs2.content.combat.magic.SpellLoader;
import com.exorth.rs2.content.combat.util.CombatState;
import com.exorth.rs2.content.combat.util.Weapons;
import com.exorth.rs2.content.combat.util.CombatState.CombatStyle;
import com.exorth.rs2.content.skills.Skill;
import com.exorth.rs2.model.items.ItemManager;
import com.exorth.rs2.model.npcs.Npc;
import com.exorth.rs2.model.players.Player;
import com.exorth.rs2.model.region.Region;
import com.exorth.rs2.model.region.RegionCoordinates;
import com.exorth.rs2.task.Task;
import com.exorth.rs2.task.impl.FollowTask;
import com.exorth.rs2.task.impl.PoisonTask;
import com.exorth.rs2.util.Misc;
import com.exorth.rs2.util.clip.RegionClipping;

public abstract class Entity {

	private static final Logger logger = Logger.getAnonymousLogger();

	/**
	 * The last known map region.
	 */
	private Position lastKnownRegion = new Position(0, 0, 0);

	/**
	 * The current region.
	 */
	private Region currentRegion = new Region(new RegionCoordinates(0, 0));

	/**
	 * Adds this entity to the specified region.
	 * 
	 * @param region The region.
	 */
	public void addToRegion(Region region) {
		region.addMob(this);
	}

	/**
	 * Gets the center location of the entity.
	 * 
	 * @return The center location of the entity.
	 */
	public Position getCentreLocation() {
		return new Position(getPosition().getX() + (int) (this.isNpc() ? Math.floor(((Npc) this).getDefinition().getSize() / 2) : 0), getPosition().getY() + (int) (this.isNpc() ? Math.floor(((Npc) this).getDefinition().getSize() / 2) : 0), getPosition().getZ());
	}

	public void removeFromRegion(Region region) {
		region.removeMob(this);
	}

	private MovementHandler movementHandler = new MovementHandler(this);

	// Entity's client index
	private int index = -1;

	/**
	 * A queue of actions.
	 */
	private final ActionDeque actionDeque = new ActionDeque(this);

	private Entity interactingEntity; // E.g talking with entity (npc)
	private Entity combatingEntity; // Combating with entity
	private Entity followingEntity; // Following with entity
	private Entity target; // Target in combat

	private boolean canWalk = true; // TODO: attr
	private boolean instigatingAttack;

	private boolean isPoisoned; // TODO attrs
	private int poisonDamage;
	private int poisonedTimer;
	private int poisonHitTimer;
	private int poisonImmunityTimer;
	private int fireImmunityTimer;

	private int hitType;

	private int combatTimer;
	private int attackTimer;

	/**
	 * The poisoning task.
	 */
	private Task poisonTask;

	/**
	 * The following task.
	 */
	private Task followTask;

	/**
	 * The permanent attributes map. Items set here are only removed when told
	 * to.
	 */
	private Map<String, Object> attributes = new HashMap<String, Object>();

	/**
	 * The position.
	 */
	private Position position = new Position(Constants.START_X, Constants.START_Y, Constants.START_Z, this);

	/**
	 * The updateflags.
	 */
	private UpdateFlags updateFlags = new UpdateFlags();

	/**
	 * The attack types.
	 */
	private AttackTypes attackType = AttackTypes.MELEE;

	// Abstract methods...
	public abstract Skill getSkill();
	public abstract void reset();
	public abstract void initAttributes();
	public abstract void process();
	public abstract void hit(int damage, int hitType, boolean isSpecial);

	/**
	 * Combat state
	 */
	private final CombatState combateState = new CombatState();

	/**
	 * The player.
	 * 
	 * @return The player.
	 */
	public boolean isPlayer() {
		return (this instanceof Player);
	}

	/**
	 * The npc.
	 * 
	 * @return The npc.
	 */
	public boolean isNpc() {
		return (this instanceof Npc);
	}

	/**
	 * Grabs the hit timer.
	 * 
	 * @return The hit timer.
	 */
	public int grabHitTimer() {
		if (isPlayer()) {
			Player player = (Player) this;
			if (player.getAttackType() == AttackTypes.MAGIC) {
				return 5;
			} else if (player.getAttackType() == AttackTypes.RANGED) {
				return Weapons.getWeaponSpeed(player) - (player.getCombatState().getCombatStyle().equals(CombatStyle.AGGRESSIVE) ? 1 : 0);
			} else if (player.getAttackType() == AttackTypes.MELEE) {
				return Weapons.getWeaponSpeed(player);
			}
		} else {
			return ((Npc) this).getDefinition().getAttackSpeed();
		}
		return 1;
	}

	/**
	 * Grabs the attack animation.
	 * 
	 * @return The attack animation.
	 */
	public int grabAttackAnimation() {
		if (isPlayer()) {
			Player player = (Player) this;

			if (player.getAttackType() == AttackTypes.MAGIC) {
				return SpellLoader.getSpellDefinitions()[Magic.getInstance().getMagicIndex(player)].getAnimationId();
			} else {
				return Weapons.getAttackAnimation(player);
			}
		} else {
			return ((Npc) this).getDefinition().getAttackAnim();
		}
	}

	/**
	 * Grabs the defence animation.
	 * 
	 * @return The defence animation.
	 */
	public int grabDefenceAnimation() {
		if (isPlayer()) {
			return Weapons.getBlockAnimation((Player) this);
		} else {
			return ((Npc) this).getDefinition().getDefenceAnim();
		}
	}

	/**
	 * Handles the auto retaliation.
	 * 
	 * @return The auto retaliate...
	 */
	public boolean handleAutoRetaliate() {
		if (isPlayer()) {
			return ((Player) this).shouldAutoRetaliate() && ((Player) this).getMovementHandler().isEmpty() && ((Player) this).getAttribute("primary_dir") == null && ((Player) this).getAttribute("secondary_dir") == null;
		} else {
			return true;
		}
	}

	/**
	 * TODO: Finish this.
	 * 
	 * @return True if the npc is a demon.
	 */
	public boolean isDemon() {
		if (isNpc()) {
			if (((Npc) this).getDefinition().getName().toLowerCase().equals("demon")) {
				return true;
			}
		}
		return false;
	}

	public int getSkillLevel(int skillId) {
		if (isPlayer()) {
			if (skillId != Skill.PRAYER) {
				return getSkill().getLevel()[skillId];
			} else {
				return getSkill().getLevelForXP(getSkill().getExp()[5]);
			}
		} else {
			return ((Npc) this).getDefinition().getCombatLevel(skillId);
		}
	}

	public int getBonus(int bonusId) {
		if (isPlayer()) {
			return ((Player) this).getBonuses().get(bonusId);
		} else {
			return (((Npc) this).getDefinition().getBonuses() == null ? 0 : ((Npc) this).getDefinition().getBonuses()[-5 + bonusId]);
		}
	}

	public void teleport(Position position) {
		int old_z = getPosition().getZ();

		getPosition().setAs(position);

		if (this.isPlayer()) {
			final Player player = (Player) this;
			final int deltaX = getPosition().getX() - getLastKnownRegion().getRegionX() * 8;
			final int deltaY = getPosition().getY() - getLastKnownRegion().getRegionY() * 8;
			if (deltaX < 16 || deltaX >= 88 || deltaY < 16 || deltaY > 88) {
				player.getActionSender().sendMapRegion();
			}
			player.stopAllActions(true);
			player.setResetMovementQueue(true);
			player.setNeedsPlacement(true);
			player.getActionSender().removeInterfaces();
			player.getActionSender().sendDetails();
			// Refreshing ground items on height update..
			if (old_z != position.getZ()) {
				World.submit(new Task(true) {
					@Override
					protected void execute() {
						ItemManager.getInstance().loadOnRegion(player);
						this.stop();
					}
				});
			}
		}
	}

	/**
	 * Teleport clipped. WARNING: Take the most BOTTOM Y and most LEFT X COORD
	 * and apply x length as width to right and up
	 * 
	 * @param position -- Most bottom left corner
	 * @param size
	 * @param xlength - max X distance
	 * @param ylength - max Y distance
	 */
	public void clipTeleport(Position position, int size, int xlength, int ylength) {
		int x = position.getX();
		int y = position.getY();
		int z = position.getZ();

		int teleX = 0, teleY = 0;

		if (xlength == 0 && ylength == 0) {
			xlength = size;
			ylength = size;
		}

		teleX = x + Misc.random(xlength + 1);
		teleY = y + Misc.random(ylength + 1);

		if (size != 1) {
			for (;;) {
				if (RegionClipping.getClipping(this, teleX, teleY, z) == 0) {
					break;
				} else {
					teleX = x + Misc.random(xlength + 1);
					teleY = y + Misc.random(ylength + 1);
					continue;
				}
			}
		} else if (RegionClipping.getClipping(this, x, y, z) != 0) {
			logger.severe("clipTeleport error with size 1, NON_WALKABLE_TILE");
			return;
		}
		teleport(new Position(teleX, teleY, z));
	}

	public void clipTeleport(Position position, int size) {
		clipTeleport(position, size, 0, 0);
	}

	public void clipTeleport(int x, int y, int z, int size) {
		clipTeleport(new Position(x, y, z), size);
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}

	public void setInteractingEntity(Entity interactingEntity) {
		this.interactingEntity = interactingEntity;
	}

	public Entity getInteractingEntity() {
		return interactingEntity;
	}

	public String getName() {
		String name = ((Npc) this).getDefinition().getName();
		return name;
	}

	public Position getPosition() {
		return position;
	}

	public UpdateFlags getUpdateFlags() {
		return updateFlags;
	}

	/**
	 * Gets an attribute.<br />
	 * WARNING: unchecked cast, be careful!
	 * 
	 * @param <T> The type of the value.
	 * @param key The key.
	 * @return The value.
	 */
	@SuppressWarnings("unchecked")
	public <T> T getAttribute(String key) {
		return (T) attributes.get(key);
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	/**
	 * Sets an attribute.<br />
	 * WARNING: unchecked cast, be careful!
	 * 
	 * @param <T> The type of the value.
	 * @param key The key.
	 * @param value The value.
	 * @return The old value.
	 */
	@SuppressWarnings("unchecked")
	public <T> T setAttribute(String key, T value) {
		return (T) attributes.put(key, value);
	}

	/**
	 * Removes an attribute.<br />
	 * WARNING: unchecked cast, be careful!
	 * 
	 * @param <T> The type of the value.
	 * @param key The key.
	 * @return The old value.
	 */
	@SuppressWarnings("unchecked")
	public <T> T removeAttribute(String key) {
		return (T) attributes.remove(key);
	}

	/**
	 * Gets the action queue.
	 * 
	 * @return The action queue.
	 */
	public ActionDeque getActionDeque() {
		return actionDeque;
	}

	public void setHitType(int hitType) {
		this.hitType = hitType;
	}

	public int getHitType() {
		return hitType;
	}

	public void setCombatTimer(int combatTimer) {
		this.combatTimer = combatTimer;
	}

	public int getCombatTimer() {
		return combatTimer;
	}

	public void setAttackTimer(int attackTimer) {
		this.attackTimer = attackTimer;
	}

	public int getAttackTimer() {
		return attackTimer;
	}

	public boolean isInstigatingAttack() {
		return instigatingAttack;
	}

	public void setInstigatingAttack(boolean instigatingAttack) {
		this.instigatingAttack = instigatingAttack;
	}

	public void setCombatingEntity(Entity combatingEntity) {
		this.combatingEntity = combatingEntity;
	}

	public Entity getCombatingEntity() {
		return combatingEntity;
	}

	public void setTarget(Entity target) {
		this.target = target;
	}

	public Entity getTarget() {
		return target;
	}

	public boolean isInCombat() {
		return getCombatTimer() != 0 && getCombatingEntity() != null;
	}

	public void setPoisoned(boolean isPoisoned) {
		if (isPoisoned) {
			setPoisonedTimer(getPoisonedTimer());
			if (getPoisonTask() == null) {
				setPoisonTask(new PoisonTask(this));
				World.submit(getPoisonTask());
			}
		} else {
			if (getPoisonTask() != null) {
				getPoisonTask().stop();
				setPoisonTask(null);
				setPoisonedTimer(0);
				setPoisonDamage(0);
				setPoisonHitTimer(0);
			}
		}
		this.isPoisoned = isPoisoned;
	}

	public boolean isPoisoned() {
		return isPoisoned;
	}

	public void setPoisonedTimer(int poisonedTimer) {
		this.poisonedTimer = poisonedTimer;
	}

	public int getPoisonedTimer() {
		return poisonedTimer;
	}

	public void setPoisonHitTimer(int poisonHitTimer) {
		this.poisonHitTimer = poisonHitTimer;
	}

	public int getPoisonHitTimer() {
		return poisonHitTimer;
	}

	public void setPoisonDamage(int poisonDamage) {
		this.poisonDamage = poisonDamage;
	}

	public int getPoisonDamage() {
		return poisonDamage;
	}

	public void setPoisonImmunityTimer(int poisonImmunityTimer) {
		this.poisonImmunityTimer = poisonImmunityTimer;
	}

	public int getPoisonImmunityTimer() {
		return poisonImmunityTimer;
	}

	public void setFireImmunityTimer(int fireImmunityTimer) {
		this.fireImmunityTimer = fireImmunityTimer;
	}

	public int getFireImmunityTimer() {
		return fireImmunityTimer;
	}

	public void setAttackType(AttackTypes attackType) {
		this.attackType = attackType;
	}

	public AttackTypes getAttackType() {
		return attackType;
	}

	//TODO: Fix combat dancing.
	public void setFollowingEntity(Entity followingEntity) {
		if (followingEntity != null) {
			if (getFollowTask() == null) {
				setFollowTask(new FollowTask(this));
				World.submit(getFollowTask());
			}
		} else {
			if (getFollowTask() != null) {
				getFollowTask().stop();
				setFollowTask(null);
			}
		}
		this.followingEntity = followingEntity;
	}

	public Entity getFollowingEntity() {
		return followingEntity;
	}

	public enum AttackTypes {
		MELEE, RANGED, MAGIC, SPECIAL
	}

	public CombatState getCombatState() {
		return combateState;
	}

	public void setPoisonTask(Task poisonTask) {
		this.poisonTask = poisonTask;
	}

	public Task getPoisonTask() {
		return poisonTask;
	}

	public void setFollowTask(Task followTask) {
		this.followTask = followTask;
	}

	public Task getFollowTask() {
		return followTask;
	}

	public void setCanWalk(boolean canWalk) {
		this.canWalk = canWalk;
	}

	public boolean canWalk() {
		return canWalk;
	}

	public MovementHandler getMovementHandler() {
		return movementHandler;
	}

	public void setRegion(Region currentRegion) {
		this.currentRegion = currentRegion;
	}

	public Region getRegion() {
		return currentRegion;
	}

	public Position getLastKnownRegion() {
		return lastKnownRegion;
	}

	public void setLastKnownRegion(Position pos) {
		this.lastKnownRegion = pos;
	}
}
